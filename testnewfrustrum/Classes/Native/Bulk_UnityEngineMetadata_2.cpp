﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2
extern TypeInfo ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo;
extern const Il2CppGenericContainer ThreadSafeDictionary_2_t1450_Il2CppGenericContainer;
extern TypeInfo ThreadSafeDictionary_2_t1450_gp_TKey_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ThreadSafeDictionary_2_t1450_gp_TKey_0_il2cpp_TypeInfo_GenericParamFull = { &ThreadSafeDictionary_2_t1450_Il2CppGenericContainer, NULL, "TKey", 0, 0 };
extern TypeInfo ThreadSafeDictionary_2_t1450_gp_TValue_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ThreadSafeDictionary_2_t1450_gp_TValue_1_il2cpp_TypeInfo_GenericParamFull = { &ThreadSafeDictionary_2_t1450_Il2CppGenericContainer, NULL, "TValue", 1, 0 };
static const Il2CppGenericParameter* ThreadSafeDictionary_2_t1450_Il2CppGenericParametersArray[2] = 
{
	&ThreadSafeDictionary_2_t1450_gp_TKey_0_il2cpp_TypeInfo_GenericParamFull,
	&ThreadSafeDictionary_2_t1450_gp_TValue_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer ThreadSafeDictionary_2_t1450_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo, 2, 0, ThreadSafeDictionary_2_t1450_Il2CppGenericParametersArray };
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1521_0_0_0;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1521_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2__ctor_m7085_ParameterInfos[] = 
{
	{"valueFactory", 0, 134219433, 0, &ThreadSafeDictionaryValueFactory_2_t1521_0_0_0},
};
extern const Il2CppType Void_t168_0_0_0;
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
extern const MethodInfo ThreadSafeDictionary_2__ctor_m7085_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2__ctor_m7085_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1609/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_t410_0_0_0;
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m7086_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t410_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1610/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1450_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1450_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_Get_m7087_ParameterInfos[] = 
{
	{"key", 0, 134219434, 0, &ThreadSafeDictionary_2_t1450_gp_0_0_0_0},
};
extern const Il2CppType ThreadSafeDictionary_2_t1450_gp_1_0_0_0;
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Get(TKey)
extern const MethodInfo ThreadSafeDictionary_2_Get_m7087_MethodInfo = 
{
	"Get"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionary_2_t1450_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_Get_m7087_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1611/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1450_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_AddValue_m7088_ParameterInfos[] = 
{
	{"key", 0, 134219435, 0, &ThreadSafeDictionary_2_t1450_gp_0_0_0_0},
};
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::AddValue(TKey)
extern const MethodInfo ThreadSafeDictionary_2_AddValue_m7088_MethodInfo = 
{
	"AddValue"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionary_2_t1450_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_AddValue_m7088_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1612/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1450_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1450_gp_1_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_Add_m7089_ParameterInfos[] = 
{
	{"key", 0, 134219436, 0, &ThreadSafeDictionary_2_t1450_gp_0_0_0_0},
	{"value", 1, 134219437, 0, &ThreadSafeDictionary_2_t1450_gp_1_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Add(TKey,TValue)
extern const MethodInfo ThreadSafeDictionary_2_Add_m7089_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_Add_m7089_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1613/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICollection_1_t1522_0_0_0;
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_Keys()
extern const MethodInfo ThreadSafeDictionary_2_get_Keys_m7090_MethodInfo = 
{
	"get_Keys"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &ICollection_1_t1522_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1614/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1450_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_Remove_m7091_ParameterInfos[] = 
{
	{"key", 0, 134219438, 0, &ThreadSafeDictionary_2_t1450_gp_0_0_0_0},
};
extern const Il2CppType Boolean_t169_0_0_0;
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Remove(TKey)
extern const MethodInfo ThreadSafeDictionary_2_Remove_m7091_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_Remove_m7091_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1615/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1450_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1450_gp_1_1_0_2;
extern const Il2CppType ThreadSafeDictionary_2_t1450_gp_1_1_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_TryGetValue_m7092_ParameterInfos[] = 
{
	{"key", 0, 134219439, 0, &ThreadSafeDictionary_2_t1450_gp_0_0_0_0},
	{"value", 1, 134219440, 0, &ThreadSafeDictionary_2_t1450_gp_1_1_0_2},
};
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::TryGetValue(TKey,TValue&)
extern const MethodInfo ThreadSafeDictionary_2_TryGetValue_m7092_MethodInfo = 
{
	"TryGetValue"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_TryGetValue_m7092_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1616/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ICollection_1_t1523_0_0_0;
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_Values()
extern const MethodInfo ThreadSafeDictionary_2_get_Values_m7093_MethodInfo = 
{
	"get_Values"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &ICollection_1_t1523_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1617/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1450_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_get_Item_m7094_ParameterInfos[] = 
{
	{"key", 0, 134219441, 0, &ThreadSafeDictionary_2_t1450_gp_0_0_0_0},
};
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_Item(TKey)
extern const MethodInfo ThreadSafeDictionary_2_get_Item_m7094_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionary_2_t1450_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_get_Item_m7094_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1618/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionary_2_t1450_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1450_gp_1_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_set_Item_m7095_ParameterInfos[] = 
{
	{"key", 0, 134219442, 0, &ThreadSafeDictionary_2_t1450_gp_0_0_0_0},
	{"value", 1, 134219443, 0, &ThreadSafeDictionary_2_t1450_gp_1_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::set_Item(TKey,TValue)
extern const MethodInfo ThreadSafeDictionary_2_set_Item_m7095_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_set_Item_m7095_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1619/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2_t1524_0_0_0;
extern const Il2CppType KeyValuePair_2_t1524_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_Add_m7096_ParameterInfos[] = 
{
	{"item", 0, 134219444, 0, &KeyValuePair_2_t1524_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern const MethodInfo ThreadSafeDictionary_2_Add_m7096_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_Add_m7096_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1620/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Clear()
extern const MethodInfo ThreadSafeDictionary_2_Clear_m7097_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1621/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2_t1524_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_Contains_m7098_ParameterInfos[] = 
{
	{"item", 0, 134219445, 0, &KeyValuePair_2_t1524_0_0_0},
};
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern const MethodInfo ThreadSafeDictionary_2_Contains_m7098_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_Contains_m7098_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1622/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2U5BU5D_t1525_0_0_0;
extern const Il2CppType KeyValuePair_2U5BU5D_t1525_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_CopyTo_m7099_ParameterInfos[] = 
{
	{"array", 0, 134219446, 0, &KeyValuePair_2U5BU5D_t1525_0_0_0},
	{"arrayIndex", 1, 134219447, 0, &Int32_t127_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern const MethodInfo ThreadSafeDictionary_2_CopyTo_m7099_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_CopyTo_m7099_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1623/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_Count()
extern const MethodInfo ThreadSafeDictionary_2_get_Count_m7100_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1624/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::get_IsReadOnly()
extern const MethodInfo ThreadSafeDictionary_2_get_IsReadOnly_m7101_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1625/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType KeyValuePair_2_t1524_0_0_0;
static const ParameterInfo ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_Remove_m7102_ParameterInfos[] = 
{
	{"item", 0, 134219448, 0, &KeyValuePair_2_t1524_0_0_0},
};
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern const MethodInfo ThreadSafeDictionary_2_Remove_m7102_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionary_2_t1450_ThreadSafeDictionary_2_Remove_m7102_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1626/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t1526_0_0_0;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2::GetEnumerator()
extern const MethodInfo ThreadSafeDictionary_2_GetEnumerator_m7103_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1526_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1627/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ThreadSafeDictionary_2_t1450_MethodInfos[] =
{
	&ThreadSafeDictionary_2__ctor_m7085_MethodInfo,
	&ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m7086_MethodInfo,
	&ThreadSafeDictionary_2_Get_m7087_MethodInfo,
	&ThreadSafeDictionary_2_AddValue_m7088_MethodInfo,
	&ThreadSafeDictionary_2_Add_m7089_MethodInfo,
	&ThreadSafeDictionary_2_get_Keys_m7090_MethodInfo,
	&ThreadSafeDictionary_2_Remove_m7091_MethodInfo,
	&ThreadSafeDictionary_2_TryGetValue_m7092_MethodInfo,
	&ThreadSafeDictionary_2_get_Values_m7093_MethodInfo,
	&ThreadSafeDictionary_2_get_Item_m7094_MethodInfo,
	&ThreadSafeDictionary_2_set_Item_m7095_MethodInfo,
	&ThreadSafeDictionary_2_Add_m7096_MethodInfo,
	&ThreadSafeDictionary_2_Clear_m7097_MethodInfo,
	&ThreadSafeDictionary_2_Contains_m7098_MethodInfo,
	&ThreadSafeDictionary_2_CopyTo_m7099_MethodInfo,
	&ThreadSafeDictionary_2_get_Count_m7100_MethodInfo,
	&ThreadSafeDictionary_2_get_IsReadOnly_m7101_MethodInfo,
	&ThreadSafeDictionary_2_Remove_m7102_MethodInfo,
	&ThreadSafeDictionary_2_GetEnumerator_m7103_MethodInfo,
	NULL
};
extern const MethodInfo ThreadSafeDictionary_2_get_Keys_m7090_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1450____Keys_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* parent */
	, "Keys"/* name */
	, &ThreadSafeDictionary_2_get_Keys_m7090_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ThreadSafeDictionary_2_get_Values_m7093_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1450____Values_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* parent */
	, "Values"/* name */
	, &ThreadSafeDictionary_2_get_Values_m7093_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ThreadSafeDictionary_2_get_Item_m7094_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_set_Item_m7095_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1450____Item_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &ThreadSafeDictionary_2_get_Item_m7094_MethodInfo/* get */
	, &ThreadSafeDictionary_2_set_Item_m7095_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ThreadSafeDictionary_2_get_Count_m7100_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1450____Count_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ThreadSafeDictionary_2_get_Count_m7100_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ThreadSafeDictionary_2_get_IsReadOnly_m7101_MethodInfo;
static const PropertyInfo ThreadSafeDictionary_2_t1450____IsReadOnly_PropertyInfo = 
{
	&ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ThreadSafeDictionary_2_get_IsReadOnly_m7101_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ThreadSafeDictionary_2_t1450_PropertyInfos[] =
{
	&ThreadSafeDictionary_2_t1450____Keys_PropertyInfo,
	&ThreadSafeDictionary_2_t1450____Values_PropertyInfo,
	&ThreadSafeDictionary_2_t1450____Item_PropertyInfo,
	&ThreadSafeDictionary_2_t1450____Count_PropertyInfo,
	&ThreadSafeDictionary_2_t1450____IsReadOnly_PropertyInfo,
	NULL
};
extern const MethodInfo Object_Equals_m540_MethodInfo;
extern const MethodInfo Object_Finalize_m515_MethodInfo;
extern const MethodInfo Object_GetHashCode_m541_MethodInfo;
extern const MethodInfo Object_ToString_m542_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m7086_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Add_m7089_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Remove_m7091_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_TryGetValue_m7092_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Add_m7096_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Clear_m7097_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Contains_m7098_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_CopyTo_m7099_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_Remove_m7102_MethodInfo;
extern const MethodInfo ThreadSafeDictionary_2_GetEnumerator_m7103_MethodInfo;
static const Il2CppMethodReference ThreadSafeDictionary_2_t1450_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m7086_MethodInfo,
	&ThreadSafeDictionary_2_Add_m7089_MethodInfo,
	&ThreadSafeDictionary_2_Remove_m7091_MethodInfo,
	&ThreadSafeDictionary_2_TryGetValue_m7092_MethodInfo,
	&ThreadSafeDictionary_2_get_Item_m7094_MethodInfo,
	&ThreadSafeDictionary_2_set_Item_m7095_MethodInfo,
	&ThreadSafeDictionary_2_get_Keys_m7090_MethodInfo,
	&ThreadSafeDictionary_2_get_Values_m7093_MethodInfo,
	&ThreadSafeDictionary_2_get_Count_m7100_MethodInfo,
	&ThreadSafeDictionary_2_get_IsReadOnly_m7101_MethodInfo,
	&ThreadSafeDictionary_2_Add_m7096_MethodInfo,
	&ThreadSafeDictionary_2_Clear_m7097_MethodInfo,
	&ThreadSafeDictionary_2_Contains_m7098_MethodInfo,
	&ThreadSafeDictionary_2_CopyTo_m7099_MethodInfo,
	&ThreadSafeDictionary_2_Remove_m7102_MethodInfo,
	&ThreadSafeDictionary_2_GetEnumerator_m7103_MethodInfo,
};
static bool ThreadSafeDictionary_2_t1450_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerable_t550_0_0_0;
extern const Il2CppType IDictionary_2_t1527_0_0_0;
extern const Il2CppType ICollection_1_t1528_0_0_0;
extern const Il2CppType IEnumerable_1_t1529_0_0_0;
static const Il2CppType* ThreadSafeDictionary_2_t1450_InterfacesTypeInfos[] = 
{
	&IEnumerable_t550_0_0_0,
	&IDictionary_2_t1527_0_0_0,
	&ICollection_1_t1528_0_0_0,
	&IEnumerable_1_t1529_0_0_0,
};
static Il2CppInterfaceOffsetPair ThreadSafeDictionary_2_t1450_InterfacesOffsets[] = 
{
	{ &IEnumerable_t550_0_0_0, 4},
	{ &IDictionary_2_t1527_0_0_0, 5},
	{ &ICollection_1_t1528_0_0_0, 12},
	{ &IEnumerable_1_t1529_0_0_0, 19},
};
extern const Il2CppGenericMethod Dictionary_2_GetEnumerator_m7214_GenericMethod;
extern const Il2CppType Enumerator_t1530_0_0_0;
extern const Il2CppGenericMethod ThreadSafeDictionary_2_AddValue_m7215_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_TryGetValue_m7216_GenericMethod;
extern const Il2CppGenericMethod ThreadSafeDictionaryValueFactory_2_Invoke_m7217_GenericMethod;
extern const Il2CppType Dictionary_2_t1531_0_0_0;
extern const Il2CppGenericMethod Dictionary_2__ctor_m7218_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_set_Item_m7219_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2__ctor_m7220_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_get_Keys_m7221_GenericMethod;
extern const Il2CppGenericMethod ThreadSafeDictionary_2_get_Item_m7222_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_get_Values_m7223_GenericMethod;
extern const Il2CppGenericMethod ThreadSafeDictionary_2_Get_m7224_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_get_Count_m7225_GenericMethod;
static Il2CppRGCTXDefinition ThreadSafeDictionary_2_t1450_RGCTXData[15] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_GetEnumerator_m7214_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Enumerator_t1530_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ThreadSafeDictionary_2_AddValue_m7215_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_TryGetValue_m7216_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ThreadSafeDictionaryValueFactory_2_Invoke_m7217_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Dictionary_2_t1531_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2__ctor_m7218_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_set_Item_m7219_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2__ctor_m7220_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_get_Keys_m7221_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ThreadSafeDictionary_2_get_Item_m7222_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_get_Values_m7223_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ThreadSafeDictionary_2_Get_m7224_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_get_Count_m7225_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ThreadSafeDictionary_2_t1450_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t1450_1_0_0;
extern const Il2CppType Object_t_0_0_0;
extern TypeInfo ReflectionUtils_t1293_il2cpp_TypeInfo;
extern const Il2CppType ReflectionUtils_t1293_0_0_0;
struct ThreadSafeDictionary_2_t1450;
const Il2CppTypeDefinitionMetadata ThreadSafeDictionary_2_t1450_DefinitionMetadata = 
{
	&ReflectionUtils_t1293_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, ThreadSafeDictionary_2_t1450_InterfacesTypeInfos/* implementedInterfaces */
	, ThreadSafeDictionary_2_t1450_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ThreadSafeDictionary_2_t1450_VTable/* vtableMethods */
	, ThreadSafeDictionary_2_t1450_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, ThreadSafeDictionary_2_t1450_RGCTXData/* rgctxDefinition */
	, 1010/* fieldStart */

};
TypeInfo ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadSafeDictionary`2"/* name */
	, ""/* namespaze */
	, ThreadSafeDictionary_2_t1450_MethodInfos/* methods */
	, ThreadSafeDictionary_2_t1450_PropertyInfos/* properties */
	, NULL/* events */
	, &ThreadSafeDictionary_2_t1450_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 757/* custom_attributes_cache */
	, &ThreadSafeDictionary_2_t1450_0_0_0/* byval_arg */
	, &ThreadSafeDictionary_2_t1450_1_0_0/* this_arg */
	, &ThreadSafeDictionary_2_t1450_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &ThreadSafeDictionary_2_t1450_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048834/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 20/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_GetDelegat.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/GetDelegate
extern TypeInfo GetDelegate_t1284_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_GetDelegatMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo GetDelegate_t1284_GetDelegate__ctor_m6684_ParameterInfos[] = 
{
	{"object", 0, 134219449, 0, &Object_t_0_0_0},
	{"method", 1, 134219450, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/GetDelegate::.ctor(System.Object,System.IntPtr)
extern const MethodInfo GetDelegate__ctor_m6684_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GetDelegate__ctor_m6684/* method */
	, &GetDelegate_t1284_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, GetDelegate_t1284_GetDelegate__ctor_m6684_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1628/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GetDelegate_t1284_GetDelegate_Invoke_m6685_ParameterInfos[] = 
{
	{"source", 0, 134219451, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/GetDelegate::Invoke(System.Object)
extern const MethodInfo GetDelegate_Invoke_m6685_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&GetDelegate_Invoke_m6685/* method */
	, &GetDelegate_t1284_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, GetDelegate_t1284_GetDelegate_Invoke_m6685_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1629/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo GetDelegate_t1284_GetDelegate_BeginInvoke_m6686_ParameterInfos[] = 
{
	{"source", 0, 134219452, 0, &Object_t_0_0_0},
	{"callback", 1, 134219453, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134219454, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t304_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/GetDelegate::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo GetDelegate_BeginInvoke_m6686_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&GetDelegate_BeginInvoke_m6686/* method */
	, &GetDelegate_t1284_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, GetDelegate_t1284_GetDelegate_BeginInvoke_m6686_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1630/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo GetDelegate_t1284_GetDelegate_EndInvoke_m6687_ParameterInfos[] = 
{
	{"result", 0, 134219455, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/GetDelegate::EndInvoke(System.IAsyncResult)
extern const MethodInfo GetDelegate_EndInvoke_m6687_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&GetDelegate_EndInvoke_m6687/* method */
	, &GetDelegate_t1284_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, GetDelegate_t1284_GetDelegate_EndInvoke_m6687_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1631/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GetDelegate_t1284_MethodInfos[] =
{
	&GetDelegate__ctor_m6684_MethodInfo,
	&GetDelegate_Invoke_m6685_MethodInfo,
	&GetDelegate_BeginInvoke_m6686_MethodInfo,
	&GetDelegate_EndInvoke_m6687_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2554_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2555_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2556_MethodInfo;
extern const MethodInfo Delegate_Clone_m2557_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2558_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2559_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2560_MethodInfo;
extern const MethodInfo GetDelegate_Invoke_m6685_MethodInfo;
extern const MethodInfo GetDelegate_BeginInvoke_m6686_MethodInfo;
extern const MethodInfo GetDelegate_EndInvoke_m6687_MethodInfo;
static const Il2CppMethodReference GetDelegate_t1284_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&GetDelegate_Invoke_m6685_MethodInfo,
	&GetDelegate_BeginInvoke_m6686_MethodInfo,
	&GetDelegate_EndInvoke_m6687_MethodInfo,
};
static bool GetDelegate_t1284_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t512_0_0_0;
extern const Il2CppType ISerializable_t513_0_0_0;
static Il2CppInterfaceOffsetPair GetDelegate_t1284_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GetDelegate_t1284_0_0_0;
extern const Il2CppType GetDelegate_t1284_1_0_0;
extern const Il2CppType MulticastDelegate_t307_0_0_0;
struct GetDelegate_t1284;
const Il2CppTypeDefinitionMetadata GetDelegate_t1284_DefinitionMetadata = 
{
	&ReflectionUtils_t1293_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GetDelegate_t1284_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, GetDelegate_t1284_VTable/* vtableMethods */
	, GetDelegate_t1284_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GetDelegate_t1284_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GetDelegate"/* name */
	, ""/* namespaze */
	, GetDelegate_t1284_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GetDelegate_t1284_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GetDelegate_t1284_0_0_0/* byval_arg */
	, &GetDelegate_t1284_1_0_0/* this_arg */
	, &GetDelegate_t1284_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_GetDelegate_t1284/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GetDelegate_t1284)/* instance_size */
	, sizeof (GetDelegate_t1284)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_SetDelegat.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/SetDelegate
extern TypeInfo SetDelegate_t1285_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_SetDelegatMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo SetDelegate_t1285_SetDelegate__ctor_m6688_ParameterInfos[] = 
{
	{"object", 0, 134219456, 0, &Object_t_0_0_0},
	{"method", 1, 134219457, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::.ctor(System.Object,System.IntPtr)
extern const MethodInfo SetDelegate__ctor_m6688_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetDelegate__ctor_m6688/* method */
	, &SetDelegate_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, SetDelegate_t1285_SetDelegate__ctor_m6688_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1632/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SetDelegate_t1285_SetDelegate_Invoke_m6689_ParameterInfos[] = 
{
	{"source", 0, 134219458, 0, &Object_t_0_0_0},
	{"value", 1, 134219459, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::Invoke(System.Object,System.Object)
extern const MethodInfo SetDelegate_Invoke_m6689_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&SetDelegate_Invoke_m6689/* method */
	, &SetDelegate_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, SetDelegate_t1285_SetDelegate_Invoke_m6689_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1633/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SetDelegate_t1285_SetDelegate_BeginInvoke_m6690_ParameterInfos[] = 
{
	{"source", 0, 134219460, 0, &Object_t_0_0_0},
	{"value", 1, 134219461, 0, &Object_t_0_0_0},
	{"callback", 2, 134219462, 0, &AsyncCallback_t305_0_0_0},
	{"object", 3, 134219463, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/SetDelegate::BeginInvoke(System.Object,System.Object,System.AsyncCallback,System.Object)
extern const MethodInfo SetDelegate_BeginInvoke_m6690_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&SetDelegate_BeginInvoke_m6690/* method */
	, &SetDelegate_t1285_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, SetDelegate_t1285_SetDelegate_BeginInvoke_m6690_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1634/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo SetDelegate_t1285_SetDelegate_EndInvoke_m6691_ParameterInfos[] = 
{
	{"result", 0, 134219464, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::EndInvoke(System.IAsyncResult)
extern const MethodInfo SetDelegate_EndInvoke_m6691_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&SetDelegate_EndInvoke_m6691/* method */
	, &SetDelegate_t1285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, SetDelegate_t1285_SetDelegate_EndInvoke_m6691_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1635/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SetDelegate_t1285_MethodInfos[] =
{
	&SetDelegate__ctor_m6688_MethodInfo,
	&SetDelegate_Invoke_m6689_MethodInfo,
	&SetDelegate_BeginInvoke_m6690_MethodInfo,
	&SetDelegate_EndInvoke_m6691_MethodInfo,
	NULL
};
extern const MethodInfo SetDelegate_Invoke_m6689_MethodInfo;
extern const MethodInfo SetDelegate_BeginInvoke_m6690_MethodInfo;
extern const MethodInfo SetDelegate_EndInvoke_m6691_MethodInfo;
static const Il2CppMethodReference SetDelegate_t1285_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&SetDelegate_Invoke_m6689_MethodInfo,
	&SetDelegate_BeginInvoke_m6690_MethodInfo,
	&SetDelegate_EndInvoke_m6691_MethodInfo,
};
static bool SetDelegate_t1285_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SetDelegate_t1285_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SetDelegate_t1285_0_0_0;
extern const Il2CppType SetDelegate_t1285_1_0_0;
struct SetDelegate_t1285;
const Il2CppTypeDefinitionMetadata SetDelegate_t1285_DefinitionMetadata = 
{
	&ReflectionUtils_t1293_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SetDelegate_t1285_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, SetDelegate_t1285_VTable/* vtableMethods */
	, SetDelegate_t1285_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SetDelegate_t1285_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetDelegate"/* name */
	, ""/* namespaze */
	, SetDelegate_t1285_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetDelegate_t1285_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetDelegate_t1285_0_0_0/* byval_arg */
	, &SetDelegate_t1285_1_0_0/* this_arg */
	, &SetDelegate_t1285_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_SetDelegate_t1285/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetDelegate_t1285)/* instance_size */
	, sizeof (SetDelegate_t1285)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_Constructo.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
extern TypeInfo ConstructorDelegate_t1286_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ConstructoMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo ConstructorDelegate_t1286_ConstructorDelegate__ctor_m6692_ParameterInfos[] = 
{
	{"object", 0, 134219465, 0, &Object_t_0_0_0},
	{"method", 1, 134219466, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::.ctor(System.Object,System.IntPtr)
extern const MethodInfo ConstructorDelegate__ctor_m6692_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ConstructorDelegate__ctor_m6692/* method */
	, &ConstructorDelegate_t1286_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, ConstructorDelegate_t1286_ConstructorDelegate__ctor_m6692_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1636/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo ConstructorDelegate_t1286_ConstructorDelegate_Invoke_m6693_ParameterInfos[] = 
{
	{"args", 0, 134219467, 758, &ObjectU5BU5D_t115_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::Invoke(System.Object[])
extern const MethodInfo ConstructorDelegate_Invoke_m6693_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&ConstructorDelegate_Invoke_m6693/* method */
	, &ConstructorDelegate_t1286_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ConstructorDelegate_t1286_ConstructorDelegate_Invoke_m6693_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1637/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ConstructorDelegate_t1286_ConstructorDelegate_BeginInvoke_m6694_ParameterInfos[] = 
{
	{"args", 0, 134219468, 759, &ObjectU5BU5D_t115_0_0_0},
	{"callback", 1, 134219469, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134219470, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::BeginInvoke(System.Object[],System.AsyncCallback,System.Object)
extern const MethodInfo ConstructorDelegate_BeginInvoke_m6694_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&ConstructorDelegate_BeginInvoke_m6694/* method */
	, &ConstructorDelegate_t1286_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, ConstructorDelegate_t1286_ConstructorDelegate_BeginInvoke_m6694_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1638/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo ConstructorDelegate_t1286_ConstructorDelegate_EndInvoke_m6695_ParameterInfos[] = 
{
	{"result", 0, 134219471, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::EndInvoke(System.IAsyncResult)
extern const MethodInfo ConstructorDelegate_EndInvoke_m6695_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&ConstructorDelegate_EndInvoke_m6695/* method */
	, &ConstructorDelegate_t1286_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ConstructorDelegate_t1286_ConstructorDelegate_EndInvoke_m6695_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1639/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ConstructorDelegate_t1286_MethodInfos[] =
{
	&ConstructorDelegate__ctor_m6692_MethodInfo,
	&ConstructorDelegate_Invoke_m6693_MethodInfo,
	&ConstructorDelegate_BeginInvoke_m6694_MethodInfo,
	&ConstructorDelegate_EndInvoke_m6695_MethodInfo,
	NULL
};
extern const MethodInfo ConstructorDelegate_Invoke_m6693_MethodInfo;
extern const MethodInfo ConstructorDelegate_BeginInvoke_m6694_MethodInfo;
extern const MethodInfo ConstructorDelegate_EndInvoke_m6695_MethodInfo;
static const Il2CppMethodReference ConstructorDelegate_t1286_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&ConstructorDelegate_Invoke_m6693_MethodInfo,
	&ConstructorDelegate_BeginInvoke_m6694_MethodInfo,
	&ConstructorDelegate_EndInvoke_m6695_MethodInfo,
};
static bool ConstructorDelegate_t1286_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ConstructorDelegate_t1286_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ConstructorDelegate_t1286_0_0_0;
extern const Il2CppType ConstructorDelegate_t1286_1_0_0;
struct ConstructorDelegate_t1286;
const Il2CppTypeDefinitionMetadata ConstructorDelegate_t1286_DefinitionMetadata = 
{
	&ReflectionUtils_t1293_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ConstructorDelegate_t1286_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, ConstructorDelegate_t1286_VTable/* vtableMethods */
	, ConstructorDelegate_t1286_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ConstructorDelegate_t1286_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ConstructorDelegate"/* name */
	, ""/* namespaze */
	, ConstructorDelegate_t1286_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ConstructorDelegate_t1286_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ConstructorDelegate_t1286_0_0_0/* byval_arg */
	, &ConstructorDelegate_t1286_1_0_0/* this_arg */
	, &ConstructorDelegate_t1286_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_ConstructorDelegate_t1286/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ConstructorDelegate_t1286)/* instance_size */
	, sizeof (ConstructorDelegate_t1286)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2
extern TypeInfo ThreadSafeDictionaryValueFactory_2_t1451_il2cpp_TypeInfo;
extern const Il2CppGenericContainer ThreadSafeDictionaryValueFactory_2_t1451_Il2CppGenericContainer;
extern TypeInfo ThreadSafeDictionaryValueFactory_2_t1451_gp_TKey_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ThreadSafeDictionaryValueFactory_2_t1451_gp_TKey_0_il2cpp_TypeInfo_GenericParamFull = { &ThreadSafeDictionaryValueFactory_2_t1451_Il2CppGenericContainer, NULL, "TKey", 0, 0 };
extern TypeInfo ThreadSafeDictionaryValueFactory_2_t1451_gp_TValue_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ThreadSafeDictionaryValueFactory_2_t1451_gp_TValue_1_il2cpp_TypeInfo_GenericParamFull = { &ThreadSafeDictionaryValueFactory_2_t1451_Il2CppGenericContainer, NULL, "TValue", 1, 0 };
static const Il2CppGenericParameter* ThreadSafeDictionaryValueFactory_2_t1451_Il2CppGenericParametersArray[2] = 
{
	&ThreadSafeDictionaryValueFactory_2_t1451_gp_TKey_0_il2cpp_TypeInfo_GenericParamFull,
	&ThreadSafeDictionaryValueFactory_2_t1451_gp_TValue_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer ThreadSafeDictionaryValueFactory_2_t1451_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&ThreadSafeDictionaryValueFactory_2_t1451_il2cpp_TypeInfo, 2, 0, ThreadSafeDictionaryValueFactory_2_t1451_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo ThreadSafeDictionaryValueFactory_2_t1451_ThreadSafeDictionaryValueFactory_2__ctor_m7104_ParameterInfos[] = 
{
	{"object", 0, 134219472, 0, &Object_t_0_0_0},
	{"method", 1, 134219473, 0, &IntPtr_t_0_0_0},
};
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo ThreadSafeDictionaryValueFactory_2__ctor_m7104_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &ThreadSafeDictionaryValueFactory_2_t1451_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionaryValueFactory_2_t1451_ThreadSafeDictionaryValueFactory_2__ctor_m7104_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1640/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1451_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1451_gp_0_0_0_0;
static const ParameterInfo ThreadSafeDictionaryValueFactory_2_t1451_ThreadSafeDictionaryValueFactory_2_Invoke_m7105_ParameterInfos[] = 
{
	{"key", 0, 134219474, 0, &ThreadSafeDictionaryValueFactory_2_t1451_gp_0_0_0_0},
};
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1451_gp_1_0_0_0;
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2::Invoke(TKey)
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_Invoke_m7105_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &ThreadSafeDictionaryValueFactory_2_t1451_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionaryValueFactory_2_t1451_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionaryValueFactory_2_t1451_ThreadSafeDictionaryValueFactory_2_Invoke_m7105_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1641/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1451_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo ThreadSafeDictionaryValueFactory_2_t1451_ThreadSafeDictionaryValueFactory_2_BeginInvoke_m7106_ParameterInfos[] = 
{
	{"key", 0, 134219475, 0, &ThreadSafeDictionaryValueFactory_2_t1451_gp_0_0_0_0},
	{"callback", 1, 134219476, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134219477, 0, &Object_t_0_0_0},
};
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2::BeginInvoke(TKey,System.AsyncCallback,System.Object)
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_BeginInvoke_m7106_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &ThreadSafeDictionaryValueFactory_2_t1451_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionaryValueFactory_2_t1451_ThreadSafeDictionaryValueFactory_2_BeginInvoke_m7106_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1642/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo ThreadSafeDictionaryValueFactory_2_t1451_ThreadSafeDictionaryValueFactory_2_EndInvoke_m7107_ParameterInfos[] = 
{
	{"result", 0, 134219478, 0, &IAsyncResult_t304_0_0_0},
};
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_EndInvoke_m7107_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &ThreadSafeDictionaryValueFactory_2_t1451_il2cpp_TypeInfo/* declaring_type */
	, &ThreadSafeDictionaryValueFactory_2_t1451_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ThreadSafeDictionaryValueFactory_2_t1451_ThreadSafeDictionaryValueFactory_2_EndInvoke_m7107_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1643/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ThreadSafeDictionaryValueFactory_2_t1451_MethodInfos[] =
{
	&ThreadSafeDictionaryValueFactory_2__ctor_m7104_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_Invoke_m7105_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_BeginInvoke_m7106_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_EndInvoke_m7107_MethodInfo,
	NULL
};
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_Invoke_m7105_MethodInfo;
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_BeginInvoke_m7106_MethodInfo;
extern const MethodInfo ThreadSafeDictionaryValueFactory_2_EndInvoke_m7107_MethodInfo;
static const Il2CppMethodReference ThreadSafeDictionaryValueFactory_2_t1451_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_Invoke_m7105_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_BeginInvoke_m7106_MethodInfo,
	&ThreadSafeDictionaryValueFactory_2_EndInvoke_m7107_MethodInfo,
};
static bool ThreadSafeDictionaryValueFactory_2_t1451_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ThreadSafeDictionaryValueFactory_2_t1451_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1451_0_0_0;
extern const Il2CppType ThreadSafeDictionaryValueFactory_2_t1451_1_0_0;
struct ThreadSafeDictionaryValueFactory_2_t1451;
const Il2CppTypeDefinitionMetadata ThreadSafeDictionaryValueFactory_2_t1451_DefinitionMetadata = 
{
	&ReflectionUtils_t1293_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ThreadSafeDictionaryValueFactory_2_t1451_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, ThreadSafeDictionaryValueFactory_2_t1451_VTable/* vtableMethods */
	, ThreadSafeDictionaryValueFactory_2_t1451_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ThreadSafeDictionaryValueFactory_2_t1451_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ThreadSafeDictionaryValueFactory`2"/* name */
	, ""/* namespaze */
	, ThreadSafeDictionaryValueFactory_2_t1451_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ThreadSafeDictionaryValueFactory_2_t1451_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ThreadSafeDictionaryValueFactory_2_t1451_0_0_0/* byval_arg */
	, &ThreadSafeDictionaryValueFactory_2_t1451_1_0_0/* this_arg */
	, &ThreadSafeDictionaryValueFactory_2_t1451_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &ThreadSafeDictionaryValueFactory_2_t1451_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetCons.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
extern TypeInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetConsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1::.ctor()
extern const MethodInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m6696_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m6696/* method */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1644/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m6697_ParameterInfos[] = 
{
	{"args", 0, 134219479, 0, &ObjectU5BU5D_t115_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1::<>m__0(System.Object[])
extern const MethodInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m6697_MethodInfo = 
{
	"<>m__0"/* name */
	, (methodPointerType)&U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m6697/* method */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m6697_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1645/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_MethodInfos[] =
{
	&U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m6696_MethodInfo,
	&U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m6697_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_0_0_0;
extern const Il2CppType U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_1_0_0;
struct U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288;
const Il2CppTypeDefinitionMetadata U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_DefinitionMetadata = 
{
	&ReflectionUtils_t1293_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_VTable/* vtableMethods */
	, U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1013/* fieldStart */

};
TypeInfo U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetConstructorByReflection>c__AnonStorey1"/* name */
	, ""/* namespaze */
	, U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 760/* custom_attributes_cache */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_0_0_0/* byval_arg */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_1_0_0/* this_arg */
	, &U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288)/* instance_size */
	, sizeof (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
extern TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetMMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2::.ctor()
extern const MethodInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m6698_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m6698/* method */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1646/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m6699_ParameterInfos[] = 
{
	{"source", 0, 134219480, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2::<>m__1(System.Object)
extern const MethodInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m6699_MethodInfo = 
{
	"<>m__1"/* name */
	, (methodPointerType)&U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m6699/* method */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m6699_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1647/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_MethodInfos[] =
{
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m6698_MethodInfo,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m6699_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_0_0_0;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_1_0_0;
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289;
const Il2CppTypeDefinitionMetadata U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_DefinitionMetadata = 
{
	&ReflectionUtils_t1293_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_VTable/* vtableMethods */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1014/* fieldStart */

};
TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetGetMethodByReflection>c__AnonStorey2"/* name */
	, ""/* namespaze */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 761/* custom_attributes_cache */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_0_0_0/* byval_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_1_0_0/* this_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289)/* instance_size */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM_0.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
extern TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3::.ctor()
extern const MethodInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m6700_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m6700/* method */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1648/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m6701_ParameterInfos[] = 
{
	{"source", 0, 134219481, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3::<>m__2(System.Object)
extern const MethodInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m6701_MethodInfo = 
{
	"<>m__2"/* name */
	, (methodPointerType)&U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m6701/* method */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m6701_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1649/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_MethodInfos[] =
{
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m6700_MethodInfo,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m6701_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_0_0_0;
extern const Il2CppType U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_1_0_0;
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290;
const Il2CppTypeDefinitionMetadata U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_DefinitionMetadata = 
{
	&ReflectionUtils_t1293_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_VTable/* vtableMethods */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1015/* fieldStart */

};
TypeInfo U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetGetMethodByReflection>c__AnonStorey3"/* name */
	, ""/* namespaze */
	, U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 762/* custom_attributes_cache */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_0_0_0/* byval_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_1_0_0/* this_arg */
	, &U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290)/* instance_size */
	, sizeof (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
extern TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetMMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4::.ctor()
extern const MethodInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m6702_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m6702/* method */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1650/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m6703_ParameterInfos[] = 
{
	{"source", 0, 134219482, 0, &Object_t_0_0_0},
	{"value", 1, 134219483, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4::<>m__3(System.Object,System.Object)
extern const MethodInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m6703_MethodInfo = 
{
	"<>m__3"/* name */
	, (methodPointerType)&U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m6703/* method */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m6703_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1651/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_MethodInfos[] =
{
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m6702_MethodInfo,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m6703_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_0_0_0;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_1_0_0;
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291;
const Il2CppTypeDefinitionMetadata U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_DefinitionMetadata = 
{
	&ReflectionUtils_t1293_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_VTable/* vtableMethods */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1016/* fieldStart */

};
TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetSetMethodByReflection>c__AnonStorey4"/* name */
	, ""/* namespaze */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 763/* custom_attributes_cache */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_0_0_0/* byval_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_1_0_0/* this_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291)/* instance_size */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM_0.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
extern TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_il2cpp_TypeInfo;
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM_0MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5::.ctor()
extern const MethodInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m6704_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m6704/* method */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1652/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m6705_ParameterInfos[] = 
{
	{"source", 0, 134219484, 0, &Object_t_0_0_0},
	{"value", 1, 134219485, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5::<>m__4(System.Object,System.Object)
extern const MethodInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m6705_MethodInfo = 
{
	"<>m__4"/* name */
	, (methodPointerType)&U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m6705/* method */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m6705_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1653/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_MethodInfos[] =
{
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m6704_MethodInfo,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m6705_MethodInfo,
	NULL
};
static const Il2CppMethodReference U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_0_0_0;
extern const Il2CppType U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_1_0_0;
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292;
const Il2CppTypeDefinitionMetadata U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_DefinitionMetadata = 
{
	&ReflectionUtils_t1293_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_VTable/* vtableMethods */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1017/* fieldStart */

};
TypeInfo U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "<GetSetMethodByReflection>c__AnonStorey5"/* name */
	, ""/* namespaze */
	, U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 764/* custom_attributes_cache */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_0_0_0/* byval_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_1_0_0/* this_arg */
	, &U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292)/* instance_size */
	, sizeof (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SimpleJson.Reflection.ReflectionUtils
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils.h"
// Metadata Definition SimpleJson.Reflection.ReflectionUtils
// SimpleJson.Reflection.ReflectionUtils
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtilsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void SimpleJson.Reflection.ReflectionUtils::.cctor()
extern const MethodInfo ReflectionUtils__cctor_m6706_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ReflectionUtils__cctor_m6706/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetConstructors_m6707_ParameterInfos[] = 
{
	{"type", 0, 134219413, 0, &Type_t_0_0_0},
};
extern const Il2CppType IEnumerable_1_t1378_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerable`1<System.Reflection.ConstructorInfo> SimpleJson.Reflection.ReflectionUtils::GetConstructors(System.Type)
extern const MethodInfo ReflectionUtils_GetConstructors_m6707_MethodInfo = 
{
	"GetConstructors"/* name */
	, (methodPointerType)&ReflectionUtils_GetConstructors_m6707/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1378_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetConstructors_m6707_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1592/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t878_0_0_0;
extern const Il2CppType TypeU5BU5D_t878_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetConstructorInfo_m6708_ParameterInfos[] = 
{
	{"type", 0, 134219414, 0, &Type_t_0_0_0},
	{"argsType", 1, 134219415, 754, &TypeU5BU5D_t878_0_0_0},
};
extern const Il2CppType ConstructorInfo_t1287_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.ConstructorInfo SimpleJson.Reflection.ReflectionUtils::GetConstructorInfo(System.Type,System.Type[])
extern const MethodInfo ReflectionUtils_GetConstructorInfo_m6708_MethodInfo = 
{
	"GetConstructorInfo"/* name */
	, (methodPointerType)&ReflectionUtils_GetConstructorInfo_m6708/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorInfo_t1287_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetConstructorInfo_m6708_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1593/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetProperties_m6709_ParameterInfos[] = 
{
	{"type", 0, 134219416, 0, &Type_t_0_0_0},
};
extern const Il2CppType IEnumerable_1_t1379_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> SimpleJson.Reflection.ReflectionUtils::GetProperties(System.Type)
extern const MethodInfo ReflectionUtils_GetProperties_m6709_MethodInfo = 
{
	"GetProperties"/* name */
	, (methodPointerType)&ReflectionUtils_GetProperties_m6709/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1379_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetProperties_m6709_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1594/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetFields_m6710_ParameterInfos[] = 
{
	{"type", 0, 134219417, 0, &Type_t_0_0_0},
};
extern const Il2CppType IEnumerable_1_t1380_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> SimpleJson.Reflection.ReflectionUtils::GetFields(System.Type)
extern const MethodInfo ReflectionUtils_GetFields_m6710_MethodInfo = 
{
	"GetFields"/* name */
	, (methodPointerType)&ReflectionUtils_GetFields_m6710/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1380_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetFields_m6710_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1595/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetGetterMethodInfo_m6711_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134219418, 0, &PropertyInfo_t_0_0_0},
};
extern const Il2CppType MethodInfo_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo SimpleJson.Reflection.ReflectionUtils::GetGetterMethodInfo(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetGetterMethodInfo_m6711_MethodInfo = 
{
	"GetGetterMethodInfo"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetterMethodInfo_m6711/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetGetterMethodInfo_m6711_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1596/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetSetterMethodInfo_m6712_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134219419, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo SimpleJson.Reflection.ReflectionUtils::GetSetterMethodInfo(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetSetterMethodInfo_m6712_MethodInfo = 
{
	"GetSetterMethodInfo"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetterMethodInfo_m6712/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetSetterMethodInfo_m6712_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1597/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t878_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetContructor_m6713_ParameterInfos[] = 
{
	{"type", 0, 134219420, 0, &Type_t_0_0_0},
	{"argsType", 1, 134219421, 755, &TypeU5BU5D_t878_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetContructor(System.Type,System.Type[])
extern const MethodInfo ReflectionUtils_GetContructor_m6713_MethodInfo = 
{
	"GetContructor"/* name */
	, (methodPointerType)&ReflectionUtils_GetContructor_m6713/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorDelegate_t1286_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetContructor_m6713_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1598/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ConstructorInfo_t1287_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetConstructorByReflection_m6714_ParameterInfos[] = 
{
	{"constructorInfo", 0, 134219422, 0, &ConstructorInfo_t1287_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetConstructorByReflection(System.Reflection.ConstructorInfo)
extern const MethodInfo ReflectionUtils_GetConstructorByReflection_m6714_MethodInfo = 
{
	"GetConstructorByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetConstructorByReflection_m6714/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorDelegate_t1286_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetConstructorByReflection_m6714_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1599/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t878_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetConstructorByReflection_m6715_ParameterInfos[] = 
{
	{"type", 0, 134219423, 0, &Type_t_0_0_0},
	{"argsType", 1, 134219424, 756, &TypeU5BU5D_t878_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetConstructorByReflection(System.Type,System.Type[])
extern const MethodInfo ReflectionUtils_GetConstructorByReflection_m6715_MethodInfo = 
{
	"GetConstructorByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetConstructorByReflection_m6715/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorDelegate_t1286_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetConstructorByReflection_m6715_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1600/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetGetMethod_m6716_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134219425, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethod(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetGetMethod_m6716_MethodInfo = 
{
	"GetGetMethod"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetMethod_m6716/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &GetDelegate_t1284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetGetMethod_m6716_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1601/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FieldInfo_t_0_0_0;
extern const Il2CppType FieldInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetGetMethod_m6717_ParameterInfos[] = 
{
	{"fieldInfo", 0, 134219426, 0, &FieldInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethod(System.Reflection.FieldInfo)
extern const MethodInfo ReflectionUtils_GetGetMethod_m6717_MethodInfo = 
{
	"GetGetMethod"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetMethod_m6717/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &GetDelegate_t1284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetGetMethod_m6717_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1602/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetGetMethodByReflection_m6718_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134219427, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethodByReflection(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetGetMethodByReflection_m6718_MethodInfo = 
{
	"GetGetMethodByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetMethodByReflection_m6718/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &GetDelegate_t1284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetGetMethodByReflection_m6718_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1603/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FieldInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetGetMethodByReflection_m6719_ParameterInfos[] = 
{
	{"fieldInfo", 0, 134219428, 0, &FieldInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethodByReflection(System.Reflection.FieldInfo)
extern const MethodInfo ReflectionUtils_GetGetMethodByReflection_m6719_MethodInfo = 
{
	"GetGetMethodByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetGetMethodByReflection_m6719/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &GetDelegate_t1284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetGetMethodByReflection_m6719_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1604/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetSetMethod_m6720_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134219429, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethod(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetSetMethod_m6720_MethodInfo = 
{
	"GetSetMethod"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetMethod_m6720/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &SetDelegate_t1285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetSetMethod_m6720_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1605/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FieldInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetSetMethod_m6721_ParameterInfos[] = 
{
	{"fieldInfo", 0, 134219430, 0, &FieldInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethod(System.Reflection.FieldInfo)
extern const MethodInfo ReflectionUtils_GetSetMethod_m6721_MethodInfo = 
{
	"GetSetMethod"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetMethod_m6721/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &SetDelegate_t1285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetSetMethod_m6721_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1606/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PropertyInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetSetMethodByReflection_m6722_ParameterInfos[] = 
{
	{"propertyInfo", 0, 134219431, 0, &PropertyInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethodByReflection(System.Reflection.PropertyInfo)
extern const MethodInfo ReflectionUtils_GetSetMethodByReflection_m6722_MethodInfo = 
{
	"GetSetMethodByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetMethodByReflection_m6722/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &SetDelegate_t1285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetSetMethodByReflection_m6722_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1607/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FieldInfo_t_0_0_0;
static const ParameterInfo ReflectionUtils_t1293_ReflectionUtils_GetSetMethodByReflection_m6723_ParameterInfos[] = 
{
	{"fieldInfo", 0, 134219432, 0, &FieldInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethodByReflection(System.Reflection.FieldInfo)
extern const MethodInfo ReflectionUtils_GetSetMethodByReflection_m6723_MethodInfo = 
{
	"GetSetMethodByReflection"/* name */
	, (methodPointerType)&ReflectionUtils_GetSetMethodByReflection_m6723/* method */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* declaring_type */
	, &SetDelegate_t1285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, ReflectionUtils_t1293_ReflectionUtils_GetSetMethodByReflection_m6723_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1608/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ReflectionUtils_t1293_MethodInfos[] =
{
	&ReflectionUtils__cctor_m6706_MethodInfo,
	&ReflectionUtils_GetConstructors_m6707_MethodInfo,
	&ReflectionUtils_GetConstructorInfo_m6708_MethodInfo,
	&ReflectionUtils_GetProperties_m6709_MethodInfo,
	&ReflectionUtils_GetFields_m6710_MethodInfo,
	&ReflectionUtils_GetGetterMethodInfo_m6711_MethodInfo,
	&ReflectionUtils_GetSetterMethodInfo_m6712_MethodInfo,
	&ReflectionUtils_GetContructor_m6713_MethodInfo,
	&ReflectionUtils_GetConstructorByReflection_m6714_MethodInfo,
	&ReflectionUtils_GetConstructorByReflection_m6715_MethodInfo,
	&ReflectionUtils_GetGetMethod_m6716_MethodInfo,
	&ReflectionUtils_GetGetMethod_m6717_MethodInfo,
	&ReflectionUtils_GetGetMethodByReflection_m6718_MethodInfo,
	&ReflectionUtils_GetGetMethodByReflection_m6719_MethodInfo,
	&ReflectionUtils_GetSetMethod_m6720_MethodInfo,
	&ReflectionUtils_GetSetMethod_m6721_MethodInfo,
	&ReflectionUtils_GetSetMethodByReflection_m6722_MethodInfo,
	&ReflectionUtils_GetSetMethodByReflection_m6723_MethodInfo,
	NULL
};
static const Il2CppType* ReflectionUtils_t1293_il2cpp_TypeInfo__nestedTypes[10] =
{
	&ThreadSafeDictionary_2_t1450_0_0_0,
	&GetDelegate_t1284_0_0_0,
	&SetDelegate_t1285_0_0_0,
	&ConstructorDelegate_t1286_0_0_0,
	&ThreadSafeDictionaryValueFactory_2_t1451_0_0_0,
	&U3CGetConstructorByReflectionU3Ec__AnonStorey1_t1288_0_0_0,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t1289_0_0_0,
	&U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t1290_0_0_0,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t1291_0_0_0,
	&U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t1292_0_0_0,
};
static const Il2CppMethodReference ReflectionUtils_t1293_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ReflectionUtils_t1293_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReflectionUtils_t1293_1_0_0;
struct ReflectionUtils_t1293;
const Il2CppTypeDefinitionMetadata ReflectionUtils_t1293_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ReflectionUtils_t1293_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ReflectionUtils_t1293_VTable/* vtableMethods */
	, ReflectionUtils_t1293_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1018/* fieldStart */

};
TypeInfo ReflectionUtils_t1293_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionUtils"/* name */
	, "SimpleJson.Reflection"/* namespaze */
	, ReflectionUtils_t1293_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReflectionUtils_t1293_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 753/* custom_attributes_cache */
	, &ReflectionUtils_t1293_0_0_0/* byval_arg */
	, &ReflectionUtils_t1293_1_0_0/* this_arg */
	, &ReflectionUtils_t1293_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionUtils_t1293)/* instance_size */
	, sizeof (ReflectionUtils_t1293)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ReflectionUtils_t1293_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 10/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
// Metadata Definition UnityEngine.WrapperlessIcall
extern TypeInfo WrapperlessIcall_t1294_il2cpp_TypeInfo;
// UnityEngine.WrapperlessIcall
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.WrapperlessIcall::.ctor()
extern const MethodInfo WrapperlessIcall__ctor_m6724_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WrapperlessIcall__ctor_m6724/* method */
	, &WrapperlessIcall_t1294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1654/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WrapperlessIcall_t1294_MethodInfos[] =
{
	&WrapperlessIcall__ctor_m6724_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m5260_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m5261_MethodInfo;
static const Il2CppMethodReference WrapperlessIcall_t1294_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool WrapperlessIcall_t1294_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t901_0_0_0;
static Il2CppInterfaceOffsetPair WrapperlessIcall_t1294_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WrapperlessIcall_t1294_0_0_0;
extern const Il2CppType WrapperlessIcall_t1294_1_0_0;
extern const Il2CppType Attribute_t138_0_0_0;
struct WrapperlessIcall_t1294;
const Il2CppTypeDefinitionMetadata WrapperlessIcall_t1294_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WrapperlessIcall_t1294_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, WrapperlessIcall_t1294_VTable/* vtableMethods */
	, WrapperlessIcall_t1294_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WrapperlessIcall_t1294_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WrapperlessIcall"/* name */
	, "UnityEngine"/* namespaze */
	, WrapperlessIcall_t1294_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WrapperlessIcall_t1294_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &WrapperlessIcall_t1294_0_0_0/* byval_arg */
	, &WrapperlessIcall_t1294_1_0_0/* this_arg */
	, &WrapperlessIcall_t1294_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WrapperlessIcall_t1294)/* instance_size */
	, sizeof (WrapperlessIcall_t1294)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
// Metadata Definition UnityEngine.IL2CPPStructAlignmentAttribute
extern TypeInfo IL2CPPStructAlignmentAttribute_t1295_il2cpp_TypeInfo;
// UnityEngine.IL2CPPStructAlignmentAttribute
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.IL2CPPStructAlignmentAttribute::.ctor()
extern const MethodInfo IL2CPPStructAlignmentAttribute__ctor_m6725_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IL2CPPStructAlignmentAttribute__ctor_m6725/* method */
	, &IL2CPPStructAlignmentAttribute_t1295_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1655/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IL2CPPStructAlignmentAttribute_t1295_MethodInfos[] =
{
	&IL2CPPStructAlignmentAttribute__ctor_m6725_MethodInfo,
	NULL
};
static const Il2CppMethodReference IL2CPPStructAlignmentAttribute_t1295_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool IL2CPPStructAlignmentAttribute_t1295_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair IL2CPPStructAlignmentAttribute_t1295_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IL2CPPStructAlignmentAttribute_t1295_0_0_0;
extern const Il2CppType IL2CPPStructAlignmentAttribute_t1295_1_0_0;
struct IL2CPPStructAlignmentAttribute_t1295;
const Il2CppTypeDefinitionMetadata IL2CPPStructAlignmentAttribute_t1295_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, IL2CPPStructAlignmentAttribute_t1295_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, IL2CPPStructAlignmentAttribute_t1295_VTable/* vtableMethods */
	, IL2CPPStructAlignmentAttribute_t1295_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1019/* fieldStart */

};
TypeInfo IL2CPPStructAlignmentAttribute_t1295_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IL2CPPStructAlignmentAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, IL2CPPStructAlignmentAttribute_t1295_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IL2CPPStructAlignmentAttribute_t1295_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 765/* custom_attributes_cache */
	, &IL2CPPStructAlignmentAttribute_t1295_0_0_0/* byval_arg */
	, &IL2CPPStructAlignmentAttribute_t1295_1_0_0/* this_arg */
	, &IL2CPPStructAlignmentAttribute_t1295_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IL2CPPStructAlignmentAttribute_t1295)/* instance_size */
	, sizeof (IL2CPPStructAlignmentAttribute_t1295)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngine.h"
// Metadata Definition UnityEngine.AttributeHelperEngine
extern TypeInfo AttributeHelperEngine_t1299_il2cpp_TypeInfo;
// UnityEngine.AttributeHelperEngine
#include "UnityEngine_UnityEngine_AttributeHelperEngineMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern const MethodInfo AttributeHelperEngine__cctor_m6726_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&AttributeHelperEngine__cctor_m6726/* method */
	, &AttributeHelperEngine_t1299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1656/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo AttributeHelperEngine_t1299_AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6727_ParameterInfos[] = 
{
	{"type", 0, 134219486, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern const MethodInfo AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6727_MethodInfo = 
{
	"GetParentTypeDisallowingMultipleInclusion"/* name */
	, (methodPointerType)&AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6727/* method */
	, &AttributeHelperEngine_t1299_il2cpp_TypeInfo/* declaring_type */
	, &Type_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, AttributeHelperEngine_t1299_AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6727_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1657/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo AttributeHelperEngine_t1299_AttributeHelperEngine_GetRequiredComponents_m6728_ParameterInfos[] = 
{
	{"klass", 0, 134219487, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern const MethodInfo AttributeHelperEngine_GetRequiredComponents_m6728_MethodInfo = 
{
	"GetRequiredComponents"/* name */
	, (methodPointerType)&AttributeHelperEngine_GetRequiredComponents_m6728/* method */
	, &AttributeHelperEngine_t1299_il2cpp_TypeInfo/* declaring_type */
	, &TypeU5BU5D_t878_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, AttributeHelperEngine_t1299_AttributeHelperEngine_GetRequiredComponents_m6728_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1658/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo AttributeHelperEngine_t1299_AttributeHelperEngine_CheckIsEditorScript_m6729_ParameterInfos[] = 
{
	{"klass", 0, 134219488, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern const MethodInfo AttributeHelperEngine_CheckIsEditorScript_m6729_MethodInfo = 
{
	"CheckIsEditorScript"/* name */
	, (methodPointerType)&AttributeHelperEngine_CheckIsEditorScript_m6729/* method */
	, &AttributeHelperEngine_t1299_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, AttributeHelperEngine_t1299_AttributeHelperEngine_CheckIsEditorScript_m6729_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1659/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AttributeHelperEngine_t1299_MethodInfos[] =
{
	&AttributeHelperEngine__cctor_m6726_MethodInfo,
	&AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m6727_MethodInfo,
	&AttributeHelperEngine_GetRequiredComponents_m6728_MethodInfo,
	&AttributeHelperEngine_CheckIsEditorScript_m6729_MethodInfo,
	NULL
};
static const Il2CppMethodReference AttributeHelperEngine_t1299_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool AttributeHelperEngine_t1299_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AttributeHelperEngine_t1299_0_0_0;
extern const Il2CppType AttributeHelperEngine_t1299_1_0_0;
struct AttributeHelperEngine_t1299;
const Il2CppTypeDefinitionMetadata AttributeHelperEngine_t1299_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AttributeHelperEngine_t1299_VTable/* vtableMethods */
	, AttributeHelperEngine_t1299_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1020/* fieldStart */

};
TypeInfo AttributeHelperEngine_t1299_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AttributeHelperEngine"/* name */
	, "UnityEngine"/* namespaze */
	, AttributeHelperEngine_t1299_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AttributeHelperEngine_t1299_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AttributeHelperEngine_t1299_0_0_0/* byval_arg */
	, &AttributeHelperEngine_t1299_1_0_0/* this_arg */
	, &AttributeHelperEngine_t1299_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AttributeHelperEngine_t1299)/* instance_size */
	, sizeof (AttributeHelperEngine_t1299)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(AttributeHelperEngine_t1299_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
// Metadata Definition UnityEngine.DisallowMultipleComponent
extern TypeInfo DisallowMultipleComponent_t501_il2cpp_TypeInfo;
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern const MethodInfo DisallowMultipleComponent__ctor_m2494_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DisallowMultipleComponent__ctor_m2494/* method */
	, &DisallowMultipleComponent_t501_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1660/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DisallowMultipleComponent_t501_MethodInfos[] =
{
	&DisallowMultipleComponent__ctor_m2494_MethodInfo,
	NULL
};
static const Il2CppMethodReference DisallowMultipleComponent_t501_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool DisallowMultipleComponent_t501_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DisallowMultipleComponent_t501_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DisallowMultipleComponent_t501_0_0_0;
extern const Il2CppType DisallowMultipleComponent_t501_1_0_0;
struct DisallowMultipleComponent_t501;
const Il2CppTypeDefinitionMetadata DisallowMultipleComponent_t501_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DisallowMultipleComponent_t501_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, DisallowMultipleComponent_t501_VTable/* vtableMethods */
	, DisallowMultipleComponent_t501_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo DisallowMultipleComponent_t501_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DisallowMultipleComponent"/* name */
	, "UnityEngine"/* namespaze */
	, DisallowMultipleComponent_t501_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &DisallowMultipleComponent_t501_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 766/* custom_attributes_cache */
	, &DisallowMultipleComponent_t501_0_0_0/* byval_arg */
	, &DisallowMultipleComponent_t501_1_0_0/* this_arg */
	, &DisallowMultipleComponent_t501_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DisallowMultipleComponent_t501)/* instance_size */
	, sizeof (DisallowMultipleComponent_t501)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// Metadata Definition UnityEngine.RequireComponent
extern TypeInfo RequireComponent_t163_il2cpp_TypeInfo;
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo RequireComponent_t163_RequireComponent__ctor_m513_ParameterInfos[] = 
{
	{"requiredComponent", 0, 134219489, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern const MethodInfo RequireComponent__ctor_m513_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RequireComponent__ctor_m513/* method */
	, &RequireComponent_t163_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, RequireComponent_t163_RequireComponent__ctor_m513_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1661/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RequireComponent_t163_MethodInfos[] =
{
	&RequireComponent__ctor_m513_MethodInfo,
	NULL
};
static const Il2CppMethodReference RequireComponent_t163_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool RequireComponent_t163_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RequireComponent_t163_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RequireComponent_t163_0_0_0;
extern const Il2CppType RequireComponent_t163_1_0_0;
struct RequireComponent_t163;
const Il2CppTypeDefinitionMetadata RequireComponent_t163_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RequireComponent_t163_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, RequireComponent_t163_VTable/* vtableMethods */
	, RequireComponent_t163_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1023/* fieldStart */

};
TypeInfo RequireComponent_t163_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RequireComponent"/* name */
	, "UnityEngine"/* namespaze */
	, RequireComponent_t163_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RequireComponent_t163_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 767/* custom_attributes_cache */
	, &RequireComponent_t163_0_0_0/* byval_arg */
	, &RequireComponent_t163_1_0_0/* this_arg */
	, &RequireComponent_t163_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RequireComponent_t163)/* instance_size */
	, sizeof (RequireComponent_t163)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// Metadata Definition UnityEngine.AddComponentMenu
extern TypeInfo AddComponentMenu_t491_il2cpp_TypeInfo;
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AddComponentMenu_t491_AddComponentMenu__ctor_m2455_ParameterInfos[] = 
{
	{"menuName", 0, 134219490, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern const MethodInfo AddComponentMenu__ctor_m2455_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AddComponentMenu__ctor_m2455/* method */
	, &AddComponentMenu_t491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, AddComponentMenu_t491_AddComponentMenu__ctor_m2455_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1662/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo AddComponentMenu_t491_AddComponentMenu__ctor_m2488_ParameterInfos[] = 
{
	{"menuName", 0, 134219491, 0, &String_t_0_0_0},
	{"order", 1, 134219492, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern const MethodInfo AddComponentMenu__ctor_m2488_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AddComponentMenu__ctor_m2488/* method */
	, &AddComponentMenu_t491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127/* invoker_method */
	, AddComponentMenu_t491_AddComponentMenu__ctor_m2488_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1663/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AddComponentMenu_t491_MethodInfos[] =
{
	&AddComponentMenu__ctor_m2455_MethodInfo,
	&AddComponentMenu__ctor_m2488_MethodInfo,
	NULL
};
static const Il2CppMethodReference AddComponentMenu_t491_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool AddComponentMenu_t491_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AddComponentMenu_t491_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AddComponentMenu_t491_0_0_0;
extern const Il2CppType AddComponentMenu_t491_1_0_0;
struct AddComponentMenu_t491;
const Il2CppTypeDefinitionMetadata AddComponentMenu_t491_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AddComponentMenu_t491_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, AddComponentMenu_t491_VTable/* vtableMethods */
	, AddComponentMenu_t491_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1026/* fieldStart */

};
TypeInfo AddComponentMenu_t491_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AddComponentMenu"/* name */
	, "UnityEngine"/* namespaze */
	, AddComponentMenu_t491_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AddComponentMenu_t491_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AddComponentMenu_t491_0_0_0/* byval_arg */
	, &AddComponentMenu_t491_1_0_0/* this_arg */
	, &AddComponentMenu_t491_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AddComponentMenu_t491)/* instance_size */
	, sizeof (AddComponentMenu_t491)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// Metadata Definition UnityEngine.ExecuteInEditMode
extern TypeInfo ExecuteInEditMode_t500_il2cpp_TypeInfo;
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern const MethodInfo ExecuteInEditMode__ctor_m2493_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExecuteInEditMode__ctor_m2493/* method */
	, &ExecuteInEditMode_t500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1664/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExecuteInEditMode_t500_MethodInfos[] =
{
	&ExecuteInEditMode__ctor_m2493_MethodInfo,
	NULL
};
static const Il2CppMethodReference ExecuteInEditMode_t500_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ExecuteInEditMode_t500_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ExecuteInEditMode_t500_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ExecuteInEditMode_t500_0_0_0;
extern const Il2CppType ExecuteInEditMode_t500_1_0_0;
struct ExecuteInEditMode_t500;
const Il2CppTypeDefinitionMetadata ExecuteInEditMode_t500_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExecuteInEditMode_t500_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, ExecuteInEditMode_t500_VTable/* vtableMethods */
	, ExecuteInEditMode_t500_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExecuteInEditMode_t500_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExecuteInEditMode"/* name */
	, "UnityEngine"/* namespaze */
	, ExecuteInEditMode_t500_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExecuteInEditMode_t500_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ExecuteInEditMode_t500_0_0_0/* byval_arg */
	, &ExecuteInEditMode_t500_1_0_0/* this_arg */
	, &ExecuteInEditMode_t500_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExecuteInEditMode_t500)/* instance_size */
	, sizeof (ExecuteInEditMode_t500)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspector.h"
// Metadata Definition UnityEngine.HideInInspector
extern TypeInfo HideInInspector_t900_il2cpp_TypeInfo;
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspectorMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.HideInInspector::.ctor()
extern const MethodInfo HideInInspector__ctor_m4695_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HideInInspector__ctor_m4695/* method */
	, &HideInInspector_t900_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1665/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HideInInspector_t900_MethodInfos[] =
{
	&HideInInspector__ctor_m4695_MethodInfo,
	NULL
};
static const Il2CppMethodReference HideInInspector_t900_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool HideInInspector_t900_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HideInInspector_t900_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HideInInspector_t900_0_0_0;
extern const Il2CppType HideInInspector_t900_1_0_0;
struct HideInInspector_t900;
const Il2CppTypeDefinitionMetadata HideInInspector_t900_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HideInInspector_t900_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, HideInInspector_t900_VTable/* vtableMethods */
	, HideInInspector_t900_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HideInInspector_t900_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HideInInspector"/* name */
	, "UnityEngine"/* namespaze */
	, HideInInspector_t900_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HideInInspector_t900_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HideInInspector_t900_0_0_0/* byval_arg */
	, &HideInInspector_t900_1_0_0/* this_arg */
	, &HideInInspector_t900_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HideInInspector_t900)/* instance_size */
	, sizeof (HideInInspector_t900)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.CastHelper`1
extern TypeInfo CastHelper_1_t1453_il2cpp_TypeInfo;
extern const Il2CppGenericContainer CastHelper_1_t1453_Il2CppGenericContainer;
extern TypeInfo CastHelper_1_t1453_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter CastHelper_1_t1453_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &CastHelper_1_t1453_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* CastHelper_1_t1453_Il2CppGenericParametersArray[1] = 
{
	&CastHelper_1_t1453_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer CastHelper_1_t1453_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&CastHelper_1_t1453_il2cpp_TypeInfo, 1, 0, CastHelper_1_t1453_Il2CppGenericParametersArray };
static const MethodInfo* CastHelper_1_t1453_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2567_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2568_MethodInfo;
extern const MethodInfo ValueType_ToString_m2571_MethodInfo;
static const Il2CppMethodReference CastHelper_1_t1453_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool CastHelper_1_t1453_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CastHelper_1_t1453_0_0_0;
extern const Il2CppType CastHelper_1_t1453_1_0_0;
extern const Il2CppType ValueType_t524_0_0_0;
const Il2CppTypeDefinitionMetadata CastHelper_1_t1453_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, CastHelper_1_t1453_VTable/* vtableMethods */
	, CastHelper_1_t1453_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1028/* fieldStart */

};
TypeInfo CastHelper_1_t1453_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t1453_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CastHelper_1_t1453_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CastHelper_1_t1453_0_0_0/* byval_arg */
	, &CastHelper_1_t1453_1_0_0/* this_arg */
	, &CastHelper_1_t1453_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &CastHelper_1_t1453_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutine.h"
// Metadata Definition UnityEngine.SetupCoroutine
extern TypeInfo SetupCoroutine_t1300_il2cpp_TypeInfo;
// UnityEngine.SetupCoroutine
#include "UnityEngine_UnityEngine_SetupCoroutineMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SetupCoroutine::.ctor()
extern const MethodInfo SetupCoroutine__ctor_m6730_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetupCoroutine__ctor_m6730/* method */
	, &SetupCoroutine_t1300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1666/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SetupCoroutine_t1300_SetupCoroutine_InvokeMember_m6731_ParameterInfos[] = 
{
	{"behaviour", 0, 134219493, 0, &Object_t_0_0_0},
	{"name", 1, 134219494, 0, &String_t_0_0_0},
	{"variable", 2, 134219495, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern const MethodInfo SetupCoroutine_InvokeMember_m6731_MethodInfo = 
{
	"InvokeMember"/* name */
	, (methodPointerType)&SetupCoroutine_InvokeMember_m6731/* method */
	, &SetupCoroutine_t1300_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, SetupCoroutine_t1300_SetupCoroutine_InvokeMember_m6731_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1667/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo SetupCoroutine_t1300_SetupCoroutine_InvokeStatic_m6732_ParameterInfos[] = 
{
	{"klass", 0, 134219496, 0, &Type_t_0_0_0},
	{"name", 1, 134219497, 0, &String_t_0_0_0},
	{"variable", 2, 134219498, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.SetupCoroutine::InvokeStatic(System.Type,System.String,System.Object)
extern const MethodInfo SetupCoroutine_InvokeStatic_m6732_MethodInfo = 
{
	"InvokeStatic"/* name */
	, (methodPointerType)&SetupCoroutine_InvokeStatic_m6732/* method */
	, &SetupCoroutine_t1300_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, SetupCoroutine_t1300_SetupCoroutine_InvokeStatic_m6732_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1668/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SetupCoroutine_t1300_MethodInfos[] =
{
	&SetupCoroutine__ctor_m6730_MethodInfo,
	&SetupCoroutine_InvokeMember_m6731_MethodInfo,
	&SetupCoroutine_InvokeStatic_m6732_MethodInfo,
	NULL
};
static const Il2CppMethodReference SetupCoroutine_t1300_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool SetupCoroutine_t1300_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SetupCoroutine_t1300_0_0_0;
extern const Il2CppType SetupCoroutine_t1300_1_0_0;
struct SetupCoroutine_t1300;
const Il2CppTypeDefinitionMetadata SetupCoroutine_t1300_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SetupCoroutine_t1300_VTable/* vtableMethods */
	, SetupCoroutine_t1300_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SetupCoroutine_t1300_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetupCoroutine"/* name */
	, "UnityEngine"/* namespaze */
	, SetupCoroutine_t1300_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetupCoroutine_t1300_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetupCoroutine_t1300_0_0_0/* byval_arg */
	, &SetupCoroutine_t1300_1_0_0/* this_arg */
	, &SetupCoroutine_t1300_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetupCoroutine_t1300)/* instance_size */
	, sizeof (SetupCoroutine_t1300)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttribute.h"
// Metadata Definition UnityEngine.WritableAttribute
extern TypeInfo WritableAttribute_t1301_il2cpp_TypeInfo;
// UnityEngine.WritableAttribute
#include "UnityEngine_UnityEngine_WritableAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.WritableAttribute::.ctor()
extern const MethodInfo WritableAttribute__ctor_m6733_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WritableAttribute__ctor_m6733/* method */
	, &WritableAttribute_t1301_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1669/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* WritableAttribute_t1301_MethodInfos[] =
{
	&WritableAttribute__ctor_m6733_MethodInfo,
	NULL
};
static const Il2CppMethodReference WritableAttribute_t1301_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool WritableAttribute_t1301_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair WritableAttribute_t1301_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType WritableAttribute_t1301_0_0_0;
extern const Il2CppType WritableAttribute_t1301_1_0_0;
struct WritableAttribute_t1301;
const Il2CppTypeDefinitionMetadata WritableAttribute_t1301_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, WritableAttribute_t1301_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, WritableAttribute_t1301_VTable/* vtableMethods */
	, WritableAttribute_t1301_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo WritableAttribute_t1301_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WritableAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, WritableAttribute_t1301_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &WritableAttribute_t1301_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 768/* custom_attributes_cache */
	, &WritableAttribute_t1301_0_0_0/* byval_arg */
	, &WritableAttribute_t1301_1_0_0/* this_arg */
	, &WritableAttribute_t1301_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WritableAttribute_t1301)/* instance_size */
	, sizeof (WritableAttribute_t1301)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly.h"
// Metadata Definition UnityEngine.AssemblyIsEditorAssembly
extern TypeInfo AssemblyIsEditorAssembly_t1302_il2cpp_TypeInfo;
// UnityEngine.AssemblyIsEditorAssembly
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssemblyMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern const MethodInfo AssemblyIsEditorAssembly__ctor_m6734_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AssemblyIsEditorAssembly__ctor_m6734/* method */
	, &AssemblyIsEditorAssembly_t1302_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1670/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AssemblyIsEditorAssembly_t1302_MethodInfos[] =
{
	&AssemblyIsEditorAssembly__ctor_m6734_MethodInfo,
	NULL
};
static const Il2CppMethodReference AssemblyIsEditorAssembly_t1302_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool AssemblyIsEditorAssembly_t1302_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AssemblyIsEditorAssembly_t1302_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AssemblyIsEditorAssembly_t1302_0_0_0;
extern const Il2CppType AssemblyIsEditorAssembly_t1302_1_0_0;
struct AssemblyIsEditorAssembly_t1302;
const Il2CppTypeDefinitionMetadata AssemblyIsEditorAssembly_t1302_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AssemblyIsEditorAssembly_t1302_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, AssemblyIsEditorAssembly_t1302_VTable/* vtableMethods */
	, AssemblyIsEditorAssembly_t1302_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo AssemblyIsEditorAssembly_t1302_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssemblyIsEditorAssembly"/* name */
	, "UnityEngine"/* namespaze */
	, AssemblyIsEditorAssembly_t1302_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &AssemblyIsEditorAssembly_t1302_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 769/* custom_attributes_cache */
	, &AssemblyIsEditorAssembly_t1302_0_0_0/* byval_arg */
	, &AssemblyIsEditorAssembly_t1302_1_0_0/* this_arg */
	, &AssemblyIsEditorAssembly_t1302_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AssemblyIsEditorAssembly_t1302)/* instance_size */
	, sizeof (AssemblyIsEditorAssembly_t1302)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
extern TypeInfo GcUserProfileData_t1303_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserProMethodDeclarations.h"
extern const Il2CppType UserProfile_t1316_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
extern const MethodInfo GcUserProfileData_ToUserProfile_m6735_MethodInfo = 
{
	"ToUserProfile"/* name */
	, (methodPointerType)&GcUserProfileData_ToUserProfile_m6735/* method */
	, &GcUserProfileData_t1303_il2cpp_TypeInfo/* declaring_type */
	, &UserProfile_t1316_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1671/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UserProfileU5BU5D_t1150_1_0_0;
extern const Il2CppType UserProfileU5BU5D_t1150_1_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo GcUserProfileData_t1303_GcUserProfileData_AddToArray_m6736_ParameterInfos[] = 
{
	{"array", 0, 134219499, 0, &UserProfileU5BU5D_t1150_1_0_0},
	{"number", 1, 134219500, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_UserProfileU5BU5DU26_t1470_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
extern const MethodInfo GcUserProfileData_AddToArray_m6736_MethodInfo = 
{
	"AddToArray"/* name */
	, (methodPointerType)&GcUserProfileData_AddToArray_m6736/* method */
	, &GcUserProfileData_t1303_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_UserProfileU5BU5DU26_t1470_Int32_t127/* invoker_method */
	, GcUserProfileData_t1303_GcUserProfileData_AddToArray_m6736_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1672/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcUserProfileData_t1303_MethodInfos[] =
{
	&GcUserProfileData_ToUserProfile_m6735_MethodInfo,
	&GcUserProfileData_AddToArray_m6736_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcUserProfileData_t1303_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool GcUserProfileData_t1303_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcUserProfileData_t1303_0_0_0;
extern const Il2CppType GcUserProfileData_t1303_1_0_0;
const Il2CppTypeDefinitionMetadata GcUserProfileData_t1303_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, GcUserProfileData_t1303_VTable/* vtableMethods */
	, GcUserProfileData_t1303_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1030/* fieldStart */

};
TypeInfo GcUserProfileData_t1303_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcUserProfileData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcUserProfileData_t1303_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcUserProfileData_t1303_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcUserProfileData_t1303_0_0_0/* byval_arg */
	, &GcUserProfileData_t1303_1_0_0/* this_arg */
	, &GcUserProfileData_t1303_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcUserProfileData_t1303)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcUserProfileData_t1303)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
extern TypeInfo GcAchievementDescriptionData_t1304_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieveMethodDeclarations.h"
extern const Il2CppType AchievementDescription_t1318_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
extern const MethodInfo GcAchievementDescriptionData_ToAchievementDescription_m6737_MethodInfo = 
{
	"ToAchievementDescription"/* name */
	, (methodPointerType)&GcAchievementDescriptionData_ToAchievementDescription_m6737/* method */
	, &GcAchievementDescriptionData_t1304_il2cpp_TypeInfo/* declaring_type */
	, &AchievementDescription_t1318_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1673/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcAchievementDescriptionData_t1304_MethodInfos[] =
{
	&GcAchievementDescriptionData_ToAchievementDescription_m6737_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcAchievementDescriptionData_t1304_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool GcAchievementDescriptionData_t1304_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcAchievementDescriptionData_t1304_0_0_0;
extern const Il2CppType GcAchievementDescriptionData_t1304_1_0_0;
const Il2CppTypeDefinitionMetadata GcAchievementDescriptionData_t1304_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, GcAchievementDescriptionData_t1304_VTable/* vtableMethods */
	, GcAchievementDescriptionData_t1304_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1034/* fieldStart */

};
TypeInfo GcAchievementDescriptionData_t1304_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementDescriptionData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcAchievementDescriptionData_t1304_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcAchievementDescriptionData_t1304_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcAchievementDescriptionData_t1304_0_0_0/* byval_arg */
	, &GcAchievementDescriptionData_t1304_1_0_0/* this_arg */
	, &GcAchievementDescriptionData_t1304_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GcAchievementDescriptionData_t1304)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcAchievementDescriptionData_t1304)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
extern TypeInfo GcAchievementData_t1305_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0MethodDeclarations.h"
extern const Il2CppType Achievement_t1317_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern const MethodInfo GcAchievementData_ToAchievement_m6738_MethodInfo = 
{
	"ToAchievement"/* name */
	, (methodPointerType)&GcAchievementData_ToAchievement_m6738/* method */
	, &GcAchievementData_t1305_il2cpp_TypeInfo/* declaring_type */
	, &Achievement_t1317_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1674/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcAchievementData_t1305_MethodInfos[] =
{
	&GcAchievementData_ToAchievement_m6738_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcAchievementData_t1305_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool GcAchievementData_t1305_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcAchievementData_t1305_0_0_0;
extern const Il2CppType GcAchievementData_t1305_1_0_0;
const Il2CppTypeDefinitionMetadata GcAchievementData_t1305_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, GcAchievementData_t1305_VTable/* vtableMethods */
	, GcAchievementData_t1305_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1041/* fieldStart */

};
TypeInfo GcAchievementData_t1305_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcAchievementData_t1305_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcAchievementData_t1305_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcAchievementData_t1305_0_0_0/* byval_arg */
	, &GcAchievementData_t1305_1_0_0/* this_arg */
	, &GcAchievementData_t1305_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)GcAchievementData_t1305_marshal/* marshal_to_native_func */
	, (methodPointerType)GcAchievementData_t1305_marshal_back/* marshal_from_native_func */
	, (methodPointerType)GcAchievementData_t1305_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (GcAchievementData_t1305)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcAchievementData_t1305)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GcAchievementData_t1305_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcScoreData
extern TypeInfo GcScoreData_t1306_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDaMethodDeclarations.h"
extern const Il2CppType Score_t1319_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
extern const MethodInfo GcScoreData_ToScore_m6739_MethodInfo = 
{
	"ToScore"/* name */
	, (methodPointerType)&GcScoreData_ToScore_m6739/* method */
	, &GcScoreData_t1306_il2cpp_TypeInfo/* declaring_type */
	, &Score_t1319_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1675/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GcScoreData_t1306_MethodInfos[] =
{
	&GcScoreData_ToScore_m6739_MethodInfo,
	NULL
};
static const Il2CppMethodReference GcScoreData_t1306_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool GcScoreData_t1306_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GcScoreData_t1306_0_0_0;
extern const Il2CppType GcScoreData_t1306_1_0_0;
const Il2CppTypeDefinitionMetadata GcScoreData_t1306_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, GcScoreData_t1306_VTable/* vtableMethods */
	, GcScoreData_t1306_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1046/* fieldStart */

};
TypeInfo GcScoreData_t1306_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcScoreData"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcScoreData_t1306_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GcScoreData_t1306_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GcScoreData_t1306_0_0_0/* byval_arg */
	, &GcScoreData_t1306_1_0_0/* this_arg */
	, &GcScoreData_t1306_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)GcScoreData_t1306_marshal/* marshal_to_native_func */
	, (methodPointerType)GcScoreData_t1306_marshal_back/* marshal_from_native_func */
	, (methodPointerType)GcScoreData_t1306_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (GcScoreData_t1306)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (GcScoreData_t1306)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(GcScoreData_t1306_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Resolution
#include "UnityEngine_UnityEngine_Resolution.h"
// Metadata Definition UnityEngine.Resolution
extern TypeInfo Resolution_t1307_il2cpp_TypeInfo;
// UnityEngine.Resolution
#include "UnityEngine_UnityEngine_ResolutionMethodDeclarations.h"
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Resolution::get_width()
extern const MethodInfo Resolution_get_width_m6740_MethodInfo = 
{
	"get_width"/* name */
	, (methodPointerType)&Resolution_get_width_m6740/* method */
	, &Resolution_t1307_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1676/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Resolution_t1307_Resolution_set_width_m6741_ParameterInfos[] = 
{
	{"value", 0, 134219501, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Resolution::set_width(System.Int32)
extern const MethodInfo Resolution_set_width_m6741_MethodInfo = 
{
	"set_width"/* name */
	, (methodPointerType)&Resolution_set_width_m6741/* method */
	, &Resolution_t1307_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Resolution_t1307_Resolution_set_width_m6741_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1677/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Resolution::get_height()
extern const MethodInfo Resolution_get_height_m6742_MethodInfo = 
{
	"get_height"/* name */
	, (methodPointerType)&Resolution_get_height_m6742/* method */
	, &Resolution_t1307_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1678/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Resolution_t1307_Resolution_set_height_m6743_ParameterInfos[] = 
{
	{"value", 0, 134219502, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Resolution::set_height(System.Int32)
extern const MethodInfo Resolution_set_height_m6743_MethodInfo = 
{
	"set_height"/* name */
	, (methodPointerType)&Resolution_set_height_m6743/* method */
	, &Resolution_t1307_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Resolution_t1307_Resolution_set_height_m6743_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1679/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Resolution::get_refreshRate()
extern const MethodInfo Resolution_get_refreshRate_m6744_MethodInfo = 
{
	"get_refreshRate"/* name */
	, (methodPointerType)&Resolution_get_refreshRate_m6744/* method */
	, &Resolution_t1307_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1680/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Resolution_t1307_Resolution_set_refreshRate_m6745_ParameterInfos[] = 
{
	{"value", 0, 134219503, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Resolution::set_refreshRate(System.Int32)
extern const MethodInfo Resolution_set_refreshRate_m6745_MethodInfo = 
{
	"set_refreshRate"/* name */
	, (methodPointerType)&Resolution_set_refreshRate_m6745/* method */
	, &Resolution_t1307_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Resolution_t1307_Resolution_set_refreshRate_m6745_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1681/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Resolution::ToString()
extern const MethodInfo Resolution_ToString_m6746_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Resolution_ToString_m6746/* method */
	, &Resolution_t1307_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1682/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Resolution_t1307_MethodInfos[] =
{
	&Resolution_get_width_m6740_MethodInfo,
	&Resolution_set_width_m6741_MethodInfo,
	&Resolution_get_height_m6742_MethodInfo,
	&Resolution_set_height_m6743_MethodInfo,
	&Resolution_get_refreshRate_m6744_MethodInfo,
	&Resolution_set_refreshRate_m6745_MethodInfo,
	&Resolution_ToString_m6746_MethodInfo,
	NULL
};
extern const MethodInfo Resolution_get_width_m6740_MethodInfo;
extern const MethodInfo Resolution_set_width_m6741_MethodInfo;
static const PropertyInfo Resolution_t1307____width_PropertyInfo = 
{
	&Resolution_t1307_il2cpp_TypeInfo/* parent */
	, "width"/* name */
	, &Resolution_get_width_m6740_MethodInfo/* get */
	, &Resolution_set_width_m6741_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Resolution_get_height_m6742_MethodInfo;
extern const MethodInfo Resolution_set_height_m6743_MethodInfo;
static const PropertyInfo Resolution_t1307____height_PropertyInfo = 
{
	&Resolution_t1307_il2cpp_TypeInfo/* parent */
	, "height"/* name */
	, &Resolution_get_height_m6742_MethodInfo/* get */
	, &Resolution_set_height_m6743_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Resolution_get_refreshRate_m6744_MethodInfo;
extern const MethodInfo Resolution_set_refreshRate_m6745_MethodInfo;
static const PropertyInfo Resolution_t1307____refreshRate_PropertyInfo = 
{
	&Resolution_t1307_il2cpp_TypeInfo/* parent */
	, "refreshRate"/* name */
	, &Resolution_get_refreshRate_m6744_MethodInfo/* get */
	, &Resolution_set_refreshRate_m6745_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Resolution_t1307_PropertyInfos[] =
{
	&Resolution_t1307____width_PropertyInfo,
	&Resolution_t1307____height_PropertyInfo,
	&Resolution_t1307____refreshRate_PropertyInfo,
	NULL
};
extern const MethodInfo Resolution_ToString_m6746_MethodInfo;
static const Il2CppMethodReference Resolution_t1307_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&Resolution_ToString_m6746_MethodInfo,
};
static bool Resolution_t1307_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Resolution_t1307_0_0_0;
extern const Il2CppType Resolution_t1307_1_0_0;
const Il2CppTypeDefinitionMetadata Resolution_t1307_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, Resolution_t1307_VTable/* vtableMethods */
	, Resolution_t1307_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1053/* fieldStart */

};
TypeInfo Resolution_t1307_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Resolution"/* name */
	, "UnityEngine"/* namespaze */
	, Resolution_t1307_MethodInfos/* methods */
	, Resolution_t1307_PropertyInfos/* properties */
	, NULL/* events */
	, &Resolution_t1307_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Resolution_t1307_0_0_0/* byval_arg */
	, &Resolution_t1307_1_0_0/* this_arg */
	, &Resolution_t1307_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Resolution_t1307)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Resolution_t1307)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Resolution_t1307 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBuffer.h"
// Metadata Definition UnityEngine.RenderBuffer
extern TypeInfo RenderBuffer_t1308_il2cpp_TypeInfo;
// UnityEngine.RenderBuffer
#include "UnityEngine_UnityEngine_RenderBufferMethodDeclarations.h"
static const MethodInfo* RenderBuffer_t1308_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RenderBuffer_t1308_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool RenderBuffer_t1308_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderBuffer_t1308_0_0_0;
extern const Il2CppType RenderBuffer_t1308_1_0_0;
const Il2CppTypeDefinitionMetadata RenderBuffer_t1308_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, RenderBuffer_t1308_VTable/* vtableMethods */
	, RenderBuffer_t1308_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1056/* fieldStart */

};
TypeInfo RenderBuffer_t1308_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderBuffer"/* name */
	, "UnityEngine"/* namespaze */
	, RenderBuffer_t1308_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RenderBuffer_t1308_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderBuffer_t1308_0_0_0/* byval_arg */
	, &RenderBuffer_t1308_1_0_0/* this_arg */
	, &RenderBuffer_t1308_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderBuffer_t1308)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderBuffer_t1308)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(RenderBuffer_t1308 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
// Metadata Definition UnityEngine.CameraClearFlags
extern TypeInfo CameraClearFlags_t1309_il2cpp_TypeInfo;
// UnityEngine.CameraClearFlags
#include "UnityEngine_UnityEngine_CameraClearFlagsMethodDeclarations.h"
static const MethodInfo* CameraClearFlags_t1309_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m514_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m516_MethodInfo;
extern const MethodInfo Enum_ToString_m517_MethodInfo;
extern const MethodInfo Enum_ToString_m518_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m519_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m520_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m521_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m522_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m523_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m524_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m525_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m526_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m527_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m528_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m529_MethodInfo;
extern const MethodInfo Enum_ToString_m530_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m531_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m532_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m533_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m534_MethodInfo;
extern const MethodInfo Enum_CompareTo_m535_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m536_MethodInfo;
static const Il2CppMethodReference CameraClearFlags_t1309_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool CameraClearFlags_t1309_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t164_0_0_0;
extern const Il2CppType IConvertible_t165_0_0_0;
extern const Il2CppType IComparable_t166_0_0_0;
static Il2CppInterfaceOffsetPair CameraClearFlags_t1309_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CameraClearFlags_t1309_0_0_0;
extern const Il2CppType CameraClearFlags_t1309_1_0_0;
extern const Il2CppType Enum_t167_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t127_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata CameraClearFlags_t1309_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, CameraClearFlags_t1309_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, CameraClearFlags_t1309_VTable/* vtableMethods */
	, CameraClearFlags_t1309_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1058/* fieldStart */

};
TypeInfo CameraClearFlags_t1309_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CameraClearFlags"/* name */
	, "UnityEngine"/* namespaze */
	, CameraClearFlags_t1309_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CameraClearFlags_t1309_0_0_0/* byval_arg */
	, &CameraClearFlags_t1309_1_0_0/* this_arg */
	, &CameraClearFlags_t1309_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CameraClearFlags_t1309)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (CameraClearFlags_t1309)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// Metadata Definition UnityEngine.ScreenOrientation
extern TypeInfo ScreenOrientation_t887_il2cpp_TypeInfo;
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientationMethodDeclarations.h"
static const MethodInfo* ScreenOrientation_t887_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ScreenOrientation_t887_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool ScreenOrientation_t887_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScreenOrientation_t887_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ScreenOrientation_t887_0_0_0;
extern const Il2CppType ScreenOrientation_t887_1_0_0;
const Il2CppTypeDefinitionMetadata ScreenOrientation_t887_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScreenOrientation_t887_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, ScreenOrientation_t887_VTable/* vtableMethods */
	, ScreenOrientation_t887_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1064/* fieldStart */

};
TypeInfo ScreenOrientation_t887_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScreenOrientation"/* name */
	, "UnityEngine"/* namespaze */
	, ScreenOrientation_t887_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScreenOrientation_t887_0_0_0/* byval_arg */
	, &ScreenOrientation_t887_1_0_0/* this_arg */
	, &ScreenOrientation_t887_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScreenOrientation_t887)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScreenOrientation_t887)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterMode.h"
// Metadata Definition UnityEngine.FilterMode
extern TypeInfo FilterMode_t1310_il2cpp_TypeInfo;
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterModeMethodDeclarations.h"
static const MethodInfo* FilterMode_t1310_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FilterMode_t1310_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool FilterMode_t1310_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FilterMode_t1310_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FilterMode_t1310_0_0_0;
extern const Il2CppType FilterMode_t1310_1_0_0;
const Il2CppTypeDefinitionMetadata FilterMode_t1310_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FilterMode_t1310_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, FilterMode_t1310_VTable/* vtableMethods */
	, FilterMode_t1310_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1072/* fieldStart */

};
TypeInfo FilterMode_t1310_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FilterMode"/* name */
	, "UnityEngine"/* namespaze */
	, FilterMode_t1310_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FilterMode_t1310_0_0_0/* byval_arg */
	, &FilterMode_t1310_1_0_0/* this_arg */
	, &FilterMode_t1310_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FilterMode_t1310)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FilterMode_t1310)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextureWrapMode
#include "UnityEngine_UnityEngine_TextureWrapMode.h"
// Metadata Definition UnityEngine.TextureWrapMode
extern TypeInfo TextureWrapMode_t1311_il2cpp_TypeInfo;
// UnityEngine.TextureWrapMode
#include "UnityEngine_UnityEngine_TextureWrapModeMethodDeclarations.h"
static const MethodInfo* TextureWrapMode_t1311_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TextureWrapMode_t1311_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool TextureWrapMode_t1311_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextureWrapMode_t1311_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextureWrapMode_t1311_0_0_0;
extern const Il2CppType TextureWrapMode_t1311_1_0_0;
const Il2CppTypeDefinitionMetadata TextureWrapMode_t1311_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextureWrapMode_t1311_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, TextureWrapMode_t1311_VTable/* vtableMethods */
	, TextureWrapMode_t1311_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1076/* fieldStart */

};
TypeInfo TextureWrapMode_t1311_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextureWrapMode"/* name */
	, "UnityEngine"/* namespaze */
	, TextureWrapMode_t1311_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextureWrapMode_t1311_0_0_0/* byval_arg */
	, &TextureWrapMode_t1311_1_0_0/* this_arg */
	, &TextureWrapMode_t1311_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextureWrapMode_t1311)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextureWrapMode_t1311)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// Metadata Definition UnityEngine.TextureFormat
extern TypeInfo TextureFormat_t908_il2cpp_TypeInfo;
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormatMethodDeclarations.h"
static const MethodInfo* TextureFormat_t908_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TextureFormat_t908_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool TextureFormat_t908_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextureFormat_t908_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextureFormat_t908_0_0_0;
extern const Il2CppType TextureFormat_t908_1_0_0;
const Il2CppTypeDefinitionMetadata TextureFormat_t908_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextureFormat_t908_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, TextureFormat_t908_VTable/* vtableMethods */
	, TextureFormat_t908_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1079/* fieldStart */

};
TypeInfo TextureFormat_t908_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextureFormat"/* name */
	, "UnityEngine"/* namespaze */
	, TextureFormat_t908_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextureFormat_t908_0_0_0/* byval_arg */
	, &TextureFormat_t908_1_0_0/* this_arg */
	, &TextureFormat_t908_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextureFormat_t908)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextureFormat_t908)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 45/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormat.h"
// Metadata Definition UnityEngine.RenderTextureFormat
extern TypeInfo RenderTextureFormat_t1312_il2cpp_TypeInfo;
// UnityEngine.RenderTextureFormat
#include "UnityEngine_UnityEngine_RenderTextureFormatMethodDeclarations.h"
static const MethodInfo* RenderTextureFormat_t1312_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RenderTextureFormat_t1312_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool RenderTextureFormat_t1312_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RenderTextureFormat_t1312_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderTextureFormat_t1312_0_0_0;
extern const Il2CppType RenderTextureFormat_t1312_1_0_0;
const Il2CppTypeDefinitionMetadata RenderTextureFormat_t1312_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RenderTextureFormat_t1312_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, RenderTextureFormat_t1312_VTable/* vtableMethods */
	, RenderTextureFormat_t1312_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1124/* fieldStart */

};
TypeInfo RenderTextureFormat_t1312_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderTextureFormat"/* name */
	, "UnityEngine"/* namespaze */
	, RenderTextureFormat_t1312_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderTextureFormat_t1312_0_0_0/* byval_arg */
	, &RenderTextureFormat_t1312_1_0_0/* this_arg */
	, &RenderTextureFormat_t1312_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderTextureFormat_t1312)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderTextureFormat_t1312)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 20/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.RenderTextureReadWrite
#include "UnityEngine_UnityEngine_RenderTextureReadWrite.h"
// Metadata Definition UnityEngine.RenderTextureReadWrite
extern TypeInfo RenderTextureReadWrite_t1313_il2cpp_TypeInfo;
// UnityEngine.RenderTextureReadWrite
#include "UnityEngine_UnityEngine_RenderTextureReadWriteMethodDeclarations.h"
static const MethodInfo* RenderTextureReadWrite_t1313_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference RenderTextureReadWrite_t1313_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool RenderTextureReadWrite_t1313_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RenderTextureReadWrite_t1313_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RenderTextureReadWrite_t1313_0_0_0;
extern const Il2CppType RenderTextureReadWrite_t1313_1_0_0;
const Il2CppTypeDefinitionMetadata RenderTextureReadWrite_t1313_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RenderTextureReadWrite_t1313_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, RenderTextureReadWrite_t1313_VTable/* vtableMethods */
	, RenderTextureReadWrite_t1313_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1144/* fieldStart */

};
TypeInfo RenderTextureReadWrite_t1313_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderTextureReadWrite"/* name */
	, "UnityEngine"/* namespaze */
	, RenderTextureReadWrite_t1313_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &RenderTextureReadWrite_t1313_0_0_0/* byval_arg */
	, &RenderTextureReadWrite_t1313_1_0_0/* this_arg */
	, &RenderTextureReadWrite_t1313_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RenderTextureReadWrite_t1313)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (RenderTextureReadWrite_t1313)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfo.h"
// Metadata Definition UnityEngine.Rendering.ReflectionProbeBlendInfo
extern TypeInfo ReflectionProbeBlendInfo_t1314_il2cpp_TypeInfo;
// UnityEngine.Rendering.ReflectionProbeBlendInfo
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeBlendInfoMethodDeclarations.h"
static const MethodInfo* ReflectionProbeBlendInfo_t1314_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ReflectionProbeBlendInfo_t1314_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool ReflectionProbeBlendInfo_t1314_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ReflectionProbeBlendInfo_t1314_0_0_0;
extern const Il2CppType ReflectionProbeBlendInfo_t1314_1_0_0;
const Il2CppTypeDefinitionMetadata ReflectionProbeBlendInfo_t1314_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, ReflectionProbeBlendInfo_t1314_VTable/* vtableMethods */
	, ReflectionProbeBlendInfo_t1314_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1148/* fieldStart */

};
TypeInfo ReflectionProbeBlendInfo_t1314_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionProbeBlendInfo"/* name */
	, "UnityEngine.Rendering"/* namespaze */
	, ReflectionProbeBlendInfo_t1314_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ReflectionProbeBlendInfo_t1314_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ReflectionProbeBlendInfo_t1314_0_0_0/* byval_arg */
	, &ReflectionProbeBlendInfo_t1314_1_0_0/* this_arg */
	, &ReflectionProbeBlendInfo_t1314_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReflectionProbeBlendInfo_t1314)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ReflectionProbeBlendInfo_t1314)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUser.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.LocalUser
extern TypeInfo LocalUser_t1151_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.LocalUser
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LocalUserMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
extern const MethodInfo LocalUser__ctor_m6747_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LocalUser__ctor_m6747/* method */
	, &LocalUser_t1151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1683/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IUserProfileU5BU5D_t1315_0_0_0;
extern const Il2CppType IUserProfileU5BU5D_t1315_0_0_0;
static const ParameterInfo LocalUser_t1151_LocalUser_SetFriends_m6748_ParameterInfos[] = 
{
	{"friends", 0, 134219504, 0, &IUserProfileU5BU5D_t1315_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
extern const MethodInfo LocalUser_SetFriends_m6748_MethodInfo = 
{
	"SetFriends"/* name */
	, (methodPointerType)&LocalUser_SetFriends_m6748/* method */
	, &LocalUser_t1151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LocalUser_t1151_LocalUser_SetFriends_m6748_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1684/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo LocalUser_t1151_LocalUser_SetAuthenticated_m6749_ParameterInfos[] = 
{
	{"value", 0, 134219505, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
extern const MethodInfo LocalUser_SetAuthenticated_m6749_MethodInfo = 
{
	"SetAuthenticated"/* name */
	, (methodPointerType)&LocalUser_SetAuthenticated_m6749/* method */
	, &LocalUser_t1151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, LocalUser_t1151_LocalUser_SetAuthenticated_m6749_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1685/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo LocalUser_t1151_LocalUser_SetUnderage_m6750_ParameterInfos[] = 
{
	{"value", 0, 134219506, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
extern const MethodInfo LocalUser_SetUnderage_m6750_MethodInfo = 
{
	"SetUnderage"/* name */
	, (methodPointerType)&LocalUser_SetUnderage_m6750/* method */
	, &LocalUser_t1151_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, LocalUser_t1151_LocalUser_SetUnderage_m6750_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1686/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
extern const MethodInfo LocalUser_get_authenticated_m6751_MethodInfo = 
{
	"get_authenticated"/* name */
	, (methodPointerType)&LocalUser_get_authenticated_m6751/* method */
	, &LocalUser_t1151_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1687/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LocalUser_t1151_MethodInfos[] =
{
	&LocalUser__ctor_m6747_MethodInfo,
	&LocalUser_SetFriends_m6748_MethodInfo,
	&LocalUser_SetAuthenticated_m6749_MethodInfo,
	&LocalUser_SetUnderage_m6750_MethodInfo,
	&LocalUser_get_authenticated_m6751_MethodInfo,
	NULL
};
extern const MethodInfo LocalUser_get_authenticated_m6751_MethodInfo;
static const PropertyInfo LocalUser_t1151____authenticated_PropertyInfo = 
{
	&LocalUser_t1151_il2cpp_TypeInfo/* parent */
	, "authenticated"/* name */
	, &LocalUser_get_authenticated_m6751_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LocalUser_t1151_PropertyInfos[] =
{
	&LocalUser_t1151____authenticated_PropertyInfo,
	NULL
};
extern const MethodInfo UserProfile_ToString_m6754_MethodInfo;
extern const MethodInfo UserProfile_get_userName_m6758_MethodInfo;
extern const MethodInfo UserProfile_get_id_m6759_MethodInfo;
extern const MethodInfo UserProfile_get_isFriend_m6760_MethodInfo;
extern const MethodInfo UserProfile_get_state_m6761_MethodInfo;
static const Il2CppMethodReference LocalUser_t1151_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&UserProfile_ToString_m6754_MethodInfo,
	&UserProfile_get_userName_m6758_MethodInfo,
	&UserProfile_get_id_m6759_MethodInfo,
	&UserProfile_get_isFriend_m6760_MethodInfo,
	&UserProfile_get_state_m6761_MethodInfo,
	&LocalUser_get_authenticated_m6751_MethodInfo,
};
static bool LocalUser_t1151_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILocalUser_t1354_0_0_0;
extern const Il2CppType IUserProfile_t1455_0_0_0;
static const Il2CppType* LocalUser_t1151_InterfacesTypeInfos[] = 
{
	&ILocalUser_t1354_0_0_0,
	&IUserProfile_t1455_0_0_0,
};
static Il2CppInterfaceOffsetPair LocalUser_t1151_InterfacesOffsets[] = 
{
	{ &IUserProfile_t1455_0_0_0, 4},
	{ &ILocalUser_t1354_0_0_0, 8},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType LocalUser_t1151_0_0_0;
extern const Il2CppType LocalUser_t1151_1_0_0;
struct LocalUser_t1151;
const Il2CppTypeDefinitionMetadata LocalUser_t1151_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LocalUser_t1151_InterfacesTypeInfos/* implementedInterfaces */
	, LocalUser_t1151_InterfacesOffsets/* interfaceOffsets */
	, &UserProfile_t1316_0_0_0/* parent */
	, LocalUser_t1151_VTable/* vtableMethods */
	, LocalUser_t1151_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1150/* fieldStart */

};
TypeInfo LocalUser_t1151_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LocalUser"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, LocalUser_t1151_MethodInfos/* methods */
	, LocalUser_t1151_PropertyInfos/* properties */
	, NULL/* events */
	, &LocalUser_t1151_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LocalUser_t1151_0_0_0/* byval_arg */
	, &LocalUser_t1151_1_0_0/* this_arg */
	, &LocalUser_t1151_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LocalUser_t1151)/* instance_size */
	, sizeof (LocalUser_t1151)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 9/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfile.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.UserProfile
extern TypeInfo UserProfile_t1316_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.UserProfile
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_UserProfileMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor()
extern const MethodInfo UserProfile__ctor_m6752_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserProfile__ctor_m6752/* method */
	, &UserProfile_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1688/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType UserState_t1326_0_0_0;
extern const Il2CppType UserState_t1326_0_0_0;
extern const Il2CppType Texture2D_t270_0_0_0;
extern const Il2CppType Texture2D_t270_0_0_0;
static const ParameterInfo UserProfile_t1316_UserProfile__ctor_m6753_ParameterInfos[] = 
{
	{"name", 0, 134219507, 0, &String_t_0_0_0},
	{"id", 1, 134219508, 0, &String_t_0_0_0},
	{"friend", 2, 134219509, 0, &Boolean_t169_0_0_0},
	{"state", 3, 134219510, 0, &UserState_t1326_0_0_0},
	{"image", 4, 134219511, 0, &Texture2D_t270_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_SByte_t170_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::.ctor(System.String,System.String,System.Boolean,UnityEngine.SocialPlatforms.UserState,UnityEngine.Texture2D)
extern const MethodInfo UserProfile__ctor_m6753_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserProfile__ctor_m6753/* method */
	, &UserProfile_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_SByte_t170_Int32_t127_Object_t/* invoker_method */
	, UserProfile_t1316_UserProfile__ctor_m6753_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1689/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::ToString()
extern const MethodInfo UserProfile_ToString_m6754_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&UserProfile_ToString_m6754/* method */
	, &UserProfile_t1316_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1690/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UserProfile_t1316_UserProfile_SetUserName_m6755_ParameterInfos[] = 
{
	{"name", 0, 134219512, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserName(System.String)
extern const MethodInfo UserProfile_SetUserName_m6755_MethodInfo = 
{
	"SetUserName"/* name */
	, (methodPointerType)&UserProfile_SetUserName_m6755/* method */
	, &UserProfile_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UserProfile_t1316_UserProfile_SetUserName_m6755_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1691/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UserProfile_t1316_UserProfile_SetUserID_m6756_ParameterInfos[] = 
{
	{"id", 0, 134219513, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetUserID(System.String)
extern const MethodInfo UserProfile_SetUserID_m6756_MethodInfo = 
{
	"SetUserID"/* name */
	, (methodPointerType)&UserProfile_SetUserID_m6756/* method */
	, &UserProfile_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UserProfile_t1316_UserProfile_SetUserID_m6756_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1692/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture2D_t270_0_0_0;
static const ParameterInfo UserProfile_t1316_UserProfile_SetImage_m6757_ParameterInfos[] = 
{
	{"image", 0, 134219514, 0, &Texture2D_t270_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.UserProfile::SetImage(UnityEngine.Texture2D)
extern const MethodInfo UserProfile_SetImage_m6757_MethodInfo = 
{
	"SetImage"/* name */
	, (methodPointerType)&UserProfile_SetImage_m6757/* method */
	, &UserProfile_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UserProfile_t1316_UserProfile_SetImage_m6757_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1693/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_userName()
extern const MethodInfo UserProfile_get_userName_m6758_MethodInfo = 
{
	"get_userName"/* name */
	, (methodPointerType)&UserProfile_get_userName_m6758/* method */
	, &UserProfile_t1316_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1694/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.UserProfile::get_id()
extern const MethodInfo UserProfile_get_id_m6759_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&UserProfile_get_id_m6759/* method */
	, &UserProfile_t1316_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1695/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.UserProfile::get_isFriend()
extern const MethodInfo UserProfile_get_isFriend_m6760_MethodInfo = 
{
	"get_isFriend"/* name */
	, (methodPointerType)&UserProfile_get_isFriend_m6760/* method */
	, &UserProfile_t1316_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1696/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_UserState_t1326 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserState UnityEngine.SocialPlatforms.Impl.UserProfile::get_state()
extern const MethodInfo UserProfile_get_state_m6761_MethodInfo = 
{
	"get_state"/* name */
	, (methodPointerType)&UserProfile_get_state_m6761/* method */
	, &UserProfile_t1316_il2cpp_TypeInfo/* declaring_type */
	, &UserState_t1326_0_0_0/* return_type */
	, RuntimeInvoker_UserState_t1326/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1697/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UserProfile_t1316_MethodInfos[] =
{
	&UserProfile__ctor_m6752_MethodInfo,
	&UserProfile__ctor_m6753_MethodInfo,
	&UserProfile_ToString_m6754_MethodInfo,
	&UserProfile_SetUserName_m6755_MethodInfo,
	&UserProfile_SetUserID_m6756_MethodInfo,
	&UserProfile_SetImage_m6757_MethodInfo,
	&UserProfile_get_userName_m6758_MethodInfo,
	&UserProfile_get_id_m6759_MethodInfo,
	&UserProfile_get_isFriend_m6760_MethodInfo,
	&UserProfile_get_state_m6761_MethodInfo,
	NULL
};
static const PropertyInfo UserProfile_t1316____userName_PropertyInfo = 
{
	&UserProfile_t1316_il2cpp_TypeInfo/* parent */
	, "userName"/* name */
	, &UserProfile_get_userName_m6758_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo UserProfile_t1316____id_PropertyInfo = 
{
	&UserProfile_t1316_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &UserProfile_get_id_m6759_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo UserProfile_t1316____isFriend_PropertyInfo = 
{
	&UserProfile_t1316_il2cpp_TypeInfo/* parent */
	, "isFriend"/* name */
	, &UserProfile_get_isFriend_m6760_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo UserProfile_t1316____state_PropertyInfo = 
{
	&UserProfile_t1316_il2cpp_TypeInfo/* parent */
	, "state"/* name */
	, &UserProfile_get_state_m6761_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* UserProfile_t1316_PropertyInfos[] =
{
	&UserProfile_t1316____userName_PropertyInfo,
	&UserProfile_t1316____id_PropertyInfo,
	&UserProfile_t1316____isFriend_PropertyInfo,
	&UserProfile_t1316____state_PropertyInfo,
	NULL
};
static const Il2CppMethodReference UserProfile_t1316_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&UserProfile_ToString_m6754_MethodInfo,
	&UserProfile_get_userName_m6758_MethodInfo,
	&UserProfile_get_id_m6759_MethodInfo,
	&UserProfile_get_isFriend_m6760_MethodInfo,
	&UserProfile_get_state_m6761_MethodInfo,
};
static bool UserProfile_t1316_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* UserProfile_t1316_InterfacesTypeInfos[] = 
{
	&IUserProfile_t1455_0_0_0,
};
static Il2CppInterfaceOffsetPair UserProfile_t1316_InterfacesOffsets[] = 
{
	{ &IUserProfile_t1455_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserProfile_t1316_1_0_0;
struct UserProfile_t1316;
const Il2CppTypeDefinitionMetadata UserProfile_t1316_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UserProfile_t1316_InterfacesTypeInfos/* implementedInterfaces */
	, UserProfile_t1316_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UserProfile_t1316_VTable/* vtableMethods */
	, UserProfile_t1316_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1153/* fieldStart */

};
TypeInfo UserProfile_t1316_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserProfile"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, UserProfile_t1316_MethodInfos/* methods */
	, UserProfile_t1316_PropertyInfos/* properties */
	, NULL/* events */
	, &UserProfile_t1316_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserProfile_t1316_0_0_0/* byval_arg */
	, &UserProfile_t1316_1_0_0/* this_arg */
	, &UserProfile_t1316_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserProfile_t1316)/* instance_size */
	, sizeof (UserProfile_t1316)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Achievement.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Achievement
extern TypeInfo Achievement_t1317_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Achievement
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t1407_0_0_0;
extern const Il2CppType Double_t1407_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType DateTime_t111_0_0_0;
extern const Il2CppType DateTime_t111_0_0_0;
static const ParameterInfo Achievement_t1317_Achievement__ctor_m6762_ParameterInfos[] = 
{
	{"id", 0, 134219515, 0, &String_t_0_0_0},
	{"percentCompleted", 1, 134219516, 0, &Double_t1407_0_0_0},
	{"completed", 2, 134219517, 0, &Boolean_t169_0_0_0},
	{"hidden", 3, 134219518, 0, &Boolean_t169_0_0_0},
	{"lastReportedDate", 4, 134219519, 0, &DateTime_t111_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Double_t1407_SByte_t170_SByte_t170_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double,System.Boolean,System.Boolean,System.DateTime)
extern const MethodInfo Achievement__ctor_m6762_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m6762/* method */
	, &Achievement_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Double_t1407_SByte_t170_SByte_t170_DateTime_t111/* invoker_method */
	, Achievement_t1317_Achievement__ctor_m6762_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1698/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo Achievement_t1317_Achievement__ctor_m6763_ParameterInfos[] = 
{
	{"id", 0, 134219520, 0, &String_t_0_0_0},
	{"percent", 1, 134219521, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor(System.String,System.Double)
extern const MethodInfo Achievement__ctor_m6763_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m6763/* method */
	, &Achievement_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Double_t1407/* invoker_method */
	, Achievement_t1317_Achievement__ctor_m6763_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1699/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::.ctor()
extern const MethodInfo Achievement__ctor_m6764_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Achievement__ctor_m6764/* method */
	, &Achievement_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1700/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::ToString()
extern const MethodInfo Achievement_ToString_m6765_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Achievement_ToString_m6765/* method */
	, &Achievement_t1317_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1701/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Achievement::get_id()
extern const MethodInfo Achievement_get_id_m6766_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&Achievement_get_id_m6766/* method */
	, &Achievement_t1317_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 772/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1702/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Achievement_t1317_Achievement_set_id_m6767_ParameterInfos[] = 
{
	{"value", 0, 134219522, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_id(System.String)
extern const MethodInfo Achievement_set_id_m6767_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&Achievement_set_id_m6767/* method */
	, &Achievement_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Achievement_t1317_Achievement_set_id_m6767_ParameterInfos/* parameters */
	, 773/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1703/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Double UnityEngine.SocialPlatforms.Impl.Achievement::get_percentCompleted()
extern const MethodInfo Achievement_get_percentCompleted_m6768_MethodInfo = 
{
	"get_percentCompleted"/* name */
	, (methodPointerType)&Achievement_get_percentCompleted_m6768/* method */
	, &Achievement_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Double_t1407_0_0_0/* return_type */
	, RuntimeInvoker_Double_t1407/* invoker_method */
	, NULL/* parameters */
	, 774/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1704/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Double_t1407_0_0_0;
static const ParameterInfo Achievement_t1317_Achievement_set_percentCompleted_m6769_ParameterInfos[] = 
{
	{"value", 0, 134219523, 0, &Double_t1407_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Double_t1407 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Achievement::set_percentCompleted(System.Double)
extern const MethodInfo Achievement_set_percentCompleted_m6769_MethodInfo = 
{
	"set_percentCompleted"/* name */
	, (methodPointerType)&Achievement_set_percentCompleted_m6769/* method */
	, &Achievement_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Double_t1407/* invoker_method */
	, Achievement_t1317_Achievement_set_percentCompleted_m6769_ParameterInfos/* parameters */
	, 775/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1705/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_completed()
extern const MethodInfo Achievement_get_completed_m6770_MethodInfo = 
{
	"get_completed"/* name */
	, (methodPointerType)&Achievement_get_completed_m6770/* method */
	, &Achievement_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1706/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.Achievement::get_hidden()
extern const MethodInfo Achievement_get_hidden_m6771_MethodInfo = 
{
	"get_hidden"/* name */
	, (methodPointerType)&Achievement_get_hidden_m6771/* method */
	, &Achievement_t1317_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1707/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_DateTime_t111 (const MethodInfo* method, void* obj, void** args);
// System.DateTime UnityEngine.SocialPlatforms.Impl.Achievement::get_lastReportedDate()
extern const MethodInfo Achievement_get_lastReportedDate_m6772_MethodInfo = 
{
	"get_lastReportedDate"/* name */
	, (methodPointerType)&Achievement_get_lastReportedDate_m6772/* method */
	, &Achievement_t1317_il2cpp_TypeInfo/* declaring_type */
	, &DateTime_t111_0_0_0/* return_type */
	, RuntimeInvoker_DateTime_t111/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1708/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Achievement_t1317_MethodInfos[] =
{
	&Achievement__ctor_m6762_MethodInfo,
	&Achievement__ctor_m6763_MethodInfo,
	&Achievement__ctor_m6764_MethodInfo,
	&Achievement_ToString_m6765_MethodInfo,
	&Achievement_get_id_m6766_MethodInfo,
	&Achievement_set_id_m6767_MethodInfo,
	&Achievement_get_percentCompleted_m6768_MethodInfo,
	&Achievement_set_percentCompleted_m6769_MethodInfo,
	&Achievement_get_completed_m6770_MethodInfo,
	&Achievement_get_hidden_m6771_MethodInfo,
	&Achievement_get_lastReportedDate_m6772_MethodInfo,
	NULL
};
extern const MethodInfo Achievement_get_id_m6766_MethodInfo;
extern const MethodInfo Achievement_set_id_m6767_MethodInfo;
static const PropertyInfo Achievement_t1317____id_PropertyInfo = 
{
	&Achievement_t1317_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &Achievement_get_id_m6766_MethodInfo/* get */
	, &Achievement_set_id_m6767_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_percentCompleted_m6768_MethodInfo;
extern const MethodInfo Achievement_set_percentCompleted_m6769_MethodInfo;
static const PropertyInfo Achievement_t1317____percentCompleted_PropertyInfo = 
{
	&Achievement_t1317_il2cpp_TypeInfo/* parent */
	, "percentCompleted"/* name */
	, &Achievement_get_percentCompleted_m6768_MethodInfo/* get */
	, &Achievement_set_percentCompleted_m6769_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_completed_m6770_MethodInfo;
static const PropertyInfo Achievement_t1317____completed_PropertyInfo = 
{
	&Achievement_t1317_il2cpp_TypeInfo/* parent */
	, "completed"/* name */
	, &Achievement_get_completed_m6770_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_hidden_m6771_MethodInfo;
static const PropertyInfo Achievement_t1317____hidden_PropertyInfo = 
{
	&Achievement_t1317_il2cpp_TypeInfo/* parent */
	, "hidden"/* name */
	, &Achievement_get_hidden_m6771_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Achievement_get_lastReportedDate_m6772_MethodInfo;
static const PropertyInfo Achievement_t1317____lastReportedDate_PropertyInfo = 
{
	&Achievement_t1317_il2cpp_TypeInfo/* parent */
	, "lastReportedDate"/* name */
	, &Achievement_get_lastReportedDate_m6772_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Achievement_t1317_PropertyInfos[] =
{
	&Achievement_t1317____id_PropertyInfo,
	&Achievement_t1317____percentCompleted_PropertyInfo,
	&Achievement_t1317____completed_PropertyInfo,
	&Achievement_t1317____hidden_PropertyInfo,
	&Achievement_t1317____lastReportedDate_PropertyInfo,
	NULL
};
extern const MethodInfo Achievement_ToString_m6765_MethodInfo;
static const Il2CppMethodReference Achievement_t1317_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Achievement_ToString_m6765_MethodInfo,
	&Achievement_get_id_m6766_MethodInfo,
	&Achievement_set_id_m6767_MethodInfo,
	&Achievement_get_percentCompleted_m6768_MethodInfo,
	&Achievement_set_percentCompleted_m6769_MethodInfo,
	&Achievement_get_completed_m6770_MethodInfo,
	&Achievement_get_hidden_m6771_MethodInfo,
	&Achievement_get_lastReportedDate_m6772_MethodInfo,
};
static bool Achievement_t1317_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IAchievement_t1358_0_0_0;
static const Il2CppType* Achievement_t1317_InterfacesTypeInfos[] = 
{
	&IAchievement_t1358_0_0_0,
};
static Il2CppInterfaceOffsetPair Achievement_t1317_InterfacesOffsets[] = 
{
	{ &IAchievement_t1358_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Achievement_t1317_1_0_0;
struct Achievement_t1317;
const Il2CppTypeDefinitionMetadata Achievement_t1317_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Achievement_t1317_InterfacesTypeInfos/* implementedInterfaces */
	, Achievement_t1317_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Achievement_t1317_VTable/* vtableMethods */
	, Achievement_t1317_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1158/* fieldStart */

};
TypeInfo Achievement_t1317_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Achievement"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Achievement_t1317_MethodInfos/* methods */
	, Achievement_t1317_PropertyInfos/* properties */
	, NULL/* events */
	, &Achievement_t1317_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Achievement_t1317_0_0_0/* byval_arg */
	, &Achievement_t1317_1_0_0/* this_arg */
	, &Achievement_t1317_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Achievement_t1317)/* instance_size */
	, sizeof (Achievement_t1317)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 11/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDesc.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.AchievementDescription
extern TypeInfo AchievementDescription_t1318_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_AchievementDescMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Texture2D_t270_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo AchievementDescription_t1318_AchievementDescription__ctor_m6773_ParameterInfos[] = 
{
	{"id", 0, 134219524, 0, &String_t_0_0_0},
	{"title", 1, 134219525, 0, &String_t_0_0_0},
	{"image", 2, 134219526, 0, &Texture2D_t270_0_0_0},
	{"achievedDescription", 3, 134219527, 0, &String_t_0_0_0},
	{"unachievedDescription", 4, 134219528, 0, &String_t_0_0_0},
	{"hidden", 5, 134219529, 0, &Boolean_t169_0_0_0},
	{"points", 6, 134219530, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t170_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::.ctor(System.String,System.String,UnityEngine.Texture2D,System.String,System.String,System.Boolean,System.Int32)
extern const MethodInfo AchievementDescription__ctor_m6773_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AchievementDescription__ctor_m6773/* method */
	, &AchievementDescription_t1318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t170_Int32_t127/* invoker_method */
	, AchievementDescription_t1318_AchievementDescription__ctor_m6773_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 7/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1709/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::ToString()
extern const MethodInfo AchievementDescription_ToString_m6774_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&AchievementDescription_ToString_m6774/* method */
	, &AchievementDescription_t1318_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1710/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture2D_t270_0_0_0;
static const ParameterInfo AchievementDescription_t1318_AchievementDescription_SetImage_m6775_ParameterInfos[] = 
{
	{"image", 0, 134219531, 0, &Texture2D_t270_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::SetImage(UnityEngine.Texture2D)
extern const MethodInfo AchievementDescription_SetImage_m6775_MethodInfo = 
{
	"SetImage"/* name */
	, (methodPointerType)&AchievementDescription_SetImage_m6775/* method */
	, &AchievementDescription_t1318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, AchievementDescription_t1318_AchievementDescription_SetImage_m6775_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1711/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_id()
extern const MethodInfo AchievementDescription_get_id_m6776_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&AchievementDescription_get_id_m6776/* method */
	, &AchievementDescription_t1318_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 777/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1712/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo AchievementDescription_t1318_AchievementDescription_set_id_m6777_ParameterInfos[] = 
{
	{"value", 0, 134219532, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.AchievementDescription::set_id(System.String)
extern const MethodInfo AchievementDescription_set_id_m6777_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&AchievementDescription_set_id_m6777/* method */
	, &AchievementDescription_t1318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, AchievementDescription_t1318_AchievementDescription_set_id_m6777_ParameterInfos/* parameters */
	, 778/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1713/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_title()
extern const MethodInfo AchievementDescription_get_title_m6778_MethodInfo = 
{
	"get_title"/* name */
	, (methodPointerType)&AchievementDescription_get_title_m6778/* method */
	, &AchievementDescription_t1318_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1714/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_achievedDescription()
extern const MethodInfo AchievementDescription_get_achievedDescription_m6779_MethodInfo = 
{
	"get_achievedDescription"/* name */
	, (methodPointerType)&AchievementDescription_get_achievedDescription_m6779/* method */
	, &AchievementDescription_t1318_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1715/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_unachievedDescription()
extern const MethodInfo AchievementDescription_get_unachievedDescription_m6780_MethodInfo = 
{
	"get_unachievedDescription"/* name */
	, (methodPointerType)&AchievementDescription_get_unachievedDescription_m6780/* method */
	, &AchievementDescription_t1318_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1716/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_hidden()
extern const MethodInfo AchievementDescription_get_hidden_m6781_MethodInfo = 
{
	"get_hidden"/* name */
	, (methodPointerType)&AchievementDescription_get_hidden_m6781/* method */
	, &AchievementDescription_t1318_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1717/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.SocialPlatforms.Impl.AchievementDescription::get_points()
extern const MethodInfo AchievementDescription_get_points_m6782_MethodInfo = 
{
	"get_points"/* name */
	, (methodPointerType)&AchievementDescription_get_points_m6782/* method */
	, &AchievementDescription_t1318_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1718/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AchievementDescription_t1318_MethodInfos[] =
{
	&AchievementDescription__ctor_m6773_MethodInfo,
	&AchievementDescription_ToString_m6774_MethodInfo,
	&AchievementDescription_SetImage_m6775_MethodInfo,
	&AchievementDescription_get_id_m6776_MethodInfo,
	&AchievementDescription_set_id_m6777_MethodInfo,
	&AchievementDescription_get_title_m6778_MethodInfo,
	&AchievementDescription_get_achievedDescription_m6779_MethodInfo,
	&AchievementDescription_get_unachievedDescription_m6780_MethodInfo,
	&AchievementDescription_get_hidden_m6781_MethodInfo,
	&AchievementDescription_get_points_m6782_MethodInfo,
	NULL
};
extern const MethodInfo AchievementDescription_get_id_m6776_MethodInfo;
extern const MethodInfo AchievementDescription_set_id_m6777_MethodInfo;
static const PropertyInfo AchievementDescription_t1318____id_PropertyInfo = 
{
	&AchievementDescription_t1318_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &AchievementDescription_get_id_m6776_MethodInfo/* get */
	, &AchievementDescription_set_id_m6777_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_title_m6778_MethodInfo;
static const PropertyInfo AchievementDescription_t1318____title_PropertyInfo = 
{
	&AchievementDescription_t1318_il2cpp_TypeInfo/* parent */
	, "title"/* name */
	, &AchievementDescription_get_title_m6778_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_achievedDescription_m6779_MethodInfo;
static const PropertyInfo AchievementDescription_t1318____achievedDescription_PropertyInfo = 
{
	&AchievementDescription_t1318_il2cpp_TypeInfo/* parent */
	, "achievedDescription"/* name */
	, &AchievementDescription_get_achievedDescription_m6779_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_unachievedDescription_m6780_MethodInfo;
static const PropertyInfo AchievementDescription_t1318____unachievedDescription_PropertyInfo = 
{
	&AchievementDescription_t1318_il2cpp_TypeInfo/* parent */
	, "unachievedDescription"/* name */
	, &AchievementDescription_get_unachievedDescription_m6780_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_hidden_m6781_MethodInfo;
static const PropertyInfo AchievementDescription_t1318____hidden_PropertyInfo = 
{
	&AchievementDescription_t1318_il2cpp_TypeInfo/* parent */
	, "hidden"/* name */
	, &AchievementDescription_get_hidden_m6781_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AchievementDescription_get_points_m6782_MethodInfo;
static const PropertyInfo AchievementDescription_t1318____points_PropertyInfo = 
{
	&AchievementDescription_t1318_il2cpp_TypeInfo/* parent */
	, "points"/* name */
	, &AchievementDescription_get_points_m6782_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AchievementDescription_t1318_PropertyInfos[] =
{
	&AchievementDescription_t1318____id_PropertyInfo,
	&AchievementDescription_t1318____title_PropertyInfo,
	&AchievementDescription_t1318____achievedDescription_PropertyInfo,
	&AchievementDescription_t1318____unachievedDescription_PropertyInfo,
	&AchievementDescription_t1318____hidden_PropertyInfo,
	&AchievementDescription_t1318____points_PropertyInfo,
	NULL
};
extern const MethodInfo AchievementDescription_ToString_m6774_MethodInfo;
static const Il2CppMethodReference AchievementDescription_t1318_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&AchievementDescription_ToString_m6774_MethodInfo,
	&AchievementDescription_get_id_m6776_MethodInfo,
	&AchievementDescription_set_id_m6777_MethodInfo,
	&AchievementDescription_get_title_m6778_MethodInfo,
	&AchievementDescription_get_achievedDescription_m6779_MethodInfo,
	&AchievementDescription_get_unachievedDescription_m6780_MethodInfo,
	&AchievementDescription_get_hidden_m6781_MethodInfo,
	&AchievementDescription_get_points_m6782_MethodInfo,
};
static bool AchievementDescription_t1318_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IAchievementDescription_t1456_0_0_0;
static const Il2CppType* AchievementDescription_t1318_InterfacesTypeInfos[] = 
{
	&IAchievementDescription_t1456_0_0_0,
};
static Il2CppInterfaceOffsetPair AchievementDescription_t1318_InterfacesOffsets[] = 
{
	{ &IAchievementDescription_t1456_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType AchievementDescription_t1318_1_0_0;
struct AchievementDescription_t1318;
const Il2CppTypeDefinitionMetadata AchievementDescription_t1318_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, AchievementDescription_t1318_InterfacesTypeInfos/* implementedInterfaces */
	, AchievementDescription_t1318_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, AchievementDescription_t1318_VTable/* vtableMethods */
	, AchievementDescription_t1318_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1163/* fieldStart */

};
TypeInfo AchievementDescription_t1318_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, AchievementDescription_t1318_MethodInfos/* methods */
	, AchievementDescription_t1318_PropertyInfos/* properties */
	, NULL/* events */
	, &AchievementDescription_t1318_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AchievementDescription_t1318_0_0_0/* byval_arg */
	, &AchievementDescription_t1318_1_0_0/* this_arg */
	, &AchievementDescription_t1318_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AchievementDescription_t1318)/* instance_size */
	, sizeof (AchievementDescription_t1318)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Score.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Score
extern TypeInfo Score_t1319_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Score
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_ScoreMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
static const ParameterInfo Score_t1319_Score__ctor_m6783_ParameterInfos[] = 
{
	{"leaderboardID", 0, 134219533, 0, &String_t_0_0_0},
	{"value", 1, 134219534, 0, &Int64_t1092_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64)
extern const MethodInfo Score__ctor_m6783_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Score__ctor_m6783/* method */
	, &Score_t1319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int64_t1092/* invoker_method */
	, Score_t1319_Score__ctor_m6783_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1719/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int64_t1092_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType DateTime_t111_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Score_t1319_Score__ctor_m6784_ParameterInfos[] = 
{
	{"leaderboardID", 0, 134219535, 0, &String_t_0_0_0},
	{"value", 1, 134219536, 0, &Int64_t1092_0_0_0},
	{"userID", 2, 134219537, 0, &String_t_0_0_0},
	{"date", 3, 134219538, 0, &DateTime_t111_0_0_0},
	{"formattedValue", 4, 134219539, 0, &String_t_0_0_0},
	{"rank", 5, 134219540, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int64_t1092_Object_t_DateTime_t111_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::.ctor(System.String,System.Int64,System.String,System.DateTime,System.String,System.Int32)
extern const MethodInfo Score__ctor_m6784_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Score__ctor_m6784/* method */
	, &Score_t1319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int64_t1092_Object_t_DateTime_t111_Object_t_Int32_t127/* invoker_method */
	, Score_t1319_Score__ctor_m6784_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1720/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Score::ToString()
extern const MethodInfo Score_ToString_m6785_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Score_ToString_m6785/* method */
	, &Score_t1319_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1721/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Score::get_leaderboardID()
extern const MethodInfo Score_get_leaderboardID_m6786_MethodInfo = 
{
	"get_leaderboardID"/* name */
	, (methodPointerType)&Score_get_leaderboardID_m6786/* method */
	, &Score_t1319_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 781/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1722/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Score_t1319_Score_set_leaderboardID_m6787_ParameterInfos[] = 
{
	{"value", 0, 134219541, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_leaderboardID(System.String)
extern const MethodInfo Score_set_leaderboardID_m6787_MethodInfo = 
{
	"set_leaderboardID"/* name */
	, (methodPointerType)&Score_set_leaderboardID_m6787/* method */
	, &Score_t1319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Score_t1319_Score_set_leaderboardID_m6787_ParameterInfos/* parameters */
	, 782/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1723/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Int64 UnityEngine.SocialPlatforms.Impl.Score::get_value()
extern const MethodInfo Score_get_value_m6788_MethodInfo = 
{
	"get_value"/* name */
	, (methodPointerType)&Score_get_value_m6788/* method */
	, &Score_t1319_il2cpp_TypeInfo/* declaring_type */
	, &Int64_t1092_0_0_0/* return_type */
	, RuntimeInvoker_Int64_t1092/* invoker_method */
	, NULL/* parameters */
	, 783/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1724/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int64_t1092_0_0_0;
static const ParameterInfo Score_t1319_Score_set_value_m6789_ParameterInfos[] = 
{
	{"value", 0, 134219542, 0, &Int64_t1092_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int64_t1092 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Score::set_value(System.Int64)
extern const MethodInfo Score_set_value_m6789_MethodInfo = 
{
	"set_value"/* name */
	, (methodPointerType)&Score_set_value_m6789/* method */
	, &Score_t1319_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int64_t1092/* invoker_method */
	, Score_t1319_Score_set_value_m6789_ParameterInfos/* parameters */
	, 784/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1725/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Score_t1319_MethodInfos[] =
{
	&Score__ctor_m6783_MethodInfo,
	&Score__ctor_m6784_MethodInfo,
	&Score_ToString_m6785_MethodInfo,
	&Score_get_leaderboardID_m6786_MethodInfo,
	&Score_set_leaderboardID_m6787_MethodInfo,
	&Score_get_value_m6788_MethodInfo,
	&Score_set_value_m6789_MethodInfo,
	NULL
};
extern const MethodInfo Score_get_leaderboardID_m6786_MethodInfo;
extern const MethodInfo Score_set_leaderboardID_m6787_MethodInfo;
static const PropertyInfo Score_t1319____leaderboardID_PropertyInfo = 
{
	&Score_t1319_il2cpp_TypeInfo/* parent */
	, "leaderboardID"/* name */
	, &Score_get_leaderboardID_m6786_MethodInfo/* get */
	, &Score_set_leaderboardID_m6787_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Score_get_value_m6788_MethodInfo;
extern const MethodInfo Score_set_value_m6789_MethodInfo;
static const PropertyInfo Score_t1319____value_PropertyInfo = 
{
	&Score_t1319_il2cpp_TypeInfo/* parent */
	, "value"/* name */
	, &Score_get_value_m6788_MethodInfo/* get */
	, &Score_set_value_m6789_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Score_t1319_PropertyInfos[] =
{
	&Score_t1319____leaderboardID_PropertyInfo,
	&Score_t1319____value_PropertyInfo,
	NULL
};
extern const MethodInfo Score_ToString_m6785_MethodInfo;
static const Il2CppMethodReference Score_t1319_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Score_ToString_m6785_MethodInfo,
	&Score_get_leaderboardID_m6786_MethodInfo,
	&Score_set_leaderboardID_m6787_MethodInfo,
	&Score_get_value_m6788_MethodInfo,
	&Score_set_value_m6789_MethodInfo,
};
static bool Score_t1319_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IScore_t1320_0_0_0;
static const Il2CppType* Score_t1319_InterfacesTypeInfos[] = 
{
	&IScore_t1320_0_0_0,
};
static Il2CppInterfaceOffsetPair Score_t1319_InterfacesOffsets[] = 
{
	{ &IScore_t1320_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Score_t1319_1_0_0;
struct Score_t1319;
const Il2CppTypeDefinitionMetadata Score_t1319_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Score_t1319_InterfacesTypeInfos/* implementedInterfaces */
	, Score_t1319_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Score_t1319_VTable/* vtableMethods */
	, Score_t1319_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1170/* fieldStart */

};
TypeInfo Score_t1319_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Score"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Score_t1319_MethodInfos/* methods */
	, Score_t1319_PropertyInfos/* properties */
	, NULL/* events */
	, &Score_t1319_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Score_t1319_0_0_0/* byval_arg */
	, &Score_t1319_1_0_0/* this_arg */
	, &Score_t1319_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Score_t1319)/* instance_size */
	, sizeof (Score_t1319)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_Leaderboard.h"
// Metadata Definition UnityEngine.SocialPlatforms.Impl.Leaderboard
extern TypeInfo Leaderboard_t1154_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Impl.Leaderboard
#include "UnityEngine_UnityEngine_SocialPlatforms_Impl_LeaderboardMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::.ctor()
extern const MethodInfo Leaderboard__ctor_m6790_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Leaderboard__ctor_m6790/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1726/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::ToString()
extern const MethodInfo Leaderboard_ToString_m6791_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&Leaderboard_ToString_m6791/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1727/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IScore_t1320_0_0_0;
static const ParameterInfo Leaderboard_t1154_Leaderboard_SetLocalUserScore_m6792_ParameterInfos[] = 
{
	{"score", 0, 134219543, 0, &IScore_t1320_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetLocalUserScore(UnityEngine.SocialPlatforms.IScore)
extern const MethodInfo Leaderboard_SetLocalUserScore_m6792_MethodInfo = 
{
	"SetLocalUserScore"/* name */
	, (methodPointerType)&Leaderboard_SetLocalUserScore_m6792/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Leaderboard_t1154_Leaderboard_SetLocalUserScore_m6792_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1728/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UInt32_t1075_0_0_0;
extern const Il2CppType UInt32_t1075_0_0_0;
static const ParameterInfo Leaderboard_t1154_Leaderboard_SetMaxRange_m6793_ParameterInfos[] = 
{
	{"maxRange", 0, 134219544, 0, &UInt32_t1075_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetMaxRange(System.UInt32)
extern const MethodInfo Leaderboard_SetMaxRange_m6793_MethodInfo = 
{
	"SetMaxRange"/* name */
	, (methodPointerType)&Leaderboard_SetMaxRange_m6793/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Leaderboard_t1154_Leaderboard_SetMaxRange_m6793_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1729/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IScoreU5BU5D_t1321_0_0_0;
extern const Il2CppType IScoreU5BU5D_t1321_0_0_0;
static const ParameterInfo Leaderboard_t1154_Leaderboard_SetScores_m6794_ParameterInfos[] = 
{
	{"scores", 0, 134219545, 0, &IScoreU5BU5D_t1321_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetScores(UnityEngine.SocialPlatforms.IScore[])
extern const MethodInfo Leaderboard_SetScores_m6794_MethodInfo = 
{
	"SetScores"/* name */
	, (methodPointerType)&Leaderboard_SetScores_m6794/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Leaderboard_t1154_Leaderboard_SetScores_m6794_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1730/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Leaderboard_t1154_Leaderboard_SetTitle_m6795_ParameterInfos[] = 
{
	{"title", 0, 134219546, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::SetTitle(System.String)
extern const MethodInfo Leaderboard_SetTitle_m6795_MethodInfo = 
{
	"SetTitle"/* name */
	, (methodPointerType)&Leaderboard_SetTitle_m6795/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Leaderboard_t1154_Leaderboard_SetTitle_m6795_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1731/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StringU5BU5D_t109_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String[] UnityEngine.SocialPlatforms.Impl.Leaderboard::GetUserFilter()
extern const MethodInfo Leaderboard_GetUserFilter_m6796_MethodInfo = 
{
	"GetUserFilter"/* name */
	, (methodPointerType)&Leaderboard_GetUserFilter_m6796/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &StringU5BU5D_t109_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1732/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.Impl.Leaderboard::get_id()
extern const MethodInfo Leaderboard_get_id_m6797_MethodInfo = 
{
	"get_id"/* name */
	, (methodPointerType)&Leaderboard_get_id_m6797/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 789/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1733/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Leaderboard_t1154_Leaderboard_set_id_m6798_ParameterInfos[] = 
{
	{"value", 0, 134219547, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_id(System.String)
extern const MethodInfo Leaderboard_set_id_m6798_MethodInfo = 
{
	"set_id"/* name */
	, (methodPointerType)&Leaderboard_set_id_m6798/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Leaderboard_t1154_Leaderboard_set_id_m6798_ParameterInfos/* parameters */
	, 790/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1734/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UserScope_t1327_0_0_0;
extern void* RuntimeInvoker_UserScope_t1327 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_userScope()
extern const MethodInfo Leaderboard_get_userScope_m6799_MethodInfo = 
{
	"get_userScope"/* name */
	, (methodPointerType)&Leaderboard_get_userScope_m6799/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t1327_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t1327/* invoker_method */
	, NULL/* parameters */
	, 791/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1735/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UserScope_t1327_0_0_0;
static const ParameterInfo Leaderboard_t1154_Leaderboard_set_userScope_m6800_ParameterInfos[] = 
{
	{"value", 0, 134219548, 0, &UserScope_t1327_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_userScope(UnityEngine.SocialPlatforms.UserScope)
extern const MethodInfo Leaderboard_set_userScope_m6800_MethodInfo = 
{
	"set_userScope"/* name */
	, (methodPointerType)&Leaderboard_set_userScope_m6800/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Leaderboard_t1154_Leaderboard_set_userScope_m6800_ParameterInfos/* parameters */
	, 792/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1736/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Range_t1322_0_0_0;
extern void* RuntimeInvoker_Range_t1322 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.Impl.Leaderboard::get_range()
extern const MethodInfo Leaderboard_get_range_m6801_MethodInfo = 
{
	"get_range"/* name */
	, (methodPointerType)&Leaderboard_get_range_m6801/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &Range_t1322_0_0_0/* return_type */
	, RuntimeInvoker_Range_t1322/* invoker_method */
	, NULL/* parameters */
	, 793/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1737/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Range_t1322_0_0_0;
static const ParameterInfo Leaderboard_t1154_Leaderboard_set_range_m6802_ParameterInfos[] = 
{
	{"value", 0, 134219549, 0, &Range_t1322_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Range_t1322 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_range(UnityEngine.SocialPlatforms.Range)
extern const MethodInfo Leaderboard_set_range_m6802_MethodInfo = 
{
	"set_range"/* name */
	, (methodPointerType)&Leaderboard_set_range_m6802/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Range_t1322/* invoker_method */
	, Leaderboard_t1154_Leaderboard_set_range_m6802_ParameterInfos/* parameters */
	, 794/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1738/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeScope_t1328_0_0_0;
extern void* RuntimeInvoker_TimeScope_t1328 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.Impl.Leaderboard::get_timeScope()
extern const MethodInfo Leaderboard_get_timeScope_m6803_MethodInfo = 
{
	"get_timeScope"/* name */
	, (methodPointerType)&Leaderboard_get_timeScope_m6803/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t1328_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t1328/* invoker_method */
	, NULL/* parameters */
	, 795/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1739/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TimeScope_t1328_0_0_0;
static const ParameterInfo Leaderboard_t1154_Leaderboard_set_timeScope_m6804_ParameterInfos[] = 
{
	{"value", 0, 134219550, 0, &TimeScope_t1328_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Impl.Leaderboard::set_timeScope(UnityEngine.SocialPlatforms.TimeScope)
extern const MethodInfo Leaderboard_set_timeScope_m6804_MethodInfo = 
{
	"set_timeScope"/* name */
	, (methodPointerType)&Leaderboard_set_timeScope_m6804/* method */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Leaderboard_t1154_Leaderboard_set_timeScope_m6804_ParameterInfos/* parameters */
	, 796/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1740/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Leaderboard_t1154_MethodInfos[] =
{
	&Leaderboard__ctor_m6790_MethodInfo,
	&Leaderboard_ToString_m6791_MethodInfo,
	&Leaderboard_SetLocalUserScore_m6792_MethodInfo,
	&Leaderboard_SetMaxRange_m6793_MethodInfo,
	&Leaderboard_SetScores_m6794_MethodInfo,
	&Leaderboard_SetTitle_m6795_MethodInfo,
	&Leaderboard_GetUserFilter_m6796_MethodInfo,
	&Leaderboard_get_id_m6797_MethodInfo,
	&Leaderboard_set_id_m6798_MethodInfo,
	&Leaderboard_get_userScope_m6799_MethodInfo,
	&Leaderboard_set_userScope_m6800_MethodInfo,
	&Leaderboard_get_range_m6801_MethodInfo,
	&Leaderboard_set_range_m6802_MethodInfo,
	&Leaderboard_get_timeScope_m6803_MethodInfo,
	&Leaderboard_set_timeScope_m6804_MethodInfo,
	NULL
};
extern const MethodInfo Leaderboard_get_id_m6797_MethodInfo;
extern const MethodInfo Leaderboard_set_id_m6798_MethodInfo;
static const PropertyInfo Leaderboard_t1154____id_PropertyInfo = 
{
	&Leaderboard_t1154_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &Leaderboard_get_id_m6797_MethodInfo/* get */
	, &Leaderboard_set_id_m6798_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Leaderboard_get_userScope_m6799_MethodInfo;
extern const MethodInfo Leaderboard_set_userScope_m6800_MethodInfo;
static const PropertyInfo Leaderboard_t1154____userScope_PropertyInfo = 
{
	&Leaderboard_t1154_il2cpp_TypeInfo/* parent */
	, "userScope"/* name */
	, &Leaderboard_get_userScope_m6799_MethodInfo/* get */
	, &Leaderboard_set_userScope_m6800_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Leaderboard_get_range_m6801_MethodInfo;
extern const MethodInfo Leaderboard_set_range_m6802_MethodInfo;
static const PropertyInfo Leaderboard_t1154____range_PropertyInfo = 
{
	&Leaderboard_t1154_il2cpp_TypeInfo/* parent */
	, "range"/* name */
	, &Leaderboard_get_range_m6801_MethodInfo/* get */
	, &Leaderboard_set_range_m6802_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Leaderboard_get_timeScope_m6803_MethodInfo;
extern const MethodInfo Leaderboard_set_timeScope_m6804_MethodInfo;
static const PropertyInfo Leaderboard_t1154____timeScope_PropertyInfo = 
{
	&Leaderboard_t1154_il2cpp_TypeInfo/* parent */
	, "timeScope"/* name */
	, &Leaderboard_get_timeScope_m6803_MethodInfo/* get */
	, &Leaderboard_set_timeScope_m6804_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Leaderboard_t1154_PropertyInfos[] =
{
	&Leaderboard_t1154____id_PropertyInfo,
	&Leaderboard_t1154____userScope_PropertyInfo,
	&Leaderboard_t1154____range_PropertyInfo,
	&Leaderboard_t1154____timeScope_PropertyInfo,
	NULL
};
extern const MethodInfo Leaderboard_ToString_m6791_MethodInfo;
static const Il2CppMethodReference Leaderboard_t1154_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Leaderboard_ToString_m6791_MethodInfo,
	&Leaderboard_get_id_m6797_MethodInfo,
	&Leaderboard_get_userScope_m6799_MethodInfo,
	&Leaderboard_get_range_m6801_MethodInfo,
	&Leaderboard_get_timeScope_m6803_MethodInfo,
	&Leaderboard_set_id_m6798_MethodInfo,
	&Leaderboard_set_userScope_m6800_MethodInfo,
	&Leaderboard_set_range_m6802_MethodInfo,
	&Leaderboard_set_timeScope_m6804_MethodInfo,
};
static bool Leaderboard_t1154_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILeaderboard_t1357_0_0_0;
static const Il2CppType* Leaderboard_t1154_InterfacesTypeInfos[] = 
{
	&ILeaderboard_t1357_0_0_0,
};
static Il2CppInterfaceOffsetPair Leaderboard_t1154_InterfacesOffsets[] = 
{
	{ &ILeaderboard_t1357_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Leaderboard_t1154_0_0_0;
extern const Il2CppType Leaderboard_t1154_1_0_0;
struct Leaderboard_t1154;
const Il2CppTypeDefinitionMetadata Leaderboard_t1154_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Leaderboard_t1154_InterfacesTypeInfos/* implementedInterfaces */
	, Leaderboard_t1154_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Leaderboard_t1154_VTable/* vtableMethods */
	, Leaderboard_t1154_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1176/* fieldStart */

};
TypeInfo Leaderboard_t1154_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Leaderboard"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, Leaderboard_t1154_MethodInfos/* methods */
	, Leaderboard_t1154_PropertyInfos/* properties */
	, NULL/* events */
	, &Leaderboard_t1154_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Leaderboard_t1154_0_0_0/* byval_arg */
	, &Leaderboard_t1154_1_0_0/* this_arg */
	, &Leaderboard_t1154_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Leaderboard_t1154)/* instance_size */
	, sizeof (Leaderboard_t1154)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 4/* property_count */
	, 10/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
// Metadata Definition UnityEngine.SendMouseEvents/HitInfo
extern TypeInfo HitInfo_t1323_il2cpp_TypeInfo;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfoMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo HitInfo_t1323_HitInfo_SendMessage_m6805_ParameterInfos[] = 
{
	{"name", 0, 134219555, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
extern const MethodInfo HitInfo_SendMessage_m6805_MethodInfo = 
{
	"SendMessage"/* name */
	, (methodPointerType)&HitInfo_SendMessage_m6805/* method */
	, &HitInfo_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, HitInfo_t1323_HitInfo_SendMessage_m6805_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1744/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HitInfo_t1323_0_0_0;
extern const Il2CppType HitInfo_t1323_0_0_0;
extern const Il2CppType HitInfo_t1323_0_0_0;
static const ParameterInfo HitInfo_t1323_HitInfo_Compare_m6806_ParameterInfos[] = 
{
	{"lhs", 0, 134219556, 0, &HitInfo_t1323_0_0_0},
	{"rhs", 1, 134219557, 0, &HitInfo_t1323_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_HitInfo_t1323_HitInfo_t1323 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
extern const MethodInfo HitInfo_Compare_m6806_MethodInfo = 
{
	"Compare"/* name */
	, (methodPointerType)&HitInfo_Compare_m6806/* method */
	, &HitInfo_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_HitInfo_t1323_HitInfo_t1323/* invoker_method */
	, HitInfo_t1323_HitInfo_Compare_m6806_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1745/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HitInfo_t1323_0_0_0;
static const ParameterInfo HitInfo_t1323_HitInfo_op_Implicit_m6807_ParameterInfos[] = 
{
	{"exists", 0, 134219558, 0, &HitInfo_t1323_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_HitInfo_t1323 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
extern const MethodInfo HitInfo_op_Implicit_m6807_MethodInfo = 
{
	"op_Implicit"/* name */
	, (methodPointerType)&HitInfo_op_Implicit_m6807/* method */
	, &HitInfo_t1323_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_HitInfo_t1323/* invoker_method */
	, HitInfo_t1323_HitInfo_op_Implicit_m6807_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1746/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HitInfo_t1323_MethodInfos[] =
{
	&HitInfo_SendMessage_m6805_MethodInfo,
	&HitInfo_Compare_m6806_MethodInfo,
	&HitInfo_op_Implicit_m6807_MethodInfo,
	NULL
};
static const Il2CppMethodReference HitInfo_t1323_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool HitInfo_t1323_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType HitInfo_t1323_1_0_0;
extern TypeInfo SendMouseEvents_t1325_il2cpp_TypeInfo;
extern const Il2CppType SendMouseEvents_t1325_0_0_0;
const Il2CppTypeDefinitionMetadata HitInfo_t1323_DefinitionMetadata = 
{
	&SendMouseEvents_t1325_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, HitInfo_t1323_VTable/* vtableMethods */
	, HitInfo_t1323_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1186/* fieldStart */

};
TypeInfo HitInfo_t1323_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "HitInfo"/* name */
	, ""/* namespaze */
	, HitInfo_t1323_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HitInfo_t1323_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HitInfo_t1323_0_0_0/* byval_arg */
	, &HitInfo_t1323_1_0_0/* this_arg */
	, &HitInfo_t1323_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HitInfo_t1323)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (HitInfo_t1323)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEvents.h"
// Metadata Definition UnityEngine.SendMouseEvents
// UnityEngine.SendMouseEvents
#include "UnityEngine_UnityEngine_SendMouseEventsMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::.cctor()
extern const MethodInfo SendMouseEvents__cctor_m6808_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&SendMouseEvents__cctor_m6808/* method */
	, &SendMouseEvents_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1741/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo SendMouseEvents_t1325_SendMouseEvents_DoSendMouseEvents_m6809_ParameterInfos[] = 
{
	{"mouseUsed", 0, 134219551, 0, &Int32_t127_0_0_0},
	{"skipRTCameras", 1, 134219552, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::DoSendMouseEvents(System.Int32,System.Int32)
extern const MethodInfo SendMouseEvents_DoSendMouseEvents_m6809_MethodInfo = 
{
	"DoSendMouseEvents"/* name */
	, (methodPointerType)&SendMouseEvents_DoSendMouseEvents_m6809/* method */
	, &SendMouseEvents_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127/* invoker_method */
	, SendMouseEvents_t1325_SendMouseEvents_DoSendMouseEvents_m6809_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1742/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType HitInfo_t1323_0_0_0;
static const ParameterInfo SendMouseEvents_t1325_SendMouseEvents_SendEvents_m6810_ParameterInfos[] = 
{
	{"i", 0, 134219553, 0, &Int32_t127_0_0_0},
	{"hit", 1, 134219554, 0, &HitInfo_t1323_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_HitInfo_t1323 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SendMouseEvents::SendEvents(System.Int32,UnityEngine.SendMouseEvents/HitInfo)
extern const MethodInfo SendMouseEvents_SendEvents_m6810_MethodInfo = 
{
	"SendEvents"/* name */
	, (methodPointerType)&SendMouseEvents_SendEvents_m6810/* method */
	, &SendMouseEvents_t1325_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_HitInfo_t1323/* invoker_method */
	, SendMouseEvents_t1325_SendMouseEvents_SendEvents_m6810_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1743/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SendMouseEvents_t1325_MethodInfos[] =
{
	&SendMouseEvents__cctor_m6808_MethodInfo,
	&SendMouseEvents_DoSendMouseEvents_m6809_MethodInfo,
	&SendMouseEvents_SendEvents_m6810_MethodInfo,
	NULL
};
static const Il2CppType* SendMouseEvents_t1325_il2cpp_TypeInfo__nestedTypes[1] =
{
	&HitInfo_t1323_0_0_0,
};
static const Il2CppMethodReference SendMouseEvents_t1325_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool SendMouseEvents_t1325_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SendMouseEvents_t1325_1_0_0;
struct SendMouseEvents_t1325;
const Il2CppTypeDefinitionMetadata SendMouseEvents_t1325_DefinitionMetadata = 
{
	NULL/* declaringType */
	, SendMouseEvents_t1325_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SendMouseEvents_t1325_VTable/* vtableMethods */
	, SendMouseEvents_t1325_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1188/* fieldStart */

};
TypeInfo SendMouseEvents_t1325_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SendMouseEvents"/* name */
	, "UnityEngine"/* namespaze */
	, SendMouseEvents_t1325_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SendMouseEvents_t1325_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SendMouseEvents_t1325_0_0_0/* byval_arg */
	, &SendMouseEvents_t1325_1_0_0/* this_arg */
	, &SendMouseEvents_t1325_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SendMouseEvents_t1325)/* instance_size */
	, sizeof (SendMouseEvents_t1325)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SendMouseEvents_t1325_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ISocialPlatform
extern TypeInfo ISocialPlatform_t1454_il2cpp_TypeInfo;
extern const Il2CppType ILocalUser_t1354_0_0_0;
extern const Il2CppType Action_1_t747_0_0_0;
extern const Il2CppType Action_1_t747_0_0_0;
static const ParameterInfo ISocialPlatform_t1454_ISocialPlatform_Authenticate_m7111_ParameterInfos[] = 
{
	{"user", 0, 134219559, 0, &ILocalUser_t1354_0_0_0},
	{"callback", 1, 134219560, 0, &Action_1_t747_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::Authenticate(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern const MethodInfo ISocialPlatform_Authenticate_m7111_MethodInfo = 
{
	"Authenticate"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1454_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, ISocialPlatform_t1454_ISocialPlatform_Authenticate_m7111_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1747/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILocalUser_t1354_0_0_0;
extern const Il2CppType Action_1_t747_0_0_0;
static const ParameterInfo ISocialPlatform_t1454_ISocialPlatform_LoadFriends_m7112_ParameterInfos[] = 
{
	{"user", 0, 134219561, 0, &ILocalUser_t1354_0_0_0},
	{"callback", 1, 134219562, 0, &Action_1_t747_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.ISocialPlatform::LoadFriends(UnityEngine.SocialPlatforms.ILocalUser,System.Action`1<System.Boolean>)
extern const MethodInfo ISocialPlatform_LoadFriends_m7112_MethodInfo = 
{
	"LoadFriends"/* name */
	, NULL/* method */
	, &ISocialPlatform_t1454_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, ISocialPlatform_t1454_ISocialPlatform_LoadFriends_m7112_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1748/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ISocialPlatform_t1454_MethodInfos[] =
{
	&ISocialPlatform_Authenticate_m7111_MethodInfo,
	&ISocialPlatform_LoadFriends_m7112_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ISocialPlatform_t1454_0_0_0;
extern const Il2CppType ISocialPlatform_t1454_1_0_0;
struct ISocialPlatform_t1454;
const Il2CppTypeDefinitionMetadata ISocialPlatform_t1454_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ISocialPlatform_t1454_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISocialPlatform"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ISocialPlatform_t1454_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ISocialPlatform_t1454_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ISocialPlatform_t1454_0_0_0/* byval_arg */
	, &ISocialPlatform_t1454_1_0_0/* this_arg */
	, &ISocialPlatform_t1454_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ILocalUser
extern TypeInfo ILocalUser_t1354_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.SocialPlatforms.ILocalUser::get_authenticated()
extern const MethodInfo ILocalUser_get_authenticated_m7113_MethodInfo = 
{
	"get_authenticated"/* name */
	, NULL/* method */
	, &ILocalUser_t1354_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1749/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILocalUser_t1354_MethodInfos[] =
{
	&ILocalUser_get_authenticated_m7113_MethodInfo,
	NULL
};
extern const MethodInfo ILocalUser_get_authenticated_m7113_MethodInfo;
static const PropertyInfo ILocalUser_t1354____authenticated_PropertyInfo = 
{
	&ILocalUser_t1354_il2cpp_TypeInfo/* parent */
	, "authenticated"/* name */
	, &ILocalUser_get_authenticated_m7113_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILocalUser_t1354_PropertyInfos[] =
{
	&ILocalUser_t1354____authenticated_PropertyInfo,
	NULL
};
static const Il2CppType* ILocalUser_t1354_InterfacesTypeInfos[] = 
{
	&IUserProfile_t1455_0_0_0,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ILocalUser_t1354_1_0_0;
struct ILocalUser_t1354;
const Il2CppTypeDefinitionMetadata ILocalUser_t1354_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILocalUser_t1354_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILocalUser_t1354_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILocalUser"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ILocalUser_t1354_MethodInfos/* methods */
	, ILocalUser_t1354_PropertyInfos/* properties */
	, NULL/* events */
	, &ILocalUser_t1354_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILocalUser_t1354_0_0_0/* byval_arg */
	, &ILocalUser_t1354_1_0_0/* this_arg */
	, &ILocalUser_t1354_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
// Metadata Definition UnityEngine.SocialPlatforms.UserState
extern TypeInfo UserState_t1326_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserStateMethodDeclarations.h"
static const MethodInfo* UserState_t1326_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UserState_t1326_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool UserState_t1326_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UserState_t1326_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserState_t1326_1_0_0;
const Il2CppTypeDefinitionMetadata UserState_t1326_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UserState_t1326_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, UserState_t1326_VTable/* vtableMethods */
	, UserState_t1326_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1195/* fieldStart */

};
TypeInfo UserState_t1326_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserState"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, UserState_t1326_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserState_t1326_0_0_0/* byval_arg */
	, &UserState_t1326_1_0_0/* this_arg */
	, &UserState_t1326_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserState_t1326)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UserState_t1326)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IUserProfile
extern TypeInfo IUserProfile_t1455_il2cpp_TypeInfo;
static const MethodInfo* IUserProfile_t1455_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IUserProfile_t1455_1_0_0;
struct IUserProfile_t1455;
const Il2CppTypeDefinitionMetadata IUserProfile_t1455_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IUserProfile_t1455_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IUserProfile"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IUserProfile_t1455_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IUserProfile_t1455_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IUserProfile_t1455_0_0_0/* byval_arg */
	, &IUserProfile_t1455_1_0_0/* this_arg */
	, &IUserProfile_t1455_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IAchievement
extern TypeInfo IAchievement_t1358_il2cpp_TypeInfo;
static const MethodInfo* IAchievement_t1358_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IAchievement_t1358_1_0_0;
struct IAchievement_t1358;
const Il2CppTypeDefinitionMetadata IAchievement_t1358_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IAchievement_t1358_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievement"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievement_t1358_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IAchievement_t1358_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAchievement_t1358_0_0_0/* byval_arg */
	, &IAchievement_t1358_1_0_0/* this_arg */
	, &IAchievement_t1358_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IAchievementDescription
extern TypeInfo IAchievementDescription_t1456_il2cpp_TypeInfo;
static const MethodInfo* IAchievementDescription_t1456_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IAchievementDescription_t1456_1_0_0;
struct IAchievementDescription_t1456;
const Il2CppTypeDefinitionMetadata IAchievementDescription_t1456_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IAchievementDescription_t1456_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievementDescription_t1456_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IAchievementDescription_t1456_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IAchievementDescription_t1456_0_0_0/* byval_arg */
	, &IAchievementDescription_t1456_1_0_0/* this_arg */
	, &IAchievementDescription_t1456_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.IScore
extern TypeInfo IScore_t1320_il2cpp_TypeInfo;
static const MethodInfo* IScore_t1320_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType IScore_t1320_1_0_0;
struct IScore_t1320;
const Il2CppTypeDefinitionMetadata IScore_t1320_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IScore_t1320_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IScore"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IScore_t1320_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IScore_t1320_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IScore_t1320_0_0_0/* byval_arg */
	, &IScore_t1320_1_0_0/* this_arg */
	, &IScore_t1320_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
// Metadata Definition UnityEngine.SocialPlatforms.UserScope
extern TypeInfo UserScope_t1327_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScopeMethodDeclarations.h"
static const MethodInfo* UserScope_t1327_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UserScope_t1327_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool UserScope_t1327_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UserScope_t1327_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserScope_t1327_1_0_0;
const Il2CppTypeDefinitionMetadata UserScope_t1327_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UserScope_t1327_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, UserScope_t1327_VTable/* vtableMethods */
	, UserScope_t1327_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1201/* fieldStart */

};
TypeInfo UserScope_t1327_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, UserScope_t1327_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UserScope_t1327_0_0_0/* byval_arg */
	, &UserScope_t1327_1_0_0/* this_arg */
	, &UserScope_t1327_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserScope_t1327)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UserScope_t1327)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
// Metadata Definition UnityEngine.SocialPlatforms.TimeScope
extern TypeInfo TimeScope_t1328_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScopeMethodDeclarations.h"
static const MethodInfo* TimeScope_t1328_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TimeScope_t1328_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool TimeScope_t1328_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TimeScope_t1328_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TimeScope_t1328_1_0_0;
const Il2CppTypeDefinitionMetadata TimeScope_t1328_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TimeScope_t1328_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, TimeScope_t1328_VTable/* vtableMethods */
	, TimeScope_t1328_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1204/* fieldStart */

};
TypeInfo TimeScope_t1328_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, TimeScope_t1328_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TimeScope_t1328_0_0_0/* byval_arg */
	, &TimeScope_t1328_1_0_0/* this_arg */
	, &TimeScope_t1328_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeScope_t1328)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TimeScope_t1328)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
// Metadata Definition UnityEngine.SocialPlatforms.Range
extern TypeInfo Range_t1322_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_RangeMethodDeclarations.h"
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Range_t1322_Range__ctor_m6811_ParameterInfos[] = 
{
	{"fromValue", 0, 134219563, 0, &Int32_t127_0_0_0},
	{"valueCount", 1, 134219564, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
extern const MethodInfo Range__ctor_m6811_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Range__ctor_m6811/* method */
	, &Range_t1322_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127/* invoker_method */
	, Range_t1322_Range__ctor_m6811_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1750/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Range_t1322_MethodInfos[] =
{
	&Range__ctor_m6811_MethodInfo,
	NULL
};
static const Il2CppMethodReference Range_t1322_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool Range_t1322_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType Range_t1322_1_0_0;
const Il2CppTypeDefinitionMetadata Range_t1322_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, Range_t1322_VTable/* vtableMethods */
	, Range_t1322_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1208/* fieldStart */

};
TypeInfo Range_t1322_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Range"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, Range_t1322_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Range_t1322_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Range_t1322_0_0_0/* byval_arg */
	, &Range_t1322_1_0_0/* this_arg */
	, &Range_t1322_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Range_t1322)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Range_t1322)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(Range_t1322 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.SocialPlatforms.ILeaderboard
extern TypeInfo ILeaderboard_t1357_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id()
extern const MethodInfo ILeaderboard_get_id_m7114_MethodInfo = 
{
	"get_id"/* name */
	, NULL/* method */
	, &ILeaderboard_t1357_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1751/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_UserScope_t1327 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope()
extern const MethodInfo ILeaderboard_get_userScope_m7115_MethodInfo = 
{
	"get_userScope"/* name */
	, NULL/* method */
	, &ILeaderboard_t1357_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t1327_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t1327/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1752/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Range_t1322 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range()
extern const MethodInfo ILeaderboard_get_range_m7116_MethodInfo = 
{
	"get_range"/* name */
	, NULL/* method */
	, &ILeaderboard_t1357_il2cpp_TypeInfo/* declaring_type */
	, &Range_t1322_0_0_0/* return_type */
	, RuntimeInvoker_Range_t1322/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1753/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TimeScope_t1328 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope()
extern const MethodInfo ILeaderboard_get_timeScope_m7117_MethodInfo = 
{
	"get_timeScope"/* name */
	, NULL/* method */
	, &ILeaderboard_t1357_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t1328_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t1328/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1754/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILeaderboard_t1357_MethodInfos[] =
{
	&ILeaderboard_get_id_m7114_MethodInfo,
	&ILeaderboard_get_userScope_m7115_MethodInfo,
	&ILeaderboard_get_range_m7116_MethodInfo,
	&ILeaderboard_get_timeScope_m7117_MethodInfo,
	NULL
};
extern const MethodInfo ILeaderboard_get_id_m7114_MethodInfo;
static const PropertyInfo ILeaderboard_t1357____id_PropertyInfo = 
{
	&ILeaderboard_t1357_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &ILeaderboard_get_id_m7114_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILeaderboard_get_userScope_m7115_MethodInfo;
static const PropertyInfo ILeaderboard_t1357____userScope_PropertyInfo = 
{
	&ILeaderboard_t1357_il2cpp_TypeInfo/* parent */
	, "userScope"/* name */
	, &ILeaderboard_get_userScope_m7115_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILeaderboard_get_range_m7116_MethodInfo;
static const PropertyInfo ILeaderboard_t1357____range_PropertyInfo = 
{
	&ILeaderboard_t1357_il2cpp_TypeInfo/* parent */
	, "range"/* name */
	, &ILeaderboard_get_range_m7116_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILeaderboard_get_timeScope_m7117_MethodInfo;
static const PropertyInfo ILeaderboard_t1357____timeScope_PropertyInfo = 
{
	&ILeaderboard_t1357_il2cpp_TypeInfo/* parent */
	, "timeScope"/* name */
	, &ILeaderboard_get_timeScope_m7117_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILeaderboard_t1357_PropertyInfos[] =
{
	&ILeaderboard_t1357____id_PropertyInfo,
	&ILeaderboard_t1357____userScope_PropertyInfo,
	&ILeaderboard_t1357____range_PropertyInfo,
	&ILeaderboard_t1357____timeScope_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ILeaderboard_t1357_1_0_0;
struct ILeaderboard_t1357;
const Il2CppTypeDefinitionMetadata ILeaderboard_t1357_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILeaderboard_t1357_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILeaderboard"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ILeaderboard_t1357_MethodInfos/* methods */
	, ILeaderboard_t1357_PropertyInfos/* properties */
	, NULL/* events */
	, &ILeaderboard_t1357_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILeaderboard_t1357_0_0_0/* byval_arg */
	, &ILeaderboard_t1357_1_0_0/* this_arg */
	, &ILeaderboard_t1357_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
// Metadata Definition UnityEngine.PropertyAttribute
extern TypeInfo PropertyAttribute_t1329_il2cpp_TypeInfo;
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern const MethodInfo PropertyAttribute__ctor_m6812_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PropertyAttribute__ctor_m6812/* method */
	, &PropertyAttribute_t1329_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1755/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PropertyAttribute_t1329_MethodInfos[] =
{
	&PropertyAttribute__ctor_m6812_MethodInfo,
	NULL
};
static const Il2CppMethodReference PropertyAttribute_t1329_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool PropertyAttribute_t1329_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PropertyAttribute_t1329_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PropertyAttribute_t1329_0_0_0;
extern const Il2CppType PropertyAttribute_t1329_1_0_0;
struct PropertyAttribute_t1329;
const Il2CppTypeDefinitionMetadata PropertyAttribute_t1329_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PropertyAttribute_t1329_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, PropertyAttribute_t1329_VTable/* vtableMethods */
	, PropertyAttribute_t1329_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PropertyAttribute_t1329_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, PropertyAttribute_t1329_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PropertyAttribute_t1329_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 797/* custom_attributes_cache */
	, &PropertyAttribute_t1329_0_0_0/* byval_arg */
	, &PropertyAttribute_t1329_1_0_0/* this_arg */
	, &PropertyAttribute_t1329_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyAttribute_t1329)/* instance_size */
	, sizeof (PropertyAttribute_t1329)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
// Metadata Definition UnityEngine.TooltipAttribute
extern TypeInfo TooltipAttribute_t505_il2cpp_TypeInfo;
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TooltipAttribute_t505_TooltipAttribute__ctor_m2501_ParameterInfos[] = 
{
	{"tooltip", 0, 134219565, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern const MethodInfo TooltipAttribute__ctor_m2501_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TooltipAttribute__ctor_m2501/* method */
	, &TooltipAttribute_t505_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, TooltipAttribute_t505_TooltipAttribute__ctor_m2501_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1756/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TooltipAttribute_t505_MethodInfos[] =
{
	&TooltipAttribute__ctor_m2501_MethodInfo,
	NULL
};
static const Il2CppMethodReference TooltipAttribute_t505_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool TooltipAttribute_t505_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TooltipAttribute_t505_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TooltipAttribute_t505_0_0_0;
extern const Il2CppType TooltipAttribute_t505_1_0_0;
struct TooltipAttribute_t505;
const Il2CppTypeDefinitionMetadata TooltipAttribute_t505_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TooltipAttribute_t505_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1329_0_0_0/* parent */
	, TooltipAttribute_t505_VTable/* vtableMethods */
	, TooltipAttribute_t505_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1210/* fieldStart */

};
TypeInfo TooltipAttribute_t505_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TooltipAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, TooltipAttribute_t505_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TooltipAttribute_t505_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 798/* custom_attributes_cache */
	, &TooltipAttribute_t505_0_0_0/* byval_arg */
	, &TooltipAttribute_t505_1_0_0/* this_arg */
	, &TooltipAttribute_t505_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TooltipAttribute_t505)/* instance_size */
	, sizeof (TooltipAttribute_t505)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
// Metadata Definition UnityEngine.SpaceAttribute
extern TypeInfo SpaceAttribute_t503_il2cpp_TypeInfo;
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttributeMethodDeclarations.h"
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo SpaceAttribute_t503_SpaceAttribute__ctor_m2499_ParameterInfos[] = 
{
	{"height", 0, 134219566, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern const MethodInfo SpaceAttribute__ctor_m2499_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SpaceAttribute__ctor_m2499/* method */
	, &SpaceAttribute_t503_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, SpaceAttribute_t503_SpaceAttribute__ctor_m2499_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1757/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SpaceAttribute_t503_MethodInfos[] =
{
	&SpaceAttribute__ctor_m2499_MethodInfo,
	NULL
};
static const Il2CppMethodReference SpaceAttribute_t503_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool SpaceAttribute_t503_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SpaceAttribute_t503_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SpaceAttribute_t503_0_0_0;
extern const Il2CppType SpaceAttribute_t503_1_0_0;
struct SpaceAttribute_t503;
const Il2CppTypeDefinitionMetadata SpaceAttribute_t503_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SpaceAttribute_t503_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1329_0_0_0/* parent */
	, SpaceAttribute_t503_VTable/* vtableMethods */
	, SpaceAttribute_t503_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1211/* fieldStart */

};
TypeInfo SpaceAttribute_t503_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpaceAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SpaceAttribute_t503_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SpaceAttribute_t503_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 799/* custom_attributes_cache */
	, &SpaceAttribute_t503_0_0_0/* byval_arg */
	, &SpaceAttribute_t503_1_0_0/* this_arg */
	, &SpaceAttribute_t503_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpaceAttribute_t503)/* instance_size */
	, sizeof (SpaceAttribute_t503)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
// Metadata Definition UnityEngine.RangeAttribute
extern TypeInfo RangeAttribute_t499_il2cpp_TypeInfo;
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo RangeAttribute_t499_RangeAttribute__ctor_m2492_ParameterInfos[] = 
{
	{"min", 0, 134219567, 0, &Single_t151_0_0_0},
	{"max", 1, 134219568, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern const MethodInfo RangeAttribute__ctor_m2492_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RangeAttribute__ctor_m2492/* method */
	, &RangeAttribute_t499_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151_Single_t151/* invoker_method */
	, RangeAttribute_t499_RangeAttribute__ctor_m2492_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1758/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* RangeAttribute_t499_MethodInfos[] =
{
	&RangeAttribute__ctor_m2492_MethodInfo,
	NULL
};
static const Il2CppMethodReference RangeAttribute_t499_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool RangeAttribute_t499_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair RangeAttribute_t499_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType RangeAttribute_t499_0_0_0;
extern const Il2CppType RangeAttribute_t499_1_0_0;
struct RangeAttribute_t499;
const Il2CppTypeDefinitionMetadata RangeAttribute_t499_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, RangeAttribute_t499_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1329_0_0_0/* parent */
	, RangeAttribute_t499_VTable/* vtableMethods */
	, RangeAttribute_t499_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1212/* fieldStart */

};
TypeInfo RangeAttribute_t499_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RangeAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, RangeAttribute_t499_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &RangeAttribute_t499_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 800/* custom_attributes_cache */
	, &RangeAttribute_t499_0_0_0/* byval_arg */
	, &RangeAttribute_t499_1_0_0/* this_arg */
	, &RangeAttribute_t499_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RangeAttribute_t499)/* instance_size */
	, sizeof (RangeAttribute_t499)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
// Metadata Definition UnityEngine.TextAreaAttribute
extern TypeInfo TextAreaAttribute_t506_il2cpp_TypeInfo;
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttributeMethodDeclarations.h"
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo TextAreaAttribute_t506_TextAreaAttribute__ctor_m2504_ParameterInfos[] = 
{
	{"minLines", 0, 134219569, 0, &Int32_t127_0_0_0},
	{"maxLines", 1, 134219570, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern const MethodInfo TextAreaAttribute__ctor_m2504_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextAreaAttribute__ctor_m2504/* method */
	, &TextAreaAttribute_t506_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_Int32_t127/* invoker_method */
	, TextAreaAttribute_t506_TextAreaAttribute__ctor_m2504_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1759/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextAreaAttribute_t506_MethodInfos[] =
{
	&TextAreaAttribute__ctor_m2504_MethodInfo,
	NULL
};
static const Il2CppMethodReference TextAreaAttribute_t506_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool TextAreaAttribute_t506_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextAreaAttribute_t506_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextAreaAttribute_t506_0_0_0;
extern const Il2CppType TextAreaAttribute_t506_1_0_0;
struct TextAreaAttribute_t506;
const Il2CppTypeDefinitionMetadata TextAreaAttribute_t506_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextAreaAttribute_t506_InterfacesOffsets/* interfaceOffsets */
	, &PropertyAttribute_t1329_0_0_0/* parent */
	, TextAreaAttribute_t506_VTable/* vtableMethods */
	, TextAreaAttribute_t506_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1214/* fieldStart */

};
TypeInfo TextAreaAttribute_t506_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextAreaAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, TextAreaAttribute_t506_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TextAreaAttribute_t506_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 801/* custom_attributes_cache */
	, &TextAreaAttribute_t506_0_0_0/* byval_arg */
	, &TextAreaAttribute_t506_1_0_0/* this_arg */
	, &TextAreaAttribute_t506_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextAreaAttribute_t506)/* instance_size */
	, sizeof (TextAreaAttribute_t506)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
// Metadata Definition UnityEngine.SelectionBaseAttribute
extern TypeInfo SelectionBaseAttribute_t504_il2cpp_TypeInfo;
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern const MethodInfo SelectionBaseAttribute__ctor_m2500_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SelectionBaseAttribute__ctor_m2500/* method */
	, &SelectionBaseAttribute_t504_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1760/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SelectionBaseAttribute_t504_MethodInfos[] =
{
	&SelectionBaseAttribute__ctor_m2500_MethodInfo,
	NULL
};
static const Il2CppMethodReference SelectionBaseAttribute_t504_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool SelectionBaseAttribute_t504_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SelectionBaseAttribute_t504_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SelectionBaseAttribute_t504_0_0_0;
extern const Il2CppType SelectionBaseAttribute_t504_1_0_0;
struct SelectionBaseAttribute_t504;
const Il2CppTypeDefinitionMetadata SelectionBaseAttribute_t504_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SelectionBaseAttribute_t504_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, SelectionBaseAttribute_t504_VTable/* vtableMethods */
	, SelectionBaseAttribute_t504_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SelectionBaseAttribute_t504_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SelectionBaseAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SelectionBaseAttribute_t504_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SelectionBaseAttribute_t504_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 802/* custom_attributes_cache */
	, &SelectionBaseAttribute_t504_0_0_0/* byval_arg */
	, &SelectionBaseAttribute_t504_1_0_0/* this_arg */
	, &SelectionBaseAttribute_t504_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SelectionBaseAttribute_t504)/* instance_size */
	, sizeof (SelectionBaseAttribute_t504)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderState.h"
// Metadata Definition UnityEngine.SliderState
extern TypeInfo SliderState_t1330_il2cpp_TypeInfo;
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderStateMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SliderState::.ctor()
extern const MethodInfo SliderState__ctor_m6813_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SliderState__ctor_m6813/* method */
	, &SliderState_t1330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1761/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SliderState_t1330_MethodInfos[] =
{
	&SliderState__ctor_m6813_MethodInfo,
	NULL
};
static const Il2CppMethodReference SliderState_t1330_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool SliderState_t1330_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SliderState_t1330_0_0_0;
extern const Il2CppType SliderState_t1330_1_0_0;
struct SliderState_t1330;
const Il2CppTypeDefinitionMetadata SliderState_t1330_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SliderState_t1330_VTable/* vtableMethods */
	, SliderState_t1330_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1216/* fieldStart */

};
TypeInfo SliderState_t1330_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderState"/* name */
	, "UnityEngine"/* namespaze */
	, SliderState_t1330_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SliderState_t1330_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SliderState_t1330_0_0_0/* byval_arg */
	, &SliderState_t1330_1_0_0/* this_arg */
	, &SliderState_t1330_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderState_t1330)/* instance_size */
	, sizeof (SliderState_t1330)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtility.h"
// Metadata Definition UnityEngine.StackTraceUtility
extern TypeInfo StackTraceUtility_t1331_il2cpp_TypeInfo;
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtilityMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::.ctor()
extern const MethodInfo StackTraceUtility__ctor_m6814_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StackTraceUtility__ctor_m6814/* method */
	, &StackTraceUtility_t1331_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1762/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern const MethodInfo StackTraceUtility__cctor_m6815_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StackTraceUtility__cctor_m6815/* method */
	, &StackTraceUtility_t1331_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1763/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo StackTraceUtility_t1331_StackTraceUtility_SetProjectFolder_m6816_ParameterInfos[] = 
{
	{"folder", 0, 134219571, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern const MethodInfo StackTraceUtility_SetProjectFolder_m6816_MethodInfo = 
{
	"SetProjectFolder"/* name */
	, (methodPointerType)&StackTraceUtility_SetProjectFolder_m6816/* method */
	, &StackTraceUtility_t1331_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, StackTraceUtility_t1331_StackTraceUtility_SetProjectFolder_m6816_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern const MethodInfo StackTraceUtility_ExtractStackTrace_m6817_MethodInfo = 
{
	"ExtractStackTrace"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStackTrace_m6817/* method */
	, &StackTraceUtility_t1331_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 803/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StackTraceUtility_t1331_StackTraceUtility_IsSystemStacktraceType_m6818_ParameterInfos[] = 
{
	{"name", 0, 134219572, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
extern const MethodInfo StackTraceUtility_IsSystemStacktraceType_m6818_MethodInfo = 
{
	"IsSystemStacktraceType"/* name */
	, (methodPointerType)&StackTraceUtility_IsSystemStacktraceType_m6818/* method */
	, &StackTraceUtility_t1331_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, StackTraceUtility_t1331_StackTraceUtility_IsSystemStacktraceType_m6818_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo StackTraceUtility_t1331_StackTraceUtility_ExtractStringFromException_m6819_ParameterInfos[] = 
{
	{"exception", 0, 134219573, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractStringFromException(System.Object)
extern const MethodInfo StackTraceUtility_ExtractStringFromException_m6819_MethodInfo = 
{
	"ExtractStringFromException"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStringFromException_m6819/* method */
	, &StackTraceUtility_t1331_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StackTraceUtility_t1331_StackTraceUtility_ExtractStringFromException_m6819_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_1_0_2;
extern const Il2CppType String_t_1_0_0;
extern const Il2CppType String_t_1_0_2;
static const ParameterInfo StackTraceUtility_t1331_StackTraceUtility_ExtractStringFromExceptionInternal_m6820_ParameterInfos[] = 
{
	{"exceptiono", 0, 134219574, 0, &Object_t_0_0_0},
	{"message", 1, 134219575, 0, &String_t_1_0_2},
	{"stackTrace", 2, 134219576, 0, &String_t_1_0_2},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StringU26_t1131_StringU26_t1131 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern const MethodInfo StackTraceUtility_ExtractStringFromExceptionInternal_m6820_MethodInfo = 
{
	"ExtractStringFromExceptionInternal"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStringFromExceptionInternal_m6820/* method */
	, &StackTraceUtility_t1331_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StringU26_t1131_StringU26_t1131/* invoker_method */
	, StackTraceUtility_t1331_StackTraceUtility_ExtractStringFromExceptionInternal_m6820_ParameterInfos/* parameters */
	, 804/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo StackTraceUtility_t1331_StackTraceUtility_PostprocessStacktrace_m6821_ParameterInfos[] = 
{
	{"oldString", 0, 134219577, 0, &String_t_0_0_0},
	{"stripEngineInternalInformation", 1, 134219578, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern const MethodInfo StackTraceUtility_PostprocessStacktrace_m6821_MethodInfo = 
{
	"PostprocessStacktrace"/* name */
	, (methodPointerType)&StackTraceUtility_PostprocessStacktrace_m6821/* method */
	, &StackTraceUtility_t1331_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t170/* invoker_method */
	, StackTraceUtility_t1331_StackTraceUtility_PostprocessStacktrace_m6821_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType StackTrace_t1381_0_0_0;
extern const Il2CppType StackTrace_t1381_0_0_0;
static const ParameterInfo StackTraceUtility_t1331_StackTraceUtility_ExtractFormattedStackTrace_m6822_ParameterInfos[] = 
{
	{"stackTrace", 0, 134219579, 0, &StackTrace_t1381_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern const MethodInfo StackTraceUtility_ExtractFormattedStackTrace_m6822_MethodInfo = 
{
	"ExtractFormattedStackTrace"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractFormattedStackTrace_m6822/* method */
	, &StackTraceUtility_t1331_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StackTraceUtility_t1331_StackTraceUtility_ExtractFormattedStackTrace_m6822_ParameterInfos/* parameters */
	, 805/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StackTraceUtility_t1331_MethodInfos[] =
{
	&StackTraceUtility__ctor_m6814_MethodInfo,
	&StackTraceUtility__cctor_m6815_MethodInfo,
	&StackTraceUtility_SetProjectFolder_m6816_MethodInfo,
	&StackTraceUtility_ExtractStackTrace_m6817_MethodInfo,
	&StackTraceUtility_IsSystemStacktraceType_m6818_MethodInfo,
	&StackTraceUtility_ExtractStringFromException_m6819_MethodInfo,
	&StackTraceUtility_ExtractStringFromExceptionInternal_m6820_MethodInfo,
	&StackTraceUtility_PostprocessStacktrace_m6821_MethodInfo,
	&StackTraceUtility_ExtractFormattedStackTrace_m6822_MethodInfo,
	NULL
};
static const Il2CppMethodReference StackTraceUtility_t1331_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool StackTraceUtility_t1331_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType StackTraceUtility_t1331_0_0_0;
extern const Il2CppType StackTraceUtility_t1331_1_0_0;
struct StackTraceUtility_t1331;
const Il2CppTypeDefinitionMetadata StackTraceUtility_t1331_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StackTraceUtility_t1331_VTable/* vtableMethods */
	, StackTraceUtility_t1331_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1219/* fieldStart */

};
TypeInfo StackTraceUtility_t1331_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StackTraceUtility"/* name */
	, "UnityEngine"/* namespaze */
	, StackTraceUtility_t1331_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StackTraceUtility_t1331_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StackTraceUtility_t1331_0_0_0/* byval_arg */
	, &StackTraceUtility_t1331_1_0_0/* this_arg */
	, &StackTraceUtility_t1331_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StackTraceUtility_t1331)/* instance_size */
	, sizeof (StackTraceUtility_t1331)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StackTraceUtility_t1331_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityException.h"
// Metadata Definition UnityEngine.UnityException
extern TypeInfo UnityException_t450_il2cpp_TypeInfo;
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityExceptionMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor()
extern const MethodInfo UnityException__ctor_m6823_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6823/* method */
	, &UnityException_t450_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo UnityException_t450_UnityException__ctor_m6824_ParameterInfos[] = 
{
	{"message", 0, 134219580, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern const MethodInfo UnityException__ctor_m6824_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6824/* method */
	, &UnityException_t450_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UnityException_t450_UnityException__ctor_m6824_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Exception_t140_0_0_0;
extern const Il2CppType Exception_t140_0_0_0;
static const ParameterInfo UnityException_t450_UnityException__ctor_m6825_ParameterInfos[] = 
{
	{"message", 0, 134219581, 0, &String_t_0_0_0},
	{"innerException", 1, 134219582, 0, &Exception_t140_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
extern const MethodInfo UnityException__ctor_m6825_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6825/* method */
	, &UnityException_t450_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, UnityException_t450_UnityException__ctor_m6825_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo UnityException_t450_UnityException__ctor_m6826_ParameterInfos[] = 
{
	{"info", 0, 134219583, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134219584, 0, &StreamingContext_t1383_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo UnityException__ctor_m6826_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6826/* method */
	, &UnityException_t450_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_StreamingContext_t1383/* invoker_method */
	, UnityException_t450_UnityException__ctor_m6826_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityException_t450_MethodInfos[] =
{
	&UnityException__ctor_m6823_MethodInfo,
	&UnityException__ctor_m6824_MethodInfo,
	&UnityException__ctor_m6825_MethodInfo,
	&UnityException__ctor_m6826_MethodInfo,
	NULL
};
extern const MethodInfo Exception_ToString_m7170_MethodInfo;
extern const MethodInfo Exception_GetObjectData_m7171_MethodInfo;
extern const MethodInfo Exception_get_InnerException_m7172_MethodInfo;
extern const MethodInfo Exception_get_Message_m7173_MethodInfo;
extern const MethodInfo Exception_get_Source_m7174_MethodInfo;
extern const MethodInfo Exception_get_StackTrace_m7175_MethodInfo;
extern const MethodInfo Exception_GetType_m7176_MethodInfo;
static const Il2CppMethodReference UnityException_t450_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Exception_ToString_m7170_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_get_InnerException_m7172_MethodInfo,
	&Exception_get_Message_m7173_MethodInfo,
	&Exception_get_Source_m7174_MethodInfo,
	&Exception_get_StackTrace_m7175_MethodInfo,
	&Exception_GetObjectData_m7171_MethodInfo,
	&Exception_GetType_m7176_MethodInfo,
};
static bool UnityException_t450_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Exception_t1473_0_0_0;
static Il2CppInterfaceOffsetPair UnityException_t450_InterfacesOffsets[] = 
{
	{ &ISerializable_t513_0_0_0, 4},
	{ &_Exception_t1473_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityException_t450_0_0_0;
extern const Il2CppType UnityException_t450_1_0_0;
struct UnityException_t450;
const Il2CppTypeDefinitionMetadata UnityException_t450_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityException_t450_InterfacesOffsets/* interfaceOffsets */
	, &Exception_t140_0_0_0/* parent */
	, UnityException_t450_VTable/* vtableMethods */
	, UnityException_t450_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1220/* fieldStart */

};
TypeInfo UnityException_t450_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityException"/* name */
	, "UnityEngine"/* namespaze */
	, UnityException_t450_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityException_t450_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityException_t450_0_0_0/* byval_arg */
	, &UnityException_t450_1_0_0/* this_arg */
	, &UnityException_t450_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityException_t450)/* instance_size */
	, sizeof (UnityException_t450)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttribute.h"
// Metadata Definition UnityEngine.SharedBetweenAnimatorsAttribute
extern TypeInfo SharedBetweenAnimatorsAttribute_t1332_il2cpp_TypeInfo;
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern const MethodInfo SharedBetweenAnimatorsAttribute__ctor_m6827_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SharedBetweenAnimatorsAttribute__ctor_m6827/* method */
	, &SharedBetweenAnimatorsAttribute_t1332_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SharedBetweenAnimatorsAttribute_t1332_MethodInfos[] =
{
	&SharedBetweenAnimatorsAttribute__ctor_m6827_MethodInfo,
	NULL
};
static const Il2CppMethodReference SharedBetweenAnimatorsAttribute_t1332_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool SharedBetweenAnimatorsAttribute_t1332_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SharedBetweenAnimatorsAttribute_t1332_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType SharedBetweenAnimatorsAttribute_t1332_0_0_0;
extern const Il2CppType SharedBetweenAnimatorsAttribute_t1332_1_0_0;
struct SharedBetweenAnimatorsAttribute_t1332;
const Il2CppTypeDefinitionMetadata SharedBetweenAnimatorsAttribute_t1332_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SharedBetweenAnimatorsAttribute_t1332_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, SharedBetweenAnimatorsAttribute_t1332_VTable/* vtableMethods */
	, SharedBetweenAnimatorsAttribute_t1332_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SharedBetweenAnimatorsAttribute_t1332_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SharedBetweenAnimatorsAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SharedBetweenAnimatorsAttribute_t1332_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SharedBetweenAnimatorsAttribute_t1332_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 806/* custom_attributes_cache */
	, &SharedBetweenAnimatorsAttribute_t1332_0_0_0/* byval_arg */
	, &SharedBetweenAnimatorsAttribute_t1332_1_0_0/* this_arg */
	, &SharedBetweenAnimatorsAttribute_t1332_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SharedBetweenAnimatorsAttribute_t1332)/* instance_size */
	, sizeof (SharedBetweenAnimatorsAttribute_t1332)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviour.h"
// Metadata Definition UnityEngine.StateMachineBehaviour
extern TypeInfo StateMachineBehaviour_t1333_il2cpp_TypeInfo;
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviourMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
extern const MethodInfo StateMachineBehaviour__ctor_m6828_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StateMachineBehaviour__ctor_m6828/* method */
	, &StateMachineBehaviour_t1333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t415_0_0_0;
extern const Il2CppType Animator_t415_0_0_0;
extern const Il2CppType AnimatorStateInfo_t1234_0_0_0;
extern const Il2CppType AnimatorStateInfo_t1234_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo StateMachineBehaviour_t1333_StateMachineBehaviour_OnStateEnter_m6829_ParameterInfos[] = 
{
	{"animator", 0, 134219585, 0, &Animator_t415_0_0_0},
	{"stateInfo", 1, 134219586, 0, &AnimatorStateInfo_t1234_0_0_0},
	{"layerIndex", 2, 134219587, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_AnimatorStateInfo_t1234_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateEnter_m6829_MethodInfo = 
{
	"OnStateEnter"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateEnter_m6829/* method */
	, &StateMachineBehaviour_t1333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_AnimatorStateInfo_t1234_Int32_t127/* invoker_method */
	, StateMachineBehaviour_t1333_StateMachineBehaviour_OnStateEnter_m6829_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t415_0_0_0;
extern const Il2CppType AnimatorStateInfo_t1234_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo StateMachineBehaviour_t1333_StateMachineBehaviour_OnStateUpdate_m6830_ParameterInfos[] = 
{
	{"animator", 0, 134219588, 0, &Animator_t415_0_0_0},
	{"stateInfo", 1, 134219589, 0, &AnimatorStateInfo_t1234_0_0_0},
	{"layerIndex", 2, 134219590, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_AnimatorStateInfo_t1234_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateUpdate_m6830_MethodInfo = 
{
	"OnStateUpdate"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateUpdate_m6830/* method */
	, &StateMachineBehaviour_t1333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_AnimatorStateInfo_t1234_Int32_t127/* invoker_method */
	, StateMachineBehaviour_t1333_StateMachineBehaviour_OnStateUpdate_m6830_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t415_0_0_0;
extern const Il2CppType AnimatorStateInfo_t1234_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo StateMachineBehaviour_t1333_StateMachineBehaviour_OnStateExit_m6831_ParameterInfos[] = 
{
	{"animator", 0, 134219591, 0, &Animator_t415_0_0_0},
	{"stateInfo", 1, 134219592, 0, &AnimatorStateInfo_t1234_0_0_0},
	{"layerIndex", 2, 134219593, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_AnimatorStateInfo_t1234_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateExit_m6831_MethodInfo = 
{
	"OnStateExit"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateExit_m6831/* method */
	, &StateMachineBehaviour_t1333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_AnimatorStateInfo_t1234_Int32_t127/* invoker_method */
	, StateMachineBehaviour_t1333_StateMachineBehaviour_OnStateExit_m6831_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1779/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t415_0_0_0;
extern const Il2CppType AnimatorStateInfo_t1234_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo StateMachineBehaviour_t1333_StateMachineBehaviour_OnStateMove_m6832_ParameterInfos[] = 
{
	{"animator", 0, 134219594, 0, &Animator_t415_0_0_0},
	{"stateInfo", 1, 134219595, 0, &AnimatorStateInfo_t1234_0_0_0},
	{"layerIndex", 2, 134219596, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_AnimatorStateInfo_t1234_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateMove_m6832_MethodInfo = 
{
	"OnStateMove"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMove_m6832/* method */
	, &StateMachineBehaviour_t1333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_AnimatorStateInfo_t1234_Int32_t127/* invoker_method */
	, StateMachineBehaviour_t1333_StateMachineBehaviour_OnStateMove_m6832_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1780/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t415_0_0_0;
extern const Il2CppType AnimatorStateInfo_t1234_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo StateMachineBehaviour_t1333_StateMachineBehaviour_OnStateIK_m6833_ParameterInfos[] = 
{
	{"animator", 0, 134219597, 0, &Animator_t415_0_0_0},
	{"stateInfo", 1, 134219598, 0, &AnimatorStateInfo_t1234_0_0_0},
	{"layerIndex", 2, 134219599, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_AnimatorStateInfo_t1234_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateIK_m6833_MethodInfo = 
{
	"OnStateIK"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateIK_m6833/* method */
	, &StateMachineBehaviour_t1333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_AnimatorStateInfo_t1234_Int32_t127/* invoker_method */
	, StateMachineBehaviour_t1333_StateMachineBehaviour_OnStateIK_m6833_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1781/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t415_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo StateMachineBehaviour_t1333_StateMachineBehaviour_OnStateMachineEnter_m6834_ParameterInfos[] = 
{
	{"animator", 0, 134219600, 0, &Animator_t415_0_0_0},
	{"stateMachinePathHash", 1, 134219601, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateMachineEnter_m6834_MethodInfo = 
{
	"OnStateMachineEnter"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMachineEnter_m6834/* method */
	, &StateMachineBehaviour_t1333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127/* invoker_method */
	, StateMachineBehaviour_t1333_StateMachineBehaviour_OnStateMachineEnter_m6834_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1782/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t415_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo StateMachineBehaviour_t1333_StateMachineBehaviour_OnStateMachineExit_m6835_ParameterInfos[] = 
{
	{"animator", 0, 134219602, 0, &Animator_t415_0_0_0},
	{"stateMachinePathHash", 1, 134219603, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
extern const MethodInfo StateMachineBehaviour_OnStateMachineExit_m6835_MethodInfo = 
{
	"OnStateMachineExit"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMachineExit_m6835/* method */
	, &StateMachineBehaviour_t1333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127/* invoker_method */
	, StateMachineBehaviour_t1333_StateMachineBehaviour_OnStateMachineExit_m6835_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1783/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StateMachineBehaviour_t1333_MethodInfos[] =
{
	&StateMachineBehaviour__ctor_m6828_MethodInfo,
	&StateMachineBehaviour_OnStateEnter_m6829_MethodInfo,
	&StateMachineBehaviour_OnStateUpdate_m6830_MethodInfo,
	&StateMachineBehaviour_OnStateExit_m6831_MethodInfo,
	&StateMachineBehaviour_OnStateMove_m6832_MethodInfo,
	&StateMachineBehaviour_OnStateIK_m6833_MethodInfo,
	&StateMachineBehaviour_OnStateMachineEnter_m6834_MethodInfo,
	&StateMachineBehaviour_OnStateMachineExit_m6835_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m537_MethodInfo;
extern const MethodInfo Object_GetHashCode_m538_MethodInfo;
extern const MethodInfo Object_ToString_m539_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateEnter_m6829_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateUpdate_m6830_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateExit_m6831_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateMove_m6832_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateIK_m6833_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateMachineEnter_m6834_MethodInfo;
extern const MethodInfo StateMachineBehaviour_OnStateMachineExit_m6835_MethodInfo;
static const Il2CppMethodReference StateMachineBehaviour_t1333_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&StateMachineBehaviour_OnStateEnter_m6829_MethodInfo,
	&StateMachineBehaviour_OnStateUpdate_m6830_MethodInfo,
	&StateMachineBehaviour_OnStateExit_m6831_MethodInfo,
	&StateMachineBehaviour_OnStateMove_m6832_MethodInfo,
	&StateMachineBehaviour_OnStateIK_m6833_MethodInfo,
	&StateMachineBehaviour_OnStateMachineEnter_m6834_MethodInfo,
	&StateMachineBehaviour_OnStateMachineExit_m6835_MethodInfo,
};
static bool StateMachineBehaviour_t1333_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType StateMachineBehaviour_t1333_0_0_0;
extern const Il2CppType StateMachineBehaviour_t1333_1_0_0;
extern const Il2CppType ScriptableObject_t955_0_0_0;
struct StateMachineBehaviour_t1333;
const Il2CppTypeDefinitionMetadata StateMachineBehaviour_t1333_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ScriptableObject_t955_0_0_0/* parent */
	, StateMachineBehaviour_t1333_VTable/* vtableMethods */
	, StateMachineBehaviour_t1333_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo StateMachineBehaviour_t1333_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StateMachineBehaviour"/* name */
	, "UnityEngine"/* namespaze */
	, StateMachineBehaviour_t1333_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StateMachineBehaviour_t1333_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StateMachineBehaviour_t1333_0_0_0/* byval_arg */
	, &StateMachineBehaviour_t1333_1_0_0/* this_arg */
	, &StateMachineBehaviour_t1333_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StateMachineBehaviour_t1333)/* instance_size */
	, sizeof (StateMachineBehaviour_t1333)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
// Metadata Definition UnityEngine.TextEditor/DblClickSnapping
extern TypeInfo DblClickSnapping_t1334_il2cpp_TypeInfo;
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappingMethodDeclarations.h"
static const MethodInfo* DblClickSnapping_t1334_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference DblClickSnapping_t1334_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool DblClickSnapping_t1334_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DblClickSnapping_t1334_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DblClickSnapping_t1334_0_0_0;
extern const Il2CppType DblClickSnapping_t1334_1_0_0;
extern TypeInfo TextEditor_t455_il2cpp_TypeInfo;
extern const Il2CppType TextEditor_t455_0_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t449_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata DblClickSnapping_t1334_DefinitionMetadata = 
{
	&TextEditor_t455_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DblClickSnapping_t1334_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, DblClickSnapping_t1334_VTable/* vtableMethods */
	, DblClickSnapping_t1334_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1222/* fieldStart */

};
TypeInfo DblClickSnapping_t1334_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DblClickSnapping"/* name */
	, ""/* namespaze */
	, DblClickSnapping_t1334_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Byte_t449_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &DblClickSnapping_t1334_0_0_0/* byval_arg */
	, &DblClickSnapping_t1334_1_0_0/* this_arg */
	, &DblClickSnapping_t1334_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DblClickSnapping_t1334)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (DblClickSnapping_t1334)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
// Metadata Definition UnityEngine.TextEditor/TextEditOp
extern TypeInfo TextEditOp_t1335_il2cpp_TypeInfo;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOpMethodDeclarations.h"
static const MethodInfo* TextEditOp_t1335_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TextEditOp_t1335_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool TextEditOp_t1335_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TextEditOp_t1335_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextEditOp_t1335_0_0_0;
extern const Il2CppType TextEditOp_t1335_1_0_0;
const Il2CppTypeDefinitionMetadata TextEditOp_t1335_DefinitionMetadata = 
{
	&TextEditor_t455_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TextEditOp_t1335_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, TextEditOp_t1335_VTable/* vtableMethods */
	, TextEditOp_t1335_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1225/* fieldStart */

};
TypeInfo TextEditOp_t1335_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditOp"/* name */
	, ""/* namespaze */
	, TextEditOp_t1335_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextEditOp_t1335_0_0_0/* byval_arg */
	, &TextEditOp_t1335_1_0_0/* this_arg */
	, &TextEditOp_t1335_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditOp_t1335)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextEditOp_t1335)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 51/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditor.h"
// Metadata Definition UnityEngine.TextEditor
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::.ctor()
extern const MethodInfo TextEditor__ctor_m2211_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextEditor__ctor_m2211/* method */
	, &TextEditor_t455_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1784/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ClearCursorPos()
extern const MethodInfo TextEditor_ClearCursorPos_m6836_MethodInfo = 
{
	"ClearCursorPos"/* name */
	, (methodPointerType)&TextEditor_ClearCursorPos_m6836/* method */
	, &TextEditor_t455_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1785/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::OnFocus()
extern const MethodInfo TextEditor_OnFocus_m2215_MethodInfo = 
{
	"OnFocus"/* name */
	, (methodPointerType)&TextEditor_OnFocus_m2215/* method */
	, &TextEditor_t455_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1786/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::SelectAll()
extern const MethodInfo TextEditor_SelectAll_m6837_MethodInfo = 
{
	"SelectAll"/* name */
	, (methodPointerType)&TextEditor_SelectAll_m6837/* method */
	, &TextEditor_t455_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1787/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
extern const MethodInfo TextEditor_DeleteSelection_m6838_MethodInfo = 
{
	"DeleteSelection"/* name */
	, (methodPointerType)&TextEditor_DeleteSelection_m6838/* method */
	, &TextEditor_t455_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1788/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TextEditor_t455_TextEditor_ReplaceSelection_m6839_ParameterInfos[] = 
{
	{"replace", 0, 134219604, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
extern const MethodInfo TextEditor_ReplaceSelection_m6839_MethodInfo = 
{
	"ReplaceSelection"/* name */
	, (methodPointerType)&TextEditor_ReplaceSelection_m6839/* method */
	, &TextEditor_t455_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, TextEditor_t455_TextEditor_ReplaceSelection_m6839_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1789/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
extern const MethodInfo TextEditor_UpdateScrollOffset_m6840_MethodInfo = 
{
	"UpdateScrollOffset"/* name */
	, (methodPointerType)&TextEditor_UpdateScrollOffset_m6840/* method */
	, &TextEditor_t455_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1790/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::Copy()
extern const MethodInfo TextEditor_Copy_m2216_MethodInfo = 
{
	"Copy"/* name */
	, (methodPointerType)&TextEditor_Copy_m2216/* method */
	, &TextEditor_t455_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1791/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TextEditor_t455_TextEditor_ReplaceNewlinesWithSpaces_m6841_ParameterInfos[] = 
{
	{"value", 0, 134219605, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
extern const MethodInfo TextEditor_ReplaceNewlinesWithSpaces_m6841_MethodInfo = 
{
	"ReplaceNewlinesWithSpaces"/* name */
	, (methodPointerType)&TextEditor_ReplaceNewlinesWithSpaces_m6841/* method */
	, &TextEditor_t455_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, TextEditor_t455_TextEditor_ReplaceNewlinesWithSpaces_m6841_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1792/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextEditor::Paste()
extern const MethodInfo TextEditor_Paste_m2212_MethodInfo = 
{
	"Paste"/* name */
	, (methodPointerType)&TextEditor_Paste_m2212/* method */
	, &TextEditor_t455_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1793/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextEditor_t455_MethodInfos[] =
{
	&TextEditor__ctor_m2211_MethodInfo,
	&TextEditor_ClearCursorPos_m6836_MethodInfo,
	&TextEditor_OnFocus_m2215_MethodInfo,
	&TextEditor_SelectAll_m6837_MethodInfo,
	&TextEditor_DeleteSelection_m6838_MethodInfo,
	&TextEditor_ReplaceSelection_m6839_MethodInfo,
	&TextEditor_UpdateScrollOffset_m6840_MethodInfo,
	&TextEditor_Copy_m2216_MethodInfo,
	&TextEditor_ReplaceNewlinesWithSpaces_m6841_MethodInfo,
	&TextEditor_Paste_m2212_MethodInfo,
	NULL
};
static const Il2CppType* TextEditor_t455_il2cpp_TypeInfo__nestedTypes[2] =
{
	&DblClickSnapping_t1334_0_0_0,
	&TextEditOp_t1335_0_0_0,
};
static const Il2CppMethodReference TextEditor_t455_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool TextEditor_t455_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextEditor_t455_1_0_0;
struct TextEditor_t455;
const Il2CppTypeDefinitionMetadata TextEditor_t455_DefinitionMetadata = 
{
	NULL/* declaringType */
	, TextEditor_t455_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TextEditor_t455_VTable/* vtableMethods */
	, TextEditor_t455_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1276/* fieldStart */

};
TypeInfo TextEditor_t455_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditor"/* name */
	, "UnityEngine"/* namespaze */
	, TextEditor_t455_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TextEditor_t455_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextEditor_t455_0_0_0/* byval_arg */
	, &TextEditor_t455_1_0_0/* this_arg */
	, &TextEditor_t455_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditor_t455)/* instance_size */
	, sizeof (TextEditor_t455)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TextEditor_t455_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
// Metadata Definition UnityEngine.TextGenerationSettings
extern TypeInfo TextGenerationSettings_t416_il2cpp_TypeInfo;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
extern const Il2CppType Color_t90_0_0_0;
extern const Il2CppType Color_t90_0_0_0;
extern const Il2CppType Color_t90_0_0_0;
static const ParameterInfo TextGenerationSettings_t416_TextGenerationSettings_CompareColors_m6842_ParameterInfos[] = 
{
	{"left", 0, 134219606, 0, &Color_t90_0_0_0},
	{"right", 1, 134219607, 0, &Color_t90_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Color_t90_Color_t90 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
extern const MethodInfo TextGenerationSettings_CompareColors_m6842_MethodInfo = 
{
	"CompareColors"/* name */
	, (methodPointerType)&TextGenerationSettings_CompareColors_m6842/* method */
	, &TextGenerationSettings_t416_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Color_t90_Color_t90/* invoker_method */
	, TextGenerationSettings_t416_TextGenerationSettings_CompareColors_m6842_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1794/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
extern const Il2CppType Vector2_t10_0_0_0;
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo TextGenerationSettings_t416_TextGenerationSettings_CompareVector2_m6843_ParameterInfos[] = 
{
	{"left", 0, 134219608, 0, &Vector2_t10_0_0_0},
	{"right", 1, 134219609, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Vector2_t10_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
extern const MethodInfo TextGenerationSettings_CompareVector2_m6843_MethodInfo = 
{
	"CompareVector2"/* name */
	, (methodPointerType)&TextGenerationSettings_CompareVector2_m6843/* method */
	, &TextGenerationSettings_t416_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Vector2_t10_Vector2_t10/* invoker_method */
	, TextGenerationSettings_t416_TextGenerationSettings_CompareVector2_m6843_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1795/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextGenerationSettings_t416_0_0_0;
extern const Il2CppType TextGenerationSettings_t416_0_0_0;
static const ParameterInfo TextGenerationSettings_t416_TextGenerationSettings_Equals_m6844_ParameterInfos[] = 
{
	{"other", 0, 134219610, 0, &TextGenerationSettings_t416_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_TextGenerationSettings_t416 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
extern const MethodInfo TextGenerationSettings_Equals_m6844_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TextGenerationSettings_Equals_m6844/* method */
	, &TextGenerationSettings_t416_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_TextGenerationSettings_t416/* invoker_method */
	, TextGenerationSettings_t416_TextGenerationSettings_Equals_m6844_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1796/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TextGenerationSettings_t416_MethodInfos[] =
{
	&TextGenerationSettings_CompareColors_m6842_MethodInfo,
	&TextGenerationSettings_CompareVector2_m6843_MethodInfo,
	&TextGenerationSettings_Equals_m6844_MethodInfo,
	NULL
};
static const Il2CppMethodReference TextGenerationSettings_t416_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool TextGenerationSettings_t416_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TextGenerationSettings_t416_1_0_0;
const Il2CppTypeDefinitionMetadata TextGenerationSettings_t416_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, TextGenerationSettings_t416_VTable/* vtableMethods */
	, TextGenerationSettings_t416_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1300/* fieldStart */

};
TypeInfo TextGenerationSettings_t416_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextGenerationSettings"/* name */
	, "UnityEngine"/* namespaze */
	, TextGenerationSettings_t416_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TextGenerationSettings_t416_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TextGenerationSettings_t416_0_0_0/* byval_arg */
	, &TextGenerationSettings_t416_1_0_0/* this_arg */
	, &TextGenerationSettings_t416_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextGenerationSettings_t416)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TextGenerationSettings_t416)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 17/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReference.h"
// Metadata Definition UnityEngine.TrackedReference
extern TypeInfo TrackedReference_t1238_il2cpp_TypeInfo;
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReferenceMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo TrackedReference_t1238_TrackedReference_Equals_m6845_ParameterInfos[] = 
{
	{"o", 0, 134219611, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern const MethodInfo TrackedReference_Equals_m6845_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TrackedReference_Equals_m6845/* method */
	, &TrackedReference_t1238_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, TrackedReference_t1238_TrackedReference_Equals_m6845_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1797/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern const MethodInfo TrackedReference_GetHashCode_m6846_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&TrackedReference_GetHashCode_m6846/* method */
	, &TrackedReference_t1238_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1798/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TrackedReference_t1238_0_0_0;
extern const Il2CppType TrackedReference_t1238_0_0_0;
extern const Il2CppType TrackedReference_t1238_0_0_0;
static const ParameterInfo TrackedReference_t1238_TrackedReference_op_Equality_m6847_ParameterInfos[] = 
{
	{"x", 0, 134219612, 0, &TrackedReference_t1238_0_0_0},
	{"y", 1, 134219613, 0, &TrackedReference_t1238_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern const MethodInfo TrackedReference_op_Equality_m6847_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&TrackedReference_op_Equality_m6847/* method */
	, &TrackedReference_t1238_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, TrackedReference_t1238_TrackedReference_op_Equality_m6847_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1799/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TrackedReference_t1238_MethodInfos[] =
{
	&TrackedReference_Equals_m6845_MethodInfo,
	&TrackedReference_GetHashCode_m6846_MethodInfo,
	&TrackedReference_op_Equality_m6847_MethodInfo,
	NULL
};
extern const MethodInfo TrackedReference_Equals_m6845_MethodInfo;
extern const MethodInfo TrackedReference_GetHashCode_m6846_MethodInfo;
static const Il2CppMethodReference TrackedReference_t1238_VTable[] =
{
	&TrackedReference_Equals_m6845_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&TrackedReference_GetHashCode_m6846_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool TrackedReference_t1238_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TrackedReference_t1238_1_0_0;
struct TrackedReference_t1238;
const Il2CppTypeDefinitionMetadata TrackedReference_t1238_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, TrackedReference_t1238_VTable/* vtableMethods */
	, TrackedReference_t1238_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1317/* fieldStart */

};
TypeInfo TrackedReference_t1238_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackedReference"/* name */
	, "UnityEngine"/* namespaze */
	, TrackedReference_t1238_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TrackedReference_t1238_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TrackedReference_t1238_0_0_0/* byval_arg */
	, &TrackedReference_t1238_1_0_0/* this_arg */
	, &TrackedReference_t1238_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)TrackedReference_t1238_marshal/* marshal_to_native_func */
	, (methodPointerType)TrackedReference_t1238_marshal_back/* marshal_from_native_func */
	, (methodPointerType)TrackedReference_t1238_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (TrackedReference_t1238)/* instance_size */
	, sizeof (TrackedReference_t1238)/* actualSize */
	, 0/* element_size */
	, sizeof(TrackedReference_t1238_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
// Metadata Definition UnityEngine.Events.PersistentListenerMode
extern TypeInfo PersistentListenerMode_t1337_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerModeMethodDeclarations.h"
static const MethodInfo* PersistentListenerMode_t1337_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference PersistentListenerMode_t1337_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool PersistentListenerMode_t1337_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PersistentListenerMode_t1337_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentListenerMode_t1337_0_0_0;
extern const Il2CppType PersistentListenerMode_t1337_1_0_0;
const Il2CppTypeDefinitionMetadata PersistentListenerMode_t1337_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PersistentListenerMode_t1337_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, PersistentListenerMode_t1337_VTable/* vtableMethods */
	, PersistentListenerMode_t1337_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1318/* fieldStart */

};
TypeInfo PersistentListenerMode_t1337_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentListenerMode"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentListenerMode_t1337_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentListenerMode_t1337_0_0_0/* byval_arg */
	, &PersistentListenerMode_t1337_1_0_0/* this_arg */
	, &PersistentListenerMode_t1337_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentListenerMode_t1337)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (PersistentListenerMode_t1337)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCache.h"
// Metadata Definition UnityEngine.Events.ArgumentCache
extern TypeInfo ArgumentCache_t1338_il2cpp_TypeInfo;
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCacheMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern const MethodInfo ArgumentCache__ctor_m6848_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArgumentCache__ctor_m6848/* method */
	, &ArgumentCache_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1800/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t123_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern const MethodInfo ArgumentCache_get_unityObjectArgument_m6849_MethodInfo = 
{
	"get_unityObjectArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_unityObjectArgument_m6849/* method */
	, &ArgumentCache_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Object_t123_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1801/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern const MethodInfo ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6850_MethodInfo = 
{
	"get_unityObjectArgumentAssemblyTypeName"/* name */
	, (methodPointerType)&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6850/* method */
	, &ArgumentCache_t1338_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1802/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern const MethodInfo ArgumentCache_get_intArgument_m6851_MethodInfo = 
{
	"get_intArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_intArgument_m6851/* method */
	, &ArgumentCache_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1803/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern const MethodInfo ArgumentCache_get_floatArgument_m6852_MethodInfo = 
{
	"get_floatArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_floatArgument_m6852/* method */
	, &ArgumentCache_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1804/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern const MethodInfo ArgumentCache_get_stringArgument_m6853_MethodInfo = 
{
	"get_stringArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_stringArgument_m6853/* method */
	, &ArgumentCache_t1338_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1805/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern const MethodInfo ArgumentCache_get_boolArgument_m6854_MethodInfo = 
{
	"get_boolArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_boolArgument_m6854/* method */
	, &ArgumentCache_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1806/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern const MethodInfo ArgumentCache_TidyAssemblyTypeName_m6855_MethodInfo = 
{
	"TidyAssemblyTypeName"/* name */
	, (methodPointerType)&ArgumentCache_TidyAssemblyTypeName_m6855/* method */
	, &ArgumentCache_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1807/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern const MethodInfo ArgumentCache_OnBeforeSerialize_m6856_MethodInfo = 
{
	"OnBeforeSerialize"/* name */
	, (methodPointerType)&ArgumentCache_OnBeforeSerialize_m6856/* method */
	, &ArgumentCache_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1808/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern const MethodInfo ArgumentCache_OnAfterDeserialize_m6857_MethodInfo = 
{
	"OnAfterDeserialize"/* name */
	, (methodPointerType)&ArgumentCache_OnAfterDeserialize_m6857/* method */
	, &ArgumentCache_t1338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1809/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ArgumentCache_t1338_MethodInfos[] =
{
	&ArgumentCache__ctor_m6848_MethodInfo,
	&ArgumentCache_get_unityObjectArgument_m6849_MethodInfo,
	&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6850_MethodInfo,
	&ArgumentCache_get_intArgument_m6851_MethodInfo,
	&ArgumentCache_get_floatArgument_m6852_MethodInfo,
	&ArgumentCache_get_stringArgument_m6853_MethodInfo,
	&ArgumentCache_get_boolArgument_m6854_MethodInfo,
	&ArgumentCache_TidyAssemblyTypeName_m6855_MethodInfo,
	&ArgumentCache_OnBeforeSerialize_m6856_MethodInfo,
	&ArgumentCache_OnAfterDeserialize_m6857_MethodInfo,
	NULL
};
extern const MethodInfo ArgumentCache_get_unityObjectArgument_m6849_MethodInfo;
static const PropertyInfo ArgumentCache_t1338____unityObjectArgument_PropertyInfo = 
{
	&ArgumentCache_t1338_il2cpp_TypeInfo/* parent */
	, "unityObjectArgument"/* name */
	, &ArgumentCache_get_unityObjectArgument_m6849_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6850_MethodInfo;
static const PropertyInfo ArgumentCache_t1338____unityObjectArgumentAssemblyTypeName_PropertyInfo = 
{
	&ArgumentCache_t1338_il2cpp_TypeInfo/* parent */
	, "unityObjectArgumentAssemblyTypeName"/* name */
	, &ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6850_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_intArgument_m6851_MethodInfo;
static const PropertyInfo ArgumentCache_t1338____intArgument_PropertyInfo = 
{
	&ArgumentCache_t1338_il2cpp_TypeInfo/* parent */
	, "intArgument"/* name */
	, &ArgumentCache_get_intArgument_m6851_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_floatArgument_m6852_MethodInfo;
static const PropertyInfo ArgumentCache_t1338____floatArgument_PropertyInfo = 
{
	&ArgumentCache_t1338_il2cpp_TypeInfo/* parent */
	, "floatArgument"/* name */
	, &ArgumentCache_get_floatArgument_m6852_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_stringArgument_m6853_MethodInfo;
static const PropertyInfo ArgumentCache_t1338____stringArgument_PropertyInfo = 
{
	&ArgumentCache_t1338_il2cpp_TypeInfo/* parent */
	, "stringArgument"/* name */
	, &ArgumentCache_get_stringArgument_m6853_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ArgumentCache_get_boolArgument_m6854_MethodInfo;
static const PropertyInfo ArgumentCache_t1338____boolArgument_PropertyInfo = 
{
	&ArgumentCache_t1338_il2cpp_TypeInfo/* parent */
	, "boolArgument"/* name */
	, &ArgumentCache_get_boolArgument_m6854_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ArgumentCache_t1338_PropertyInfos[] =
{
	&ArgumentCache_t1338____unityObjectArgument_PropertyInfo,
	&ArgumentCache_t1338____unityObjectArgumentAssemblyTypeName_PropertyInfo,
	&ArgumentCache_t1338____intArgument_PropertyInfo,
	&ArgumentCache_t1338____floatArgument_PropertyInfo,
	&ArgumentCache_t1338____stringArgument_PropertyInfo,
	&ArgumentCache_t1338____boolArgument_PropertyInfo,
	NULL
};
extern const MethodInfo ArgumentCache_OnBeforeSerialize_m6856_MethodInfo;
extern const MethodInfo ArgumentCache_OnAfterDeserialize_m6857_MethodInfo;
static const Il2CppMethodReference ArgumentCache_t1338_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&ArgumentCache_OnBeforeSerialize_m6856_MethodInfo,
	&ArgumentCache_OnAfterDeserialize_m6857_MethodInfo,
};
static bool ArgumentCache_t1338_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ISerializationCallbackReceiver_t510_0_0_0;
static const Il2CppType* ArgumentCache_t1338_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t510_0_0_0,
};
static Il2CppInterfaceOffsetPair ArgumentCache_t1338_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t510_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ArgumentCache_t1338_0_0_0;
extern const Il2CppType ArgumentCache_t1338_1_0_0;
struct ArgumentCache_t1338;
const Il2CppTypeDefinitionMetadata ArgumentCache_t1338_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ArgumentCache_t1338_InterfacesTypeInfos/* implementedInterfaces */
	, ArgumentCache_t1338_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ArgumentCache_t1338_VTable/* vtableMethods */
	, ArgumentCache_t1338_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1326/* fieldStart */

};
TypeInfo ArgumentCache_t1338_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentCache"/* name */
	, "UnityEngine.Events"/* namespaze */
	, ArgumentCache_t1338_MethodInfos/* methods */
	, ArgumentCache_t1338_PropertyInfos/* properties */
	, NULL/* events */
	, &ArgumentCache_t1338_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ArgumentCache_t1338_0_0_0/* byval_arg */
	, &ArgumentCache_t1338_1_0_0/* this_arg */
	, &ArgumentCache_t1338_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentCache_t1338)/* instance_size */
	, sizeof (ArgumentCache_t1338)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// Metadata Definition UnityEngine.Events.BaseInvokableCall
extern TypeInfo BaseInvokableCall_t1339_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern const MethodInfo BaseInvokableCall__ctor_m6858_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseInvokableCall__ctor_m6858/* method */
	, &BaseInvokableCall_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1810/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo BaseInvokableCall_t1339_BaseInvokableCall__ctor_m6859_ParameterInfos[] = 
{
	{"target", 0, 134219614, 0, &Object_t_0_0_0},
	{"function", 1, 134219615, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo BaseInvokableCall__ctor_m6859_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseInvokableCall__ctor_m6859/* method */
	, &BaseInvokableCall_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, BaseInvokableCall_t1339_BaseInvokableCall__ctor_m6859_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1811/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo BaseInvokableCall_t1339_BaseInvokableCall_Invoke_m7118_ParameterInfos[] = 
{
	{"args", 0, 134219616, 0, &ObjectU5BU5D_t115_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[])
extern const MethodInfo BaseInvokableCall_Invoke_m7118_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, BaseInvokableCall_t1339_BaseInvokableCall_Invoke_m7118_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1812/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo BaseInvokableCall_t1339_BaseInvokableCall_ThrowOnInvalidArg_m7119_ParameterInfos[] = 
{
	{"arg", 0, 134219617, 0, &Object_t_0_0_0},
};
extern const Il2CppGenericContainer BaseInvokableCall_ThrowOnInvalidArg_m7119_Il2CppGenericContainer;
extern TypeInfo BaseInvokableCall_ThrowOnInvalidArg_m7119_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter BaseInvokableCall_ThrowOnInvalidArg_m7119_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &BaseInvokableCall_ThrowOnInvalidArg_m7119_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* BaseInvokableCall_ThrowOnInvalidArg_m7119_Il2CppGenericParametersArray[1] = 
{
	&BaseInvokableCall_ThrowOnInvalidArg_m7119_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo BaseInvokableCall_ThrowOnInvalidArg_m7119_MethodInfo;
extern const Il2CppGenericContainer BaseInvokableCall_ThrowOnInvalidArg_m7119_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&BaseInvokableCall_ThrowOnInvalidArg_m7119_MethodInfo, 1, 1, BaseInvokableCall_ThrowOnInvalidArg_m7119_Il2CppGenericParametersArray };
extern const Il2CppType BaseInvokableCall_ThrowOnInvalidArg_m7119_gp_0_0_0_0;
static Il2CppRGCTXDefinition BaseInvokableCall_ThrowOnInvalidArg_m7119_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&BaseInvokableCall_ThrowOnInvalidArg_m7119_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&BaseInvokableCall_ThrowOnInvalidArg_m7119_gp_0_0_0_0 }/* Type */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg(System.Object)
extern const MethodInfo BaseInvokableCall_ThrowOnInvalidArg_m7119_MethodInfo = 
{
	"ThrowOnInvalidArg"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, BaseInvokableCall_t1339_BaseInvokableCall_ThrowOnInvalidArg_m7119_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 1813/* token */
	, BaseInvokableCall_ThrowOnInvalidArg_m7119_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &BaseInvokableCall_ThrowOnInvalidArg_m7119_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType Delegate_t143_0_0_0;
extern const Il2CppType Delegate_t143_0_0_0;
static const ParameterInfo BaseInvokableCall_t1339_BaseInvokableCall_AllowInvoke_m6860_ParameterInfos[] = 
{
	{"delegate", 0, 134219618, 0, &Delegate_t143_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern const MethodInfo BaseInvokableCall_AllowInvoke_m6860_MethodInfo = 
{
	"AllowInvoke"/* name */
	, (methodPointerType)&BaseInvokableCall_AllowInvoke_m6860/* method */
	, &BaseInvokableCall_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, BaseInvokableCall_t1339_BaseInvokableCall_AllowInvoke_m6860_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1814/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo BaseInvokableCall_t1339_BaseInvokableCall_Find_m7120_ParameterInfos[] = 
{
	{"targetObj", 0, 134219619, 0, &Object_t_0_0_0},
	{"method", 1, 134219620, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo BaseInvokableCall_Find_m7120_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1339_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, BaseInvokableCall_t1339_BaseInvokableCall_Find_m7120_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1815/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BaseInvokableCall_t1339_MethodInfos[] =
{
	&BaseInvokableCall__ctor_m6858_MethodInfo,
	&BaseInvokableCall__ctor_m6859_MethodInfo,
	&BaseInvokableCall_Invoke_m7118_MethodInfo,
	&BaseInvokableCall_ThrowOnInvalidArg_m7119_MethodInfo,
	&BaseInvokableCall_AllowInvoke_m6860_MethodInfo,
	&BaseInvokableCall_Find_m7120_MethodInfo,
	NULL
};
static const Il2CppMethodReference BaseInvokableCall_t1339_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	NULL,
	NULL,
};
static bool BaseInvokableCall_t1339_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType BaseInvokableCall_t1339_0_0_0;
extern const Il2CppType BaseInvokableCall_t1339_1_0_0;
struct BaseInvokableCall_t1339;
const Il2CppTypeDefinitionMetadata BaseInvokableCall_t1339_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, BaseInvokableCall_t1339_VTable/* vtableMethods */
	, BaseInvokableCall_t1339_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo BaseInvokableCall_t1339_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseInvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, BaseInvokableCall_t1339_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &BaseInvokableCall_t1339_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &BaseInvokableCall_t1339_0_0_0/* byval_arg */
	, &BaseInvokableCall_t1339_1_0_0/* this_arg */
	, &BaseInvokableCall_t1339_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseInvokableCall_t1339)/* instance_size */
	, sizeof (BaseInvokableCall_t1339)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCall.h"
// Metadata Definition UnityEngine.Events.InvokableCall
extern TypeInfo InvokableCall_t1340_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCallMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_t1340_InvokableCall__ctor_m6861_ParameterInfos[] = 
{
	{"target", 0, 134219621, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219622, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall__ctor_m6861_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall__ctor_m6861/* method */
	, &InvokableCall_t1340_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, InvokableCall_t1340_InvokableCall__ctor_m6861_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1816/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo InvokableCall_t1340_InvokableCall_Invoke_m6862_ParameterInfos[] = 
{
	{"args", 0, 134219623, 0, &ObjectU5BU5D_t115_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern const MethodInfo InvokableCall_Invoke_m6862_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_Invoke_m6862/* method */
	, &InvokableCall_t1340_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, InvokableCall_t1340_InvokableCall_Invoke_m6862_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1817/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_t1340_InvokableCall_Find_m6863_ParameterInfos[] = 
{
	{"targetObj", 0, 134219624, 0, &Object_t_0_0_0},
	{"method", 1, 134219625, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_Find_m6863_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_Find_m6863/* method */
	, &InvokableCall_t1340_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t_Object_t/* invoker_method */
	, InvokableCall_t1340_InvokableCall_Find_m6863_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1818/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_t1340_MethodInfos[] =
{
	&InvokableCall__ctor_m6861_MethodInfo,
	&InvokableCall_Invoke_m6862_MethodInfo,
	&InvokableCall_Find_m6863_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_Invoke_m6862_MethodInfo;
extern const MethodInfo InvokableCall_Find_m6863_MethodInfo;
static const Il2CppMethodReference InvokableCall_t1340_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&InvokableCall_Invoke_m6862_MethodInfo,
	&InvokableCall_Find_m6863_MethodInfo,
};
static bool InvokableCall_t1340_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_t1340_0_0_0;
extern const Il2CppType InvokableCall_t1340_1_0_0;
struct InvokableCall_t1340;
const Il2CppTypeDefinitionMetadata InvokableCall_t1340_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1339_0_0_0/* parent */
	, InvokableCall_t1340_VTable/* vtableMethods */
	, InvokableCall_t1340_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1332/* fieldStart */

};
TypeInfo InvokableCall_t1340_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_t1340_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_t1340_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_t1340_0_0_0/* byval_arg */
	, &InvokableCall_t1340_1_0_0/* this_arg */
	, &InvokableCall_t1340_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_t1340)/* instance_size */
	, sizeof (InvokableCall_t1340)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`1
extern TypeInfo InvokableCall_1_t1457_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_1_t1457_Il2CppGenericContainer;
extern TypeInfo InvokableCall_1_t1457_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_1_t1457_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_1_t1457_Il2CppGenericContainer, NULL, "T1", 0, 0 };
static const Il2CppGenericParameter* InvokableCall_1_t1457_Il2CppGenericParametersArray[1] = 
{
	&InvokableCall_1_t1457_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_1_t1457_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_1_t1457_il2cpp_TypeInfo, 1, 0, InvokableCall_1_t1457_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_1_t1457_InvokableCall_1__ctor_m7121_ParameterInfos[] = 
{
	{"target", 0, 134219626, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219627, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`1::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_1__ctor_m7121_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1457_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1457_InvokableCall_1__ctor_m7121_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1819/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1537_0_0_0;
extern const Il2CppType UnityAction_1_t1537_0_0_0;
static const ParameterInfo InvokableCall_1_t1457_InvokableCall_1__ctor_m7122_ParameterInfos[] = 
{
	{"callback", 0, 134219628, 0, &UnityAction_1_t1537_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`1::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern const MethodInfo InvokableCall_1__ctor_m7122_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1457_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1457_InvokableCall_1__ctor_m7122_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1820/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo InvokableCall_1_t1457_InvokableCall_1_Invoke_m7123_ParameterInfos[] = 
{
	{"args", 0, 134219629, 0, &ObjectU5BU5D_t115_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`1::Invoke(System.Object[])
extern const MethodInfo InvokableCall_1_Invoke_m7123_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1457_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1457_InvokableCall_1_Invoke_m7123_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1821/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_1_t1457_InvokableCall_1_Find_m7124_ParameterInfos[] = 
{
	{"targetObj", 0, 134219630, 0, &Object_t_0_0_0},
	{"method", 1, 134219631, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`1::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_1_Find_m7124_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1457_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1457_InvokableCall_1_Find_m7124_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1822/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_1_t1457_MethodInfos[] =
{
	&InvokableCall_1__ctor_m7121_MethodInfo,
	&InvokableCall_1__ctor_m7122_MethodInfo,
	&InvokableCall_1_Invoke_m7123_MethodInfo,
	&InvokableCall_1_Find_m7124_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_1_Invoke_m7123_MethodInfo;
extern const MethodInfo InvokableCall_1_Find_m7124_MethodInfo;
static const Il2CppMethodReference InvokableCall_1_t1457_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&InvokableCall_1_Invoke_m7123_MethodInfo,
	&InvokableCall_1_Find_m7124_MethodInfo,
};
static bool InvokableCall_1_t1457_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1536_m7226_GenericMethod;
extern const Il2CppType InvokableCall_1_t1457_gp_0_0_0_0;
extern const Il2CppGenericMethod UnityAction_1_Invoke_m7227_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_1_t1457_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_1_t1537_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_1_t1537_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1536_m7226_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_1_t1457_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_1_Invoke_m7227_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_1_t1457_0_0_0;
extern const Il2CppType InvokableCall_1_t1457_1_0_0;
struct InvokableCall_1_t1457;
const Il2CppTypeDefinitionMetadata InvokableCall_1_t1457_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1339_0_0_0/* parent */
	, InvokableCall_1_t1457_VTable/* vtableMethods */
	, InvokableCall_1_t1457_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_1_t1457_RGCTXData/* rgctxDefinition */
	, 1333/* fieldStart */

};
TypeInfo InvokableCall_1_t1457_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t1457_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_1_t1457_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_1_t1457_0_0_0/* byval_arg */
	, &InvokableCall_1_t1457_1_0_0/* this_arg */
	, &InvokableCall_1_t1457_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_1_t1457_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`2
extern TypeInfo InvokableCall_2_t1458_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_2_t1458_Il2CppGenericContainer;
extern TypeInfo InvokableCall_2_t1458_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_2_t1458_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_2_t1458_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo InvokableCall_2_t1458_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_2_t1458_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_2_t1458_Il2CppGenericContainer, NULL, "T2", 1, 0 };
static const Il2CppGenericParameter* InvokableCall_2_t1458_Il2CppGenericParametersArray[2] = 
{
	&InvokableCall_2_t1458_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_2_t1458_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_2_t1458_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_2_t1458_il2cpp_TypeInfo, 2, 0, InvokableCall_2_t1458_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_2_t1458_InvokableCall_2__ctor_m7125_ParameterInfos[] = 
{
	{"target", 0, 134219632, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219633, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`2::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_2__ctor_m7125_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1458_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1458_InvokableCall_2__ctor_m7125_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1823/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo InvokableCall_2_t1458_InvokableCall_2_Invoke_m7126_ParameterInfos[] = 
{
	{"args", 0, 134219634, 0, &ObjectU5BU5D_t115_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`2::Invoke(System.Object[])
extern const MethodInfo InvokableCall_2_Invoke_m7126_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1458_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1458_InvokableCall_2_Invoke_m7126_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1824/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_2_t1458_InvokableCall_2_Find_m7127_ParameterInfos[] = 
{
	{"targetObj", 0, 134219635, 0, &Object_t_0_0_0},
	{"method", 1, 134219636, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`2::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_2_Find_m7127_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1458_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1458_InvokableCall_2_Find_m7127_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1825/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_2_t1458_MethodInfos[] =
{
	&InvokableCall_2__ctor_m7125_MethodInfo,
	&InvokableCall_2_Invoke_m7126_MethodInfo,
	&InvokableCall_2_Find_m7127_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_2_Invoke_m7126_MethodInfo;
extern const MethodInfo InvokableCall_2_Find_m7127_MethodInfo;
static const Il2CppMethodReference InvokableCall_2_t1458_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&InvokableCall_2_Invoke_m7126_MethodInfo,
	&InvokableCall_2_Find_m7127_MethodInfo,
};
static bool InvokableCall_2_t1458_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType UnityAction_2_t1540_0_0_0;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1538_m7228_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1539_m7229_GenericMethod;
extern const Il2CppType InvokableCall_2_t1458_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t1458_gp_1_0_0_0;
extern const Il2CppGenericMethod UnityAction_2_Invoke_m7230_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_2_t1458_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_2_t1540_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_2_t1540_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1538_m7228_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1539_m7229_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_2_t1458_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_2_t1458_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_2_Invoke_m7230_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_2_t1458_0_0_0;
extern const Il2CppType InvokableCall_2_t1458_1_0_0;
struct InvokableCall_2_t1458;
const Il2CppTypeDefinitionMetadata InvokableCall_2_t1458_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1339_0_0_0/* parent */
	, InvokableCall_2_t1458_VTable/* vtableMethods */
	, InvokableCall_2_t1458_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_2_t1458_RGCTXData/* rgctxDefinition */
	, 1334/* fieldStart */

};
TypeInfo InvokableCall_2_t1458_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_2_t1458_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_2_t1458_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_2_t1458_0_0_0/* byval_arg */
	, &InvokableCall_2_t1458_1_0_0/* this_arg */
	, &InvokableCall_2_t1458_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_2_t1458_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`3
extern TypeInfo InvokableCall_3_t1459_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_3_t1459_Il2CppGenericContainer;
extern TypeInfo InvokableCall_3_t1459_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_3_t1459_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_3_t1459_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo InvokableCall_3_t1459_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_3_t1459_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_3_t1459_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo InvokableCall_3_t1459_gp_T3_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_3_t1459_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_3_t1459_Il2CppGenericContainer, NULL, "T3", 2, 0 };
static const Il2CppGenericParameter* InvokableCall_3_t1459_Il2CppGenericParametersArray[3] = 
{
	&InvokableCall_3_t1459_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_3_t1459_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_3_t1459_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_3_t1459_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_3_t1459_il2cpp_TypeInfo, 3, 0, InvokableCall_3_t1459_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_3_t1459_InvokableCall_3__ctor_m7128_ParameterInfos[] = 
{
	{"target", 0, 134219637, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219638, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`3::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_3__ctor_m7128_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1459_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1459_InvokableCall_3__ctor_m7128_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1826/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo InvokableCall_3_t1459_InvokableCall_3_Invoke_m7129_ParameterInfos[] = 
{
	{"args", 0, 134219639, 0, &ObjectU5BU5D_t115_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`3::Invoke(System.Object[])
extern const MethodInfo InvokableCall_3_Invoke_m7129_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1459_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1459_InvokableCall_3_Invoke_m7129_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1827/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_3_t1459_InvokableCall_3_Find_m7130_ParameterInfos[] = 
{
	{"targetObj", 0, 134219640, 0, &Object_t_0_0_0},
	{"method", 1, 134219641, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`3::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_3_Find_m7130_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1459_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1459_InvokableCall_3_Find_m7130_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1828/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_3_t1459_MethodInfos[] =
{
	&InvokableCall_3__ctor_m7128_MethodInfo,
	&InvokableCall_3_Invoke_m7129_MethodInfo,
	&InvokableCall_3_Find_m7130_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_3_Invoke_m7129_MethodInfo;
extern const MethodInfo InvokableCall_3_Find_m7130_MethodInfo;
static const Il2CppMethodReference InvokableCall_3_t1459_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&InvokableCall_3_Invoke_m7129_MethodInfo,
	&InvokableCall_3_Find_m7130_MethodInfo,
};
static bool InvokableCall_3_t1459_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType UnityAction_3_t1544_0_0_0;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1541_m7231_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1542_m7232_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1543_m7233_GenericMethod;
extern const Il2CppType InvokableCall_3_t1459_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t1459_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t1459_gp_2_0_0_0;
extern const Il2CppGenericMethod UnityAction_3_Invoke_m7234_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_3_t1459_RGCTXData[10] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_3_t1544_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_3_t1544_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1541_m7231_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1542_m7232_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1543_m7233_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t1459_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t1459_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t1459_gp_2_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_3_Invoke_m7234_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_3_t1459_0_0_0;
extern const Il2CppType InvokableCall_3_t1459_1_0_0;
struct InvokableCall_3_t1459;
const Il2CppTypeDefinitionMetadata InvokableCall_3_t1459_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1339_0_0_0/* parent */
	, InvokableCall_3_t1459_VTable/* vtableMethods */
	, InvokableCall_3_t1459_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_3_t1459_RGCTXData/* rgctxDefinition */
	, 1335/* fieldStart */

};
TypeInfo InvokableCall_3_t1459_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_3_t1459_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_3_t1459_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_3_t1459_0_0_0/* byval_arg */
	, &InvokableCall_3_t1459_1_0_0/* this_arg */
	, &InvokableCall_3_t1459_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_3_t1459_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.InvokableCall`4
extern TypeInfo InvokableCall_4_t1460_il2cpp_TypeInfo;
extern const Il2CppGenericContainer InvokableCall_4_t1460_Il2CppGenericContainer;
extern TypeInfo InvokableCall_4_t1460_gp_T1_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t1460_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t1460_Il2CppGenericContainer, NULL, "T1", 0, 0 };
extern TypeInfo InvokableCall_4_t1460_gp_T2_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t1460_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t1460_Il2CppGenericContainer, NULL, "T2", 1, 0 };
extern TypeInfo InvokableCall_4_t1460_gp_T3_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t1460_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t1460_Il2CppGenericContainer, NULL, "T3", 2, 0 };
extern TypeInfo InvokableCall_4_t1460_gp_T4_3_il2cpp_TypeInfo;
extern const Il2CppGenericParameter InvokableCall_4_t1460_gp_T4_3_il2cpp_TypeInfo_GenericParamFull = { &InvokableCall_4_t1460_Il2CppGenericContainer, NULL, "T4", 3, 0 };
static const Il2CppGenericParameter* InvokableCall_4_t1460_Il2CppGenericParametersArray[4] = 
{
	&InvokableCall_4_t1460_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1460_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1460_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1460_gp_T4_3_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer InvokableCall_4_t1460_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&InvokableCall_4_t1460_il2cpp_TypeInfo, 4, 0, InvokableCall_4_t1460_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_4_t1460_InvokableCall_4__ctor_m7131_ParameterInfos[] = 
{
	{"target", 0, 134219642, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219643, 0, &MethodInfo_t_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`4::.ctor(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_4__ctor_m7131_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1460_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1460_InvokableCall_4__ctor_m7131_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1829/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo InvokableCall_4_t1460_InvokableCall_4_Invoke_m7132_ParameterInfos[] = 
{
	{"args", 0, 134219644, 0, &ObjectU5BU5D_t115_0_0_0},
};
// System.Void UnityEngine.Events.InvokableCall`4::Invoke(System.Object[])
extern const MethodInfo InvokableCall_4_Invoke_m7132_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1460_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1460_InvokableCall_4_Invoke_m7132_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1830/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCall_4_t1460_InvokableCall_4_Find_m7133_ParameterInfos[] = 
{
	{"targetObj", 0, 134219645, 0, &Object_t_0_0_0},
	{"method", 1, 134219646, 0, &MethodInfo_t_0_0_0},
};
// System.Boolean UnityEngine.Events.InvokableCall`4::Find(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCall_4_Find_m7133_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1460_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1460_InvokableCall_4_Find_m7133_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1831/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCall_4_t1460_MethodInfos[] =
{
	&InvokableCall_4__ctor_m7131_MethodInfo,
	&InvokableCall_4_Invoke_m7132_MethodInfo,
	&InvokableCall_4_Find_m7133_MethodInfo,
	NULL
};
extern const MethodInfo InvokableCall_4_Invoke_m7132_MethodInfo;
extern const MethodInfo InvokableCall_4_Find_m7133_MethodInfo;
static const Il2CppMethodReference InvokableCall_4_t1460_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&InvokableCall_4_Invoke_m7132_MethodInfo,
	&InvokableCall_4_Find_m7133_MethodInfo,
};
static bool InvokableCall_4_t1460_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType UnityAction_4_t1549_0_0_0;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1545_m7235_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1546_m7236_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1547_m7237_GenericMethod;
extern const Il2CppGenericMethod BaseInvokableCall_ThrowOnInvalidArg_TisT4_t1548_m7238_GenericMethod;
extern const Il2CppType InvokableCall_4_t1460_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t1460_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t1460_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t1460_gp_3_0_0_0;
extern const Il2CppGenericMethod UnityAction_4_Invoke_m7239_GenericMethod;
static Il2CppRGCTXDefinition InvokableCall_4_t1460_RGCTXData[12] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityAction_4_t1549_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityAction_4_t1549_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT1_t1545_m7235_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT2_t1546_m7236_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT3_t1547_m7237_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &BaseInvokableCall_ThrowOnInvalidArg_TisT4_t1548_m7238_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1460_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1460_gp_1_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1460_gp_2_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1460_gp_3_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_4_Invoke_m7239_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCall_4_t1460_0_0_0;
extern const Il2CppType InvokableCall_4_t1460_1_0_0;
struct InvokableCall_4_t1460;
const Il2CppTypeDefinitionMetadata InvokableCall_4_t1460_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &BaseInvokableCall_t1339_0_0_0/* parent */
	, InvokableCall_4_t1460_VTable/* vtableMethods */
	, InvokableCall_4_t1460_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, InvokableCall_4_t1460_RGCTXData/* rgctxDefinition */
	, 1336/* fieldStart */

};
TypeInfo InvokableCall_4_t1460_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_4_t1460_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCall_4_t1460_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCall_4_t1460_0_0_0/* byval_arg */
	, &InvokableCall_4_t1460_1_0_0/* this_arg */
	, &InvokableCall_4_t1460_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &InvokableCall_4_t1460_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1
extern TypeInfo CachedInvokableCall_1_t1439_il2cpp_TypeInfo;
extern const Il2CppGenericContainer CachedInvokableCall_1_t1439_Il2CppGenericContainer;
extern TypeInfo CachedInvokableCall_1_t1439_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter CachedInvokableCall_1_t1439_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &CachedInvokableCall_1_t1439_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* CachedInvokableCall_1_t1439_Il2CppGenericParametersArray[1] = 
{
	&CachedInvokableCall_1_t1439_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer CachedInvokableCall_1_t1439_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&CachedInvokableCall_1_t1439_il2cpp_TypeInfo, 1, 0, CachedInvokableCall_1_t1439_Il2CppGenericParametersArray };
extern const Il2CppType Object_t123_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t1439_gp_0_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t1439_gp_0_0_0_0;
static const ParameterInfo CachedInvokableCall_1_t1439_CachedInvokableCall_1__ctor_m7134_ParameterInfos[] = 
{
	{"target", 0, 134219647, 0, &Object_t123_0_0_0},
	{"theFunction", 1, 134219648, 0, &MethodInfo_t_0_0_0},
	{"argument", 2, 134219649, 0, &CachedInvokableCall_1_t1439_gp_0_0_0_0},
};
// System.Void UnityEngine.Events.CachedInvokableCall`1::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern const MethodInfo CachedInvokableCall_1__ctor_m7134_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &CachedInvokableCall_1_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, CachedInvokableCall_1_t1439_CachedInvokableCall_1__ctor_m7134_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1832/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo CachedInvokableCall_1_t1439_CachedInvokableCall_1_Invoke_m7135_ParameterInfos[] = 
{
	{"args", 0, 134219650, 0, &ObjectU5BU5D_t115_0_0_0},
};
// System.Void UnityEngine.Events.CachedInvokableCall`1::Invoke(System.Object[])
extern const MethodInfo CachedInvokableCall_1_Invoke_m7135_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &CachedInvokableCall_1_t1439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, CachedInvokableCall_1_t1439_CachedInvokableCall_1_Invoke_m7135_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1833/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CachedInvokableCall_1_t1439_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m7134_MethodInfo,
	&CachedInvokableCall_1_Invoke_m7135_MethodInfo,
	NULL
};
extern const MethodInfo CachedInvokableCall_1_Invoke_m7135_MethodInfo;
extern const Il2CppGenericMethod InvokableCall_1_Find_m7240_GenericMethod;
static const Il2CppMethodReference CachedInvokableCall_1_t1439_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&CachedInvokableCall_1_Invoke_m7135_MethodInfo,
	&InvokableCall_1_Find_m7240_GenericMethod,
};
static bool CachedInvokableCall_1_t1439_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	true,
};
extern const Il2CppGenericMethod InvokableCall_1__ctor_m7241_GenericMethod;
extern const Il2CppGenericMethod InvokableCall_1_Invoke_m7242_GenericMethod;
static Il2CppRGCTXDefinition CachedInvokableCall_1_t1439_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m7241_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&CachedInvokableCall_1_t1439_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1_Invoke_m7242_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType CachedInvokableCall_1_t1439_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t1439_1_0_0;
extern const Il2CppType InvokableCall_1_t1551_0_0_0;
struct CachedInvokableCall_1_t1439;
const Il2CppTypeDefinitionMetadata CachedInvokableCall_1_t1439_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &InvokableCall_1_t1551_0_0_0/* parent */
	, CachedInvokableCall_1_t1439_VTable/* vtableMethods */
	, CachedInvokableCall_1_t1439_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, CachedInvokableCall_1_t1439_RGCTXData/* rgctxDefinition */
	, 1337/* fieldStart */

};
TypeInfo CachedInvokableCall_1_t1439_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t1439_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CachedInvokableCall_1_t1439_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CachedInvokableCall_1_t1439_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t1439_1_0_0/* this_arg */
	, &CachedInvokableCall_1_t1439_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &CachedInvokableCall_1_t1439_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallState.h"
// Metadata Definition UnityEngine.Events.UnityEventCallState
extern TypeInfo UnityEventCallState_t1341_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallStateMethodDeclarations.h"
static const MethodInfo* UnityEventCallState_t1341_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference UnityEventCallState_t1341_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool UnityEventCallState_t1341_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEventCallState_t1341_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEventCallState_t1341_0_0_0;
extern const Il2CppType UnityEventCallState_t1341_1_0_0;
const Il2CppTypeDefinitionMetadata UnityEventCallState_t1341_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEventCallState_t1341_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, UnityEventCallState_t1341_VTable/* vtableMethods */
	, UnityEventCallState_t1341_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1338/* fieldStart */

};
TypeInfo UnityEventCallState_t1341_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventCallState"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEventCallState_t1341_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEventCallState_t1341_0_0_0/* byval_arg */
	, &UnityEventCallState_t1341_1_0_0/* this_arg */
	, &UnityEventCallState_t1341_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventCallState_t1341)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (UnityEventCallState_t1341)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCall.h"
// Metadata Definition UnityEngine.Events.PersistentCall
extern TypeInfo PersistentCall_t1342_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCallMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern const MethodInfo PersistentCall__ctor_m6864_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PersistentCall__ctor_m6864/* method */
	, &PersistentCall_t1342_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern const MethodInfo PersistentCall_get_target_m6865_MethodInfo = 
{
	"get_target"/* name */
	, (methodPointerType)&PersistentCall_get_target_m6865/* method */
	, &PersistentCall_t1342_il2cpp_TypeInfo/* declaring_type */
	, &Object_t123_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern const MethodInfo PersistentCall_get_methodName_m6866_MethodInfo = 
{
	"get_methodName"/* name */
	, (methodPointerType)&PersistentCall_get_methodName_m6866/* method */
	, &PersistentCall_t1342_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_PersistentListenerMode_t1337 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern const MethodInfo PersistentCall_get_mode_m6867_MethodInfo = 
{
	"get_mode"/* name */
	, (methodPointerType)&PersistentCall_get_mode_m6867/* method */
	, &PersistentCall_t1342_il2cpp_TypeInfo/* declaring_type */
	, &PersistentListenerMode_t1337_0_0_0/* return_type */
	, RuntimeInvoker_PersistentListenerMode_t1337/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern const MethodInfo PersistentCall_get_arguments_m6868_MethodInfo = 
{
	"get_arguments"/* name */
	, (methodPointerType)&PersistentCall_get_arguments_m6868/* method */
	, &PersistentCall_t1342_il2cpp_TypeInfo/* declaring_type */
	, &ArgumentCache_t1338_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern const MethodInfo PersistentCall_IsValid_m6869_MethodInfo = 
{
	"IsValid"/* name */
	, (methodPointerType)&PersistentCall_IsValid_m6869/* method */
	, &PersistentCall_t1342_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityEventBase_t1347_0_0_0;
extern const Il2CppType UnityEventBase_t1347_0_0_0;
static const ParameterInfo PersistentCall_t1342_PersistentCall_GetRuntimeCall_m6870_ParameterInfos[] = 
{
	{"theEvent", 0, 134219651, 0, &UnityEventBase_t1347_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern const MethodInfo PersistentCall_GetRuntimeCall_m6870_MethodInfo = 
{
	"GetRuntimeCall"/* name */
	, (methodPointerType)&PersistentCall_GetRuntimeCall_m6870/* method */
	, &PersistentCall_t1342_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1339_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PersistentCall_t1342_PersistentCall_GetRuntimeCall_m6870_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t123_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType ArgumentCache_t1338_0_0_0;
static const ParameterInfo PersistentCall_t1342_PersistentCall_GetObjectCall_m6871_ParameterInfos[] = 
{
	{"target", 0, 134219652, 0, &Object_t123_0_0_0},
	{"method", 1, 134219653, 0, &MethodInfo_t_0_0_0},
	{"arguments", 2, 134219654, 0, &ArgumentCache_t1338_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern const MethodInfo PersistentCall_GetObjectCall_m6871_MethodInfo = 
{
	"GetObjectCall"/* name */
	, (methodPointerType)&PersistentCall_GetObjectCall_m6871/* method */
	, &PersistentCall_t1342_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1339_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, PersistentCall_t1342_PersistentCall_GetObjectCall_m6871_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PersistentCall_t1342_MethodInfos[] =
{
	&PersistentCall__ctor_m6864_MethodInfo,
	&PersistentCall_get_target_m6865_MethodInfo,
	&PersistentCall_get_methodName_m6866_MethodInfo,
	&PersistentCall_get_mode_m6867_MethodInfo,
	&PersistentCall_get_arguments_m6868_MethodInfo,
	&PersistentCall_IsValid_m6869_MethodInfo,
	&PersistentCall_GetRuntimeCall_m6870_MethodInfo,
	&PersistentCall_GetObjectCall_m6871_MethodInfo,
	NULL
};
extern const MethodInfo PersistentCall_get_target_m6865_MethodInfo;
static const PropertyInfo PersistentCall_t1342____target_PropertyInfo = 
{
	&PersistentCall_t1342_il2cpp_TypeInfo/* parent */
	, "target"/* name */
	, &PersistentCall_get_target_m6865_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PersistentCall_get_methodName_m6866_MethodInfo;
static const PropertyInfo PersistentCall_t1342____methodName_PropertyInfo = 
{
	&PersistentCall_t1342_il2cpp_TypeInfo/* parent */
	, "methodName"/* name */
	, &PersistentCall_get_methodName_m6866_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PersistentCall_get_mode_m6867_MethodInfo;
static const PropertyInfo PersistentCall_t1342____mode_PropertyInfo = 
{
	&PersistentCall_t1342_il2cpp_TypeInfo/* parent */
	, "mode"/* name */
	, &PersistentCall_get_mode_m6867_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo PersistentCall_get_arguments_m6868_MethodInfo;
static const PropertyInfo PersistentCall_t1342____arguments_PropertyInfo = 
{
	&PersistentCall_t1342_il2cpp_TypeInfo/* parent */
	, "arguments"/* name */
	, &PersistentCall_get_arguments_m6868_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* PersistentCall_t1342_PropertyInfos[] =
{
	&PersistentCall_t1342____target_PropertyInfo,
	&PersistentCall_t1342____methodName_PropertyInfo,
	&PersistentCall_t1342____mode_PropertyInfo,
	&PersistentCall_t1342____arguments_PropertyInfo,
	NULL
};
static const Il2CppMethodReference PersistentCall_t1342_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool PersistentCall_t1342_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentCall_t1342_0_0_0;
extern const Il2CppType PersistentCall_t1342_1_0_0;
struct PersistentCall_t1342;
const Il2CppTypeDefinitionMetadata PersistentCall_t1342_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PersistentCall_t1342_VTable/* vtableMethods */
	, PersistentCall_t1342_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1342/* fieldStart */

};
TypeInfo PersistentCall_t1342_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentCall_t1342_MethodInfos/* methods */
	, PersistentCall_t1342_PropertyInfos/* properties */
	, NULL/* events */
	, &PersistentCall_t1342_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentCall_t1342_0_0_0/* byval_arg */
	, &PersistentCall_t1342_1_0_0/* this_arg */
	, &PersistentCall_t1342_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCall_t1342)/* instance_size */
	, sizeof (PersistentCall_t1342)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 8/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup.h"
// Metadata Definition UnityEngine.Events.PersistentCallGroup
extern TypeInfo PersistentCallGroup_t1344_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern const MethodInfo PersistentCallGroup__ctor_m6872_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PersistentCallGroup__ctor_m6872/* method */
	, &PersistentCallGroup_t1344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType InvokableCallList_t1346_0_0_0;
extern const Il2CppType InvokableCallList_t1346_0_0_0;
extern const Il2CppType UnityEventBase_t1347_0_0_0;
static const ParameterInfo PersistentCallGroup_t1344_PersistentCallGroup_Initialize_m6873_ParameterInfos[] = 
{
	{"invokableList", 0, 134219655, 0, &InvokableCallList_t1346_0_0_0},
	{"unityEventBase", 1, 134219656, 0, &UnityEventBase_t1347_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern const MethodInfo PersistentCallGroup_Initialize_m6873_MethodInfo = 
{
	"Initialize"/* name */
	, (methodPointerType)&PersistentCallGroup_Initialize_m6873/* method */
	, &PersistentCallGroup_t1344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, PersistentCallGroup_t1344_PersistentCallGroup_Initialize_m6873_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PersistentCallGroup_t1344_MethodInfos[] =
{
	&PersistentCallGroup__ctor_m6872_MethodInfo,
	&PersistentCallGroup_Initialize_m6873_MethodInfo,
	NULL
};
static const Il2CppMethodReference PersistentCallGroup_t1344_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool PersistentCallGroup_t1344_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType PersistentCallGroup_t1344_0_0_0;
extern const Il2CppType PersistentCallGroup_t1344_1_0_0;
struct PersistentCallGroup_t1344;
const Il2CppTypeDefinitionMetadata PersistentCallGroup_t1344_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PersistentCallGroup_t1344_VTable/* vtableMethods */
	, PersistentCallGroup_t1344_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1347/* fieldStart */

};
TypeInfo PersistentCallGroup_t1344_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCallGroup"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentCallGroup_t1344_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PersistentCallGroup_t1344_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PersistentCallGroup_t1344_0_0_0/* byval_arg */
	, &PersistentCallGroup_t1344_1_0_0/* this_arg */
	, &PersistentCallGroup_t1344_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCallGroup_t1344)/* instance_size */
	, sizeof (PersistentCallGroup_t1344)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallList.h"
// Metadata Definition UnityEngine.Events.InvokableCallList
extern TypeInfo InvokableCallList_t1346_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallListMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern const MethodInfo InvokableCallList__ctor_m6874_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCallList__ctor_m6874/* method */
	, &InvokableCallList_t1346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseInvokableCall_t1339_0_0_0;
static const ParameterInfo InvokableCallList_t1346_InvokableCallList_AddPersistentInvokableCall_m6875_ParameterInfos[] = 
{
	{"call", 0, 134219657, 0, &BaseInvokableCall_t1339_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo InvokableCallList_AddPersistentInvokableCall_m6875_MethodInfo = 
{
	"AddPersistentInvokableCall"/* name */
	, (methodPointerType)&InvokableCallList_AddPersistentInvokableCall_m6875/* method */
	, &InvokableCallList_t1346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, InvokableCallList_t1346_InvokableCallList_AddPersistentInvokableCall_m6875_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseInvokableCall_t1339_0_0_0;
static const ParameterInfo InvokableCallList_t1346_InvokableCallList_AddListener_m6876_ParameterInfos[] = 
{
	{"call", 0, 134219658, 0, &BaseInvokableCall_t1339_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo InvokableCallList_AddListener_m6876_MethodInfo = 
{
	"AddListener"/* name */
	, (methodPointerType)&InvokableCallList_AddListener_m6876/* method */
	, &InvokableCallList_t1346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, InvokableCallList_t1346_InvokableCallList_AddListener_m6876_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo InvokableCallList_t1346_InvokableCallList_RemoveListener_m6877_ParameterInfos[] = 
{
	{"targetObj", 0, 134219659, 0, &Object_t_0_0_0},
	{"method", 1, 134219660, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo InvokableCallList_RemoveListener_m6877_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&InvokableCallList_RemoveListener_m6877/* method */
	, &InvokableCallList_t1346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, InvokableCallList_t1346_InvokableCallList_RemoveListener_m6877_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern const MethodInfo InvokableCallList_ClearPersistent_m6878_MethodInfo = 
{
	"ClearPersistent"/* name */
	, (methodPointerType)&InvokableCallList_ClearPersistent_m6878/* method */
	, &InvokableCallList_t1346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo InvokableCallList_t1346_InvokableCallList_Invoke_m6879_ParameterInfos[] = 
{
	{"parameters", 0, 134219661, 0, &ObjectU5BU5D_t115_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
extern const MethodInfo InvokableCallList_Invoke_m6879_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCallList_Invoke_m6879/* method */
	, &InvokableCallList_t1346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, InvokableCallList_t1346_InvokableCallList_Invoke_m6879_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* InvokableCallList_t1346_MethodInfos[] =
{
	&InvokableCallList__ctor_m6874_MethodInfo,
	&InvokableCallList_AddPersistentInvokableCall_m6875_MethodInfo,
	&InvokableCallList_AddListener_m6876_MethodInfo,
	&InvokableCallList_RemoveListener_m6877_MethodInfo,
	&InvokableCallList_ClearPersistent_m6878_MethodInfo,
	&InvokableCallList_Invoke_m6879_MethodInfo,
	NULL
};
static const Il2CppMethodReference InvokableCallList_t1346_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool InvokableCallList_t1346_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType InvokableCallList_t1346_1_0_0;
struct InvokableCallList_t1346;
const Il2CppTypeDefinitionMetadata InvokableCallList_t1346_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, InvokableCallList_t1346_VTable/* vtableMethods */
	, InvokableCallList_t1346_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1348/* fieldStart */

};
TypeInfo InvokableCallList_t1346_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCallList"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCallList_t1346_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &InvokableCallList_t1346_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &InvokableCallList_t1346_0_0_0/* byval_arg */
	, &InvokableCallList_t1346_1_0_0/* this_arg */
	, &InvokableCallList_t1346_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCallList_t1346)/* instance_size */
	, sizeof (InvokableCallList_t1346)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// Metadata Definition UnityEngine.Events.UnityEventBase
extern TypeInfo UnityEventBase_t1347_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern const MethodInfo UnityEventBase__ctor_m6880_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEventBase__ctor_m6880/* method */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2550_MethodInfo = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize"/* name */
	, (methodPointerType)&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2550/* method */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2551_MethodInfo = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize"/* name */
	, (methodPointerType)&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2551/* method */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEventBase_t1347_UnityEventBase_FindMethod_Impl_m7136_ParameterInfos[] = 
{
	{"name", 0, 134219662, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219663, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEventBase_FindMethod_Impl_m7136_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1347_UnityEventBase_FindMethod_Impl_m7136_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEventBase_t1347_UnityEventBase_GetDelegate_m7137_ParameterInfos[] = 
{
	{"target", 0, 134219664, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219665, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEventBase_GetDelegate_m7137_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1339_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1347_UnityEventBase_GetDelegate_m7137_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1475/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PersistentCall_t1342_0_0_0;
static const ParameterInfo UnityEventBase_t1347_UnityEventBase_FindMethod_m6881_ParameterInfos[] = 
{
	{"call", 0, 134219666, 0, &PersistentCall_t1342_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern const MethodInfo UnityEventBase_FindMethod_m6881_MethodInfo = 
{
	"FindMethod"/* name */
	, (methodPointerType)&UnityEventBase_FindMethod_m6881/* method */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1347_UnityEventBase_FindMethod_m6881_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType PersistentListenerMode_t1337_0_0_0;
extern const Il2CppType Type_t_0_0_0;
static const ParameterInfo UnityEventBase_t1347_UnityEventBase_FindMethod_m6882_ParameterInfos[] = 
{
	{"name", 0, 134219667, 0, &String_t_0_0_0},
	{"listener", 1, 134219668, 0, &Object_t_0_0_0},
	{"mode", 2, 134219669, 0, &PersistentListenerMode_t1337_0_0_0},
	{"argumentType", 3, 134219670, 0, &Type_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t127_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern const MethodInfo UnityEventBase_FindMethod_m6882_MethodInfo = 
{
	"FindMethod"/* name */
	, (methodPointerType)&UnityEventBase_FindMethod_m6882/* method */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t127_Object_t/* invoker_method */
	, UnityEventBase_t1347_UnityEventBase_FindMethod_m6882_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern const MethodInfo UnityEventBase_DirtyPersistentCalls_m6883_MethodInfo = 
{
	"DirtyPersistentCalls"/* name */
	, (methodPointerType)&UnityEventBase_DirtyPersistentCalls_m6883/* method */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern const MethodInfo UnityEventBase_RebuildPersistentCallsIfNeeded_m6884_MethodInfo = 
{
	"RebuildPersistentCallsIfNeeded"/* name */
	, (methodPointerType)&UnityEventBase_RebuildPersistentCallsIfNeeded_m6884/* method */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseInvokableCall_t1339_0_0_0;
static const ParameterInfo UnityEventBase_t1347_UnityEventBase_AddCall_m6885_ParameterInfos[] = 
{
	{"call", 0, 134219671, 0, &BaseInvokableCall_t1339_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern const MethodInfo UnityEventBase_AddCall_m6885_MethodInfo = 
{
	"AddCall"/* name */
	, (methodPointerType)&UnityEventBase_AddCall_m6885/* method */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UnityEventBase_t1347_UnityEventBase_AddCall_m6885_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEventBase_t1347_UnityEventBase_RemoveListener_m6886_ParameterInfos[] = 
{
	{"targetObj", 0, 134219672, 0, &Object_t_0_0_0},
	{"method", 1, 134219673, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEventBase_RemoveListener_m6886_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&UnityEventBase_RemoveListener_m6886/* method */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1347_UnityEventBase_RemoveListener_m6886_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectU5BU5D_t115_0_0_0;
static const ParameterInfo UnityEventBase_t1347_UnityEventBase_Invoke_m6887_ParameterInfos[] = 
{
	{"parameters", 0, 134219674, 0, &ObjectU5BU5D_t115_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern const MethodInfo UnityEventBase_Invoke_m6887_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEventBase_Invoke_m6887/* method */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UnityEventBase_t1347_UnityEventBase_Invoke_m6887_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern const MethodInfo UnityEventBase_ToString_m2549_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&UnityEventBase_ToString_m2549/* method */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType TypeU5BU5D_t878_0_0_0;
static const ParameterInfo UnityEventBase_t1347_UnityEventBase_GetValidMethodInfo_m6888_ParameterInfos[] = 
{
	{"obj", 0, 134219675, 0, &Object_t_0_0_0},
	{"functionName", 1, 134219676, 0, &String_t_0_0_0},
	{"argumentTypes", 2, 134219677, 0, &TypeU5BU5D_t878_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern const MethodInfo UnityEventBase_GetValidMethodInfo_m6888_MethodInfo = 
{
	"GetValidMethodInfo"/* name */
	, (methodPointerType)&UnityEventBase_GetValidMethodInfo_m6888/* method */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1347_UnityEventBase_GetValidMethodInfo_m6888_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEventBase_t1347_MethodInfos[] =
{
	&UnityEventBase__ctor_m6880_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2550_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2551_MethodInfo,
	&UnityEventBase_FindMethod_Impl_m7136_MethodInfo,
	&UnityEventBase_GetDelegate_m7137_MethodInfo,
	&UnityEventBase_FindMethod_m6881_MethodInfo,
	&UnityEventBase_FindMethod_m6882_MethodInfo,
	&UnityEventBase_DirtyPersistentCalls_m6883_MethodInfo,
	&UnityEventBase_RebuildPersistentCallsIfNeeded_m6884_MethodInfo,
	&UnityEventBase_AddCall_m6885_MethodInfo,
	&UnityEventBase_RemoveListener_m6886_MethodInfo,
	&UnityEventBase_Invoke_m6887_MethodInfo,
	&UnityEventBase_ToString_m2549_MethodInfo,
	&UnityEventBase_GetValidMethodInfo_m6888_MethodInfo,
	NULL
};
extern const MethodInfo UnityEventBase_ToString_m2549_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2550_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2551_MethodInfo;
static const Il2CppMethodReference UnityEventBase_t1347_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&UnityEventBase_ToString_m2549_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2550_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2551_MethodInfo,
	NULL,
	NULL,
};
static bool UnityEventBase_t1347_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* UnityEventBase_t1347_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t510_0_0_0,
};
static Il2CppInterfaceOffsetPair UnityEventBase_t1347_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t510_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEventBase_t1347_1_0_0;
struct UnityEventBase_t1347;
const Il2CppTypeDefinitionMetadata UnityEventBase_t1347_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, UnityEventBase_t1347_InterfacesTypeInfos/* implementedInterfaces */
	, UnityEventBase_t1347_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, UnityEventBase_t1347_VTable/* vtableMethods */
	, UnityEventBase_t1347_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1351/* fieldStart */

};
TypeInfo UnityEventBase_t1347_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventBase"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEventBase_t1347_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEventBase_t1347_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEventBase_t1347_0_0_0/* byval_arg */
	, &UnityEventBase_t1347_1_0_0/* this_arg */
	, &UnityEventBase_t1347_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventBase_t1347)/* instance_size */
	, sizeof (UnityEventBase_t1347)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 14/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEvent.h"
// Metadata Definition UnityEngine.Events.UnityEvent
extern TypeInfo UnityEvent_t256_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEventMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern const MethodInfo UnityEvent__ctor_m2059_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEvent__ctor_m2059/* method */
	, &UnityEvent_t256_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_t256_UnityEvent_FindMethod_Impl_m2574_ParameterInfos[] = 
{
	{"name", 0, 134219678, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219679, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_FindMethod_Impl_m2574_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, (methodPointerType)&UnityEvent_FindMethod_Impl_m2574/* method */
	, &UnityEvent_t256_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_t256_UnityEvent_FindMethod_Impl_m2574_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_t256_UnityEvent_GetDelegate_m2575_ParameterInfos[] = 
{
	{"target", 0, 134219680, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219681, 0, &MethodInfo_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_GetDelegate_m2575_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_GetDelegate_m2575/* method */
	, &UnityEvent_t256_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1339_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_t256_UnityEvent_GetDelegate_m2575_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern const MethodInfo UnityEvent_Invoke_m2062_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEvent_Invoke_m2062/* method */
	, &UnityEvent_t256_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_t256_MethodInfos[] =
{
	&UnityEvent__ctor_m2059_MethodInfo,
	&UnityEvent_FindMethod_Impl_m2574_MethodInfo,
	&UnityEvent_GetDelegate_m2575_MethodInfo,
	&UnityEvent_Invoke_m2062_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_FindMethod_Impl_m2574_MethodInfo;
extern const MethodInfo UnityEvent_GetDelegate_m2575_MethodInfo;
static const Il2CppMethodReference UnityEvent_t256_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&UnityEventBase_ToString_m2549_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2550_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2551_MethodInfo,
	&UnityEvent_FindMethod_Impl_m2574_MethodInfo,
	&UnityEvent_GetDelegate_m2575_MethodInfo,
};
static bool UnityEvent_t256_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_t256_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t510_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_t256_0_0_0;
extern const Il2CppType UnityEvent_t256_1_0_0;
struct UnityEvent_t256;
const Il2CppTypeDefinitionMetadata UnityEvent_t256_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_t256_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1347_0_0_0/* parent */
	, UnityEvent_t256_VTable/* vtableMethods */
	, UnityEvent_t256_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1355/* fieldStart */

};
TypeInfo UnityEvent_t256_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_t256_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_t256_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_t256_0_0_0/* byval_arg */
	, &UnityEvent_t256_1_0_0/* this_arg */
	, &UnityEvent_t256_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_t256)/* instance_size */
	, sizeof (UnityEvent_t256)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`1
extern TypeInfo UnityEvent_1_t1461_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_1_t1461_Il2CppGenericContainer;
extern TypeInfo UnityEvent_1_t1461_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_1_t1461_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_1_t1461_Il2CppGenericContainer, NULL, "T0", 0, 0 };
static const Il2CppGenericParameter* UnityEvent_1_t1461_Il2CppGenericParametersArray[1] = 
{
	&UnityEvent_1_t1461_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_1_t1461_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_1_t1461_il2cpp_TypeInfo, 1, 0, UnityEvent_1_t1461_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`1::.ctor()
extern const MethodInfo UnityEvent_1__ctor_m7138_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1461_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1553_0_0_0;
extern const Il2CppType UnityAction_1_t1553_0_0_0;
static const ParameterInfo UnityEvent_1_t1461_UnityEvent_1_AddListener_m7139_ParameterInfos[] = 
{
	{"call", 0, 134219682, 0, &UnityAction_1_t1553_0_0_0},
};
// System.Void UnityEngine.Events.UnityEvent`1::AddListener(UnityEngine.Events.UnityAction`1<T0>)
extern const MethodInfo UnityEvent_1_AddListener_m7139_MethodInfo = 
{
	"AddListener"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1461_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1461_UnityEvent_1_AddListener_m7139_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1553_0_0_0;
static const ParameterInfo UnityEvent_1_t1461_UnityEvent_1_RemoveListener_m7140_ParameterInfos[] = 
{
	{"call", 0, 134219683, 0, &UnityAction_1_t1553_0_0_0},
};
// System.Void UnityEngine.Events.UnityEvent`1::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
extern const MethodInfo UnityEvent_1_RemoveListener_m7140_MethodInfo = 
{
	"RemoveListener"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1461_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1461_UnityEvent_1_RemoveListener_m7140_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_1_t1461_UnityEvent_1_FindMethod_Impl_m7141_ParameterInfos[] = 
{
	{"name", 0, 134219684, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219685, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_1_FindMethod_Impl_m7141_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1461_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1461_UnityEvent_1_FindMethod_Impl_m7141_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_1_t1461_UnityEvent_1_GetDelegate_m7142_ParameterInfos[] = 
{
	{"target", 0, 134219686, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219687, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_1_GetDelegate_m7142_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1461_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1339_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1461_UnityEvent_1_GetDelegate_m7142_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1553_0_0_0;
static const ParameterInfo UnityEvent_1_t1461_UnityEvent_1_GetDelegate_m7143_ParameterInfos[] = 
{
	{"action", 0, 134219688, 0, &UnityAction_1_t1553_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
extern const MethodInfo UnityEvent_1_GetDelegate_m7143_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1461_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1339_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1461_UnityEvent_1_GetDelegate_m7143_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityEvent_1_t1461_gp_0_0_0_0;
extern const Il2CppType UnityEvent_1_t1461_gp_0_0_0_0;
static const ParameterInfo UnityEvent_1_t1461_UnityEvent_1_Invoke_m7144_ParameterInfos[] = 
{
	{"arg0", 0, 134219689, 0, &UnityEvent_1_t1461_gp_0_0_0_0},
};
// System.Void UnityEngine.Events.UnityEvent`1::Invoke(T0)
extern const MethodInfo UnityEvent_1_Invoke_m7144_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1461_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1461_UnityEvent_1_Invoke_m7144_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_1_t1461_MethodInfos[] =
{
	&UnityEvent_1__ctor_m7138_MethodInfo,
	&UnityEvent_1_AddListener_m7139_MethodInfo,
	&UnityEvent_1_RemoveListener_m7140_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m7141_MethodInfo,
	&UnityEvent_1_GetDelegate_m7142_MethodInfo,
	&UnityEvent_1_GetDelegate_m7143_MethodInfo,
	&UnityEvent_1_Invoke_m7144_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_1_FindMethod_Impl_m7141_MethodInfo;
extern const MethodInfo UnityEvent_1_GetDelegate_m7142_MethodInfo;
static const Il2CppMethodReference UnityEvent_1_t1461_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&UnityEventBase_ToString_m2549_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2550_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2551_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m7141_MethodInfo,
	&UnityEvent_1_GetDelegate_m7142_MethodInfo,
};
static bool UnityEvent_1_t1461_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_1_t1461_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t510_0_0_0, 4},
};
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m7243_GenericMethod;
extern const Il2CppType InvokableCall_1_t1554_0_0_0;
extern const Il2CppGenericMethod InvokableCall_1__ctor_m7244_GenericMethod;
extern const Il2CppGenericMethod InvokableCall_1__ctor_m7245_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_1_t1461_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityEvent_1_GetDelegate_m7243_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_1_t1461_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_1_t1554_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m7244_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_1__ctor_m7245_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&UnityEvent_1_t1461_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_1_t1461_0_0_0;
extern const Il2CppType UnityEvent_1_t1461_1_0_0;
struct UnityEvent_1_t1461;
const Il2CppTypeDefinitionMetadata UnityEvent_1_t1461_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_1_t1461_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1347_0_0_0/* parent */
	, UnityEvent_1_t1461_VTable/* vtableMethods */
	, UnityEvent_1_t1461_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_1_t1461_RGCTXData/* rgctxDefinition */
	, 1356/* fieldStart */

};
TypeInfo UnityEvent_1_t1461_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_1_t1461_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_1_t1461_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_1_t1461_0_0_0/* byval_arg */
	, &UnityEvent_1_t1461_1_0_0/* this_arg */
	, &UnityEvent_1_t1461_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_1_t1461_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`2
extern TypeInfo UnityEvent_2_t1462_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_2_t1462_Il2CppGenericContainer;
extern TypeInfo UnityEvent_2_t1462_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_2_t1462_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_2_t1462_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityEvent_2_t1462_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_2_t1462_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_2_t1462_Il2CppGenericContainer, NULL, "T1", 1, 0 };
static const Il2CppGenericParameter* UnityEvent_2_t1462_Il2CppGenericParametersArray[2] = 
{
	&UnityEvent_2_t1462_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_2_t1462_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_2_t1462_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_2_t1462_il2cpp_TypeInfo, 2, 0, UnityEvent_2_t1462_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`2::.ctor()
extern const MethodInfo UnityEvent_2__ctor_m7145_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1462_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_2_t1462_UnityEvent_2_FindMethod_Impl_m7146_ParameterInfos[] = 
{
	{"name", 0, 134219690, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219691, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_2_FindMethod_Impl_m7146_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1462_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_2_t1462_UnityEvent_2_FindMethod_Impl_m7146_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_2_t1462_UnityEvent_2_GetDelegate_m7147_ParameterInfos[] = 
{
	{"target", 0, 134219692, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219693, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_2_GetDelegate_m7147_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1462_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1339_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_2_t1462_UnityEvent_2_GetDelegate_m7147_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_2_t1462_MethodInfos[] =
{
	&UnityEvent_2__ctor_m7145_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m7146_MethodInfo,
	&UnityEvent_2_GetDelegate_m7147_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_2_FindMethod_Impl_m7146_MethodInfo;
extern const MethodInfo UnityEvent_2_GetDelegate_m7147_MethodInfo;
static const Il2CppMethodReference UnityEvent_2_t1462_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&UnityEventBase_ToString_m2549_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2550_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2551_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m7146_MethodInfo,
	&UnityEvent_2_GetDelegate_m7147_MethodInfo,
};
static bool UnityEvent_2_t1462_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_2_t1462_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t510_0_0_0, 4},
};
extern const Il2CppType UnityEvent_2_t1462_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t1462_gp_1_0_0_0;
extern const Il2CppType InvokableCall_2_t1557_0_0_0;
extern const Il2CppGenericMethod InvokableCall_2__ctor_m7246_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_2_t1462_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_2_t1462_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_2_t1462_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_2_t1557_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_2__ctor_m7246_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_2_t1462_0_0_0;
extern const Il2CppType UnityEvent_2_t1462_1_0_0;
struct UnityEvent_2_t1462;
const Il2CppTypeDefinitionMetadata UnityEvent_2_t1462_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_2_t1462_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1347_0_0_0/* parent */
	, UnityEvent_2_t1462_VTable/* vtableMethods */
	, UnityEvent_2_t1462_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_2_t1462_RGCTXData/* rgctxDefinition */
	, 1357/* fieldStart */

};
TypeInfo UnityEvent_2_t1462_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_2_t1462_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_2_t1462_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_2_t1462_0_0_0/* byval_arg */
	, &UnityEvent_2_t1462_1_0_0/* this_arg */
	, &UnityEvent_2_t1462_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_2_t1462_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`3
extern TypeInfo UnityEvent_3_t1463_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_3_t1463_Il2CppGenericContainer;
extern TypeInfo UnityEvent_3_t1463_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_3_t1463_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_3_t1463_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityEvent_3_t1463_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_3_t1463_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_3_t1463_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityEvent_3_t1463_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_3_t1463_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_3_t1463_Il2CppGenericContainer, NULL, "T2", 2, 0 };
static const Il2CppGenericParameter* UnityEvent_3_t1463_Il2CppGenericParametersArray[3] = 
{
	&UnityEvent_3_t1463_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_3_t1463_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_3_t1463_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_3_t1463_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_3_t1463_il2cpp_TypeInfo, 3, 0, UnityEvent_3_t1463_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`3::.ctor()
extern const MethodInfo UnityEvent_3__ctor_m7148_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1463_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_3_t1463_UnityEvent_3_FindMethod_Impl_m7149_ParameterInfos[] = 
{
	{"name", 0, 134219694, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219695, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_3_FindMethod_Impl_m7149_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1463_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_3_t1463_UnityEvent_3_FindMethod_Impl_m7149_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_3_t1463_UnityEvent_3_GetDelegate_m7150_ParameterInfos[] = 
{
	{"target", 0, 134219696, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219697, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_3_GetDelegate_m7150_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1463_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1339_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_3_t1463_UnityEvent_3_GetDelegate_m7150_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_3_t1463_MethodInfos[] =
{
	&UnityEvent_3__ctor_m7148_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m7149_MethodInfo,
	&UnityEvent_3_GetDelegate_m7150_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_3_FindMethod_Impl_m7149_MethodInfo;
extern const MethodInfo UnityEvent_3_GetDelegate_m7150_MethodInfo;
static const Il2CppMethodReference UnityEvent_3_t1463_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&UnityEventBase_ToString_m2549_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2550_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2551_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m7149_MethodInfo,
	&UnityEvent_3_GetDelegate_m7150_MethodInfo,
};
static bool UnityEvent_3_t1463_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_3_t1463_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t510_0_0_0, 4},
};
extern const Il2CppType UnityEvent_3_t1463_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t1463_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t1463_gp_2_0_0_0;
extern const Il2CppType InvokableCall_3_t1561_0_0_0;
extern const Il2CppGenericMethod InvokableCall_3__ctor_m7247_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_3_t1463_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_3_t1463_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_3_t1463_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_3_t1463_gp_2_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_3_t1561_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_3__ctor_m7247_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_3_t1463_0_0_0;
extern const Il2CppType UnityEvent_3_t1463_1_0_0;
struct UnityEvent_3_t1463;
const Il2CppTypeDefinitionMetadata UnityEvent_3_t1463_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_3_t1463_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1347_0_0_0/* parent */
	, UnityEvent_3_t1463_VTable/* vtableMethods */
	, UnityEvent_3_t1463_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_3_t1463_RGCTXData/* rgctxDefinition */
	, 1358/* fieldStart */

};
TypeInfo UnityEvent_3_t1463_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_3_t1463_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_3_t1463_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_3_t1463_0_0_0/* byval_arg */
	, &UnityEvent_3_t1463_1_0_0/* this_arg */
	, &UnityEvent_3_t1463_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_3_t1463_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityEvent`4
extern TypeInfo UnityEvent_4_t1464_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityEvent_4_t1464_Il2CppGenericContainer;
extern TypeInfo UnityEvent_4_t1464_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t1464_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t1464_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityEvent_4_t1464_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t1464_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t1464_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityEvent_4_t1464_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t1464_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t1464_Il2CppGenericContainer, NULL, "T2", 2, 0 };
extern TypeInfo UnityEvent_4_t1464_gp_T3_3_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityEvent_4_t1464_gp_T3_3_il2cpp_TypeInfo_GenericParamFull = { &UnityEvent_4_t1464_Il2CppGenericContainer, NULL, "T3", 3, 0 };
static const Il2CppGenericParameter* UnityEvent_4_t1464_Il2CppGenericParametersArray[4] = 
{
	&UnityEvent_4_t1464_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1464_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1464_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1464_gp_T3_3_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityEvent_4_t1464_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityEvent_4_t1464_il2cpp_TypeInfo, 4, 0, UnityEvent_4_t1464_Il2CppGenericParametersArray };
// System.Void UnityEngine.Events.UnityEvent`4::.ctor()
extern const MethodInfo UnityEvent_4__ctor_m7151_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1464_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityEvent_4_t1464_UnityEvent_4_FindMethod_Impl_m7152_ParameterInfos[] = 
{
	{"name", 0, 134219698, 0, &String_t_0_0_0},
	{"targetObj", 1, 134219699, 0, &Object_t_0_0_0},
};
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4::FindMethod_Impl(System.String,System.Object)
extern const MethodInfo UnityEvent_4_FindMethod_Impl_m7152_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1464_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_4_t1464_UnityEvent_4_FindMethod_Impl_m7152_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1882/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
static const ParameterInfo UnityEvent_4_t1464_UnityEvent_4_GetDelegate_m7153_ParameterInfos[] = 
{
	{"target", 0, 134219700, 0, &Object_t_0_0_0},
	{"theFunction", 1, 134219701, 0, &MethodInfo_t_0_0_0},
};
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern const MethodInfo UnityEvent_4_GetDelegate_m7153_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1464_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1339_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_4_t1464_UnityEvent_4_GetDelegate_m7153_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1883/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityEvent_4_t1464_MethodInfos[] =
{
	&UnityEvent_4__ctor_m7151_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m7152_MethodInfo,
	&UnityEvent_4_GetDelegate_m7153_MethodInfo,
	NULL
};
extern const MethodInfo UnityEvent_4_FindMethod_Impl_m7152_MethodInfo;
extern const MethodInfo UnityEvent_4_GetDelegate_m7153_MethodInfo;
static const Il2CppMethodReference UnityEvent_4_t1464_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&UnityEventBase_ToString_m2549_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2550_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2551_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m7152_MethodInfo,
	&UnityEvent_4_GetDelegate_m7153_MethodInfo,
};
static bool UnityEvent_4_t1464_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityEvent_4_t1464_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t510_0_0_0, 4},
};
extern const Il2CppType UnityEvent_4_t1464_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t1464_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t1464_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t1464_gp_3_0_0_0;
extern const Il2CppType InvokableCall_4_t1566_0_0_0;
extern const Il2CppGenericMethod InvokableCall_4__ctor_m7248_GenericMethod;
static Il2CppRGCTXDefinition UnityEvent_4_t1464_RGCTXData[7] = 
{
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t1464_gp_0_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t1464_gp_1_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t1464_gp_2_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_TYPE, (void*)&UnityEvent_4_t1464_gp_3_0_0_0 }/* Type */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&InvokableCall_4_t1566_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &InvokableCall_4__ctor_m7248_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityEvent_4_t1464_0_0_0;
extern const Il2CppType UnityEvent_4_t1464_1_0_0;
struct UnityEvent_4_t1464;
const Il2CppTypeDefinitionMetadata UnityEvent_4_t1464_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityEvent_4_t1464_InterfacesOffsets/* interfaceOffsets */
	, &UnityEventBase_t1347_0_0_0/* parent */
	, UnityEvent_4_t1464_VTable/* vtableMethods */
	, UnityEvent_4_t1464_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, UnityEvent_4_t1464_RGCTXData/* rgctxDefinition */
	, 1359/* fieldStart */

};
TypeInfo UnityEvent_4_t1464_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_4_t1464_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityEvent_4_t1464_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityEvent_4_t1464_0_0_0/* byval_arg */
	, &UnityEvent_4_t1464_1_0_0/* this_arg */
	, &UnityEvent_4_t1464_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityEvent_4_t1464_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialog.h"
// Metadata Definition UnityEngine.UserAuthorizationDialog
extern TypeInfo UserAuthorizationDialog_t1348_il2cpp_TypeInfo;
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialogMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::.ctor()
extern const MethodInfo UserAuthorizationDialog__ctor_m6889_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserAuthorizationDialog__ctor_m6889/* method */
	, &UserAuthorizationDialog_t1348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::Start()
extern const MethodInfo UserAuthorizationDialog_Start_m6890_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&UserAuthorizationDialog_Start_m6890/* method */
	, &UserAuthorizationDialog_t1348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::OnGUI()
extern const MethodInfo UserAuthorizationDialog_OnGUI_m6891_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&UserAuthorizationDialog_OnGUI_m6891/* method */
	, &UserAuthorizationDialog_t1348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1886/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo UserAuthorizationDialog_t1348_UserAuthorizationDialog_DoUserAuthorizationDialog_m6892_ParameterInfos[] = 
{
	{"windowID", 0, 134219702, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::DoUserAuthorizationDialog(System.Int32)
extern const MethodInfo UserAuthorizationDialog_DoUserAuthorizationDialog_m6892_MethodInfo = 
{
	"DoUserAuthorizationDialog"/* name */
	, (methodPointerType)&UserAuthorizationDialog_DoUserAuthorizationDialog_m6892/* method */
	, &UserAuthorizationDialog_t1348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, UserAuthorizationDialog_t1348_UserAuthorizationDialog_DoUserAuthorizationDialog_m6892_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UserAuthorizationDialog_t1348_MethodInfos[] =
{
	&UserAuthorizationDialog__ctor_m6889_MethodInfo,
	&UserAuthorizationDialog_Start_m6890_MethodInfo,
	&UserAuthorizationDialog_OnGUI_m6891_MethodInfo,
	&UserAuthorizationDialog_DoUserAuthorizationDialog_m6892_MethodInfo,
	NULL
};
static const Il2CppMethodReference UserAuthorizationDialog_t1348_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
};
static bool UserAuthorizationDialog_t1348_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UserAuthorizationDialog_t1348_0_0_0;
extern const Il2CppType UserAuthorizationDialog_t1348_1_0_0;
extern const Il2CppType MonoBehaviour_t7_0_0_0;
struct UserAuthorizationDialog_t1348;
const Il2CppTypeDefinitionMetadata UserAuthorizationDialog_t1348_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &MonoBehaviour_t7_0_0_0/* parent */
	, UserAuthorizationDialog_t1348_VTable/* vtableMethods */
	, UserAuthorizationDialog_t1348_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1360/* fieldStart */

};
TypeInfo UserAuthorizationDialog_t1348_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserAuthorizationDialog"/* name */
	, "UnityEngine"/* namespaze */
	, UserAuthorizationDialog_t1348_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UserAuthorizationDialog_t1348_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 821/* custom_attributes_cache */
	, &UserAuthorizationDialog_t1348_0_0_0/* byval_arg */
	, &UserAuthorizationDialog_t1348_1_0_0/* this_arg */
	, &UserAuthorizationDialog_t1348_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserAuthorizationDialog_t1348)/* instance_size */
	, sizeof (UserAuthorizationDialog_t1348)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
// Metadata Definition UnityEngine.Internal.DefaultValueAttribute
extern TypeInfo DefaultValueAttribute_t1349_il2cpp_TypeInfo;
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo DefaultValueAttribute_t1349_DefaultValueAttribute__ctor_m6893_ParameterInfos[] = 
{
	{"value", 0, 134219703, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern const MethodInfo DefaultValueAttribute__ctor_m6893_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultValueAttribute__ctor_m6893/* method */
	, &DefaultValueAttribute_t1349_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, DefaultValueAttribute_t1349_DefaultValueAttribute__ctor_m6893_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern const MethodInfo DefaultValueAttribute_get_Value_m6894_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&DefaultValueAttribute_get_Value_m6894/* method */
	, &DefaultValueAttribute_t1349_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo DefaultValueAttribute_t1349_DefaultValueAttribute_Equals_m6895_ParameterInfos[] = 
{
	{"obj", 0, 134219704, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern const MethodInfo DefaultValueAttribute_Equals_m6895_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&DefaultValueAttribute_Equals_m6895/* method */
	, &DefaultValueAttribute_t1349_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, DefaultValueAttribute_t1349_DefaultValueAttribute_Equals_m6895_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern const MethodInfo DefaultValueAttribute_GetHashCode_m6896_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&DefaultValueAttribute_GetHashCode_m6896/* method */
	, &DefaultValueAttribute_t1349_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* DefaultValueAttribute_t1349_MethodInfos[] =
{
	&DefaultValueAttribute__ctor_m6893_MethodInfo,
	&DefaultValueAttribute_get_Value_m6894_MethodInfo,
	&DefaultValueAttribute_Equals_m6895_MethodInfo,
	&DefaultValueAttribute_GetHashCode_m6896_MethodInfo,
	NULL
};
extern const MethodInfo DefaultValueAttribute_get_Value_m6894_MethodInfo;
static const PropertyInfo DefaultValueAttribute_t1349____Value_PropertyInfo = 
{
	&DefaultValueAttribute_t1349_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &DefaultValueAttribute_get_Value_m6894_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* DefaultValueAttribute_t1349_PropertyInfos[] =
{
	&DefaultValueAttribute_t1349____Value_PropertyInfo,
	NULL
};
extern const MethodInfo DefaultValueAttribute_Equals_m6895_MethodInfo;
extern const MethodInfo DefaultValueAttribute_GetHashCode_m6896_MethodInfo;
static const Il2CppMethodReference DefaultValueAttribute_t1349_VTable[] =
{
	&DefaultValueAttribute_Equals_m6895_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&DefaultValueAttribute_GetHashCode_m6896_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool DefaultValueAttribute_t1349_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair DefaultValueAttribute_t1349_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType DefaultValueAttribute_t1349_0_0_0;
extern const Il2CppType DefaultValueAttribute_t1349_1_0_0;
struct DefaultValueAttribute_t1349;
const Il2CppTypeDefinitionMetadata DefaultValueAttribute_t1349_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, DefaultValueAttribute_t1349_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, DefaultValueAttribute_t1349_VTable/* vtableMethods */
	, DefaultValueAttribute_t1349_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1364/* fieldStart */

};
TypeInfo DefaultValueAttribute_t1349_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultValueAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, DefaultValueAttribute_t1349_MethodInfos/* methods */
	, DefaultValueAttribute_t1349_PropertyInfos/* properties */
	, NULL/* events */
	, &DefaultValueAttribute_t1349_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 822/* custom_attributes_cache */
	, &DefaultValueAttribute_t1349_0_0_0/* byval_arg */
	, &DefaultValueAttribute_t1349_1_0_0/* this_arg */
	, &DefaultValueAttribute_t1349_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultValueAttribute_t1349)/* instance_size */
	, sizeof (DefaultValueAttribute_t1349)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
// Metadata Definition UnityEngine.Internal.ExcludeFromDocsAttribute
extern TypeInfo ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo;
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern const MethodInfo ExcludeFromDocsAttribute__ctor_m6897_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExcludeFromDocsAttribute__ctor_m6897/* method */
	, &ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExcludeFromDocsAttribute_t1350_MethodInfos[] =
{
	&ExcludeFromDocsAttribute__ctor_m6897_MethodInfo,
	NULL
};
static const Il2CppMethodReference ExcludeFromDocsAttribute_t1350_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ExcludeFromDocsAttribute_t1350_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ExcludeFromDocsAttribute_t1350_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType ExcludeFromDocsAttribute_t1350_0_0_0;
extern const Il2CppType ExcludeFromDocsAttribute_t1350_1_0_0;
struct ExcludeFromDocsAttribute_t1350;
const Il2CppTypeDefinitionMetadata ExcludeFromDocsAttribute_t1350_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExcludeFromDocsAttribute_t1350_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, ExcludeFromDocsAttribute_t1350_VTable/* vtableMethods */
	, ExcludeFromDocsAttribute_t1350_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExcludeFromDocsAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, ExcludeFromDocsAttribute_t1350_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExcludeFromDocsAttribute_t1350_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 823/* custom_attributes_cache */
	, &ExcludeFromDocsAttribute_t1350_0_0_0/* byval_arg */
	, &ExcludeFromDocsAttribute_t1350_1_0_0/* this_arg */
	, &ExcludeFromDocsAttribute_t1350_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExcludeFromDocsAttribute_t1350)/* instance_size */
	, sizeof (ExcludeFromDocsAttribute_t1350)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// Metadata Definition UnityEngine.Serialization.FormerlySerializedAsAttribute
extern TypeInfo FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo FormerlySerializedAsAttribute_t492_FormerlySerializedAsAttribute__ctor_m2456_ParameterInfos[] = 
{
	{"oldName", 0, 134219705, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern const MethodInfo FormerlySerializedAsAttribute__ctor_m2456_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormerlySerializedAsAttribute__ctor_m2456/* method */
	, &FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, FormerlySerializedAsAttribute_t492_FormerlySerializedAsAttribute__ctor_m2456_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* FormerlySerializedAsAttribute_t492_MethodInfos[] =
{
	&FormerlySerializedAsAttribute__ctor_m2456_MethodInfo,
	NULL
};
static const Il2CppMethodReference FormerlySerializedAsAttribute_t492_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool FormerlySerializedAsAttribute_t492_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FormerlySerializedAsAttribute_t492_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType FormerlySerializedAsAttribute_t492_0_0_0;
extern const Il2CppType FormerlySerializedAsAttribute_t492_1_0_0;
struct FormerlySerializedAsAttribute_t492;
const Il2CppTypeDefinitionMetadata FormerlySerializedAsAttribute_t492_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FormerlySerializedAsAttribute_t492_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, FormerlySerializedAsAttribute_t492_VTable/* vtableMethods */
	, FormerlySerializedAsAttribute_t492_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1365/* fieldStart */

};
TypeInfo FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormerlySerializedAsAttribute"/* name */
	, "UnityEngine.Serialization"/* namespaze */
	, FormerlySerializedAsAttribute_t492_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &FormerlySerializedAsAttribute_t492_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 824/* custom_attributes_cache */
	, &FormerlySerializedAsAttribute_t492_0_0_0/* byval_arg */
	, &FormerlySerializedAsAttribute_t492_1_0_0/* this_arg */
	, &FormerlySerializedAsAttribute_t492_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormerlySerializedAsAttribute_t492)/* instance_size */
	, sizeof (FormerlySerializedAsAttribute_t492)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"
// Metadata Definition UnityEngineInternal.TypeInferenceRules
extern TypeInfo TypeInferenceRules_t1351_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRulesMethodDeclarations.h"
static const MethodInfo* TypeInferenceRules_t1351_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference TypeInferenceRules_t1351_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool TypeInferenceRules_t1351_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeInferenceRules_t1351_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TypeInferenceRules_t1351_0_0_0;
extern const Il2CppType TypeInferenceRules_t1351_1_0_0;
const Il2CppTypeDefinitionMetadata TypeInferenceRules_t1351_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInferenceRules_t1351_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, TypeInferenceRules_t1351_VTable/* vtableMethods */
	, TypeInferenceRules_t1351_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1366/* fieldStart */

};
TypeInfo TypeInferenceRules_t1351_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRules"/* name */
	, "UnityEngineInternal"/* namespaze */
	, TypeInferenceRules_t1351_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &TypeInferenceRules_t1351_0_0_0/* byval_arg */
	, &TypeInferenceRules_t1351_1_0_0/* this_arg */
	, &TypeInferenceRules_t1351_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRules_t1351)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (TypeInferenceRules_t1351)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
// Metadata Definition UnityEngineInternal.TypeInferenceRuleAttribute
extern TypeInfo TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"
extern const Il2CppType TypeInferenceRules_t1351_0_0_0;
static const ParameterInfo TypeInferenceRuleAttribute_t1352_TypeInferenceRuleAttribute__ctor_m6898_ParameterInfos[] = 
{
	{"rule", 0, 134219706, 0, &TypeInferenceRules_t1351_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern const MethodInfo TypeInferenceRuleAttribute__ctor_m6898_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute__ctor_m6898/* method */
	, &TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, TypeInferenceRuleAttribute_t1352_TypeInferenceRuleAttribute__ctor_m6898_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo TypeInferenceRuleAttribute_t1352_TypeInferenceRuleAttribute__ctor_m6899_ParameterInfos[] = 
{
	{"rule", 0, 134219707, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern const MethodInfo TypeInferenceRuleAttribute__ctor_m6899_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute__ctor_m6899/* method */
	, &TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, TypeInferenceRuleAttribute_t1352_TypeInferenceRuleAttribute__ctor_m6899_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern const MethodInfo TypeInferenceRuleAttribute_ToString_m6900_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute_ToString_m6900/* method */
	, &TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* TypeInferenceRuleAttribute_t1352_MethodInfos[] =
{
	&TypeInferenceRuleAttribute__ctor_m6898_MethodInfo,
	&TypeInferenceRuleAttribute__ctor_m6899_MethodInfo,
	&TypeInferenceRuleAttribute_ToString_m6900_MethodInfo,
	NULL
};
extern const MethodInfo TypeInferenceRuleAttribute_ToString_m6900_MethodInfo;
static const Il2CppMethodReference TypeInferenceRuleAttribute_t1352_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&TypeInferenceRuleAttribute_ToString_m6900_MethodInfo,
};
static bool TypeInferenceRuleAttribute_t1352_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair TypeInferenceRuleAttribute_t1352_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType TypeInferenceRuleAttribute_t1352_0_0_0;
extern const Il2CppType TypeInferenceRuleAttribute_t1352_1_0_0;
struct TypeInferenceRuleAttribute_t1352;
const Il2CppTypeDefinitionMetadata TypeInferenceRuleAttribute_t1352_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, TypeInferenceRuleAttribute_t1352_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, TypeInferenceRuleAttribute_t1352_VTable/* vtableMethods */
	, TypeInferenceRuleAttribute_t1352_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 1371/* fieldStart */

};
TypeInfo TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRuleAttribute"/* name */
	, "UnityEngineInternal"/* namespaze */
	, TypeInferenceRuleAttribute_t1352_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &TypeInferenceRuleAttribute_t1352_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 825/* custom_attributes_cache */
	, &TypeInferenceRuleAttribute_t1352_0_0_0/* byval_arg */
	, &TypeInferenceRuleAttribute_t1352_1_0_0/* this_arg */
	, &TypeInferenceRuleAttribute_t1352_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRuleAttribute_t1352)/* instance_size */
	, sizeof (TypeInferenceRuleAttribute_t1352)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
// Metadata Definition UnityEngineInternal.GenericStack
extern TypeInfo GenericStack_t1167_il2cpp_TypeInfo;
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.GenericStack::.ctor()
extern const MethodInfo GenericStack__ctor_m6901_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GenericStack__ctor_m6901/* method */
	, &GenericStack_t1167_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GenericStack_t1167_MethodInfos[] =
{
	&GenericStack__ctor_m6901_MethodInfo,
	NULL
};
extern const MethodInfo Stack_GetEnumerator_m7249_MethodInfo;
extern const MethodInfo Stack_get_Count_m7250_MethodInfo;
extern const MethodInfo Stack_get_IsSynchronized_m7251_MethodInfo;
extern const MethodInfo Stack_get_SyncRoot_m7252_MethodInfo;
extern const MethodInfo Stack_CopyTo_m7253_MethodInfo;
extern const MethodInfo Stack_Clear_m7254_MethodInfo;
extern const MethodInfo Stack_Peek_m7255_MethodInfo;
extern const MethodInfo Stack_Pop_m7256_MethodInfo;
extern const MethodInfo Stack_Push_m7257_MethodInfo;
static const Il2CppMethodReference GenericStack_t1167_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&Stack_GetEnumerator_m7249_MethodInfo,
	&Stack_get_Count_m7250_MethodInfo,
	&Stack_get_IsSynchronized_m7251_MethodInfo,
	&Stack_get_SyncRoot_m7252_MethodInfo,
	&Stack_CopyTo_m7253_MethodInfo,
	&Stack_get_Count_m7250_MethodInfo,
	&Stack_get_IsSynchronized_m7251_MethodInfo,
	&Stack_get_SyncRoot_m7252_MethodInfo,
	&Stack_Clear_m7254_MethodInfo,
	&Stack_CopyTo_m7253_MethodInfo,
	&Stack_GetEnumerator_m7249_MethodInfo,
	&Stack_Peek_m7255_MethodInfo,
	&Stack_Pop_m7256_MethodInfo,
	&Stack_Push_m7257_MethodInfo,
};
static bool GenericStack_t1167_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICollection_t1513_0_0_0;
static Il2CppInterfaceOffsetPair GenericStack_t1167_InterfacesOffsets[] = 
{
	{ &IEnumerable_t550_0_0_0, 4},
	{ &ICloneable_t512_0_0_0, 5},
	{ &ICollection_t1513_0_0_0, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType GenericStack_t1167_0_0_0;
extern const Il2CppType GenericStack_t1167_1_0_0;
extern const Il2CppType Stack_t1353_0_0_0;
struct GenericStack_t1167;
const Il2CppTypeDefinitionMetadata GenericStack_t1167_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GenericStack_t1167_InterfacesOffsets/* interfaceOffsets */
	, &Stack_t1353_0_0_0/* parent */
	, GenericStack_t1167_VTable/* vtableMethods */
	, GenericStack_t1167_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo GenericStack_t1167_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericStack"/* name */
	, "UnityEngineInternal"/* namespaze */
	, GenericStack_t1167_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &GenericStack_t1167_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &GenericStack_t1167_0_0_0/* byval_arg */
	, &GenericStack_t1167_1_0_0/* this_arg */
	, &GenericStack_t1167_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericStack_t1167)/* instance_size */
	, sizeof (GenericStack_t1167)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityAction.h"
// Metadata Definition UnityEngine.Events.UnityAction
extern TypeInfo UnityAction_t275_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityActionMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_t275_UnityAction__ctor_m2209_ParameterInfos[] = 
{
	{"object", 0, 134219708, 0, &Object_t_0_0_0},
	{"method", 1, 134219709, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction__ctor_m2209_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction__ctor_m2209/* method */
	, &UnityAction_t275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, UnityAction_t275_UnityAction__ctor_m2209_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern const MethodInfo UnityAction_Invoke_m6902_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_Invoke_m6902/* method */
	, &UnityAction_t275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_t275_UnityAction_BeginInvoke_m6903_ParameterInfos[] = 
{
	{"callback", 0, 134219710, 0, &AsyncCallback_t305_0_0_0},
	{"object", 1, 134219711, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_BeginInvoke_m6903_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_BeginInvoke_m6903/* method */
	, &UnityAction_t275_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_t275_UnityAction_BeginInvoke_m6903_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo UnityAction_t275_UnityAction_EndInvoke_m6904_ParameterInfos[] = 
{
	{"result", 0, 134219712, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_EndInvoke_m6904_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_EndInvoke_m6904/* method */
	, &UnityAction_t275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, UnityAction_t275_UnityAction_EndInvoke_m6904_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_t275_MethodInfos[] =
{
	&UnityAction__ctor_m2209_MethodInfo,
	&UnityAction_Invoke_m6902_MethodInfo,
	&UnityAction_BeginInvoke_m6903_MethodInfo,
	&UnityAction_EndInvoke_m6904_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_Invoke_m6902_MethodInfo;
extern const MethodInfo UnityAction_BeginInvoke_m6903_MethodInfo;
extern const MethodInfo UnityAction_EndInvoke_m6904_MethodInfo;
static const Il2CppMethodReference UnityAction_t275_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&UnityAction_Invoke_m6902_MethodInfo,
	&UnityAction_BeginInvoke_m6903_MethodInfo,
	&UnityAction_EndInvoke_m6904_MethodInfo,
};
static bool UnityAction_t275_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_t275_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_t275_0_0_0;
extern const Il2CppType UnityAction_t275_1_0_0;
struct UnityAction_t275;
const Il2CppTypeDefinitionMetadata UnityAction_t275_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_t275_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, UnityAction_t275_VTable/* vtableMethods */
	, UnityAction_t275_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_t275_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_t275_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_t275_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_t275_0_0_0/* byval_arg */
	, &UnityAction_t275_1_0_0/* this_arg */
	, &UnityAction_t275_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_UnityAction_t275/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_t275)/* instance_size */
	, sizeof (UnityAction_t275)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`1
extern TypeInfo UnityAction_1_t1465_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_1_t1465_Il2CppGenericContainer;
extern TypeInfo UnityAction_1_t1465_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_1_t1465_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_1_t1465_Il2CppGenericContainer, NULL, "T0", 0, 0 };
static const Il2CppGenericParameter* UnityAction_1_t1465_Il2CppGenericParametersArray[1] = 
{
	&UnityAction_1_t1465_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_1_t1465_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_1_t1465_il2cpp_TypeInfo, 1, 0, UnityAction_1_t1465_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_1_t1465_UnityAction_1__ctor_m7154_ParameterInfos[] = 
{
	{"object", 0, 134219713, 0, &Object_t_0_0_0},
	{"method", 1, 134219714, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`1::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_1__ctor_m7154_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_1_t1465_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1465_UnityAction_1__ctor_m7154_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1465_gp_0_0_0_0;
extern const Il2CppType UnityAction_1_t1465_gp_0_0_0_0;
static const ParameterInfo UnityAction_1_t1465_UnityAction_1_Invoke_m7155_ParameterInfos[] = 
{
	{"arg0", 0, 134219715, 0, &UnityAction_1_t1465_gp_0_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`1::Invoke(T0)
extern const MethodInfo UnityAction_1_Invoke_m7155_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1465_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1465_UnityAction_1_Invoke_m7155_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_1_t1465_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_1_t1465_UnityAction_1_BeginInvoke_m7156_ParameterInfos[] = 
{
	{"arg0", 0, 134219716, 0, &UnityAction_1_t1465_gp_0_0_0_0},
	{"callback", 1, 134219717, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134219718, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Events.UnityAction`1::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_1_BeginInvoke_m7156_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1465_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1465_UnityAction_1_BeginInvoke_m7156_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo UnityAction_1_t1465_UnityAction_1_EndInvoke_m7157_ParameterInfos[] = 
{
	{"result", 0, 134219719, 0, &IAsyncResult_t304_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`1::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_1_EndInvoke_m7157_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1465_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1465_UnityAction_1_EndInvoke_m7157_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_1_t1465_MethodInfos[] =
{
	&UnityAction_1__ctor_m7154_MethodInfo,
	&UnityAction_1_Invoke_m7155_MethodInfo,
	&UnityAction_1_BeginInvoke_m7156_MethodInfo,
	&UnityAction_1_EndInvoke_m7157_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_1_Invoke_m7155_MethodInfo;
extern const MethodInfo UnityAction_1_BeginInvoke_m7156_MethodInfo;
extern const MethodInfo UnityAction_1_EndInvoke_m7157_MethodInfo;
static const Il2CppMethodReference UnityAction_1_t1465_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&UnityAction_1_Invoke_m7155_MethodInfo,
	&UnityAction_1_BeginInvoke_m7156_MethodInfo,
	&UnityAction_1_EndInvoke_m7157_MethodInfo,
};
static bool UnityAction_1_t1465_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t1465_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_1_t1465_0_0_0;
extern const Il2CppType UnityAction_1_t1465_1_0_0;
struct UnityAction_1_t1465;
const Il2CppTypeDefinitionMetadata UnityAction_1_t1465_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_1_t1465_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, UnityAction_1_t1465_VTable/* vtableMethods */
	, UnityAction_1_t1465_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_1_t1465_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t1465_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_1_t1465_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_1_t1465_0_0_0/* byval_arg */
	, &UnityAction_1_t1465_1_0_0/* this_arg */
	, &UnityAction_1_t1465_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_1_t1465_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
