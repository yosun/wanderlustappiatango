﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenManager
struct TweenManager_t980;
// DG.Tweening.Sequence
struct Sequence_t122;
// DG.Tweening.Tween
struct Tween_t934;
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t946;
// DG.Tweening.UpdateType
#include "DOTween_DG_Tweening_UpdateType.h"
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.TweenManager/CapacityIncreaseMode
#include "DOTween_DG_Tweening_Core_TweenManager_CapacityIncreaseMode.h"

// DG.Tweening.Sequence DG.Tweening.Core.TweenManager::GetSequence()
extern "C" Sequence_t122 * TweenManager_GetSequence_m5410 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::AddActiveTweenToSequence(DG.Tweening.Tween)
extern "C" void TweenManager_AddActiveTweenToSequence_m5411 (Object_t * __this /* static, unused */, Tween_t934 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::Despawn(DG.Tweening.Tween,System.Boolean)
extern "C" void TweenManager_Despawn_m5412 (Object_t * __this /* static, unused */, Tween_t934 * ___t, bool ___modifyActiveLists, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::SetCapacities(System.Int32,System.Int32)
extern "C" void TweenManager_SetCapacities_m5413 (Object_t * __this /* static, unused */, int32_t ___tweenersCapacity, int32_t ___sequencesCapacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.Core.TweenManager::Validate()
extern "C" int32_t TweenManager_Validate_m5414 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::Update(DG.Tweening.UpdateType,System.Single,System.Single)
extern "C" void TweenManager_Update_m5415 (Object_t * __this /* static, unused */, int32_t ___updateType, float ___deltaTime, float ___independentTime, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.Core.TweenManager::Goto(DG.Tweening.Tween,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateMode)
extern "C" bool TweenManager_Goto_m5416 (Object_t * __this /* static, unused */, Tween_t934 * ___t, float ___to, bool ___andPlay, int32_t ___updateMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::MarkForKilling(DG.Tweening.Tween)
extern "C" void TweenManager_MarkForKilling_m5417 (Object_t * __this /* static, unused */, Tween_t934 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::AddActiveTween(DG.Tweening.Tween)
extern "C" void TweenManager_AddActiveTween_m5418 (Object_t * __this /* static, unused */, Tween_t934 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::ReorganizeActiveTweens()
extern "C" void TweenManager_ReorganizeActiveTweens_m5419 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::DespawnTweens(System.Collections.Generic.List`1<DG.Tweening.Tween>,System.Boolean)
extern "C" void TweenManager_DespawnTweens_m5420 (Object_t * __this /* static, unused */, List_1_t946 * ___tweens, bool ___modifyActiveLists, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::RemoveActiveTween(DG.Tweening.Tween)
extern "C" void TweenManager_RemoveActiveTween_m5421 (Object_t * __this /* static, unused */, Tween_t934 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::IncreaseCapacities(DG.Tweening.Core.TweenManager/CapacityIncreaseMode)
extern "C" void TweenManager_IncreaseCapacities_m5422 (Object_t * __this /* static, unused */, int32_t ___increaseMode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.TweenManager::.cctor()
extern "C" void TweenManager__cctor_m5423 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
