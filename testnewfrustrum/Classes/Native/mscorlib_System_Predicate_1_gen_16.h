﻿#pragma once
#include <stdint.h>
// UnityEngine.Canvas
struct Canvas_t274;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Canvas>
struct  Predicate_1_t3283  : public MulticastDelegate_t307
{
};
