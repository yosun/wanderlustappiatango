﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WritableAttribute
struct WritableAttribute_t1301;

// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C" void WritableAttribute__ctor_m6733 (WritableAttribute_t1301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
