﻿#pragma once
#include <stdint.h>
// System.OperatingSystem
struct OperatingSystem_t2518;
// System.Object
#include "mscorlib_System_Object.h"
// System.Environment
struct  Environment_t2519  : public Object_t
{
};
struct Environment_t2519_StaticFields{
	// System.OperatingSystem System.Environment::os
	OperatingSystem_t2518 * ___os_0;
};
