﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SetRenderQueue
struct SetRenderQueue_t20;

// System.Void SetRenderQueue::.ctor()
extern "C" void SetRenderQueue__ctor_m85 (SetRenderQueue_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetRenderQueue::Awake()
extern "C" void SetRenderQueue_Awake_m86 (SetRenderQueue_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
