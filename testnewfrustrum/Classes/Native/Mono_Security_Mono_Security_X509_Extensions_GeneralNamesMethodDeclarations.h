﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.X509.Extensions.GeneralNames
struct GeneralNames_t1713;
// System.String[]
struct StringU5BU5D_t109;
// Mono.Security.ASN1
struct ASN1_t1669;
// System.String
struct String_t;

// System.Void Mono.Security.X509.Extensions.GeneralNames::.ctor(Mono.Security.ASN1)
extern "C" void GeneralNames__ctor_m7707 (GeneralNames_t1713 * __this, ASN1_t1669 * ___sequence, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Mono.Security.X509.Extensions.GeneralNames::get_DNSNames()
extern "C" StringU5BU5D_t109* GeneralNames_get_DNSNames_m7708 (GeneralNames_t1713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Mono.Security.X509.Extensions.GeneralNames::get_IPAddresses()
extern "C" StringU5BU5D_t109* GeneralNames_get_IPAddresses_m7709 (GeneralNames_t1713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.Extensions.GeneralNames::ToString()
extern "C" String_t* GeneralNames_ToString_m7710 (GeneralNames_t1713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
