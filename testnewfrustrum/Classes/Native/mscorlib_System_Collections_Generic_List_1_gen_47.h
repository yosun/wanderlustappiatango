﻿#pragma once
#include <stdint.h>
// DG.Tweening.Tween[]
struct TweenU5BU5D_t978;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct  List_1_t946  : public Object_t
{
	// T[] System.Collections.Generic.List`1<DG.Tweening.Tween>::_items
	TweenU5BU5D_t978* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Tween>::_version
	int32_t ____version_3;
};
struct List_1_t946_StaticFields{
	// T[] System.Collections.Generic.List`1<DG.Tweening.Tween>::EmptyArray
	TweenU5BU5D_t978* ___EmptyArray_4;
};
