﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackableBehaviour
struct SmartTerrainTrackableBehaviour_t591;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t589;

// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableBehaviour::get_SmartTerrainTrackable()
extern "C" Object_t * SmartTerrainTrackableBehaviour_get_SmartTerrainTrackable_m2779 (SmartTerrainTrackableBehaviour_t591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackableBehaviour::get_AutomaticUpdatesDisabled()
extern "C" bool SmartTerrainTrackableBehaviour_get_AutomaticUpdatesDisabled_m2780 (SmartTerrainTrackableBehaviour_t591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::UpdateMeshAndColliders()
extern "C" void SmartTerrainTrackableBehaviour_UpdateMeshAndColliders_m701 (SmartTerrainTrackableBehaviour_t591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::SetAutomaticUpdatesDisabled(System.Boolean)
extern "C" void SmartTerrainTrackableBehaviour_SetAutomaticUpdatesDisabled_m2781 (SmartTerrainTrackableBehaviour_t591 * __this, bool ___disabled, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::Start()
extern "C" void SmartTerrainTrackableBehaviour_Start_m702 (SmartTerrainTrackableBehaviour_t591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::.ctor()
extern "C" void SmartTerrainTrackableBehaviour__ctor_m2782 (SmartTerrainTrackableBehaviour_t591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
