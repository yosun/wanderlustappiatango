﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.OutAttribute
struct OutAttribute_t2049;

// System.Void System.Runtime.InteropServices.OutAttribute::.ctor()
extern "C" void OutAttribute__ctor_m10297 (OutAttribute_t2049 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
