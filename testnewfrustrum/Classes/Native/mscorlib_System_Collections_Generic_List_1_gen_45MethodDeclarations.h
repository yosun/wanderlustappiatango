﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
struct List_1_t756;
// System.Object
struct Object_t;
// Vuforia.IVirtualButtonEventHandler
struct IVirtualButtonEventHandler_t791;
// System.Collections.Generic.IEnumerable`1<Vuforia.IVirtualButtonEventHandler>
struct IEnumerable_1_t4280;
// System.Collections.Generic.IEnumerator`1<Vuforia.IVirtualButtonEventHandler>
struct IEnumerator_1_t4281;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.IVirtualButtonEventHandler>
struct ICollection_1_t4282;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVirtualButtonEventHandler>
struct ReadOnlyCollection_1_t3660;
// Vuforia.IVirtualButtonEventHandler[]
struct IVirtualButtonEventHandlerU5BU5D_t3658;
// System.Predicate`1<Vuforia.IVirtualButtonEventHandler>
struct Predicate_1_t3661;
// System.Comparison`1<Vuforia.IVirtualButtonEventHandler>
struct Comparison_1_t3662;
// System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_21.h"

// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4671(__this, method) (( void (*) (List_1_t756 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23203(__this, ___collection, method) (( void (*) (List_1_t756 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m23204(__this, ___capacity, method) (( void (*) (List_1_t756 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::.cctor()
#define List_1__cctor_m23205(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23206(__this, method) (( Object_t* (*) (List_1_t756 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23207(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t756 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23208(__this, method) (( Object_t * (*) (List_1_t756 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23209(__this, ___item, method) (( int32_t (*) (List_1_t756 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23210(__this, ___item, method) (( bool (*) (List_1_t756 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23211(__this, ___item, method) (( int32_t (*) (List_1_t756 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23212(__this, ___index, ___item, method) (( void (*) (List_1_t756 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23213(__this, ___item, method) (( void (*) (List_1_t756 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23214(__this, method) (( bool (*) (List_1_t756 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23215(__this, method) (( bool (*) (List_1_t756 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23216(__this, method) (( Object_t * (*) (List_1_t756 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23217(__this, method) (( bool (*) (List_1_t756 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23218(__this, method) (( bool (*) (List_1_t756 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23219(__this, ___index, method) (( Object_t * (*) (List_1_t756 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23220(__this, ___index, ___value, method) (( void (*) (List_1_t756 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Add(T)
#define List_1_Add_m23221(__this, ___item, method) (( void (*) (List_1_t756 *, Object_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23222(__this, ___newCount, method) (( void (*) (List_1_t756 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23223(__this, ___collection, method) (( void (*) (List_1_t756 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23224(__this, ___enumerable, method) (( void (*) (List_1_t756 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23225(__this, ___collection, method) (( void (*) (List_1_t756 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m23226(__this, method) (( ReadOnlyCollection_1_t3660 * (*) (List_1_t756 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Clear()
#define List_1_Clear_m23227(__this, method) (( void (*) (List_1_t756 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Contains(T)
#define List_1_Contains_m23228(__this, ___item, method) (( bool (*) (List_1_t756 *, Object_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23229(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t756 *, IVirtualButtonEventHandlerU5BU5D_t3658*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m23230(__this, ___match, method) (( Object_t * (*) (List_1_t756 *, Predicate_1_t3661 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23231(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3661 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23232(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t756 *, int32_t, int32_t, Predicate_1_t3661 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4672(__this, method) (( Enumerator_t895  (*) (List_1_t756 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::IndexOf(T)
#define List_1_IndexOf_m23233(__this, ___item, method) (( int32_t (*) (List_1_t756 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23234(__this, ___start, ___delta, method) (( void (*) (List_1_t756 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23235(__this, ___index, method) (( void (*) (List_1_t756 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m23236(__this, ___index, ___item, method) (( void (*) (List_1_t756 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23237(__this, ___collection, method) (( void (*) (List_1_t756 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Remove(T)
#define List_1_Remove_m23238(__this, ___item, method) (( bool (*) (List_1_t756 *, Object_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23239(__this, ___match, method) (( int32_t (*) (List_1_t756 *, Predicate_1_t3661 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23240(__this, ___index, method) (( void (*) (List_1_t756 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Reverse()
#define List_1_Reverse_m23241(__this, method) (( void (*) (List_1_t756 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Sort()
#define List_1_Sort_m23242(__this, method) (( void (*) (List_1_t756 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23243(__this, ___comparison, method) (( void (*) (List_1_t756 *, Comparison_1_t3662 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::ToArray()
#define List_1_ToArray_m23244(__this, method) (( IVirtualButtonEventHandlerU5BU5D_t3658* (*) (List_1_t756 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::TrimExcess()
#define List_1_TrimExcess_m23245(__this, method) (( void (*) (List_1_t756 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::get_Capacity()
#define List_1_get_Capacity_m23246(__this, method) (( int32_t (*) (List_1_t756 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23247(__this, ___value, method) (( void (*) (List_1_t756 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::get_Count()
#define List_1_get_Count_m23248(__this, method) (( int32_t (*) (List_1_t756 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m23249(__this, ___index, method) (( Object_t * (*) (List_1_t756 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m23250(__this, ___index, ___value, method) (( void (*) (List_1_t756 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
