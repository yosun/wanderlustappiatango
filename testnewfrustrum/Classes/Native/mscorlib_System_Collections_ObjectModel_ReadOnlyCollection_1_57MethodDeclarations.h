﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>
struct ReadOnlyCollection_1_t3766;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t459;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1365;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t4332;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m25058_gshared (ReadOnlyCollection_1_t3766 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m25058(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3766 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m25058_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m25059_gshared (ReadOnlyCollection_1_t3766 * __this, UILineInfo_t458  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m25059(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3766 *, UILineInfo_t458 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m25059_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m25060_gshared (ReadOnlyCollection_1_t3766 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m25060(__this, method) (( void (*) (ReadOnlyCollection_1_t3766 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m25060_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m25061_gshared (ReadOnlyCollection_1_t3766 * __this, int32_t ___index, UILineInfo_t458  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m25061(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3766 *, int32_t, UILineInfo_t458 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m25061_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m25062_gshared (ReadOnlyCollection_1_t3766 * __this, UILineInfo_t458  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m25062(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3766 *, UILineInfo_t458 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m25062_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m25063_gshared (ReadOnlyCollection_1_t3766 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m25063(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3766 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m25063_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" UILineInfo_t458  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m25064_gshared (ReadOnlyCollection_1_t3766 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m25064(__this, ___index, method) (( UILineInfo_t458  (*) (ReadOnlyCollection_1_t3766 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m25064_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m25065_gshared (ReadOnlyCollection_1_t3766 * __this, int32_t ___index, UILineInfo_t458  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m25065(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3766 *, int32_t, UILineInfo_t458 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m25065_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25066_gshared (ReadOnlyCollection_1_t3766 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25066(__this, method) (( bool (*) (ReadOnlyCollection_1_t3766 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25066_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m25067_gshared (ReadOnlyCollection_1_t3766 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m25067(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3766 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m25067_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m25068_gshared (ReadOnlyCollection_1_t3766 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m25068(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3766 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m25068_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m25069_gshared (ReadOnlyCollection_1_t3766 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m25069(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3766 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m25069_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m25070_gshared (ReadOnlyCollection_1_t3766 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m25070(__this, method) (( void (*) (ReadOnlyCollection_1_t3766 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m25070_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m25071_gshared (ReadOnlyCollection_1_t3766 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m25071(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3766 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m25071_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m25072_gshared (ReadOnlyCollection_1_t3766 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m25072(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3766 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m25072_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m25073_gshared (ReadOnlyCollection_1_t3766 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m25073(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3766 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m25073_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m25074_gshared (ReadOnlyCollection_1_t3766 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m25074(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3766 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m25074_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m25075_gshared (ReadOnlyCollection_1_t3766 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m25075(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3766 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m25075_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m25076_gshared (ReadOnlyCollection_1_t3766 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m25076(__this, method) (( bool (*) (ReadOnlyCollection_1_t3766 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m25076_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m25077_gshared (ReadOnlyCollection_1_t3766 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m25077(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3766 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m25077_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m25078_gshared (ReadOnlyCollection_1_t3766 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m25078(__this, method) (( bool (*) (ReadOnlyCollection_1_t3766 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m25078_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m25079_gshared (ReadOnlyCollection_1_t3766 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m25079(__this, method) (( bool (*) (ReadOnlyCollection_1_t3766 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m25079_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m25080_gshared (ReadOnlyCollection_1_t3766 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m25080(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3766 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m25080_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m25081_gshared (ReadOnlyCollection_1_t3766 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m25081(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3766 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m25081_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m25082_gshared (ReadOnlyCollection_1_t3766 * __this, UILineInfo_t458  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m25082(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3766 *, UILineInfo_t458 , const MethodInfo*))ReadOnlyCollection_1_Contains_m25082_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m25083_gshared (ReadOnlyCollection_1_t3766 * __this, UILineInfoU5BU5D_t1365* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m25083(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3766 *, UILineInfoU5BU5D_t1365*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m25083_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m25084_gshared (ReadOnlyCollection_1_t3766 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m25084(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3766 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m25084_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m25085_gshared (ReadOnlyCollection_1_t3766 * __this, UILineInfo_t458  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m25085(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3766 *, UILineInfo_t458 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m25085_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m25086_gshared (ReadOnlyCollection_1_t3766 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m25086(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3766 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m25086_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t458  ReadOnlyCollection_1_get_Item_m25087_gshared (ReadOnlyCollection_1_t3766 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m25087(__this, ___index, method) (( UILineInfo_t458  (*) (ReadOnlyCollection_1_t3766 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m25087_gshared)(__this, ___index, method)
