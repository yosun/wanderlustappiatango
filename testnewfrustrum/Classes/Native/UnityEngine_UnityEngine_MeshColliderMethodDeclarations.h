﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.MeshCollider
struct MeshCollider_t590;
// UnityEngine.Mesh
struct Mesh_t153;

// System.Void UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)
extern "C" void MeshCollider_set_sharedMesh_m4338 (MeshCollider_t590 * __this, Mesh_t153 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
