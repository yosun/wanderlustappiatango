﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t48;
// UnityEngine.GameObject
struct GameObject_t2;
// System.String
struct String_t;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// UnityEngine.GameObject Vuforia.HideExcessAreaAbstractBehaviour::CreateQuad(UnityEngine.GameObject,System.String,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.Int32)
extern "C" GameObject_t2 * HideExcessAreaAbstractBehaviour_CreateQuad_m2705 (HideExcessAreaAbstractBehaviour_t48 * __this, GameObject_t2 * ___parent, String_t* ___name, Vector3_t15  ___position, Quaternion_t13  ___rotation, Vector3_t15  ___scale, int32_t ___layer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::SetPlanesActive(System.Boolean)
extern "C" void HideExcessAreaAbstractBehaviour_SetPlanesActive_m2706 (HideExcessAreaAbstractBehaviour_t48 * __this, bool ___activeflag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnQCARStarted()
extern "C" void HideExcessAreaAbstractBehaviour_OnQCARStarted_m2707 (HideExcessAreaAbstractBehaviour_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::HasCalculationDataChanged()
extern "C" bool HideExcessAreaAbstractBehaviour_HasCalculationDataChanged_m2708 (HideExcessAreaAbstractBehaviour_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Start()
extern "C" void HideExcessAreaAbstractBehaviour_Start_m2709 (HideExcessAreaAbstractBehaviour_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDestroy()
extern "C" void HideExcessAreaAbstractBehaviour_OnDestroy_m2710 (HideExcessAreaAbstractBehaviour_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Update()
extern "C" void HideExcessAreaAbstractBehaviour_Update_m2711 (HideExcessAreaAbstractBehaviour_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::.ctor()
extern "C" void HideExcessAreaAbstractBehaviour__ctor_m418 (HideExcessAreaAbstractBehaviour_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
