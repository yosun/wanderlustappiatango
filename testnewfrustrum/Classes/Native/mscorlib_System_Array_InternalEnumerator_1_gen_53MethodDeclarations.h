﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>
struct InternalEnumerator_1_t3655;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.HashSet`1/Link<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m23184_gshared (InternalEnumerator_1_t3655 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m23184(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3655 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m23184_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23185_gshared (InternalEnumerator_1_t3655 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23185(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3655 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m23185_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m23186_gshared (InternalEnumerator_1_t3655 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m23186(__this, method) (( void (*) (InternalEnumerator_1_t3655 *, const MethodInfo*))InternalEnumerator_1_Dispose_m23186_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m23187_gshared (InternalEnumerator_1_t3655 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m23187(__this, method) (( bool (*) (InternalEnumerator_1_t3655 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m23187_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern "C" Link_t3653  InternalEnumerator_1_get_Current_m23188_gshared (InternalEnumerator_1_t3655 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m23188(__this, method) (( Link_t3653  (*) (InternalEnumerator_1_t3655 *, const MethodInfo*))InternalEnumerator_1_get_Current_m23188_gshared)(__this, method)
