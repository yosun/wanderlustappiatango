﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.JoinMatchResponse
struct JoinMatchResponse_t1256;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.JoinMatchResponse>
struct  ResponseDelegate_1_t1368  : public MulticastDelegate_t307
{
};
