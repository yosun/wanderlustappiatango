﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.HideExcessAreaBehaviour
struct HideExcessAreaBehaviour_t47;

// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
extern "C" void HideExcessAreaBehaviour__ctor_m157 (HideExcessAreaBehaviour_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
