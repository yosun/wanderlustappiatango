﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>
struct KeyValuePair_2_t3529;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t65;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m21440(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3529 *, int32_t, PropAbstractBehaviour_t65 *, const MethodInfo*))KeyValuePair_2__ctor_m16229_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m21441(__this, method) (( int32_t (*) (KeyValuePair_2_t3529 *, const MethodInfo*))KeyValuePair_2_get_Key_m16230_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m21442(__this, ___value, method) (( void (*) (KeyValuePair_2_t3529 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16231_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m21443(__this, method) (( PropAbstractBehaviour_t65 * (*) (KeyValuePair_2_t3529 *, const MethodInfo*))KeyValuePair_2_get_Value_m16232_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m21444(__this, ___value, method) (( void (*) (KeyValuePair_2_t3529 *, PropAbstractBehaviour_t65 *, const MethodInfo*))KeyValuePair_2_set_Value_m16233_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::ToString()
#define KeyValuePair_2_ToString_m21445(__this, method) (( String_t* (*) (KeyValuePair_2_t3529 *, const MethodInfo*))KeyValuePair_2_ToString_m16234_gshared)(__this, method)
