﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
struct SafeHandleZeroOrMinusOneIsInvalid_t2066;

// System.Void Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid::.ctor(System.Boolean)
extern "C" void SafeHandleZeroOrMinusOneIsInvalid__ctor_m10322 (SafeHandleZeroOrMinusOneIsInvalid_t2066 * __this, bool ___ownsHandle, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid::get_IsInvalid()
extern "C" bool SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m10323 (SafeHandleZeroOrMinusOneIsInvalid_t2066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
