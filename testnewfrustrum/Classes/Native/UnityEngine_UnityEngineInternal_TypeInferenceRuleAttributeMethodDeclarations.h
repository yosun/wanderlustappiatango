﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngineInternal.TypeInferenceRuleAttribute
struct TypeInferenceRuleAttribute_t1352;
// System.String
struct String_t;
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"

// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern "C" void TypeInferenceRuleAttribute__ctor_m6898 (TypeInferenceRuleAttribute_t1352 * __this, int32_t ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern "C" void TypeInferenceRuleAttribute__ctor_m6899 (TypeInferenceRuleAttribute_t1352 * __this, String_t* ___rule, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern "C" String_t* TypeInferenceRuleAttribute_ToString_m6900 (TypeInferenceRuleAttribute_t1352 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
