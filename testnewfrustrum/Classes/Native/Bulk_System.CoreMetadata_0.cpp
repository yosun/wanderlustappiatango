﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// <Module>
#include "System_Core_U3CModuleU3E.h"
// Metadata Definition <Module>
extern TypeInfo U3CModuleU3E_t1577_il2cpp_TypeInfo;
// <Module>
#include "System_Core_U3CModuleU3EMethodDeclarations.h"
static const MethodInfo* U3CModuleU3E_t1577_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CModuleU3E_t1577_0_0_0;
extern const Il2CppType U3CModuleU3E_t1577_1_0_0;
struct U3CModuleU3E_t1577;
const Il2CppTypeDefinitionMetadata U3CModuleU3E_t1577_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U3CModuleU3E_t1577_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t1577_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CModuleU3E_t1577_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U3CModuleU3E_t1577_0_0_0/* byval_arg */
	, &U3CModuleU3E_t1577_1_0_0/* this_arg */
	, &U3CModuleU3E_t1577_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t1577)/* instance_size */
	, sizeof (U3CModuleU3E_t1577)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// Metadata Definition System.Runtime.CompilerServices.ExtensionAttribute
extern TypeInfo ExtensionAttribute_t899_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern const Il2CppType Void_t168_0_0_0;
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
extern const MethodInfo ExtensionAttribute__ctor_m4681_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExtensionAttribute__ctor_m4681/* method */
	, &ExtensionAttribute_t899_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ExtensionAttribute_t899_MethodInfos[] =
{
	&ExtensionAttribute__ctor_m4681_MethodInfo,
	NULL
};
extern const MethodInfo Attribute_Equals_m5260_MethodInfo;
extern const MethodInfo Object_Finalize_m515_MethodInfo;
extern const MethodInfo Attribute_GetHashCode_m5261_MethodInfo;
extern const MethodInfo Object_ToString_m542_MethodInfo;
static const Il2CppMethodReference ExtensionAttribute_t899_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ExtensionAttribute_t899_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType _Attribute_t901_0_0_0;
static Il2CppInterfaceOffsetPair ExtensionAttribute_t899_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType ExtensionAttribute_t899_0_0_0;
extern const Il2CppType ExtensionAttribute_t899_1_0_0;
extern const Il2CppType Attribute_t138_0_0_0;
struct ExtensionAttribute_t899;
const Il2CppTypeDefinitionMetadata ExtensionAttribute_t899_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ExtensionAttribute_t899_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, ExtensionAttribute_t899_VTable/* vtableMethods */
	, ExtensionAttribute_t899_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ExtensionAttribute_t899_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExtensionAttribute"/* name */
	, "System.Runtime.CompilerServices"/* namespaze */
	, ExtensionAttribute_t899_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ExtensionAttribute_t899_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 2/* custom_attributes_cache */
	, &ExtensionAttribute_t899_0_0_0/* byval_arg */
	, &ExtensionAttribute_t899_1_0_0/* this_arg */
	, &ExtensionAttribute_t899_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExtensionAttribute_t899)/* instance_size */
	, sizeof (ExtensionAttribute_t899)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttribute.h"
// Metadata Definition System.MonoTODOAttribute
extern TypeInfo MonoTODOAttribute_t1578_il2cpp_TypeInfo;
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttributeMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.MonoTODOAttribute::.ctor()
extern const MethodInfo MonoTODOAttribute__ctor_m7258_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MonoTODOAttribute__ctor_m7258/* method */
	, &MonoTODOAttribute_t1578_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MonoTODOAttribute_t1578_MethodInfos[] =
{
	&MonoTODOAttribute__ctor_m7258_MethodInfo,
	NULL
};
static const Il2CppMethodReference MonoTODOAttribute_t1578_VTable[] =
{
	&Attribute_Equals_m5260_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Attribute_GetHashCode_m5261_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool MonoTODOAttribute_t1578_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair MonoTODOAttribute_t1578_InterfacesOffsets[] = 
{
	{ &_Attribute_t901_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType MonoTODOAttribute_t1578_0_0_0;
extern const Il2CppType MonoTODOAttribute_t1578_1_0_0;
struct MonoTODOAttribute_t1578;
const Il2CppTypeDefinitionMetadata MonoTODOAttribute_t1578_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, MonoTODOAttribute_t1578_InterfacesOffsets/* interfaceOffsets */
	, &Attribute_t138_0_0_0/* parent */
	, MonoTODOAttribute_t1578_VTable/* vtableMethods */
	, MonoTODOAttribute_t1578_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo MonoTODOAttribute_t1578_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "MonoTODOAttribute"/* name */
	, "System"/* namespaze */
	, MonoTODOAttribute_t1578_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MonoTODOAttribute_t1578_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 3/* custom_attributes_cache */
	, &MonoTODOAttribute_t1578_0_0_0/* byval_arg */
	, &MonoTODOAttribute_t1578_1_0_0/* this_arg */
	, &MonoTODOAttribute_t1578_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MonoTODOAttribute_t1578)/* instance_size */
	, sizeof (MonoTODOAttribute_t1578)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.HashSet`1/Link
extern TypeInfo Link_t1590_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Link_t1590_Il2CppGenericContainer;
extern TypeInfo Link_t1590_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Link_t1590_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Link_t1590_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Link_t1590_Il2CppGenericParametersArray[1] = 
{
	&Link_t1590_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Link_t1590_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Link_t1590_il2cpp_TypeInfo, 1, 0, Link_t1590_Il2CppGenericParametersArray };
static const MethodInfo* Link_t1590_MethodInfos[] =
{
	NULL
};
extern const MethodInfo ValueType_Equals_m2567_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2568_MethodInfo;
extern const MethodInfo ValueType_ToString_m2571_MethodInfo;
static const Il2CppMethodReference Link_t1590_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool Link_t1590_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Link_t1590_0_0_0;
extern const Il2CppType Link_t1590_1_0_0;
extern const Il2CppType ValueType_t524_0_0_0;
extern TypeInfo HashSet_1_t1589_il2cpp_TypeInfo;
extern const Il2CppType HashSet_1_t1589_0_0_0;
const Il2CppTypeDefinitionMetadata Link_t1590_DefinitionMetadata = 
{
	&HashSet_1_t1589_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, Link_t1590_VTable/* vtableMethods */
	, Link_t1590_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 0/* fieldStart */

};
TypeInfo Link_t1590_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Link"/* name */
	, ""/* namespaze */
	, Link_t1590_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Link_t1590_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Link_t1590_0_0_0/* byval_arg */
	, &Link_t1590_1_0_0/* this_arg */
	, &Link_t1590_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Link_t1590_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.HashSet`1/Enumerator
extern TypeInfo Enumerator_t1591_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Enumerator_t1591_Il2CppGenericContainer;
extern TypeInfo Enumerator_t1591_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerator_t1591_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerator_t1591_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* Enumerator_t1591_Il2CppGenericParametersArray[1] = 
{
	&Enumerator_t1591_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Enumerator_t1591_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerator_t1591_il2cpp_TypeInfo, 1, 0, Enumerator_t1591_Il2CppGenericParametersArray };
extern const Il2CppType HashSet_1_t1598_0_0_0;
extern const Il2CppType HashSet_1_t1598_0_0_0;
static const ParameterInfo Enumerator_t1591_Enumerator__ctor_m7295_ParameterInfos[] = 
{
	{"hashset", 0, 134217753, 0, &HashSet_1_t1598_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
extern const MethodInfo Enumerator__ctor_m7295_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Enumerator_t1591_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerator_t1591_Enumerator__ctor_m7295_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 26/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
// System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
extern const MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m7296_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &Enumerator_t1591_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 27/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
extern const MethodInfo Enumerator_MoveNext_m7297_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &Enumerator_t1591_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 28/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Enumerator_t1591_gp_0_0_0_0;
// T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
extern const MethodInfo Enumerator_get_Current_m7298_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &Enumerator_t1591_il2cpp_TypeInfo/* declaring_type */
	, &Enumerator_t1591_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 29/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
extern const MethodInfo Enumerator_Dispose_m7299_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &Enumerator_t1591_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 30/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void System.Collections.Generic.HashSet`1/Enumerator::CheckState()
extern const MethodInfo Enumerator_CheckState_m7300_MethodInfo = 
{
	"CheckState"/* name */
	, NULL/* method */
	, &Enumerator_t1591_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 31/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Enumerator_t1591_MethodInfos[] =
{
	&Enumerator__ctor_m7295_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m7296_MethodInfo,
	&Enumerator_MoveNext_m7297_MethodInfo,
	&Enumerator_get_Current_m7298_MethodInfo,
	&Enumerator_Dispose_m7299_MethodInfo,
	&Enumerator_CheckState_m7300_MethodInfo,
	NULL
};
extern const MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m7296_MethodInfo;
static const PropertyInfo Enumerator_t1591____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&Enumerator_t1591_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &Enumerator_System_Collections_IEnumerator_get_Current_m7296_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Enumerator_get_Current_m7298_MethodInfo;
static const PropertyInfo Enumerator_t1591____Current_PropertyInfo = 
{
	&Enumerator_t1591_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &Enumerator_get_Current_m7298_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Enumerator_t1591_PropertyInfos[] =
{
	&Enumerator_t1591____System_Collections_IEnumerator_Current_PropertyInfo,
	&Enumerator_t1591____Current_PropertyInfo,
	NULL
};
extern const MethodInfo Enumerator_MoveNext_m7297_MethodInfo;
extern const MethodInfo Enumerator_Dispose_m7299_MethodInfo;
static const Il2CppMethodReference Enumerator_t1591_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m7296_MethodInfo,
	&Enumerator_MoveNext_m7297_MethodInfo,
	&Enumerator_Dispose_m7299_MethodInfo,
	&Enumerator_get_Current_m7298_MethodInfo,
};
static bool Enumerator_t1591_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerator_t410_0_0_0;
extern const Il2CppType IDisposable_t144_0_0_0;
extern const Il2CppType IEnumerator_1_t1599_0_0_0;
static const Il2CppType* Enumerator_t1591_InterfacesTypeInfos[] = 
{
	&IEnumerator_t410_0_0_0,
	&IDisposable_t144_0_0_0,
	&IEnumerator_1_t1599_0_0_0,
};
static Il2CppInterfaceOffsetPair Enumerator_t1591_InterfacesOffsets[] = 
{
	{ &IEnumerator_t410_0_0_0, 4},
	{ &IDisposable_t144_0_0_0, 6},
	{ &IEnumerator_1_t1599_0_0_0, 7},
};
extern const Il2CppGenericMethod Enumerator_CheckState_m7333_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_GetLinkHashCode_m7334_GenericMethod;
static Il2CppRGCTXDefinition Enumerator_t1591_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerator_CheckState_m7333_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Enumerator_t1591_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_GetLinkHashCode_m7334_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Enumerator_t1591_0_0_0;
extern const Il2CppType Enumerator_t1591_1_0_0;
const Il2CppTypeDefinitionMetadata Enumerator_t1591_DefinitionMetadata = 
{
	&HashSet_1_t1589_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, Enumerator_t1591_InterfacesTypeInfos/* implementedInterfaces */
	, Enumerator_t1591_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, Enumerator_t1591_VTable/* vtableMethods */
	, Enumerator_t1591_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, Enumerator_t1591_RGCTXData/* rgctxDefinition */
	, 2/* fieldStart */

};
TypeInfo Enumerator_t1591_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, Enumerator_t1591_MethodInfos/* methods */
	, Enumerator_t1591_PropertyInfos/* properties */
	, NULL/* events */
	, &Enumerator_t1591_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Enumerator_t1591_0_0_0/* byval_arg */
	, &Enumerator_t1591_1_0_0/* this_arg */
	, &Enumerator_t1591_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Enumerator_t1591_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.HashSet`1/PrimeHelper
extern TypeInfo PrimeHelper_t1592_il2cpp_TypeInfo;
extern const Il2CppGenericContainer PrimeHelper_t1592_Il2CppGenericContainer;
extern TypeInfo PrimeHelper_t1592_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter PrimeHelper_t1592_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &PrimeHelper_t1592_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* PrimeHelper_t1592_Il2CppGenericParametersArray[1] = 
{
	&PrimeHelper_t1592_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer PrimeHelper_t1592_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&PrimeHelper_t1592_il2cpp_TypeInfo, 1, 0, PrimeHelper_t1592_Il2CppGenericParametersArray };
// System.Void System.Collections.Generic.HashSet`1/PrimeHelper::.cctor()
extern const MethodInfo PrimeHelper__cctor_m7301_MethodInfo = 
{
	".cctor"/* name */
	, NULL/* method */
	, &PrimeHelper_t1592_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 32/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo PrimeHelper_t1592_PrimeHelper_TestPrime_m7302_ParameterInfos[] = 
{
	{"x", 0, 134217754, 0, &Int32_t127_0_0_0},
};
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper::TestPrime(System.Int32)
extern const MethodInfo PrimeHelper_TestPrime_m7302_MethodInfo = 
{
	"TestPrime"/* name */
	, NULL/* method */
	, &PrimeHelper_t1592_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, PrimeHelper_t1592_PrimeHelper_TestPrime_m7302_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 33/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo PrimeHelper_t1592_PrimeHelper_CalcPrime_m7303_ParameterInfos[] = 
{
	{"x", 0, 134217755, 0, &Int32_t127_0_0_0},
};
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper::CalcPrime(System.Int32)
extern const MethodInfo PrimeHelper_CalcPrime_m7303_MethodInfo = 
{
	"CalcPrime"/* name */
	, NULL/* method */
	, &PrimeHelper_t1592_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, PrimeHelper_t1592_PrimeHelper_CalcPrime_m7303_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 34/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo PrimeHelper_t1592_PrimeHelper_ToPrime_m7304_ParameterInfos[] = 
{
	{"x", 0, 134217756, 0, &Int32_t127_0_0_0},
};
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper::ToPrime(System.Int32)
extern const MethodInfo PrimeHelper_ToPrime_m7304_MethodInfo = 
{
	"ToPrime"/* name */
	, NULL/* method */
	, &PrimeHelper_t1592_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, PrimeHelper_t1592_PrimeHelper_ToPrime_m7304_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 35/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PrimeHelper_t1592_MethodInfos[] =
{
	&PrimeHelper__cctor_m7301_MethodInfo,
	&PrimeHelper_TestPrime_m7302_MethodInfo,
	&PrimeHelper_CalcPrime_m7303_MethodInfo,
	&PrimeHelper_ToPrime_m7304_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m540_MethodInfo;
extern const MethodInfo Object_GetHashCode_m541_MethodInfo;
static const Il2CppMethodReference PrimeHelper_t1592_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool PrimeHelper_t1592_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType PrimeHelper_t1601_0_0_0;
extern const Il2CppGenericMethod PrimeHelper_TestPrime_m7335_GenericMethod;
extern const Il2CppGenericMethod PrimeHelper_CalcPrime_m7336_GenericMethod;
static Il2CppRGCTXDefinition PrimeHelper_t1592_RGCTXData[4] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&PrimeHelper_t1601_0_0_0 }/* Static */,
	{ IL2CPP_RGCTX_DATA_METHOD, &PrimeHelper_TestPrime_m7335_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &PrimeHelper_CalcPrime_m7336_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType PrimeHelper_t1592_0_0_0;
extern const Il2CppType PrimeHelper_t1592_1_0_0;
struct PrimeHelper_t1592;
const Il2CppTypeDefinitionMetadata PrimeHelper_t1592_DefinitionMetadata = 
{
	&HashSet_1_t1589_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, PrimeHelper_t1592_VTable/* vtableMethods */
	, PrimeHelper_t1592_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, PrimeHelper_t1592_RGCTXData/* rgctxDefinition */
	, 6/* fieldStart */

};
TypeInfo PrimeHelper_t1592_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimeHelper"/* name */
	, ""/* namespaze */
	, PrimeHelper_t1592_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PrimeHelper_t1592_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &PrimeHelper_t1592_0_0_0/* byval_arg */
	, &PrimeHelper_t1592_1_0_0/* this_arg */
	, &PrimeHelper_t1592_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &PrimeHelper_t1592_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048963/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Collections.Generic.HashSet`1
extern const Il2CppGenericContainer HashSet_1_t1589_Il2CppGenericContainer;
extern TypeInfo HashSet_1_t1589_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter HashSet_1_t1589_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &HashSet_1_t1589_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* HashSet_1_t1589_Il2CppGenericParametersArray[1] = 
{
	&HashSet_1_t1589_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer HashSet_1_t1589_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&HashSet_1_t1589_il2cpp_TypeInfo, 1, 0, HashSet_1_t1589_Il2CppGenericParametersArray };
// System.Void System.Collections.Generic.HashSet`1::.ctor()
extern const MethodInfo HashSet_1__ctor_m7272_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1__ctor_m7273_ParameterInfos[] = 
{
	{"info", 0, 134217729, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134217730, 0, &StreamingContext_t1383_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo HashSet_1__ctor_m7273_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1__ctor_m7273_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t1603_0_0_0;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern const MethodInfo HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7274_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1603_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern const MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7275_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TU5BU5D_t1604_0_0_0;
extern const Il2CppType TU5BU5D_t1604_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7276_ParameterInfos[] = 
{
	{"array", 0, 134217731, 0, &TU5BU5D_t1604_0_0_0},
	{"index", 1, 134217732, 0, &Int32_t127_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
extern const MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7276_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.CopyTo"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7276_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HashSet_1_t1589_gp_0_0_0_0;
extern const Il2CppType HashSet_1_t1589_gp_0_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7277_ParameterInfos[] = 
{
	{"item", 0, 134217733, 0, &HashSet_1_t1589_gp_0_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
extern const MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7277_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.Add"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7277_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo HashSet_1_System_Collections_IEnumerable_GetEnumerator_m7278_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t410_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 System.Collections.Generic.HashSet`1::get_Count()
extern const MethodInfo HashSet_1_get_Count_m7279_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType IEqualityComparer_1_t1605_0_0_0;
extern const Il2CppType IEqualityComparer_1_t1605_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1_Init_m7280_ParameterInfos[] = 
{
	{"capacity", 0, 134217734, 0, &Int32_t127_0_0_0},
	{"comparer", 1, 134217735, 0, &IEqualityComparer_1_t1605_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
extern const MethodInfo HashSet_1_Init_m7280_MethodInfo = 
{
	"Init"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1_Init_m7280_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1_InitArrays_m7281_ParameterInfos[] = 
{
	{"size", 0, 134217736, 0, &Int32_t127_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::InitArrays(System.Int32)
extern const MethodInfo HashSet_1_InitArrays_m7281_MethodInfo = 
{
	"InitArrays"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1_InitArrays_m7281_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType HashSet_1_t1589_gp_0_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1_SlotsContainsAt_m7282_ParameterInfos[] = 
{
	{"index", 0, 134217737, 0, &Int32_t127_0_0_0},
	{"hash", 1, 134217738, 0, &Int32_t127_0_0_0},
	{"item", 2, 134217739, 0, &HashSet_1_t1589_gp_0_0_0_0},
};
// System.Boolean System.Collections.Generic.HashSet`1::SlotsContainsAt(System.Int32,System.Int32,T)
extern const MethodInfo HashSet_1_SlotsContainsAt_m7282_MethodInfo = 
{
	"SlotsContainsAt"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1_SlotsContainsAt_m7282_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TU5BU5D_t1604_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1_CopyTo_m7283_ParameterInfos[] = 
{
	{"array", 0, 134217740, 0, &TU5BU5D_t1604_0_0_0},
	{"index", 1, 134217741, 0, &Int32_t127_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
extern const MethodInfo HashSet_1_CopyTo_m7283_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1_CopyTo_m7283_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TU5BU5D_t1604_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1_CopyTo_m7284_ParameterInfos[] = 
{
	{"array", 0, 134217742, 0, &TU5BU5D_t1604_0_0_0},
	{"index", 1, 134217743, 0, &Int32_t127_0_0_0},
	{"count", 2, 134217744, 0, &Int32_t127_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
extern const MethodInfo HashSet_1_CopyTo_m7284_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1_CopyTo_m7284_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void System.Collections.Generic.HashSet`1::Resize()
extern const MethodInfo HashSet_1_Resize_m7285_MethodInfo = 
{
	"Resize"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 16/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1_GetLinkHashCode_m7286_ParameterInfos[] = 
{
	{"index", 0, 134217745, 0, &Int32_t127_0_0_0},
};
// System.Int32 System.Collections.Generic.HashSet`1::GetLinkHashCode(System.Int32)
extern const MethodInfo HashSet_1_GetLinkHashCode_m7286_MethodInfo = 
{
	"GetLinkHashCode"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1_GetLinkHashCode_m7286_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 17/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HashSet_1_t1589_gp_0_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1_GetItemHashCode_m7287_ParameterInfos[] = 
{
	{"item", 0, 134217746, 0, &HashSet_1_t1589_gp_0_0_0_0},
};
// System.Int32 System.Collections.Generic.HashSet`1::GetItemHashCode(T)
extern const MethodInfo HashSet_1_GetItemHashCode_m7287_MethodInfo = 
{
	"GetItemHashCode"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1_GetItemHashCode_m7287_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 18/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HashSet_1_t1589_gp_0_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1_Add_m7288_ParameterInfos[] = 
{
	{"item", 0, 134217747, 0, &HashSet_1_t1589_gp_0_0_0_0},
};
// System.Boolean System.Collections.Generic.HashSet`1::Add(T)
extern const MethodInfo HashSet_1_Add_m7288_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1_Add_m7288_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 19/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void System.Collections.Generic.HashSet`1::Clear()
extern const MethodInfo HashSet_1_Clear_m7289_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 20/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HashSet_1_t1589_gp_0_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1_Contains_m7290_ParameterInfos[] = 
{
	{"item", 0, 134217748, 0, &HashSet_1_t1589_gp_0_0_0_0},
};
// System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
extern const MethodInfo HashSet_1_Contains_m7290_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1_Contains_m7290_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 21/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HashSet_1_t1589_gp_0_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1_Remove_m7291_ParameterInfos[] = 
{
	{"item", 0, 134217749, 0, &HashSet_1_t1589_gp_0_0_0_0},
};
// System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
extern const MethodInfo HashSet_1_Remove_m7291_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1_Remove_m7291_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 22/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SerializationInfo_t1382_0_0_0;
extern const Il2CppType StreamingContext_t1383_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1_GetObjectData_m7292_ParameterInfos[] = 
{
	{"info", 0, 134217750, 0, &SerializationInfo_t1382_0_0_0},
	{"context", 1, 134217751, 0, &StreamingContext_t1383_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern const MethodInfo HashSet_1_GetObjectData_m7292_MethodInfo = 
{
	"GetObjectData"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1_GetObjectData_m7292_ParameterInfos/* parameters */
	, 4/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 23/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo HashSet_1_t1589_HashSet_1_OnDeserialization_m7293_ParameterInfos[] = 
{
	{"sender", 0, 134217752, 0, &Object_t_0_0_0},
};
// System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
extern const MethodInfo HashSet_1_OnDeserialization_m7293_MethodInfo = 
{
	"OnDeserialization"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, HashSet_1_t1589_HashSet_1_OnDeserialization_m7293_ParameterInfos/* parameters */
	, 5/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 24/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Enumerator_t1606_0_0_0;
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
extern const MethodInfo HashSet_1_GetEnumerator_m7294_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* declaring_type */
	, &Enumerator_t1606_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 25/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HashSet_1_t1589_MethodInfos[] =
{
	&HashSet_1__ctor_m7272_MethodInfo,
	&HashSet_1__ctor_m7273_MethodInfo,
	&HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7274_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7275_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7276_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7277_MethodInfo,
	&HashSet_1_System_Collections_IEnumerable_GetEnumerator_m7278_MethodInfo,
	&HashSet_1_get_Count_m7279_MethodInfo,
	&HashSet_1_Init_m7280_MethodInfo,
	&HashSet_1_InitArrays_m7281_MethodInfo,
	&HashSet_1_SlotsContainsAt_m7282_MethodInfo,
	&HashSet_1_CopyTo_m7283_MethodInfo,
	&HashSet_1_CopyTo_m7284_MethodInfo,
	&HashSet_1_Resize_m7285_MethodInfo,
	&HashSet_1_GetLinkHashCode_m7286_MethodInfo,
	&HashSet_1_GetItemHashCode_m7287_MethodInfo,
	&HashSet_1_Add_m7288_MethodInfo,
	&HashSet_1_Clear_m7289_MethodInfo,
	&HashSet_1_Contains_m7290_MethodInfo,
	&HashSet_1_Remove_m7291_MethodInfo,
	&HashSet_1_GetObjectData_m7292_MethodInfo,
	&HashSet_1_OnDeserialization_m7293_MethodInfo,
	&HashSet_1_GetEnumerator_m7294_MethodInfo,
	NULL
};
extern const MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7275_MethodInfo;
static const PropertyInfo HashSet_1_t1589____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&HashSet_1_t1589_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.ICollection<T>.IsReadOnly"/* name */
	, &HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7275_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo HashSet_1_get_Count_m7279_MethodInfo;
static const PropertyInfo HashSet_1_t1589____Count_PropertyInfo = 
{
	&HashSet_1_t1589_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &HashSet_1_get_Count_m7279_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* HashSet_1_t1589_PropertyInfos[] =
{
	&HashSet_1_t1589____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&HashSet_1_t1589____Count_PropertyInfo,
	NULL
};
static const Il2CppType* HashSet_1_t1589_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Link_t1590_0_0_0,
	&Enumerator_t1591_0_0_0,
	&PrimeHelper_t1592_0_0_0,
};
extern const MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7277_MethodInfo;
extern const MethodInfo HashSet_1_Clear_m7289_MethodInfo;
extern const MethodInfo HashSet_1_Contains_m7290_MethodInfo;
extern const MethodInfo HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7276_MethodInfo;
extern const MethodInfo HashSet_1_Remove_m7291_MethodInfo;
extern const MethodInfo HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7274_MethodInfo;
extern const MethodInfo HashSet_1_System_Collections_IEnumerable_GetEnumerator_m7278_MethodInfo;
extern const MethodInfo HashSet_1_GetObjectData_m7292_MethodInfo;
extern const MethodInfo HashSet_1_OnDeserialization_m7293_MethodInfo;
extern const MethodInfo HashSet_1_CopyTo_m7283_MethodInfo;
static const Il2CppMethodReference HashSet_1_t1589_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&HashSet_1_get_Count_m7279_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7275_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m7277_MethodInfo,
	&HashSet_1_Clear_m7289_MethodInfo,
	&HashSet_1_Contains_m7290_MethodInfo,
	&HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m7276_MethodInfo,
	&HashSet_1_Remove_m7291_MethodInfo,
	&HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7274_MethodInfo,
	&HashSet_1_System_Collections_IEnumerable_GetEnumerator_m7278_MethodInfo,
	&HashSet_1_GetObjectData_m7292_MethodInfo,
	&HashSet_1_OnDeserialization_m7293_MethodInfo,
	&HashSet_1_CopyTo_m7283_MethodInfo,
	&HashSet_1_GetObjectData_m7292_MethodInfo,
	&HashSet_1_OnDeserialization_m7293_MethodInfo,
};
static bool HashSet_1_t1589_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICollection_1_t1607_0_0_0;
extern const Il2CppType IEnumerable_1_t1608_0_0_0;
extern const Il2CppType IEnumerable_t550_0_0_0;
extern const Il2CppType ISerializable_t513_0_0_0;
extern const Il2CppType IDeserializationCallback_t1609_0_0_0;
static const Il2CppType* HashSet_1_t1589_InterfacesTypeInfos[] = 
{
	&ICollection_1_t1607_0_0_0,
	&IEnumerable_1_t1608_0_0_0,
	&IEnumerable_t550_0_0_0,
	&ISerializable_t513_0_0_0,
	&IDeserializationCallback_t1609_0_0_0,
};
static Il2CppInterfaceOffsetPair HashSet_1_t1589_InterfacesOffsets[] = 
{
	{ &ICollection_1_t1607_0_0_0, 4},
	{ &IEnumerable_1_t1608_0_0_0, 11},
	{ &IEnumerable_t550_0_0_0, 12},
	{ &ISerializable_t513_0_0_0, 13},
	{ &IDeserializationCallback_t1609_0_0_0, 14},
};
extern const Il2CppGenericMethod HashSet_1_Init_m7337_GenericMethod;
extern const Il2CppGenericMethod Enumerator__ctor_m7338_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_CopyTo_m7339_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_Add_m7340_GenericMethod;
extern const Il2CppGenericMethod EqualityComparer_1_get_Default_m7341_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_InitArrays_m7342_GenericMethod;
extern const Il2CppType LinkU5BU5D_t1610_0_0_0;
extern const Il2CppGenericMethod HashSet_1_CopyTo_m7343_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_GetLinkHashCode_m7344_GenericMethod;
extern const Il2CppGenericMethod PrimeHelper_ToPrime_m7345_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_GetItemHashCode_m7346_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_SlotsContainsAt_m7347_GenericMethod;
extern const Il2CppGenericMethod HashSet_1_Resize_m7348_GenericMethod;
static Il2CppRGCTXDefinition HashSet_1_t1589_RGCTXData[18] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_Init_m7337_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Enumerator_t1606_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerator__ctor_m7338_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_CopyTo_m7339_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_Add_m7340_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &EqualityComparer_1_get_Default_m7341_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_InitArrays_m7342_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&LinkU5BU5D_t1610_0_0_0 }/* Array */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&TU5BU5D_t1604_0_0_0 }/* Array */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&HashSet_1_t1589_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEqualityComparer_1_t1605_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_CopyTo_m7343_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_GetLinkHashCode_m7344_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &PrimeHelper_ToPrime_m7345_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_GetItemHashCode_m7346_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_SlotsContainsAt_m7347_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &HashSet_1_Resize_m7348_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType HashSet_1_t1589_1_0_0;
struct HashSet_1_t1589;
const Il2CppTypeDefinitionMetadata HashSet_1_t1589_DefinitionMetadata = 
{
	NULL/* declaringType */
	, HashSet_1_t1589_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, HashSet_1_t1589_InterfacesTypeInfos/* implementedInterfaces */
	, HashSet_1_t1589_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, HashSet_1_t1589_VTable/* vtableMethods */
	, HashSet_1_t1589_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, HashSet_1_t1589_RGCTXData/* rgctxDefinition */
	, 7/* fieldStart */

};
TypeInfo HashSet_1_t1589_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "HashSet`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, HashSet_1_t1589_MethodInfos/* methods */
	, HashSet_1_t1589_PropertyInfos/* properties */
	, NULL/* events */
	, &HashSet_1_t1589_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HashSet_1_t1589_0_0_0/* byval_arg */
	, &HashSet_1_t1589_1_0_0/* this_arg */
	, &HashSet_1_t1589_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &HashSet_1_t1589_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 23/* method_count */
	, 2/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 18/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Linq.Check
#include "System_Core_System_Linq_Check.h"
// Metadata Definition System.Linq.Check
extern TypeInfo Check_t1579_il2cpp_TypeInfo;
// System.Linq.Check
#include "System_Core_System_Linq_CheckMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Check_t1579_Check_Source_m7259_ParameterInfos[] = 
{
	{"source", 0, 134217757, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Linq.Check::Source(System.Object)
extern const MethodInfo Check_Source_m7259_MethodInfo = 
{
	"Source"/* name */
	, (methodPointerType)&Check_Source_m7259/* method */
	, &Check_t1579_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Check_t1579_Check_Source_m7259_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 36/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Check_t1579_Check_SourceAndPredicate_m7260_ParameterInfos[] = 
{
	{"source", 0, 134217758, 0, &Object_t_0_0_0},
	{"predicate", 1, 134217759, 0, &Object_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Linq.Check::SourceAndPredicate(System.Object,System.Object)
extern const MethodInfo Check_SourceAndPredicate_m7260_MethodInfo = 
{
	"SourceAndPredicate"/* name */
	, (methodPointerType)&Check_SourceAndPredicate_m7260/* method */
	, &Check_t1579_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, Check_t1579_Check_SourceAndPredicate_m7260_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 37/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Check_t1579_MethodInfos[] =
{
	&Check_Source_m7259_MethodInfo,
	&Check_SourceAndPredicate_m7260_MethodInfo,
	NULL
};
static const Il2CppMethodReference Check_t1579_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool Check_t1579_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Check_t1579_0_0_0;
extern const Il2CppType Check_t1579_1_0_0;
struct Check_t1579;
const Il2CppTypeDefinitionMetadata Check_t1579_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Check_t1579_VTable/* vtableMethods */
	, Check_t1579_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Check_t1579_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Check"/* name */
	, "System.Linq"/* namespaze */
	, Check_t1579_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Check_t1579_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Check_t1579_0_0_0/* byval_arg */
	, &Check_t1579_1_0_0/* this_arg */
	, &Check_t1579_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Check_t1579)/* instance_size */
	, sizeof (Check_t1579)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1
extern TypeInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_il2cpp_TypeInfo;
extern const Il2CppGenericContainer U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_Il2CppGenericContainer;
extern TypeInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_gp_TResult_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull = { &U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_Il2CppGenericContainer, NULL, "TResult", 0, 0 };
static const Il2CppGenericParameter* U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_Il2CppGenericParametersArray[1] = 
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_il2cpp_TypeInfo, 1, 0, U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_Il2CppGenericParametersArray };
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::.ctor()
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7315_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 48/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_gp_0_0_0_0;
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7316_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<TResult>.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_il2cpp_TypeInfo/* declaring_type */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 18/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 49/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7317_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 19/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 50/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7318_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t410_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 20/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 51/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t1612_0_0_0;
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7319_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<TResult>.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1612_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 21/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 52/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::MoveNext()
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m7320_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 53/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1::Dispose()
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7321_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 22/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 54/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_MethodInfos[] =
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7315_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7316_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7317_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7318_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7319_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m7320_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7321_MethodInfo,
	NULL
};
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7316_MethodInfo;
static const PropertyInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1593____System_Collections_Generic_IEnumeratorU3CTResultU3E_Current_PropertyInfo = 
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<TResult>.Current"/* name */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7316_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7317_MethodInfo;
static const PropertyInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1593____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7317_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_PropertyInfos[] =
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1593____System_Collections_Generic_IEnumeratorU3CTResultU3E_Current_PropertyInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1593____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m7320_MethodInfo;
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7321_MethodInfo;
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7318_MethodInfo;
extern const MethodInfo U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7319_MethodInfo;
static const Il2CppMethodReference U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7317_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m7320_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7321_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7318_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7319_MethodInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7316_MethodInfo,
};
static bool U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerable_1_t1613_0_0_0;
static const Il2CppType* U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_InterfacesTypeInfos[] = 
{
	&IEnumerator_t410_0_0_0,
	&IDisposable_t144_0_0_0,
	&IEnumerable_t550_0_0_0,
	&IEnumerable_1_t1613_0_0_0,
	&IEnumerator_1_t1612_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_InterfacesOffsets[] = 
{
	{ &IEnumerator_t410_0_0_0, 4},
	{ &IDisposable_t144_0_0_0, 6},
	{ &IEnumerable_t550_0_0_0, 7},
	{ &IEnumerable_1_t1613_0_0_0, 8},
	{ &IEnumerator_1_t1612_0_0_0, 9},
};
extern const Il2CppGenericMethod U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7349_GenericMethod;
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1614_0_0_0;
extern const Il2CppGenericMethod U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7350_GenericMethod;
static Il2CppRGCTXDefinition U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7349_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateCastIteratorU3Ec__Iterator0_1_t1614_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7350_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_0_0_0;
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_1_0_0;
extern TypeInfo Enumerable_t132_il2cpp_TypeInfo;
extern const Il2CppType Enumerable_t132_0_0_0;
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t1593;
const Il2CppTypeDefinitionMetadata U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_DefinitionMetadata = 
{
	&Enumerable_t132_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_VTable/* vtableMethods */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_RGCTXData/* rgctxDefinition */
	, 21/* fieldStart */

};
TypeInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateCastIterator>c__Iterator0`1"/* name */
	, ""/* namespaze */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_MethodInfos/* methods */
	, U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 17/* custom_attributes_cache */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_0_0_0/* byval_arg */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_1_0_0/* this_arg */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// Metadata Definition System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_il2cpp_TypeInfo;
extern const Il2CppGenericContainer U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_Il2CppGenericContainer;
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_Il2CppGenericParametersArray[1] = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_il2cpp_TypeInfo, 1, 0, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_Il2CppGenericParametersArray };
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::.ctor()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7322_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 55/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_gp_0_0_0_0;
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7323_MethodInfo = 
{
	"System.Collections.Generic.IEnumerator<TSource>.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_il2cpp_TypeInfo/* declaring_type */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 24/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 56/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.IEnumerator.get_Current()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7324_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 25/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 57/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7325_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t410_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 26/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 58/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t1616_0_0_0;
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7326_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<TSource>.GetEnumerator"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1616_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 27/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 59/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::MoveNext()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m7327_MethodInfo = 
{
	"MoveNext"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 60/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1::Dispose()
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7328_MethodInfo = 
{
	"Dispose"/* name */
	, NULL/* method */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 28/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 61/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_MethodInfos[] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7322_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7323_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7324_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7325_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7326_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m7327_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7328_MethodInfo,
	NULL
};
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7323_MethodInfo;
static const PropertyInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.IEnumerator<TSource>.Current"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7323_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7324_MethodInfo;
static const PropertyInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7324_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_PropertyInfos[] =
{
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594____System_Collections_Generic_IEnumeratorU3CTSourceU3E_Current_PropertyInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594____System_Collections_IEnumerator_Current_PropertyInfo,
	NULL
};
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m7327_MethodInfo;
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7328_MethodInfo;
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7325_MethodInfo;
extern const MethodInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7326_MethodInfo;
static const Il2CppMethodReference U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7324_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m7327_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7328_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7325_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7326_MethodInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7323_MethodInfo,
};
static bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEnumerable_1_t1617_0_0_0;
static const Il2CppType* U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_InterfacesTypeInfos[] = 
{
	&IEnumerator_t410_0_0_0,
	&IDisposable_t144_0_0_0,
	&IEnumerable_t550_0_0_0,
	&IEnumerable_1_t1617_0_0_0,
	&IEnumerator_1_t1616_0_0_0,
};
static Il2CppInterfaceOffsetPair U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_InterfacesOffsets[] = 
{
	{ &IEnumerator_t410_0_0_0, 4},
	{ &IDisposable_t144_0_0_0, 6},
	{ &IEnumerable_t550_0_0_0, 7},
	{ &IEnumerable_1_t1617_0_0_0, 8},
	{ &IEnumerator_1_t1616_0_0_0, 9},
};
extern const Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7351_GenericMethod;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1618_0_0_0;
extern const Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7352_GenericMethod;
extern const Il2CppGenericMethod Func_2_Invoke_m7353_GenericMethod;
static Il2CppRGCTXDefinition U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_RGCTXData[8] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7351_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1618_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7352_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerable_1_t1617_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerator_1_t1616_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Func_2_Invoke_m7353_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_0_0_0;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_1_0_0;
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594;
const Il2CppTypeDefinitionMetadata U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_DefinitionMetadata = 
{
	&Enumerable_t132_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_InterfacesTypeInfos/* implementedInterfaces */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_VTable/* vtableMethods */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_RGCTXData/* rgctxDefinition */
	, 27/* fieldStart */

};
TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<CreateWhereIterator>c__Iterator1D`1"/* name */
	, ""/* namespaze */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_MethodInfos/* methods */
	, U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_PropertyInfos/* properties */
	, NULL/* events */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 23/* custom_attributes_cache */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_0_0_0/* byval_arg */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_1_0_0/* this_arg */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048835/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 2/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 5/* interfaces_count */
	, 5/* interface_offsets_count */

};
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
// Metadata Definition System.Linq.Enumerable
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
extern const Il2CppType IEnumerable_1_t1619_0_0_0;
extern const Il2CppType IEnumerable_1_t1619_0_0_0;
static const ParameterInfo Enumerable_t132_Enumerable_Any_m7305_ParameterInfos[] = 
{
	{"source", 0, 134217760, 0, &IEnumerable_1_t1619_0_0_0},
};
extern const Il2CppGenericContainer Enumerable_Any_m7305_Il2CppGenericContainer;
extern TypeInfo Enumerable_Any_m7305_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_Any_m7305_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_Any_m7305_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_Any_m7305_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Any_m7305_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_Any_m7305_MethodInfo;
extern const Il2CppGenericContainer Enumerable_Any_m7305_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_Any_m7305_MethodInfo, 1, 1, Enumerable_Any_m7305_Il2CppGenericParametersArray };
extern const Il2CppType ICollection_1_t1621_0_0_0;
static Il2CppRGCTXDefinition Enumerable_Any_m7305_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ICollection_1_t1621_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerable_1_t1619_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
extern const MethodInfo Enumerable_Any_m7305_MethodInfo = 
{
	"Any"/* name */
	, NULL/* method */
	, &Enumerable_t132_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t132_Enumerable_Any_m7305_ParameterInfos/* parameters */
	, 7/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 38/* token */
	, Enumerable_Any_m7305_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Any_m7305_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_t550_0_0_0;
static const ParameterInfo Enumerable_t132_Enumerable_Cast_m7306_ParameterInfos[] = 
{
	{"source", 0, 134217761, 0, &IEnumerable_t550_0_0_0},
};
extern const Il2CppType IEnumerable_1_t1622_0_0_0;
extern const Il2CppGenericContainer Enumerable_Cast_m7306_Il2CppGenericContainer;
extern TypeInfo Enumerable_Cast_m7306_gp_TResult_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_Cast_m7306_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_Cast_m7306_Il2CppGenericContainer, NULL, "TResult", 0, 0 };
static const Il2CppGenericParameter* Enumerable_Cast_m7306_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Cast_m7306_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_Cast_m7306_MethodInfo;
extern const Il2CppGenericContainer Enumerable_Cast_m7306_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_Cast_m7306_MethodInfo, 1, 1, Enumerable_Cast_m7306_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod Enumerable_CreateCastIterator_TisTResult_t1623_m7354_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Cast_m7306_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerable_1_t1622_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_CreateCastIterator_TisTResult_t1623_m7354_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
extern const MethodInfo Enumerable_Cast_m7306_MethodInfo = 
{
	"Cast"/* name */
	, NULL/* method */
	, &Enumerable_t132_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1622_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t132_Enumerable_Cast_m7306_ParameterInfos/* parameters */
	, 8/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 39/* token */
	, Enumerable_Cast_m7306_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Cast_m7306_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_t550_0_0_0;
static const ParameterInfo Enumerable_t132_Enumerable_CreateCastIterator_m7307_ParameterInfos[] = 
{
	{"source", 0, 134217762, 0, &IEnumerable_t550_0_0_0},
};
extern const Il2CppType IEnumerable_1_t1624_0_0_0;
extern const Il2CppGenericContainer Enumerable_CreateCastIterator_m7307_Il2CppGenericContainer;
extern TypeInfo Enumerable_CreateCastIterator_m7307_gp_TResult_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_CreateCastIterator_m7307_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_CreateCastIterator_m7307_Il2CppGenericContainer, NULL, "TResult", 0, 0 };
static const Il2CppGenericParameter* Enumerable_CreateCastIterator_m7307_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_CreateCastIterator_m7307_gp_TResult_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_CreateCastIterator_m7307_MethodInfo;
extern const Il2CppGenericContainer Enumerable_CreateCastIterator_m7307_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_CreateCastIterator_m7307_MethodInfo, 1, 1, Enumerable_CreateCastIterator_m7307_Il2CppGenericParametersArray };
extern const Il2CppType U3CCreateCastIteratorU3Ec__Iterator0_1_t1626_0_0_0;
extern const Il2CppGenericMethod U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7355_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_CreateCastIterator_m7307_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateCastIteratorU3Ec__Iterator0_1_t1626_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m7355_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CreateCastIterator(System.Collections.IEnumerable)
extern const MethodInfo Enumerable_CreateCastIterator_m7307_MethodInfo = 
{
	"CreateCastIterator"/* name */
	, NULL/* method */
	, &Enumerable_t132_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1624_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t132_Enumerable_CreateCastIterator_m7307_ParameterInfos/* parameters */
	, 9/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 40/* token */
	, Enumerable_CreateCastIterator_m7307_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_CreateCastIterator_m7307_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1627_0_0_0;
extern const Il2CppType IEnumerable_1_t1627_0_0_0;
extern const Il2CppType Enumerable_Contains_m7308_gp_0_0_0_0;
extern const Il2CppType Enumerable_Contains_m7308_gp_0_0_0_0;
static const ParameterInfo Enumerable_t132_Enumerable_Contains_m7308_ParameterInfos[] = 
{
	{"source", 0, 134217763, 0, &IEnumerable_1_t1627_0_0_0},
	{"value", 1, 134217764, 0, &Enumerable_Contains_m7308_gp_0_0_0_0},
};
extern const Il2CppGenericContainer Enumerable_Contains_m7308_Il2CppGenericContainer;
extern TypeInfo Enumerable_Contains_m7308_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_Contains_m7308_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_Contains_m7308_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_Contains_m7308_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Contains_m7308_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_Contains_m7308_MethodInfo;
extern const Il2CppGenericContainer Enumerable_Contains_m7308_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_Contains_m7308_MethodInfo, 1, 1, Enumerable_Contains_m7308_Il2CppGenericParametersArray };
extern const Il2CppType ICollection_1_t1629_0_0_0;
extern const Il2CppGenericMethod Enumerable_Contains_TisTSource_t1628_m7356_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Contains_m7308_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ICollection_1_t1629_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_Contains_TisTSource_t1628_m7356_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
extern const MethodInfo Enumerable_Contains_m7308_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &Enumerable_t132_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t132_Enumerable_Contains_m7308_ParameterInfos/* parameters */
	, 10/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 41/* token */
	, Enumerable_Contains_m7308_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Contains_m7308_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1630_0_0_0;
extern const Il2CppType IEnumerable_1_t1630_0_0_0;
extern const Il2CppType Enumerable_Contains_m7309_gp_0_0_0_0;
extern const Il2CppType Enumerable_Contains_m7309_gp_0_0_0_0;
extern const Il2CppType IEqualityComparer_1_t1632_0_0_0;
extern const Il2CppType IEqualityComparer_1_t1632_0_0_0;
static const ParameterInfo Enumerable_t132_Enumerable_Contains_m7309_ParameterInfos[] = 
{
	{"source", 0, 134217765, 0, &IEnumerable_1_t1630_0_0_0},
	{"value", 1, 134217766, 0, &Enumerable_Contains_m7309_gp_0_0_0_0},
	{"comparer", 2, 134217767, 0, &IEqualityComparer_1_t1632_0_0_0},
};
extern const Il2CppGenericContainer Enumerable_Contains_m7309_Il2CppGenericContainer;
extern TypeInfo Enumerable_Contains_m7309_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_Contains_m7309_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_Contains_m7309_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_Contains_m7309_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Contains_m7309_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_Contains_m7309_MethodInfo;
extern const Il2CppGenericContainer Enumerable_Contains_m7309_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_Contains_m7309_MethodInfo, 1, 1, Enumerable_Contains_m7309_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod EqualityComparer_1_get_Default_m7357_GenericMethod;
extern const Il2CppType IEnumerator_1_t1633_0_0_0;
static Il2CppRGCTXDefinition Enumerable_Contains_m7309_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &EqualityComparer_1_get_Default_m7357_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerable_1_t1630_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerator_1_t1633_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEqualityComparer_1_t1632_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
extern const MethodInfo Enumerable_Contains_m7309_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &Enumerable_t132_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t132_Enumerable_Contains_m7309_ParameterInfos/* parameters */
	, 11/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 42/* token */
	, Enumerable_Contains_m7309_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Contains_m7309_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1634_0_0_0;
extern const Il2CppType IEnumerable_1_t1634_0_0_0;
static const ParameterInfo Enumerable_t132_Enumerable_First_m7310_ParameterInfos[] = 
{
	{"source", 0, 134217768, 0, &IEnumerable_1_t1634_0_0_0},
};
extern const Il2CppType Enumerable_First_m7310_gp_0_0_0_0;
extern const Il2CppGenericContainer Enumerable_First_m7310_Il2CppGenericContainer;
extern TypeInfo Enumerable_First_m7310_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_First_m7310_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_First_m7310_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_First_m7310_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_First_m7310_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_First_m7310_MethodInfo;
extern const Il2CppGenericContainer Enumerable_First_m7310_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_First_m7310_MethodInfo, 1, 1, Enumerable_First_m7310_Il2CppGenericParametersArray };
extern const Il2CppType IList_1_t1636_0_0_0;
extern const Il2CppType ICollection_1_t1637_0_0_0;
extern const Il2CppType IEnumerator_1_t1638_0_0_0;
static Il2CppRGCTXDefinition Enumerable_First_m7310_RGCTXData[5] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IList_1_t1636_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ICollection_1_t1637_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerable_1_t1634_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&IEnumerator_1_t1638_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
extern const MethodInfo Enumerable_First_m7310_MethodInfo = 
{
	"First"/* name */
	, NULL/* method */
	, &Enumerable_t132_il2cpp_TypeInfo/* declaring_type */
	, &Enumerable_First_m7310_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t132_Enumerable_First_m7310_ParameterInfos/* parameters */
	, 12/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 43/* token */
	, Enumerable_First_m7310_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_First_m7310_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1639_0_0_0;
extern const Il2CppType IEnumerable_1_t1639_0_0_0;
static const ParameterInfo Enumerable_t132_Enumerable_ToArray_m7311_ParameterInfos[] = 
{
	{"source", 0, 134217769, 0, &IEnumerable_1_t1639_0_0_0},
};
extern const Il2CppType TSourceU5BU5D_t1640_0_0_0;
extern const Il2CppGenericContainer Enumerable_ToArray_m7311_Il2CppGenericContainer;
extern TypeInfo Enumerable_ToArray_m7311_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_ToArray_m7311_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_ToArray_m7311_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_ToArray_m7311_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_ToArray_m7311_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_ToArray_m7311_MethodInfo;
extern const Il2CppGenericContainer Enumerable_ToArray_m7311_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_ToArray_m7311_MethodInfo, 1, 1, Enumerable_ToArray_m7311_Il2CppGenericParametersArray };
extern const Il2CppType ICollection_1_t1642_0_0_0;
extern const Il2CppType List_1_t1643_0_0_0;
extern const Il2CppGenericMethod List_1__ctor_m7358_GenericMethod;
extern const Il2CppGenericMethod List_1_ToArray_m7359_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_ToArray_m7311_RGCTXData[6] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ICollection_1_t1642_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&TSourceU5BU5D_t1640_0_0_0 }/* Array */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&List_1_t1643_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1__ctor_m7358_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_ToArray_m7359_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
extern const MethodInfo Enumerable_ToArray_m7311_MethodInfo = 
{
	"ToArray"/* name */
	, NULL/* method */
	, &Enumerable_t132_il2cpp_TypeInfo/* declaring_type */
	, &TSourceU5BU5D_t1640_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t132_Enumerable_ToArray_m7311_ParameterInfos/* parameters */
	, 13/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 44/* token */
	, Enumerable_ToArray_m7311_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_ToArray_m7311_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1644_0_0_0;
extern const Il2CppType IEnumerable_1_t1644_0_0_0;
static const ParameterInfo Enumerable_t132_Enumerable_ToList_m7312_ParameterInfos[] = 
{
	{"source", 0, 134217770, 0, &IEnumerable_1_t1644_0_0_0},
};
extern const Il2CppType List_1_t1645_0_0_0;
extern const Il2CppGenericContainer Enumerable_ToList_m7312_Il2CppGenericContainer;
extern TypeInfo Enumerable_ToList_m7312_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_ToList_m7312_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_ToList_m7312_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_ToList_m7312_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_ToList_m7312_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_ToList_m7312_MethodInfo;
extern const Il2CppGenericContainer Enumerable_ToList_m7312_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_ToList_m7312_MethodInfo, 1, 1, Enumerable_ToList_m7312_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod List_1__ctor_m7360_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_ToList_m7312_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&List_1_t1645_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1__ctor_m7360_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
extern const MethodInfo Enumerable_ToList_m7312_MethodInfo = 
{
	"ToList"/* name */
	, NULL/* method */
	, &Enumerable_t132_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t1645_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t132_Enumerable_ToList_m7312_ParameterInfos/* parameters */
	, 14/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 45/* token */
	, Enumerable_ToList_m7312_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_ToList_m7312_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1647_0_0_0;
extern const Il2CppType IEnumerable_1_t1647_0_0_0;
extern const Il2CppType Func_2_t1648_0_0_0;
extern const Il2CppType Func_2_t1648_0_0_0;
static const ParameterInfo Enumerable_t132_Enumerable_Where_m7313_ParameterInfos[] = 
{
	{"source", 0, 134217771, 0, &IEnumerable_1_t1647_0_0_0},
	{"predicate", 1, 134217772, 0, &Func_2_t1648_0_0_0},
};
extern const Il2CppGenericContainer Enumerable_Where_m7313_Il2CppGenericContainer;
extern TypeInfo Enumerable_Where_m7313_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_Where_m7313_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_Where_m7313_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_Where_m7313_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_Where_m7313_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_Where_m7313_MethodInfo;
extern const Il2CppGenericContainer Enumerable_Where_m7313_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_Where_m7313_MethodInfo, 1, 1, Enumerable_Where_m7313_Il2CppGenericParametersArray };
extern const Il2CppGenericMethod Enumerable_CreateWhereIterator_TisTSource_t1649_m7361_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_Where_m7313_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_METHOD, &Enumerable_CreateWhereIterator_TisTSource_t1649_m7361_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern const MethodInfo Enumerable_Where_m7313_MethodInfo = 
{
	"Where"/* name */
	, NULL/* method */
	, &Enumerable_t132_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1647_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t132_Enumerable_Where_m7313_ParameterInfos/* parameters */
	, 15/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 46/* token */
	, Enumerable_Where_m7313_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_Where_m7313_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType IEnumerable_1_t1650_0_0_0;
extern const Il2CppType IEnumerable_1_t1650_0_0_0;
extern const Il2CppType Func_2_t1651_0_0_0;
extern const Il2CppType Func_2_t1651_0_0_0;
static const ParameterInfo Enumerable_t132_Enumerable_CreateWhereIterator_m7314_ParameterInfos[] = 
{
	{"source", 0, 134217773, 0, &IEnumerable_1_t1650_0_0_0},
	{"predicate", 1, 134217774, 0, &Func_2_t1651_0_0_0},
};
extern const Il2CppGenericContainer Enumerable_CreateWhereIterator_m7314_Il2CppGenericContainer;
extern TypeInfo Enumerable_CreateWhereIterator_m7314_gp_TSource_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Enumerable_CreateWhereIterator_m7314_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull = { &Enumerable_CreateWhereIterator_m7314_Il2CppGenericContainer, NULL, "TSource", 0, 0 };
static const Il2CppGenericParameter* Enumerable_CreateWhereIterator_m7314_Il2CppGenericParametersArray[1] = 
{
	&Enumerable_CreateWhereIterator_m7314_gp_TSource_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo Enumerable_CreateWhereIterator_m7314_MethodInfo;
extern const Il2CppGenericContainer Enumerable_CreateWhereIterator_m7314_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Enumerable_CreateWhereIterator_m7314_MethodInfo, 1, 1, Enumerable_CreateWhereIterator_m7314_Il2CppGenericParametersArray };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1653_0_0_0;
extern const Il2CppGenericMethod U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7362_GenericMethod;
static Il2CppRGCTXDefinition Enumerable_CreateWhereIterator_m7314_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1653_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m7362_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::CreateWhereIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
extern const MethodInfo Enumerable_CreateWhereIterator_m7314_MethodInfo = 
{
	"CreateWhereIterator"/* name */
	, NULL/* method */
	, &Enumerable_t132_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t1650_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Enumerable_t132_Enumerable_CreateWhereIterator_m7314_ParameterInfos/* parameters */
	, 16/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 47/* token */
	, Enumerable_CreateWhereIterator_m7314_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerable_CreateWhereIterator_m7314_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* Enumerable_t132_MethodInfos[] =
{
	&Enumerable_Any_m7305_MethodInfo,
	&Enumerable_Cast_m7306_MethodInfo,
	&Enumerable_CreateCastIterator_m7307_MethodInfo,
	&Enumerable_Contains_m7308_MethodInfo,
	&Enumerable_Contains_m7309_MethodInfo,
	&Enumerable_First_m7310_MethodInfo,
	&Enumerable_ToArray_m7311_MethodInfo,
	&Enumerable_ToList_m7312_MethodInfo,
	&Enumerable_Where_m7313_MethodInfo,
	&Enumerable_CreateWhereIterator_m7314_MethodInfo,
	NULL
};
static const Il2CppType* Enumerable_t132_il2cpp_TypeInfo__nestedTypes[2] =
{
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_0_0_0,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_0_0_0,
};
static const Il2CppMethodReference Enumerable_t132_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool Enumerable_t132_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Enumerable_t132_1_0_0;
struct Enumerable_t132;
const Il2CppTypeDefinitionMetadata Enumerable_t132_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Enumerable_t132_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, Enumerable_t132_VTable/* vtableMethods */
	, Enumerable_t132_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Enumerable_t132_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerable"/* name */
	, "System.Linq"/* namespaze */
	, Enumerable_t132_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Enumerable_t132_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 6/* custom_attributes_cache */
	, &Enumerable_t132_0_0_0/* byval_arg */
	, &Enumerable_t132_1_0_0/* this_arg */
	, &Enumerable_t132_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerable_t132)/* instance_size */
	, sizeof (Enumerable_t132)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 10/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Action
#include "System_Core_System_Action.h"
// Metadata Definition System.Action
extern TypeInfo Action_t139_il2cpp_TypeInfo;
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Action_t139_Action__ctor_m4319_ParameterInfos[] = 
{
	{"object", 0, 134217775, 0, &Object_t_0_0_0},
	{"method", 1, 134217776, 0, &IntPtr_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Action__ctor_m4319_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Action__ctor_m4319/* method */
	, &Action_t139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_IntPtr_t/* invoker_method */
	, Action_t139_Action__ctor_m4319_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 62/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void System.Action::Invoke()
extern const MethodInfo Action_Invoke_m7261_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Action_Invoke_m7261/* method */
	, &Action_t139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 63/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Action_t139_Action_BeginInvoke_m7262_ParameterInfos[] = 
{
	{"callback", 0, 134217777, 0, &AsyncCallback_t305_0_0_0},
	{"object", 1, 134217778, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t304_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.IAsyncResult System.Action::BeginInvoke(System.AsyncCallback,System.Object)
extern const MethodInfo Action_BeginInvoke_m7262_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Action_BeginInvoke_m7262/* method */
	, &Action_t139_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, Action_t139_Action_BeginInvoke_m7262_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 64/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo Action_t139_Action_EndInvoke_m7263_ParameterInfos[] = 
{
	{"result", 0, 134217779, 0, &IAsyncResult_t304_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void System.Action::EndInvoke(System.IAsyncResult)
extern const MethodInfo Action_EndInvoke_m7263_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Action_EndInvoke_m7263/* method */
	, &Action_t139_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Action_t139_Action_EndInvoke_m7263_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 65/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Action_t139_MethodInfos[] =
{
	&Action__ctor_m4319_MethodInfo,
	&Action_Invoke_m7261_MethodInfo,
	&Action_BeginInvoke_m7262_MethodInfo,
	&Action_EndInvoke_m7263_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2554_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2555_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2556_MethodInfo;
extern const MethodInfo Delegate_Clone_m2557_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2558_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2559_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2560_MethodInfo;
extern const MethodInfo Action_Invoke_m7261_MethodInfo;
extern const MethodInfo Action_BeginInvoke_m7262_MethodInfo;
extern const MethodInfo Action_EndInvoke_m7263_MethodInfo;
static const Il2CppMethodReference Action_t139_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&Action_Invoke_m7261_MethodInfo,
	&Action_BeginInvoke_m7262_MethodInfo,
	&Action_EndInvoke_m7263_MethodInfo,
};
static bool Action_t139_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t512_0_0_0;
static Il2CppInterfaceOffsetPair Action_t139_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Action_t139_0_0_0;
extern const Il2CppType Action_t139_1_0_0;
extern const Il2CppType MulticastDelegate_t307_0_0_0;
struct Action_t139;
const Il2CppTypeDefinitionMetadata Action_t139_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Action_t139_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, Action_t139_VTable/* vtableMethods */
	, Action_t139_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Action_t139_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action"/* name */
	, "System"/* namespaze */
	, Action_t139_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Action_t139_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Action_t139_0_0_0/* byval_arg */
	, &Action_t139_1_0_0/* this_arg */
	, &Action_t139_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_Action_t139/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Action_t139)/* instance_size */
	, sizeof (Action_t139)/* actualSize */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition System.Func`2
extern TypeInfo Func_2_t1595_il2cpp_TypeInfo;
extern const Il2CppGenericContainer Func_2_t1595_Il2CppGenericContainer;
extern TypeInfo Func_2_t1595_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Func_2_t1595_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &Func_2_t1595_Il2CppGenericContainer, NULL, "T", 0, 0 };
extern TypeInfo Func_2_t1595_gp_TResult_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter Func_2_t1595_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull = { &Func_2_t1595_Il2CppGenericContainer, NULL, "TResult", 1, 0 };
static const Il2CppGenericParameter* Func_2_t1595_Il2CppGenericParametersArray[2] = 
{
	&Func_2_t1595_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
	&Func_2_t1595_gp_TResult_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer Func_2_t1595_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&Func_2_t1595_il2cpp_TypeInfo, 2, 0, Func_2_t1595_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo Func_2_t1595_Func_2__ctor_m7329_ParameterInfos[] = 
{
	{"object", 0, 134217780, 0, &Object_t_0_0_0},
	{"method", 1, 134217781, 0, &IntPtr_t_0_0_0},
};
// System.Void System.Func`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo Func_2__ctor_m7329_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &Func_2_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1595_Func_2__ctor_m7329_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 66/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Func_2_t1595_gp_0_0_0_0;
extern const Il2CppType Func_2_t1595_gp_0_0_0_0;
static const ParameterInfo Func_2_t1595_Func_2_Invoke_m7330_ParameterInfos[] = 
{
	{"arg1", 0, 134217782, 0, &Func_2_t1595_gp_0_0_0_0},
};
extern const Il2CppType Func_2_t1595_gp_1_0_0_0;
// TResult System.Func`2::Invoke(T)
extern const MethodInfo Func_2_Invoke_m7330_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &Func_2_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Func_2_t1595_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1595_Func_2_Invoke_m7330_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 67/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Func_2_t1595_gp_0_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo Func_2_t1595_Func_2_BeginInvoke_m7331_ParameterInfos[] = 
{
	{"arg1", 0, 134217783, 0, &Func_2_t1595_gp_0_0_0_0},
	{"callback", 1, 134217784, 0, &AsyncCallback_t305_0_0_0},
	{"object", 2, 134217785, 0, &Object_t_0_0_0},
};
// System.IAsyncResult System.Func`2::BeginInvoke(T,System.AsyncCallback,System.Object)
extern const MethodInfo Func_2_BeginInvoke_m7331_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &Func_2_t1595_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1595_Func_2_BeginInvoke_m7331_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 68/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo Func_2_t1595_Func_2_EndInvoke_m7332_ParameterInfos[] = 
{
	{"result", 0, 134217786, 0, &IAsyncResult_t304_0_0_0},
};
// TResult System.Func`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo Func_2_EndInvoke_m7332_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &Func_2_t1595_il2cpp_TypeInfo/* declaring_type */
	, &Func_2_t1595_gp_1_0_0_0/* return_type */
	, NULL/* invoker_method */
	, Func_2_t1595_Func_2_EndInvoke_m7332_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 69/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Func_2_t1595_MethodInfos[] =
{
	&Func_2__ctor_m7329_MethodInfo,
	&Func_2_Invoke_m7330_MethodInfo,
	&Func_2_BeginInvoke_m7331_MethodInfo,
	&Func_2_EndInvoke_m7332_MethodInfo,
	NULL
};
extern const MethodInfo Func_2_Invoke_m7330_MethodInfo;
extern const MethodInfo Func_2_BeginInvoke_m7331_MethodInfo;
extern const MethodInfo Func_2_EndInvoke_m7332_MethodInfo;
static const Il2CppMethodReference Func_2_t1595_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&Func_2_Invoke_m7330_MethodInfo,
	&Func_2_BeginInvoke_m7331_MethodInfo,
	&Func_2_EndInvoke_m7332_MethodInfo,
};
static bool Func_2_t1595_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Func_2_t1595_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType Func_2_t1595_0_0_0;
extern const Il2CppType Func_2_t1595_1_0_0;
struct Func_2_t1595;
const Il2CppTypeDefinitionMetadata Func_2_t1595_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Func_2_t1595_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, Func_2_t1595_VTable/* vtableMethods */
	, Func_2_t1595_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Func_2_t1595_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Func`2"/* name */
	, "System"/* namespaze */
	, Func_2_t1595_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Func_2_t1595_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Func_2_t1595_0_0_0/* byval_arg */
	, &Func_2_t1595_1_0_0/* this_arg */
	, &Func_2_t1595_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &Func_2_t1595_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// <PrivateImplementationDetails>/$ArrayType$136
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU.h"
// Metadata Definition <PrivateImplementationDetails>/$ArrayType$136
extern TypeInfo U24ArrayTypeU24136_t1580_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$136
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeUMethodDeclarations.h"
static const MethodInfo* U24ArrayTypeU24136_t1580_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference U24ArrayTypeU24136_t1580_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool U24ArrayTypeU24136_t1580_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U24ArrayTypeU24136_t1580_0_0_0;
extern const Il2CppType U24ArrayTypeU24136_t1580_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1581_il2cpp_TypeInfo;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1581_0_0_0;
const Il2CppTypeDefinitionMetadata U24ArrayTypeU24136_t1580_DefinitionMetadata = 
{
	&U3CPrivateImplementationDetailsU3E_t1581_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, U24ArrayTypeU24136_t1580_VTable/* vtableMethods */
	, U24ArrayTypeU24136_t1580_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo U24ArrayTypeU24136_t1580_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "$ArrayType$136"/* name */
	, ""/* namespaze */
	, U24ArrayTypeU24136_t1580_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U24ArrayTypeU24136_t1580_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &U24ArrayTypeU24136_t1580_0_0_0/* byval_arg */
	, &U24ArrayTypeU24136_t1580_1_0_0/* this_arg */
	, &U24ArrayTypeU24136_t1580_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)U24ArrayTypeU24136_t1580_marshal/* marshal_to_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t1580_marshal_back/* marshal_from_native_func */
	, (methodPointerType)U24ArrayTypeU24136_t1580_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (U24ArrayTypeU24136_t1580)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (U24ArrayTypeU24136_t1580)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(U24ArrayTypeU24136_t1580_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3E.h"
// Metadata Definition <PrivateImplementationDetails>
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3EMethodDeclarations.h"
static const MethodInfo* U3CPrivateImplementationDetailsU3E_t1581_MethodInfos[] =
{
	NULL
};
static const Il2CppType* U3CPrivateImplementationDetailsU3E_t1581_il2cpp_TypeInfo__nestedTypes[1] =
{
	&U24ArrayTypeU24136_t1580_0_0_0,
};
static const Il2CppMethodReference U3CPrivateImplementationDetailsU3E_t1581_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool U3CPrivateImplementationDetailsU3E_t1581_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_System_Core_dll_Image;
extern const Il2CppType U3CPrivateImplementationDetailsU3E_t1581_1_0_0;
struct U3CPrivateImplementationDetailsU3E_t1581;
const Il2CppTypeDefinitionMetadata U3CPrivateImplementationDetailsU3E_t1581_DefinitionMetadata = 
{
	NULL/* declaringType */
	, U3CPrivateImplementationDetailsU3E_t1581_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, U3CPrivateImplementationDetailsU3E_t1581_VTable/* vtableMethods */
	, U3CPrivateImplementationDetailsU3E_t1581_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 35/* fieldStart */

};
TypeInfo U3CPrivateImplementationDetailsU3E_t1581_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3E_t1581_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &U3CPrivateImplementationDetailsU3E_t1581_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 29/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3E_t1581_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3E_t1581_1_0_0/* this_arg */
	, &U3CPrivateImplementationDetailsU3E_t1581_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1581)/* instance_size */
	, sizeof (U3CPrivateImplementationDetailsU3E_t1581)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3E_t1581_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
