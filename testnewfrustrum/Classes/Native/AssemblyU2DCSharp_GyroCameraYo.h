﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t11;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// GyroCameraYo
struct  GyroCameraYo_t14  : public MonoBehaviour_t7
{
	// UnityEngine.Transform GyroCameraYo::parentTransform
	Transform_t11 * ___parentTransform_2;
	// UnityEngine.Transform GyroCameraYo::transform
	Transform_t11 * ___transform_3;
	// UnityEngine.Quaternion GyroCameraYo::rotFix
	Quaternion_t13  ___rotFix_4;
	// UnityEngine.Vector3 GyroCameraYo::rotateAround
	Vector3_t15  ___rotateAround_5;
};
