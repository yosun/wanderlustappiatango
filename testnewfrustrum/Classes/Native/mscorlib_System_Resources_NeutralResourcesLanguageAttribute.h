﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
struct  NeutralResourcesLanguageAttribute_t1588  : public Attribute_t138
{
	// System.String System.Resources.NeutralResourcesLanguageAttribute::culture
	String_t* ___culture_0;
};
