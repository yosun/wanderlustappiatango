﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>
struct Enumerator_t3325;
// System.Object
struct Object_t;
// UnityEngine.CanvasGroup
struct CanvasGroup_t442;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t338;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m17920(__this, ___l, method) (( void (*) (Enumerator_t3325 *, List_1_t338 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17921(__this, method) (( Object_t * (*) (Enumerator_t3325 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::Dispose()
#define Enumerator_Dispose_m17922(__this, method) (( void (*) (Enumerator_t3325 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::VerifyState()
#define Enumerator_VerifyState_m17923(__this, method) (( void (*) (Enumerator_t3325 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::MoveNext()
#define Enumerator_MoveNext_m17924(__this, method) (( bool (*) (Enumerator_t3325 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.CanvasGroup>::get_Current()
#define Enumerator_get_Current_m17925(__this, method) (( CanvasGroup_t442 * (*) (Enumerator_t3325 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
