﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.BaseVertexEffect
struct BaseVertexEffect_t386;
// UnityEngine.UI.Graphic
struct Graphic_t278;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t315;

// System.Void UnityEngine.UI.BaseVertexEffect::.ctor()
extern "C" void BaseVertexEffect__ctor_m1924 (BaseVertexEffect_t386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Graphic UnityEngine.UI.BaseVertexEffect::get_graphic()
extern "C" Graphic_t278 * BaseVertexEffect_get_graphic_m1925 (BaseVertexEffect_t386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseVertexEffect::OnEnable()
extern "C" void BaseVertexEffect_OnEnable_m1926 (BaseVertexEffect_t386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseVertexEffect::OnDisable()
extern "C" void BaseVertexEffect_OnDisable_m1927 (BaseVertexEffect_t386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseVertexEffect::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
