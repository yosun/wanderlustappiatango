﻿#pragma once
#include <stdint.h>
// System.MulticastDelegate
struct MulticastDelegate_t307;
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.MulticastDelegate
struct  MulticastDelegate_t307  : public Delegate_t143
{
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t307 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t307 * ___kpm_next_10;
};
