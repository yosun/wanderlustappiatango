﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t461;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>
struct  ReadOnlyCollection_1_t3757  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>::list
	Object_t* ___list_0;
};
