﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.Prop>
struct List_1_t780;
// System.Object
struct Object_t;
// Vuforia.Prop
struct Prop_t97;
// System.Collections.Generic.IEnumerable`1<Vuforia.Prop>
struct IEnumerable_1_t778;
// System.Collections.Generic.IEnumerator`1<Vuforia.Prop>
struct IEnumerator_1_t859;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.Prop>
struct ICollection_1_t4219;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Prop>
struct ReadOnlyCollection_1_t3533;
// Vuforia.Prop[]
struct PropU5BU5D_t3526;
// System.Predicate`1<Vuforia.Prop>
struct Predicate_1_t3534;
// System.Comparison`1<Vuforia.Prop>
struct Comparison_1_t3535;
// System.Collections.Generic.List`1/Enumerator<Vuforia.Prop>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_13.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4544(__this, method) (( void (*) (List_1_t780 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m21491(__this, ___collection, method) (( void (*) (List_1_t780 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::.ctor(System.Int32)
#define List_1__ctor_m21492(__this, ___capacity, method) (( void (*) (List_1_t780 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::.cctor()
#define List_1__cctor_m21493(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21494(__this, method) (( Object_t* (*) (List_1_t780 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m21495(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t780 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m21496(__this, method) (( Object_t * (*) (List_1_t780 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m21497(__this, ___item, method) (( int32_t (*) (List_1_t780 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m21498(__this, ___item, method) (( bool (*) (List_1_t780 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m21499(__this, ___item, method) (( int32_t (*) (List_1_t780 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m21500(__this, ___index, ___item, method) (( void (*) (List_1_t780 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m21501(__this, ___item, method) (( void (*) (List_1_t780 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21502(__this, method) (( bool (*) (List_1_t780 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m21503(__this, method) (( bool (*) (List_1_t780 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m21504(__this, method) (( Object_t * (*) (List_1_t780 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m21505(__this, method) (( bool (*) (List_1_t780 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m21506(__this, method) (( bool (*) (List_1_t780 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m21507(__this, ___index, method) (( Object_t * (*) (List_1_t780 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m21508(__this, ___index, ___value, method) (( void (*) (List_1_t780 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::Add(T)
#define List_1_Add_m21509(__this, ___item, method) (( void (*) (List_1_t780 *, Object_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m21510(__this, ___newCount, method) (( void (*) (List_1_t780 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m21511(__this, ___collection, method) (( void (*) (List_1_t780 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m21512(__this, ___enumerable, method) (( void (*) (List_1_t780 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m21513(__this, ___collection, method) (( void (*) (List_1_t780 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Prop>::AsReadOnly()
#define List_1_AsReadOnly_m21514(__this, method) (( ReadOnlyCollection_1_t3533 * (*) (List_1_t780 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::Clear()
#define List_1_Clear_m21515(__this, method) (( void (*) (List_1_t780 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Prop>::Contains(T)
#define List_1_Contains_m21516(__this, ___item, method) (( bool (*) (List_1_t780 *, Object_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m21517(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t780 *, PropU5BU5D_t3526*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.Prop>::Find(System.Predicate`1<T>)
#define List_1_Find_m21518(__this, ___match, method) (( Object_t * (*) (List_1_t780 *, Predicate_1_t3534 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m21519(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3534 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m21520(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t780 *, int32_t, int32_t, Predicate_1_t3534 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Prop>::GetEnumerator()
#define List_1_GetEnumerator_m4536(__this, method) (( Enumerator_t861  (*) (List_1_t780 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::IndexOf(T)
#define List_1_IndexOf_m21521(__this, ___item, method) (( int32_t (*) (List_1_t780 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m21522(__this, ___start, ___delta, method) (( void (*) (List_1_t780 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m21523(__this, ___index, method) (( void (*) (List_1_t780 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::Insert(System.Int32,T)
#define List_1_Insert_m21524(__this, ___index, ___item, method) (( void (*) (List_1_t780 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m21525(__this, ___collection, method) (( void (*) (List_1_t780 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Prop>::Remove(T)
#define List_1_Remove_m21526(__this, ___item, method) (( bool (*) (List_1_t780 *, Object_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m21527(__this, ___match, method) (( int32_t (*) (List_1_t780 *, Predicate_1_t3534 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m21528(__this, ___index, method) (( void (*) (List_1_t780 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::Reverse()
#define List_1_Reverse_m21529(__this, method) (( void (*) (List_1_t780 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::Sort()
#define List_1_Sort_m21530(__this, method) (( void (*) (List_1_t780 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m21531(__this, ___comparison, method) (( void (*) (List_1_t780 *, Comparison_1_t3535 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.Prop>::ToArray()
#define List_1_ToArray_m21532(__this, method) (( PropU5BU5D_t3526* (*) (List_1_t780 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::TrimExcess()
#define List_1_TrimExcess_m21533(__this, method) (( void (*) (List_1_t780 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::get_Capacity()
#define List_1_get_Capacity_m21534(__this, method) (( int32_t (*) (List_1_t780 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m21535(__this, ___value, method) (( void (*) (List_1_t780 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::get_Count()
#define List_1_get_Count_m21536(__this, method) (( int32_t (*) (List_1_t780 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.Prop>::get_Item(System.Int32)
#define List_1_get_Item_m21537(__this, ___index, method) (( Object_t * (*) (List_1_t780 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Prop>::set_Item(System.Int32,T)
#define List_1_set_Item_m21538(__this, ___index, ___value, method) (( void (*) (List_1_t780 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
