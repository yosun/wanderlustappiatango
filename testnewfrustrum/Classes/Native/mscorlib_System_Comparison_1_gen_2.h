﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t278;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.UI.Graphic>
struct  Comparison_1_t281  : public MulticastDelegate_t307
{
};
