﻿#pragma once
#include <stdint.h>
// Vuforia.Trackable
struct Trackable_t565;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.Trackable>
struct  Predicate_1_t3409  : public MulticastDelegate_t307
{
};
