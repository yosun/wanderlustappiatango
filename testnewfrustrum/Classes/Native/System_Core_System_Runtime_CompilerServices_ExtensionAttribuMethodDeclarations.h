﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t899;

// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
extern "C" void ExtensionAttribute__ctor_m4681 (ExtensionAttribute_t899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
