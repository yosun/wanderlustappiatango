﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ImageTargetBehaviour
struct ImageTargetBehaviour_t49;

// System.Void Vuforia.ImageTargetBehaviour::.ctor()
extern "C" void ImageTargetBehaviour__ctor_m158 (ImageTargetBehaviour_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
