﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t1170;

// System.Void UnityEngine.GUILayoutUtility/LayoutCache::.ctor()
extern "C" void LayoutCache__ctor_m5816 (LayoutCache_t1170 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
