﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Text
struct Text_t23;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Text>
struct  Predicate_1_t3249  : public MulticastDelegate_t307
{
};
