﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.Word>
struct Enumerator_t839;
// System.Object
struct Object_t;
// Vuforia.Word
struct Word_t696;
// System.Collections.Generic.List`1<Vuforia.Word>
struct List_1_t690;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Word>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m20537(__this, ___l, method) (( void (*) (Enumerator_t839 *, List_1_t690 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Word>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20538(__this, method) (( Object_t * (*) (Enumerator_t839 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Word>::Dispose()
#define Enumerator_Dispose_m20539(__this, method) (( void (*) (Enumerator_t839 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Word>::VerifyState()
#define Enumerator_VerifyState_m20540(__this, method) (( void (*) (Enumerator_t839 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Word>::MoveNext()
#define Enumerator_MoveNext_m4482(__this, method) (( bool (*) (Enumerator_t839 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Word>::get_Current()
#define Enumerator_get_Current_m4481(__this, method) (( Object_t * (*) (Enumerator_t839 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
