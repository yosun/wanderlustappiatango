﻿#pragma once
#include <stdint.h>
// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t777;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.ISmartTerrainEventHandler>
struct  Predicate_1_t3517  : public MulticastDelegate_t307
{
};
