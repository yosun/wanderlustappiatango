﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t198;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m1947_gshared (Comparison_1_t198 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m1947(__this, ___object, ___method, method) (( void (*) (Comparison_1_t198 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m1947_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m15303_gshared (Comparison_1_t198 * __this, RaycastResult_t231  ___x, RaycastResult_t231  ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m15303(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t198 *, RaycastResult_t231 , RaycastResult_t231 , const MethodInfo*))Comparison_1_Invoke_m15303_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m15304_gshared (Comparison_1_t198 * __this, RaycastResult_t231  ___x, RaycastResult_t231  ___y, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m15304(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t198 *, RaycastResult_t231 , RaycastResult_t231 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m15304_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m15305_gshared (Comparison_1_t198 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m15305(__this, ___result, method) (( int32_t (*) (Comparison_1_t198 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m15305_gshared)(__this, ___result, method)
