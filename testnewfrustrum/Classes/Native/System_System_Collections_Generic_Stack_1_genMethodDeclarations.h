﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<DG.Tweening.Tween>
struct Stack_1_t979;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<DG.Tweening.Tween>
struct IEnumerator_1_t4287;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// DG.Tweening.Tween
struct Tween_t934;
// System.Collections.Generic.Stack`1/Enumerator<DG.Tweening.Tween>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.Stack`1<DG.Tweening.Tween>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_1MethodDeclarations.h"
#define Stack_1__ctor_m5540(__this, method) (( void (*) (Stack_1_t979 *, const MethodInfo*))Stack_1__ctor_m15420_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<DG.Tweening.Tween>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m23646(__this, method) (( bool (*) (Stack_1_t979 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m15421_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<DG.Tweening.Tween>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m23647(__this, method) (( Object_t * (*) (Stack_1_t979 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m15422_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<DG.Tweening.Tween>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m23648(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t979 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m15423_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<DG.Tweening.Tween>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23649(__this, method) (( Object_t* (*) (Stack_1_t979 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15424_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<DG.Tweening.Tween>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m23650(__this, method) (( Object_t * (*) (Stack_1_t979 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m15425_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<DG.Tweening.Tween>::Peek()
#define Stack_1_Peek_m23651(__this, method) (( Tween_t934 * (*) (Stack_1_t979 *, const MethodInfo*))Stack_1_Peek_m15426_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<DG.Tweening.Tween>::Pop()
#define Stack_1_Pop_m5536(__this, method) (( Tween_t934 * (*) (Stack_1_t979 *, const MethodInfo*))Stack_1_Pop_m15427_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<DG.Tweening.Tween>::Push(T)
#define Stack_1_Push_m5537(__this, ___t, method) (( void (*) (Stack_1_t979 *, Tween_t934 *, const MethodInfo*))Stack_1_Push_m15428_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<DG.Tweening.Tween>::get_Count()
#define Stack_1_get_Count_m23652(__this, method) (( int32_t (*) (Stack_1_t979 *, const MethodInfo*))Stack_1_get_Count_m15429_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<DG.Tweening.Tween>::GetEnumerator()
#define Stack_1_GetEnumerator_m23653(__this, method) (( Enumerator_t3684  (*) (Stack_1_t979 *, const MethodInfo*))Stack_1_GetEnumerator_m15430_gshared)(__this, method)
