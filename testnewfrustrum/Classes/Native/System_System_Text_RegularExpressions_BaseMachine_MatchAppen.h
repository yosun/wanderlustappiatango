﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.Match
struct Match_t1061;
// System.Text.StringBuilder
struct StringBuilder_t423;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Text.RegularExpressions.BaseMachine/MatchAppendEvaluator
struct  MatchAppendEvaluator_t1921  : public MulticastDelegate_t307
{
};
