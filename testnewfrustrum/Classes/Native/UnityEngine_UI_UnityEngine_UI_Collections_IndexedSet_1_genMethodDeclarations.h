﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>
struct IndexedSet_1_t262;
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t411;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ICanvasElement>
struct IEnumerator_1_t4064;
// UnityEngine.UI.ICanvasElement[]
struct ICanvasElementU5BU5D_t3239;
// System.Predicate`1<UnityEngine.UI.ICanvasElement>
struct Predicate_1_t264;
// System.Comparison`1<UnityEngine.UI.ICanvasElement>
struct Comparison_1_t263;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m2064(__this, method) (( void (*) (IndexedSet_1_t262 *, const MethodInfo*))IndexedSet_1__ctor_m16490_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m16491(__this, method) (( Object_t * (*) (IndexedSet_1_t262 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m16492_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Add(T)
#define IndexedSet_1_Add_m16493(__this, ___item, method) (( void (*) (IndexedSet_1_t262 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m16494_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Remove(T)
#define IndexedSet_1_Remove_m16495(__this, ___item, method) (( bool (*) (IndexedSet_1_t262 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m16496_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m16497(__this, method) (( Object_t* (*) (IndexedSet_1_t262 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m16498_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Clear()
#define IndexedSet_1_Clear_m16499(__this, method) (( void (*) (IndexedSet_1_t262 *, const MethodInfo*))IndexedSet_1_Clear_m16500_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Contains(T)
#define IndexedSet_1_Contains_m16501(__this, ___item, method) (( bool (*) (IndexedSet_1_t262 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m16502_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m16503(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t262 *, ICanvasElementU5BU5D_t3239*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m16504_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Count()
#define IndexedSet_1_get_Count_m16505(__this, method) (( int32_t (*) (IndexedSet_1_t262 *, const MethodInfo*))IndexedSet_1_get_Count_m16506_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m16507(__this, method) (( bool (*) (IndexedSet_1_t262 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m16508_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::IndexOf(T)
#define IndexedSet_1_IndexOf_m16509(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t262 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m16510_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m16511(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t262 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m16512_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m16513(__this, ___index, method) (( void (*) (IndexedSet_1_t262 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m16514_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m16515(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t262 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m16516_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m16517(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t262 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m16518_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m2069(__this, ___match, method) (( void (*) (IndexedSet_1_t262 *, Predicate_1_t264 *, const MethodInfo*))IndexedSet_1_RemoveAll_m16519_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m2070(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t262 *, Comparison_1_t263 *, const MethodInfo*))IndexedSet_1_Sort_m16520_gshared)(__this, ___sortLayoutFunction, method)
