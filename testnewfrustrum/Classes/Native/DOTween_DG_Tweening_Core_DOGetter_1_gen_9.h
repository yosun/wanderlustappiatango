﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>
struct  DOGetter_1_t1047  : public MulticastDelegate_t307
{
};
