﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$96
struct U24ArrayTypeU2496_t2575;
struct U24ArrayTypeU2496_t2575_marshaled;

void U24ArrayTypeU2496_t2575_marshal(const U24ArrayTypeU2496_t2575& unmarshaled, U24ArrayTypeU2496_t2575_marshaled& marshaled);
void U24ArrayTypeU2496_t2575_marshal_back(const U24ArrayTypeU2496_t2575_marshaled& marshaled, U24ArrayTypeU2496_t2575& unmarshaled);
void U24ArrayTypeU2496_t2575_marshal_cleanup(U24ArrayTypeU2496_t2575_marshaled& marshaled);
