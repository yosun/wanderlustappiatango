﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>
struct KeyValuePair_2_t1419;
// System.Type
struct Type_t;
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
struct SetDelegate_t1285;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"
#define KeyValuePair_2__ctor_m7006(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1419 *, Type_t *, SetDelegate_t1285 *, const MethodInfo*))KeyValuePair_2__ctor_m16908_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::get_Key()
#define KeyValuePair_2_get_Key_m25950(__this, method) (( Type_t * (*) (KeyValuePair_2_t1419 *, const MethodInfo*))KeyValuePair_2_get_Key_m16909_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m25951(__this, ___value, method) (( void (*) (KeyValuePair_2_t1419 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m16910_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::get_Value()
#define KeyValuePair_2_get_Value_m25952(__this, method) (( SetDelegate_t1285 * (*) (KeyValuePair_2_t1419 *, const MethodInfo*))KeyValuePair_2_get_Value_m16911_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m25953(__this, ___value, method) (( void (*) (KeyValuePair_2_t1419 *, SetDelegate_t1285 *, const MethodInfo*))KeyValuePair_2_set_Value_m16912_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::ToString()
#define KeyValuePair_2_ToString_m25954(__this, method) (( String_t* (*) (KeyValuePair_2_t1419 *, const MethodInfo*))KeyValuePair_2_ToString_m16913_gshared)(__this, method)
