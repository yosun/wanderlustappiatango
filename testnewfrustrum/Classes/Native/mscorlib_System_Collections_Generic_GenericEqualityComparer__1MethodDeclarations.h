﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.Guid>
struct GenericEqualityComparer_1_t2623;
// System.Guid
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m14013_gshared (GenericEqualityComparer_1_t2623 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m14013(__this, method) (( void (*) (GenericEqualityComparer_1_t2623 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m14013_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m27709_gshared (GenericEqualityComparer_1_t2623 * __this, Guid_t108  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m27709(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t2623 *, Guid_t108 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m27709_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Guid>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m27710_gshared (GenericEqualityComparer_1_t2623 * __this, Guid_t108  ___x, Guid_t108  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m27710(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t2623 *, Guid_t108 , Guid_t108 , const MethodInfo*))GenericEqualityComparer_1_Equals_m27710_gshared)(__this, ___x, ___y, method)
