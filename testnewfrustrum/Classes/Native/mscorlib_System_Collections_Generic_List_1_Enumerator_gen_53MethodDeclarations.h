﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.UInt16>
struct Enumerator_t3688;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.UInt16>
struct List_1_t3687;

// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt16>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m23821_gshared (Enumerator_t3688 * __this, List_1_t3687 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m23821(__this, ___l, method) (( void (*) (Enumerator_t3688 *, List_1_t3687 *, const MethodInfo*))Enumerator__ctor_m23821_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m23822_gshared (Enumerator_t3688 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m23822(__this, method) (( Object_t * (*) (Enumerator_t3688 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m23822_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt16>::Dispose()
extern "C" void Enumerator_Dispose_m23823_gshared (Enumerator_t3688 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m23823(__this, method) (( void (*) (Enumerator_t3688 *, const MethodInfo*))Enumerator_Dispose_m23823_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.UInt16>::VerifyState()
extern "C" void Enumerator_VerifyState_m23824_gshared (Enumerator_t3688 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m23824(__this, method) (( void (*) (Enumerator_t3688 *, const MethodInfo*))Enumerator_VerifyState_m23824_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.UInt16>::MoveNext()
extern "C" bool Enumerator_MoveNext_m23825_gshared (Enumerator_t3688 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m23825(__this, method) (( bool (*) (Enumerator_t3688 *, const MethodInfo*))Enumerator_MoveNext_m23825_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.UInt16>::get_Current()
extern "C" uint16_t Enumerator_get_Current_m23826_gshared (Enumerator_t3688 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m23826(__this, method) (( uint16_t (*) (Enumerator_t3688 *, const MethodInfo*))Enumerator_get_Current_m23826_gshared)(__this, method)
