﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Nullable`1<System.TimeSpan>
struct Nullable_1_t2598;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.Nullable`1<System.TimeSpan>
#include "mscorlib_System_Nullable_1_gen_1.h"

// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C" void Nullable_1__ctor_m14009_gshared (Nullable_1_t2598 * __this, TimeSpan_t112  ___value, const MethodInfo* method);
#define Nullable_1__ctor_m14009(__this, ___value, method) (( void (*) (Nullable_1_t2598 *, TimeSpan_t112 , const MethodInfo*))Nullable_1__ctor_m14009_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m14010_gshared (Nullable_1_t2598 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m14010(__this, method) (( bool (*) (Nullable_1_t2598 *, const MethodInfo*))Nullable_1_get_HasValue_m14010_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern "C" TimeSpan_t112  Nullable_1_get_Value_m14011_gshared (Nullable_1_t2598 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m14011(__this, method) (( TimeSpan_t112  (*) (Nullable_1_t2598 *, const MethodInfo*))Nullable_1_get_Value_m14011_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m27698_gshared (Nullable_1_t2598 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m27698(__this, ___other, method) (( bool (*) (Nullable_1_t2598 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m27698_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m27699_gshared (Nullable_1_t2598 * __this, Nullable_1_t2598  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m27699(__this, ___other, method) (( bool (*) (Nullable_1_t2598 *, Nullable_1_t2598 , const MethodInfo*))Nullable_1_Equals_m27699_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m27700_gshared (Nullable_1_t2598 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m27700(__this, method) (( int32_t (*) (Nullable_1_t2598 *, const MethodInfo*))Nullable_1_GetHashCode_m27700_gshared)(__this, method)
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern "C" String_t* Nullable_1_ToString_m27701_gshared (Nullable_1_t2598 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m27701(__this, method) (( String_t* (*) (Nullable_1_t2598 *, const MethodInfo*))Nullable_1_ToString_m27701_gshared)(__this, method)
