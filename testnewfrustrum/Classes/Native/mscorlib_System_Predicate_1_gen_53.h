﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.UInt16
#include "mscorlib_System_UInt16.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.UInt16>
struct  Predicate_1_t3692  : public MulticastDelegate_t307
{
};
