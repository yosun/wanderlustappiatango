﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>
struct Enumerator_t809;
// System.Object
struct Object_t;
// Vuforia.DataSetImpl
struct DataSetImpl_t578;
// System.Collections.Generic.List`1<Vuforia.DataSetImpl>
struct List_1_t624;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m19467(__this, ___l, method) (( void (*) (Enumerator_t809 *, List_1_t624 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19468(__this, method) (( Object_t * (*) (Enumerator_t809 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::Dispose()
#define Enumerator_Dispose_m19469(__this, method) (( void (*) (Enumerator_t809 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::VerifyState()
#define Enumerator_VerifyState_m19470(__this, method) (( void (*) (Enumerator_t809 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::MoveNext()
#define Enumerator_MoveNext_m4388(__this, method) (( bool (*) (Enumerator_t809 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::get_Current()
#define Enumerator_get_Current_m4387(__this, method) (( DataSetImpl_t578 * (*) (Enumerator_t809 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
