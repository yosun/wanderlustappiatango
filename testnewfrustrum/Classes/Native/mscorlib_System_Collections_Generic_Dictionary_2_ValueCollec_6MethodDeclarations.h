﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct Enumerator_t820;
// System.Object
struct Object_t;
// Vuforia.Image
struct Image_t615;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct Dictionary_2_t608;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28MethodDeclarations.h"
#define Enumerator__ctor_m19831(__this, ___host, method) (( void (*) (Enumerator_t820 *, Dictionary_2_t608 *, const MethodInfo*))Enumerator__ctor_m16289_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m19832(__this, method) (( Object_t * (*) (Enumerator_t820 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16290_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::Dispose()
#define Enumerator_Dispose_m19833(__this, method) (( void (*) (Enumerator_t820 *, const MethodInfo*))Enumerator_Dispose_m16291_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::MoveNext()
#define Enumerator_MoveNext_m4429(__this, method) (( bool (*) (Enumerator_t820 *, const MethodInfo*))Enumerator_MoveNext_m16292_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Current()
#define Enumerator_get_Current_m4428(__this, method) (( Image_t615 * (*) (Enumerator_t820 *, const MethodInfo*))Enumerator_get_Current_m16293_gshared)(__this, method)
