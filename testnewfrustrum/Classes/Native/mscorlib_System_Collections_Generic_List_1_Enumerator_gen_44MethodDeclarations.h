﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>
struct Enumerator_t3380;
// System.Object
struct Object_t;
// Vuforia.VirtualButton
struct VirtualButton_t731;
// System.Collections.Generic.List`1<Vuforia.VirtualButton>
struct List_1_t799;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m18751(__this, ___l, method) (( void (*) (Enumerator_t3380 *, List_1_t799 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18752(__this, method) (( Object_t * (*) (Enumerator_t3380 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::Dispose()
#define Enumerator_Dispose_m18753(__this, method) (( void (*) (Enumerator_t3380 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::VerifyState()
#define Enumerator_VerifyState_m18754(__this, method) (( void (*) (Enumerator_t3380 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::MoveNext()
#define Enumerator_MoveNext_m18755(__this, method) (( bool (*) (Enumerator_t3380 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButton>::get_Current()
#define Enumerator_get_Current_m18756(__this, method) (( VirtualButton_t731 * (*) (Enumerator_t3380 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
