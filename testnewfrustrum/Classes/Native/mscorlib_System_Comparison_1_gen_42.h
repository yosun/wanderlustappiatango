﻿#pragma once
#include <stdint.h>
// Vuforia.Surface
struct Surface_t98;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Surface>
struct  Comparison_1_t3539  : public MulticastDelegate_t307
{
};
