﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>
struct Enumerator_t3250;
// System.Object
struct Object_t;
// UnityEngine.UI.Text
struct Text_t23;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t437;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m16893(__this, ___l, method) (( void (*) (Enumerator_t3250 *, List_1_t437 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16894(__this, method) (( Object_t * (*) (Enumerator_t3250 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::Dispose()
#define Enumerator_Dispose_m16895(__this, method) (( void (*) (Enumerator_t3250 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::VerifyState()
#define Enumerator_VerifyState_m16896(__this, method) (( void (*) (Enumerator_t3250 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::MoveNext()
#define Enumerator_MoveNext_m16897(__this, method) (( bool (*) (Enumerator_t3250 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Text>::get_Current()
#define Enumerator_get_Current_m16898(__this, method) (( Text_t23 * (*) (Enumerator_t3250 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
