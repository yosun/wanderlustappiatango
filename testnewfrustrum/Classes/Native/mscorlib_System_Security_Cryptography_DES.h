﻿#pragma once
#include <stdint.h>
// System.Byte[,]
struct ByteU5BU2CU5D_t2396;
// System.Security.Cryptography.SymmetricAlgorithm
#include "mscorlib_System_Security_Cryptography_SymmetricAlgorithm.h"
// System.Security.Cryptography.DES
struct  DES_t1823  : public SymmetricAlgorithm_t1687
{
};
struct DES_t1823_StaticFields{
	// System.Byte[,] System.Security.Cryptography.DES::weakKeys
	ByteU5BU2CU5D_t2396* ___weakKeys_10;
	// System.Byte[,] System.Security.Cryptography.DES::semiWeakKeys
	ByteU5BU2CU5D_t2396* ___semiWeakKeys_11;
};
