﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.UInt16>
struct List_1_t3687;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.UInt16>
struct IEnumerable_1_t4293;
// System.Collections.Generic.IEnumerator`1<System.UInt16>
struct IEnumerator_1_t4180;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<System.UInt16>
struct ICollection_1_t4173;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt16>
struct ReadOnlyCollection_1_t3690;
// System.UInt16[]
struct UInt16U5BU5D_t1060;
// System.Predicate`1<System.UInt16>
struct Predicate_1_t3692;
// System.Comparison`1<System.UInt16>
struct Comparison_1_t3696;
// System.Collections.Generic.List`1/Enumerator<System.UInt16>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_53.h"

// System.Void System.Collections.Generic.List`1<System.UInt16>::.ctor()
extern "C" void List_1__ctor_m23722_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1__ctor_m23722(__this, method) (( void (*) (List_1_t3687 *, const MethodInfo*))List_1__ctor_m23722_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1__ctor_m23724_gshared (List_1_t3687 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1__ctor_m23724(__this, ___collection, method) (( void (*) (List_1_t3687 *, Object_t*, const MethodInfo*))List_1__ctor_m23724_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::.ctor(System.Int32)
extern "C" void List_1__ctor_m23726_gshared (List_1_t3687 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m23726(__this, ___capacity, method) (( void (*) (List_1_t3687 *, int32_t, const MethodInfo*))List_1__ctor_m23726_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::.cctor()
extern "C" void List_1__cctor_m23728_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m23728(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m23728_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.UInt16>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23730_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23730(__this, method) (( Object_t* (*) (List_1_t3687 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23730_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m23732_gshared (List_1_t3687 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m23732(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3687 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m23732_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m23734_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23734(__this, method) (( Object_t * (*) (List_1_t3687 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m23734_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m23736_gshared (List_1_t3687 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m23736(__this, ___item, method) (( int32_t (*) (List_1_t3687 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m23736_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m23738_gshared (List_1_t3687 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m23738(__this, ___item, method) (( bool (*) (List_1_t3687 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m23738_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m23740_gshared (List_1_t3687 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m23740(__this, ___item, method) (( int32_t (*) (List_1_t3687 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m23740_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m23742_gshared (List_1_t3687 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m23742(__this, ___index, ___item, method) (( void (*) (List_1_t3687 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m23742_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m23744_gshared (List_1_t3687 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m23744(__this, ___item, method) (( void (*) (List_1_t3687 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m23744_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23746_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23746(__this, method) (( bool (*) (List_1_t3687 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23746_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m23748_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23748(__this, method) (( bool (*) (List_1_t3687 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m23748_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.UInt16>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m23750_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m23750(__this, method) (( Object_t * (*) (List_1_t3687 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m23750_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m23752_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m23752(__this, method) (( bool (*) (List_1_t3687 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m23752_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m23754_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m23754(__this, method) (( bool (*) (List_1_t3687 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m23754_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m23756_gshared (List_1_t3687 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m23756(__this, ___index, method) (( Object_t * (*) (List_1_t3687 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m23756_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m23758_gshared (List_1_t3687 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m23758(__this, ___index, ___value, method) (( void (*) (List_1_t3687 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m23758_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::Add(T)
extern "C" void List_1_Add_m23760_gshared (List_1_t3687 * __this, uint16_t ___item, const MethodInfo* method);
#define List_1_Add_m23760(__this, ___item, method) (( void (*) (List_1_t3687 *, uint16_t, const MethodInfo*))List_1_Add_m23760_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m23762_gshared (List_1_t3687 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m23762(__this, ___newCount, method) (( void (*) (List_1_t3687 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m23762_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m23764_gshared (List_1_t3687 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m23764(__this, ___collection, method) (( void (*) (List_1_t3687 *, Object_t*, const MethodInfo*))List_1_AddCollection_m23764_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m23766_gshared (List_1_t3687 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m23766(__this, ___enumerable, method) (( void (*) (List_1_t3687 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m23766_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m23768_gshared (List_1_t3687 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m23768(__this, ___collection, method) (( void (*) (List_1_t3687 *, Object_t*, const MethodInfo*))List_1_AddRange_m23768_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.UInt16>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t3690 * List_1_AsReadOnly_m23770_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m23770(__this, method) (( ReadOnlyCollection_1_t3690 * (*) (List_1_t3687 *, const MethodInfo*))List_1_AsReadOnly_m23770_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::Clear()
extern "C" void List_1_Clear_m23772_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_Clear_m23772(__this, method) (( void (*) (List_1_t3687 *, const MethodInfo*))List_1_Clear_m23772_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::Contains(T)
extern "C" bool List_1_Contains_m23774_gshared (List_1_t3687 * __this, uint16_t ___item, const MethodInfo* method);
#define List_1_Contains_m23774(__this, ___item, method) (( bool (*) (List_1_t3687 *, uint16_t, const MethodInfo*))List_1_Contains_m23774_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m23776_gshared (List_1_t3687 * __this, UInt16U5BU5D_t1060* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m23776(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t3687 *, UInt16U5BU5D_t1060*, int32_t, const MethodInfo*))List_1_CopyTo_m23776_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.UInt16>::Find(System.Predicate`1<T>)
extern "C" uint16_t List_1_Find_m23778_gshared (List_1_t3687 * __this, Predicate_1_t3692 * ___match, const MethodInfo* method);
#define List_1_Find_m23778(__this, ___match, method) (( uint16_t (*) (List_1_t3687 *, Predicate_1_t3692 *, const MethodInfo*))List_1_Find_m23778_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m23780_gshared (Object_t * __this /* static, unused */, Predicate_1_t3692 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m23780(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3692 *, const MethodInfo*))List_1_CheckMatch_m23780_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m23782_gshared (List_1_t3687 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3692 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m23782(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t3687 *, int32_t, int32_t, Predicate_1_t3692 *, const MethodInfo*))List_1_GetIndex_m23782_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.UInt16>::GetEnumerator()
extern "C" Enumerator_t3688  List_1_GetEnumerator_m23784_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m23784(__this, method) (( Enumerator_t3688  (*) (List_1_t3687 *, const MethodInfo*))List_1_GetEnumerator_m23784_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m23786_gshared (List_1_t3687 * __this, uint16_t ___item, const MethodInfo* method);
#define List_1_IndexOf_m23786(__this, ___item, method) (( int32_t (*) (List_1_t3687 *, uint16_t, const MethodInfo*))List_1_IndexOf_m23786_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m23788_gshared (List_1_t3687 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m23788(__this, ___start, ___delta, method) (( void (*) (List_1_t3687 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m23788_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m23790_gshared (List_1_t3687 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m23790(__this, ___index, method) (( void (*) (List_1_t3687 *, int32_t, const MethodInfo*))List_1_CheckIndex_m23790_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m23792_gshared (List_1_t3687 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method);
#define List_1_Insert_m23792(__this, ___index, ___item, method) (( void (*) (List_1_t3687 *, int32_t, uint16_t, const MethodInfo*))List_1_Insert_m23792_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m23794_gshared (List_1_t3687 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m23794(__this, ___collection, method) (( void (*) (List_1_t3687 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m23794_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.UInt16>::Remove(T)
extern "C" bool List_1_Remove_m23796_gshared (List_1_t3687 * __this, uint16_t ___item, const MethodInfo* method);
#define List_1_Remove_m23796(__this, ___item, method) (( bool (*) (List_1_t3687 *, uint16_t, const MethodInfo*))List_1_Remove_m23796_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m23798_gshared (List_1_t3687 * __this, Predicate_1_t3692 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m23798(__this, ___match, method) (( int32_t (*) (List_1_t3687 *, Predicate_1_t3692 *, const MethodInfo*))List_1_RemoveAll_m23798_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m23800_gshared (List_1_t3687 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m23800(__this, ___index, method) (( void (*) (List_1_t3687 *, int32_t, const MethodInfo*))List_1_RemoveAt_m23800_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::Reverse()
extern "C" void List_1_Reverse_m23802_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_Reverse_m23802(__this, method) (( void (*) (List_1_t3687 *, const MethodInfo*))List_1_Reverse_m23802_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::Sort()
extern "C" void List_1_Sort_m23804_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_Sort_m23804(__this, method) (( void (*) (List_1_t3687 *, const MethodInfo*))List_1_Sort_m23804_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m23806_gshared (List_1_t3687 * __this, Comparison_1_t3696 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m23806(__this, ___comparison, method) (( void (*) (List_1_t3687 *, Comparison_1_t3696 *, const MethodInfo*))List_1_Sort_m23806_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.UInt16>::ToArray()
extern "C" UInt16U5BU5D_t1060* List_1_ToArray_m23808_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_ToArray_m23808(__this, method) (( UInt16U5BU5D_t1060* (*) (List_1_t3687 *, const MethodInfo*))List_1_ToArray_m23808_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::TrimExcess()
extern "C" void List_1_TrimExcess_m23810_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m23810(__this, method) (( void (*) (List_1_t3687 *, const MethodInfo*))List_1_TrimExcess_m23810_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m23812_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m23812(__this, method) (( int32_t (*) (List_1_t3687 *, const MethodInfo*))List_1_get_Capacity_m23812_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m23814_gshared (List_1_t3687 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m23814(__this, ___value, method) (( void (*) (List_1_t3687 *, int32_t, const MethodInfo*))List_1_set_Capacity_m23814_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.UInt16>::get_Count()
extern "C" int32_t List_1_get_Count_m23816_gshared (List_1_t3687 * __this, const MethodInfo* method);
#define List_1_get_Count_m23816(__this, method) (( int32_t (*) (List_1_t3687 *, const MethodInfo*))List_1_get_Count_m23816_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.UInt16>::get_Item(System.Int32)
extern "C" uint16_t List_1_get_Item_m23818_gshared (List_1_t3687 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m23818(__this, ___index, method) (( uint16_t (*) (List_1_t3687 *, int32_t, const MethodInfo*))List_1_get_Item_m23818_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.UInt16>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m23820_gshared (List_1_t3687 * __this, int32_t ___index, uint16_t ___value, const MethodInfo* method);
#define List_1_set_Item_m23820(__this, ___index, ___value, method) (( void (*) (List_1_t3687 *, int32_t, uint16_t, const MethodInfo*))List_1_set_Item_m23820_gshared)(__this, ___index, ___value, method)
