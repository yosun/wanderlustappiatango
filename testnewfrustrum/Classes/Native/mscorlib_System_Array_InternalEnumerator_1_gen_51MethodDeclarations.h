﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>
struct InternalEnumerator_1_t3626;
// System.Object
struct Object_t;
// Vuforia.VideoBackgroundAbstractBehaviour
struct VideoBackgroundAbstractBehaviour_t82;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m22771(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3626 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14858_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22772(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3626 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m22773(__this, method) (( void (*) (InternalEnumerator_1_t3626 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14862_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m22774(__this, method) (( bool (*) (InternalEnumerator_1_t3626 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14864_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m22775(__this, method) (( VideoBackgroundAbstractBehaviour_t82 * (*) (InternalEnumerator_1_t3626 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14866_gshared)(__this, method)
