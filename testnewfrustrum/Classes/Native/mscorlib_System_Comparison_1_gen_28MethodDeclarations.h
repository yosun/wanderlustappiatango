﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparison_1_t3401;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Int32>
#include "mscorlib_System_Comparison_1_gen_27MethodDeclarations.h"
#define Comparison_1__ctor_m19080(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3401 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m19036_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>::Invoke(T,T)
#define Comparison_1_Invoke_m19081(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3401 *, int32_t, int32_t, const MethodInfo*))Comparison_1_Invoke_m19037_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m19082(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3401 *, int32_t, int32_t, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m19038_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m19083(__this, ___result, method) (( int32_t (*) (Comparison_1_t3401 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m19039_gshared)(__this, ___result, method)
