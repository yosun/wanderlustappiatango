﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t77;

// System.Void Vuforia.TurnOffAbstractBehaviour::.ctor()
extern "C" void TurnOffAbstractBehaviour__ctor_m470 (TurnOffAbstractBehaviour_t77 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
