﻿#pragma once
#include <stdint.h>
// UnityEngine.Gyroscope
struct Gyroscope_t105;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Input
struct  Input_t102  : public Object_t
{
};
struct Input_t102_StaticFields{
	// UnityEngine.Gyroscope UnityEngine.Input::m_MainGyro
	Gyroscope_t105 * ___m_MainGyro_0;
};
