﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.PropertyAttribute
struct PropertyAttribute_t1329;

// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C" void PropertyAttribute__ctor_m6812 (PropertyAttribute_t1329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
