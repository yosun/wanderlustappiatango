﻿#pragma once
#include <stdint.h>
// Vuforia.ICloudRecoEventHandler[]
struct ICloudRecoEventHandlerU5BU5D_t3362;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
struct  List_1_t575  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::_items
	ICloudRecoEventHandlerU5BU5D_t3362* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t575_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>::EmptyArray
	ICloudRecoEventHandlerU5BU5D_t3362* ___EmptyArray_4;
};
