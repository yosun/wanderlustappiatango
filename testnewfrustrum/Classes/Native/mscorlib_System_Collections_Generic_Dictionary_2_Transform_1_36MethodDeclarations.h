﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,System.Int32>
struct Transform_1_t3571;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m22033_gshared (Transform_1_t3571 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m22033(__this, ___object, ___method, method) (( void (*) (Transform_1_t3571 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m22033_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,System.Int32>::Invoke(TKey,TValue)
extern "C" int32_t Transform_1_Invoke_m22034_gshared (Transform_1_t3571 * __this, int32_t ___key, VirtualButtonData_t643  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m22034(__this, ___key, ___value, method) (( int32_t (*) (Transform_1_t3571 *, int32_t, VirtualButtonData_t643 , const MethodInfo*))Transform_1_Invoke_m22034_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,System.Int32>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m22035_gshared (Transform_1_t3571 * __this, int32_t ___key, VirtualButtonData_t643  ___value, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m22035(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3571 *, int32_t, VirtualButtonData_t643 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m22035_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,System.Int32>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Transform_1_EndInvoke_m22036_gshared (Transform_1_t3571 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m22036(__this, ___result, method) (( int32_t (*) (Transform_1_t3571 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m22036_gshared)(__this, ___result, method)
