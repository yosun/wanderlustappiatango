﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t110;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Enum
struct  Enum_t167  : public ValueType_t524
{
};
struct Enum_t167_StaticFields{
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t110* ___split_char_0;
};
