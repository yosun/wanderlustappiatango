﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>
struct PrimeHelper_t3657;

// System.Void System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::.cctor()
extern "C" void PrimeHelper__cctor_m23195_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define PrimeHelper__cctor_m23195(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))PrimeHelper__cctor_m23195_gshared)(__this /* static, unused */, method)
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::TestPrime(System.Int32)
extern "C" bool PrimeHelper_TestPrime_m23196_gshared (Object_t * __this /* static, unused */, int32_t ___x, const MethodInfo* method);
#define PrimeHelper_TestPrime_m23196(__this /* static, unused */, ___x, method) (( bool (*) (Object_t * /* static, unused */, int32_t, const MethodInfo*))PrimeHelper_TestPrime_m23196_gshared)(__this /* static, unused */, ___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::CalcPrime(System.Int32)
extern "C" int32_t PrimeHelper_CalcPrime_m23197_gshared (Object_t * __this /* static, unused */, int32_t ___x, const MethodInfo* method);
#define PrimeHelper_CalcPrime_m23197(__this /* static, unused */, ___x, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, const MethodInfo*))PrimeHelper_CalcPrime_m23197_gshared)(__this /* static, unused */, ___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>::ToPrime(System.Int32)
extern "C" int32_t PrimeHelper_ToPrime_m23198_gshared (Object_t * __this /* static, unused */, int32_t ___x, const MethodInfo* method);
#define PrimeHelper_ToPrime_m23198(__this /* static, unused */, ___x, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, const MethodInfo*))PrimeHelper_ToPrime_m23198_gshared)(__this /* static, unused */, ___x, method)
