﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARRenderer/VideoBGCfgData
struct VideoBGCfgData_t662;
struct VideoBGCfgData_t662_marshaled;

void VideoBGCfgData_t662_marshal(const VideoBGCfgData_t662& unmarshaled, VideoBGCfgData_t662_marshaled& marshaled);
void VideoBGCfgData_t662_marshal_back(const VideoBGCfgData_t662_marshaled& marshaled, VideoBGCfgData_t662& unmarshaled);
void VideoBGCfgData_t662_marshal_cleanup(VideoBGCfgData_t662_marshaled& marshaled);
