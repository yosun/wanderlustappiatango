﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t1052;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::.ctor()
extern "C" void TweenerCore_3__ctor_m23981_gshared (TweenerCore_3_t1052 * __this, const MethodInfo* method);
#define TweenerCore_3__ctor_m23981(__this, method) (( void (*) (TweenerCore_3_t1052 *, const MethodInfo*))TweenerCore_3__ctor_m23981_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset()
extern "C" void TweenerCore_3_Reset_m23982_gshared (TweenerCore_3_t1052 * __this, const MethodInfo* method);
#define TweenerCore_3_Reset_m23982(__this, method) (( void (*) (TweenerCore_3_t1052 *, const MethodInfo*))TweenerCore_3_Reset_m23982_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Validate()
extern "C" bool TweenerCore_3_Validate_m23983_gshared (TweenerCore_3_t1052 * __this, const MethodInfo* method);
#define TweenerCore_3_Validate_m23983(__this, method) (( bool (*) (TweenerCore_3_t1052 *, const MethodInfo*))TweenerCore_3_Validate_m23983_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23984_gshared (TweenerCore_3_t1052 * __this, float ___elapsed, const MethodInfo* method);
#define TweenerCore_3_UpdateDelay_m23984(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1052 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m23984_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23985_gshared (TweenerCore_3_t1052 * __this, const MethodInfo* method);
#define TweenerCore_3_Startup_m23985(__this, method) (( bool (*) (TweenerCore_3_t1052 *, const MethodInfo*))TweenerCore_3_Startup_m23985_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool TweenerCore_3_ApplyTween_m23986_gshared (TweenerCore_3_t1052 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method);
#define TweenerCore_3_ApplyTween_m23986(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1052 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m23986_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
