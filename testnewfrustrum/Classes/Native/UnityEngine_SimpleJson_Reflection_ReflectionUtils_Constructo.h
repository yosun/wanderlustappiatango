﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct  ConstructorDelegate_t1286  : public MulticastDelegate_t307
{
};
