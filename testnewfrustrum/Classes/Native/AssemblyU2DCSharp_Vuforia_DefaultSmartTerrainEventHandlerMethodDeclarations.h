﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DefaultSmartTerrainEventHandler
struct DefaultSmartTerrainEventHandler_t43;
// Vuforia.Prop
struct Prop_t97;
// Vuforia.Surface
struct Surface_t98;

// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
extern "C" void DefaultSmartTerrainEventHandler__ctor_m142 (DefaultSmartTerrainEventHandler_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
extern "C" void DefaultSmartTerrainEventHandler_Start_m143 (DefaultSmartTerrainEventHandler_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
extern "C" void DefaultSmartTerrainEventHandler_OnDestroy_m144 (DefaultSmartTerrainEventHandler_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
extern "C" void DefaultSmartTerrainEventHandler_OnPropCreated_m145 (DefaultSmartTerrainEventHandler_t43 * __this, Object_t * ___prop, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
extern "C" void DefaultSmartTerrainEventHandler_OnSurfaceCreated_m146 (DefaultSmartTerrainEventHandler_t43 * __this, Object_t * ___surface, const MethodInfo* method) IL2CPP_METHOD_ATTR;
