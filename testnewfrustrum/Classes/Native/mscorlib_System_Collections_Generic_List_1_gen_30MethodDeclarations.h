﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.WordResult>
struct List_1_t689;
// System.Object
struct Object_t;
// Vuforia.WordResult
struct WordResult_t695;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult>
struct IEnumerable_1_t771;
// System.Collections.Generic.IEnumerator`1<Vuforia.WordResult>
struct IEnumerator_1_t892;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.WordResult>
struct ICollection_1_t4183;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordResult>
struct ReadOnlyCollection_1_t3486;
// Vuforia.WordResult[]
struct WordResultU5BU5D_t3481;
// System.Predicate`1<Vuforia.WordResult>
struct Predicate_1_t3487;
// System.Comparison`1<Vuforia.WordResult>
struct Comparison_1_t3488;
// System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_10.h"

// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4493(__this, method) (( void (*) (List_1_t689 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m20366(__this, ___collection, method) (( void (*) (List_1_t689 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.ctor(System.Int32)
#define List_1__ctor_m20367(__this, ___capacity, method) (( void (*) (List_1_t689 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.cctor()
#define List_1__cctor_m20368(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20369(__this, method) (( Object_t* (*) (List_1_t689 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20370(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t689 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20371(__this, method) (( Object_t * (*) (List_1_t689 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20372(__this, ___item, method) (( int32_t (*) (List_1_t689 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20373(__this, ___item, method) (( bool (*) (List_1_t689 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20374(__this, ___item, method) (( int32_t (*) (List_1_t689 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20375(__this, ___index, ___item, method) (( void (*) (List_1_t689 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20376(__this, ___item, method) (( void (*) (List_1_t689 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20377(__this, method) (( bool (*) (List_1_t689 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20378(__this, method) (( bool (*) (List_1_t689 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20379(__this, method) (( Object_t * (*) (List_1_t689 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20380(__this, method) (( bool (*) (List_1_t689 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20381(__this, method) (( bool (*) (List_1_t689 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20382(__this, ___index, method) (( Object_t * (*) (List_1_t689 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20383(__this, ___index, ___value, method) (( void (*) (List_1_t689 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Add(T)
#define List_1_Add_m20384(__this, ___item, method) (( void (*) (List_1_t689 *, WordResult_t695 *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20385(__this, ___newCount, method) (( void (*) (List_1_t689 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20386(__this, ___collection, method) (( void (*) (List_1_t689 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20387(__this, ___enumerable, method) (( void (*) (List_1_t689 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20388(__this, ___collection, method) (( void (*) (List_1_t689 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.WordResult>::AsReadOnly()
#define List_1_AsReadOnly_m20389(__this, method) (( ReadOnlyCollection_1_t3486 * (*) (List_1_t689 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Clear()
#define List_1_Clear_m20390(__this, method) (( void (*) (List_1_t689 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::Contains(T)
#define List_1_Contains_m20391(__this, ___item, method) (( bool (*) (List_1_t689 *, WordResult_t695 *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20392(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t689 *, WordResultU5BU5D_t3481*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.WordResult>::Find(System.Predicate`1<T>)
#define List_1_Find_m20393(__this, ___match, method) (( WordResult_t695 * (*) (List_1_t689 *, Predicate_1_t3487 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20394(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3487 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20395(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t689 *, int32_t, int32_t, Predicate_1_t3487 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.WordResult>::GetEnumerator()
#define List_1_GetEnumerator_m4483(__this, method) (( Enumerator_t840  (*) (List_1_t689 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::IndexOf(T)
#define List_1_IndexOf_m20396(__this, ___item, method) (( int32_t (*) (List_1_t689 *, WordResult_t695 *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20397(__this, ___start, ___delta, method) (( void (*) (List_1_t689 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20398(__this, ___index, method) (( void (*) (List_1_t689 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Insert(System.Int32,T)
#define List_1_Insert_m20399(__this, ___index, ___item, method) (( void (*) (List_1_t689 *, int32_t, WordResult_t695 *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20400(__this, ___collection, method) (( void (*) (List_1_t689 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::Remove(T)
#define List_1_Remove_m20401(__this, ___item, method) (( bool (*) (List_1_t689 *, WordResult_t695 *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20402(__this, ___match, method) (( int32_t (*) (List_1_t689 *, Predicate_1_t3487 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20403(__this, ___index, method) (( void (*) (List_1_t689 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Reverse()
#define List_1_Reverse_m20404(__this, method) (( void (*) (List_1_t689 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Sort()
#define List_1_Sort_m20405(__this, method) (( void (*) (List_1_t689 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20406(__this, ___comparison, method) (( void (*) (List_1_t689 *, Comparison_1_t3488 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.WordResult>::ToArray()
#define List_1_ToArray_m20407(__this, method) (( WordResultU5BU5D_t3481* (*) (List_1_t689 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::TrimExcess()
#define List_1_TrimExcess_m20408(__this, method) (( void (*) (List_1_t689 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::get_Capacity()
#define List_1_get_Capacity_m20409(__this, method) (( int32_t (*) (List_1_t689 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20410(__this, ___value, method) (( void (*) (List_1_t689 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::get_Count()
#define List_1_get_Count_m20411(__this, method) (( int32_t (*) (List_1_t689 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.WordResult>::get_Item(System.Int32)
#define List_1_get_Item_m20412(__this, ___index, method) (( WordResult_t695 * (*) (List_1_t689 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::set_Item(System.Int32,T)
#define List_1_set_Item_m20413(__this, ___index, ___value, method) (( void (*) (List_1_t689 *, int32_t, WordResult_t695 *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
