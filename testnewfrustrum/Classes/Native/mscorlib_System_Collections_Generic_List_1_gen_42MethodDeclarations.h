﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Type>
struct List_1_t749;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t4136;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t4137;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t4138;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Type>
struct ReadOnlyCollection_1_t3369;
// System.Type[]
struct TypeU5BU5D_t878;
// System.Predicate`1<System.Type>
struct Predicate_1_t3370;
// System.Comparison`1<System.Type>
struct Comparison_1_t3371;
// System.Collections.Generic.List`1/Enumerator<System.Type>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_18.h"

// System.Void System.Collections.Generic.List`1<System.Type>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4646(__this, method) (( void (*) (List_1_t749 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18490(__this, ___collection, method) (( void (*) (List_1_t749 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Type>::.ctor(System.Int32)
#define List_1__ctor_m18491(__this, ___capacity, method) (( void (*) (List_1_t749 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Type>::.cctor()
#define List_1__cctor_m18492(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Type>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18493(__this, method) (( Object_t* (*) (List_1_t749 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18494(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t749 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Type>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18495(__this, method) (( Object_t * (*) (List_1_t749 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Type>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18496(__this, ___item, method) (( int32_t (*) (List_1_t749 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Type>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18497(__this, ___item, method) (( bool (*) (List_1_t749 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Type>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m18498(__this, ___item, method) (( int32_t (*) (List_1_t749 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Type>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m18499(__this, ___index, ___item, method) (( void (*) (List_1_t749 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Type>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m18500(__this, ___item, method) (( void (*) (List_1_t749 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Type>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18501(__this, method) (( bool (*) (List_1_t749 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Type>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m18502(__this, method) (( bool (*) (List_1_t749 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Type>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m18503(__this, method) (( Object_t * (*) (List_1_t749 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Type>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m18504(__this, method) (( bool (*) (List_1_t749 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Type>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m18505(__this, method) (( bool (*) (List_1_t749 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Type>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m18506(__this, ___index, method) (( Object_t * (*) (List_1_t749 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Type>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m18507(__this, ___index, ___value, method) (( void (*) (List_1_t749 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Add(T)
#define List_1_Add_m18508(__this, ___item, method) (( void (*) (List_1_t749 *, Type_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Type>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m18509(__this, ___newCount, method) (( void (*) (List_1_t749 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Type>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m18510(__this, ___collection, method) (( void (*) (List_1_t749 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Type>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m18511(__this, ___enumerable, method) (( void (*) (List_1_t749 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Type>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m18512(__this, ___collection, method) (( void (*) (List_1_t749 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Type>::AsReadOnly()
#define List_1_AsReadOnly_m18513(__this, method) (( ReadOnlyCollection_1_t3369 * (*) (List_1_t749 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Clear()
#define List_1_Clear_m18514(__this, method) (( void (*) (List_1_t749 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Type>::Contains(T)
#define List_1_Contains_m18515(__this, ___item, method) (( bool (*) (List_1_t749 *, Type_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Type>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m18516(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t749 *, TypeU5BU5D_t878*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Type>::Find(System.Predicate`1<T>)
#define List_1_Find_m18517(__this, ___match, method) (( Type_t * (*) (List_1_t749 *, Predicate_1_t3370 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Type>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m18518(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3370 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Type>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m18519(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t749 *, int32_t, int32_t, Predicate_1_t3370 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Type>::GetEnumerator()
#define List_1_GetEnumerator_m4641(__this, method) (( Enumerator_t889  (*) (List_1_t749 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Type>::IndexOf(T)
#define List_1_IndexOf_m18520(__this, ___item, method) (( int32_t (*) (List_1_t749 *, Type_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m18521(__this, ___start, ___delta, method) (( void (*) (List_1_t749 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Type>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m18522(__this, ___index, method) (( void (*) (List_1_t749 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Insert(System.Int32,T)
#define List_1_Insert_m18523(__this, ___index, ___item, method) (( void (*) (List_1_t749 *, int32_t, Type_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Type>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m18524(__this, ___collection, method) (( void (*) (List_1_t749 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Type>::Remove(T)
#define List_1_Remove_m18525(__this, ___item, method) (( bool (*) (List_1_t749 *, Type_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Type>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18526(__this, ___match, method) (( int32_t (*) (List_1_t749 *, Predicate_1_t3370 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Type>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m18527(__this, ___index, method) (( void (*) (List_1_t749 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Reverse()
#define List_1_Reverse_m18528(__this, method) (( void (*) (List_1_t749 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Sort()
#define List_1_Sort_m18529(__this, method) (( void (*) (List_1_t749 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m18530(__this, ___comparison, method) (( void (*) (List_1_t749 *, Comparison_1_t3371 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Type>::ToArray()
#define List_1_ToArray_m7017(__this, method) (( TypeU5BU5D_t878* (*) (List_1_t749 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::TrimExcess()
#define List_1_TrimExcess_m18531(__this, method) (( void (*) (List_1_t749 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Type>::get_Capacity()
#define List_1_get_Capacity_m18532(__this, method) (( int32_t (*) (List_1_t749 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m18533(__this, ___value, method) (( void (*) (List_1_t749 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Type>::get_Count()
#define List_1_get_Count_m18534(__this, method) (( int32_t (*) (List_1_t749 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Type>::get_Item(System.Int32)
#define List_1_get_Item_m18535(__this, ___index, method) (( Type_t * (*) (List_1_t749 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Type>::set_Item(System.Int32,T)
#define List_1_set_Item_m18536(__this, ___index, ___value, method) (( void (*) (List_1_t749 *, int32_t, Type_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
