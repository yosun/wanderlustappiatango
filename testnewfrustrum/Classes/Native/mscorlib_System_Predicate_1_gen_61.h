﻿#pragma once
#include <stdint.h>
// UnityEngine.Networking.Match.MatchDirectConnectInfo
struct MatchDirectConnectInfo_t1260;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct  Predicate_1_t3799  : public MulticastDelegate_t307
{
};
