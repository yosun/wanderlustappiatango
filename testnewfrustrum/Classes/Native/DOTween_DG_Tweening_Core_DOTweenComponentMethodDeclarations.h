﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOTweenComponent
struct DOTweenComponent_t935;
// DG.Tweening.IDOTweenInit
struct IDOTweenInit_t1023;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// DG.Tweening.Tween
struct Tween_t934;

// System.Void DG.Tweening.Core.DOTweenComponent::Awake()
extern "C" void DOTweenComponent_Awake_m5307 (DOTweenComponent_t935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent::Start()
extern "C" void DOTweenComponent_Start_m5308 (DOTweenComponent_t935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent::Update()
extern "C" void DOTweenComponent_Update_m5309 (DOTweenComponent_t935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent::LateUpdate()
extern "C" void DOTweenComponent_LateUpdate_m5310 (DOTweenComponent_t935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent::FixedUpdate()
extern "C" void DOTweenComponent_FixedUpdate_m5311 (DOTweenComponent_t935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent::OnLevelWasLoaded()
extern "C" void DOTweenComponent_OnLevelWasLoaded_m5312 (DOTweenComponent_t935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent::OnDrawGizmos()
extern "C" void DOTweenComponent_OnDrawGizmos_m5313 (DOTweenComponent_t935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent::OnDestroy()
extern "C" void DOTweenComponent_OnDestroy_m5314 (DOTweenComponent_t935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent::OnApplicationQuit()
extern "C" void DOTweenComponent_OnApplicationQuit_m5315 (DOTweenComponent_t935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.IDOTweenInit DG.Tweening.Core.DOTweenComponent::SetCapacity(System.Int32,System.Int32)
extern "C" Object_t * DOTweenComponent_SetCapacity_m5316 (DOTweenComponent_t935 * __this, int32_t ___tweenersCapacity, int32_t ___sequencesCapacity, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForCompletion(DG.Tweening.Tween)
extern "C" Object_t * DOTweenComponent_WaitForCompletion_m5317 (DOTweenComponent_t935 * __this, Tween_t934 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForRewind(DG.Tweening.Tween)
extern "C" Object_t * DOTweenComponent_WaitForRewind_m5318 (DOTweenComponent_t935 * __this, Tween_t934 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForKill(DG.Tweening.Tween)
extern "C" Object_t * DOTweenComponent_WaitForKill_m5319 (DOTweenComponent_t935 * __this, Tween_t934 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32)
extern "C" Object_t * DOTweenComponent_WaitForElapsedLoops_m5320 (DOTweenComponent_t935 * __this, Tween_t934 * ___t, int32_t ___elapsedLoops, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForPosition(DG.Tweening.Tween,System.Single)
extern "C" Object_t * DOTweenComponent_WaitForPosition_m5321 (DOTweenComponent_t935 * __this, Tween_t934 * ___t, float ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DG.Tweening.Core.DOTweenComponent::WaitForStart(DG.Tweening.Tween)
extern "C" Object_t * DOTweenComponent_WaitForStart_m5322 (DOTweenComponent_t935 * __this, Tween_t934 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent::Create()
extern "C" void DOTweenComponent_Create_m5323 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent::DestroyInstance()
extern "C" void DOTweenComponent_DestroyInstance_m5324 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent::.ctor()
extern "C" void DOTweenComponent__ctor_m5325 (DOTweenComponent_t935 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
