﻿#pragma once
#include <stdint.h>
// Vuforia.VirtualButton[]
struct VirtualButtonU5BU5D_t3376;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.VirtualButton>
struct  List_1_t799  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.VirtualButton>::_items
	VirtualButtonU5BU5D_t3376* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.VirtualButton>::_version
	int32_t ____version_3;
};
struct List_1_t799_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.VirtualButton>::EmptyArray
	VirtualButtonU5BU5D_t3376* ___EmptyArray_4;
};
