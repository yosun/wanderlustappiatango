﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>
struct Comparer_1_t3592;
// System.Object
struct Object_t;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void Comparer_1__ctor_m22312_gshared (Comparer_1_t3592 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m22312(__this, method) (( void (*) (Comparer_1_t3592 *, const MethodInfo*))Comparer_1__ctor_m22312_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::.cctor()
extern "C" void Comparer_1__cctor_m22313_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m22313(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m22313_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m22314_gshared (Comparer_1_t3592 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m22314(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t3592 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m22314_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::get_Default()
extern "C" Comparer_1_t3592 * Comparer_1_get_Default_m22315_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m22315(__this /* static, unused */, method) (( Comparer_1_t3592 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m22315_gshared)(__this /* static, unused */, method)
