﻿#pragma once
#include <stdint.h>
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
struct EnvoyTerminatorSink_t2321;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink
struct  EnvoyTerminatorSink_t2321  : public Object_t
{
};
struct EnvoyTerminatorSink_t2321_StaticFields{
	// System.Runtime.Remoting.Messaging.EnvoyTerminatorSink System.Runtime.Remoting.Messaging.EnvoyTerminatorSink::Instance
	EnvoyTerminatorSink_t2321 * ___Instance_0;
};
