﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>
struct List_1_t746;
// System.Object
struct Object_t;
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t171;
// System.Collections.Generic.IEnumerable`1<Vuforia.IVideoBackgroundEventHandler>
struct IEnumerable_1_t4270;
// System.Collections.Generic.IEnumerator`1<Vuforia.IVideoBackgroundEventHandler>
struct IEnumerator_1_t4271;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>
struct ICollection_1_t4272;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IVideoBackgroundEventHandler>
struct ReadOnlyCollection_1_t3634;
// Vuforia.IVideoBackgroundEventHandler[]
struct IVideoBackgroundEventHandlerU5BU5D_t3632;
// System.Predicate`1<Vuforia.IVideoBackgroundEventHandler>
struct Predicate_1_t3635;
// System.Comparison`1<Vuforia.IVideoBackgroundEventHandler>
struct Comparison_1_t3636;
// System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_16.h"

// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4645(__this, method) (( void (*) (List_1_t746 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m22866(__this, ___collection, method) (( void (*) (List_1_t746 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Int32)
#define List_1__ctor_m22867(__this, ___capacity, method) (( void (*) (List_1_t746 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::.cctor()
#define List_1__cctor_m22868(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22869(__this, method) (( Object_t* (*) (List_1_t746 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m22870(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t746 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22871(__this, method) (( Object_t * (*) (List_1_t746 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m22872(__this, ___item, method) (( int32_t (*) (List_1_t746 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m22873(__this, ___item, method) (( bool (*) (List_1_t746 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m22874(__this, ___item, method) (( int32_t (*) (List_1_t746 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m22875(__this, ___index, ___item, method) (( void (*) (List_1_t746 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m22876(__this, ___item, method) (( void (*) (List_1_t746 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22877(__this, method) (( bool (*) (List_1_t746 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22878(__this, method) (( bool (*) (List_1_t746 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m22879(__this, method) (( Object_t * (*) (List_1_t746 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m22880(__this, method) (( bool (*) (List_1_t746 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m22881(__this, method) (( bool (*) (List_1_t746 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m22882(__this, ___index, method) (( Object_t * (*) (List_1_t746 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m22883(__this, ___index, ___value, method) (( void (*) (List_1_t746 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Add(T)
#define List_1_Add_m22884(__this, ___item, method) (( void (*) (List_1_t746 *, Object_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m22885(__this, ___newCount, method) (( void (*) (List_1_t746 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m22886(__this, ___collection, method) (( void (*) (List_1_t746 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m22887(__this, ___enumerable, method) (( void (*) (List_1_t746 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m22888(__this, ___collection, method) (( void (*) (List_1_t746 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::AsReadOnly()
#define List_1_AsReadOnly_m22889(__this, method) (( ReadOnlyCollection_1_t3634 * (*) (List_1_t746 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Clear()
#define List_1_Clear_m22890(__this, method) (( void (*) (List_1_t746 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Contains(T)
#define List_1_Contains_m22891(__this, ___item, method) (( bool (*) (List_1_t746 *, Object_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m22892(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t746 *, IVideoBackgroundEventHandlerU5BU5D_t3632*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Find(System.Predicate`1<T>)
#define List_1_Find_m22893(__this, ___match, method) (( Object_t * (*) (List_1_t746 *, Predicate_1_t3635 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m22894(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3635 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m22895(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t746 *, int32_t, int32_t, Predicate_1_t3635 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::GetEnumerator()
#define List_1_GetEnumerator_m4631(__this, method) (( Enumerator_t885  (*) (List_1_t746 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::IndexOf(T)
#define List_1_IndexOf_m22896(__this, ___item, method) (( int32_t (*) (List_1_t746 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m22897(__this, ___start, ___delta, method) (( void (*) (List_1_t746 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m22898(__this, ___index, method) (( void (*) (List_1_t746 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Insert(System.Int32,T)
#define List_1_Insert_m22899(__this, ___index, ___item, method) (( void (*) (List_1_t746 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m22900(__this, ___collection, method) (( void (*) (List_1_t746 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Remove(T)
#define List_1_Remove_m22901(__this, ___item, method) (( bool (*) (List_1_t746 *, Object_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m22902(__this, ___match, method) (( int32_t (*) (List_1_t746 *, Predicate_1_t3635 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m22903(__this, ___index, method) (( void (*) (List_1_t746 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Reverse()
#define List_1_Reverse_m22904(__this, method) (( void (*) (List_1_t746 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Sort()
#define List_1_Sort_m22905(__this, method) (( void (*) (List_1_t746 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m22906(__this, ___comparison, method) (( void (*) (List_1_t746 *, Comparison_1_t3636 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::ToArray()
#define List_1_ToArray_m22907(__this, method) (( IVideoBackgroundEventHandlerU5BU5D_t3632* (*) (List_1_t746 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::TrimExcess()
#define List_1_TrimExcess_m22908(__this, method) (( void (*) (List_1_t746 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::get_Capacity()
#define List_1_get_Capacity_m22909(__this, method) (( int32_t (*) (List_1_t746 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m22910(__this, ___value, method) (( void (*) (List_1_t746 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::get_Count()
#define List_1_get_Count_m22911(__this, method) (( int32_t (*) (List_1_t746 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::get_Item(System.Int32)
#define List_1_get_Item_m22912(__this, ___index, method) (( Object_t * (*) (List_1_t746 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>::set_Item(System.Int32,T)
#define List_1_set_Item_m22913(__this, ___index, ___value, method) (( void (*) (List_1_t746 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
