﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>
struct KeyValuePair_2_t3476;
// System.Type
struct Type_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18MethodDeclarations.h"
#define KeyValuePair_2__ctor_m20218(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3476 *, Type_t *, uint16_t, const MethodInfo*))KeyValuePair_2__ctor_m20125_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::get_Key()
#define KeyValuePair_2_get_Key_m20219(__this, method) (( Type_t * (*) (KeyValuePair_2_t3476 *, const MethodInfo*))KeyValuePair_2_get_Key_m20126_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20220(__this, ___value, method) (( void (*) (KeyValuePair_2_t3476 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m20127_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::get_Value()
#define KeyValuePair_2_get_Value_m20221(__this, method) (( uint16_t (*) (KeyValuePair_2_t3476 *, const MethodInfo*))KeyValuePair_2_get_Value_m20128_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20222(__this, ___value, method) (( void (*) (KeyValuePair_2_t3476 *, uint16_t, const MethodInfo*))KeyValuePair_2_set_Value_m20129_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::ToString()
#define KeyValuePair_2_ToString_m20223(__this, method) (( String_t* (*) (KeyValuePair_2_t3476 *, const MethodInfo*))KeyValuePair_2_ToString_m20130_gshared)(__this, method)
