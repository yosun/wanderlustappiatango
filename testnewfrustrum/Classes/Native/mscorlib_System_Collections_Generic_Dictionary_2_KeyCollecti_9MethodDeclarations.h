﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>
struct Enumerator_t3231;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3224;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16596_gshared (Enumerator_t3231 * __this, Dictionary_2_t3224 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m16596(__this, ___host, method) (( void (*) (Enumerator_t3231 *, Dictionary_2_t3224 *, const MethodInfo*))Enumerator__ctor_m16596_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16597_gshared (Enumerator_t3231 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16597(__this, method) (( Object_t * (*) (Enumerator_t3231 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16597_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m16598_gshared (Enumerator_t3231 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16598(__this, method) (( void (*) (Enumerator_t3231 *, const MethodInfo*))Enumerator_Dispose_m16598_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16599_gshared (Enumerator_t3231 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16599(__this, method) (( bool (*) (Enumerator_t3231 *, const MethodInfo*))Enumerator_MoveNext_m16599_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m16600_gshared (Enumerator_t3231 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16600(__this, method) (( Object_t * (*) (Enumerator_t3231 *, const MethodInfo*))Enumerator_get_Current_m16600_gshared)(__this, method)
