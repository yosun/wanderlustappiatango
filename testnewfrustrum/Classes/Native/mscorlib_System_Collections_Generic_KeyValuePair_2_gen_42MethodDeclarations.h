﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>
struct KeyValuePair_2_t3873;
// System.String
struct String_t;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_38MethodDeclarations.h"
#define KeyValuePair_2__ctor_m26500(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3873 *, String_t*, KeyValuePair_2_t1419 , const MethodInfo*))KeyValuePair_2__ctor_m25960_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_Key()
#define KeyValuePair_2_get_Key_m26501(__this, method) (( String_t* (*) (KeyValuePair_2_t3873 *, const MethodInfo*))KeyValuePair_2_get_Key_m25961_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m26502(__this, ___value, method) (( void (*) (KeyValuePair_2_t3873 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m25962_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_Value()
#define KeyValuePair_2_get_Value_m26503(__this, method) (( KeyValuePair_2_t1419  (*) (KeyValuePair_2_t3873 *, const MethodInfo*))KeyValuePair_2_get_Value_m25963_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m26504(__this, ___value, method) (( void (*) (KeyValuePair_2_t3873 *, KeyValuePair_2_t1419 , const MethodInfo*))KeyValuePair_2_set_Value_m25964_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::ToString()
#define KeyValuePair_2_ToString_m26505(__this, method) (( String_t* (*) (KeyValuePair_2_t3873 *, const MethodInfo*))KeyValuePair_2_ToString_m25965_gshared)(__this, method)
