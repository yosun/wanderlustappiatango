﻿#pragma once
#include <stdint.h>
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t790;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.IUserDefinedTargetEventHandler>
struct  Comparison_1_t3647  : public MulticastDelegate_t307
{
};
