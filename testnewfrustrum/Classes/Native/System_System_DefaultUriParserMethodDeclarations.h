﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.DefaultUriParser
struct DefaultUriParser_t1981;
// System.String
struct String_t;

// System.Void System.DefaultUriParser::.ctor()
extern "C" void DefaultUriParser__ctor_m9239 (DefaultUriParser_t1981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DefaultUriParser::.ctor(System.String)
extern "C" void DefaultUriParser__ctor_m9240 (DefaultUriParser_t1981 * __this, String_t* ___scheme, const MethodInfo* method) IL2CPP_METHOD_ATTR;
