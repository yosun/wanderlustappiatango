﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>
struct KeyValuePair_2_t3735;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_8MethodDeclarations.h"
#define KeyValuePair_2__ctor_m24519(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3735 *, String_t*, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m16576_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m24520(__this, method) (( String_t* (*) (KeyValuePair_2_t3735 *, const MethodInfo*))KeyValuePair_2_get_Key_m16577_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m24521(__this, ___value, method) (( void (*) (KeyValuePair_2_t3735 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m16578_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m24522(__this, method) (( int32_t (*) (KeyValuePair_2_t3735 *, const MethodInfo*))KeyValuePair_2_get_Value_m16579_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m24523(__this, ___value, method) (( void (*) (KeyValuePair_2_t3735 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m16580_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m24524(__this, method) (( String_t* (*) (KeyValuePair_2_t3735 *, const MethodInfo*))KeyValuePair_2_ToString_m16581_gshared)(__this, method)
