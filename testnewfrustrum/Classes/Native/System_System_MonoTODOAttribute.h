﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.MonoTODOAttribute
struct  MonoTODOAttribute_t1847  : public Attribute_t138
{
	// System.String System.MonoTODOAttribute::comment
	String_t* ___comment_0;
};
