﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyInformationalVersionAttribute_t1582_il2cpp_TypeInfo_var;
extern TypeInfo* SatelliteContractVersionAttribute_t1583_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCopyrightAttribute_t484_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t483_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t482_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDefaultAliasAttribute_t1584_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t480_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t486_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t487_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggableAttribute_t897_il2cpp_TypeInfo_var;
extern TypeInfo* CompilationRelaxationsAttribute_t898_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyKeyFileAttribute_t1585_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDelaySignAttribute_t1586_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var;
extern TypeInfo* NeutralResourcesLanguageAttribute_t1588_il2cpp_TypeInfo_var;
void g_System_Core_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		AssemblyInformationalVersionAttribute_t1582_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3098);
		SatelliteContractVersionAttribute_t1583_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3099);
		AssemblyCopyrightAttribute_t484_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(482);
		AssemblyProductAttribute_t483_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(481);
		AssemblyCompanyAttribute_t482_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(480);
		AssemblyDefaultAliasAttribute_t1584_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3100);
		AssemblyDescriptionAttribute_t480_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(478);
		AssemblyTitleAttribute_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(484);
		AssemblyFileVersionAttribute_t487_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(485);
		RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(81);
		DebuggableAttribute_t897_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1270);
		CompilationRelaxationsAttribute_t898_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1271);
		ComVisibleAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(483);
		AssemblyKeyFileAttribute_t1585_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3101);
		AssemblyDelaySignAttribute_t1586_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3102);
		CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3103);
		NeutralResourcesLanguageAttribute_t1588_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3104);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 18;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AssemblyInformationalVersionAttribute_t1582 * tmp;
		tmp = (AssemblyInformationalVersionAttribute_t1582 *)il2cpp_codegen_object_new (AssemblyInformationalVersionAttribute_t1582_il2cpp_TypeInfo_var);
		AssemblyInformationalVersionAttribute__ctor_m7264(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SatelliteContractVersionAttribute_t1583 * tmp;
		tmp = (SatelliteContractVersionAttribute_t1583 *)il2cpp_codegen_object_new (SatelliteContractVersionAttribute_t1583_il2cpp_TypeInfo_var);
		SatelliteContractVersionAttribute__ctor_m7265(tmp, il2cpp_codegen_string_new_wrapper("2.0.5.0"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t484 * tmp;
		tmp = (AssemblyCopyrightAttribute_t484 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t484_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m2432(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t483 * tmp;
		tmp = (AssemblyProductAttribute_t483 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t483_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m2431(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t482 * tmp;
		tmp = (AssemblyCompanyAttribute_t482 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t482_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m2430(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDefaultAliasAttribute_t1584 * tmp;
		tmp = (AssemblyDefaultAliasAttribute_t1584 *)il2cpp_codegen_object_new (AssemblyDefaultAliasAttribute_t1584_il2cpp_TypeInfo_var);
		AssemblyDefaultAliasAttribute__ctor_m7266(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t480 * tmp;
		tmp = (AssemblyDescriptionAttribute_t480 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t480_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m2428(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t486 * tmp;
		tmp = (AssemblyTitleAttribute_t486 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t486_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m2434(tmp, il2cpp_codegen_string_new_wrapper("System.Core.dll"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t487 * tmp;
		tmp = (AssemblyFileVersionAttribute_t487 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t487_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m2435(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t160 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t160 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t160_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m508(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m509(tmp, true, NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t897 * tmp;
		tmp = (DebuggableAttribute_t897 *)il2cpp_codegen_object_new (DebuggableAttribute_t897_il2cpp_TypeInfo_var);
		DebuggableAttribute__ctor_m4679(tmp, 2, NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t898 * tmp;
		tmp = (CompilationRelaxationsAttribute_t898 *)il2cpp_codegen_object_new (CompilationRelaxationsAttribute_t898_il2cpp_TypeInfo_var);
		CompilationRelaxationsAttribute__ctor_m7267(tmp, 8, NULL);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t485 * tmp;
		tmp = (ComVisibleAttribute_t485 *)il2cpp_codegen_object_new (ComVisibleAttribute_t485_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2433(tmp, false, NULL);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		AssemblyKeyFileAttribute_t1585 * tmp;
		tmp = (AssemblyKeyFileAttribute_t1585 *)il2cpp_codegen_object_new (AssemblyKeyFileAttribute_t1585_il2cpp_TypeInfo_var);
		AssemblyKeyFileAttribute__ctor_m7268(tmp, il2cpp_codegen_string_new_wrapper("../silverlight.pub"), NULL);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDelaySignAttribute_t1586 * tmp;
		tmp = (AssemblyDelaySignAttribute_t1586 *)il2cpp_codegen_object_new (AssemblyDelaySignAttribute_t1586_il2cpp_TypeInfo_var);
		AssemblyDelaySignAttribute__ctor_m7269(tmp, true, NULL);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1587 * tmp;
		tmp = (CLSCompliantAttribute_t1587 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1587_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7270(tmp, true, NULL);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		NeutralResourcesLanguageAttribute_t1588 * tmp;
		tmp = (NeutralResourcesLanguageAttribute_t1588 *)il2cpp_codegen_object_new (NeutralResourcesLanguageAttribute_t1588_il2cpp_TypeInfo_var);
		NeutralResourcesLanguageAttribute__ctor_m7271(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
}
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void ExtensionAttribute_t899_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 69, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var;
void MonoTODOAttribute_t1578_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2319);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1452 * tmp;
		tmp = (AttributeUsageAttribute_t1452 *)il2cpp_codegen_object_new (AttributeUsageAttribute_t1452_il2cpp_TypeInfo_var);
		AttributeUsageAttribute__ctor_m7108(tmp, 32767, NULL);
		AttributeUsageAttribute_set_AllowMultiple_m7110(tmp, true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttribute.h"
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttributeMethodDeclarations.h"
extern TypeInfo* MonoTODOAttribute_t1578_il2cpp_TypeInfo_var;
void HashSet_1_t1589_CustomAttributesCacheGenerator_HashSet_1_GetObjectData_m7292(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1578_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3105);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1578 * tmp;
		tmp = (MonoTODOAttribute_t1578 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1578_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m7258(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* MonoTODOAttribute_t1578_il2cpp_TypeInfo_var;
void HashSet_1_t1589_CustomAttributesCacheGenerator_HashSet_1_OnDeserialization_m7293(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoTODOAttribute_t1578_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3105);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		MonoTODOAttribute_t1578 * tmp;
		tmp = (MonoTODOAttribute_t1578 *)il2cpp_codegen_object_new (MonoTODOAttribute_t1578_il2cpp_TypeInfo_var);
		MonoTODOAttribute__ctor_m7258(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void Enumerable_t132_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_Any_m7305(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_Cast_m7306(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_CreateCastIterator_m7307(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_Contains_m7308(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_Contains_m7309(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_First_m7310(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_ToArray_m7311(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_ToList_m7312(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t899_il2cpp_TypeInfo_var;
void Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_Where_m7313(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t899_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1272);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t899 * tmp;
		tmp = (ExtensionAttribute_t899 *)il2cpp_codegen_object_new (ExtensionAttribute_t899_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4681(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_CreateWhereIterator_m7314(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7316(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7317(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7318(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7319(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7321(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7323(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7324(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7325(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7326(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var;
void U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7328(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t497 * tmp;
		tmp = (DebuggerHiddenAttribute_t497 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t497_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2479(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3E_t1581_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(83);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t162 * tmp;
		tmp = (CompilerGeneratedAttribute_t162 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t162_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m511(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_System_Core_Assembly_AttributeGenerators[30] = 
{
	NULL,
	g_System_Core_Assembly_CustomAttributesCacheGenerator,
	ExtensionAttribute_t899_CustomAttributesCacheGenerator,
	MonoTODOAttribute_t1578_CustomAttributesCacheGenerator,
	HashSet_1_t1589_CustomAttributesCacheGenerator_HashSet_1_GetObjectData_m7292,
	HashSet_1_t1589_CustomAttributesCacheGenerator_HashSet_1_OnDeserialization_m7293,
	Enumerable_t132_CustomAttributesCacheGenerator,
	Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_Any_m7305,
	Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_Cast_m7306,
	Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_CreateCastIterator_m7307,
	Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_Contains_m7308,
	Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_Contains_m7309,
	Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_First_m7310,
	Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_ToArray_m7311,
	Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_ToList_m7312,
	Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_Where_m7313,
	Enumerable_t132_CustomAttributesCacheGenerator_Enumerable_CreateWhereIterator_m7314,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_CustomAttributesCacheGenerator,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m7316,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m7317,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m7318,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m7319,
	U3CCreateCastIteratorU3Ec__Iterator0_1_t1593_CustomAttributesCacheGenerator_U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m7321,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_CustomAttributesCacheGenerator,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m7323,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m7324,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m7325,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m7326,
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1594_CustomAttributesCacheGenerator_U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m7328,
	U3CPrivateImplementationDetailsU3E_t1581_CustomAttributesCacheGenerator,
};
