﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.UintPlugin
struct UintPlugin_t927;
// DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1017;
// DG.Tweening.Tween
struct Tween_t934;
// DG.Tweening.Core.DOGetter`1<System.UInt32>
struct DOGetter_1_t1018;
// DG.Tweening.Core.DOSetter`1<System.UInt32>
struct DOSetter_1_t1019;
// DG.Tweening.Plugins.Options.NoOptions
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Plugins.UintPlugin::Reset(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void UintPlugin_Reset_m5263 (UintPlugin_t927 * __this, TweenerCore_3_t1017 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 DG.Tweening.Plugins.UintPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>,System.UInt32)
extern "C" uint32_t UintPlugin_ConvertToStartValue_m5264 (UintPlugin_t927 * __this, TweenerCore_3_t1017 * ___t, uint32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.UintPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void UintPlugin_SetRelativeEndValue_m5265 (UintPlugin_t927 * __this, TweenerCore_3_t1017 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.UintPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.NoOptions>)
extern "C" void UintPlugin_SetChangeValue_m5266 (UintPlugin_t927 * __this, TweenerCore_3_t1017 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Plugins.UintPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.NoOptions,System.Single,System.UInt32)
extern "C" float UintPlugin_GetSpeedBasedDuration_m5267 (UintPlugin_t927 * __this, NoOptions_t933  ___options, float ___unitsXSecond, uint32_t ___changeValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.UintPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.NoOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<System.UInt32>,DG.Tweening.Core.DOSetter`1<System.UInt32>,System.Single,System.UInt32,System.UInt32,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void UintPlugin_EvaluateAndApply_m5268 (UintPlugin_t927 * __this, NoOptions_t933  ___options, Tween_t934 * ___t, bool ___isRelative, DOGetter_1_t1018 * ___getter, DOSetter_1_t1019 * ___setter, float ___elapsed, uint32_t ___startValue, uint32_t ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.UintPlugin::.ctor()
extern "C" void UintPlugin__ctor_m5269 (UintPlugin_t927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
