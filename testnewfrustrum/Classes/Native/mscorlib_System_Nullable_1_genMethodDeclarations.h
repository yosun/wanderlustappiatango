﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Nullable`1<System.Boolean>
struct Nullable_1_t117;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Nullable`1<System.Boolean>
#include "mscorlib_System_Nullable_1_gen.h"

// System.Void System.Nullable`1<System.Boolean>::.ctor(T)
// System.Nullable`1<System.Byte>
#include "mscorlib_System_Nullable_1_gen_2MethodDeclarations.h"
#define Nullable_1__ctor_m357(__this, ___value, method) (( void (*) (Nullable_1_t117 *, bool, const MethodInfo*))Nullable_1__ctor_m14897_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
#define Nullable_1_get_HasValue_m5555(__this, method) (( bool (*) (Nullable_1_t117 *, const MethodInfo*))Nullable_1_get_HasValue_m14898_gshared)(__this, method)
// T System.Nullable`1<System.Boolean>::get_Value()
#define Nullable_1_get_Value_m5556(__this, method) (( bool (*) (Nullable_1_t117 *, const MethodInfo*))Nullable_1_get_Value_m14899_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Object)
#define Nullable_1_Equals_m14900(__this, ___other, method) (( bool (*) (Nullable_1_t117 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m14901_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m14902(__this, ___other, method) (( bool (*) (Nullable_1_t117 *, Nullable_1_t117 , const MethodInfo*))Nullable_1_Equals_m14903_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.Boolean>::GetHashCode()
#define Nullable_1_GetHashCode_m14904(__this, method) (( int32_t (*) (Nullable_1_t117 *, const MethodInfo*))Nullable_1_GetHashCode_m14905_gshared)(__this, method)
// System.String System.Nullable`1<System.Boolean>::ToString()
#define Nullable_1_ToString_m14906(__this, method) (( String_t* (*) (Nullable_1_t117 *, const MethodInfo*))Nullable_1_ToString_m14907_gshared)(__this, method)
