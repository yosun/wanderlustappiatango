﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CacheIndex
struct CacheIndex_t1205;
struct CacheIndex_t1205_marshaled;

void CacheIndex_t1205_marshal(const CacheIndex_t1205& unmarshaled, CacheIndex_t1205_marshaled& marshaled);
void CacheIndex_t1205_marshal_back(const CacheIndex_t1205_marshaled& marshaled, CacheIndex_t1205& unmarshaled);
void CacheIndex_t1205_marshal_cleanup(CacheIndex_t1205_marshaled& marshaled);
