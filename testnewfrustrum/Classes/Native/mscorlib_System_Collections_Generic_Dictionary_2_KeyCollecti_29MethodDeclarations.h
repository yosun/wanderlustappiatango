﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Enumerator_t3553;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Dictionary_2_t869;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21871_gshared (Enumerator_t3553 * __this, Dictionary_2_t869 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m21871(__this, ___host, method) (( void (*) (Enumerator_t3553 *, Dictionary_2_t869 *, const MethodInfo*))Enumerator__ctor_m21871_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m21872_gshared (Enumerator_t3553 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m21872(__this, method) (( Object_t * (*) (Enumerator_t3553 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m21872_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Dispose()
extern "C" void Enumerator_Dispose_m21873_gshared (Enumerator_t3553 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m21873(__this, method) (( void (*) (Enumerator_t3553 *, const MethodInfo*))Enumerator_Dispose_m21873_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m21874_gshared (Enumerator_t3553 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m21874(__this, method) (( bool (*) (Enumerator_t3553 *, const MethodInfo*))Enumerator_MoveNext_m21874_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Current()
extern "C" int32_t Enumerator_get_Current_m21875_gshared (Enumerator_t3553 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m21875(__this, method) (( int32_t (*) (Enumerator_t3553 *, const MethodInfo*))Enumerator_get_Current_m21875_gshared)(__this, method)
