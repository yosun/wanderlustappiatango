﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.EyewearComponentFactory/NullEyewearComponentFactory
struct NullEyewearComponentFactory_t557;
// System.Type
struct Type_t;

// System.Type Vuforia.EyewearComponentFactory/NullEyewearComponentFactory::GetOVRInitControllerType()
extern "C" Type_t * NullEyewearComponentFactory_GetOVRInitControllerType_m2614 (NullEyewearComponentFactory_t557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.EyewearComponentFactory/NullEyewearComponentFactory::.ctor()
extern "C" void NullEyewearComponentFactory__ctor_m2615 (NullEyewearComponentFactory_t557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
