﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>
struct KeyValuePair_2_t841;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t93;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m20596(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t841 *, int32_t, WordAbstractBehaviour_t93 *, const MethodInfo*))KeyValuePair_2__ctor_m16229_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m4488(__this, method) (( int32_t (*) (KeyValuePair_2_t841 *, const MethodInfo*))KeyValuePair_2_get_Key_m16230_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m20597(__this, ___value, method) (( void (*) (KeyValuePair_2_t841 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16231_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m4489(__this, method) (( WordAbstractBehaviour_t93 * (*) (KeyValuePair_2_t841 *, const MethodInfo*))KeyValuePair_2_get_Value_m16232_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m20598(__this, ___value, method) (( void (*) (KeyValuePair_2_t841 *, WordAbstractBehaviour_t93 *, const MethodInfo*))KeyValuePair_2_set_Value_m16233_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::ToString()
#define KeyValuePair_2_ToString_m20599(__this, method) (( String_t* (*) (KeyValuePair_2_t841 *, const MethodInfo*))KeyValuePair_2_ToString_m16234_gshared)(__this, method)
