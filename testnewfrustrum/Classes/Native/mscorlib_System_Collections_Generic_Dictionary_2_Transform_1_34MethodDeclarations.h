﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>
struct Transform_1_t3549;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m21915_gshared (Transform_1_t3549 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m21915(__this, ___object, ___method, method) (( void (*) (Transform_1_t3549 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m21915_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
extern "C" DictionaryEntry_t1996  Transform_1_Invoke_m21916_gshared (Transform_1_t3549 * __this, int32_t ___key, TrackableResultData_t642  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m21916(__this, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Transform_1_t3549 *, int32_t, TrackableResultData_t642 , const MethodInfo*))Transform_1_Invoke_m21916_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m21917_gshared (Transform_1_t3549 * __this, int32_t ___key, TrackableResultData_t642  ___value, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m21917(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3549 *, int32_t, TrackableResultData_t642 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m21917_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
extern "C" DictionaryEntry_t1996  Transform_1_EndInvoke_m21918_gshared (Transform_1_t3549 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m21918(__this, ___result, method) (( DictionaryEntry_t1996  (*) (Transform_1_t3549 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m21918_gshared)(__this, ___result, method)
