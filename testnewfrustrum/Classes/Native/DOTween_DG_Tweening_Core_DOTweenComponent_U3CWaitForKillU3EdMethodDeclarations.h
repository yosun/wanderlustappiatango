﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4
struct U3CWaitForKillU3Ed__4_t938;
// System.Object
struct Object_t;

// System.Boolean DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::MoveNext()
extern "C" bool U3CWaitForKillU3Ed__4_MoveNext_m5287 (U3CWaitForKillU3Ed__4_t938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern "C" Object_t * U3CWaitForKillU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288 (U3CWaitForKillU3Ed__4_t938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::System.IDisposable.Dispose()
extern "C" void U3CWaitForKillU3Ed__4_System_IDisposable_Dispose_m5289 (U3CWaitForKillU3Ed__4_t938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CWaitForKillU3Ed__4_System_Collections_IEnumerator_get_Current_m5290 (U3CWaitForKillU3Ed__4_t938 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.DOTweenComponent/<WaitForKill>d__4::.ctor(System.Int32)
extern "C" void U3CWaitForKillU3Ed__4__ctor_m5291 (U3CWaitForKillU3Ed__4_t938 * __this, int32_t ___U3CU3E1__state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
