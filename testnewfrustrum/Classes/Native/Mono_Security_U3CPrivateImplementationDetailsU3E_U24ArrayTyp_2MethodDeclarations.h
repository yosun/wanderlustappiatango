﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$32
struct U24ArrayTypeU2432_t1793;
struct U24ArrayTypeU2432_t1793_marshaled;

void U24ArrayTypeU2432_t1793_marshal(const U24ArrayTypeU2432_t1793& unmarshaled, U24ArrayTypeU2432_t1793_marshaled& marshaled);
void U24ArrayTypeU2432_t1793_marshal_back(const U24ArrayTypeU2432_t1793_marshaled& marshaled, U24ArrayTypeU2432_t1793& unmarshaled);
void U24ArrayTypeU2432_t1793_marshal_cleanup(U24ArrayTypeU2432_t1793_marshaled& marshaled);
