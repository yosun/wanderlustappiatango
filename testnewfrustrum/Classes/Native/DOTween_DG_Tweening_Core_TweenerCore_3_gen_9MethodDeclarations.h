﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
struct TweenerCore_3_t1043;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_16MethodDeclarations.h"
#define TweenerCore_3__ctor_m23708(__this, method) (( void (*) (TweenerCore_3_t1043 *, const MethodInfo*))TweenerCore_3__ctor_m23702_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::Reset()
#define TweenerCore_3_Reset_m23709(__this, method) (( void (*) (TweenerCore_3_t1043 *, const MethodInfo*))TweenerCore_3_Reset_m23703_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::Validate()
#define TweenerCore_3_Validate_m23710(__this, method) (( bool (*) (TweenerCore_3_t1043 *, const MethodInfo*))TweenerCore_3_Validate_m23704_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::UpdateDelay(System.Single)
#define TweenerCore_3_UpdateDelay_m23711(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1043 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m23705_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::Startup()
#define TweenerCore_3_Startup_m23712(__this, method) (( bool (*) (TweenerCore_3_t1043 *, const MethodInfo*))TweenerCore_3_Startup_m23706_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
#define TweenerCore_3_ApplyTween_m23713(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1043 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m23707_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
