﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t197;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct  EventFunction_1_t422  : public MulticastDelegate_t307
{
};
