﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TextRecoBehaviour
struct TextRecoBehaviour_t74;

// System.Void Vuforia.TextRecoBehaviour::.ctor()
extern "C" void TextRecoBehaviour__ctor_m215 (TextRecoBehaviour_t74 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
