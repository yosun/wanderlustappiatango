﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.SearchPattern
struct SearchPattern_t2202;

// System.Void System.IO.SearchPattern::.cctor()
extern "C" void SearchPattern__cctor_m11425 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
