﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1025;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen_15MethodDeclarations.h"
#define TweenerCore_3__ctor_m23618(__this, method) (( void (*) (TweenerCore_3_t1025 *, const MethodInfo*))TweenerCore_3__ctor_m23612_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>::Reset()
#define TweenerCore_3_Reset_m23619(__this, method) (( void (*) (TweenerCore_3_t1025 *, const MethodInfo*))TweenerCore_3_Reset_m23613_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>::Validate()
#define TweenerCore_3_Validate_m23620(__this, method) (( bool (*) (TweenerCore_3_t1025 *, const MethodInfo*))TweenerCore_3_Validate_m23614_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
#define TweenerCore_3_UpdateDelay_m23621(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1025 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m23615_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>::Startup()
#define TweenerCore_3_Startup_m23622(__this, method) (( bool (*) (TweenerCore_3_t1025 *, const MethodInfo*))TweenerCore_3_Startup_m23616_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
#define TweenerCore_3_ApplyTween_m23623(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1025 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m23617_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
