﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t1049;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C" void TweenerCore_3__ctor_m23967_gshared (TweenerCore_3_t1049 * __this, const MethodInfo* method);
#define TweenerCore_3__ctor_m23967(__this, method) (( void (*) (TweenerCore_3_t1049 *, const MethodInfo*))TweenerCore_3__ctor_m23967_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset()
extern "C" void TweenerCore_3_Reset_m23968_gshared (TweenerCore_3_t1049 * __this, const MethodInfo* method);
#define TweenerCore_3_Reset_m23968(__this, method) (( void (*) (TweenerCore_3_t1049 *, const MethodInfo*))TweenerCore_3_Reset_m23968_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Validate()
extern "C" bool TweenerCore_3_Validate_m23969_gshared (TweenerCore_3_t1049 * __this, const MethodInfo* method);
#define TweenerCore_3_Validate_m23969(__this, method) (( bool (*) (TweenerCore_3_t1049 *, const MethodInfo*))TweenerCore_3_Validate_m23969_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23970_gshared (TweenerCore_3_t1049 * __this, float ___elapsed, const MethodInfo* method);
#define TweenerCore_3_UpdateDelay_m23970(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1049 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m23970_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23971_gshared (TweenerCore_3_t1049 * __this, const MethodInfo* method);
#define TweenerCore_3_Startup_m23971(__this, method) (( bool (*) (TweenerCore_3_t1049 *, const MethodInfo*))TweenerCore_3_Startup_m23971_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool TweenerCore_3_ApplyTween_m23972_gshared (TweenerCore_3_t1049 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method);
#define TweenerCore_3_ApplyTween_m23972(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1049 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m23972_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
