﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.ShortcutExtensions
struct ShortcutExtensions_t964;
// DG.Tweening.Tweener
struct Tweener_t99;
// UnityEngine.Transform
struct Transform_t11;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// DG.Tweening.RotateMode
#include "DOTween_DG_Tweening_RotateMode.h"

// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C" Tweener_t99 * ShortcutExtensions_DOMove_m360 (Object_t * __this /* static, unused */, Transform_t11 * ___target, Vector3_t15  ___endValue, float ___duration, bool ___snapping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveX(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern "C" Tweener_t99 * ShortcutExtensions_DOMoveX_m371 (Object_t * __this /* static, unused */, Transform_t11 * ___target, float ___endValue, float ___duration, bool ___snapping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveY(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern "C" Tweener_t99 * ShortcutExtensions_DOMoveY_m366 (Object_t * __this /* static, unused */, Transform_t11 * ___target, float ___endValue, float ___duration, bool ___snapping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern "C" Tweener_t99 * ShortcutExtensions_DORotate_m250 (Object_t * __this /* static, unused */, Transform_t11 * ___target, Vector3_t15  ___endValue, float ___duration, int32_t ___mode, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScaleY(UnityEngine.Transform,System.Single,System.Single)
extern "C" Tweener_t99 * ShortcutExtensions_DOScaleY_m369 (Object_t * __this /* static, unused */, Transform_t11 * ___target, float ___endValue, float ___duration, const MethodInfo* method) IL2CPP_METHOD_ATTR;
