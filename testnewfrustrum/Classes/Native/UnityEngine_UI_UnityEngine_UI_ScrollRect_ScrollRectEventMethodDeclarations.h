﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ScrollRect/ScrollRectEvent
struct ScrollRectEvent_t331;

// System.Void UnityEngine.UI.ScrollRect/ScrollRectEvent::.ctor()
extern "C" void ScrollRectEvent__ctor_m1466 (ScrollRectEvent_t331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
