﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>
struct Enumerator_t3738;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1187;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_33.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6MethodDeclarations.h"
#define Enumerator__ctor_m24553(__this, ___dictionary, method) (( void (*) (Enumerator_t3738 *, Dictionary_2_t1187 *, const MethodInfo*))Enumerator__ctor_m16601_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m24554(__this, method) (( Object_t * (*) (Enumerator_t3738 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16602_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m24555(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3738 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16603_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m24556(__this, method) (( Object_t * (*) (Enumerator_t3738 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16604_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m24557(__this, method) (( Object_t * (*) (Enumerator_t3738 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16605_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m24558(__this, method) (( bool (*) (Enumerator_t3738 *, const MethodInfo*))Enumerator_MoveNext_m16606_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_Current()
#define Enumerator_get_Current_m24559(__this, method) (( KeyValuePair_2_t3735  (*) (Enumerator_t3738 *, const MethodInfo*))Enumerator_get_Current_m16607_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m24560(__this, method) (( String_t* (*) (Enumerator_t3738 *, const MethodInfo*))Enumerator_get_CurrentKey_m16608_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m24561(__this, method) (( int32_t (*) (Enumerator_t3738 *, const MethodInfo*))Enumerator_get_CurrentValue_m16609_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m24562(__this, method) (( void (*) (Enumerator_t3738 *, const MethodInfo*))Enumerator_VerifyState_m16610_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m24563(__this, method) (( void (*) (Enumerator_t3738 *, const MethodInfo*))Enumerator_VerifyCurrent_m16611_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::Dispose()
#define Enumerator_Dispose_m24564(__this, method) (( void (*) (Enumerator_t3738 *, const MethodInfo*))Enumerator_Dispose_m16612_gshared)(__this, method)
