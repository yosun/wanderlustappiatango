﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// Metadata Definition UnityEngine.Events.UnityAction`2
extern TypeInfo UnityAction_2_t1466_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_2_t1466_Il2CppGenericContainer;
extern TypeInfo UnityAction_2_t1466_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_2_t1466_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_2_t1466_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityAction_2_t1466_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_2_t1466_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_2_t1466_Il2CppGenericContainer, NULL, "T1", 1, 0 };
static const Il2CppGenericParameter* UnityAction_2_t1466_Il2CppGenericParametersArray[2] = 
{
	&UnityAction_2_t1466_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_2_t1466_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_2_t1466_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_2_t1466_il2cpp_TypeInfo, 2, 0, UnityAction_2_t1466_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_2_t1466_UnityAction_2__ctor_m7158_ParameterInfos[] = 
{
	{"object", 0, 134219720, 0, &Object_t_0_0_0},
	{"method", 1, 134219721, 0, &IntPtr_t_0_0_0},
};
extern const Il2CppType Void_t168_0_0_0;
// System.Void UnityEngine.Events.UnityAction`2::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_2__ctor_m7158_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_2_t1466_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1466_UnityAction_2__ctor_m7158_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_2_t1466_gp_0_0_0_0;
extern const Il2CppType UnityAction_2_t1466_gp_0_0_0_0;
extern const Il2CppType UnityAction_2_t1466_gp_1_0_0_0;
extern const Il2CppType UnityAction_2_t1466_gp_1_0_0_0;
static const ParameterInfo UnityAction_2_t1466_UnityAction_2_Invoke_m7159_ParameterInfos[] = 
{
	{"arg0", 0, 134219722, 0, &UnityAction_2_t1466_gp_0_0_0_0},
	{"arg1", 1, 134219723, 0, &UnityAction_2_t1466_gp_1_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`2::Invoke(T0,T1)
extern const MethodInfo UnityAction_2_Invoke_m7159_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1466_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1466_UnityAction_2_Invoke_m7159_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_2_t1466_gp_0_0_0_0;
extern const Il2CppType UnityAction_2_t1466_gp_1_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_2_t1466_UnityAction_2_BeginInvoke_m7160_ParameterInfos[] = 
{
	{"arg0", 0, 134219724, 0, &UnityAction_2_t1466_gp_0_0_0_0},
	{"arg1", 1, 134219725, 0, &UnityAction_2_t1466_gp_1_0_0_0},
	{"callback", 2, 134219726, 0, &AsyncCallback_t305_0_0_0},
	{"object", 3, 134219727, 0, &Object_t_0_0_0},
};
extern const Il2CppType IAsyncResult_t304_0_0_0;
// System.IAsyncResult UnityEngine.Events.UnityAction`2::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_2_BeginInvoke_m7160_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1466_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1466_UnityAction_2_BeginInvoke_m7160_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo UnityAction_2_t1466_UnityAction_2_EndInvoke_m7161_ParameterInfos[] = 
{
	{"result", 0, 134219728, 0, &IAsyncResult_t304_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`2::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_2_EndInvoke_m7161_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1466_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1466_UnityAction_2_EndInvoke_m7161_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_2_t1466_MethodInfos[] =
{
	&UnityAction_2__ctor_m7158_MethodInfo,
	&UnityAction_2_Invoke_m7159_MethodInfo,
	&UnityAction_2_BeginInvoke_m7160_MethodInfo,
	&UnityAction_2_EndInvoke_m7161_MethodInfo,
	NULL
};
extern const MethodInfo MulticastDelegate_Equals_m2554_MethodInfo;
extern const MethodInfo Object_Finalize_m515_MethodInfo;
extern const MethodInfo MulticastDelegate_GetHashCode_m2555_MethodInfo;
extern const MethodInfo Object_ToString_m542_MethodInfo;
extern const MethodInfo MulticastDelegate_GetObjectData_m2556_MethodInfo;
extern const MethodInfo Delegate_Clone_m2557_MethodInfo;
extern const MethodInfo MulticastDelegate_GetInvocationList_m2558_MethodInfo;
extern const MethodInfo MulticastDelegate_CombineImpl_m2559_MethodInfo;
extern const MethodInfo MulticastDelegate_RemoveImpl_m2560_MethodInfo;
extern const MethodInfo UnityAction_2_Invoke_m7159_MethodInfo;
extern const MethodInfo UnityAction_2_BeginInvoke_m7160_MethodInfo;
extern const MethodInfo UnityAction_2_EndInvoke_m7161_MethodInfo;
static const Il2CppMethodReference UnityAction_2_t1466_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&UnityAction_2_Invoke_m7159_MethodInfo,
	&UnityAction_2_BeginInvoke_m7160_MethodInfo,
	&UnityAction_2_EndInvoke_m7161_MethodInfo,
};
static bool UnityAction_2_t1466_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ICloneable_t512_0_0_0;
extern const Il2CppType ISerializable_t513_0_0_0;
static Il2CppInterfaceOffsetPair UnityAction_2_t1466_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_2_t1466_0_0_0;
extern const Il2CppType UnityAction_2_t1466_1_0_0;
extern const Il2CppType MulticastDelegate_t307_0_0_0;
struct UnityAction_2_t1466;
const Il2CppTypeDefinitionMetadata UnityAction_2_t1466_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_2_t1466_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, UnityAction_2_t1466_VTable/* vtableMethods */
	, UnityAction_2_t1466_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_2_t1466_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_2_t1466_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_2_t1466_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_2_t1466_0_0_0/* byval_arg */
	, &UnityAction_2_t1466_1_0_0/* this_arg */
	, &UnityAction_2_t1466_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_2_t1466_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`3
extern TypeInfo UnityAction_3_t1467_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_3_t1467_Il2CppGenericContainer;
extern TypeInfo UnityAction_3_t1467_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_3_t1467_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_3_t1467_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityAction_3_t1467_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_3_t1467_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_3_t1467_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityAction_3_t1467_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_3_t1467_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_3_t1467_Il2CppGenericContainer, NULL, "T2", 2, 0 };
static const Il2CppGenericParameter* UnityAction_3_t1467_Il2CppGenericParametersArray[3] = 
{
	&UnityAction_3_t1467_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_3_t1467_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_3_t1467_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_3_t1467_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_3_t1467_il2cpp_TypeInfo, 3, 0, UnityAction_3_t1467_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_3_t1467_UnityAction_3__ctor_m7162_ParameterInfos[] = 
{
	{"object", 0, 134219729, 0, &Object_t_0_0_0},
	{"method", 1, 134219730, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`3::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_3__ctor_m7162_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_3_t1467_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1467_UnityAction_3__ctor_m7162_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_3_t1467_gp_0_0_0_0;
extern const Il2CppType UnityAction_3_t1467_gp_0_0_0_0;
extern const Il2CppType UnityAction_3_t1467_gp_1_0_0_0;
extern const Il2CppType UnityAction_3_t1467_gp_1_0_0_0;
extern const Il2CppType UnityAction_3_t1467_gp_2_0_0_0;
extern const Il2CppType UnityAction_3_t1467_gp_2_0_0_0;
static const ParameterInfo UnityAction_3_t1467_UnityAction_3_Invoke_m7163_ParameterInfos[] = 
{
	{"arg0", 0, 134219731, 0, &UnityAction_3_t1467_gp_0_0_0_0},
	{"arg1", 1, 134219732, 0, &UnityAction_3_t1467_gp_1_0_0_0},
	{"arg2", 2, 134219733, 0, &UnityAction_3_t1467_gp_2_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`3::Invoke(T0,T1,T2)
extern const MethodInfo UnityAction_3_Invoke_m7163_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1467_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1467_UnityAction_3_Invoke_m7163_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_3_t1467_gp_0_0_0_0;
extern const Il2CppType UnityAction_3_t1467_gp_1_0_0_0;
extern const Il2CppType UnityAction_3_t1467_gp_2_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_3_t1467_UnityAction_3_BeginInvoke_m7164_ParameterInfos[] = 
{
	{"arg0", 0, 134219734, 0, &UnityAction_3_t1467_gp_0_0_0_0},
	{"arg1", 1, 134219735, 0, &UnityAction_3_t1467_gp_1_0_0_0},
	{"arg2", 2, 134219736, 0, &UnityAction_3_t1467_gp_2_0_0_0},
	{"callback", 3, 134219737, 0, &AsyncCallback_t305_0_0_0},
	{"object", 4, 134219738, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Events.UnityAction`3::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_3_BeginInvoke_m7164_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1467_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1467_UnityAction_3_BeginInvoke_m7164_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo UnityAction_3_t1467_UnityAction_3_EndInvoke_m7165_ParameterInfos[] = 
{
	{"result", 0, 134219739, 0, &IAsyncResult_t304_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`3::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_3_EndInvoke_m7165_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1467_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1467_UnityAction_3_EndInvoke_m7165_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_3_t1467_MethodInfos[] =
{
	&UnityAction_3__ctor_m7162_MethodInfo,
	&UnityAction_3_Invoke_m7163_MethodInfo,
	&UnityAction_3_BeginInvoke_m7164_MethodInfo,
	&UnityAction_3_EndInvoke_m7165_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_3_Invoke_m7163_MethodInfo;
extern const MethodInfo UnityAction_3_BeginInvoke_m7164_MethodInfo;
extern const MethodInfo UnityAction_3_EndInvoke_m7165_MethodInfo;
static const Il2CppMethodReference UnityAction_3_t1467_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&UnityAction_3_Invoke_m7163_MethodInfo,
	&UnityAction_3_BeginInvoke_m7164_MethodInfo,
	&UnityAction_3_EndInvoke_m7165_MethodInfo,
};
static bool UnityAction_3_t1467_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_3_t1467_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_3_t1467_0_0_0;
extern const Il2CppType UnityAction_3_t1467_1_0_0;
struct UnityAction_3_t1467;
const Il2CppTypeDefinitionMetadata UnityAction_3_t1467_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_3_t1467_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, UnityAction_3_t1467_VTable/* vtableMethods */
	, UnityAction_3_t1467_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_3_t1467_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_3_t1467_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_3_t1467_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_3_t1467_0_0_0/* byval_arg */
	, &UnityAction_3_t1467_1_0_0/* this_arg */
	, &UnityAction_3_t1467_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_3_t1467_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Metadata Definition UnityEngine.Events.UnityAction`4
extern TypeInfo UnityAction_4_t1468_il2cpp_TypeInfo;
extern const Il2CppGenericContainer UnityAction_4_t1468_Il2CppGenericContainer;
extern TypeInfo UnityAction_4_t1468_gp_T0_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t1468_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t1468_Il2CppGenericContainer, NULL, "T0", 0, 0 };
extern TypeInfo UnityAction_4_t1468_gp_T1_1_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t1468_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t1468_Il2CppGenericContainer, NULL, "T1", 1, 0 };
extern TypeInfo UnityAction_4_t1468_gp_T2_2_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t1468_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t1468_Il2CppGenericContainer, NULL, "T2", 2, 0 };
extern TypeInfo UnityAction_4_t1468_gp_T3_3_il2cpp_TypeInfo;
extern const Il2CppGenericParameter UnityAction_4_t1468_gp_T3_3_il2cpp_TypeInfo_GenericParamFull = { &UnityAction_4_t1468_Il2CppGenericContainer, NULL, "T3", 3, 0 };
static const Il2CppGenericParameter* UnityAction_4_t1468_Il2CppGenericParametersArray[4] = 
{
	&UnityAction_4_t1468_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1468_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1468_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1468_gp_T3_3_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer UnityAction_4_t1468_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&UnityAction_4_t1468_il2cpp_TypeInfo, 4, 0, UnityAction_4_t1468_Il2CppGenericParametersArray };
extern const Il2CppType Object_t_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
static const ParameterInfo UnityAction_4_t1468_UnityAction_4__ctor_m7166_ParameterInfos[] = 
{
	{"object", 0, 134219740, 0, &Object_t_0_0_0},
	{"method", 1, 134219741, 0, &IntPtr_t_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`4::.ctor(System.Object,System.IntPtr)
extern const MethodInfo UnityAction_4__ctor_m7166_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_4_t1468_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1468_UnityAction_4__ctor_m7166_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_4_t1468_gp_0_0_0_0;
extern const Il2CppType UnityAction_4_t1468_gp_0_0_0_0;
extern const Il2CppType UnityAction_4_t1468_gp_1_0_0_0;
extern const Il2CppType UnityAction_4_t1468_gp_1_0_0_0;
extern const Il2CppType UnityAction_4_t1468_gp_2_0_0_0;
extern const Il2CppType UnityAction_4_t1468_gp_2_0_0_0;
extern const Il2CppType UnityAction_4_t1468_gp_3_0_0_0;
extern const Il2CppType UnityAction_4_t1468_gp_3_0_0_0;
static const ParameterInfo UnityAction_4_t1468_UnityAction_4_Invoke_m7167_ParameterInfos[] = 
{
	{"arg0", 0, 134219742, 0, &UnityAction_4_t1468_gp_0_0_0_0},
	{"arg1", 1, 134219743, 0, &UnityAction_4_t1468_gp_1_0_0_0},
	{"arg2", 2, 134219744, 0, &UnityAction_4_t1468_gp_2_0_0_0},
	{"arg3", 3, 134219745, 0, &UnityAction_4_t1468_gp_3_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`4::Invoke(T0,T1,T2,T3)
extern const MethodInfo UnityAction_4_Invoke_m7167_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1468_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1468_UnityAction_4_Invoke_m7167_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType UnityAction_4_t1468_gp_0_0_0_0;
extern const Il2CppType UnityAction_4_t1468_gp_1_0_0_0;
extern const Il2CppType UnityAction_4_t1468_gp_2_0_0_0;
extern const Il2CppType UnityAction_4_t1468_gp_3_0_0_0;
extern const Il2CppType AsyncCallback_t305_0_0_0;
extern const Il2CppType Object_t_0_0_0;
static const ParameterInfo UnityAction_4_t1468_UnityAction_4_BeginInvoke_m7168_ParameterInfos[] = 
{
	{"arg0", 0, 134219746, 0, &UnityAction_4_t1468_gp_0_0_0_0},
	{"arg1", 1, 134219747, 0, &UnityAction_4_t1468_gp_1_0_0_0},
	{"arg2", 2, 134219748, 0, &UnityAction_4_t1468_gp_2_0_0_0},
	{"arg3", 3, 134219749, 0, &UnityAction_4_t1468_gp_3_0_0_0},
	{"callback", 4, 134219750, 0, &AsyncCallback_t305_0_0_0},
	{"object", 5, 134219751, 0, &Object_t_0_0_0},
};
// System.IAsyncResult UnityEngine.Events.UnityAction`4::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
extern const MethodInfo UnityAction_4_BeginInvoke_m7168_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1468_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t304_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1468_UnityAction_4_BeginInvoke_m7168_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IAsyncResult_t304_0_0_0;
static const ParameterInfo UnityAction_4_t1468_UnityAction_4_EndInvoke_m7169_ParameterInfos[] = 
{
	{"result", 0, 134219752, 0, &IAsyncResult_t304_0_0_0},
};
// System.Void UnityEngine.Events.UnityAction`4::EndInvoke(System.IAsyncResult)
extern const MethodInfo UnityAction_4_EndInvoke_m7169_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1468_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1468_UnityAction_4_EndInvoke_m7169_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* UnityAction_4_t1468_MethodInfos[] =
{
	&UnityAction_4__ctor_m7166_MethodInfo,
	&UnityAction_4_Invoke_m7167_MethodInfo,
	&UnityAction_4_BeginInvoke_m7168_MethodInfo,
	&UnityAction_4_EndInvoke_m7169_MethodInfo,
	NULL
};
extern const MethodInfo UnityAction_4_Invoke_m7167_MethodInfo;
extern const MethodInfo UnityAction_4_BeginInvoke_m7168_MethodInfo;
extern const MethodInfo UnityAction_4_EndInvoke_m7169_MethodInfo;
static const Il2CppMethodReference UnityAction_4_t1468_VTable[] =
{
	&MulticastDelegate_Equals_m2554_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&MulticastDelegate_GetHashCode_m2555_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&Delegate_Clone_m2557_MethodInfo,
	&MulticastDelegate_GetObjectData_m2556_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2558_MethodInfo,
	&MulticastDelegate_CombineImpl_m2559_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2560_MethodInfo,
	&UnityAction_4_Invoke_m7167_MethodInfo,
	&UnityAction_4_BeginInvoke_m7168_MethodInfo,
	&UnityAction_4_EndInvoke_m7169_MethodInfo,
};
static bool UnityAction_4_t1468_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair UnityAction_4_t1468_InterfacesOffsets[] = 
{
	{ &ICloneable_t512_0_0_0, 4},
	{ &ISerializable_t513_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern const Il2CppType UnityAction_4_t1468_0_0_0;
extern const Il2CppType UnityAction_4_t1468_1_0_0;
struct UnityAction_4_t1468;
const Il2CppTypeDefinitionMetadata UnityAction_4_t1468_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, UnityAction_4_t1468_InterfacesOffsets/* interfaceOffsets */
	, &MulticastDelegate_t307_0_0_0/* parent */
	, UnityAction_4_t1468_VTable/* vtableMethods */
	, UnityAction_4_t1468_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo UnityAction_4_t1468_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_4_t1468_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &UnityAction_4_t1468_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &UnityAction_4_t1468_0_0_0/* byval_arg */
	, &UnityAction_4_t1468_1_0_0/* this_arg */
	, &UnityAction_4_t1468_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &UnityAction_4_t1468_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
