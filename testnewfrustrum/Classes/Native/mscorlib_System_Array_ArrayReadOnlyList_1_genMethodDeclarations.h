﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t3970;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t528;
// System.Exception
struct Exception_t140;

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m27417_gshared (ArrayReadOnlyList_1_t3970 * __this, ObjectU5BU5D_t115* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m27417(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t3970 *, ObjectU5BU5D_t115*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m27417_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m27418_gshared (ArrayReadOnlyList_1_t3970 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m27418(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t3970 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m27418_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ArrayReadOnlyList_1_get_Item_m27419_gshared (ArrayReadOnlyList_1_t3970 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m27419(__this, ___index, method) (( Object_t * (*) (ArrayReadOnlyList_1_t3970 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m27419_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m27420_gshared (ArrayReadOnlyList_1_t3970 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m27420(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t3970 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_set_Item_m27420_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m27421_gshared (ArrayReadOnlyList_1_t3970 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m27421(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t3970 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m27421_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m27422_gshared (ArrayReadOnlyList_1_t3970 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m27422(__this, method) (( bool (*) (ArrayReadOnlyList_1_t3970 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m27422_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m27423_gshared (ArrayReadOnlyList_1_t3970 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m27423(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3970 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Add_m27423_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m27424_gshared (ArrayReadOnlyList_1_t3970 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m27424(__this, method) (( void (*) (ArrayReadOnlyList_1_t3970 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m27424_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m27425_gshared (ArrayReadOnlyList_1_t3970 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m27425(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3970 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Contains_m27425_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m27426_gshared (ArrayReadOnlyList_1_t3970 * __this, ObjectU5BU5D_t115* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m27426(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3970 *, ObjectU5BU5D_t115*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m27426_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m27427_gshared (ArrayReadOnlyList_1_t3970 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m27427(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t3970 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m27427_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m27428_gshared (ArrayReadOnlyList_1_t3970 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m27428(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t3970 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m27428_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m27429_gshared (ArrayReadOnlyList_1_t3970 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m27429(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t3970 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Insert_m27429_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m27430_gshared (ArrayReadOnlyList_1_t3970 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m27430(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t3970 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Remove_m27430_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m27431_gshared (ArrayReadOnlyList_1_t3970 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m27431(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t3970 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m27431_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C" Exception_t140 * ArrayReadOnlyList_1_ReadOnlyError_m27432_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m27432(__this /* static, unused */, method) (( Exception_t140 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m27432_gshared)(__this /* static, unused */, method)
