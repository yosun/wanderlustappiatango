﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.TweenExtensions
struct TweenExtensions_t957;
// DG.Tweening.Tween
struct Tween_t934;

// System.Single DG.Tweening.TweenExtensions::Duration(DG.Tweening.Tween,System.Boolean)
extern "C" float TweenExtensions_Duration_m370 (Object_t * __this /* static, unused */, Tween_t934 * ___t, bool ___includeLoops, const MethodInfo* method) IL2CPP_METHOD_ATTR;
