﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Enumerator_t3570;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Dictionary_2_t870;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_26.h"
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22021_gshared (Enumerator_t3570 * __this, Dictionary_2_t870 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m22021(__this, ___dictionary, method) (( void (*) (Enumerator_t3570 *, Dictionary_2_t870 *, const MethodInfo*))Enumerator__ctor_m22021_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22022_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22022(__this, method) (( Object_t * (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22022_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1996  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22023_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22023(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22023_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22024_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22024(__this, method) (( Object_t * (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22024_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22025_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22025(__this, method) (( Object_t * (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22025_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22026_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22026(__this, method) (( bool (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_MoveNext_m22026_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Current()
extern "C" KeyValuePair_2_t3565  Enumerator_get_Current_m22027_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22027(__this, method) (( KeyValuePair_2_t3565  (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_get_Current_m22027_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m22028_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m22028(__this, method) (( int32_t (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_get_CurrentKey_m22028_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_CurrentValue()
extern "C" VirtualButtonData_t643  Enumerator_get_CurrentValue_m22029_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m22029(__this, method) (( VirtualButtonData_t643  (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_get_CurrentValue_m22029_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::VerifyState()
extern "C" void Enumerator_VerifyState_m22030_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22030(__this, method) (( void (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_VerifyState_m22030_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m22031_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m22031(__this, method) (( void (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_VerifyCurrent_m22031_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Dispose()
extern "C" void Enumerator_Dispose_m22032_gshared (Enumerator_t3570 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22032(__this, method) (( void (*) (Enumerator_t3570 *, const MethodInfo*))Enumerator_Dispose_m22032_gshared)(__this, method)
