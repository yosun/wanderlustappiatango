﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>
struct Enumerator_t3466;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.UInt16>
struct Dictionary_2_t3461;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m20150_gshared (Enumerator_t3466 * __this, Dictionary_2_t3461 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m20150(__this, ___dictionary, method) (( void (*) (Enumerator_t3466 *, Dictionary_2_t3461 *, const MethodInfo*))Enumerator__ctor_m20150_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m20151_gshared (Enumerator_t3466 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m20151(__this, method) (( Object_t * (*) (Enumerator_t3466 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20151_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1996  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20152_gshared (Enumerator_t3466 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20152(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3466 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20152_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20153_gshared (Enumerator_t3466 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20153(__this, method) (( Object_t * (*) (Enumerator_t3466 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20153_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20154_gshared (Enumerator_t3466 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20154(__this, method) (( Object_t * (*) (Enumerator_t3466 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20154_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::MoveNext()
extern "C" bool Enumerator_MoveNext_m20155_gshared (Enumerator_t3466 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m20155(__this, method) (( bool (*) (Enumerator_t3466 *, const MethodInfo*))Enumerator_MoveNext_m20155_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_Current()
extern "C" KeyValuePair_2_t3462  Enumerator_get_Current_m20156_gshared (Enumerator_t3466 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m20156(__this, method) (( KeyValuePair_2_t3462  (*) (Enumerator_t3466 *, const MethodInfo*))Enumerator_get_Current_m20156_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m20157_gshared (Enumerator_t3466 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m20157(__this, method) (( Object_t * (*) (Enumerator_t3466 *, const MethodInfo*))Enumerator_get_CurrentKey_m20157_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::get_CurrentValue()
extern "C" uint16_t Enumerator_get_CurrentValue_m20158_gshared (Enumerator_t3466 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m20158(__this, method) (( uint16_t (*) (Enumerator_t3466 *, const MethodInfo*))Enumerator_get_CurrentValue_m20158_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::VerifyState()
extern "C" void Enumerator_VerifyState_m20159_gshared (Enumerator_t3466 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m20159(__this, method) (( void (*) (Enumerator_t3466 *, const MethodInfo*))Enumerator_VerifyState_m20159_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m20160_gshared (Enumerator_t3466 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m20160(__this, method) (( void (*) (Enumerator_t3466 *, const MethodInfo*))Enumerator_VerifyCurrent_m20160_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>::Dispose()
extern "C" void Enumerator_Dispose_m20161_gshared (Enumerator_t3466 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m20161(__this, method) (( void (*) (Enumerator_t3466 *, const MethodInfo*))Enumerator_Dispose_m20161_gshared)(__this, method)
