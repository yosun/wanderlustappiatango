﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// UnityEngine.Internal.DefaultValueAttribute
struct  DefaultValueAttribute_t1349  : public Attribute_t138
{
	// System.Object UnityEngine.Internal.DefaultValueAttribute::DefaultValue
	Object_t * ___DefaultValue_0;
};
