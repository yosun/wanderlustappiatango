﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,Vuforia.QCARManagerImpl/TrackableResultData>
struct Transform_1_t3558;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m21911_gshared (Transform_1_t3558 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m21911(__this, ___object, ___method, method) (( void (*) (Transform_1_t3558 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m21911_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,Vuforia.QCARManagerImpl/TrackableResultData>::Invoke(TKey,TValue)
extern "C" TrackableResultData_t642  Transform_1_Invoke_m21912_gshared (Transform_1_t3558 * __this, int32_t ___key, TrackableResultData_t642  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m21912(__this, ___key, ___value, method) (( TrackableResultData_t642  (*) (Transform_1_t3558 *, int32_t, TrackableResultData_t642 , const MethodInfo*))Transform_1_Invoke_m21912_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,Vuforia.QCARManagerImpl/TrackableResultData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m21913_gshared (Transform_1_t3558 * __this, int32_t ___key, TrackableResultData_t642  ___value, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m21913(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3558 *, int32_t, TrackableResultData_t642 , AsyncCallback_t305 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m21913_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData,Vuforia.QCARManagerImpl/TrackableResultData>::EndInvoke(System.IAsyncResult)
extern "C" TrackableResultData_t642  Transform_1_EndInvoke_m21914_gshared (Transform_1_t3558 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m21914(__this, ___result, method) (( TrackableResultData_t642  (*) (Transform_1_t3558 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m21914_gshared)(__this, ___result, method)
