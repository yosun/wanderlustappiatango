﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t19;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SetRenderQueueChildren
struct  SetRenderQueueChildren_t21  : public MonoBehaviour_t7
{
	// System.Int32[] SetRenderQueueChildren::m_queues
	Int32U5BU5D_t19* ___m_queues_2;
};
