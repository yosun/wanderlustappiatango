﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.GameObject>
struct InternalEnumerator_1_t3099;
// System.Object
struct Object_t;
// UnityEngine.GameObject
struct GameObject_t2;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.GameObject>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m14857(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3099 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14858_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.GameObject>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14859(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3099 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14860_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GameObject>::Dispose()
#define InternalEnumerator_1_Dispose_m14861(__this, method) (( void (*) (InternalEnumerator_1_t3099 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14862_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.GameObject>::MoveNext()
#define InternalEnumerator_1_MoveNext_m14863(__this, method) (( bool (*) (InternalEnumerator_1_t3099 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14864_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.GameObject>::get_Current()
#define InternalEnumerator_1_get_Current_m14865(__this, method) (( GameObject_t2 * (*) (InternalEnumerator_1_t3099 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14866_gshared)(__this, method)
