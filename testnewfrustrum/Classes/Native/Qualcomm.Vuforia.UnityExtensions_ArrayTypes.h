﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// Vuforia.InternalEyewear/EyewearCalibrationReading[]
// Vuforia.InternalEyewear/EyewearCalibrationReading[]
struct  EyewearCalibrationReadingU5BU5D_t760  : public Array_t
{
};
// Vuforia.ITrackableEventHandler[]
// Vuforia.ITrackableEventHandler[]
struct  ITrackableEventHandlerU5BU5D_t3357  : public Array_t
{
};
// Vuforia.ICloudRecoEventHandler[]
// Vuforia.ICloudRecoEventHandler[]
struct  ICloudRecoEventHandlerU5BU5D_t3362  : public Array_t
{
};
// Vuforia.VirtualButton[]
// Vuforia.VirtualButton[]
struct  VirtualButtonU5BU5D_t3376  : public Array_t
{
};
// Vuforia.Image/PIXEL_FORMAT[]
// Vuforia.Image/PIXEL_FORMAT[]
struct  PIXEL_FORMATU5BU5D_t3382  : public Array_t
{
};
// Vuforia.Image[]
// Vuforia.Image[]
struct  ImageU5BU5D_t3383  : public Array_t
{
};
// Vuforia.Trackable[]
// Vuforia.Trackable[]
struct  TrackableU5BU5D_t3402  : public Array_t
{
};
// Vuforia.DataSetImpl[]
// Vuforia.DataSetImpl[]
struct  DataSetImplU5BU5D_t3418  : public Array_t
{
};
// Vuforia.DataSet[]
// Vuforia.DataSet[]
struct  DataSetU5BU5D_t3423  : public Array_t
{
};
// Vuforia.Marker[]
// Vuforia.Marker[]
struct  MarkerU5BU5D_t3430  : public Array_t
{
};
// Vuforia.QCARManagerImpl/TrackableResultData[]
// Vuforia.QCARManagerImpl/TrackableResultData[]
struct  TrackableResultDataU5BU5D_t656  : public Array_t
{
};
// Vuforia.QCARManagerImpl/WordData[]
// Vuforia.QCARManagerImpl/WordData[]
#pragma pack(push, tp, 1)
struct  WordDataU5BU5D_t657  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/WordResultData[]
// Vuforia.QCARManagerImpl/WordResultData[]
struct  WordResultDataU5BU5D_t658  : public Array_t
{
};
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData[]
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData[]
#pragma pack(push, tp, 1)
struct  SmartTerrainRevisionDataU5BU5D_t768  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/SurfaceData[]
// Vuforia.QCARManagerImpl/SurfaceData[]
#pragma pack(push, tp, 1)
struct  SurfaceDataU5BU5D_t769  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/PropData[]
// Vuforia.QCARManagerImpl/PropData[]
#pragma pack(push, tp, 1)
struct  PropDataU5BU5D_t770  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.TrackableBehaviour[]
// Vuforia.TrackableBehaviour[]
struct  TrackableBehaviourU5BU5D_t818  : public Array_t
{
};
// Vuforia.IEditorTrackableBehaviour[]
// Vuforia.IEditorTrackableBehaviour[]
struct  IEditorTrackableBehaviourU5BU5D_t4493  : public Array_t
{
};
// Vuforia.SmartTerrainTrackable[]
// Vuforia.SmartTerrainTrackable[]
struct  SmartTerrainTrackableU5BU5D_t3447  : public Array_t
{
};
// Vuforia.ReconstructionAbstractBehaviour[]
// Vuforia.ReconstructionAbstractBehaviour[]
struct  ReconstructionAbstractBehaviourU5BU5D_t824  : public Array_t
{
};
// Vuforia.SmartTerrainTrackableBehaviour[]
// Vuforia.SmartTerrainTrackableBehaviour[]
struct  SmartTerrainTrackableBehaviourU5BU5D_t823  : public Array_t
{
};
// Vuforia.RectangleData[]
// Vuforia.RectangleData[]
#pragma pack(push, tp, 1)
struct  RectangleDataU5BU5D_t684  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.WordResult[]
// Vuforia.WordResult[]
struct  WordResultU5BU5D_t3481  : public Array_t
{
};
// Vuforia.Word[]
// Vuforia.Word[]
struct  WordU5BU5D_t3489  : public Array_t
{
};
// Vuforia.WordAbstractBehaviour[]
// Vuforia.WordAbstractBehaviour[]
struct  WordAbstractBehaviourU5BU5D_t832  : public Array_t
{
};
// Vuforia.ILoadLevelEventHandler[]
// Vuforia.ILoadLevelEventHandler[]
struct  ILoadLevelEventHandlerU5BU5D_t3504  : public Array_t
{
};
// Vuforia.ISmartTerrainEventHandler[]
// Vuforia.ISmartTerrainEventHandler[]
struct  ISmartTerrainEventHandlerU5BU5D_t3514  : public Array_t
{
};
// Vuforia.Surface[]
// Vuforia.Surface[]
struct  SurfaceU5BU5D_t3519  : public Array_t
{
};
// Vuforia.SurfaceAbstractBehaviour[]
// Vuforia.SurfaceAbstractBehaviour[]
struct  SurfaceAbstractBehaviourU5BU5D_t3521  : public Array_t
{
};
// Vuforia.Prop[]
// Vuforia.Prop[]
struct  PropU5BU5D_t3526  : public Array_t
{
};
// Vuforia.PropAbstractBehaviour[]
// Vuforia.PropAbstractBehaviour[]
struct  PropAbstractBehaviourU5BU5D_t846  : public Array_t
{
};
// Vuforia.MarkerAbstractBehaviour[]
// Vuforia.MarkerAbstractBehaviour[]
struct  MarkerAbstractBehaviourU5BU5D_t866  : public Array_t
{
};
// Vuforia.IEditorMarkerBehaviour[]
// Vuforia.IEditorMarkerBehaviour[]
struct  IEditorMarkerBehaviourU5BU5D_t4494  : public Array_t
{
};
// Vuforia.WorldCenterTrackableBehaviour[]
// Vuforia.WorldCenterTrackableBehaviour[]
struct  WorldCenterTrackableBehaviourU5BU5D_t4495  : public Array_t
{
};
// Vuforia.DataSetTrackableBehaviour[]
// Vuforia.DataSetTrackableBehaviour[]
struct  DataSetTrackableBehaviourU5BU5D_t867  : public Array_t
{
};
// Vuforia.IEditorDataSetTrackableBehaviour[]
// Vuforia.IEditorDataSetTrackableBehaviour[]
struct  IEditorDataSetTrackableBehaviourU5BU5D_t4496  : public Array_t
{
};
// Vuforia.VirtualButtonAbstractBehaviour[]
// Vuforia.VirtualButtonAbstractBehaviour[]
struct  VirtualButtonAbstractBehaviourU5BU5D_t783  : public Array_t
{
};
// Vuforia.IEditorVirtualButtonBehaviour[]
// Vuforia.IEditorVirtualButtonBehaviour[]
struct  IEditorVirtualButtonBehaviourU5BU5D_t4497  : public Array_t
{
};
// Vuforia.QCARManagerImpl/VirtualButtonData[]
// Vuforia.QCARManagerImpl/VirtualButtonData[]
#pragma pack(push, tp, 1)
struct  VirtualButtonDataU5BU5D_t3563  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.TargetFinder/TargetSearchResult[]
// Vuforia.TargetFinder/TargetSearchResult[]
struct  TargetSearchResultU5BU5D_t3583  : public Array_t
{
};
// Vuforia.ImageTarget[]
// Vuforia.ImageTarget[]
struct  ImageTargetU5BU5D_t875  : public Array_t
{
};
// Vuforia.WebCamProfile/ProfileData[]
// Vuforia.WebCamProfile/ProfileData[]
struct  ProfileDataU5BU5D_t3600  : public Array_t
{
};
// Vuforia.VideoBackgroundAbstractBehaviour[]
// Vuforia.VideoBackgroundAbstractBehaviour[]
struct  VideoBackgroundAbstractBehaviourU5BU5D_t748  : public Array_t
{
};
// Vuforia.ITrackerEventHandler[]
// Vuforia.ITrackerEventHandler[]
struct  ITrackerEventHandlerU5BU5D_t3627  : public Array_t
{
};
// Vuforia.IVideoBackgroundEventHandler[]
// Vuforia.IVideoBackgroundEventHandler[]
struct  IVideoBackgroundEventHandlerU5BU5D_t3632  : public Array_t
{
};
// Vuforia.BackgroundPlaneAbstractBehaviour[]
// Vuforia.BackgroundPlaneAbstractBehaviour[]
struct  BackgroundPlaneAbstractBehaviourU5BU5D_t884  : public Array_t
{
};
struct BackgroundPlaneAbstractBehaviourU5BU5D_t884_StaticFields{
};
// Vuforia.ITextRecoEventHandler[]
// Vuforia.ITextRecoEventHandler[]
struct  ITextRecoEventHandlerU5BU5D_t3638  : public Array_t
{
};
// Vuforia.IUserDefinedTargetEventHandler[]
// Vuforia.IUserDefinedTargetEventHandler[]
struct  IUserDefinedTargetEventHandlerU5BU5D_t3643  : public Array_t
{
};
// Vuforia.IVirtualButtonEventHandler[]
// Vuforia.IVirtualButtonEventHandler[]
struct  IVirtualButtonEventHandlerU5BU5D_t3658  : public Array_t
{
};
