﻿#pragma once
#include <stdint.h>
// DG.Tweening.Core.ABSSequentiable
struct ABSSequentiable_t943;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<DG.Tweening.Core.ABSSequentiable>
struct  Predicate_1_t3677  : public MulticastDelegate_t307
{
};
