﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Enumerator_t3503;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t692;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Dictionary_2_t693;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7MethodDeclarations.h"
#define Enumerator__ctor_m20810(__this, ___dictionary, method) (( void (*) (Enumerator_t3503 *, Dictionary_2_t693 *, const MethodInfo*))Enumerator__ctor_m16933_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20811(__this, method) (( Object_t * (*) (Enumerator_t3503 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16934_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20812(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3503 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16935_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20813(__this, method) (( Object_t * (*) (Enumerator_t3503 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16936_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20814(__this, method) (( Object_t * (*) (Enumerator_t3503 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16937_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::MoveNext()
#define Enumerator_MoveNext_m20815(__this, method) (( bool (*) (Enumerator_t3503 *, const MethodInfo*))Enumerator_MoveNext_m16938_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Current()
#define Enumerator_get_Current_m20816(__this, method) (( KeyValuePair_2_t3502  (*) (Enumerator_t3503 *, const MethodInfo*))Enumerator_get_Current_m16939_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m20817(__this, method) (( String_t* (*) (Enumerator_t3503 *, const MethodInfo*))Enumerator_get_CurrentKey_m16940_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m20818(__this, method) (( List_1_t692 * (*) (Enumerator_t3503 *, const MethodInfo*))Enumerator_get_CurrentValue_m16941_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::VerifyState()
#define Enumerator_VerifyState_m20819(__this, method) (( void (*) (Enumerator_t3503 *, const MethodInfo*))Enumerator_VerifyState_m16942_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m20820(__this, method) (( void (*) (Enumerator_t3503 *, const MethodInfo*))Enumerator_VerifyCurrent_m16943_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Dispose()
#define Enumerator_Dispose_m20821(__this, method) (( void (*) (Enumerator_t3503 *, const MethodInfo*))Enumerator_Dispose_m16944_gshared)(__this, method)
