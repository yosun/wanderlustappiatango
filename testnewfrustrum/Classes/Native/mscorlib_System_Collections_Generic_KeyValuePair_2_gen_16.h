﻿#pragma once
#include <stdint.h>
// Vuforia.VirtualButton
struct VirtualButton_t731;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>
struct  KeyValuePair_2_t3415 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>::value
	VirtualButton_t731 * ___value_1;
};
