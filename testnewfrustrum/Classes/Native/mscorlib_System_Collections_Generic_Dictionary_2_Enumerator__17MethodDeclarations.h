﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>
struct Enumerator_t3479;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t680;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.UInt16>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__16MethodDeclarations.h"
#define Enumerator__ctor_m20252(__this, ___dictionary, method) (( void (*) (Enumerator_t3479 *, Dictionary_2_t680 *, const MethodInfo*))Enumerator__ctor_m20150_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20253(__this, method) (( Object_t * (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m20151_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20254(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20152_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20255(__this, method) (( Object_t * (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20153_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20256(__this, method) (( Object_t * (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20154_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::MoveNext()
#define Enumerator_MoveNext_m20257(__this, method) (( bool (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_MoveNext_m20155_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_Current()
#define Enumerator_get_Current_m20258(__this, method) (( KeyValuePair_2_t3476  (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_get_Current_m20156_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m20259(__this, method) (( Type_t * (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_get_CurrentKey_m20157_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m20260(__this, method) (( uint16_t (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_get_CurrentValue_m20158_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::VerifyState()
#define Enumerator_VerifyState_m20261(__this, method) (( void (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_VerifyState_m20159_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m20262(__this, method) (( void (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_VerifyCurrent_m20160_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::Dispose()
#define Enumerator_Dispose_m20263(__this, method) (( void (*) (Enumerator_t3479 *, const MethodInfo*))Enumerator_Dispose_m20161_gshared)(__this, method)
