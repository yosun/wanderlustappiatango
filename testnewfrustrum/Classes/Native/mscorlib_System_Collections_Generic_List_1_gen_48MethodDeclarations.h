﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>
struct List_1_t947;
// System.Object
struct Object_t;
// DG.Tweening.Core.ABSSequentiable
struct ABSSequentiable_t943;
// System.Collections.Generic.IEnumerable`1<DG.Tweening.Core.ABSSequentiable>
struct IEnumerable_1_t4289;
// System.Collections.Generic.IEnumerator`1<DG.Tweening.Core.ABSSequentiable>
struct IEnumerator_1_t4290;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<DG.Tweening.Core.ABSSequentiable>
struct ICollection_1_t4291;
// System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Core.ABSSequentiable>
struct ReadOnlyCollection_1_t3676;
// DG.Tweening.Core.ABSSequentiable[]
struct ABSSequentiableU5BU5D_t3674;
// System.Predicate`1<DG.Tweening.Core.ABSSequentiable>
struct Predicate_1_t3677;
// System.Comparison`1<DG.Tweening.Core.ABSSequentiable>
struct Comparison_1_t1058;
// System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_52.h"

// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m5513(__this, method) (( void (*) (List_1_t947 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23508(__this, ___collection, method) (( void (*) (List_1_t947 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::.ctor(System.Int32)
#define List_1__ctor_m23509(__this, ___capacity, method) (( void (*) (List_1_t947 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::.cctor()
#define List_1__cctor_m23510(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23511(__this, method) (( Object_t* (*) (List_1_t947 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23512(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t947 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23513(__this, method) (( Object_t * (*) (List_1_t947 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23514(__this, ___item, method) (( int32_t (*) (List_1_t947 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23515(__this, ___item, method) (( bool (*) (List_1_t947 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23516(__this, ___item, method) (( int32_t (*) (List_1_t947 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23517(__this, ___index, ___item, method) (( void (*) (List_1_t947 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23518(__this, ___item, method) (( void (*) (List_1_t947 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23519(__this, method) (( bool (*) (List_1_t947 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23520(__this, method) (( bool (*) (List_1_t947 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23521(__this, method) (( Object_t * (*) (List_1_t947 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23522(__this, method) (( bool (*) (List_1_t947 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23523(__this, method) (( bool (*) (List_1_t947 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23524(__this, ___index, method) (( Object_t * (*) (List_1_t947 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23525(__this, ___index, ___value, method) (( void (*) (List_1_t947 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::Add(T)
#define List_1_Add_m23526(__this, ___item, method) (( void (*) (List_1_t947 *, ABSSequentiable_t943 *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23527(__this, ___newCount, method) (( void (*) (List_1_t947 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23528(__this, ___collection, method) (( void (*) (List_1_t947 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23529(__this, ___enumerable, method) (( void (*) (List_1_t947 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23530(__this, ___collection, method) (( void (*) (List_1_t947 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::AsReadOnly()
#define List_1_AsReadOnly_m23531(__this, method) (( ReadOnlyCollection_1_t3676 * (*) (List_1_t947 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::Clear()
#define List_1_Clear_m23532(__this, method) (( void (*) (List_1_t947 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::Contains(T)
#define List_1_Contains_m23533(__this, ___item, method) (( bool (*) (List_1_t947 *, ABSSequentiable_t943 *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23534(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t947 *, ABSSequentiableU5BU5D_t3674*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::Find(System.Predicate`1<T>)
#define List_1_Find_m23535(__this, ___match, method) (( ABSSequentiable_t943 * (*) (List_1_t947 *, Predicate_1_t3677 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23536(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3677 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23537(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t947 *, int32_t, int32_t, Predicate_1_t3677 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::GetEnumerator()
#define List_1_GetEnumerator_m23538(__this, method) (( Enumerator_t3678  (*) (List_1_t947 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::IndexOf(T)
#define List_1_IndexOf_m23539(__this, ___item, method) (( int32_t (*) (List_1_t947 *, ABSSequentiable_t943 *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23540(__this, ___start, ___delta, method) (( void (*) (List_1_t947 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23541(__this, ___index, method) (( void (*) (List_1_t947 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::Insert(System.Int32,T)
#define List_1_Insert_m23542(__this, ___index, ___item, method) (( void (*) (List_1_t947 *, int32_t, ABSSequentiable_t943 *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23543(__this, ___collection, method) (( void (*) (List_1_t947 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::Remove(T)
#define List_1_Remove_m23544(__this, ___item, method) (( bool (*) (List_1_t947 *, ABSSequentiable_t943 *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23545(__this, ___match, method) (( int32_t (*) (List_1_t947 *, Predicate_1_t3677 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23546(__this, ___index, method) (( void (*) (List_1_t947 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::Reverse()
#define List_1_Reverse_m23547(__this, method) (( void (*) (List_1_t947 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::Sort()
#define List_1_Sort_m23548(__this, method) (( void (*) (List_1_t947 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m5515(__this, ___comparison, method) (( void (*) (List_1_t947 *, Comparison_1_t1058 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::ToArray()
#define List_1_ToArray_m23549(__this, method) (( ABSSequentiableU5BU5D_t3674* (*) (List_1_t947 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::TrimExcess()
#define List_1_TrimExcess_m23550(__this, method) (( void (*) (List_1_t947 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::get_Capacity()
#define List_1_get_Capacity_m23551(__this, method) (( int32_t (*) (List_1_t947 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23552(__this, ___value, method) (( void (*) (List_1_t947 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::get_Count()
#define List_1_get_Count_m23553(__this, method) (( int32_t (*) (List_1_t947 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::get_Item(System.Int32)
#define List_1_get_Item_m23554(__this, ___index, method) (( ABSSequentiable_t943 * (*) (List_1_t947 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>::set_Item(System.Int32,T)
#define List_1_set_Item_m23555(__this, ___index, ___value, method) (( void (*) (List_1_t947 *, int32_t, ABSSequentiable_t943 *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
