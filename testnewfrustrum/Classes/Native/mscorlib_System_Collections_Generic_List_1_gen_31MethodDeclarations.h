﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.Word>
struct List_1_t690;
// System.Object
struct Object_t;
// Vuforia.Word
struct Word_t696;
// System.Collections.Generic.IEnumerable`1<Vuforia.Word>
struct IEnumerable_1_t772;
// System.Collections.Generic.IEnumerator`1<Vuforia.Word>
struct IEnumerator_1_t891;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<Vuforia.Word>
struct ICollection_1_t4189;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Word>
struct ReadOnlyCollection_1_t3491;
// Vuforia.Word[]
struct WordU5BU5D_t3489;
// System.Predicate`1<Vuforia.Word>
struct Predicate_1_t3492;
// System.Comparison`1<Vuforia.Word>
struct Comparison_1_t3493;
// System.Collections.Generic.List`1/Enumerator<Vuforia.Word>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_9.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Word>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4494(__this, method) (( void (*) (List_1_t690 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m4479(__this, ___collection, method) (( void (*) (List_1_t690 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::.ctor(System.Int32)
#define List_1__ctor_m20456(__this, ___capacity, method) (( void (*) (List_1_t690 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::.cctor()
#define List_1__cctor_m20457(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20458(__this, method) (( Object_t* (*) (List_1_t690 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20459(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t690 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20460(__this, method) (( Object_t * (*) (List_1_t690 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20461(__this, ___item, method) (( int32_t (*) (List_1_t690 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20462(__this, ___item, method) (( bool (*) (List_1_t690 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20463(__this, ___item, method) (( int32_t (*) (List_1_t690 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20464(__this, ___index, ___item, method) (( void (*) (List_1_t690 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20465(__this, ___item, method) (( void (*) (List_1_t690 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20466(__this, method) (( bool (*) (List_1_t690 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20467(__this, method) (( bool (*) (List_1_t690 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20468(__this, method) (( Object_t * (*) (List_1_t690 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20469(__this, method) (( bool (*) (List_1_t690 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20470(__this, method) (( bool (*) (List_1_t690 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20471(__this, ___index, method) (( Object_t * (*) (List_1_t690 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20472(__this, ___index, ___value, method) (( void (*) (List_1_t690 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::Add(T)
#define List_1_Add_m20473(__this, ___item, method) (( void (*) (List_1_t690 *, Object_t *, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20474(__this, ___newCount, method) (( void (*) (List_1_t690 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20475(__this, ___collection, method) (( void (*) (List_1_t690 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20476(__this, ___enumerable, method) (( void (*) (List_1_t690 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20477(__this, ___collection, method) (( void (*) (List_1_t690 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Word>::AsReadOnly()
#define List_1_AsReadOnly_m20478(__this, method) (( ReadOnlyCollection_1_t3491 * (*) (List_1_t690 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::Clear()
#define List_1_Clear_m20479(__this, method) (( void (*) (List_1_t690 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Word>::Contains(T)
#define List_1_Contains_m20480(__this, ___item, method) (( bool (*) (List_1_t690 *, Object_t *, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20481(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t690 *, WordU5BU5D_t3489*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.Word>::Find(System.Predicate`1<T>)
#define List_1_Find_m20482(__this, ___match, method) (( Object_t * (*) (List_1_t690 *, Predicate_1_t3492 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20483(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3492 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20484(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t690 *, int32_t, int32_t, Predicate_1_t3492 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Word>::GetEnumerator()
#define List_1_GetEnumerator_m4480(__this, method) (( Enumerator_t839  (*) (List_1_t690 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::IndexOf(T)
#define List_1_IndexOf_m20485(__this, ___item, method) (( int32_t (*) (List_1_t690 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20486(__this, ___start, ___delta, method) (( void (*) (List_1_t690 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20487(__this, ___index, method) (( void (*) (List_1_t690 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::Insert(System.Int32,T)
#define List_1_Insert_m20488(__this, ___index, ___item, method) (( void (*) (List_1_t690 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20489(__this, ___collection, method) (( void (*) (List_1_t690 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Word>::Remove(T)
#define List_1_Remove_m20490(__this, ___item, method) (( bool (*) (List_1_t690 *, Object_t *, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20491(__this, ___match, method) (( int32_t (*) (List_1_t690 *, Predicate_1_t3492 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20492(__this, ___index, method) (( void (*) (List_1_t690 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::Reverse()
#define List_1_Reverse_m20493(__this, method) (( void (*) (List_1_t690 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::Sort()
#define List_1_Sort_m20494(__this, method) (( void (*) (List_1_t690 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20495(__this, ___comparison, method) (( void (*) (List_1_t690 *, Comparison_1_t3493 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.Word>::ToArray()
#define List_1_ToArray_m20496(__this, method) (( WordU5BU5D_t3489* (*) (List_1_t690 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::TrimExcess()
#define List_1_TrimExcess_m20497(__this, method) (( void (*) (List_1_t690 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::get_Capacity()
#define List_1_get_Capacity_m20498(__this, method) (( int32_t (*) (List_1_t690 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20499(__this, ___value, method) (( void (*) (List_1_t690 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Word>::get_Count()
#define List_1_get_Count_m20500(__this, method) (( int32_t (*) (List_1_t690 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.Word>::get_Item(System.Int32)
#define List_1_get_Item_m20501(__this, ___index, method) (( Object_t * (*) (List_1_t690 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Word>::set_Item(System.Int32,T)
#define List_1_set_Item_m20502(__this, ___index, ___value, method) (( void (*) (List_1_t690 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
