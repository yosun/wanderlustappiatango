﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoTODOAttribute
struct MonoTODOAttribute_t1578;

// System.Void System.MonoTODOAttribute::.ctor()
extern "C" void MonoTODOAttribute__ctor_m7258 (MonoTODOAttribute_t1578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
