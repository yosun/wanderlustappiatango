﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct InternalEnumerator_1_t3198;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m16224_gshared (InternalEnumerator_1_t3198 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m16224(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3198 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m16224_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16225_gshared (InternalEnumerator_1_t3198 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16225(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3198 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16225_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m16226_gshared (InternalEnumerator_1_t3198 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m16226(__this, method) (( void (*) (InternalEnumerator_1_t3198 *, const MethodInfo*))InternalEnumerator_1_Dispose_m16226_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m16227_gshared (InternalEnumerator_1_t3198 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m16227(__this, method) (( bool (*) (InternalEnumerator_1_t3198 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m16227_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t3197  InternalEnumerator_1_get_Current_m16228_gshared (InternalEnumerator_1_t3198 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m16228(__this, method) (( KeyValuePair_2_t3197  (*) (InternalEnumerator_1_t3198 *, const MethodInfo*))InternalEnumerator_1_get_Current_m16228_gshared)(__this, method)
