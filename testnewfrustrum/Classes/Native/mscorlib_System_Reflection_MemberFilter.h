﻿#pragma once
#include <stdint.h>
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Reflection.MemberFilter
struct  MemberFilter_t2046  : public MulticastDelegate_t307
{
};
