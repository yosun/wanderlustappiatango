﻿#pragma once
#include <stdint.h>
// System.Type
struct Type_t;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Reflection.TypeFilter
struct  TypeFilter_t2251  : public MulticastDelegate_t307
{
};
