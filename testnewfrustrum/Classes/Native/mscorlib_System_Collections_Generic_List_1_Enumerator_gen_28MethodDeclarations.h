﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>
struct Enumerator_t3153;
// System.Object
struct Object_t;
// UnityEngine.Component
struct Component_t103;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t418;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m15536(__this, ___l, method) (( void (*) (Enumerator_t3153 *, List_1_t418 *, const MethodInfo*))Enumerator__ctor_m15049_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15537(__this, method) (( Object_t * (*) (Enumerator_t3153 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::Dispose()
#define Enumerator_Dispose_m15538(__this, method) (( void (*) (Enumerator_t3153 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::VerifyState()
#define Enumerator_VerifyState_m15539(__this, method) (( void (*) (Enumerator_t3153 *, const MethodInfo*))Enumerator_VerifyState_m15052_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::MoveNext()
#define Enumerator_MoveNext_m15540(__this, method) (( bool (*) (Enumerator_t3153 *, const MethodInfo*))Enumerator_MoveNext_m15053_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Component>::get_Current()
#define Enumerator_get_Current_m15541(__this, method) (( Component_t103 * (*) (Enumerator_t3153 *, const MethodInfo*))Enumerator_get_Current_m15054_gshared)(__this, method)
