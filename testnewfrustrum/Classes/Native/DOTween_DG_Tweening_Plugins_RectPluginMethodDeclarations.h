﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.RectPlugin
struct RectPlugin_t981;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct TweenerCore_3_t1034;
// DG.Tweening.Tween
struct Tween_t934;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>
struct DOGetter_1_t1035;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>
struct DOSetter_1_t1036;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// DG.Tweening.Plugins.Options.RectOptions
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Plugins.RectPlugin::Reset(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern "C" void RectPlugin_Reset_m5424 (RectPlugin_t981 * __this, TweenerCore_3_t1034 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect DG.Tweening.Plugins.RectPlugin::ConvertToStartValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>,UnityEngine.Rect)
extern "C" Rect_t124  RectPlugin_ConvertToStartValue_m5425 (RectPlugin_t981 * __this, TweenerCore_3_t1034 * ___t, Rect_t124  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.RectPlugin::SetRelativeEndValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern "C" void RectPlugin_SetRelativeEndValue_m5426 (RectPlugin_t981 * __this, TweenerCore_3_t1034 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.RectPlugin::SetChangeValue(DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>)
extern "C" void RectPlugin_SetChangeValue_m5427 (RectPlugin_t981 * __this, TweenerCore_3_t1034 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.Plugins.RectPlugin::GetSpeedBasedDuration(DG.Tweening.Plugins.Options.RectOptions,System.Single,UnityEngine.Rect)
extern "C" float RectPlugin_GetSpeedBasedDuration_m5428 (RectPlugin_t981 * __this, RectOptions_t1004  ___options, float ___unitsXSecond, Rect_t124  ___changeValue, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.RectPlugin::EvaluateAndApply(DG.Tweening.Plugins.Options.RectOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>,DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>,System.Single,UnityEngine.Rect,UnityEngine.Rect,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" void RectPlugin_EvaluateAndApply_m5429 (RectPlugin_t981 * __this, RectOptions_t1004  ___options, Tween_t934 * ___t, bool ___isRelative, DOGetter_1_t1035 * ___getter, DOSetter_1_t1036 * ___setter, float ___elapsed, Rect_t124  ___startValue, Rect_t124  ___changeValue, float ___duration, bool ___usingInversePosition, int32_t ___updateNotice, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Plugins.RectPlugin::.ctor()
extern "C" void RectPlugin__ctor_m5430 (RectPlugin_t981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
