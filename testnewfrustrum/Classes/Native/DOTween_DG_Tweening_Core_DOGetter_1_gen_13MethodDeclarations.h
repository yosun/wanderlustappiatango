﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<System.Object>
struct DOGetter_1_t3680;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m23603_gshared (DOGetter_1_t3680 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m23603(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t3680 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m23603_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke()
extern "C" Object_t * DOGetter_1_Invoke_m23604_gshared (DOGetter_1_t3680 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m23604(__this, method) (( Object_t * (*) (DOGetter_1_t3680 *, const MethodInfo*))DOGetter_1_Invoke_m23604_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m23605_gshared (DOGetter_1_t3680 * __this, AsyncCallback_t305 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m23605(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t3680 *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m23605_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * DOGetter_1_EndInvoke_m23606_gshared (DOGetter_1_t3680 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m23606(__this, ___result, method) (( Object_t * (*) (DOGetter_1_t3680 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m23606_gshared)(__this, ___result, method)
