﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>
struct Enumerator_t3621;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t735;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_29.h"
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__26MethodDeclarations.h"
#define Enumerator__ctor_m22661(__this, ___dictionary, method) (( void (*) (Enumerator_t3621 *, Dictionary_2_t735 *, const MethodInfo*))Enumerator__ctor_m22562_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22662(__this, method) (( Object_t * (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22563_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22663(__this, method) (( DictionaryEntry_t1996  (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22564_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22664(__this, method) (( Object_t * (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22565_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22665(__this, method) (( Object_t * (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22566_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::MoveNext()
#define Enumerator_MoveNext_m22666(__this, method) (( bool (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_MoveNext_m22567_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_Current()
#define Enumerator_get_Current_m22667(__this, method) (( KeyValuePair_2_t3618  (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_get_Current_m22568_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m22668(__this, method) (( String_t* (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_get_CurrentKey_m22569_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m22669(__this, method) (( ProfileData_t734  (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_get_CurrentValue_m22570_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::VerifyState()
#define Enumerator_VerifyState_m22670(__this, method) (( void (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_VerifyState_m22571_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m22671(__this, method) (( void (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_VerifyCurrent_m22572_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,Vuforia.WebCamProfile/ProfileData>::Dispose()
#define Enumerator_Dispose_m22672(__this, method) (( void (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_Dispose_m22573_gshared)(__this, method)
