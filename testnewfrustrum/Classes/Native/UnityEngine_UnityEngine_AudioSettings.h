﻿#pragma once
#include <stdint.h>
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_t1226;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.AudioSettings
struct  AudioSettings_t1227  : public Object_t
{
};
struct AudioSettings_t1227_StaticFields{
	// UnityEngine.AudioSettings/AudioConfigurationChangeHandler UnityEngine.AudioSettings::OnAudioConfigurationChanged
	AudioConfigurationChangeHandler_t1226 * ___OnAudioConfigurationChanged_0;
};
