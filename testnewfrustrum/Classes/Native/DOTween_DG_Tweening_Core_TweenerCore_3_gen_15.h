﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// DG.Tweening.Core.DOGetter`1<System.Object>
struct DOGetter_1_t3680;
// DG.Tweening.Core.DOSetter`1<System.Object>
struct DOSetter_1_t3681;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t3682;
// DG.Tweening.Tweener
#include "DOTween_DG_Tweening_Tweener.h"
// DG.Tweening.Plugins.Options.NoOptions
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions.h"
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
struct  TweenerCore_3_t3683  : public Tweener_t99
{
	// T2 DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::startValue
	Object_t * ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::endValue
	Object_t * ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::changeValue
	Object_t * ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::plugOptions
	NoOptions_t933  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::getter
	DOGetter_1_t3680 * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::setter
	DOSetter_1_t3681 * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::tweenPlugin
	ABSTweenPlugin_3_t3682 * ___tweenPlugin_59;
};
