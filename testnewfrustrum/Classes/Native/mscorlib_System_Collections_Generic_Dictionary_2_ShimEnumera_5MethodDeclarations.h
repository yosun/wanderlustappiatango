﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
struct ShimEnumerator_t3615;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3603;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m22609_gshared (ShimEnumerator_t3615 * __this, Dictionary_2_t3603 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m22609(__this, ___host, method) (( void (*) (ShimEnumerator_t3615 *, Dictionary_2_t3603 *, const MethodInfo*))ShimEnumerator__ctor_m22609_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m22610_gshared (ShimEnumerator_t3615 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m22610(__this, method) (( bool (*) (ShimEnumerator_t3615 *, const MethodInfo*))ShimEnumerator_MoveNext_m22610_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Entry()
extern "C" DictionaryEntry_t1996  ShimEnumerator_get_Entry_m22611_gshared (ShimEnumerator_t3615 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m22611(__this, method) (( DictionaryEntry_t1996  (*) (ShimEnumerator_t3615 *, const MethodInfo*))ShimEnumerator_get_Entry_m22611_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m22612_gshared (ShimEnumerator_t3615 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m22612(__this, method) (( Object_t * (*) (ShimEnumerator_t3615 *, const MethodInfo*))ShimEnumerator_get_Key_m22612_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m22613_gshared (ShimEnumerator_t3615 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m22613(__this, method) (( Object_t * (*) (ShimEnumerator_t3615 *, const MethodInfo*))ShimEnumerator_get_Value_m22613_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m22614_gshared (ShimEnumerator_t3615 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m22614(__this, method) (( Object_t * (*) (ShimEnumerator_t3615 *, const MethodInfo*))ShimEnumerator_get_Current_m22614_gshared)(__this, method)
