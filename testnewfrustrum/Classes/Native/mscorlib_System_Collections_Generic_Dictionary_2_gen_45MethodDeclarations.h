﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1932;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t4050;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t3956;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t3960;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t3193;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>
struct IDictionary_2_t4425;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1382;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t4426;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct IEnumerator_1_t4427;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1995;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_46.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__42.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m27259_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m27259(__this, method) (( void (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2__ctor_m27259_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m27260_gshared (Dictionary_2_t1932 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m27260(__this, ___comparer, method) (( void (*) (Dictionary_2_t1932 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m27260_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m27261_gshared (Dictionary_2_t1932 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m27261(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1932 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m27261_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m27262_gshared (Dictionary_2_t1932 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m27262(__this, ___capacity, method) (( void (*) (Dictionary_2_t1932 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m27262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m27263_gshared (Dictionary_2_t1932 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m27263(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1932 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m27263_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m27264_gshared (Dictionary_2_t1932 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m27264(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1932 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2__ctor_m27264_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27265_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27265(__this, method) (( Object_t* (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m27265_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27266_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27266(__this, method) (( Object_t* (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m27266_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m27267_gshared (Dictionary_2_t1932 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m27267(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1932 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m27267_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m27268_gshared (Dictionary_2_t1932 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m27268(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1932 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m27268_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m27269_gshared (Dictionary_2_t1932 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m27269(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1932 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m27269_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m27270_gshared (Dictionary_2_t1932 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m27270(__this, ___key, method) (( bool (*) (Dictionary_2_t1932 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m27270_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m27271_gshared (Dictionary_2_t1932 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m27271(__this, ___key, method) (( void (*) (Dictionary_2_t1932 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m27271_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27272_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27272(__this, method) (( bool (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m27272_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27273_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27273(__this, method) (( Object_t * (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m27273_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27274_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27274(__this, method) (( bool (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m27274_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27275_gshared (Dictionary_2_t1932 * __this, KeyValuePair_2_t3954  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27275(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1932 *, KeyValuePair_2_t3954 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m27275_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27276_gshared (Dictionary_2_t1932 * __this, KeyValuePair_2_t3954  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27276(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1932 *, KeyValuePair_2_t3954 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m27276_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27277_gshared (Dictionary_2_t1932 * __this, KeyValuePair_2U5BU5D_t4426* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27277(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1932 *, KeyValuePair_2U5BU5D_t4426*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m27277_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27278_gshared (Dictionary_2_t1932 * __this, KeyValuePair_2_t3954  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27278(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1932 *, KeyValuePair_2_t3954 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m27278_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m27279_gshared (Dictionary_2_t1932 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m27279(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1932 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m27279_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27280_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27280(__this, method) (( Object_t * (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m27280_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27281_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27281(__this, method) (( Object_t* (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m27281_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27282_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27282(__this, method) (( Object_t * (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m27282_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m27283_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m27283(__this, method) (( int32_t (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2_get_Count_m27283_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m27284_gshared (Dictionary_2_t1932 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m27284(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1932 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m27284_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m27285_gshared (Dictionary_2_t1932 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m27285(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1932 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m27285_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m27286_gshared (Dictionary_2_t1932 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m27286(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1932 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m27286_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m27287_gshared (Dictionary_2_t1932 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m27287(__this, ___size, method) (( void (*) (Dictionary_2_t1932 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m27287_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m27288_gshared (Dictionary_2_t1932 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m27288(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1932 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m27288_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3954  Dictionary_2_make_pair_m27289_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m27289(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3954  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m27289_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m27290_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m27290(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m27290_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m27291_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m27291(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m27291_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m27292_gshared (Dictionary_2_t1932 * __this, KeyValuePair_2U5BU5D_t4426* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m27292(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1932 *, KeyValuePair_2U5BU5D_t4426*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m27292_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m27293_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m27293(__this, method) (( void (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2_Resize_m27293_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m27294_gshared (Dictionary_2_t1932 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m27294(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1932 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m27294_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m27295_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m27295(__this, method) (( void (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2_Clear_m27295_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m27296_gshared (Dictionary_2_t1932 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m27296(__this, ___key, method) (( bool (*) (Dictionary_2_t1932 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m27296_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m27297_gshared (Dictionary_2_t1932 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m27297(__this, ___value, method) (( bool (*) (Dictionary_2_t1932 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m27297_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m27298_gshared (Dictionary_2_t1932 * __this, SerializationInfo_t1382 * ___info, StreamingContext_t1383  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m27298(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1932 *, SerializationInfo_t1382 *, StreamingContext_t1383 , const MethodInfo*))Dictionary_2_GetObjectData_m27298_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m27299_gshared (Dictionary_2_t1932 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m27299(__this, ___sender, method) (( void (*) (Dictionary_2_t1932 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m27299_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m27300_gshared (Dictionary_2_t1932 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m27300(__this, ___key, method) (( bool (*) (Dictionary_2_t1932 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m27300_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m27301_gshared (Dictionary_2_t1932 * __this, int32_t ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m27301(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1932 *, int32_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m27301_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Keys()
extern "C" KeyCollection_t3956 * Dictionary_2_get_Keys_m27302_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m27302(__this, method) (( KeyCollection_t3956 * (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2_get_Keys_m27302_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Values()
extern "C" ValueCollection_t3960 * Dictionary_2_get_Values_m27303_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m27303(__this, method) (( ValueCollection_t3960 * (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2_get_Values_m27303_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m27304_gshared (Dictionary_2_t1932 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m27304(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1932 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m27304_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m27305_gshared (Dictionary_2_t1932 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m27305(__this, ___value, method) (( int32_t (*) (Dictionary_2_t1932 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m27305_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m27306_gshared (Dictionary_2_t1932 * __this, KeyValuePair_2_t3954  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m27306(__this, ___pair, method) (( bool (*) (Dictionary_2_t1932 *, KeyValuePair_2_t3954 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m27306_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetEnumerator()
extern "C" Enumerator_t3958  Dictionary_2_GetEnumerator_m27307_gshared (Dictionary_2_t1932 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m27307(__this, method) (( Enumerator_t3958  (*) (Dictionary_2_t1932 *, const MethodInfo*))Dictionary_2_GetEnumerator_m27307_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1996  Dictionary_2_U3CCopyToU3Em__0_m27308_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m27308(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1996  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m27308_gshared)(__this /* static, unused */, ___key, ___value, method)
