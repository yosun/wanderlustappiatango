﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Cryptography.CryptoConvert
struct CryptoConvert_t1677;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t616;

// System.String Mono.Security.Cryptography.CryptoConvert::ToHex(System.Byte[])
extern "C" String_t* CryptoConvert_ToHex_m7490 (Object_t * __this /* static, unused */, ByteU5BU5D_t616* ___input, const MethodInfo* method) IL2CPP_METHOD_ATTR;
