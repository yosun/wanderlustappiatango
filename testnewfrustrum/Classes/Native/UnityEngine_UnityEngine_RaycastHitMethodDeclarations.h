﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RaycastHit
struct RaycastHit_t94;
// UnityEngine.Collider
struct Collider_t157;
// UnityEngine.Rigidbody
struct Rigidbody_t1223;
// UnityEngine.Transform
struct Transform_t11;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C" Vector3_t15  RaycastHit_get_point_m256 (RaycastHit_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RaycastHit::set_point(UnityEngine.Vector3)
extern "C" void RaycastHit_set_point_m319 (RaycastHit_t94 * __this, Vector3_t15  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C" Vector3_t15  RaycastHit_get_normal_m2054 (RaycastHit_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C" float RaycastHit_get_distance_m2053 (RaycastHit_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C" Collider_t157 * RaycastHit_get_collider_m2052 (RaycastHit_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C" Rigidbody_t1223 * RaycastHit_get_rigidbody_m6354 (RaycastHit_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C" Transform_t11 * RaycastHit_get_transform_m258 (RaycastHit_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
