﻿#pragma once
#include <stdint.h>
// Vuforia.VirtualButton
struct VirtualButton_t731;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.VirtualButton>
struct  Comparison_1_t3381  : public MulticastDelegate_t307
{
};
