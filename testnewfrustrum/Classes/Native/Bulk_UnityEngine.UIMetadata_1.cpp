﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "stringLiterals.h"
// UnityEngine.UI.ScrollRect/ScrollRectEvent
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRectEvent.h"
// Metadata Definition UnityEngine.UI.ScrollRect/ScrollRectEvent
extern TypeInfo ScrollRectEvent_t331_il2cpp_TypeInfo;
// UnityEngine.UI.ScrollRect/ScrollRectEvent
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRectEventMethodDeclarations.h"
extern const Il2CppType Void_t168_0_0_0;
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect/ScrollRectEvent::.ctor()
extern const MethodInfo ScrollRectEvent__ctor_m1466_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ScrollRectEvent__ctor_m1466/* method */
	, &ScrollRectEvent_t331_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 820/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ScrollRectEvent_t331_MethodInfos[] =
{
	&ScrollRectEvent__ctor_m1466_MethodInfo,
	NULL
};
extern const MethodInfo Object_Equals_m540_MethodInfo;
extern const MethodInfo Object_Finalize_m515_MethodInfo;
extern const MethodInfo Object_GetHashCode_m541_MethodInfo;
extern const MethodInfo UnityEventBase_ToString_m2549_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2550_MethodInfo;
extern const MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2551_MethodInfo;
extern const Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m2580_GenericMethod;
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m2581_GenericMethod;
static const Il2CppMethodReference ScrollRectEvent_t331_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&UnityEventBase_ToString_m2549_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2550_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2551_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m2580_GenericMethod,
	&UnityEvent_1_GetDelegate_m2581_GenericMethod,
};
static bool ScrollRectEvent_t331_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
extern const Il2CppType ISerializationCallbackReceiver_t510_0_0_0;
static Il2CppInterfaceOffsetPair ScrollRectEvent_t331_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t510_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScrollRectEvent_t331_0_0_0;
extern const Il2CppType ScrollRectEvent_t331_1_0_0;
extern const Il2CppType UnityEvent_1_t332_0_0_0;
extern TypeInfo ScrollRect_t333_il2cpp_TypeInfo;
extern const Il2CppType ScrollRect_t333_0_0_0;
struct ScrollRectEvent_t331;
const Il2CppTypeDefinitionMetadata ScrollRectEvent_t331_DefinitionMetadata = 
{
	&ScrollRect_t333_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScrollRectEvent_t331_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t332_0_0_0/* parent */
	, ScrollRectEvent_t331_VTable/* vtableMethods */
	, ScrollRectEvent_t331_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ScrollRectEvent_t331_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollRectEvent"/* name */
	, ""/* namespaze */
	, ScrollRectEvent_t331_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ScrollRectEvent_t331_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScrollRectEvent_t331_0_0_0/* byval_arg */
	, &ScrollRectEvent_t331_1_0_0/* this_arg */
	, &ScrollRectEvent_t331_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollRectEvent_t331)/* instance_size */
	, sizeof (ScrollRectEvent_t331)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.ScrollRect
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect.h"
// Metadata Definition UnityEngine.UI.ScrollRect
// UnityEngine.UI.ScrollRect
#include "UnityEngine_UI_UnityEngine_UI_ScrollRectMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::.ctor()
extern const MethodInfo ScrollRect__ctor_m1467_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ScrollRect__ctor_m1467/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 764/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::get_content()
extern const MethodInfo ScrollRect_get_content_m1468_MethodInfo = 
{
	"get_content"/* name */
	, (methodPointerType)&ScrollRect_get_content_m1468/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t272_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 765/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_content_m1469_ParameterInfos[] = 
{
	{"value", 0, 134218190, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_content(UnityEngine.RectTransform)
extern const MethodInfo ScrollRect_set_content_m1469_MethodInfo = 
{
	"set_content"/* name */
	, (methodPointerType)&ScrollRect_set_content_m1469/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_content_m1469_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 766/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_horizontal()
extern const MethodInfo ScrollRect_get_horizontal_m1470_MethodInfo = 
{
	"get_horizontal"/* name */
	, (methodPointerType)&ScrollRect_get_horizontal_m1470/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 767/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_horizontal_m1471_ParameterInfos[] = 
{
	{"value", 0, 134218191, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontal(System.Boolean)
extern const MethodInfo ScrollRect_set_horizontal_m1471_MethodInfo = 
{
	"set_horizontal"/* name */
	, (methodPointerType)&ScrollRect_set_horizontal_m1471/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_horizontal_m1471_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 768/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_vertical()
extern const MethodInfo ScrollRect_get_vertical_m1472_MethodInfo = 
{
	"get_vertical"/* name */
	, (methodPointerType)&ScrollRect_get_vertical_m1472/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 769/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_vertical_m1473_ParameterInfos[] = 
{
	{"value", 0, 134218192, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_vertical(System.Boolean)
extern const MethodInfo ScrollRect_set_vertical_m1473_MethodInfo = 
{
	"set_vertical"/* name */
	, (methodPointerType)&ScrollRect_set_vertical_m1473/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_vertical_m1473_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 770/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MovementType_t330_0_0_0;
extern void* RuntimeInvoker_MovementType_t330 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ScrollRect/MovementType UnityEngine.UI.ScrollRect::get_movementType()
extern const MethodInfo ScrollRect_get_movementType_m1474_MethodInfo = 
{
	"get_movementType"/* name */
	, (methodPointerType)&ScrollRect_get_movementType_m1474/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &MovementType_t330_0_0_0/* return_type */
	, RuntimeInvoker_MovementType_t330/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 771/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType MovementType_t330_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_movementType_m1475_ParameterInfos[] = 
{
	{"value", 0, 134218193, 0, &MovementType_t330_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_movementType(UnityEngine.UI.ScrollRect/MovementType)
extern const MethodInfo ScrollRect_set_movementType_m1475_MethodInfo = 
{
	"set_movementType"/* name */
	, (methodPointerType)&ScrollRect_set_movementType_m1475/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_movementType_m1475_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 772/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_elasticity()
extern const MethodInfo ScrollRect_get_elasticity_m1476_MethodInfo = 
{
	"get_elasticity"/* name */
	, (methodPointerType)&ScrollRect_get_elasticity_m1476/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 773/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_elasticity_m1477_ParameterInfos[] = 
{
	{"value", 0, 134218194, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_elasticity(System.Single)
extern const MethodInfo ScrollRect_set_elasticity_m1477_MethodInfo = 
{
	"set_elasticity"/* name */
	, (methodPointerType)&ScrollRect_set_elasticity_m1477/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_elasticity_m1477_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 774/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::get_inertia()
extern const MethodInfo ScrollRect_get_inertia_m1478_MethodInfo = 
{
	"get_inertia"/* name */
	, (methodPointerType)&ScrollRect_get_inertia_m1478/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 775/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_inertia_m1479_ParameterInfos[] = 
{
	{"value", 0, 134218195, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_inertia(System.Boolean)
extern const MethodInfo ScrollRect_set_inertia_m1479_MethodInfo = 
{
	"set_inertia"/* name */
	, (methodPointerType)&ScrollRect_set_inertia_m1479/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_inertia_m1479_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 776/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_decelerationRate()
extern const MethodInfo ScrollRect_get_decelerationRate_m1480_MethodInfo = 
{
	"get_decelerationRate"/* name */
	, (methodPointerType)&ScrollRect_get_decelerationRate_m1480/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 777/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_decelerationRate_m1481_ParameterInfos[] = 
{
	{"value", 0, 134218196, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_decelerationRate(System.Single)
extern const MethodInfo ScrollRect_set_decelerationRate_m1481_MethodInfo = 
{
	"set_decelerationRate"/* name */
	, (methodPointerType)&ScrollRect_set_decelerationRate_m1481/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_decelerationRate_m1481_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 778/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_scrollSensitivity()
extern const MethodInfo ScrollRect_get_scrollSensitivity_m1482_MethodInfo = 
{
	"get_scrollSensitivity"/* name */
	, (methodPointerType)&ScrollRect_get_scrollSensitivity_m1482/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 779/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_scrollSensitivity_m1483_ParameterInfos[] = 
{
	{"value", 0, 134218197, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_scrollSensitivity(System.Single)
extern const MethodInfo ScrollRect_set_scrollSensitivity_m1483_MethodInfo = 
{
	"set_scrollSensitivity"/* name */
	, (methodPointerType)&ScrollRect_set_scrollSensitivity_m1483/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_scrollSensitivity_m1483_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 780/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Scrollbar_t327_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::get_horizontalScrollbar()
extern const MethodInfo ScrollRect_get_horizontalScrollbar_m1484_MethodInfo = 
{
	"get_horizontalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_get_horizontalScrollbar_m1484/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Scrollbar_t327_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 781/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Scrollbar_t327_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_horizontalScrollbar_m1485_ParameterInfos[] = 
{
	{"value", 0, 134218198, 0, &Scrollbar_t327_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontalScrollbar(UnityEngine.UI.Scrollbar)
extern const MethodInfo ScrollRect_set_horizontalScrollbar_m1485_MethodInfo = 
{
	"set_horizontalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_set_horizontalScrollbar_m1485/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_horizontalScrollbar_m1485_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 782/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Scrollbar UnityEngine.UI.ScrollRect::get_verticalScrollbar()
extern const MethodInfo ScrollRect_get_verticalScrollbar_m1486_MethodInfo = 
{
	"get_verticalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_get_verticalScrollbar_m1486/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Scrollbar_t327_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 783/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Scrollbar_t327_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_verticalScrollbar_m1487_ParameterInfos[] = 
{
	{"value", 0, 134218199, 0, &Scrollbar_t327_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_verticalScrollbar(UnityEngine.UI.Scrollbar)
extern const MethodInfo ScrollRect_set_verticalScrollbar_m1487_MethodInfo = 
{
	"set_verticalScrollbar"/* name */
	, (methodPointerType)&ScrollRect_set_verticalScrollbar_m1487/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_verticalScrollbar_m1487_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 784/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ScrollRect/ScrollRectEvent UnityEngine.UI.ScrollRect::get_onValueChanged()
extern const MethodInfo ScrollRect_get_onValueChanged_m1488_MethodInfo = 
{
	"get_onValueChanged"/* name */
	, (methodPointerType)&ScrollRect_get_onValueChanged_m1488/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &ScrollRectEvent_t331_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 785/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ScrollRectEvent_t331_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_onValueChanged_m1489_ParameterInfos[] = 
{
	{"value", 0, 134218200, 0, &ScrollRectEvent_t331_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_onValueChanged(UnityEngine.UI.ScrollRect/ScrollRectEvent)
extern const MethodInfo ScrollRect_set_onValueChanged_m1489_MethodInfo = 
{
	"set_onValueChanged"/* name */
	, (methodPointerType)&ScrollRect_set_onValueChanged_m1489/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_onValueChanged_m1489_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 786/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ScrollRect::get_viewRect()
extern const MethodInfo ScrollRect_get_viewRect_m1490_MethodInfo = 
{
	"get_viewRect"/* name */
	, (methodPointerType)&ScrollRect_get_viewRect_m1490/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t272_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 787/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
extern void* RuntimeInvoker_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::get_velocity()
extern const MethodInfo ScrollRect_get_velocity_m1491_MethodInfo = 
{
	"get_velocity"/* name */
	, (methodPointerType)&ScrollRect_get_velocity_m1491/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 788/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_velocity_m1492_ParameterInfos[] = 
{
	{"value", 0, 134218201, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_velocity(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_set_velocity_m1492_MethodInfo = 
{
	"set_velocity"/* name */
	, (methodPointerType)&ScrollRect_set_velocity_m1492/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector2_t10/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_velocity_m1492_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 789/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t260_0_0_0;
extern const Il2CppType CanvasUpdate_t260_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_Rebuild_m1493_ParameterInfos[] = 
{
	{"executing", 0, 134218202, 0, &CanvasUpdate_t260_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo ScrollRect_Rebuild_m1493_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&ScrollRect_Rebuild_m1493/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, ScrollRect_t333_ScrollRect_Rebuild_m1493_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 790/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnEnable()
extern const MethodInfo ScrollRect_OnEnable_m1494_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&ScrollRect_OnEnable_m1494/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 791/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnDisable()
extern const MethodInfo ScrollRect_OnDisable_m1495_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ScrollRect_OnDisable_m1495/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 792/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::IsActive()
extern const MethodInfo ScrollRect_IsActive_m1496_MethodInfo = 
{
	"IsActive"/* name */
	, (methodPointerType)&ScrollRect_IsActive_m1496/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 793/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::EnsureLayoutHasRebuilt()
extern const MethodInfo ScrollRect_EnsureLayoutHasRebuilt_m1497_MethodInfo = 
{
	"EnsureLayoutHasRebuilt"/* name */
	, (methodPointerType)&ScrollRect_EnsureLayoutHasRebuilt_m1497/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 794/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::StopMovement()
extern const MethodInfo ScrollRect_StopMovement_m1498_MethodInfo = 
{
	"StopMovement"/* name */
	, (methodPointerType)&ScrollRect_StopMovement_m1498/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 795/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
extern const Il2CppType PointerEventData_t236_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_OnScroll_m1499_ParameterInfos[] = 
{
	{"data", 0, 134218203, 0, &PointerEventData_t236_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnScroll(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnScroll_m1499_MethodInfo = 
{
	"OnScroll"/* name */
	, (methodPointerType)&ScrollRect_OnScroll_m1499/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ScrollRect_t333_ScrollRect_OnScroll_m1499_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 796/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_OnInitializePotentialDrag_m1500_ParameterInfos[] = 
{
	{"eventData", 0, 134218204, 0, &PointerEventData_t236_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnInitializePotentialDrag_m1500_MethodInfo = 
{
	"OnInitializePotentialDrag"/* name */
	, (methodPointerType)&ScrollRect_OnInitializePotentialDrag_m1500/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ScrollRect_t333_ScrollRect_OnInitializePotentialDrag_m1500_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 797/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_OnBeginDrag_m1501_ParameterInfos[] = 
{
	{"eventData", 0, 134218205, 0, &PointerEventData_t236_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnBeginDrag_m1501_MethodInfo = 
{
	"OnBeginDrag"/* name */
	, (methodPointerType)&ScrollRect_OnBeginDrag_m1501/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ScrollRect_t333_ScrollRect_OnBeginDrag_m1501_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 798/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_OnEndDrag_m1502_ParameterInfos[] = 
{
	{"eventData", 0, 134218206, 0, &PointerEventData_t236_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnEndDrag_m1502_MethodInfo = 
{
	"OnEndDrag"/* name */
	, (methodPointerType)&ScrollRect_OnEndDrag_m1502/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ScrollRect_t333_ScrollRect_OnEndDrag_m1502_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 799/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_OnDrag_m1503_ParameterInfos[] = 
{
	{"eventData", 0, 134218207, 0, &PointerEventData_t236_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo ScrollRect_OnDrag_m1503_MethodInfo = 
{
	"OnDrag"/* name */
	, (methodPointerType)&ScrollRect_OnDrag_m1503/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ScrollRect_t333_ScrollRect_OnDrag_m1503_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 800/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_SetContentAnchoredPosition_m1504_ParameterInfos[] = 
{
	{"position", 0, 134218208, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetContentAnchoredPosition(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_SetContentAnchoredPosition_m1504_MethodInfo = 
{
	"SetContentAnchoredPosition"/* name */
	, (methodPointerType)&ScrollRect_SetContentAnchoredPosition_m1504/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector2_t10/* invoker_method */
	, ScrollRect_t333_ScrollRect_SetContentAnchoredPosition_m1504_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 801/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::LateUpdate()
extern const MethodInfo ScrollRect_LateUpdate_m1505_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&ScrollRect_LateUpdate_m1505/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 802/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdatePrevData()
extern const MethodInfo ScrollRect_UpdatePrevData_m1506_MethodInfo = 
{
	"UpdatePrevData"/* name */
	, (methodPointerType)&ScrollRect_UpdatePrevData_m1506/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 803/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_UpdateScrollbars_m1507_ParameterInfos[] = 
{
	{"offset", 0, 134218209, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdateScrollbars(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_UpdateScrollbars_m1507_MethodInfo = 
{
	"UpdateScrollbars"/* name */
	, (methodPointerType)&ScrollRect_UpdateScrollbars_m1507/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector2_t10/* invoker_method */
	, ScrollRect_t333_ScrollRect_UpdateScrollbars_m1507_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 804/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::get_normalizedPosition()
extern const MethodInfo ScrollRect_get_normalizedPosition_m1508_MethodInfo = 
{
	"get_normalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_normalizedPosition_m1508/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 805/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_normalizedPosition_m1509_ParameterInfos[] = 
{
	{"value", 0, 134218210, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_normalizedPosition(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_set_normalizedPosition_m1509_MethodInfo = 
{
	"set_normalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_normalizedPosition_m1509/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector2_t10/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_normalizedPosition_m1509_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 806/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_horizontalNormalizedPosition()
extern const MethodInfo ScrollRect_get_horizontalNormalizedPosition_m1510_MethodInfo = 
{
	"get_horizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_horizontalNormalizedPosition_m1510/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 807/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_horizontalNormalizedPosition_m1511_ParameterInfos[] = 
{
	{"value", 0, 134218211, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_horizontalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_set_horizontalNormalizedPosition_m1511_MethodInfo = 
{
	"set_horizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_horizontalNormalizedPosition_m1511/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_horizontalNormalizedPosition_m1511_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 808/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::get_verticalNormalizedPosition()
extern const MethodInfo ScrollRect_get_verticalNormalizedPosition_m1512_MethodInfo = 
{
	"get_verticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_get_verticalNormalizedPosition_m1512/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 809/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_set_verticalNormalizedPosition_m1513_ParameterInfos[] = 
{
	{"value", 0, 134218212, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::set_verticalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_set_verticalNormalizedPosition_m1513_MethodInfo = 
{
	"set_verticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_set_verticalNormalizedPosition_m1513/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, ScrollRect_t333_ScrollRect_set_verticalNormalizedPosition_m1513_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 810/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_SetHorizontalNormalizedPosition_m1514_ParameterInfos[] = 
{
	{"value", 0, 134218213, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetHorizontalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_SetHorizontalNormalizedPosition_m1514_MethodInfo = 
{
	"SetHorizontalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetHorizontalNormalizedPosition_m1514/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, ScrollRect_t333_ScrollRect_SetHorizontalNormalizedPosition_m1514_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 811/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_SetVerticalNormalizedPosition_m1515_ParameterInfos[] = 
{
	{"value", 0, 134218214, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetVerticalNormalizedPosition(System.Single)
extern const MethodInfo ScrollRect_SetVerticalNormalizedPosition_m1515_MethodInfo = 
{
	"SetVerticalNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetVerticalNormalizedPosition_m1515/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, ScrollRect_t333_ScrollRect_SetVerticalNormalizedPosition_m1515_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 812/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_SetNormalizedPosition_m1516_ParameterInfos[] = 
{
	{"value", 0, 134218215, 0, &Single_t151_0_0_0},
	{"axis", 1, 134218216, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::SetNormalizedPosition(System.Single,System.Int32)
extern const MethodInfo ScrollRect_SetNormalizedPosition_m1516_MethodInfo = 
{
	"SetNormalizedPosition"/* name */
	, (methodPointerType)&ScrollRect_SetNormalizedPosition_m1516/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151_Int32_t127/* invoker_method */
	, ScrollRect_t333_ScrollRect_SetNormalizedPosition_m1516_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 813/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_RubberDelta_m1517_ParameterInfos[] = 
{
	{"overStretching", 0, 134218217, 0, &Single_t151_0_0_0},
	{"viewSize", 1, 134218218, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ScrollRect::RubberDelta(System.Single,System.Single)
extern const MethodInfo ScrollRect_RubberDelta_m1517_MethodInfo = 
{
	"RubberDelta"/* name */
	, (methodPointerType)&ScrollRect_RubberDelta_m1517/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Single_t151_Single_t151/* invoker_method */
	, ScrollRect_t333_ScrollRect_RubberDelta_m1517_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 814/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ScrollRect::UpdateBounds()
extern const MethodInfo ScrollRect_UpdateBounds_m1518_MethodInfo = 
{
	"UpdateBounds"/* name */
	, (methodPointerType)&ScrollRect_UpdateBounds_m1518/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 815/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Bounds_t334_0_0_0;
extern void* RuntimeInvoker_Bounds_t334 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Bounds UnityEngine.UI.ScrollRect::GetBounds()
extern const MethodInfo ScrollRect_GetBounds_m1519_MethodInfo = 
{
	"GetBounds"/* name */
	, (methodPointerType)&ScrollRect_GetBounds_m1519/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Bounds_t334_0_0_0/* return_type */
	, RuntimeInvoker_Bounds_t334/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 816/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo ScrollRect_t333_ScrollRect_CalculateOffset_m1520_ParameterInfos[] = 
{
	{"delta", 0, 134218219, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t10_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.ScrollRect::CalculateOffset(UnityEngine.Vector2)
extern const MethodInfo ScrollRect_CalculateOffset_m1520_MethodInfo = 
{
	"CalculateOffset"/* name */
	, (methodPointerType)&ScrollRect_CalculateOffset_m1520/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10_Vector2_t10/* invoker_method */
	, ScrollRect_t333_ScrollRect_CalculateOffset_m1520_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 817/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ScrollRect::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1521_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1521/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 818/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transform_t11_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.ScrollRect::UnityEngine.UI.ICanvasElement.get_transform()
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1522_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1522/* method */
	, &ScrollRect_t333_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 819/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ScrollRect_t333_MethodInfos[] =
{
	&ScrollRect__ctor_m1467_MethodInfo,
	&ScrollRect_get_content_m1468_MethodInfo,
	&ScrollRect_set_content_m1469_MethodInfo,
	&ScrollRect_get_horizontal_m1470_MethodInfo,
	&ScrollRect_set_horizontal_m1471_MethodInfo,
	&ScrollRect_get_vertical_m1472_MethodInfo,
	&ScrollRect_set_vertical_m1473_MethodInfo,
	&ScrollRect_get_movementType_m1474_MethodInfo,
	&ScrollRect_set_movementType_m1475_MethodInfo,
	&ScrollRect_get_elasticity_m1476_MethodInfo,
	&ScrollRect_set_elasticity_m1477_MethodInfo,
	&ScrollRect_get_inertia_m1478_MethodInfo,
	&ScrollRect_set_inertia_m1479_MethodInfo,
	&ScrollRect_get_decelerationRate_m1480_MethodInfo,
	&ScrollRect_set_decelerationRate_m1481_MethodInfo,
	&ScrollRect_get_scrollSensitivity_m1482_MethodInfo,
	&ScrollRect_set_scrollSensitivity_m1483_MethodInfo,
	&ScrollRect_get_horizontalScrollbar_m1484_MethodInfo,
	&ScrollRect_set_horizontalScrollbar_m1485_MethodInfo,
	&ScrollRect_get_verticalScrollbar_m1486_MethodInfo,
	&ScrollRect_set_verticalScrollbar_m1487_MethodInfo,
	&ScrollRect_get_onValueChanged_m1488_MethodInfo,
	&ScrollRect_set_onValueChanged_m1489_MethodInfo,
	&ScrollRect_get_viewRect_m1490_MethodInfo,
	&ScrollRect_get_velocity_m1491_MethodInfo,
	&ScrollRect_set_velocity_m1492_MethodInfo,
	&ScrollRect_Rebuild_m1493_MethodInfo,
	&ScrollRect_OnEnable_m1494_MethodInfo,
	&ScrollRect_OnDisable_m1495_MethodInfo,
	&ScrollRect_IsActive_m1496_MethodInfo,
	&ScrollRect_EnsureLayoutHasRebuilt_m1497_MethodInfo,
	&ScrollRect_StopMovement_m1498_MethodInfo,
	&ScrollRect_OnScroll_m1499_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m1500_MethodInfo,
	&ScrollRect_OnBeginDrag_m1501_MethodInfo,
	&ScrollRect_OnEndDrag_m1502_MethodInfo,
	&ScrollRect_OnDrag_m1503_MethodInfo,
	&ScrollRect_SetContentAnchoredPosition_m1504_MethodInfo,
	&ScrollRect_LateUpdate_m1505_MethodInfo,
	&ScrollRect_UpdatePrevData_m1506_MethodInfo,
	&ScrollRect_UpdateScrollbars_m1507_MethodInfo,
	&ScrollRect_get_normalizedPosition_m1508_MethodInfo,
	&ScrollRect_set_normalizedPosition_m1509_MethodInfo,
	&ScrollRect_get_horizontalNormalizedPosition_m1510_MethodInfo,
	&ScrollRect_set_horizontalNormalizedPosition_m1511_MethodInfo,
	&ScrollRect_get_verticalNormalizedPosition_m1512_MethodInfo,
	&ScrollRect_set_verticalNormalizedPosition_m1513_MethodInfo,
	&ScrollRect_SetHorizontalNormalizedPosition_m1514_MethodInfo,
	&ScrollRect_SetVerticalNormalizedPosition_m1515_MethodInfo,
	&ScrollRect_SetNormalizedPosition_m1516_MethodInfo,
	&ScrollRect_RubberDelta_m1517_MethodInfo,
	&ScrollRect_UpdateBounds_m1518_MethodInfo,
	&ScrollRect_GetBounds_m1519_MethodInfo,
	&ScrollRect_CalculateOffset_m1520_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1521_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1522_MethodInfo,
	NULL
};
extern const MethodInfo ScrollRect_get_content_m1468_MethodInfo;
extern const MethodInfo ScrollRect_set_content_m1469_MethodInfo;
static const PropertyInfo ScrollRect_t333____content_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "content"/* name */
	, &ScrollRect_get_content_m1468_MethodInfo/* get */
	, &ScrollRect_set_content_m1469_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_horizontal_m1470_MethodInfo;
extern const MethodInfo ScrollRect_set_horizontal_m1471_MethodInfo;
static const PropertyInfo ScrollRect_t333____horizontal_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "horizontal"/* name */
	, &ScrollRect_get_horizontal_m1470_MethodInfo/* get */
	, &ScrollRect_set_horizontal_m1471_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_vertical_m1472_MethodInfo;
extern const MethodInfo ScrollRect_set_vertical_m1473_MethodInfo;
static const PropertyInfo ScrollRect_t333____vertical_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "vertical"/* name */
	, &ScrollRect_get_vertical_m1472_MethodInfo/* get */
	, &ScrollRect_set_vertical_m1473_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_movementType_m1474_MethodInfo;
extern const MethodInfo ScrollRect_set_movementType_m1475_MethodInfo;
static const PropertyInfo ScrollRect_t333____movementType_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "movementType"/* name */
	, &ScrollRect_get_movementType_m1474_MethodInfo/* get */
	, &ScrollRect_set_movementType_m1475_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_elasticity_m1476_MethodInfo;
extern const MethodInfo ScrollRect_set_elasticity_m1477_MethodInfo;
static const PropertyInfo ScrollRect_t333____elasticity_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "elasticity"/* name */
	, &ScrollRect_get_elasticity_m1476_MethodInfo/* get */
	, &ScrollRect_set_elasticity_m1477_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_inertia_m1478_MethodInfo;
extern const MethodInfo ScrollRect_set_inertia_m1479_MethodInfo;
static const PropertyInfo ScrollRect_t333____inertia_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "inertia"/* name */
	, &ScrollRect_get_inertia_m1478_MethodInfo/* get */
	, &ScrollRect_set_inertia_m1479_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_decelerationRate_m1480_MethodInfo;
extern const MethodInfo ScrollRect_set_decelerationRate_m1481_MethodInfo;
static const PropertyInfo ScrollRect_t333____decelerationRate_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "decelerationRate"/* name */
	, &ScrollRect_get_decelerationRate_m1480_MethodInfo/* get */
	, &ScrollRect_set_decelerationRate_m1481_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_scrollSensitivity_m1482_MethodInfo;
extern const MethodInfo ScrollRect_set_scrollSensitivity_m1483_MethodInfo;
static const PropertyInfo ScrollRect_t333____scrollSensitivity_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "scrollSensitivity"/* name */
	, &ScrollRect_get_scrollSensitivity_m1482_MethodInfo/* get */
	, &ScrollRect_set_scrollSensitivity_m1483_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_horizontalScrollbar_m1484_MethodInfo;
extern const MethodInfo ScrollRect_set_horizontalScrollbar_m1485_MethodInfo;
static const PropertyInfo ScrollRect_t333____horizontalScrollbar_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "horizontalScrollbar"/* name */
	, &ScrollRect_get_horizontalScrollbar_m1484_MethodInfo/* get */
	, &ScrollRect_set_horizontalScrollbar_m1485_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_verticalScrollbar_m1486_MethodInfo;
extern const MethodInfo ScrollRect_set_verticalScrollbar_m1487_MethodInfo;
static const PropertyInfo ScrollRect_t333____verticalScrollbar_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "verticalScrollbar"/* name */
	, &ScrollRect_get_verticalScrollbar_m1486_MethodInfo/* get */
	, &ScrollRect_set_verticalScrollbar_m1487_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_onValueChanged_m1488_MethodInfo;
extern const MethodInfo ScrollRect_set_onValueChanged_m1489_MethodInfo;
static const PropertyInfo ScrollRect_t333____onValueChanged_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "onValueChanged"/* name */
	, &ScrollRect_get_onValueChanged_m1488_MethodInfo/* get */
	, &ScrollRect_set_onValueChanged_m1489_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_viewRect_m1490_MethodInfo;
static const PropertyInfo ScrollRect_t333____viewRect_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "viewRect"/* name */
	, &ScrollRect_get_viewRect_m1490_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_velocity_m1491_MethodInfo;
extern const MethodInfo ScrollRect_set_velocity_m1492_MethodInfo;
static const PropertyInfo ScrollRect_t333____velocity_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "velocity"/* name */
	, &ScrollRect_get_velocity_m1491_MethodInfo/* get */
	, &ScrollRect_set_velocity_m1492_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_normalizedPosition_m1508_MethodInfo;
extern const MethodInfo ScrollRect_set_normalizedPosition_m1509_MethodInfo;
static const PropertyInfo ScrollRect_t333____normalizedPosition_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "normalizedPosition"/* name */
	, &ScrollRect_get_normalizedPosition_m1508_MethodInfo/* get */
	, &ScrollRect_set_normalizedPosition_m1509_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_horizontalNormalizedPosition_m1510_MethodInfo;
extern const MethodInfo ScrollRect_set_horizontalNormalizedPosition_m1511_MethodInfo;
static const PropertyInfo ScrollRect_t333____horizontalNormalizedPosition_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "horizontalNormalizedPosition"/* name */
	, &ScrollRect_get_horizontalNormalizedPosition_m1510_MethodInfo/* get */
	, &ScrollRect_set_horizontalNormalizedPosition_m1511_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ScrollRect_get_verticalNormalizedPosition_m1512_MethodInfo;
extern const MethodInfo ScrollRect_set_verticalNormalizedPosition_m1513_MethodInfo;
static const PropertyInfo ScrollRect_t333____verticalNormalizedPosition_PropertyInfo = 
{
	&ScrollRect_t333_il2cpp_TypeInfo/* parent */
	, "verticalNormalizedPosition"/* name */
	, &ScrollRect_get_verticalNormalizedPosition_m1512_MethodInfo/* get */
	, &ScrollRect_set_verticalNormalizedPosition_m1513_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ScrollRect_t333_PropertyInfos[] =
{
	&ScrollRect_t333____content_PropertyInfo,
	&ScrollRect_t333____horizontal_PropertyInfo,
	&ScrollRect_t333____vertical_PropertyInfo,
	&ScrollRect_t333____movementType_PropertyInfo,
	&ScrollRect_t333____elasticity_PropertyInfo,
	&ScrollRect_t333____inertia_PropertyInfo,
	&ScrollRect_t333____decelerationRate_PropertyInfo,
	&ScrollRect_t333____scrollSensitivity_PropertyInfo,
	&ScrollRect_t333____horizontalScrollbar_PropertyInfo,
	&ScrollRect_t333____verticalScrollbar_PropertyInfo,
	&ScrollRect_t333____onValueChanged_PropertyInfo,
	&ScrollRect_t333____viewRect_PropertyInfo,
	&ScrollRect_t333____velocity_PropertyInfo,
	&ScrollRect_t333____normalizedPosition_PropertyInfo,
	&ScrollRect_t333____horizontalNormalizedPosition_PropertyInfo,
	&ScrollRect_t333____verticalNormalizedPosition_PropertyInfo,
	NULL
};
static const Il2CppType* ScrollRect_t333_il2cpp_TypeInfo__nestedTypes[2] =
{
	&MovementType_t330_0_0_0,
	&ScrollRectEvent_t331_0_0_0,
};
extern const MethodInfo Object_Equals_m537_MethodInfo;
extern const MethodInfo Object_GetHashCode_m538_MethodInfo;
extern const MethodInfo Object_ToString_m539_MethodInfo;
extern const MethodInfo UIBehaviour_Awake_m852_MethodInfo;
extern const MethodInfo ScrollRect_OnEnable_m1494_MethodInfo;
extern const MethodInfo UIBehaviour_Start_m854_MethodInfo;
extern const MethodInfo ScrollRect_OnDisable_m1495_MethodInfo;
extern const MethodInfo UIBehaviour_OnDestroy_m856_MethodInfo;
extern const MethodInfo ScrollRect_IsActive_m1496_MethodInfo;
extern const MethodInfo UIBehaviour_OnRectTransformDimensionsChange_m858_MethodInfo;
extern const MethodInfo UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo;
extern const MethodInfo UIBehaviour_OnTransformParentChanged_m860_MethodInfo;
extern const MethodInfo UIBehaviour_OnDidApplyAnimationProperties_m861_MethodInfo;
extern const MethodInfo UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo;
extern const MethodInfo UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo;
extern const MethodInfo ScrollRect_OnBeginDrag_m1501_MethodInfo;
extern const MethodInfo ScrollRect_OnInitializePotentialDrag_m1500_MethodInfo;
extern const MethodInfo ScrollRect_OnDrag_m1503_MethodInfo;
extern const MethodInfo ScrollRect_OnEndDrag_m1502_MethodInfo;
extern const MethodInfo ScrollRect_OnScroll_m1499_MethodInfo;
extern const MethodInfo ScrollRect_Rebuild_m1493_MethodInfo;
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1522_MethodInfo;
extern const MethodInfo ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1521_MethodInfo;
extern const MethodInfo ScrollRect_StopMovement_m1498_MethodInfo;
extern const MethodInfo ScrollRect_SetContentAnchoredPosition_m1504_MethodInfo;
extern const MethodInfo ScrollRect_LateUpdate_m1505_MethodInfo;
static const Il2CppMethodReference ScrollRect_t333_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&ScrollRect_OnEnable_m1494_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&ScrollRect_OnDisable_m1495_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&ScrollRect_IsActive_m1496_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m858_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m861_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&ScrollRect_OnBeginDrag_m1501_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m1500_MethodInfo,
	&ScrollRect_OnDrag_m1503_MethodInfo,
	&ScrollRect_OnEndDrag_m1502_MethodInfo,
	&ScrollRect_OnScroll_m1499_MethodInfo,
	&ScrollRect_Rebuild_m1493_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1522_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1521_MethodInfo,
	&ScrollRect_Rebuild_m1493_MethodInfo,
	&ScrollRect_StopMovement_m1498_MethodInfo,
	&ScrollRect_OnScroll_m1499_MethodInfo,
	&ScrollRect_OnInitializePotentialDrag_m1500_MethodInfo,
	&ScrollRect_OnBeginDrag_m1501_MethodInfo,
	&ScrollRect_OnEndDrag_m1502_MethodInfo,
	&ScrollRect_OnDrag_m1503_MethodInfo,
	&ScrollRect_SetContentAnchoredPosition_m1504_MethodInfo,
	&ScrollRect_LateUpdate_m1505_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1521_MethodInfo,
	&ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1522_MethodInfo,
};
static bool ScrollRect_t333_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEventSystemHandler_t490_0_0_0;
extern const Il2CppType IBeginDragHandler_t396_0_0_0;
extern const Il2CppType IInitializePotentialDragHandler_t395_0_0_0;
extern const Il2CppType IDragHandler_t397_0_0_0;
extern const Il2CppType IEndDragHandler_t398_0_0_0;
extern const Il2CppType IScrollHandler_t400_0_0_0;
extern const Il2CppType ICanvasElement_t411_0_0_0;
static const Il2CppType* ScrollRect_t333_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t490_0_0_0,
	&IBeginDragHandler_t396_0_0_0,
	&IInitializePotentialDragHandler_t395_0_0_0,
	&IDragHandler_t397_0_0_0,
	&IEndDragHandler_t398_0_0_0,
	&IScrollHandler_t400_0_0_0,
	&ICanvasElement_t411_0_0_0,
};
static Il2CppInterfaceOffsetPair ScrollRect_t333_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t490_0_0_0, 16},
	{ &IBeginDragHandler_t396_0_0_0, 16},
	{ &IInitializePotentialDragHandler_t395_0_0_0, 17},
	{ &IDragHandler_t397_0_0_0, 18},
	{ &IEndDragHandler_t398_0_0_0, 19},
	{ &IScrollHandler_t400_0_0_0, 20},
	{ &ICanvasElement_t411_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScrollRect_t333_1_0_0;
extern const Il2CppType UIBehaviour_t199_0_0_0;
struct ScrollRect_t333;
const Il2CppTypeDefinitionMetadata ScrollRect_t333_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ScrollRect_t333_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ScrollRect_t333_InterfacesTypeInfos/* implementedInterfaces */
	, ScrollRect_t333_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t199_0_0_0/* parent */
	, ScrollRect_t333_VTable/* vtableMethods */
	, ScrollRect_t333_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 418/* fieldStart */

};
TypeInfo ScrollRect_t333_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScrollRect"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ScrollRect_t333_MethodInfos/* methods */
	, ScrollRect_t333_PropertyInfos/* properties */
	, NULL/* events */
	, &ScrollRect_t333_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 213/* custom_attributes_cache */
	, &ScrollRect_t333_0_0_0/* byval_arg */
	, &ScrollRect_t333_1_0_0/* this_arg */
	, &ScrollRect_t333_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScrollRect_t333)/* instance_size */
	, sizeof (ScrollRect_t333)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 56/* method_count */
	, 16/* property_count */
	, 23/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 35/* vtable_count */
	, 7/* interfaces_count */
	, 7/* interface_offsets_count */

};
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
// Metadata Definition UnityEngine.UI.Selectable/Transition
extern TypeInfo Transition_t335_il2cpp_TypeInfo;
// UnityEngine.UI.Selectable/Transition
#include "UnityEngine_UI_UnityEngine_UI_Selectable_TransitionMethodDeclarations.h"
static const MethodInfo* Transition_t335_MethodInfos[] =
{
	NULL
};
extern const MethodInfo Enum_Equals_m514_MethodInfo;
extern const MethodInfo Enum_GetHashCode_m516_MethodInfo;
extern const MethodInfo Enum_ToString_m517_MethodInfo;
extern const MethodInfo Enum_ToString_m518_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToBoolean_m519_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToByte_m520_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToChar_m521_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDateTime_m522_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDecimal_m523_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToDouble_m524_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt16_m525_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt32_m526_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToInt64_m527_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSByte_m528_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToSingle_m529_MethodInfo;
extern const MethodInfo Enum_ToString_m530_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToType_m531_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt16_m532_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt32_m533_MethodInfo;
extern const MethodInfo Enum_System_IConvertible_ToUInt64_m534_MethodInfo;
extern const MethodInfo Enum_CompareTo_m535_MethodInfo;
extern const MethodInfo Enum_GetTypeCode_m536_MethodInfo;
static const Il2CppMethodReference Transition_t335_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool Transition_t335_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IFormattable_t164_0_0_0;
extern const Il2CppType IConvertible_t165_0_0_0;
extern const Il2CppType IComparable_t166_0_0_0;
static Il2CppInterfaceOffsetPair Transition_t335_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Transition_t335_0_0_0;
extern const Il2CppType Transition_t335_1_0_0;
extern const Il2CppType Enum_t167_0_0_0;
extern TypeInfo Selectable_t259_il2cpp_TypeInfo;
extern const Il2CppType Selectable_t259_0_0_0;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t127_il2cpp_TypeInfo;
const Il2CppTypeDefinitionMetadata Transition_t335_DefinitionMetadata = 
{
	&Selectable_t259_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Transition_t335_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, Transition_t335_VTable/* vtableMethods */
	, Transition_t335_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 441/* fieldStart */

};
TypeInfo Transition_t335_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Transition"/* name */
	, ""/* namespaze */
	, Transition_t335_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Transition_t335_0_0_0/* byval_arg */
	, &Transition_t335_1_0_0/* this_arg */
	, &Transition_t335_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Transition_t335)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Transition_t335)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
// Metadata Definition UnityEngine.UI.Selectable/SelectionState
extern TypeInfo SelectionState_t336_il2cpp_TypeInfo;
// UnityEngine.UI.Selectable/SelectionState
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionStateMethodDeclarations.h"
static const MethodInfo* SelectionState_t336_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference SelectionState_t336_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool SelectionState_t336_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair SelectionState_t336_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SelectionState_t336_0_0_0;
extern const Il2CppType SelectionState_t336_1_0_0;
const Il2CppTypeDefinitionMetadata SelectionState_t336_DefinitionMetadata = 
{
	&Selectable_t259_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SelectionState_t336_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, SelectionState_t336_VTable/* vtableMethods */
	, SelectionState_t336_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 446/* fieldStart */

};
TypeInfo SelectionState_t336_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SelectionState"/* name */
	, ""/* namespaze */
	, SelectionState_t336_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SelectionState_t336_0_0_0/* byval_arg */
	, &SelectionState_t336_1_0_0/* this_arg */
	, &SelectionState_t336_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SelectionState_t336)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SelectionState_t336)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 260/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_Selectable.h"
// Metadata Definition UnityEngine.UI.Selectable
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_SelectableMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::.ctor()
extern const MethodInfo Selectable__ctor_m1523_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Selectable__ctor_m1523/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 821/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::.cctor()
extern const MethodInfo Selectable__cctor_m1524_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Selectable__cctor_m1524/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 822/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t337_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.UI.Selectable> UnityEngine.UI.Selectable::get_allSelectables()
extern const MethodInfo Selectable_get_allSelectables_m1525_MethodInfo = 
{
	"get_allSelectables"/* name */
	, (methodPointerType)&Selectable_get_allSelectables_m1525/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t337_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 823/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Navigation_t320_0_0_0;
extern void* RuntimeInvoker_Navigation_t320 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::get_navigation()
extern const MethodInfo Selectable_get_navigation_m1526_MethodInfo = 
{
	"get_navigation"/* name */
	, (methodPointerType)&Selectable_get_navigation_m1526/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Navigation_t320_0_0_0/* return_type */
	, RuntimeInvoker_Navigation_t320/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 824/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Navigation_t320_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_set_navigation_m1527_ParameterInfos[] = 
{
	{"value", 0, 134218220, 0, &Navigation_t320_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Navigation_t320 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_navigation(UnityEngine.UI.Navigation)
extern const MethodInfo Selectable_set_navigation_m1527_MethodInfo = 
{
	"set_navigation"/* name */
	, (methodPointerType)&Selectable_set_navigation_m1527/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Navigation_t320/* invoker_method */
	, Selectable_t259_Selectable_set_navigation_m1527_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 825/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Transition_t335 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::get_transition()
extern const MethodInfo Selectable_get_transition_m1528_MethodInfo = 
{
	"get_transition"/* name */
	, (methodPointerType)&Selectable_get_transition_m1528/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Transition_t335_0_0_0/* return_type */
	, RuntimeInvoker_Transition_t335/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 826/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Transition_t335_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_set_transition_m1529_ParameterInfos[] = 
{
	{"value", 0, 134218221, 0, &Transition_t335_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_transition(UnityEngine.UI.Selectable/Transition)
extern const MethodInfo Selectable_set_transition_m1529_MethodInfo = 
{
	"set_transition"/* name */
	, (methodPointerType)&Selectable_set_transition_m1529/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Selectable_t259_Selectable_set_transition_m1529_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 827/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorBlock_t265_0_0_0;
extern void* RuntimeInvoker_ColorBlock_t265 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::get_colors()
extern const MethodInfo Selectable_get_colors_m1530_MethodInfo = 
{
	"get_colors"/* name */
	, (methodPointerType)&Selectable_get_colors_m1530/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &ColorBlock_t265_0_0_0/* return_type */
	, RuntimeInvoker_ColorBlock_t265/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 828/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ColorBlock_t265_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_set_colors_m1531_ParameterInfos[] = 
{
	{"value", 0, 134218222, 0, &ColorBlock_t265_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_ColorBlock_t265 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_colors(UnityEngine.UI.ColorBlock)
extern const MethodInfo Selectable_set_colors_m1531_MethodInfo = 
{
	"set_colors"/* name */
	, (methodPointerType)&Selectable_set_colors_m1531/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_ColorBlock_t265/* invoker_method */
	, Selectable_t259_Selectable_set_colors_m1531_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 829/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SpriteState_t339_0_0_0;
extern void* RuntimeInvoker_SpriteState_t339 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::get_spriteState()
extern const MethodInfo Selectable_get_spriteState_m1532_MethodInfo = 
{
	"get_spriteState"/* name */
	, (methodPointerType)&Selectable_get_spriteState_m1532/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &SpriteState_t339_0_0_0/* return_type */
	, RuntimeInvoker_SpriteState_t339/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 830/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SpriteState_t339_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_set_spriteState_m1533_ParameterInfos[] = 
{
	{"value", 0, 134218223, 0, &SpriteState_t339_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SpriteState_t339 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_spriteState(UnityEngine.UI.SpriteState)
extern const MethodInfo Selectable_set_spriteState_m1533_MethodInfo = 
{
	"set_spriteState"/* name */
	, (methodPointerType)&Selectable_set_spriteState_m1533/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SpriteState_t339/* invoker_method */
	, Selectable_t259_Selectable_set_spriteState_m1533_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 831/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AnimationTriggers_t254_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::get_animationTriggers()
extern const MethodInfo Selectable_get_animationTriggers_m1534_MethodInfo = 
{
	"get_animationTriggers"/* name */
	, (methodPointerType)&Selectable_get_animationTriggers_m1534/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &AnimationTriggers_t254_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 832/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AnimationTriggers_t254_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_set_animationTriggers_m1535_ParameterInfos[] = 
{
	{"value", 0, 134218224, 0, &AnimationTriggers_t254_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_animationTriggers(UnityEngine.UI.AnimationTriggers)
extern const MethodInfo Selectable_set_animationTriggers_m1535_MethodInfo = 
{
	"set_animationTriggers"/* name */
	, (methodPointerType)&Selectable_set_animationTriggers_m1535/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Selectable_t259_Selectable_set_animationTriggers_m1535_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 833/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Graphic_t278_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::get_targetGraphic()
extern const MethodInfo Selectable_get_targetGraphic_m1536_MethodInfo = 
{
	"get_targetGraphic"/* name */
	, (methodPointerType)&Selectable_get_targetGraphic_m1536/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t278_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 834/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Graphic_t278_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_set_targetGraphic_m1537_ParameterInfos[] = 
{
	{"value", 0, 134218225, 0, &Graphic_t278_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_targetGraphic(UnityEngine.UI.Graphic)
extern const MethodInfo Selectable_set_targetGraphic_m1537_MethodInfo = 
{
	"set_targetGraphic"/* name */
	, (methodPointerType)&Selectable_set_targetGraphic_m1537/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Selectable_t259_Selectable_set_targetGraphic_m1537_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 835/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_interactable()
extern const MethodInfo Selectable_get_interactable_m1538_MethodInfo = 
{
	"get_interactable"/* name */
	, (methodPointerType)&Selectable_get_interactable_m1538/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 836/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_set_interactable_m1539_ParameterInfos[] = 
{
	{"value", 0, 134218226, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_interactable(System.Boolean)
extern const MethodInfo Selectable_set_interactable_m1539_MethodInfo = 
{
	"set_interactable"/* name */
	, (methodPointerType)&Selectable_set_interactable_m1539/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Selectable_t259_Selectable_set_interactable_m1539_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 837/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_isPointerInside()
extern const MethodInfo Selectable_get_isPointerInside_m1540_MethodInfo = 
{
	"get_isPointerInside"/* name */
	, (methodPointerType)&Selectable_get_isPointerInside_m1540/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 236/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 838/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_set_isPointerInside_m1541_ParameterInfos[] = 
{
	{"value", 0, 134218227, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_isPointerInside(System.Boolean)
extern const MethodInfo Selectable_set_isPointerInside_m1541_MethodInfo = 
{
	"set_isPointerInside"/* name */
	, (methodPointerType)&Selectable_set_isPointerInside_m1541/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Selectable_t259_Selectable_set_isPointerInside_m1541_ParameterInfos/* parameters */
	, 237/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 839/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_isPointerDown()
extern const MethodInfo Selectable_get_isPointerDown_m1542_MethodInfo = 
{
	"get_isPointerDown"/* name */
	, (methodPointerType)&Selectable_get_isPointerDown_m1542/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 238/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 840/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_set_isPointerDown_m1543_ParameterInfos[] = 
{
	{"value", 0, 134218228, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_isPointerDown(System.Boolean)
extern const MethodInfo Selectable_set_isPointerDown_m1543_MethodInfo = 
{
	"set_isPointerDown"/* name */
	, (methodPointerType)&Selectable_set_isPointerDown_m1543/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Selectable_t259_Selectable_set_isPointerDown_m1543_ParameterInfos/* parameters */
	, 239/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 841/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::get_hasSelection()
extern const MethodInfo Selectable_get_hasSelection_m1544_MethodInfo = 
{
	"get_hasSelection"/* name */
	, (methodPointerType)&Selectable_get_hasSelection_m1544/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 240/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 842/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_set_hasSelection_m1545_ParameterInfos[] = 
{
	{"value", 0, 134218229, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_hasSelection(System.Boolean)
extern const MethodInfo Selectable_set_hasSelection_m1545_MethodInfo = 
{
	"set_hasSelection"/* name */
	, (methodPointerType)&Selectable_set_hasSelection_m1545/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Selectable_t259_Selectable_set_hasSelection_m1545_ParameterInfos/* parameters */
	, 241/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 843/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Image_t294_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Image UnityEngine.UI.Selectable::get_image()
extern const MethodInfo Selectable_get_image_m1546_MethodInfo = 
{
	"get_image"/* name */
	, (methodPointerType)&Selectable_get_image_m1546/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Image_t294_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 844/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Image_t294_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_set_image_m1547_ParameterInfos[] = 
{
	{"value", 0, 134218230, 0, &Image_t294_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::set_image(UnityEngine.UI.Image)
extern const MethodInfo Selectable_set_image_m1547_MethodInfo = 
{
	"set_image"/* name */
	, (methodPointerType)&Selectable_set_image_m1547/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Selectable_t259_Selectable_set_image_m1547_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 845/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Animator_t415_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Animator UnityEngine.UI.Selectable::get_animator()
extern const MethodInfo Selectable_get_animator_m1548_MethodInfo = 
{
	"get_animator"/* name */
	, (methodPointerType)&Selectable_get_animator_m1548/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Animator_t415_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 846/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Awake()
extern const MethodInfo Selectable_Awake_m1549_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&Selectable_Awake_m1549/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 847/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnCanvasGroupChanged()
extern const MethodInfo Selectable_OnCanvasGroupChanged_m1550_MethodInfo = 
{
	"OnCanvasGroupChanged"/* name */
	, (methodPointerType)&Selectable_OnCanvasGroupChanged_m1550/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 848/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsInteractable()
extern const MethodInfo Selectable_IsInteractable_m1551_MethodInfo = 
{
	"IsInteractable"/* name */
	, (methodPointerType)&Selectable_IsInteractable_m1551/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 849/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDidApplyAnimationProperties()
extern const MethodInfo Selectable_OnDidApplyAnimationProperties_m1552_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&Selectable_OnDidApplyAnimationProperties_m1552/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 850/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnEnable()
extern const MethodInfo Selectable_OnEnable_m1553_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Selectable_OnEnable_m1553/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 851/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnSetProperty()
extern const MethodInfo Selectable_OnSetProperty_m1554_MethodInfo = 
{
	"OnSetProperty"/* name */
	, (methodPointerType)&Selectable_OnSetProperty_m1554/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 852/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDisable()
extern const MethodInfo Selectable_OnDisable_m1555_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Selectable_OnDisable_m1555/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 853/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_SelectionState_t336 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable/SelectionState UnityEngine.UI.Selectable::get_currentSelectionState()
extern const MethodInfo Selectable_get_currentSelectionState_m1556_MethodInfo = 
{
	"get_currentSelectionState"/* name */
	, (methodPointerType)&Selectable_get_currentSelectionState_m1556/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &SelectionState_t336_0_0_0/* return_type */
	, RuntimeInvoker_SelectionState_t336/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 854/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::InstantClearState()
extern const MethodInfo Selectable_InstantClearState_m1557_MethodInfo = 
{
	"InstantClearState"/* name */
	, (methodPointerType)&Selectable_InstantClearState_m1557/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 855/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SelectionState_t336_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_DoStateTransition_m1558_ParameterInfos[] = 
{
	{"state", 0, 134218231, 0, &SelectionState_t336_0_0_0},
	{"instant", 1, 134218232, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::DoStateTransition(UnityEngine.UI.Selectable/SelectionState,System.Boolean)
extern const MethodInfo Selectable_DoStateTransition_m1558_MethodInfo = 
{
	"DoStateTransition"/* name */
	, (methodPointerType)&Selectable_DoStateTransition_m1558/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_SByte_t170/* invoker_method */
	, Selectable_t259_Selectable_DoStateTransition_m1558_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 856/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector3_t15_0_0_0;
extern const Il2CppType Vector3_t15_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_FindSelectable_m1559_ParameterInfos[] = 
{
	{"dir", 0, 134218233, 0, &Vector3_t15_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Vector3_t15 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectable(UnityEngine.Vector3)
extern const MethodInfo Selectable_FindSelectable_m1559_MethodInfo = 
{
	"FindSelectable"/* name */
	, (methodPointerType)&Selectable_FindSelectable_m1559/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t259_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Vector3_t15/* invoker_method */
	, Selectable_t259_Selectable_FindSelectable_m1559_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 857/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_GetPointOnRectEdge_m1560_ParameterInfos[] = 
{
	{"rect", 0, 134218234, 0, &RectTransform_t272_0_0_0},
	{"dir", 1, 134218235, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Vector3_t15_Object_t_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 UnityEngine.UI.Selectable::GetPointOnRectEdge(UnityEngine.RectTransform,UnityEngine.Vector2)
extern const MethodInfo Selectable_GetPointOnRectEdge_m1560_MethodInfo = 
{
	"GetPointOnRectEdge"/* name */
	, (methodPointerType)&Selectable_GetPointOnRectEdge_m1560/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t15_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t15_Object_t_Vector2_t10/* invoker_method */
	, Selectable_t259_Selectable_GetPointOnRectEdge_m1560_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 858/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AxisEventData_t232_0_0_0;
extern const Il2CppType AxisEventData_t232_0_0_0;
extern const Il2CppType Selectable_t259_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_Navigate_m1561_ParameterInfos[] = 
{
	{"eventData", 0, 134218236, 0, &AxisEventData_t232_0_0_0},
	{"sel", 1, 134218237, 0, &Selectable_t259_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Navigate(UnityEngine.EventSystems.AxisEventData,UnityEngine.UI.Selectable)
extern const MethodInfo Selectable_Navigate_m1561_MethodInfo = 
{
	"Navigate"/* name */
	, (methodPointerType)&Selectable_Navigate_m1561/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, Selectable_t259_Selectable_Navigate_m1561_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 859/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnLeft()
extern const MethodInfo Selectable_FindSelectableOnLeft_m1562_MethodInfo = 
{
	"FindSelectableOnLeft"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnLeft_m1562/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t259_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 860/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnRight()
extern const MethodInfo Selectable_FindSelectableOnRight_m1563_MethodInfo = 
{
	"FindSelectableOnRight"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnRight_m1563/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t259_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 861/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnUp()
extern const MethodInfo Selectable_FindSelectableOnUp_m1564_MethodInfo = 
{
	"FindSelectableOnUp"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnUp_m1564/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t259_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 862/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Selectable::FindSelectableOnDown()
extern const MethodInfo Selectable_FindSelectableOnDown_m1565_MethodInfo = 
{
	"FindSelectableOnDown"/* name */
	, (methodPointerType)&Selectable_FindSelectableOnDown_m1565/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t259_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 863/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AxisEventData_t232_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_OnMove_m1566_ParameterInfos[] = 
{
	{"eventData", 0, 134218238, 0, &AxisEventData_t232_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnMove(UnityEngine.EventSystems.AxisEventData)
extern const MethodInfo Selectable_OnMove_m1566_MethodInfo = 
{
	"OnMove"/* name */
	, (methodPointerType)&Selectable_OnMove_m1566/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Selectable_t259_Selectable_OnMove_m1566_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 864/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color_t90_0_0_0;
extern const Il2CppType Color_t90_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_StartColorTween_m1567_ParameterInfos[] = 
{
	{"targetColor", 0, 134218239, 0, &Color_t90_0_0_0},
	{"instant", 1, 134218240, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Color_t90_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::StartColorTween(UnityEngine.Color,System.Boolean)
extern const MethodInfo Selectable_StartColorTween_m1567_MethodInfo = 
{
	"StartColorTween"/* name */
	, (methodPointerType)&Selectable_StartColorTween_m1567/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Color_t90_SByte_t170/* invoker_method */
	, Selectable_t259_Selectable_StartColorTween_m1567_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 865/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t292_0_0_0;
extern const Il2CppType Sprite_t292_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_DoSpriteSwap_m1568_ParameterInfos[] = 
{
	{"newSprite", 0, 134218241, 0, &Sprite_t292_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::DoSpriteSwap(UnityEngine.Sprite)
extern const MethodInfo Selectable_DoSpriteSwap_m1568_MethodInfo = 
{
	"DoSpriteSwap"/* name */
	, (methodPointerType)&Selectable_DoSpriteSwap_m1568/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Selectable_t259_Selectable_DoSpriteSwap_m1568_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 866/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_TriggerAnimation_m1569_ParameterInfos[] = 
{
	{"triggername", 0, 134218242, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::TriggerAnimation(System.String)
extern const MethodInfo Selectable_TriggerAnimation_m1569_MethodInfo = 
{
	"TriggerAnimation"/* name */
	, (methodPointerType)&Selectable_TriggerAnimation_m1569/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Selectable_t259_Selectable_TriggerAnimation_m1569_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 867/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t197_0_0_0;
extern const Il2CppType BaseEventData_t197_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_IsHighlighted_m1570_ParameterInfos[] = 
{
	{"eventData", 0, 134218243, 0, &BaseEventData_t197_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsHighlighted(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_IsHighlighted_m1570_MethodInfo = 
{
	"IsHighlighted"/* name */
	, (methodPointerType)&Selectable_IsHighlighted_m1570/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Selectable_t259_Selectable_IsHighlighted_m1570_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 868/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t197_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_IsPressed_m1571_ParameterInfos[] = 
{
	{"eventData", 0, 134218244, 0, &BaseEventData_t197_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsPressed(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_IsPressed_m1571_MethodInfo = 
{
	"IsPressed"/* name */
	, (methodPointerType)&Selectable_IsPressed_m1571/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Selectable_t259_Selectable_IsPressed_m1571_ParameterInfos/* parameters */
	, 242/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 869/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Selectable::IsPressed()
extern const MethodInfo Selectable_IsPressed_m1572_MethodInfo = 
{
	"IsPressed"/* name */
	, (methodPointerType)&Selectable_IsPressed_m1572/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 870/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t197_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_UpdateSelectionState_m1573_ParameterInfos[] = 
{
	{"eventData", 0, 134218245, 0, &BaseEventData_t197_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::UpdateSelectionState(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_UpdateSelectionState_m1573_MethodInfo = 
{
	"UpdateSelectionState"/* name */
	, (methodPointerType)&Selectable_UpdateSelectionState_m1573/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Selectable_t259_Selectable_UpdateSelectionState_m1573_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 871/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t197_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_EvaluateAndTransitionToSelectionState_m1574_ParameterInfos[] = 
{
	{"eventData", 0, 134218246, 0, &BaseEventData_t197_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::EvaluateAndTransitionToSelectionState(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_EvaluateAndTransitionToSelectionState_m1574_MethodInfo = 
{
	"EvaluateAndTransitionToSelectionState"/* name */
	, (methodPointerType)&Selectable_EvaluateAndTransitionToSelectionState_m1574/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Selectable_t259_Selectable_EvaluateAndTransitionToSelectionState_m1574_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 872/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_InternalEvaluateAndTransitionToSelectionState_m1575_ParameterInfos[] = 
{
	{"instant", 0, 134218247, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::InternalEvaluateAndTransitionToSelectionState(System.Boolean)
extern const MethodInfo Selectable_InternalEvaluateAndTransitionToSelectionState_m1575_MethodInfo = 
{
	"InternalEvaluateAndTransitionToSelectionState"/* name */
	, (methodPointerType)&Selectable_InternalEvaluateAndTransitionToSelectionState_m1575/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Selectable_t259_Selectable_InternalEvaluateAndTransitionToSelectionState_m1575_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 873/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_OnPointerDown_m1576_ParameterInfos[] = 
{
	{"eventData", 0, 134218248, 0, &PointerEventData_t236_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerDown_m1576_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&Selectable_OnPointerDown_m1576/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Selectable_t259_Selectable_OnPointerDown_m1576_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 874/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_OnPointerUp_m1577_ParameterInfos[] = 
{
	{"eventData", 0, 134218249, 0, &PointerEventData_t236_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerUp_m1577_MethodInfo = 
{
	"OnPointerUp"/* name */
	, (methodPointerType)&Selectable_OnPointerUp_m1577/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Selectable_t259_Selectable_OnPointerUp_m1577_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 875/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_OnPointerEnter_m1578_ParameterInfos[] = 
{
	{"eventData", 0, 134218250, 0, &PointerEventData_t236_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerEnter_m1578_MethodInfo = 
{
	"OnPointerEnter"/* name */
	, (methodPointerType)&Selectable_OnPointerEnter_m1578/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Selectable_t259_Selectable_OnPointerEnter_m1578_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 876/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_OnPointerExit_m1579_ParameterInfos[] = 
{
	{"eventData", 0, 134218251, 0, &PointerEventData_t236_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Selectable_OnPointerExit_m1579_MethodInfo = 
{
	"OnPointerExit"/* name */
	, (methodPointerType)&Selectable_OnPointerExit_m1579/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Selectable_t259_Selectable_OnPointerExit_m1579_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 877/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t197_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_OnSelect_m1580_ParameterInfos[] = 
{
	{"eventData", 0, 134218252, 0, &BaseEventData_t197_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_OnSelect_m1580_MethodInfo = 
{
	"OnSelect"/* name */
	, (methodPointerType)&Selectable_OnSelect_m1580/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Selectable_t259_Selectable_OnSelect_m1580_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 878/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t197_0_0_0;
static const ParameterInfo Selectable_t259_Selectable_OnDeselect_m1581_ParameterInfos[] = 
{
	{"eventData", 0, 134218253, 0, &BaseEventData_t197_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Selectable_OnDeselect_m1581_MethodInfo = 
{
	"OnDeselect"/* name */
	, (methodPointerType)&Selectable_OnDeselect_m1581/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Selectable_t259_Selectable_OnDeselect_m1581_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 879/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Selectable::Select()
extern const MethodInfo Selectable_Select_m1582_MethodInfo = 
{
	"Select"/* name */
	, (methodPointerType)&Selectable_Select_m1582/* method */
	, &Selectable_t259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 880/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Selectable_t259_MethodInfos[] =
{
	&Selectable__ctor_m1523_MethodInfo,
	&Selectable__cctor_m1524_MethodInfo,
	&Selectable_get_allSelectables_m1525_MethodInfo,
	&Selectable_get_navigation_m1526_MethodInfo,
	&Selectable_set_navigation_m1527_MethodInfo,
	&Selectable_get_transition_m1528_MethodInfo,
	&Selectable_set_transition_m1529_MethodInfo,
	&Selectable_get_colors_m1530_MethodInfo,
	&Selectable_set_colors_m1531_MethodInfo,
	&Selectable_get_spriteState_m1532_MethodInfo,
	&Selectable_set_spriteState_m1533_MethodInfo,
	&Selectable_get_animationTriggers_m1534_MethodInfo,
	&Selectable_set_animationTriggers_m1535_MethodInfo,
	&Selectable_get_targetGraphic_m1536_MethodInfo,
	&Selectable_set_targetGraphic_m1537_MethodInfo,
	&Selectable_get_interactable_m1538_MethodInfo,
	&Selectable_set_interactable_m1539_MethodInfo,
	&Selectable_get_isPointerInside_m1540_MethodInfo,
	&Selectable_set_isPointerInside_m1541_MethodInfo,
	&Selectable_get_isPointerDown_m1542_MethodInfo,
	&Selectable_set_isPointerDown_m1543_MethodInfo,
	&Selectable_get_hasSelection_m1544_MethodInfo,
	&Selectable_set_hasSelection_m1545_MethodInfo,
	&Selectable_get_image_m1546_MethodInfo,
	&Selectable_set_image_m1547_MethodInfo,
	&Selectable_get_animator_m1548_MethodInfo,
	&Selectable_Awake_m1549_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m1550_MethodInfo,
	&Selectable_IsInteractable_m1551_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m1552_MethodInfo,
	&Selectable_OnEnable_m1553_MethodInfo,
	&Selectable_OnSetProperty_m1554_MethodInfo,
	&Selectable_OnDisable_m1555_MethodInfo,
	&Selectable_get_currentSelectionState_m1556_MethodInfo,
	&Selectable_InstantClearState_m1557_MethodInfo,
	&Selectable_DoStateTransition_m1558_MethodInfo,
	&Selectable_FindSelectable_m1559_MethodInfo,
	&Selectable_GetPointOnRectEdge_m1560_MethodInfo,
	&Selectable_Navigate_m1561_MethodInfo,
	&Selectable_FindSelectableOnLeft_m1562_MethodInfo,
	&Selectable_FindSelectableOnRight_m1563_MethodInfo,
	&Selectable_FindSelectableOnUp_m1564_MethodInfo,
	&Selectable_FindSelectableOnDown_m1565_MethodInfo,
	&Selectable_OnMove_m1566_MethodInfo,
	&Selectable_StartColorTween_m1567_MethodInfo,
	&Selectable_DoSpriteSwap_m1568_MethodInfo,
	&Selectable_TriggerAnimation_m1569_MethodInfo,
	&Selectable_IsHighlighted_m1570_MethodInfo,
	&Selectable_IsPressed_m1571_MethodInfo,
	&Selectable_IsPressed_m1572_MethodInfo,
	&Selectable_UpdateSelectionState_m1573_MethodInfo,
	&Selectable_EvaluateAndTransitionToSelectionState_m1574_MethodInfo,
	&Selectable_InternalEvaluateAndTransitionToSelectionState_m1575_MethodInfo,
	&Selectable_OnPointerDown_m1576_MethodInfo,
	&Selectable_OnPointerUp_m1577_MethodInfo,
	&Selectable_OnPointerEnter_m1578_MethodInfo,
	&Selectable_OnPointerExit_m1579_MethodInfo,
	&Selectable_OnSelect_m1580_MethodInfo,
	&Selectable_OnDeselect_m1581_MethodInfo,
	&Selectable_Select_m1582_MethodInfo,
	NULL
};
extern const MethodInfo Selectable_get_allSelectables_m1525_MethodInfo;
static const PropertyInfo Selectable_t259____allSelectables_PropertyInfo = 
{
	&Selectable_t259_il2cpp_TypeInfo/* parent */
	, "allSelectables"/* name */
	, &Selectable_get_allSelectables_m1525_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_navigation_m1526_MethodInfo;
extern const MethodInfo Selectable_set_navigation_m1527_MethodInfo;
static const PropertyInfo Selectable_t259____navigation_PropertyInfo = 
{
	&Selectable_t259_il2cpp_TypeInfo/* parent */
	, "navigation"/* name */
	, &Selectable_get_navigation_m1526_MethodInfo/* get */
	, &Selectable_set_navigation_m1527_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_transition_m1528_MethodInfo;
extern const MethodInfo Selectable_set_transition_m1529_MethodInfo;
static const PropertyInfo Selectable_t259____transition_PropertyInfo = 
{
	&Selectable_t259_il2cpp_TypeInfo/* parent */
	, "transition"/* name */
	, &Selectable_get_transition_m1528_MethodInfo/* get */
	, &Selectable_set_transition_m1529_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_colors_m1530_MethodInfo;
extern const MethodInfo Selectable_set_colors_m1531_MethodInfo;
static const PropertyInfo Selectable_t259____colors_PropertyInfo = 
{
	&Selectable_t259_il2cpp_TypeInfo/* parent */
	, "colors"/* name */
	, &Selectable_get_colors_m1530_MethodInfo/* get */
	, &Selectable_set_colors_m1531_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_spriteState_m1532_MethodInfo;
extern const MethodInfo Selectable_set_spriteState_m1533_MethodInfo;
static const PropertyInfo Selectable_t259____spriteState_PropertyInfo = 
{
	&Selectable_t259_il2cpp_TypeInfo/* parent */
	, "spriteState"/* name */
	, &Selectable_get_spriteState_m1532_MethodInfo/* get */
	, &Selectable_set_spriteState_m1533_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_animationTriggers_m1534_MethodInfo;
extern const MethodInfo Selectable_set_animationTriggers_m1535_MethodInfo;
static const PropertyInfo Selectable_t259____animationTriggers_PropertyInfo = 
{
	&Selectable_t259_il2cpp_TypeInfo/* parent */
	, "animationTriggers"/* name */
	, &Selectable_get_animationTriggers_m1534_MethodInfo/* get */
	, &Selectable_set_animationTriggers_m1535_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_targetGraphic_m1536_MethodInfo;
extern const MethodInfo Selectable_set_targetGraphic_m1537_MethodInfo;
static const PropertyInfo Selectable_t259____targetGraphic_PropertyInfo = 
{
	&Selectable_t259_il2cpp_TypeInfo/* parent */
	, "targetGraphic"/* name */
	, &Selectable_get_targetGraphic_m1536_MethodInfo/* get */
	, &Selectable_set_targetGraphic_m1537_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_interactable_m1538_MethodInfo;
extern const MethodInfo Selectable_set_interactable_m1539_MethodInfo;
static const PropertyInfo Selectable_t259____interactable_PropertyInfo = 
{
	&Selectable_t259_il2cpp_TypeInfo/* parent */
	, "interactable"/* name */
	, &Selectable_get_interactable_m1538_MethodInfo/* get */
	, &Selectable_set_interactable_m1539_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_isPointerInside_m1540_MethodInfo;
extern const MethodInfo Selectable_set_isPointerInside_m1541_MethodInfo;
static const PropertyInfo Selectable_t259____isPointerInside_PropertyInfo = 
{
	&Selectable_t259_il2cpp_TypeInfo/* parent */
	, "isPointerInside"/* name */
	, &Selectable_get_isPointerInside_m1540_MethodInfo/* get */
	, &Selectable_set_isPointerInside_m1541_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_isPointerDown_m1542_MethodInfo;
extern const MethodInfo Selectable_set_isPointerDown_m1543_MethodInfo;
static const PropertyInfo Selectable_t259____isPointerDown_PropertyInfo = 
{
	&Selectable_t259_il2cpp_TypeInfo/* parent */
	, "isPointerDown"/* name */
	, &Selectable_get_isPointerDown_m1542_MethodInfo/* get */
	, &Selectable_set_isPointerDown_m1543_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_hasSelection_m1544_MethodInfo;
extern const MethodInfo Selectable_set_hasSelection_m1545_MethodInfo;
static const PropertyInfo Selectable_t259____hasSelection_PropertyInfo = 
{
	&Selectable_t259_il2cpp_TypeInfo/* parent */
	, "hasSelection"/* name */
	, &Selectable_get_hasSelection_m1544_MethodInfo/* get */
	, &Selectable_set_hasSelection_m1545_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_image_m1546_MethodInfo;
extern const MethodInfo Selectable_set_image_m1547_MethodInfo;
static const PropertyInfo Selectable_t259____image_PropertyInfo = 
{
	&Selectable_t259_il2cpp_TypeInfo/* parent */
	, "image"/* name */
	, &Selectable_get_image_m1546_MethodInfo/* get */
	, &Selectable_set_image_m1547_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_animator_m1548_MethodInfo;
static const PropertyInfo Selectable_t259____animator_PropertyInfo = 
{
	&Selectable_t259_il2cpp_TypeInfo/* parent */
	, "animator"/* name */
	, &Selectable_get_animator_m1548_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Selectable_get_currentSelectionState_m1556_MethodInfo;
static const PropertyInfo Selectable_t259____currentSelectionState_PropertyInfo = 
{
	&Selectable_t259_il2cpp_TypeInfo/* parent */
	, "currentSelectionState"/* name */
	, &Selectable_get_currentSelectionState_m1556_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Selectable_t259_PropertyInfos[] =
{
	&Selectable_t259____allSelectables_PropertyInfo,
	&Selectable_t259____navigation_PropertyInfo,
	&Selectable_t259____transition_PropertyInfo,
	&Selectable_t259____colors_PropertyInfo,
	&Selectable_t259____spriteState_PropertyInfo,
	&Selectable_t259____animationTriggers_PropertyInfo,
	&Selectable_t259____targetGraphic_PropertyInfo,
	&Selectable_t259____interactable_PropertyInfo,
	&Selectable_t259____isPointerInside_PropertyInfo,
	&Selectable_t259____isPointerDown_PropertyInfo,
	&Selectable_t259____hasSelection_PropertyInfo,
	&Selectable_t259____image_PropertyInfo,
	&Selectable_t259____animator_PropertyInfo,
	&Selectable_t259____currentSelectionState_PropertyInfo,
	NULL
};
static const Il2CppType* Selectable_t259_il2cpp_TypeInfo__nestedTypes[2] =
{
	&Transition_t335_0_0_0,
	&SelectionState_t336_0_0_0,
};
extern const MethodInfo Selectable_Awake_m1549_MethodInfo;
extern const MethodInfo Selectable_OnEnable_m1553_MethodInfo;
extern const MethodInfo Selectable_OnDisable_m1555_MethodInfo;
extern const MethodInfo UIBehaviour_IsActive_m857_MethodInfo;
extern const MethodInfo Selectable_OnDidApplyAnimationProperties_m1552_MethodInfo;
extern const MethodInfo Selectable_OnCanvasGroupChanged_m1550_MethodInfo;
extern const MethodInfo Selectable_OnPointerEnter_m1578_MethodInfo;
extern const MethodInfo Selectable_OnPointerExit_m1579_MethodInfo;
extern const MethodInfo Selectable_OnPointerDown_m1576_MethodInfo;
extern const MethodInfo Selectable_OnPointerUp_m1577_MethodInfo;
extern const MethodInfo Selectable_OnSelect_m1580_MethodInfo;
extern const MethodInfo Selectable_OnDeselect_m1581_MethodInfo;
extern const MethodInfo Selectable_OnMove_m1566_MethodInfo;
extern const MethodInfo Selectable_IsInteractable_m1551_MethodInfo;
extern const MethodInfo Selectable_InstantClearState_m1557_MethodInfo;
extern const MethodInfo Selectable_DoStateTransition_m1558_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnLeft_m1562_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnRight_m1563_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnUp_m1564_MethodInfo;
extern const MethodInfo Selectable_FindSelectableOnDown_m1565_MethodInfo;
extern const MethodInfo Selectable_Select_m1582_MethodInfo;
static const Il2CppMethodReference Selectable_t259_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&Selectable_Awake_m1549_MethodInfo,
	&Selectable_OnEnable_m1553_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&Selectable_OnDisable_m1555_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m858_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m1552_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m1550_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&Selectable_OnPointerEnter_m1578_MethodInfo,
	&Selectable_OnPointerExit_m1579_MethodInfo,
	&Selectable_OnPointerDown_m1576_MethodInfo,
	&Selectable_OnPointerUp_m1577_MethodInfo,
	&Selectable_OnSelect_m1580_MethodInfo,
	&Selectable_OnDeselect_m1581_MethodInfo,
	&Selectable_OnMove_m1566_MethodInfo,
	&Selectable_IsInteractable_m1551_MethodInfo,
	&Selectable_InstantClearState_m1557_MethodInfo,
	&Selectable_DoStateTransition_m1558_MethodInfo,
	&Selectable_FindSelectableOnLeft_m1562_MethodInfo,
	&Selectable_FindSelectableOnRight_m1563_MethodInfo,
	&Selectable_FindSelectableOnUp_m1564_MethodInfo,
	&Selectable_FindSelectableOnDown_m1565_MethodInfo,
	&Selectable_OnMove_m1566_MethodInfo,
	&Selectable_OnPointerDown_m1576_MethodInfo,
	&Selectable_OnPointerUp_m1577_MethodInfo,
	&Selectable_OnPointerEnter_m1578_MethodInfo,
	&Selectable_OnPointerExit_m1579_MethodInfo,
	&Selectable_OnSelect_m1580_MethodInfo,
	&Selectable_OnDeselect_m1581_MethodInfo,
	&Selectable_Select_m1582_MethodInfo,
};
static bool Selectable_t259_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IPointerEnterHandler_t390_0_0_0;
extern const Il2CppType IPointerExitHandler_t391_0_0_0;
extern const Il2CppType IPointerDownHandler_t392_0_0_0;
extern const Il2CppType IPointerUpHandler_t393_0_0_0;
extern const Il2CppType ISelectHandler_t402_0_0_0;
extern const Il2CppType IDeselectHandler_t403_0_0_0;
extern const Il2CppType IMoveHandler_t404_0_0_0;
static const Il2CppType* Selectable_t259_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t490_0_0_0,
	&IPointerEnterHandler_t390_0_0_0,
	&IPointerExitHandler_t391_0_0_0,
	&IPointerDownHandler_t392_0_0_0,
	&IPointerUpHandler_t393_0_0_0,
	&ISelectHandler_t402_0_0_0,
	&IDeselectHandler_t403_0_0_0,
	&IMoveHandler_t404_0_0_0,
};
static Il2CppInterfaceOffsetPair Selectable_t259_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t490_0_0_0, 16},
	{ &IPointerEnterHandler_t390_0_0_0, 16},
	{ &IPointerExitHandler_t391_0_0_0, 17},
	{ &IPointerDownHandler_t392_0_0_0, 18},
	{ &IPointerUpHandler_t393_0_0_0, 19},
	{ &ISelectHandler_t402_0_0_0, 20},
	{ &IDeselectHandler_t403_0_0_0, 21},
	{ &IMoveHandler_t404_0_0_0, 22},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Selectable_t259_1_0_0;
struct Selectable_t259;
const Il2CppTypeDefinitionMetadata Selectable_t259_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Selectable_t259_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Selectable_t259_InterfacesTypeInfos/* implementedInterfaces */
	, Selectable_t259_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t199_0_0_0/* parent */
	, Selectable_t259_VTable/* vtableMethods */
	, Selectable_t259_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 451/* fieldStart */

};
TypeInfo Selectable_t259_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Selectable"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Selectable_t259_MethodInfos/* methods */
	, Selectable_t259_PropertyInfos/* properties */
	, NULL/* events */
	, &Selectable_t259_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 225/* custom_attributes_cache */
	, &Selectable_t259_0_0_0/* byval_arg */
	, &Selectable_t259_1_0_0/* this_arg */
	, &Selectable_t259_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Selectable_t259)/* instance_size */
	, sizeof (Selectable_t259)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Selectable_t259_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 60/* method_count */
	, 14/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 38/* vtable_count */
	, 8/* interfaces_count */
	, 8/* interface_offsets_count */

};
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility.h"
// Metadata Definition UnityEngine.UI.SetPropertyUtility
extern TypeInfo SetPropertyUtility_t340_il2cpp_TypeInfo;
// UnityEngine.UI.SetPropertyUtility
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtilityMethodDeclarations.h"
extern const Il2CppType Color_t90_1_0_0;
extern const Il2CppType Color_t90_1_0_0;
extern const Il2CppType Color_t90_0_0_0;
static const ParameterInfo SetPropertyUtility_t340_SetPropertyUtility_SetColor_m1583_ParameterInfos[] = 
{
	{"currentValue", 0, 134218254, 0, &Color_t90_1_0_0},
	{"newValue", 1, 134218255, 0, &Color_t90_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_ColorU26_t536_Color_t90 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetColor(UnityEngine.Color&,UnityEngine.Color)
extern const MethodInfo SetPropertyUtility_SetColor_m1583_MethodInfo = 
{
	"SetColor"/* name */
	, (methodPointerType)&SetPropertyUtility_SetColor_m1583/* method */
	, &SetPropertyUtility_t340_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_ColorU26_t536_Color_t90/* invoker_method */
	, SetPropertyUtility_t340_SetPropertyUtility_SetColor_m1583_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 881/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SetPropertyUtility_SetStruct_m2502_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m2502_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m2502_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m2502_gp_0_0_0_0;
static const ParameterInfo SetPropertyUtility_t340_SetPropertyUtility_SetStruct_m2502_ParameterInfos[] = 
{
	{"currentValue", 0, 134218256, 0, &SetPropertyUtility_SetStruct_m2502_gp_0_1_0_0},
	{"newValue", 1, 134218257, 0, &SetPropertyUtility_SetStruct_m2502_gp_0_0_0_0},
};
extern const Il2CppGenericContainer SetPropertyUtility_SetStruct_m2502_Il2CppGenericContainer;
extern TypeInfo SetPropertyUtility_SetStruct_m2502_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppType ValueType_t524_0_0_0;
static const Il2CppType* SetPropertyUtility_SetStruct_m2502_gp_T_0_il2cpp_TypeInfo_constraints[] = { 
&ValueType_t524_0_0_0 /* System.ValueType */, 
 NULL };
extern const Il2CppGenericParameter SetPropertyUtility_SetStruct_m2502_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &SetPropertyUtility_SetStruct_m2502_Il2CppGenericContainer, SetPropertyUtility_SetStruct_m2502_gp_T_0_il2cpp_TypeInfo_constraints, "T", 0, 24 };
static const Il2CppGenericParameter* SetPropertyUtility_SetStruct_m2502_Il2CppGenericParametersArray[1] = 
{
	&SetPropertyUtility_SetStruct_m2502_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo SetPropertyUtility_SetStruct_m2502_MethodInfo;
extern const Il2CppGenericContainer SetPropertyUtility_SetStruct_m2502_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&SetPropertyUtility_SetStruct_m2502_MethodInfo, 1, 1, SetPropertyUtility_SetStruct_m2502_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition SetPropertyUtility_SetStruct_m2502_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&SetPropertyUtility_SetStruct_m2502_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetStruct(T&,T)
extern const MethodInfo SetPropertyUtility_SetStruct_m2502_MethodInfo = 
{
	"SetStruct"/* name */
	, NULL/* method */
	, &SetPropertyUtility_t340_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, SetPropertyUtility_t340_SetPropertyUtility_SetStruct_m2502_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 882/* token */
	, SetPropertyUtility_SetStruct_m2502_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &SetPropertyUtility_SetStruct_m2502_Il2CppGenericContainer/* genericContainer */

};
extern const Il2CppType SetPropertyUtility_SetClass_m2503_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetClass_m2503_gp_0_1_0_0;
extern const Il2CppType SetPropertyUtility_SetClass_m2503_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetClass_m2503_gp_0_0_0_0;
static const ParameterInfo SetPropertyUtility_t340_SetPropertyUtility_SetClass_m2503_ParameterInfos[] = 
{
	{"currentValue", 0, 134218258, 0, &SetPropertyUtility_SetClass_m2503_gp_0_1_0_0},
	{"newValue", 1, 134218259, 0, &SetPropertyUtility_SetClass_m2503_gp_0_0_0_0},
};
extern const Il2CppGenericContainer SetPropertyUtility_SetClass_m2503_Il2CppGenericContainer;
extern TypeInfo SetPropertyUtility_SetClass_m2503_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter SetPropertyUtility_SetClass_m2503_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &SetPropertyUtility_SetClass_m2503_Il2CppGenericContainer, NULL, "T", 0, 4 };
static const Il2CppGenericParameter* SetPropertyUtility_SetClass_m2503_Il2CppGenericParametersArray[1] = 
{
	&SetPropertyUtility_SetClass_m2503_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo SetPropertyUtility_SetClass_m2503_MethodInfo;
extern const Il2CppGenericContainer SetPropertyUtility_SetClass_m2503_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&SetPropertyUtility_SetClass_m2503_MethodInfo, 1, 1, SetPropertyUtility_SetClass_m2503_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition SetPropertyUtility_SetClass_m2503_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&SetPropertyUtility_SetClass_m2503_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Boolean UnityEngine.UI.SetPropertyUtility::SetClass(T&,T)
extern const MethodInfo SetPropertyUtility_SetClass_m2503_MethodInfo = 
{
	"SetClass"/* name */
	, NULL/* method */
	, &SetPropertyUtility_t340_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, SetPropertyUtility_t340_SetPropertyUtility_SetClass_m2503_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 883/* token */
	, SetPropertyUtility_SetClass_m2503_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &SetPropertyUtility_SetClass_m2503_Il2CppGenericContainer/* genericContainer */

};
static const MethodInfo* SetPropertyUtility_t340_MethodInfos[] =
{
	&SetPropertyUtility_SetColor_m1583_MethodInfo,
	&SetPropertyUtility_SetStruct_m2502_MethodInfo,
	&SetPropertyUtility_SetClass_m2503_MethodInfo,
	NULL
};
extern const MethodInfo Object_ToString_m542_MethodInfo;
static const Il2CppMethodReference SetPropertyUtility_t340_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool SetPropertyUtility_t340_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SetPropertyUtility_t340_0_0_0;
extern const Il2CppType SetPropertyUtility_t340_1_0_0;
extern const Il2CppType Object_t_0_0_0;
struct SetPropertyUtility_t340;
const Il2CppTypeDefinitionMetadata SetPropertyUtility_t340_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, SetPropertyUtility_t340_VTable/* vtableMethods */
	, SetPropertyUtility_t340_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SetPropertyUtility_t340_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetPropertyUtility"/* name */
	, "UnityEngine.UI"/* namespaze */
	, SetPropertyUtility_t340_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SetPropertyUtility_t340_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SetPropertyUtility_t340_0_0_0/* byval_arg */
	, &SetPropertyUtility_t340_1_0_0/* this_arg */
	, &SetPropertyUtility_t340_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetPropertyUtility_t340)/* instance_size */
	, sizeof (SetPropertyUtility_t340)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
// Metadata Definition UnityEngine.UI.Slider/Direction
extern TypeInfo Direction_t341_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/Direction
#include "UnityEngine_UI_UnityEngine_UI_Slider_DirectionMethodDeclarations.h"
static const MethodInfo* Direction_t341_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Direction_t341_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool Direction_t341_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Direction_t341_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Direction_t341_0_0_0;
extern const Il2CppType Direction_t341_1_0_0;
extern TypeInfo Slider_t22_il2cpp_TypeInfo;
extern const Il2CppType Slider_t22_0_0_0;
const Il2CppTypeDefinitionMetadata Direction_t341_DefinitionMetadata = 
{
	&Slider_t22_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Direction_t341_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, Direction_t341_VTable/* vtableMethods */
	, Direction_t341_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 465/* fieldStart */

};
TypeInfo Direction_t341_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Direction"/* name */
	, ""/* namespaze */
	, Direction_t341_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Direction_t341_0_0_0/* byval_arg */
	, &Direction_t341_1_0_0/* this_arg */
	, &Direction_t341_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Direction_t341)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Direction_t341)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Slider/SliderEvent
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent.h"
// Metadata Definition UnityEngine.UI.Slider/SliderEvent
extern TypeInfo SliderEvent_t342_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/SliderEvent
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEventMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider/SliderEvent::.ctor()
extern const MethodInfo SliderEvent__ctor_m1584_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SliderEvent__ctor_m1584/* method */
	, &SliderEvent_t342_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 927/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SliderEvent_t342_MethodInfos[] =
{
	&SliderEvent__ctor_m1584_MethodInfo,
	NULL
};
extern const Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m2578_GenericMethod;
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m2579_GenericMethod;
static const Il2CppMethodReference SliderEvent_t342_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&UnityEventBase_ToString_m2549_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2550_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2551_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m2578_GenericMethod,
	&UnityEvent_1_GetDelegate_m2579_GenericMethod,
};
static bool SliderEvent_t342_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
static Il2CppInterfaceOffsetPair SliderEvent_t342_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t510_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SliderEvent_t342_0_0_0;
extern const Il2CppType SliderEvent_t342_1_0_0;
extern const Il2CppType UnityEvent_1_t325_0_0_0;
struct SliderEvent_t342;
const Il2CppTypeDefinitionMetadata SliderEvent_t342_DefinitionMetadata = 
{
	&Slider_t22_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, SliderEvent_t342_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t325_0_0_0/* parent */
	, SliderEvent_t342_VTable/* vtableMethods */
	, SliderEvent_t342_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo SliderEvent_t342_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderEvent"/* name */
	, ""/* namespaze */
	, SliderEvent_t342_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &SliderEvent_t342_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SliderEvent_t342_0_0_0/* byval_arg */
	, &SliderEvent_t342_1_0_0/* this_arg */
	, &SliderEvent_t342_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderEvent_t342)/* instance_size */
	, sizeof (SliderEvent_t342)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
// Metadata Definition UnityEngine.UI.Slider/Axis
extern TypeInfo Axis_t343_il2cpp_TypeInfo;
// UnityEngine.UI.Slider/Axis
#include "UnityEngine_UI_UnityEngine_UI_Slider_AxisMethodDeclarations.h"
static const MethodInfo* Axis_t343_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Axis_t343_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool Axis_t343_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Axis_t343_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Axis_t343_0_0_0;
extern const Il2CppType Axis_t343_1_0_0;
const Il2CppTypeDefinitionMetadata Axis_t343_DefinitionMetadata = 
{
	&Slider_t22_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Axis_t343_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, Axis_t343_VTable/* vtableMethods */
	, Axis_t343_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 470/* fieldStart */

};
TypeInfo Axis_t343_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Axis"/* name */
	, ""/* namespaze */
	, Axis_t343_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Axis_t343_0_0_0/* byval_arg */
	, &Axis_t343_1_0_0/* this_arg */
	, &Axis_t343_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Axis_t343)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Axis_t343)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_Slider.h"
// Metadata Definition UnityEngine.UI.Slider
// UnityEngine.UI.Slider
#include "UnityEngine_UI_UnityEngine_UI_SliderMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::.ctor()
extern const MethodInfo Slider__ctor_m1585_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Slider__ctor_m1585/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 884/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Slider::get_fillRect()
extern const MethodInfo Slider_get_fillRect_m1586_MethodInfo = 
{
	"get_fillRect"/* name */
	, (methodPointerType)&Slider_get_fillRect_m1586/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t272_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 885/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo Slider_t22_Slider_set_fillRect_m1587_ParameterInfos[] = 
{
	{"value", 0, 134218260, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_fillRect(UnityEngine.RectTransform)
extern const MethodInfo Slider_set_fillRect_m1587_MethodInfo = 
{
	"set_fillRect"/* name */
	, (methodPointerType)&Slider_set_fillRect_m1587/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Slider_t22_Slider_set_fillRect_m1587_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 886/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Slider::get_handleRect()
extern const MethodInfo Slider_get_handleRect_m1588_MethodInfo = 
{
	"get_handleRect"/* name */
	, (methodPointerType)&Slider_get_handleRect_m1588/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t272_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 887/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo Slider_t22_Slider_set_handleRect_m1589_ParameterInfos[] = 
{
	{"value", 0, 134218261, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_handleRect(UnityEngine.RectTransform)
extern const MethodInfo Slider_set_handleRect_m1589_MethodInfo = 
{
	"set_handleRect"/* name */
	, (methodPointerType)&Slider_set_handleRect_m1589/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Slider_t22_Slider_set_handleRect_m1589_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 888/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Direction_t341 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::get_direction()
extern const MethodInfo Slider_get_direction_m1590_MethodInfo = 
{
	"get_direction"/* name */
	, (methodPointerType)&Slider_get_direction_m1590/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Direction_t341_0_0_0/* return_type */
	, RuntimeInvoker_Direction_t341/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 889/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Direction_t341_0_0_0;
static const ParameterInfo Slider_t22_Slider_set_direction_m1591_ParameterInfos[] = 
{
	{"value", 0, 134218262, 0, &Direction_t341_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_direction(UnityEngine.UI.Slider/Direction)
extern const MethodInfo Slider_set_direction_m1591_MethodInfo = 
{
	"set_direction"/* name */
	, (methodPointerType)&Slider_set_direction_m1591/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Slider_t22_Slider_set_direction_m1591_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 890/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_minValue()
extern const MethodInfo Slider_get_minValue_m1592_MethodInfo = 
{
	"get_minValue"/* name */
	, (methodPointerType)&Slider_get_minValue_m1592/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 891/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Slider_t22_Slider_set_minValue_m1593_ParameterInfos[] = 
{
	{"value", 0, 134218263, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_minValue(System.Single)
extern const MethodInfo Slider_set_minValue_m1593_MethodInfo = 
{
	"set_minValue"/* name */
	, (methodPointerType)&Slider_set_minValue_m1593/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, Slider_t22_Slider_set_minValue_m1593_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 892/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_maxValue()
extern const MethodInfo Slider_get_maxValue_m1594_MethodInfo = 
{
	"get_maxValue"/* name */
	, (methodPointerType)&Slider_get_maxValue_m1594/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 893/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Slider_t22_Slider_set_maxValue_m1595_ParameterInfos[] = 
{
	{"value", 0, 134218264, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_maxValue(System.Single)
extern const MethodInfo Slider_set_maxValue_m1595_MethodInfo = 
{
	"set_maxValue"/* name */
	, (methodPointerType)&Slider_set_maxValue_m1595/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, Slider_t22_Slider_set_maxValue_m1595_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 894/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::get_wholeNumbers()
extern const MethodInfo Slider_get_wholeNumbers_m1596_MethodInfo = 
{
	"get_wholeNumbers"/* name */
	, (methodPointerType)&Slider_get_wholeNumbers_m1596/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 895/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Slider_t22_Slider_set_wholeNumbers_m1597_ParameterInfos[] = 
{
	{"value", 0, 134218265, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_wholeNumbers(System.Boolean)
extern const MethodInfo Slider_set_wholeNumbers_m1597_MethodInfo = 
{
	"set_wholeNumbers"/* name */
	, (methodPointerType)&Slider_set_wholeNumbers_m1597/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Slider_t22_Slider_set_wholeNumbers_m1597_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 896/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_value()
extern const MethodInfo Slider_get_value_m344_MethodInfo = 
{
	"get_value"/* name */
	, (methodPointerType)&Slider_get_value_m344/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 897/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Slider_t22_Slider_set_value_m1598_ParameterInfos[] = 
{
	{"value", 0, 134218266, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_value(System.Single)
extern const MethodInfo Slider_set_value_m1598_MethodInfo = 
{
	"set_value"/* name */
	, (methodPointerType)&Slider_set_value_m1598/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, Slider_t22_Slider_set_value_m1598_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 898/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_normalizedValue()
extern const MethodInfo Slider_get_normalizedValue_m1599_MethodInfo = 
{
	"get_normalizedValue"/* name */
	, (methodPointerType)&Slider_get_normalizedValue_m1599/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 899/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Slider_t22_Slider_set_normalizedValue_m1600_ParameterInfos[] = 
{
	{"value", 0, 134218267, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_normalizedValue(System.Single)
extern const MethodInfo Slider_set_normalizedValue_m1600_MethodInfo = 
{
	"set_normalizedValue"/* name */
	, (methodPointerType)&Slider_set_normalizedValue_m1600/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, Slider_t22_Slider_set_normalizedValue_m1600_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 900/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::get_onValueChanged()
extern const MethodInfo Slider_get_onValueChanged_m1601_MethodInfo = 
{
	"get_onValueChanged"/* name */
	, (methodPointerType)&Slider_get_onValueChanged_m1601/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &SliderEvent_t342_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 901/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType SliderEvent_t342_0_0_0;
static const ParameterInfo Slider_t22_Slider_set_onValueChanged_m1602_ParameterInfos[] = 
{
	{"value", 0, 134218268, 0, &SliderEvent_t342_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::set_onValueChanged(UnityEngine.UI.Slider/SliderEvent)
extern const MethodInfo Slider_set_onValueChanged_m1602_MethodInfo = 
{
	"set_onValueChanged"/* name */
	, (methodPointerType)&Slider_set_onValueChanged_m1602/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Slider_t22_Slider_set_onValueChanged_m1602_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 902/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Slider::get_stepSize()
extern const MethodInfo Slider_get_stepSize_m1603_MethodInfo = 
{
	"get_stepSize"/* name */
	, (methodPointerType)&Slider_get_stepSize_m1603/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 903/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t260_0_0_0;
static const ParameterInfo Slider_t22_Slider_Rebuild_m1604_ParameterInfos[] = 
{
	{"executing", 0, 134218269, 0, &CanvasUpdate_t260_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo Slider_Rebuild_m1604_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&Slider_Rebuild_m1604/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Slider_t22_Slider_Rebuild_m1604_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 43/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 904/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnEnable()
extern const MethodInfo Slider_OnEnable_m1605_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Slider_OnEnable_m1605/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 905/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnDisable()
extern const MethodInfo Slider_OnDisable_m1606_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Slider_OnDisable_m1606/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 906/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateCachedReferences()
extern const MethodInfo Slider_UpdateCachedReferences_m1607_MethodInfo = 
{
	"UpdateCachedReferences"/* name */
	, (methodPointerType)&Slider_UpdateCachedReferences_m1607/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 907/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Slider_t22_Slider_Set_m1608_ParameterInfos[] = 
{
	{"input", 0, 134218270, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Set(System.Single)
extern const MethodInfo Slider_Set_m1608_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Slider_Set_m1608/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, Slider_t22_Slider_Set_m1608_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 908/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Slider_t22_Slider_Set_m1609_ParameterInfos[] = 
{
	{"input", 0, 134218271, 0, &Single_t151_0_0_0},
	{"sendCallback", 1, 134218272, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::Set(System.Single,System.Boolean)
extern const MethodInfo Slider_Set_m1609_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Slider_Set_m1609/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151_SByte_t170/* invoker_method */
	, Slider_t22_Slider_Set_m1609_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 909/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnRectTransformDimensionsChange()
extern const MethodInfo Slider_OnRectTransformDimensionsChange_m1610_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&Slider_OnRectTransformDimensionsChange_m1610/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 910/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Axis_t343 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Slider/Axis UnityEngine.UI.Slider::get_axis()
extern const MethodInfo Slider_get_axis_m1611_MethodInfo = 
{
	"get_axis"/* name */
	, (methodPointerType)&Slider_get_axis_m1611/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Axis_t343_0_0_0/* return_type */
	, RuntimeInvoker_Axis_t343/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 911/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::get_reverseValue()
extern const MethodInfo Slider_get_reverseValue_m1612_MethodInfo = 
{
	"get_reverseValue"/* name */
	, (methodPointerType)&Slider_get_reverseValue_m1612/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 912/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateVisuals()
extern const MethodInfo Slider_UpdateVisuals_m1613_MethodInfo = 
{
	"UpdateVisuals"/* name */
	, (methodPointerType)&Slider_UpdateVisuals_m1613/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 913/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
extern const Il2CppType Camera_t3_0_0_0;
extern const Il2CppType Camera_t3_0_0_0;
static const ParameterInfo Slider_t22_Slider_UpdateDrag_m1614_ParameterInfos[] = 
{
	{"eventData", 0, 134218273, 0, &PointerEventData_t236_0_0_0},
	{"cam", 1, 134218274, 0, &Camera_t3_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::UpdateDrag(UnityEngine.EventSystems.PointerEventData,UnityEngine.Camera)
extern const MethodInfo Slider_UpdateDrag_m1614_MethodInfo = 
{
	"UpdateDrag"/* name */
	, (methodPointerType)&Slider_UpdateDrag_m1614/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, Slider_t22_Slider_UpdateDrag_m1614_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 914/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
static const ParameterInfo Slider_t22_Slider_MayDrag_m1615_ParameterInfos[] = 
{
	{"eventData", 0, 134218275, 0, &PointerEventData_t236_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::MayDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_MayDrag_m1615_MethodInfo = 
{
	"MayDrag"/* name */
	, (methodPointerType)&Slider_MayDrag_m1615/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, Slider_t22_Slider_MayDrag_m1615_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 915/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
static const ParameterInfo Slider_t22_Slider_OnPointerDown_m1616_ParameterInfos[] = 
{
	{"eventData", 0, 134218276, 0, &PointerEventData_t236_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_OnPointerDown_m1616_MethodInfo = 
{
	"OnPointerDown"/* name */
	, (methodPointerType)&Slider_OnPointerDown_m1616/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Slider_t22_Slider_OnPointerDown_m1616_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 916/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
static const ParameterInfo Slider_t22_Slider_OnDrag_m1617_ParameterInfos[] = 
{
	{"eventData", 0, 134218277, 0, &PointerEventData_t236_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_OnDrag_m1617_MethodInfo = 
{
	"OnDrag"/* name */
	, (methodPointerType)&Slider_OnDrag_m1617/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Slider_t22_Slider_OnDrag_m1617_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 44/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 917/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AxisEventData_t232_0_0_0;
static const ParameterInfo Slider_t22_Slider_OnMove_m1618_ParameterInfos[] = 
{
	{"eventData", 0, 134218278, 0, &AxisEventData_t232_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnMove(UnityEngine.EventSystems.AxisEventData)
extern const MethodInfo Slider_OnMove_m1618_MethodInfo = 
{
	"OnMove"/* name */
	, (methodPointerType)&Slider_OnMove_m1618/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Slider_t22_Slider_OnMove_m1618_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 918/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnLeft()
extern const MethodInfo Slider_FindSelectableOnLeft_m1619_MethodInfo = 
{
	"FindSelectableOnLeft"/* name */
	, (methodPointerType)&Slider_FindSelectableOnLeft_m1619/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t259_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 919/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnRight()
extern const MethodInfo Slider_FindSelectableOnRight_m1620_MethodInfo = 
{
	"FindSelectableOnRight"/* name */
	, (methodPointerType)&Slider_FindSelectableOnRight_m1620/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t259_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 920/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnUp()
extern const MethodInfo Slider_FindSelectableOnUp_m1621_MethodInfo = 
{
	"FindSelectableOnUp"/* name */
	, (methodPointerType)&Slider_FindSelectableOnUp_m1621/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t259_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 921/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Selectable UnityEngine.UI.Slider::FindSelectableOnDown()
extern const MethodInfo Slider_FindSelectableOnDown_m1622_MethodInfo = 
{
	"FindSelectableOnDown"/* name */
	, (methodPointerType)&Slider_FindSelectableOnDown_m1622/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Selectable_t259_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 922/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
static const ParameterInfo Slider_t22_Slider_OnInitializePotentialDrag_m1623_ParameterInfos[] = 
{
	{"eventData", 0, 134218279, 0, &PointerEventData_t236_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Slider_OnInitializePotentialDrag_m1623_MethodInfo = 
{
	"OnInitializePotentialDrag"/* name */
	, (methodPointerType)&Slider_OnInitializePotentialDrag_m1623/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Slider_t22_Slider_OnInitializePotentialDrag_m1623_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 45/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 923/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Direction_t341_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Slider_t22_Slider_SetDirection_m1624_ParameterInfos[] = 
{
	{"direction", 0, 134218280, 0, &Direction_t341_0_0_0},
	{"includeRectLayouts", 1, 134218281, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Slider::SetDirection(UnityEngine.UI.Slider/Direction,System.Boolean)
extern const MethodInfo Slider_SetDirection_m1624_MethodInfo = 
{
	"SetDirection"/* name */
	, (methodPointerType)&Slider_SetDirection_m1624/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_SByte_t170/* invoker_method */
	, Slider_t22_Slider_SetDirection_m1624_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 924/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Slider::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1625_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1625/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 46/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 925/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.Slider::UnityEngine.UI.ICanvasElement.get_transform()
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_get_transform_m1626_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&Slider_UnityEngine_UI_ICanvasElement_get_transform_m1626/* method */
	, &Slider_t22_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 47/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 926/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Slider_t22_MethodInfos[] =
{
	&Slider__ctor_m1585_MethodInfo,
	&Slider_get_fillRect_m1586_MethodInfo,
	&Slider_set_fillRect_m1587_MethodInfo,
	&Slider_get_handleRect_m1588_MethodInfo,
	&Slider_set_handleRect_m1589_MethodInfo,
	&Slider_get_direction_m1590_MethodInfo,
	&Slider_set_direction_m1591_MethodInfo,
	&Slider_get_minValue_m1592_MethodInfo,
	&Slider_set_minValue_m1593_MethodInfo,
	&Slider_get_maxValue_m1594_MethodInfo,
	&Slider_set_maxValue_m1595_MethodInfo,
	&Slider_get_wholeNumbers_m1596_MethodInfo,
	&Slider_set_wholeNumbers_m1597_MethodInfo,
	&Slider_get_value_m344_MethodInfo,
	&Slider_set_value_m1598_MethodInfo,
	&Slider_get_normalizedValue_m1599_MethodInfo,
	&Slider_set_normalizedValue_m1600_MethodInfo,
	&Slider_get_onValueChanged_m1601_MethodInfo,
	&Slider_set_onValueChanged_m1602_MethodInfo,
	&Slider_get_stepSize_m1603_MethodInfo,
	&Slider_Rebuild_m1604_MethodInfo,
	&Slider_OnEnable_m1605_MethodInfo,
	&Slider_OnDisable_m1606_MethodInfo,
	&Slider_UpdateCachedReferences_m1607_MethodInfo,
	&Slider_Set_m1608_MethodInfo,
	&Slider_Set_m1609_MethodInfo,
	&Slider_OnRectTransformDimensionsChange_m1610_MethodInfo,
	&Slider_get_axis_m1611_MethodInfo,
	&Slider_get_reverseValue_m1612_MethodInfo,
	&Slider_UpdateVisuals_m1613_MethodInfo,
	&Slider_UpdateDrag_m1614_MethodInfo,
	&Slider_MayDrag_m1615_MethodInfo,
	&Slider_OnPointerDown_m1616_MethodInfo,
	&Slider_OnDrag_m1617_MethodInfo,
	&Slider_OnMove_m1618_MethodInfo,
	&Slider_FindSelectableOnLeft_m1619_MethodInfo,
	&Slider_FindSelectableOnRight_m1620_MethodInfo,
	&Slider_FindSelectableOnUp_m1621_MethodInfo,
	&Slider_FindSelectableOnDown_m1622_MethodInfo,
	&Slider_OnInitializePotentialDrag_m1623_MethodInfo,
	&Slider_SetDirection_m1624_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1625_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m1626_MethodInfo,
	NULL
};
extern const MethodInfo Slider_get_fillRect_m1586_MethodInfo;
extern const MethodInfo Slider_set_fillRect_m1587_MethodInfo;
static const PropertyInfo Slider_t22____fillRect_PropertyInfo = 
{
	&Slider_t22_il2cpp_TypeInfo/* parent */
	, "fillRect"/* name */
	, &Slider_get_fillRect_m1586_MethodInfo/* get */
	, &Slider_set_fillRect_m1587_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_handleRect_m1588_MethodInfo;
extern const MethodInfo Slider_set_handleRect_m1589_MethodInfo;
static const PropertyInfo Slider_t22____handleRect_PropertyInfo = 
{
	&Slider_t22_il2cpp_TypeInfo/* parent */
	, "handleRect"/* name */
	, &Slider_get_handleRect_m1588_MethodInfo/* get */
	, &Slider_set_handleRect_m1589_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_direction_m1590_MethodInfo;
extern const MethodInfo Slider_set_direction_m1591_MethodInfo;
static const PropertyInfo Slider_t22____direction_PropertyInfo = 
{
	&Slider_t22_il2cpp_TypeInfo/* parent */
	, "direction"/* name */
	, &Slider_get_direction_m1590_MethodInfo/* get */
	, &Slider_set_direction_m1591_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_minValue_m1592_MethodInfo;
extern const MethodInfo Slider_set_minValue_m1593_MethodInfo;
static const PropertyInfo Slider_t22____minValue_PropertyInfo = 
{
	&Slider_t22_il2cpp_TypeInfo/* parent */
	, "minValue"/* name */
	, &Slider_get_minValue_m1592_MethodInfo/* get */
	, &Slider_set_minValue_m1593_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_maxValue_m1594_MethodInfo;
extern const MethodInfo Slider_set_maxValue_m1595_MethodInfo;
static const PropertyInfo Slider_t22____maxValue_PropertyInfo = 
{
	&Slider_t22_il2cpp_TypeInfo/* parent */
	, "maxValue"/* name */
	, &Slider_get_maxValue_m1594_MethodInfo/* get */
	, &Slider_set_maxValue_m1595_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_wholeNumbers_m1596_MethodInfo;
extern const MethodInfo Slider_set_wholeNumbers_m1597_MethodInfo;
static const PropertyInfo Slider_t22____wholeNumbers_PropertyInfo = 
{
	&Slider_t22_il2cpp_TypeInfo/* parent */
	, "wholeNumbers"/* name */
	, &Slider_get_wholeNumbers_m1596_MethodInfo/* get */
	, &Slider_set_wholeNumbers_m1597_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_value_m344_MethodInfo;
extern const MethodInfo Slider_set_value_m1598_MethodInfo;
static const PropertyInfo Slider_t22____value_PropertyInfo = 
{
	&Slider_t22_il2cpp_TypeInfo/* parent */
	, "value"/* name */
	, &Slider_get_value_m344_MethodInfo/* get */
	, &Slider_set_value_m1598_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_normalizedValue_m1599_MethodInfo;
extern const MethodInfo Slider_set_normalizedValue_m1600_MethodInfo;
static const PropertyInfo Slider_t22____normalizedValue_PropertyInfo = 
{
	&Slider_t22_il2cpp_TypeInfo/* parent */
	, "normalizedValue"/* name */
	, &Slider_get_normalizedValue_m1599_MethodInfo/* get */
	, &Slider_set_normalizedValue_m1600_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_onValueChanged_m1601_MethodInfo;
extern const MethodInfo Slider_set_onValueChanged_m1602_MethodInfo;
static const PropertyInfo Slider_t22____onValueChanged_PropertyInfo = 
{
	&Slider_t22_il2cpp_TypeInfo/* parent */
	, "onValueChanged"/* name */
	, &Slider_get_onValueChanged_m1601_MethodInfo/* get */
	, &Slider_set_onValueChanged_m1602_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_stepSize_m1603_MethodInfo;
static const PropertyInfo Slider_t22____stepSize_PropertyInfo = 
{
	&Slider_t22_il2cpp_TypeInfo/* parent */
	, "stepSize"/* name */
	, &Slider_get_stepSize_m1603_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_axis_m1611_MethodInfo;
static const PropertyInfo Slider_t22____axis_PropertyInfo = 
{
	&Slider_t22_il2cpp_TypeInfo/* parent */
	, "axis"/* name */
	, &Slider_get_axis_m1611_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Slider_get_reverseValue_m1612_MethodInfo;
static const PropertyInfo Slider_t22____reverseValue_PropertyInfo = 
{
	&Slider_t22_il2cpp_TypeInfo/* parent */
	, "reverseValue"/* name */
	, &Slider_get_reverseValue_m1612_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Slider_t22_PropertyInfos[] =
{
	&Slider_t22____fillRect_PropertyInfo,
	&Slider_t22____handleRect_PropertyInfo,
	&Slider_t22____direction_PropertyInfo,
	&Slider_t22____minValue_PropertyInfo,
	&Slider_t22____maxValue_PropertyInfo,
	&Slider_t22____wholeNumbers_PropertyInfo,
	&Slider_t22____value_PropertyInfo,
	&Slider_t22____normalizedValue_PropertyInfo,
	&Slider_t22____onValueChanged_PropertyInfo,
	&Slider_t22____stepSize_PropertyInfo,
	&Slider_t22____axis_PropertyInfo,
	&Slider_t22____reverseValue_PropertyInfo,
	NULL
};
static const Il2CppType* Slider_t22_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Direction_t341_0_0_0,
	&SliderEvent_t342_0_0_0,
	&Axis_t343_0_0_0,
};
extern const MethodInfo Slider_OnEnable_m1605_MethodInfo;
extern const MethodInfo Slider_OnDisable_m1606_MethodInfo;
extern const MethodInfo Slider_OnRectTransformDimensionsChange_m1610_MethodInfo;
extern const MethodInfo Slider_OnPointerDown_m1616_MethodInfo;
extern const MethodInfo Slider_OnMove_m1618_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnLeft_m1619_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnRight_m1620_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnUp_m1621_MethodInfo;
extern const MethodInfo Slider_FindSelectableOnDown_m1622_MethodInfo;
extern const MethodInfo Slider_OnInitializePotentialDrag_m1623_MethodInfo;
extern const MethodInfo Slider_OnDrag_m1617_MethodInfo;
extern const MethodInfo Slider_Rebuild_m1604_MethodInfo;
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_get_transform_m1626_MethodInfo;
extern const MethodInfo Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1625_MethodInfo;
static const Il2CppMethodReference Slider_t22_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&Selectable_Awake_m1549_MethodInfo,
	&Slider_OnEnable_m1605_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&Slider_OnDisable_m1606_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&Slider_OnRectTransformDimensionsChange_m1610_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m1552_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m1550_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&Selectable_OnPointerEnter_m1578_MethodInfo,
	&Selectable_OnPointerExit_m1579_MethodInfo,
	&Slider_OnPointerDown_m1616_MethodInfo,
	&Selectable_OnPointerUp_m1577_MethodInfo,
	&Selectable_OnSelect_m1580_MethodInfo,
	&Selectable_OnDeselect_m1581_MethodInfo,
	&Slider_OnMove_m1618_MethodInfo,
	&Selectable_IsInteractable_m1551_MethodInfo,
	&Selectable_InstantClearState_m1557_MethodInfo,
	&Selectable_DoStateTransition_m1558_MethodInfo,
	&Slider_FindSelectableOnLeft_m1619_MethodInfo,
	&Slider_FindSelectableOnRight_m1620_MethodInfo,
	&Slider_FindSelectableOnUp_m1621_MethodInfo,
	&Slider_FindSelectableOnDown_m1622_MethodInfo,
	&Slider_OnMove_m1618_MethodInfo,
	&Slider_OnPointerDown_m1616_MethodInfo,
	&Selectable_OnPointerUp_m1577_MethodInfo,
	&Selectable_OnPointerEnter_m1578_MethodInfo,
	&Selectable_OnPointerExit_m1579_MethodInfo,
	&Selectable_OnSelect_m1580_MethodInfo,
	&Selectable_OnDeselect_m1581_MethodInfo,
	&Selectable_Select_m1582_MethodInfo,
	&Slider_OnInitializePotentialDrag_m1623_MethodInfo,
	&Slider_OnDrag_m1617_MethodInfo,
	&Slider_Rebuild_m1604_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m1626_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1625_MethodInfo,
	&Slider_Rebuild_m1604_MethodInfo,
	&Slider_OnDrag_m1617_MethodInfo,
	&Slider_OnInitializePotentialDrag_m1623_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1625_MethodInfo,
	&Slider_UnityEngine_UI_ICanvasElement_get_transform_m1626_MethodInfo,
};
static bool Slider_t22_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* Slider_t22_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t490_0_0_0,
	&IInitializePotentialDragHandler_t395_0_0_0,
	&IDragHandler_t397_0_0_0,
	&ICanvasElement_t411_0_0_0,
};
static Il2CppInterfaceOffsetPair Slider_t22_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t490_0_0_0, 16},
	{ &IPointerEnterHandler_t390_0_0_0, 16},
	{ &IPointerExitHandler_t391_0_0_0, 17},
	{ &IPointerDownHandler_t392_0_0_0, 18},
	{ &IPointerUpHandler_t393_0_0_0, 19},
	{ &ISelectHandler_t402_0_0_0, 20},
	{ &IDeselectHandler_t403_0_0_0, 21},
	{ &IMoveHandler_t404_0_0_0, 22},
	{ &IInitializePotentialDragHandler_t395_0_0_0, 38},
	{ &IDragHandler_t397_0_0_0, 39},
	{ &ICanvasElement_t411_0_0_0, 40},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Slider_t22_1_0_0;
struct Slider_t22;
const Il2CppTypeDefinitionMetadata Slider_t22_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Slider_t22_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Slider_t22_InterfacesTypeInfos/* implementedInterfaces */
	, Slider_t22_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t259_0_0_0/* parent */
	, Slider_t22_VTable/* vtableMethods */
	, Slider_t22_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 473/* fieldStart */

};
TypeInfo Slider_t22_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Slider"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Slider_t22_MethodInfos/* methods */
	, Slider_t22_PropertyInfos/* properties */
	, NULL/* events */
	, &Slider_t22_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 243/* custom_attributes_cache */
	, &Slider_t22_0_0_0/* byval_arg */
	, &Slider_t22_1_0_0/* this_arg */
	, &Slider_t22_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Slider_t22)/* instance_size */
	, sizeof (Slider_t22)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 43/* method_count */
	, 12/* property_count */
	, 15/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 48/* vtable_count */
	, 4/* interfaces_count */
	, 11/* interface_offsets_count */

};
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
// Metadata Definition UnityEngine.UI.SpriteState
extern TypeInfo SpriteState_t339_il2cpp_TypeInfo;
// UnityEngine.UI.SpriteState
#include "UnityEngine_UI_UnityEngine_UI_SpriteStateMethodDeclarations.h"
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_highlightedSprite()
extern const MethodInfo SpriteState_get_highlightedSprite_m1627_MethodInfo = 
{
	"get_highlightedSprite"/* name */
	, (methodPointerType)&SpriteState_get_highlightedSprite_m1627/* method */
	, &SpriteState_t339_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t292_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 928/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t292_0_0_0;
static const ParameterInfo SpriteState_t339_SpriteState_set_highlightedSprite_m1628_ParameterInfos[] = 
{
	{"value", 0, 134218282, 0, &Sprite_t292_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_highlightedSprite(UnityEngine.Sprite)
extern const MethodInfo SpriteState_set_highlightedSprite_m1628_MethodInfo = 
{
	"set_highlightedSprite"/* name */
	, (methodPointerType)&SpriteState_set_highlightedSprite_m1628/* method */
	, &SpriteState_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, SpriteState_t339_SpriteState_set_highlightedSprite_m1628_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 929/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_pressedSprite()
extern const MethodInfo SpriteState_get_pressedSprite_m1629_MethodInfo = 
{
	"get_pressedSprite"/* name */
	, (methodPointerType)&SpriteState_get_pressedSprite_m1629/* method */
	, &SpriteState_t339_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t292_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 930/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t292_0_0_0;
static const ParameterInfo SpriteState_t339_SpriteState_set_pressedSprite_m1630_ParameterInfos[] = 
{
	{"value", 0, 134218283, 0, &Sprite_t292_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_pressedSprite(UnityEngine.Sprite)
extern const MethodInfo SpriteState_set_pressedSprite_m1630_MethodInfo = 
{
	"set_pressedSprite"/* name */
	, (methodPointerType)&SpriteState_set_pressedSprite_m1630/* method */
	, &SpriteState_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, SpriteState_t339_SpriteState_set_pressedSprite_m1630_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 931/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_disabledSprite()
extern const MethodInfo SpriteState_get_disabledSprite_m1631_MethodInfo = 
{
	"get_disabledSprite"/* name */
	, (methodPointerType)&SpriteState_get_disabledSprite_m1631/* method */
	, &SpriteState_t339_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t292_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 932/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Sprite_t292_0_0_0;
static const ParameterInfo SpriteState_t339_SpriteState_set_disabledSprite_m1632_ParameterInfos[] = 
{
	{"value", 0, 134218284, 0, &Sprite_t292_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.SpriteState::set_disabledSprite(UnityEngine.Sprite)
extern const MethodInfo SpriteState_set_disabledSprite_m1632_MethodInfo = 
{
	"set_disabledSprite"/* name */
	, (methodPointerType)&SpriteState_set_disabledSprite_m1632/* method */
	, &SpriteState_t339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, SpriteState_t339_SpriteState_set_disabledSprite_m1632_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 933/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* SpriteState_t339_MethodInfos[] =
{
	&SpriteState_get_highlightedSprite_m1627_MethodInfo,
	&SpriteState_set_highlightedSprite_m1628_MethodInfo,
	&SpriteState_get_pressedSprite_m1629_MethodInfo,
	&SpriteState_set_pressedSprite_m1630_MethodInfo,
	&SpriteState_get_disabledSprite_m1631_MethodInfo,
	&SpriteState_set_disabledSprite_m1632_MethodInfo,
	NULL
};
extern const MethodInfo SpriteState_get_highlightedSprite_m1627_MethodInfo;
extern const MethodInfo SpriteState_set_highlightedSprite_m1628_MethodInfo;
static const PropertyInfo SpriteState_t339____highlightedSprite_PropertyInfo = 
{
	&SpriteState_t339_il2cpp_TypeInfo/* parent */
	, "highlightedSprite"/* name */
	, &SpriteState_get_highlightedSprite_m1627_MethodInfo/* get */
	, &SpriteState_set_highlightedSprite_m1628_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SpriteState_get_pressedSprite_m1629_MethodInfo;
extern const MethodInfo SpriteState_set_pressedSprite_m1630_MethodInfo;
static const PropertyInfo SpriteState_t339____pressedSprite_PropertyInfo = 
{
	&SpriteState_t339_il2cpp_TypeInfo/* parent */
	, "pressedSprite"/* name */
	, &SpriteState_get_pressedSprite_m1629_MethodInfo/* get */
	, &SpriteState_set_pressedSprite_m1630_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo SpriteState_get_disabledSprite_m1631_MethodInfo;
extern const MethodInfo SpriteState_set_disabledSprite_m1632_MethodInfo;
static const PropertyInfo SpriteState_t339____disabledSprite_PropertyInfo = 
{
	&SpriteState_t339_il2cpp_TypeInfo/* parent */
	, "disabledSprite"/* name */
	, &SpriteState_get_disabledSprite_m1631_MethodInfo/* get */
	, &SpriteState_set_disabledSprite_m1632_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* SpriteState_t339_PropertyInfos[] =
{
	&SpriteState_t339____highlightedSprite_PropertyInfo,
	&SpriteState_t339____pressedSprite_PropertyInfo,
	&SpriteState_t339____disabledSprite_PropertyInfo,
	NULL
};
extern const MethodInfo ValueType_Equals_m2567_MethodInfo;
extern const MethodInfo ValueType_GetHashCode_m2568_MethodInfo;
extern const MethodInfo ValueType_ToString_m2571_MethodInfo;
static const Il2CppMethodReference SpriteState_t339_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&ValueType_GetHashCode_m2568_MethodInfo,
	&ValueType_ToString_m2571_MethodInfo,
};
static bool SpriteState_t339_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType SpriteState_t339_1_0_0;
const Il2CppTypeDefinitionMetadata SpriteState_t339_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, SpriteState_t339_VTable/* vtableMethods */
	, SpriteState_t339_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 488/* fieldStart */

};
TypeInfo SpriteState_t339_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpriteState"/* name */
	, "UnityEngine.UI"/* namespaze */
	, SpriteState_t339_MethodInfos/* methods */
	, SpriteState_t339_PropertyInfos/* properties */
	, NULL/* events */
	, &SpriteState_t339_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &SpriteState_t339_0_0_0/* byval_arg */
	, &SpriteState_t339_1_0_0/* this_arg */
	, &SpriteState_t339_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpriteState_t339)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (SpriteState_t339)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 6/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntry.h"
// Metadata Definition UnityEngine.UI.StencilMaterial/MatEntry
extern TypeInfo MatEntry_t344_il2cpp_TypeInfo;
// UnityEngine.UI.StencilMaterial/MatEntry
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatEntryMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial/MatEntry::.ctor()
extern const MethodInfo MatEntry__ctor_m1633_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MatEntry__ctor_m1633/* method */
	, &MatEntry_t344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 937/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* MatEntry_t344_MethodInfos[] =
{
	&MatEntry__ctor_m1633_MethodInfo,
	NULL
};
static const Il2CppMethodReference MatEntry_t344_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool MatEntry_t344_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType MatEntry_t344_0_0_0;
extern const Il2CppType MatEntry_t344_1_0_0;
extern TypeInfo StencilMaterial_t346_il2cpp_TypeInfo;
extern const Il2CppType StencilMaterial_t346_0_0_0;
struct MatEntry_t344;
const Il2CppTypeDefinitionMetadata MatEntry_t344_DefinitionMetadata = 
{
	&StencilMaterial_t346_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, MatEntry_t344_VTable/* vtableMethods */
	, MatEntry_t344_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 491/* fieldStart */

};
TypeInfo MatEntry_t344_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "MatEntry"/* name */
	, ""/* namespaze */
	, MatEntry_t344_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &MatEntry_t344_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &MatEntry_t344_0_0_0/* byval_arg */
	, &MatEntry_t344_1_0_0/* this_arg */
	, &MatEntry_t344_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MatEntry_t344)/* instance_size */
	, sizeof (MatEntry_t344)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048579/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial.h"
// Metadata Definition UnityEngine.UI.StencilMaterial
// UnityEngine.UI.StencilMaterial
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterialMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial::.cctor()
extern const MethodInfo StencilMaterial__cctor_m1634_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StencilMaterial__cctor_m1634/* method */
	, &StencilMaterial_t346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 934/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t4_0_0_0;
extern const Il2CppType Material_t4_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo StencilMaterial_t346_StencilMaterial_Add_m1635_ParameterInfos[] = 
{
	{"baseMat", 0, 134218285, 0, &Material_t4_0_0_0},
	{"stencilID", 1, 134218286, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.StencilMaterial::Add(UnityEngine.Material,System.Int32)
extern const MethodInfo StencilMaterial_Add_m1635_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&StencilMaterial_Add_m1635/* method */
	, &StencilMaterial_t346_il2cpp_TypeInfo/* declaring_type */
	, &Material_t4_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Int32_t127/* invoker_method */
	, StencilMaterial_t346_StencilMaterial_Add_m1635_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 935/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t4_0_0_0;
static const ParameterInfo StencilMaterial_t346_StencilMaterial_Remove_m1636_ParameterInfos[] = 
{
	{"customMat", 0, 134218287, 0, &Material_t4_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.StencilMaterial::Remove(UnityEngine.Material)
extern const MethodInfo StencilMaterial_Remove_m1636_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&StencilMaterial_Remove_m1636/* method */
	, &StencilMaterial_t346_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, StencilMaterial_t346_StencilMaterial_Remove_m1636_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 936/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* StencilMaterial_t346_MethodInfos[] =
{
	&StencilMaterial__cctor_m1634_MethodInfo,
	&StencilMaterial_Add_m1635_MethodInfo,
	&StencilMaterial_Remove_m1636_MethodInfo,
	NULL
};
static const Il2CppType* StencilMaterial_t346_il2cpp_TypeInfo__nestedTypes[1] =
{
	&MatEntry_t344_0_0_0,
};
static const Il2CppMethodReference StencilMaterial_t346_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool StencilMaterial_t346_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType StencilMaterial_t346_1_0_0;
struct StencilMaterial_t346;
const Il2CppTypeDefinitionMetadata StencilMaterial_t346_DefinitionMetadata = 
{
	NULL/* declaringType */
	, StencilMaterial_t346_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, StencilMaterial_t346_VTable/* vtableMethods */
	, StencilMaterial_t346_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 495/* fieldStart */

};
TypeInfo StencilMaterial_t346_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "StencilMaterial"/* name */
	, "UnityEngine.UI"/* namespaze */
	, StencilMaterial_t346_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &StencilMaterial_t346_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &StencilMaterial_t346_0_0_0/* byval_arg */
	, &StencilMaterial_t346_1_0_0/* this_arg */
	, &StencilMaterial_t346_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StencilMaterial_t346)/* instance_size */
	, sizeof (StencilMaterial_t346)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StencilMaterial_t346_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
// Metadata Definition UnityEngine.UI.Text
extern TypeInfo Text_t23_il2cpp_TypeInfo;
// UnityEngine.UI.Text
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::.ctor()
extern const MethodInfo Text__ctor_m1637_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Text__ctor_m1637/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 938/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::.cctor()
extern const MethodInfo Text__cctor_m1638_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Text__cctor_m1638/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 939/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextGenerator_t314_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerator UnityEngine.UI.Text::get_cachedTextGenerator()
extern const MethodInfo Text_get_cachedTextGenerator_m1639_MethodInfo = 
{
	"get_cachedTextGenerator"/* name */
	, (methodPointerType)&Text_get_cachedTextGenerator_m1639/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerator_t314_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 940/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerator UnityEngine.UI.Text::get_cachedTextGeneratorForLayout()
extern const MethodInfo Text_get_cachedTextGeneratorForLayout_m1640_MethodInfo = 
{
	"get_cachedTextGeneratorForLayout"/* name */
	, (methodPointerType)&Text_get_cachedTextGeneratorForLayout_m1640/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerator_t314_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 941/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.Text::get_defaultMaterial()
extern const MethodInfo Text_get_defaultMaterial_m1641_MethodInfo = 
{
	"get_defaultMaterial"/* name */
	, (methodPointerType)&Text_get_defaultMaterial_m1641/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Material_t4_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 942/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Texture_t321_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Texture UnityEngine.UI.Text::get_mainTexture()
extern const MethodInfo Text_get_mainTexture_m1642_MethodInfo = 
{
	"get_mainTexture"/* name */
	, (methodPointerType)&Text_get_mainTexture_m1642/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Texture_t321_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2246/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 943/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::FontTextureChanged()
extern const MethodInfo Text_FontTextureChanged_m1643_MethodInfo = 
{
	"FontTextureChanged"/* name */
	, (methodPointerType)&Text_FontTextureChanged_m1643/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 944/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Font_t266_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Font UnityEngine.UI.Text::get_font()
extern const MethodInfo Text_get_font_m1644_MethodInfo = 
{
	"get_font"/* name */
	, (methodPointerType)&Text_get_font_m1644/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Font_t266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 945/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Font_t266_0_0_0;
static const ParameterInfo Text_t23_Text_set_font_m1645_ParameterInfos[] = 
{
	{"value", 0, 134218288, 0, &Font_t266_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_font(UnityEngine.Font)
extern const MethodInfo Text_set_font_m1645_MethodInfo = 
{
	"set_font"/* name */
	, (methodPointerType)&Text_set_font_m1645/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Text_t23_Text_set_font_m1645_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 946/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.UI.Text::get_text()
extern const MethodInfo Text_get_text_m1646_MethodInfo = 
{
	"get_text"/* name */
	, (methodPointerType)&Text_get_text_m1646/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 47/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 947/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType String_t_0_0_0;
static const ParameterInfo Text_t23_Text_set_text_m1647_ParameterInfos[] = 
{
	{"value", 0, 134218289, 0, &String_t_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_text(System.String)
extern const MethodInfo Text_set_text_m1647_MethodInfo = 
{
	"set_text"/* name */
	, (methodPointerType)&Text_set_text_m1647/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Text_t23_Text_set_text_m1647_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 48/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 948/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Text::get_supportRichText()
extern const MethodInfo Text_get_supportRichText_m1648_MethodInfo = 
{
	"get_supportRichText"/* name */
	, (methodPointerType)&Text_get_supportRichText_m1648/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 949/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Text_t23_Text_set_supportRichText_m1649_ParameterInfos[] = 
{
	{"value", 0, 134218290, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_supportRichText(System.Boolean)
extern const MethodInfo Text_set_supportRichText_m1649_MethodInfo = 
{
	"set_supportRichText"/* name */
	, (methodPointerType)&Text_set_supportRichText_m1649/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Text_t23_Text_set_supportRichText_m1649_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 950/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Text::get_resizeTextForBestFit()
extern const MethodInfo Text_get_resizeTextForBestFit_m1650_MethodInfo = 
{
	"get_resizeTextForBestFit"/* name */
	, (methodPointerType)&Text_get_resizeTextForBestFit_m1650/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 951/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Text_t23_Text_set_resizeTextForBestFit_m1651_ParameterInfos[] = 
{
	{"value", 0, 134218291, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextForBestFit(System.Boolean)
extern const MethodInfo Text_set_resizeTextForBestFit_m1651_MethodInfo = 
{
	"set_resizeTextForBestFit"/* name */
	, (methodPointerType)&Text_set_resizeTextForBestFit_m1651/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Text_t23_Text_set_resizeTextForBestFit_m1651_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 952/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_resizeTextMinSize()
extern const MethodInfo Text_get_resizeTextMinSize_m1652_MethodInfo = 
{
	"get_resizeTextMinSize"/* name */
	, (methodPointerType)&Text_get_resizeTextMinSize_m1652/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 953/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Text_t23_Text_set_resizeTextMinSize_m1653_ParameterInfos[] = 
{
	{"value", 0, 134218292, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextMinSize(System.Int32)
extern const MethodInfo Text_set_resizeTextMinSize_m1653_MethodInfo = 
{
	"set_resizeTextMinSize"/* name */
	, (methodPointerType)&Text_set_resizeTextMinSize_m1653/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Text_t23_Text_set_resizeTextMinSize_m1653_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 954/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_resizeTextMaxSize()
extern const MethodInfo Text_get_resizeTextMaxSize_m1654_MethodInfo = 
{
	"get_resizeTextMaxSize"/* name */
	, (methodPointerType)&Text_get_resizeTextMaxSize_m1654/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 955/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Text_t23_Text_set_resizeTextMaxSize_m1655_ParameterInfos[] = 
{
	{"value", 0, 134218293, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_resizeTextMaxSize(System.Int32)
extern const MethodInfo Text_set_resizeTextMaxSize_m1655_MethodInfo = 
{
	"set_resizeTextMaxSize"/* name */
	, (methodPointerType)&Text_set_resizeTextMaxSize_m1655/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Text_t23_Text_set_resizeTextMaxSize_m1655_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 956/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t471_0_0_0;
extern void* RuntimeInvoker_TextAnchor_t471 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextAnchor UnityEngine.UI.Text::get_alignment()
extern const MethodInfo Text_get_alignment_m1656_MethodInfo = 
{
	"get_alignment"/* name */
	, (methodPointerType)&Text_get_alignment_m1656/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &TextAnchor_t471_0_0_0/* return_type */
	, RuntimeInvoker_TextAnchor_t471/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 957/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t471_0_0_0;
static const ParameterInfo Text_t23_Text_set_alignment_m1657_ParameterInfos[] = 
{
	{"value", 0, 134218294, 0, &TextAnchor_t471_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_alignment(UnityEngine.TextAnchor)
extern const MethodInfo Text_set_alignment_m1657_MethodInfo = 
{
	"set_alignment"/* name */
	, (methodPointerType)&Text_set_alignment_m1657/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Text_t23_Text_set_alignment_m1657_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 958/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_fontSize()
extern const MethodInfo Text_get_fontSize_m1658_MethodInfo = 
{
	"get_fontSize"/* name */
	, (methodPointerType)&Text_get_fontSize_m1658/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 959/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo Text_t23_Text_set_fontSize_m1659_ParameterInfos[] = 
{
	{"value", 0, 134218295, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_fontSize(System.Int32)
extern const MethodInfo Text_set_fontSize_m1659_MethodInfo = 
{
	"set_fontSize"/* name */
	, (methodPointerType)&Text_set_fontSize_m1659/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Text_t23_Text_set_fontSize_m1659_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 960/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HorizontalWrapMode_t532_0_0_0;
extern void* RuntimeInvoker_HorizontalWrapMode_t532 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.HorizontalWrapMode UnityEngine.UI.Text::get_horizontalOverflow()
extern const MethodInfo Text_get_horizontalOverflow_m1660_MethodInfo = 
{
	"get_horizontalOverflow"/* name */
	, (methodPointerType)&Text_get_horizontalOverflow_m1660/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &HorizontalWrapMode_t532_0_0_0/* return_type */
	, RuntimeInvoker_HorizontalWrapMode_t532/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 961/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType HorizontalWrapMode_t532_0_0_0;
static const ParameterInfo Text_t23_Text_set_horizontalOverflow_m1661_ParameterInfos[] = 
{
	{"value", 0, 134218296, 0, &HorizontalWrapMode_t532_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_horizontalOverflow(UnityEngine.HorizontalWrapMode)
extern const MethodInfo Text_set_horizontalOverflow_m1661_MethodInfo = 
{
	"set_horizontalOverflow"/* name */
	, (methodPointerType)&Text_set_horizontalOverflow_m1661/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Text_t23_Text_set_horizontalOverflow_m1661_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 962/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VerticalWrapMode_t533_0_0_0;
extern void* RuntimeInvoker_VerticalWrapMode_t533 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.VerticalWrapMode UnityEngine.UI.Text::get_verticalOverflow()
extern const MethodInfo Text_get_verticalOverflow_m1662_MethodInfo = 
{
	"get_verticalOverflow"/* name */
	, (methodPointerType)&Text_get_verticalOverflow_m1662/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &VerticalWrapMode_t533_0_0_0/* return_type */
	, RuntimeInvoker_VerticalWrapMode_t533/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 963/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType VerticalWrapMode_t533_0_0_0;
static const ParameterInfo Text_t23_Text_set_verticalOverflow_m1663_ParameterInfos[] = 
{
	{"value", 0, 134218297, 0, &VerticalWrapMode_t533_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_verticalOverflow(UnityEngine.VerticalWrapMode)
extern const MethodInfo Text_set_verticalOverflow_m1663_MethodInfo = 
{
	"set_verticalOverflow"/* name */
	, (methodPointerType)&Text_set_verticalOverflow_m1663/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Text_t23_Text_set_verticalOverflow_m1663_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 964/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_lineSpacing()
extern const MethodInfo Text_get_lineSpacing_m1664_MethodInfo = 
{
	"get_lineSpacing"/* name */
	, (methodPointerType)&Text_get_lineSpacing_m1664/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 965/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Text_t23_Text_set_lineSpacing_m1665_ParameterInfos[] = 
{
	{"value", 0, 134218298, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_lineSpacing(System.Single)
extern const MethodInfo Text_set_lineSpacing_m1665_MethodInfo = 
{
	"set_lineSpacing"/* name */
	, (methodPointerType)&Text_set_lineSpacing_m1665/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, Text_t23_Text_set_lineSpacing_m1665_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 966/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FontStyle_t531_0_0_0;
extern void* RuntimeInvoker_FontStyle_t531 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.FontStyle UnityEngine.UI.Text::get_fontStyle()
extern const MethodInfo Text_get_fontStyle_m1666_MethodInfo = 
{
	"get_fontStyle"/* name */
	, (methodPointerType)&Text_get_fontStyle_m1666/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &FontStyle_t531_0_0_0/* return_type */
	, RuntimeInvoker_FontStyle_t531/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 967/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FontStyle_t531_0_0_0;
static const ParameterInfo Text_t23_Text_set_fontStyle_m1667_ParameterInfos[] = 
{
	{"value", 0, 134218299, 0, &FontStyle_t531_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::set_fontStyle(UnityEngine.FontStyle)
extern const MethodInfo Text_set_fontStyle_m1667_MethodInfo = 
{
	"set_fontStyle"/* name */
	, (methodPointerType)&Text_set_fontStyle_m1667/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Text_t23_Text_set_fontStyle_m1667_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 968/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_pixelsPerUnit()
extern const MethodInfo Text_get_pixelsPerUnit_m1668_MethodInfo = 
{
	"get_pixelsPerUnit"/* name */
	, (methodPointerType)&Text_get_pixelsPerUnit_m1668/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 969/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnEnable()
extern const MethodInfo Text_OnEnable_m1669_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Text_OnEnable_m1669/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 970/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnDisable()
extern const MethodInfo Text_OnDisable_m1670_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Text_OnDisable_m1670/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 971/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::UpdateGeometry()
extern const MethodInfo Text_UpdateGeometry_m1671_MethodInfo = 
{
	"UpdateGeometry"/* name */
	, (methodPointerType)&Text_UpdateGeometry_m1671/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 972/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo Text_t23_Text_GetGenerationSettings_m1672_ParameterInfos[] = 
{
	{"extents", 0, 134218300, 0, &Vector2_t10_0_0_0},
};
extern const Il2CppType TextGenerationSettings_t416_0_0_0;
extern void* RuntimeInvoker_TextGenerationSettings_t416_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextGenerationSettings UnityEngine.UI.Text::GetGenerationSettings(UnityEngine.Vector2)
extern const MethodInfo Text_GetGenerationSettings_m1672_MethodInfo = 
{
	"GetGenerationSettings"/* name */
	, (methodPointerType)&Text_GetGenerationSettings_m1672/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &TextGenerationSettings_t416_0_0_0/* return_type */
	, RuntimeInvoker_TextGenerationSettings_t416_Vector2_t10/* invoker_method */
	, Text_t23_Text_GetGenerationSettings_m1672_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 973/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t471_0_0_0;
static const ParameterInfo Text_t23_Text_GetTextAnchorPivot_m1673_ParameterInfos[] = 
{
	{"anchor", 0, 134218301, 0, &TextAnchor_t471_0_0_0},
};
extern void* RuntimeInvoker_Vector2_t10_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.Text::GetTextAnchorPivot(UnityEngine.TextAnchor)
extern const MethodInfo Text_GetTextAnchorPivot_m1673_MethodInfo = 
{
	"GetTextAnchorPivot"/* name */
	, (methodPointerType)&Text_GetTextAnchorPivot_m1673/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10_Int32_t127/* invoker_method */
	, Text_t23_Text_GetTextAnchorPivot_m1673_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 974/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t315_0_0_0;
extern const Il2CppType List_1_t315_0_0_0;
static const ParameterInfo Text_t23_Text_OnFillVBO_m1674_ParameterInfos[] = 
{
	{"vbo", 0, 134218302, 0, &List_1_t315_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo Text_OnFillVBO_m1674_MethodInfo = 
{
	"OnFillVBO"/* name */
	, (methodPointerType)&Text_OnFillVBO_m1674/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Text_t23_Text_OnFillVBO_m1674_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 975/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::CalculateLayoutInputHorizontal()
extern const MethodInfo Text_CalculateLayoutInputHorizontal_m1675_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&Text_CalculateLayoutInputHorizontal_m1675/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 49/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 976/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Text::CalculateLayoutInputVertical()
extern const MethodInfo Text_CalculateLayoutInputVertical_m1676_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&Text_CalculateLayoutInputVertical_m1676/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 50/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 977/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_minWidth()
extern const MethodInfo Text_get_minWidth_m1677_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&Text_get_minWidth_m1677/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 51/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 978/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_preferredWidth()
extern const MethodInfo Text_get_preferredWidth_m1678_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&Text_get_preferredWidth_m1678/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 52/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 979/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_flexibleWidth()
extern const MethodInfo Text_get_flexibleWidth_m1679_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&Text_get_flexibleWidth_m1679/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 53/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 980/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_minHeight()
extern const MethodInfo Text_get_minHeight_m1680_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&Text_get_minHeight_m1680/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 54/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 981/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_preferredHeight()
extern const MethodInfo Text_get_preferredHeight_m1681_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&Text_get_preferredHeight_m1681/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 55/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 982/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.Text::get_flexibleHeight()
extern const MethodInfo Text_get_flexibleHeight_m1682_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&Text_get_flexibleHeight_m1682/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 56/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 983/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.Text::get_layoutPriority()
extern const MethodInfo Text_get_layoutPriority_m1683_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&Text_get_layoutPriority_m1683/* method */
	, &Text_t23_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 57/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 984/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Text_t23_MethodInfos[] =
{
	&Text__ctor_m1637_MethodInfo,
	&Text__cctor_m1638_MethodInfo,
	&Text_get_cachedTextGenerator_m1639_MethodInfo,
	&Text_get_cachedTextGeneratorForLayout_m1640_MethodInfo,
	&Text_get_defaultMaterial_m1641_MethodInfo,
	&Text_get_mainTexture_m1642_MethodInfo,
	&Text_FontTextureChanged_m1643_MethodInfo,
	&Text_get_font_m1644_MethodInfo,
	&Text_set_font_m1645_MethodInfo,
	&Text_get_text_m1646_MethodInfo,
	&Text_set_text_m1647_MethodInfo,
	&Text_get_supportRichText_m1648_MethodInfo,
	&Text_set_supportRichText_m1649_MethodInfo,
	&Text_get_resizeTextForBestFit_m1650_MethodInfo,
	&Text_set_resizeTextForBestFit_m1651_MethodInfo,
	&Text_get_resizeTextMinSize_m1652_MethodInfo,
	&Text_set_resizeTextMinSize_m1653_MethodInfo,
	&Text_get_resizeTextMaxSize_m1654_MethodInfo,
	&Text_set_resizeTextMaxSize_m1655_MethodInfo,
	&Text_get_alignment_m1656_MethodInfo,
	&Text_set_alignment_m1657_MethodInfo,
	&Text_get_fontSize_m1658_MethodInfo,
	&Text_set_fontSize_m1659_MethodInfo,
	&Text_get_horizontalOverflow_m1660_MethodInfo,
	&Text_set_horizontalOverflow_m1661_MethodInfo,
	&Text_get_verticalOverflow_m1662_MethodInfo,
	&Text_set_verticalOverflow_m1663_MethodInfo,
	&Text_get_lineSpacing_m1664_MethodInfo,
	&Text_set_lineSpacing_m1665_MethodInfo,
	&Text_get_fontStyle_m1666_MethodInfo,
	&Text_set_fontStyle_m1667_MethodInfo,
	&Text_get_pixelsPerUnit_m1668_MethodInfo,
	&Text_OnEnable_m1669_MethodInfo,
	&Text_OnDisable_m1670_MethodInfo,
	&Text_UpdateGeometry_m1671_MethodInfo,
	&Text_GetGenerationSettings_m1672_MethodInfo,
	&Text_GetTextAnchorPivot_m1673_MethodInfo,
	&Text_OnFillVBO_m1674_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m1675_MethodInfo,
	&Text_CalculateLayoutInputVertical_m1676_MethodInfo,
	&Text_get_minWidth_m1677_MethodInfo,
	&Text_get_preferredWidth_m1678_MethodInfo,
	&Text_get_flexibleWidth_m1679_MethodInfo,
	&Text_get_minHeight_m1680_MethodInfo,
	&Text_get_preferredHeight_m1681_MethodInfo,
	&Text_get_flexibleHeight_m1682_MethodInfo,
	&Text_get_layoutPriority_m1683_MethodInfo,
	NULL
};
extern const MethodInfo Text_get_cachedTextGenerator_m1639_MethodInfo;
static const PropertyInfo Text_t23____cachedTextGenerator_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "cachedTextGenerator"/* name */
	, &Text_get_cachedTextGenerator_m1639_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_cachedTextGeneratorForLayout_m1640_MethodInfo;
static const PropertyInfo Text_t23____cachedTextGeneratorForLayout_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "cachedTextGeneratorForLayout"/* name */
	, &Text_get_cachedTextGeneratorForLayout_m1640_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_defaultMaterial_m1641_MethodInfo;
static const PropertyInfo Text_t23____defaultMaterial_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "defaultMaterial"/* name */
	, &Text_get_defaultMaterial_m1641_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_mainTexture_m1642_MethodInfo;
static const PropertyInfo Text_t23____mainTexture_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "mainTexture"/* name */
	, &Text_get_mainTexture_m1642_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_font_m1644_MethodInfo;
extern const MethodInfo Text_set_font_m1645_MethodInfo;
static const PropertyInfo Text_t23____font_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "font"/* name */
	, &Text_get_font_m1644_MethodInfo/* get */
	, &Text_set_font_m1645_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_text_m1646_MethodInfo;
extern const MethodInfo Text_set_text_m1647_MethodInfo;
static const PropertyInfo Text_t23____text_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "text"/* name */
	, &Text_get_text_m1646_MethodInfo/* get */
	, &Text_set_text_m1647_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_supportRichText_m1648_MethodInfo;
extern const MethodInfo Text_set_supportRichText_m1649_MethodInfo;
static const PropertyInfo Text_t23____supportRichText_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "supportRichText"/* name */
	, &Text_get_supportRichText_m1648_MethodInfo/* get */
	, &Text_set_supportRichText_m1649_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_resizeTextForBestFit_m1650_MethodInfo;
extern const MethodInfo Text_set_resizeTextForBestFit_m1651_MethodInfo;
static const PropertyInfo Text_t23____resizeTextForBestFit_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "resizeTextForBestFit"/* name */
	, &Text_get_resizeTextForBestFit_m1650_MethodInfo/* get */
	, &Text_set_resizeTextForBestFit_m1651_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_resizeTextMinSize_m1652_MethodInfo;
extern const MethodInfo Text_set_resizeTextMinSize_m1653_MethodInfo;
static const PropertyInfo Text_t23____resizeTextMinSize_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "resizeTextMinSize"/* name */
	, &Text_get_resizeTextMinSize_m1652_MethodInfo/* get */
	, &Text_set_resizeTextMinSize_m1653_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_resizeTextMaxSize_m1654_MethodInfo;
extern const MethodInfo Text_set_resizeTextMaxSize_m1655_MethodInfo;
static const PropertyInfo Text_t23____resizeTextMaxSize_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "resizeTextMaxSize"/* name */
	, &Text_get_resizeTextMaxSize_m1654_MethodInfo/* get */
	, &Text_set_resizeTextMaxSize_m1655_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_alignment_m1656_MethodInfo;
extern const MethodInfo Text_set_alignment_m1657_MethodInfo;
static const PropertyInfo Text_t23____alignment_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "alignment"/* name */
	, &Text_get_alignment_m1656_MethodInfo/* get */
	, &Text_set_alignment_m1657_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_fontSize_m1658_MethodInfo;
extern const MethodInfo Text_set_fontSize_m1659_MethodInfo;
static const PropertyInfo Text_t23____fontSize_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "fontSize"/* name */
	, &Text_get_fontSize_m1658_MethodInfo/* get */
	, &Text_set_fontSize_m1659_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_horizontalOverflow_m1660_MethodInfo;
extern const MethodInfo Text_set_horizontalOverflow_m1661_MethodInfo;
static const PropertyInfo Text_t23____horizontalOverflow_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "horizontalOverflow"/* name */
	, &Text_get_horizontalOverflow_m1660_MethodInfo/* get */
	, &Text_set_horizontalOverflow_m1661_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_verticalOverflow_m1662_MethodInfo;
extern const MethodInfo Text_set_verticalOverflow_m1663_MethodInfo;
static const PropertyInfo Text_t23____verticalOverflow_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "verticalOverflow"/* name */
	, &Text_get_verticalOverflow_m1662_MethodInfo/* get */
	, &Text_set_verticalOverflow_m1663_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_lineSpacing_m1664_MethodInfo;
extern const MethodInfo Text_set_lineSpacing_m1665_MethodInfo;
static const PropertyInfo Text_t23____lineSpacing_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "lineSpacing"/* name */
	, &Text_get_lineSpacing_m1664_MethodInfo/* get */
	, &Text_set_lineSpacing_m1665_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_fontStyle_m1666_MethodInfo;
extern const MethodInfo Text_set_fontStyle_m1667_MethodInfo;
static const PropertyInfo Text_t23____fontStyle_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "fontStyle"/* name */
	, &Text_get_fontStyle_m1666_MethodInfo/* get */
	, &Text_set_fontStyle_m1667_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_pixelsPerUnit_m1668_MethodInfo;
static const PropertyInfo Text_t23____pixelsPerUnit_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "pixelsPerUnit"/* name */
	, &Text_get_pixelsPerUnit_m1668_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_minWidth_m1677_MethodInfo;
static const PropertyInfo Text_t23____minWidth_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &Text_get_minWidth_m1677_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_preferredWidth_m1678_MethodInfo;
static const PropertyInfo Text_t23____preferredWidth_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &Text_get_preferredWidth_m1678_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_flexibleWidth_m1679_MethodInfo;
static const PropertyInfo Text_t23____flexibleWidth_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &Text_get_flexibleWidth_m1679_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_minHeight_m1680_MethodInfo;
static const PropertyInfo Text_t23____minHeight_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &Text_get_minHeight_m1680_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_preferredHeight_m1681_MethodInfo;
static const PropertyInfo Text_t23____preferredHeight_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &Text_get_preferredHeight_m1681_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_flexibleHeight_m1682_MethodInfo;
static const PropertyInfo Text_t23____flexibleHeight_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &Text_get_flexibleHeight_m1682_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Text_get_layoutPriority_m1683_MethodInfo;
static const PropertyInfo Text_t23____layoutPriority_PropertyInfo = 
{
	&Text_t23_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &Text_get_layoutPriority_m1683_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Text_t23_PropertyInfos[] =
{
	&Text_t23____cachedTextGenerator_PropertyInfo,
	&Text_t23____cachedTextGeneratorForLayout_PropertyInfo,
	&Text_t23____defaultMaterial_PropertyInfo,
	&Text_t23____mainTexture_PropertyInfo,
	&Text_t23____font_PropertyInfo,
	&Text_t23____text_PropertyInfo,
	&Text_t23____supportRichText_PropertyInfo,
	&Text_t23____resizeTextForBestFit_PropertyInfo,
	&Text_t23____resizeTextMinSize_PropertyInfo,
	&Text_t23____resizeTextMaxSize_PropertyInfo,
	&Text_t23____alignment_PropertyInfo,
	&Text_t23____fontSize_PropertyInfo,
	&Text_t23____horizontalOverflow_PropertyInfo,
	&Text_t23____verticalOverflow_PropertyInfo,
	&Text_t23____lineSpacing_PropertyInfo,
	&Text_t23____fontStyle_PropertyInfo,
	&Text_t23____pixelsPerUnit_PropertyInfo,
	&Text_t23____minWidth_PropertyInfo,
	&Text_t23____preferredWidth_PropertyInfo,
	&Text_t23____flexibleWidth_PropertyInfo,
	&Text_t23____minHeight_PropertyInfo,
	&Text_t23____preferredHeight_PropertyInfo,
	&Text_t23____flexibleHeight_PropertyInfo,
	&Text_t23____layoutPriority_PropertyInfo,
	NULL
};
extern const MethodInfo Text_OnEnable_m1669_MethodInfo;
extern const MethodInfo Text_OnDisable_m1670_MethodInfo;
extern const MethodInfo Graphic_OnRectTransformDimensionsChange_m1134_MethodInfo;
extern const MethodInfo Graphic_OnBeforeTransformParentChanged_m1135_MethodInfo;
extern const MethodInfo MaskableGraphic_OnTransformParentChanged_m1393_MethodInfo;
extern const MethodInfo Graphic_OnDidApplyAnimationProperties_m1155_MethodInfo;
extern const MethodInfo Graphic_OnCanvasHierarchyChanged_m1150_MethodInfo;
extern const MethodInfo Graphic_Rebuild_m1151_MethodInfo;
extern const MethodInfo Graphic_UnityEngine_UI_ICanvasElement_get_transform_m1173_MethodInfo;
extern const MethodInfo Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m1172_MethodInfo;
extern const MethodInfo Graphic_SetAllDirty_m1130_MethodInfo;
extern const MethodInfo Graphic_SetLayoutDirty_m1131_MethodInfo;
extern const MethodInfo Graphic_SetVerticesDirty_m1132_MethodInfo;
extern const MethodInfo MaskableGraphic_SetMaterialDirty_m1396_MethodInfo;
extern const MethodInfo MaskableGraphic_get_material_m1388_MethodInfo;
extern const MethodInfo MaskableGraphic_set_material_m1389_MethodInfo;
extern const MethodInfo Graphic_get_materialForRendering_m1145_MethodInfo;
extern const MethodInfo Text_UpdateGeometry_m1671_MethodInfo;
extern const MethodInfo Graphic_UpdateMaterial_m1153_MethodInfo;
extern const MethodInfo Text_OnFillVBO_m1674_MethodInfo;
extern const MethodInfo Graphic_SetNativeSize_m1156_MethodInfo;
extern const MethodInfo Graphic_Raycast_m1157_MethodInfo;
extern const MethodInfo MaskableGraphic_ParentMaskStateChanged_m1394_MethodInfo;
extern const MethodInfo Text_CalculateLayoutInputHorizontal_m1675_MethodInfo;
extern const MethodInfo Text_CalculateLayoutInputVertical_m1676_MethodInfo;
static const Il2CppMethodReference Text_t23_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&Text_OnEnable_m1669_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&Text_OnDisable_m1670_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&Graphic_OnRectTransformDimensionsChange_m1134_MethodInfo,
	&Graphic_OnBeforeTransformParentChanged_m1135_MethodInfo,
	&MaskableGraphic_OnTransformParentChanged_m1393_MethodInfo,
	&Graphic_OnDidApplyAnimationProperties_m1155_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&Graphic_OnCanvasHierarchyChanged_m1150_MethodInfo,
	&Graphic_Rebuild_m1151_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_get_transform_m1173_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m1172_MethodInfo,
	&Graphic_SetAllDirty_m1130_MethodInfo,
	&Graphic_SetLayoutDirty_m1131_MethodInfo,
	&Graphic_SetVerticesDirty_m1132_MethodInfo,
	&MaskableGraphic_SetMaterialDirty_m1396_MethodInfo,
	&Text_get_defaultMaterial_m1641_MethodInfo,
	&MaskableGraphic_get_material_m1388_MethodInfo,
	&MaskableGraphic_set_material_m1389_MethodInfo,
	&Graphic_get_materialForRendering_m1145_MethodInfo,
	&Text_get_mainTexture_m1642_MethodInfo,
	&Graphic_Rebuild_m1151_MethodInfo,
	&Text_UpdateGeometry_m1671_MethodInfo,
	&Graphic_UpdateMaterial_m1153_MethodInfo,
	&Text_OnFillVBO_m1674_MethodInfo,
	&Graphic_SetNativeSize_m1156_MethodInfo,
	&Graphic_Raycast_m1157_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m1172_MethodInfo,
	&Graphic_UnityEngine_UI_ICanvasElement_get_transform_m1173_MethodInfo,
	&MaskableGraphic_ParentMaskStateChanged_m1394_MethodInfo,
	&MaskableGraphic_ParentMaskStateChanged_m1394_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m1675_MethodInfo,
	&Text_CalculateLayoutInputVertical_m1676_MethodInfo,
	&Text_get_minWidth_m1677_MethodInfo,
	&Text_get_preferredWidth_m1678_MethodInfo,
	&Text_get_flexibleWidth_m1679_MethodInfo,
	&Text_get_minHeight_m1680_MethodInfo,
	&Text_get_preferredHeight_m1681_MethodInfo,
	&Text_get_flexibleHeight_m1682_MethodInfo,
	&Text_get_layoutPriority_m1683_MethodInfo,
	&Text_get_text_m1646_MethodInfo,
	&Text_set_text_m1647_MethodInfo,
	&Text_CalculateLayoutInputHorizontal_m1675_MethodInfo,
	&Text_CalculateLayoutInputVertical_m1676_MethodInfo,
	&Text_get_minWidth_m1677_MethodInfo,
	&Text_get_preferredWidth_m1678_MethodInfo,
	&Text_get_flexibleWidth_m1679_MethodInfo,
	&Text_get_minHeight_m1680_MethodInfo,
	&Text_get_preferredHeight_m1681_MethodInfo,
	&Text_get_flexibleHeight_m1682_MethodInfo,
	&Text_get_layoutPriority_m1683_MethodInfo,
};
static bool Text_t23_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILayoutElement_t419_0_0_0;
static const Il2CppType* Text_t23_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t419_0_0_0,
};
extern const Il2CppType IMaskable_t478_0_0_0;
static Il2CppInterfaceOffsetPair Text_t23_InterfacesOffsets[] = 
{
	{ &IMaskable_t478_0_0_0, 36},
	{ &ICanvasElement_t411_0_0_0, 16},
	{ &ILayoutElement_t419_0_0_0, 38},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Text_t23_0_0_0;
extern const Il2CppType Text_t23_1_0_0;
extern const Il2CppType MaskableGraphic_t295_0_0_0;
struct Text_t23;
const Il2CppTypeDefinitionMetadata Text_t23_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Text_t23_InterfacesTypeInfos/* implementedInterfaces */
	, Text_t23_InterfacesOffsets/* interfaceOffsets */
	, &MaskableGraphic_t295_0_0_0/* parent */
	, Text_t23_VTable/* vtableMethods */
	, Text_t23_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 496/* fieldStart */

};
TypeInfo Text_t23_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Text"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Text_t23_MethodInfos/* methods */
	, Text_t23_PropertyInfos/* properties */
	, NULL/* events */
	, &Text_t23_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 255/* custom_attributes_cache */
	, &Text_t23_0_0_0/* byval_arg */
	, &Text_t23_1_0_0/* this_arg */
	, &Text_t23_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Text_t23)/* instance_size */
	, sizeof (Text_t23)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Text_t23_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 47/* method_count */
	, 24/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 58/* vtable_count */
	, 1/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransition.h"
// Metadata Definition UnityEngine.UI.Toggle/ToggleTransition
extern TypeInfo ToggleTransition_t347_il2cpp_TypeInfo;
// UnityEngine.UI.Toggle/ToggleTransition
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransitionMethodDeclarations.h"
static const MethodInfo* ToggleTransition_t347_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ToggleTransition_t347_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool ToggleTransition_t347_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ToggleTransition_t347_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleTransition_t347_0_0_0;
extern const Il2CppType ToggleTransition_t347_1_0_0;
extern TypeInfo Toggle_t351_il2cpp_TypeInfo;
extern const Il2CppType Toggle_t351_0_0_0;
const Il2CppTypeDefinitionMetadata ToggleTransition_t347_DefinitionMetadata = 
{
	&Toggle_t351_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ToggleTransition_t347_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, ToggleTransition_t347_VTable/* vtableMethods */
	, ToggleTransition_t347_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 502/* fieldStart */

};
TypeInfo ToggleTransition_t347_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleTransition"/* name */
	, ""/* namespaze */
	, ToggleTransition_t347_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ToggleTransition_t347_0_0_0/* byval_arg */
	, &ToggleTransition_t347_1_0_0/* this_arg */
	, &ToggleTransition_t347_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleTransition_t347)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ToggleTransition_t347)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent.h"
// Metadata Definition UnityEngine.UI.Toggle/ToggleEvent
extern TypeInfo ToggleEvent_t348_il2cpp_TypeInfo;
// UnityEngine.UI.Toggle/ToggleEvent
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEventMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle/ToggleEvent::.ctor()
extern const MethodInfo ToggleEvent__ctor_m1684_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ToggleEvent__ctor_m1684/* method */
	, &ToggleEvent_t348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1003/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ToggleEvent_t348_MethodInfos[] =
{
	&ToggleEvent__ctor_m1684_MethodInfo,
	NULL
};
extern const Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m2582_GenericMethod;
extern const Il2CppGenericMethod UnityEvent_1_GetDelegate_m2583_GenericMethod;
static const Il2CppMethodReference ToggleEvent_t348_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&UnityEventBase_ToString_m2549_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m2550_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2551_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m2582_GenericMethod,
	&UnityEvent_1_GetDelegate_m2583_GenericMethod,
};
static bool ToggleEvent_t348_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	true,
	true,
};
static Il2CppInterfaceOffsetPair ToggleEvent_t348_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t510_0_0_0, 4},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleEvent_t348_0_0_0;
extern const Il2CppType ToggleEvent_t348_1_0_0;
extern const Il2CppType UnityEvent_1_t349_0_0_0;
struct ToggleEvent_t348;
const Il2CppTypeDefinitionMetadata ToggleEvent_t348_DefinitionMetadata = 
{
	&Toggle_t351_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ToggleEvent_t348_InterfacesOffsets/* interfaceOffsets */
	, &UnityEvent_1_t349_0_0_0/* parent */
	, ToggleEvent_t348_VTable/* vtableMethods */
	, ToggleEvent_t348_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ToggleEvent_t348_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleEvent"/* name */
	, ""/* namespaze */
	, ToggleEvent_t348_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ToggleEvent_t348_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ToggleEvent_t348_0_0_0/* byval_arg */
	, &ToggleEvent_t348_1_0_0/* this_arg */
	, &ToggleEvent_t348_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleEvent_t348)/* instance_size */
	, sizeof (ToggleEvent_t348)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056770/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_Toggle.h"
// Metadata Definition UnityEngine.UI.Toggle
// UnityEngine.UI.Toggle
#include "UnityEngine_UI_UnityEngine_UI_ToggleMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::.ctor()
extern const MethodInfo Toggle__ctor_m1685_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Toggle__ctor_m1685/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 985/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ToggleGroup_t350_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ToggleGroup UnityEngine.UI.Toggle::get_group()
extern const MethodInfo Toggle_get_group_m1686_MethodInfo = 
{
	"get_group"/* name */
	, (methodPointerType)&Toggle_get_group_m1686/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &ToggleGroup_t350_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 986/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ToggleGroup_t350_0_0_0;
static const ParameterInfo Toggle_t351_Toggle_set_group_m1687_ParameterInfos[] = 
{
	{"value", 0, 134218303, 0, &ToggleGroup_t350_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::set_group(UnityEngine.UI.ToggleGroup)
extern const MethodInfo Toggle_set_group_m1687_MethodInfo = 
{
	"set_group"/* name */
	, (methodPointerType)&Toggle_set_group_m1687/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Toggle_t351_Toggle_set_group_m1687_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 987/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t260_0_0_0;
static const ParameterInfo Toggle_t351_Toggle_Rebuild_m1688_ParameterInfos[] = 
{
	{"executing", 0, 134218304, 0, &CanvasUpdate_t260_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo Toggle_Rebuild_m1688_MethodInfo = 
{
	"Rebuild"/* name */
	, (methodPointerType)&Toggle_Rebuild_m1688/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, Toggle_t351_Toggle_Rebuild_m1688_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 43/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 988/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnEnable()
extern const MethodInfo Toggle_OnEnable_m1689_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Toggle_OnEnable_m1689/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 989/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnDisable()
extern const MethodInfo Toggle_OnDisable_m1690_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Toggle_OnDisable_m1690/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 990/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ToggleGroup_t350_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Toggle_t351_Toggle_SetToggleGroup_m1691_ParameterInfos[] = 
{
	{"newGroup", 0, 134218305, 0, &ToggleGroup_t350_0_0_0},
	{"setMemberValue", 1, 134218306, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::SetToggleGroup(UnityEngine.UI.ToggleGroup,System.Boolean)
extern const MethodInfo Toggle_SetToggleGroup_m1691_MethodInfo = 
{
	"SetToggleGroup"/* name */
	, (methodPointerType)&Toggle_SetToggleGroup_m1691/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_SByte_t170/* invoker_method */
	, Toggle_t351_Toggle_SetToggleGroup_m1691_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 991/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Toggle::get_isOn()
extern const MethodInfo Toggle_get_isOn_m1692_MethodInfo = 
{
	"get_isOn"/* name */
	, (methodPointerType)&Toggle_get_isOn_m1692/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 992/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Toggle_t351_Toggle_set_isOn_m1693_ParameterInfos[] = 
{
	{"value", 0, 134218307, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::set_isOn(System.Boolean)
extern const MethodInfo Toggle_set_isOn_m1693_MethodInfo = 
{
	"set_isOn"/* name */
	, (methodPointerType)&Toggle_set_isOn_m1693/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Toggle_t351_Toggle_set_isOn_m1693_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 993/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Toggle_t351_Toggle_Set_m1694_ParameterInfos[] = 
{
	{"value", 0, 134218308, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean)
extern const MethodInfo Toggle_Set_m1694_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Toggle_Set_m1694/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Toggle_t351_Toggle_Set_m1694_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 994/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Toggle_t351_Toggle_Set_m1695_ParameterInfos[] = 
{
	{"value", 0, 134218309, 0, &Boolean_t169_0_0_0},
	{"sendCallback", 1, 134218310, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Set(System.Boolean,System.Boolean)
extern const MethodInfo Toggle_Set_m1695_MethodInfo = 
{
	"Set"/* name */
	, (methodPointerType)&Toggle_Set_m1695/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170_SByte_t170/* invoker_method */
	, Toggle_t351_Toggle_Set_m1695_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 995/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Toggle_t351_Toggle_PlayEffect_m1696_ParameterInfos[] = 
{
	{"instant", 0, 134218311, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::PlayEffect(System.Boolean)
extern const MethodInfo Toggle_PlayEffect_m1696_MethodInfo = 
{
	"PlayEffect"/* name */
	, (methodPointerType)&Toggle_PlayEffect_m1696/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Toggle_t351_Toggle_PlayEffect_m1696_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 996/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::Start()
extern const MethodInfo Toggle_Start_m1697_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&Toggle_Start_m1697/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 997/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::InternalToggle()
extern const MethodInfo Toggle_InternalToggle_m1698_MethodInfo = 
{
	"InternalToggle"/* name */
	, (methodPointerType)&Toggle_InternalToggle_m1698/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 998/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType PointerEventData_t236_0_0_0;
static const ParameterInfo Toggle_t351_Toggle_OnPointerClick_m1699_ParameterInfos[] = 
{
	{"eventData", 0, 134218312, 0, &PointerEventData_t236_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern const MethodInfo Toggle_OnPointerClick_m1699_MethodInfo = 
{
	"OnPointerClick"/* name */
	, (methodPointerType)&Toggle_OnPointerClick_m1699/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Toggle_t351_Toggle_OnPointerClick_m1699_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 44/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 999/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType BaseEventData_t197_0_0_0;
static const ParameterInfo Toggle_t351_Toggle_OnSubmit_m1700_ParameterInfos[] = 
{
	{"eventData", 0, 134218313, 0, &BaseEventData_t197_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Toggle::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern const MethodInfo Toggle_OnSubmit_m1700_MethodInfo = 
{
	"OnSubmit"/* name */
	, (methodPointerType)&Toggle_OnSubmit_m1700/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Toggle_t351_Toggle_OnSubmit_m1700_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 45/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1000/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.IsDestroyed()
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1701_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.IsDestroyed"/* name */
	, (methodPointerType)&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1701/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 46/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1001/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.Toggle::UnityEngine.UI.ICanvasElement.get_transform()
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1702_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.get_transform"/* name */
	, (methodPointerType)&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1702/* method */
	, &Toggle_t351_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 960/* flags */
	, 0/* iflags */
	, 47/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1002/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Toggle_t351_MethodInfos[] =
{
	&Toggle__ctor_m1685_MethodInfo,
	&Toggle_get_group_m1686_MethodInfo,
	&Toggle_set_group_m1687_MethodInfo,
	&Toggle_Rebuild_m1688_MethodInfo,
	&Toggle_OnEnable_m1689_MethodInfo,
	&Toggle_OnDisable_m1690_MethodInfo,
	&Toggle_SetToggleGroup_m1691_MethodInfo,
	&Toggle_get_isOn_m1692_MethodInfo,
	&Toggle_set_isOn_m1693_MethodInfo,
	&Toggle_Set_m1694_MethodInfo,
	&Toggle_Set_m1695_MethodInfo,
	&Toggle_PlayEffect_m1696_MethodInfo,
	&Toggle_Start_m1697_MethodInfo,
	&Toggle_InternalToggle_m1698_MethodInfo,
	&Toggle_OnPointerClick_m1699_MethodInfo,
	&Toggle_OnSubmit_m1700_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1701_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1702_MethodInfo,
	NULL
};
extern const MethodInfo Toggle_get_group_m1686_MethodInfo;
extern const MethodInfo Toggle_set_group_m1687_MethodInfo;
static const PropertyInfo Toggle_t351____group_PropertyInfo = 
{
	&Toggle_t351_il2cpp_TypeInfo/* parent */
	, "group"/* name */
	, &Toggle_get_group_m1686_MethodInfo/* get */
	, &Toggle_set_group_m1687_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Toggle_get_isOn_m1692_MethodInfo;
extern const MethodInfo Toggle_set_isOn_m1693_MethodInfo;
static const PropertyInfo Toggle_t351____isOn_PropertyInfo = 
{
	&Toggle_t351_il2cpp_TypeInfo/* parent */
	, "isOn"/* name */
	, &Toggle_get_isOn_m1692_MethodInfo/* get */
	, &Toggle_set_isOn_m1693_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Toggle_t351_PropertyInfos[] =
{
	&Toggle_t351____group_PropertyInfo,
	&Toggle_t351____isOn_PropertyInfo,
	NULL
};
static const Il2CppType* Toggle_t351_il2cpp_TypeInfo__nestedTypes[2] =
{
	&ToggleTransition_t347_0_0_0,
	&ToggleEvent_t348_0_0_0,
};
extern const MethodInfo Toggle_OnEnable_m1689_MethodInfo;
extern const MethodInfo Toggle_Start_m1697_MethodInfo;
extern const MethodInfo Toggle_OnDisable_m1690_MethodInfo;
extern const MethodInfo Toggle_OnPointerClick_m1699_MethodInfo;
extern const MethodInfo Toggle_OnSubmit_m1700_MethodInfo;
extern const MethodInfo Toggle_Rebuild_m1688_MethodInfo;
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1702_MethodInfo;
extern const MethodInfo Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1701_MethodInfo;
static const Il2CppMethodReference Toggle_t351_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&Selectable_Awake_m1549_MethodInfo,
	&Toggle_OnEnable_m1689_MethodInfo,
	&Toggle_Start_m1697_MethodInfo,
	&Toggle_OnDisable_m1690_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m858_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&Selectable_OnDidApplyAnimationProperties_m1552_MethodInfo,
	&Selectable_OnCanvasGroupChanged_m1550_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&Selectable_OnPointerEnter_m1578_MethodInfo,
	&Selectable_OnPointerExit_m1579_MethodInfo,
	&Selectable_OnPointerDown_m1576_MethodInfo,
	&Selectable_OnPointerUp_m1577_MethodInfo,
	&Selectable_OnSelect_m1580_MethodInfo,
	&Selectable_OnDeselect_m1581_MethodInfo,
	&Selectable_OnMove_m1566_MethodInfo,
	&Selectable_IsInteractable_m1551_MethodInfo,
	&Selectable_InstantClearState_m1557_MethodInfo,
	&Selectable_DoStateTransition_m1558_MethodInfo,
	&Selectable_FindSelectableOnLeft_m1562_MethodInfo,
	&Selectable_FindSelectableOnRight_m1563_MethodInfo,
	&Selectable_FindSelectableOnUp_m1564_MethodInfo,
	&Selectable_FindSelectableOnDown_m1565_MethodInfo,
	&Selectable_OnMove_m1566_MethodInfo,
	&Selectable_OnPointerDown_m1576_MethodInfo,
	&Selectable_OnPointerUp_m1577_MethodInfo,
	&Selectable_OnPointerEnter_m1578_MethodInfo,
	&Selectable_OnPointerExit_m1579_MethodInfo,
	&Selectable_OnSelect_m1580_MethodInfo,
	&Selectable_OnDeselect_m1581_MethodInfo,
	&Selectable_Select_m1582_MethodInfo,
	&Toggle_OnPointerClick_m1699_MethodInfo,
	&Toggle_OnSubmit_m1700_MethodInfo,
	&Toggle_Rebuild_m1688_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1702_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1701_MethodInfo,
	&Toggle_Rebuild_m1688_MethodInfo,
	&Toggle_OnPointerClick_m1699_MethodInfo,
	&Toggle_OnSubmit_m1700_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1701_MethodInfo,
	&Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1702_MethodInfo,
};
static bool Toggle_t351_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IPointerClickHandler_t394_0_0_0;
extern const Il2CppType ISubmitHandler_t405_0_0_0;
static const Il2CppType* Toggle_t351_InterfacesTypeInfos[] = 
{
	&IEventSystemHandler_t490_0_0_0,
	&IPointerClickHandler_t394_0_0_0,
	&ISubmitHandler_t405_0_0_0,
	&ICanvasElement_t411_0_0_0,
};
static Il2CppInterfaceOffsetPair Toggle_t351_InterfacesOffsets[] = 
{
	{ &IEventSystemHandler_t490_0_0_0, 16},
	{ &IPointerEnterHandler_t390_0_0_0, 16},
	{ &IPointerExitHandler_t391_0_0_0, 17},
	{ &IPointerDownHandler_t392_0_0_0, 18},
	{ &IPointerUpHandler_t393_0_0_0, 19},
	{ &ISelectHandler_t402_0_0_0, 20},
	{ &IDeselectHandler_t403_0_0_0, 21},
	{ &IMoveHandler_t404_0_0_0, 22},
	{ &IPointerClickHandler_t394_0_0_0, 38},
	{ &ISubmitHandler_t405_0_0_0, 39},
	{ &ICanvasElement_t411_0_0_0, 40},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Toggle_t351_1_0_0;
struct Toggle_t351;
const Il2CppTypeDefinitionMetadata Toggle_t351_DefinitionMetadata = 
{
	NULL/* declaringType */
	, Toggle_t351_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, Toggle_t351_InterfacesTypeInfos/* implementedInterfaces */
	, Toggle_t351_InterfacesOffsets/* interfaceOffsets */
	, &Selectable_t259_0_0_0/* parent */
	, Toggle_t351_VTable/* vtableMethods */
	, Toggle_t351_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 505/* fieldStart */

};
TypeInfo Toggle_t351_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Toggle"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Toggle_t351_MethodInfos/* methods */
	, Toggle_t351_PropertyInfos/* properties */
	, NULL/* events */
	, &Toggle_t351_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 258/* custom_attributes_cache */
	, &Toggle_t351_0_0_0/* byval_arg */
	, &Toggle_t351_1_0_0/* this_arg */
	, &Toggle_t351_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Toggle_t351)/* instance_size */
	, sizeof (Toggle_t351)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 2/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 48/* vtable_count */
	, 4/* interfaces_count */
	, 11/* interface_offsets_count */

};
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup.h"
// Metadata Definition UnityEngine.UI.ToggleGroup
extern TypeInfo ToggleGroup_t350_il2cpp_TypeInfo;
// UnityEngine.UI.ToggleGroup
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::.ctor()
extern const MethodInfo ToggleGroup__ctor_m1703_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ToggleGroup__ctor_m1703/* method */
	, &ToggleGroup_t350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1004/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::get_allowSwitchOff()
extern const MethodInfo ToggleGroup_get_allowSwitchOff_m1704_MethodInfo = 
{
	"get_allowSwitchOff"/* name */
	, (methodPointerType)&ToggleGroup_get_allowSwitchOff_m1704/* method */
	, &ToggleGroup_t350_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1005/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo ToggleGroup_t350_ToggleGroup_set_allowSwitchOff_m1705_ParameterInfos[] = 
{
	{"value", 0, 134218314, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::set_allowSwitchOff(System.Boolean)
extern const MethodInfo ToggleGroup_set_allowSwitchOff_m1705_MethodInfo = 
{
	"set_allowSwitchOff"/* name */
	, (methodPointerType)&ToggleGroup_set_allowSwitchOff_m1705/* method */
	, &ToggleGroup_t350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, ToggleGroup_t350_ToggleGroup_set_allowSwitchOff_m1705_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1006/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t351_0_0_0;
static const ParameterInfo ToggleGroup_t350_ToggleGroup_ValidateToggleIsInGroup_m1706_ParameterInfos[] = 
{
	{"toggle", 0, 134218315, 0, &Toggle_t351_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::ValidateToggleIsInGroup(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_ValidateToggleIsInGroup_m1706_MethodInfo = 
{
	"ValidateToggleIsInGroup"/* name */
	, (methodPointerType)&ToggleGroup_ValidateToggleIsInGroup_m1706/* method */
	, &ToggleGroup_t350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ToggleGroup_t350_ToggleGroup_ValidateToggleIsInGroup_m1706_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1007/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t351_0_0_0;
static const ParameterInfo ToggleGroup_t350_ToggleGroup_NotifyToggleOn_m1707_ParameterInfos[] = 
{
	{"toggle", 0, 134218316, 0, &Toggle_t351_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::NotifyToggleOn(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_NotifyToggleOn_m1707_MethodInfo = 
{
	"NotifyToggleOn"/* name */
	, (methodPointerType)&ToggleGroup_NotifyToggleOn_m1707/* method */
	, &ToggleGroup_t350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ToggleGroup_t350_ToggleGroup_NotifyToggleOn_m1707_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1008/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t351_0_0_0;
static const ParameterInfo ToggleGroup_t350_ToggleGroup_UnregisterToggle_m1708_ParameterInfos[] = 
{
	{"toggle", 0, 134218317, 0, &Toggle_t351_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::UnregisterToggle(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_UnregisterToggle_m1708_MethodInfo = 
{
	"UnregisterToggle"/* name */
	, (methodPointerType)&ToggleGroup_UnregisterToggle_m1708/* method */
	, &ToggleGroup_t350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ToggleGroup_t350_ToggleGroup_UnregisterToggle_m1708_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1009/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t351_0_0_0;
static const ParameterInfo ToggleGroup_t350_ToggleGroup_RegisterToggle_m1709_ParameterInfos[] = 
{
	{"toggle", 0, 134218318, 0, &Toggle_t351_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::RegisterToggle(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_RegisterToggle_m1709_MethodInfo = 
{
	"RegisterToggle"/* name */
	, (methodPointerType)&ToggleGroup_RegisterToggle_m1709/* method */
	, &ToggleGroup_t350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ToggleGroup_t350_ToggleGroup_RegisterToggle_m1709_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1010/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::AnyTogglesOn()
extern const MethodInfo ToggleGroup_AnyTogglesOn_m1710_MethodInfo = 
{
	"AnyTogglesOn"/* name */
	, (methodPointerType)&ToggleGroup_AnyTogglesOn_m1710/* method */
	, &ToggleGroup_t350_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1011/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerable_1_t417_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Toggle> UnityEngine.UI.ToggleGroup::ActiveToggles()
extern const MethodInfo ToggleGroup_ActiveToggles_m1711_MethodInfo = 
{
	"ActiveToggles"/* name */
	, (methodPointerType)&ToggleGroup_ActiveToggles_m1711/* method */
	, &ToggleGroup_t350_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_1_t417_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1012/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ToggleGroup::SetAllTogglesOff()
extern const MethodInfo ToggleGroup_SetAllTogglesOff_m1712_MethodInfo = 
{
	"SetAllTogglesOff"/* name */
	, (methodPointerType)&ToggleGroup_SetAllTogglesOff_m1712/* method */
	, &ToggleGroup_t350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1013/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t351_0_0_0;
static const ParameterInfo ToggleGroup_t350_ToggleGroup_U3CAnyTogglesOnU3Em__7_m1713_ParameterInfos[] = 
{
	{"x", 0, 134218319, 0, &Toggle_t351_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::<AnyTogglesOn>m__7(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_U3CAnyTogglesOnU3Em__7_m1713_MethodInfo = 
{
	"<AnyTogglesOn>m__7"/* name */
	, (methodPointerType)&ToggleGroup_U3CAnyTogglesOnU3Em__7_m1713/* method */
	, &ToggleGroup_t350_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, ToggleGroup_t350_ToggleGroup_U3CAnyTogglesOnU3Em__7_m1713_ParameterInfos/* parameters */
	, 265/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1014/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Toggle_t351_0_0_0;
static const ParameterInfo ToggleGroup_t350_ToggleGroup_U3CActiveTogglesU3Em__8_m1714_ParameterInfos[] = 
{
	{"x", 0, 134218320, 0, &Toggle_t351_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ToggleGroup::<ActiveToggles>m__8(UnityEngine.UI.Toggle)
extern const MethodInfo ToggleGroup_U3CActiveTogglesU3Em__8_m1714_MethodInfo = 
{
	"<ActiveToggles>m__8"/* name */
	, (methodPointerType)&ToggleGroup_U3CActiveTogglesU3Em__8_m1714/* method */
	, &ToggleGroup_t350_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, ToggleGroup_t350_ToggleGroup_U3CActiveTogglesU3Em__8_m1714_ParameterInfos/* parameters */
	, 266/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1015/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ToggleGroup_t350_MethodInfos[] =
{
	&ToggleGroup__ctor_m1703_MethodInfo,
	&ToggleGroup_get_allowSwitchOff_m1704_MethodInfo,
	&ToggleGroup_set_allowSwitchOff_m1705_MethodInfo,
	&ToggleGroup_ValidateToggleIsInGroup_m1706_MethodInfo,
	&ToggleGroup_NotifyToggleOn_m1707_MethodInfo,
	&ToggleGroup_UnregisterToggle_m1708_MethodInfo,
	&ToggleGroup_RegisterToggle_m1709_MethodInfo,
	&ToggleGroup_AnyTogglesOn_m1710_MethodInfo,
	&ToggleGroup_ActiveToggles_m1711_MethodInfo,
	&ToggleGroup_SetAllTogglesOff_m1712_MethodInfo,
	&ToggleGroup_U3CAnyTogglesOnU3Em__7_m1713_MethodInfo,
	&ToggleGroup_U3CActiveTogglesU3Em__8_m1714_MethodInfo,
	NULL
};
extern const MethodInfo ToggleGroup_get_allowSwitchOff_m1704_MethodInfo;
extern const MethodInfo ToggleGroup_set_allowSwitchOff_m1705_MethodInfo;
static const PropertyInfo ToggleGroup_t350____allowSwitchOff_PropertyInfo = 
{
	&ToggleGroup_t350_il2cpp_TypeInfo/* parent */
	, "allowSwitchOff"/* name */
	, &ToggleGroup_get_allowSwitchOff_m1704_MethodInfo/* get */
	, &ToggleGroup_set_allowSwitchOff_m1705_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ToggleGroup_t350_PropertyInfos[] =
{
	&ToggleGroup_t350____allowSwitchOff_PropertyInfo,
	NULL
};
extern const MethodInfo UIBehaviour_OnEnable_m853_MethodInfo;
extern const MethodInfo UIBehaviour_OnDisable_m855_MethodInfo;
static const Il2CppMethodReference ToggleGroup_t350_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&UIBehaviour_OnEnable_m853_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&UIBehaviour_OnDisable_m855_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m858_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m861_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
};
static bool ToggleGroup_t350_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ToggleGroup_t350_1_0_0;
struct ToggleGroup_t350;
const Il2CppTypeDefinitionMetadata ToggleGroup_t350_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t199_0_0_0/* parent */
	, ToggleGroup_t350_VTable/* vtableMethods */
	, ToggleGroup_t350_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 510/* fieldStart */

};
TypeInfo ToggleGroup_t350_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ToggleGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ToggleGroup_t350_MethodInfos/* methods */
	, ToggleGroup_t350_PropertyInfos/* properties */
	, NULL/* events */
	, &ToggleGroup_t350_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 261/* custom_attributes_cache */
	, &ToggleGroup_t350_0_0_0/* byval_arg */
	, &ToggleGroup_t350_1_0_0/* this_arg */
	, &ToggleGroup_t350_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ToggleGroup_t350)/* instance_size */
	, sizeof (ToggleGroup_t350)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ToggleGroup_t350_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 12/* method_count */
	, 1/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 16/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
// Metadata Definition UnityEngine.UI.AspectRatioFitter/AspectMode
extern TypeInfo AspectMode_t355_il2cpp_TypeInfo;
// UnityEngine.UI.AspectRatioFitter/AspectMode
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectModeMethodDeclarations.h"
static const MethodInfo* AspectMode_t355_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference AspectMode_t355_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool AspectMode_t355_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair AspectMode_t355_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType AspectMode_t355_0_0_0;
extern const Il2CppType AspectMode_t355_1_0_0;
extern TypeInfo AspectRatioFitter_t356_il2cpp_TypeInfo;
extern const Il2CppType AspectRatioFitter_t356_0_0_0;
const Il2CppTypeDefinitionMetadata AspectMode_t355_DefinitionMetadata = 
{
	&AspectRatioFitter_t356_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, AspectMode_t355_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, AspectMode_t355_VTable/* vtableMethods */
	, AspectMode_t355_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 514/* fieldStart */

};
TypeInfo AspectMode_t355_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "AspectMode"/* name */
	, ""/* namespaze */
	, AspectMode_t355_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &AspectMode_t355_0_0_0/* byval_arg */
	, &AspectMode_t355_1_0_0/* this_arg */
	, &AspectMode_t355_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AspectMode_t355)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (AspectMode_t355)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter.h"
// Metadata Definition UnityEngine.UI.AspectRatioFitter
// UnityEngine.UI.AspectRatioFitter
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::.ctor()
extern const MethodInfo AspectRatioFitter__ctor_m1715_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AspectRatioFitter__ctor_m1715/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1016/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_AspectMode_t355 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::get_aspectMode()
extern const MethodInfo AspectRatioFitter_get_aspectMode_m1716_MethodInfo = 
{
	"get_aspectMode"/* name */
	, (methodPointerType)&AspectRatioFitter_get_aspectMode_m1716/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &AspectMode_t355_0_0_0/* return_type */
	, RuntimeInvoker_AspectMode_t355/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1017/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType AspectMode_t355_0_0_0;
static const ParameterInfo AspectRatioFitter_t356_AspectRatioFitter_set_aspectMode_m1717_ParameterInfos[] = 
{
	{"value", 0, 134218321, 0, &AspectMode_t355_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectMode(UnityEngine.UI.AspectRatioFitter/AspectMode)
extern const MethodInfo AspectRatioFitter_set_aspectMode_m1717_MethodInfo = 
{
	"set_aspectMode"/* name */
	, (methodPointerType)&AspectRatioFitter_set_aspectMode_m1717/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, AspectRatioFitter_t356_AspectRatioFitter_set_aspectMode_m1717_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1018/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.AspectRatioFitter::get_aspectRatio()
extern const MethodInfo AspectRatioFitter_get_aspectRatio_m1718_MethodInfo = 
{
	"get_aspectRatio"/* name */
	, (methodPointerType)&AspectRatioFitter_get_aspectRatio_m1718/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1019/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo AspectRatioFitter_t356_AspectRatioFitter_set_aspectRatio_m1719_ParameterInfos[] = 
{
	{"value", 0, 134218322, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectRatio(System.Single)
extern const MethodInfo AspectRatioFitter_set_aspectRatio_m1719_MethodInfo = 
{
	"set_aspectRatio"/* name */
	, (methodPointerType)&AspectRatioFitter_set_aspectRatio_m1719/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, AspectRatioFitter_t356_AspectRatioFitter_set_aspectRatio_m1719_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1020/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::get_rectTransform()
extern const MethodInfo AspectRatioFitter_get_rectTransform_m1720_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&AspectRatioFitter_get_rectTransform_m1720/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t272_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1021/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnEnable()
extern const MethodInfo AspectRatioFitter_OnEnable_m1721_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&AspectRatioFitter_OnEnable_m1721/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1022/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnDisable()
extern const MethodInfo AspectRatioFitter_OnDisable_m1722_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&AspectRatioFitter_OnDisable_m1722/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1023/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::OnRectTransformDimensionsChange()
extern const MethodInfo AspectRatioFitter_OnRectTransformDimensionsChange_m1723_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&AspectRatioFitter_OnRectTransformDimensionsChange_m1723/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1024/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::UpdateRect()
extern const MethodInfo AspectRatioFitter_UpdateRect_m1724_MethodInfo = 
{
	"UpdateRect"/* name */
	, (methodPointerType)&AspectRatioFitter_UpdateRect_m1724/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1025/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo AspectRatioFitter_t356_AspectRatioFitter_GetSizeDeltaToProduceSize_m1725_ParameterInfos[] = 
{
	{"size", 0, 134218323, 0, &Single_t151_0_0_0},
	{"axis", 1, 134218324, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Single_t151_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.AspectRatioFitter::GetSizeDeltaToProduceSize(System.Single,System.Int32)
extern const MethodInfo AspectRatioFitter_GetSizeDeltaToProduceSize_m1725_MethodInfo = 
{
	"GetSizeDeltaToProduceSize"/* name */
	, (methodPointerType)&AspectRatioFitter_GetSizeDeltaToProduceSize_m1725/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Single_t151_Int32_t127/* invoker_method */
	, AspectRatioFitter_t356_AspectRatioFitter_GetSizeDeltaToProduceSize_m1725_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1026/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.AspectRatioFitter::GetParentSize()
extern const MethodInfo AspectRatioFitter_GetParentSize_m1726_MethodInfo = 
{
	"GetParentSize"/* name */
	, (methodPointerType)&AspectRatioFitter_GetParentSize_m1726/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1027/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutHorizontal()
extern const MethodInfo AspectRatioFitter_SetLayoutHorizontal_m1727_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&AspectRatioFitter_SetLayoutHorizontal_m1727/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1028/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutVertical()
extern const MethodInfo AspectRatioFitter_SetLayoutVertical_m1728_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&AspectRatioFitter_SetLayoutVertical_m1728/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1029/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.AspectRatioFitter::SetDirty()
extern const MethodInfo AspectRatioFitter_SetDirty_m1729_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&AspectRatioFitter_SetDirty_m1729/* method */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1030/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* AspectRatioFitter_t356_MethodInfos[] =
{
	&AspectRatioFitter__ctor_m1715_MethodInfo,
	&AspectRatioFitter_get_aspectMode_m1716_MethodInfo,
	&AspectRatioFitter_set_aspectMode_m1717_MethodInfo,
	&AspectRatioFitter_get_aspectRatio_m1718_MethodInfo,
	&AspectRatioFitter_set_aspectRatio_m1719_MethodInfo,
	&AspectRatioFitter_get_rectTransform_m1720_MethodInfo,
	&AspectRatioFitter_OnEnable_m1721_MethodInfo,
	&AspectRatioFitter_OnDisable_m1722_MethodInfo,
	&AspectRatioFitter_OnRectTransformDimensionsChange_m1723_MethodInfo,
	&AspectRatioFitter_UpdateRect_m1724_MethodInfo,
	&AspectRatioFitter_GetSizeDeltaToProduceSize_m1725_MethodInfo,
	&AspectRatioFitter_GetParentSize_m1726_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m1727_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m1728_MethodInfo,
	&AspectRatioFitter_SetDirty_m1729_MethodInfo,
	NULL
};
extern const MethodInfo AspectRatioFitter_get_aspectMode_m1716_MethodInfo;
extern const MethodInfo AspectRatioFitter_set_aspectMode_m1717_MethodInfo;
static const PropertyInfo AspectRatioFitter_t356____aspectMode_PropertyInfo = 
{
	&AspectRatioFitter_t356_il2cpp_TypeInfo/* parent */
	, "aspectMode"/* name */
	, &AspectRatioFitter_get_aspectMode_m1716_MethodInfo/* get */
	, &AspectRatioFitter_set_aspectMode_m1717_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AspectRatioFitter_get_aspectRatio_m1718_MethodInfo;
extern const MethodInfo AspectRatioFitter_set_aspectRatio_m1719_MethodInfo;
static const PropertyInfo AspectRatioFitter_t356____aspectRatio_PropertyInfo = 
{
	&AspectRatioFitter_t356_il2cpp_TypeInfo/* parent */
	, "aspectRatio"/* name */
	, &AspectRatioFitter_get_aspectRatio_m1718_MethodInfo/* get */
	, &AspectRatioFitter_set_aspectRatio_m1719_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo AspectRatioFitter_get_rectTransform_m1720_MethodInfo;
static const PropertyInfo AspectRatioFitter_t356____rectTransform_PropertyInfo = 
{
	&AspectRatioFitter_t356_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &AspectRatioFitter_get_rectTransform_m1720_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* AspectRatioFitter_t356_PropertyInfos[] =
{
	&AspectRatioFitter_t356____aspectMode_PropertyInfo,
	&AspectRatioFitter_t356____aspectRatio_PropertyInfo,
	&AspectRatioFitter_t356____rectTransform_PropertyInfo,
	NULL
};
static const Il2CppType* AspectRatioFitter_t356_il2cpp_TypeInfo__nestedTypes[1] =
{
	&AspectMode_t355_0_0_0,
};
extern const MethodInfo AspectRatioFitter_OnEnable_m1721_MethodInfo;
extern const MethodInfo AspectRatioFitter_OnDisable_m1722_MethodInfo;
extern const MethodInfo AspectRatioFitter_OnRectTransformDimensionsChange_m1723_MethodInfo;
extern const MethodInfo AspectRatioFitter_SetLayoutHorizontal_m1727_MethodInfo;
extern const MethodInfo AspectRatioFitter_SetLayoutVertical_m1728_MethodInfo;
static const Il2CppMethodReference AspectRatioFitter_t356_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&AspectRatioFitter_OnEnable_m1721_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&AspectRatioFitter_OnDisable_m1722_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&AspectRatioFitter_OnRectTransformDimensionsChange_m1723_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m861_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m1727_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m1728_MethodInfo,
	&AspectRatioFitter_SetLayoutHorizontal_m1727_MethodInfo,
	&AspectRatioFitter_SetLayoutVertical_m1728_MethodInfo,
};
static bool AspectRatioFitter_t356_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILayoutController_t475_0_0_0;
extern const Il2CppType ILayoutSelfController_t476_0_0_0;
static const Il2CppType* AspectRatioFitter_t356_InterfacesTypeInfos[] = 
{
	&ILayoutController_t475_0_0_0,
	&ILayoutSelfController_t476_0_0_0,
};
static Il2CppInterfaceOffsetPair AspectRatioFitter_t356_InterfacesOffsets[] = 
{
	{ &ILayoutController_t475_0_0_0, 16},
	{ &ILayoutSelfController_t476_0_0_0, 18},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType AspectRatioFitter_t356_1_0_0;
struct AspectRatioFitter_t356;
const Il2CppTypeDefinitionMetadata AspectRatioFitter_t356_DefinitionMetadata = 
{
	NULL/* declaringType */
	, AspectRatioFitter_t356_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, AspectRatioFitter_t356_InterfacesTypeInfos/* implementedInterfaces */
	, AspectRatioFitter_t356_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t199_0_0_0/* parent */
	, AspectRatioFitter_t356_VTable/* vtableMethods */
	, AspectRatioFitter_t356_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 520/* fieldStart */

};
TypeInfo AspectRatioFitter_t356_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "AspectRatioFitter"/* name */
	, "UnityEngine.UI"/* namespaze */
	, AspectRatioFitter_t356_MethodInfos/* methods */
	, AspectRatioFitter_t356_PropertyInfos/* properties */
	, NULL/* events */
	, &AspectRatioFitter_t356_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 267/* custom_attributes_cache */
	, &AspectRatioFitter_t356_0_0_0/* byval_arg */
	, &AspectRatioFitter_t356_1_0_0/* this_arg */
	, &AspectRatioFitter_t356_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AspectRatioFitter_t356)/* instance_size */
	, sizeof (AspectRatioFitter_t356)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 15/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 20/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/ScaleMode
extern TypeInfo ScaleMode_t357_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/ScaleMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleModeMethodDeclarations.h"
static const MethodInfo* ScaleMode_t357_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ScaleMode_t357_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool ScaleMode_t357_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScaleMode_t357_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScaleMode_t357_0_0_0;
extern const Il2CppType ScaleMode_t357_1_0_0;
extern TypeInfo CanvasScaler_t360_il2cpp_TypeInfo;
extern const Il2CppType CanvasScaler_t360_0_0_0;
const Il2CppTypeDefinitionMetadata ScaleMode_t357_DefinitionMetadata = 
{
	&CanvasScaler_t360_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScaleMode_t357_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, ScaleMode_t357_VTable/* vtableMethods */
	, ScaleMode_t357_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 524/* fieldStart */

};
TypeInfo ScaleMode_t357_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScaleMode"/* name */
	, ""/* namespaze */
	, ScaleMode_t357_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScaleMode_t357_0_0_0/* byval_arg */
	, &ScaleMode_t357_1_0_0/* this_arg */
	, &ScaleMode_t357_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScaleMode_t357)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScaleMode_t357)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/ScreenMatchMode
extern TypeInfo ScreenMatchMode_t358_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/ScreenMatchMode
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchModeMethodDeclarations.h"
static const MethodInfo* ScreenMatchMode_t358_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference ScreenMatchMode_t358_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool ScreenMatchMode_t358_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair ScreenMatchMode_t358_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ScreenMatchMode_t358_0_0_0;
extern const Il2CppType ScreenMatchMode_t358_1_0_0;
const Il2CppTypeDefinitionMetadata ScreenMatchMode_t358_DefinitionMetadata = 
{
	&CanvasScaler_t360_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, ScreenMatchMode_t358_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, ScreenMatchMode_t358_VTable/* vtableMethods */
	, ScreenMatchMode_t358_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 528/* fieldStart */

};
TypeInfo ScreenMatchMode_t358_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScreenMatchMode"/* name */
	, ""/* namespaze */
	, ScreenMatchMode_t358_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ScreenMatchMode_t358_0_0_0/* byval_arg */
	, &ScreenMatchMode_t358_1_0_0/* this_arg */
	, &ScreenMatchMode_t358_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ScreenMatchMode_t358)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (ScreenMatchMode_t358)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
// Metadata Definition UnityEngine.UI.CanvasScaler/Unit
extern TypeInfo Unit_t359_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasScaler/Unit
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_UnitMethodDeclarations.h"
static const MethodInfo* Unit_t359_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Unit_t359_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool Unit_t359_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Unit_t359_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Unit_t359_0_0_0;
extern const Il2CppType Unit_t359_1_0_0;
const Il2CppTypeDefinitionMetadata Unit_t359_DefinitionMetadata = 
{
	&CanvasScaler_t360_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Unit_t359_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, Unit_t359_VTable/* vtableMethods */
	, Unit_t359_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 532/* fieldStart */

};
TypeInfo Unit_t359_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Unit"/* name */
	, ""/* namespaze */
	, Unit_t359_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Unit_t359_0_0_0/* byval_arg */
	, &Unit_t359_1_0_0/* this_arg */
	, &Unit_t359_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Unit_t359)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Unit_t359)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler.h"
// Metadata Definition UnityEngine.UI.CanvasScaler
// UnityEngine.UI.CanvasScaler
#include "UnityEngine_UI_UnityEngine_UI_CanvasScalerMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::.ctor()
extern const MethodInfo CanvasScaler__ctor_m1730_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CanvasScaler__ctor_m1730/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1031/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_ScaleMode_t357 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/ScaleMode UnityEngine.UI.CanvasScaler::get_uiScaleMode()
extern const MethodInfo CanvasScaler_get_uiScaleMode_m1731_MethodInfo = 
{
	"get_uiScaleMode"/* name */
	, (methodPointerType)&CanvasScaler_get_uiScaleMode_m1731/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &ScaleMode_t357_0_0_0/* return_type */
	, RuntimeInvoker_ScaleMode_t357/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1032/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ScaleMode_t357_0_0_0;
static const ParameterInfo CanvasScaler_t360_CanvasScaler_set_uiScaleMode_m1732_ParameterInfos[] = 
{
	{"value", 0, 134218325, 0, &ScaleMode_t357_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_uiScaleMode(UnityEngine.UI.CanvasScaler/ScaleMode)
extern const MethodInfo CanvasScaler_set_uiScaleMode_m1732_MethodInfo = 
{
	"set_uiScaleMode"/* name */
	, (methodPointerType)&CanvasScaler_set_uiScaleMode_m1732/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, CanvasScaler_t360_CanvasScaler_set_uiScaleMode_m1732_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1033/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_referencePixelsPerUnit()
extern const MethodInfo CanvasScaler_get_referencePixelsPerUnit_m1733_MethodInfo = 
{
	"get_referencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_referencePixelsPerUnit_m1733/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1034/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo CanvasScaler_t360_CanvasScaler_set_referencePixelsPerUnit_m1734_ParameterInfos[] = 
{
	{"value", 0, 134218326, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_referencePixelsPerUnit(System.Single)
extern const MethodInfo CanvasScaler_set_referencePixelsPerUnit_m1734_MethodInfo = 
{
	"set_referencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_referencePixelsPerUnit_m1734/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, CanvasScaler_t360_CanvasScaler_set_referencePixelsPerUnit_m1734_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1035/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_scaleFactor()
extern const MethodInfo CanvasScaler_get_scaleFactor_m1735_MethodInfo = 
{
	"get_scaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_get_scaleFactor_m1735/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1036/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo CanvasScaler_t360_CanvasScaler_set_scaleFactor_m1736_ParameterInfos[] = 
{
	{"value", 0, 134218327, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_scaleFactor(System.Single)
extern const MethodInfo CanvasScaler_set_scaleFactor_m1736_MethodInfo = 
{
	"set_scaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_set_scaleFactor_m1736/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, CanvasScaler_t360_CanvasScaler_set_scaleFactor_m1736_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1037/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::get_referenceResolution()
extern const MethodInfo CanvasScaler_get_referenceResolution_m1737_MethodInfo = 
{
	"get_referenceResolution"/* name */
	, (methodPointerType)&CanvasScaler_get_referenceResolution_m1737/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1038/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo CanvasScaler_t360_CanvasScaler_set_referenceResolution_m1738_ParameterInfos[] = 
{
	{"value", 0, 134218328, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_referenceResolution(UnityEngine.Vector2)
extern const MethodInfo CanvasScaler_set_referenceResolution_m1738_MethodInfo = 
{
	"set_referenceResolution"/* name */
	, (methodPointerType)&CanvasScaler_set_referenceResolution_m1738/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector2_t10/* invoker_method */
	, CanvasScaler_t360_CanvasScaler_set_referenceResolution_m1738_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1039/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_ScreenMatchMode_t358 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/ScreenMatchMode UnityEngine.UI.CanvasScaler::get_screenMatchMode()
extern const MethodInfo CanvasScaler_get_screenMatchMode_m1739_MethodInfo = 
{
	"get_screenMatchMode"/* name */
	, (methodPointerType)&CanvasScaler_get_screenMatchMode_m1739/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &ScreenMatchMode_t358_0_0_0/* return_type */
	, RuntimeInvoker_ScreenMatchMode_t358/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1040/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ScreenMatchMode_t358_0_0_0;
static const ParameterInfo CanvasScaler_t360_CanvasScaler_set_screenMatchMode_m1740_ParameterInfos[] = 
{
	{"value", 0, 134218329, 0, &ScreenMatchMode_t358_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_screenMatchMode(UnityEngine.UI.CanvasScaler/ScreenMatchMode)
extern const MethodInfo CanvasScaler_set_screenMatchMode_m1740_MethodInfo = 
{
	"set_screenMatchMode"/* name */
	, (methodPointerType)&CanvasScaler_set_screenMatchMode_m1740/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, CanvasScaler_t360_CanvasScaler_set_screenMatchMode_m1740_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1041/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_matchWidthOrHeight()
extern const MethodInfo CanvasScaler_get_matchWidthOrHeight_m1741_MethodInfo = 
{
	"get_matchWidthOrHeight"/* name */
	, (methodPointerType)&CanvasScaler_get_matchWidthOrHeight_m1741/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1042/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo CanvasScaler_t360_CanvasScaler_set_matchWidthOrHeight_m1742_ParameterInfos[] = 
{
	{"value", 0, 134218330, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_matchWidthOrHeight(System.Single)
extern const MethodInfo CanvasScaler_set_matchWidthOrHeight_m1742_MethodInfo = 
{
	"set_matchWidthOrHeight"/* name */
	, (methodPointerType)&CanvasScaler_set_matchWidthOrHeight_m1742/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, CanvasScaler_t360_CanvasScaler_set_matchWidthOrHeight_m1742_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1043/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Unit_t359 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.CanvasScaler/Unit UnityEngine.UI.CanvasScaler::get_physicalUnit()
extern const MethodInfo CanvasScaler_get_physicalUnit_m1743_MethodInfo = 
{
	"get_physicalUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_physicalUnit_m1743/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Unit_t359_0_0_0/* return_type */
	, RuntimeInvoker_Unit_t359/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1044/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Unit_t359_0_0_0;
static const ParameterInfo CanvasScaler_t360_CanvasScaler_set_physicalUnit_m1744_ParameterInfos[] = 
{
	{"value", 0, 134218331, 0, &Unit_t359_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_physicalUnit(UnityEngine.UI.CanvasScaler/Unit)
extern const MethodInfo CanvasScaler_set_physicalUnit_m1744_MethodInfo = 
{
	"set_physicalUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_physicalUnit_m1744/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, CanvasScaler_t360_CanvasScaler_set_physicalUnit_m1744_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1045/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_fallbackScreenDPI()
extern const MethodInfo CanvasScaler_get_fallbackScreenDPI_m1745_MethodInfo = 
{
	"get_fallbackScreenDPI"/* name */
	, (methodPointerType)&CanvasScaler_get_fallbackScreenDPI_m1745/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1046/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo CanvasScaler_t360_CanvasScaler_set_fallbackScreenDPI_m1746_ParameterInfos[] = 
{
	{"value", 0, 134218332, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_fallbackScreenDPI(System.Single)
extern const MethodInfo CanvasScaler_set_fallbackScreenDPI_m1746_MethodInfo = 
{
	"set_fallbackScreenDPI"/* name */
	, (methodPointerType)&CanvasScaler_set_fallbackScreenDPI_m1746/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, CanvasScaler_t360_CanvasScaler_set_fallbackScreenDPI_m1746_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1047/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_defaultSpriteDPI()
extern const MethodInfo CanvasScaler_get_defaultSpriteDPI_m1747_MethodInfo = 
{
	"get_defaultSpriteDPI"/* name */
	, (methodPointerType)&CanvasScaler_get_defaultSpriteDPI_m1747/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1048/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo CanvasScaler_t360_CanvasScaler_set_defaultSpriteDPI_m1748_ParameterInfos[] = 
{
	{"value", 0, 134218333, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_defaultSpriteDPI(System.Single)
extern const MethodInfo CanvasScaler_set_defaultSpriteDPI_m1748_MethodInfo = 
{
	"set_defaultSpriteDPI"/* name */
	, (methodPointerType)&CanvasScaler_set_defaultSpriteDPI_m1748/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, CanvasScaler_t360_CanvasScaler_set_defaultSpriteDPI_m1748_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1049/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.CanvasScaler::get_dynamicPixelsPerUnit()
extern const MethodInfo CanvasScaler_get_dynamicPixelsPerUnit_m1749_MethodInfo = 
{
	"get_dynamicPixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_get_dynamicPixelsPerUnit_m1749/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1050/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo CanvasScaler_t360_CanvasScaler_set_dynamicPixelsPerUnit_m1750_ParameterInfos[] = 
{
	{"value", 0, 134218334, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::set_dynamicPixelsPerUnit(System.Single)
extern const MethodInfo CanvasScaler_set_dynamicPixelsPerUnit_m1750_MethodInfo = 
{
	"set_dynamicPixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_set_dynamicPixelsPerUnit_m1750/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, CanvasScaler_t360_CanvasScaler_set_dynamicPixelsPerUnit_m1750_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1051/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::OnEnable()
extern const MethodInfo CanvasScaler_OnEnable_m1751_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&CanvasScaler_OnEnable_m1751/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1052/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::OnDisable()
extern const MethodInfo CanvasScaler_OnDisable_m1752_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&CanvasScaler_OnDisable_m1752/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1053/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::Update()
extern const MethodInfo CanvasScaler_Update_m1753_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&CanvasScaler_Update_m1753/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1054/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::Handle()
extern const MethodInfo CanvasScaler_Handle_m1754_MethodInfo = 
{
	"Handle"/* name */
	, (methodPointerType)&CanvasScaler_Handle_m1754/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1055/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleWorldCanvas()
extern const MethodInfo CanvasScaler_HandleWorldCanvas_m1755_MethodInfo = 
{
	"HandleWorldCanvas"/* name */
	, (methodPointerType)&CanvasScaler_HandleWorldCanvas_m1755/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1056/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleConstantPixelSize()
extern const MethodInfo CanvasScaler_HandleConstantPixelSize_m1756_MethodInfo = 
{
	"HandleConstantPixelSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleConstantPixelSize_m1756/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1057/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleScaleWithScreenSize()
extern const MethodInfo CanvasScaler_HandleScaleWithScreenSize_m1757_MethodInfo = 
{
	"HandleScaleWithScreenSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleScaleWithScreenSize_m1757/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1058/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::HandleConstantPhysicalSize()
extern const MethodInfo CanvasScaler_HandleConstantPhysicalSize_m1758_MethodInfo = 
{
	"HandleConstantPhysicalSize"/* name */
	, (methodPointerType)&CanvasScaler_HandleConstantPhysicalSize_m1758/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1059/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo CanvasScaler_t360_CanvasScaler_SetScaleFactor_m1759_ParameterInfos[] = 
{
	{"scaleFactor", 0, 134218335, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::SetScaleFactor(System.Single)
extern const MethodInfo CanvasScaler_SetScaleFactor_m1759_MethodInfo = 
{
	"SetScaleFactor"/* name */
	, (methodPointerType)&CanvasScaler_SetScaleFactor_m1759/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, CanvasScaler_t360_CanvasScaler_SetScaleFactor_m1759_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1060/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo CanvasScaler_t360_CanvasScaler_SetReferencePixelsPerUnit_m1760_ParameterInfos[] = 
{
	{"referencePixelsPerUnit", 0, 134218336, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasScaler::SetReferencePixelsPerUnit(System.Single)
extern const MethodInfo CanvasScaler_SetReferencePixelsPerUnit_m1760_MethodInfo = 
{
	"SetReferencePixelsPerUnit"/* name */
	, (methodPointerType)&CanvasScaler_SetReferencePixelsPerUnit_m1760/* method */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, CanvasScaler_t360_CanvasScaler_SetReferencePixelsPerUnit_m1760_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1061/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CanvasScaler_t360_MethodInfos[] =
{
	&CanvasScaler__ctor_m1730_MethodInfo,
	&CanvasScaler_get_uiScaleMode_m1731_MethodInfo,
	&CanvasScaler_set_uiScaleMode_m1732_MethodInfo,
	&CanvasScaler_get_referencePixelsPerUnit_m1733_MethodInfo,
	&CanvasScaler_set_referencePixelsPerUnit_m1734_MethodInfo,
	&CanvasScaler_get_scaleFactor_m1735_MethodInfo,
	&CanvasScaler_set_scaleFactor_m1736_MethodInfo,
	&CanvasScaler_get_referenceResolution_m1737_MethodInfo,
	&CanvasScaler_set_referenceResolution_m1738_MethodInfo,
	&CanvasScaler_get_screenMatchMode_m1739_MethodInfo,
	&CanvasScaler_set_screenMatchMode_m1740_MethodInfo,
	&CanvasScaler_get_matchWidthOrHeight_m1741_MethodInfo,
	&CanvasScaler_set_matchWidthOrHeight_m1742_MethodInfo,
	&CanvasScaler_get_physicalUnit_m1743_MethodInfo,
	&CanvasScaler_set_physicalUnit_m1744_MethodInfo,
	&CanvasScaler_get_fallbackScreenDPI_m1745_MethodInfo,
	&CanvasScaler_set_fallbackScreenDPI_m1746_MethodInfo,
	&CanvasScaler_get_defaultSpriteDPI_m1747_MethodInfo,
	&CanvasScaler_set_defaultSpriteDPI_m1748_MethodInfo,
	&CanvasScaler_get_dynamicPixelsPerUnit_m1749_MethodInfo,
	&CanvasScaler_set_dynamicPixelsPerUnit_m1750_MethodInfo,
	&CanvasScaler_OnEnable_m1751_MethodInfo,
	&CanvasScaler_OnDisable_m1752_MethodInfo,
	&CanvasScaler_Update_m1753_MethodInfo,
	&CanvasScaler_Handle_m1754_MethodInfo,
	&CanvasScaler_HandleWorldCanvas_m1755_MethodInfo,
	&CanvasScaler_HandleConstantPixelSize_m1756_MethodInfo,
	&CanvasScaler_HandleScaleWithScreenSize_m1757_MethodInfo,
	&CanvasScaler_HandleConstantPhysicalSize_m1758_MethodInfo,
	&CanvasScaler_SetScaleFactor_m1759_MethodInfo,
	&CanvasScaler_SetReferencePixelsPerUnit_m1760_MethodInfo,
	NULL
};
extern const MethodInfo CanvasScaler_get_uiScaleMode_m1731_MethodInfo;
extern const MethodInfo CanvasScaler_set_uiScaleMode_m1732_MethodInfo;
static const PropertyInfo CanvasScaler_t360____uiScaleMode_PropertyInfo = 
{
	&CanvasScaler_t360_il2cpp_TypeInfo/* parent */
	, "uiScaleMode"/* name */
	, &CanvasScaler_get_uiScaleMode_m1731_MethodInfo/* get */
	, &CanvasScaler_set_uiScaleMode_m1732_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_referencePixelsPerUnit_m1733_MethodInfo;
extern const MethodInfo CanvasScaler_set_referencePixelsPerUnit_m1734_MethodInfo;
static const PropertyInfo CanvasScaler_t360____referencePixelsPerUnit_PropertyInfo = 
{
	&CanvasScaler_t360_il2cpp_TypeInfo/* parent */
	, "referencePixelsPerUnit"/* name */
	, &CanvasScaler_get_referencePixelsPerUnit_m1733_MethodInfo/* get */
	, &CanvasScaler_set_referencePixelsPerUnit_m1734_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_scaleFactor_m1735_MethodInfo;
extern const MethodInfo CanvasScaler_set_scaleFactor_m1736_MethodInfo;
static const PropertyInfo CanvasScaler_t360____scaleFactor_PropertyInfo = 
{
	&CanvasScaler_t360_il2cpp_TypeInfo/* parent */
	, "scaleFactor"/* name */
	, &CanvasScaler_get_scaleFactor_m1735_MethodInfo/* get */
	, &CanvasScaler_set_scaleFactor_m1736_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_referenceResolution_m1737_MethodInfo;
extern const MethodInfo CanvasScaler_set_referenceResolution_m1738_MethodInfo;
static const PropertyInfo CanvasScaler_t360____referenceResolution_PropertyInfo = 
{
	&CanvasScaler_t360_il2cpp_TypeInfo/* parent */
	, "referenceResolution"/* name */
	, &CanvasScaler_get_referenceResolution_m1737_MethodInfo/* get */
	, &CanvasScaler_set_referenceResolution_m1738_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_screenMatchMode_m1739_MethodInfo;
extern const MethodInfo CanvasScaler_set_screenMatchMode_m1740_MethodInfo;
static const PropertyInfo CanvasScaler_t360____screenMatchMode_PropertyInfo = 
{
	&CanvasScaler_t360_il2cpp_TypeInfo/* parent */
	, "screenMatchMode"/* name */
	, &CanvasScaler_get_screenMatchMode_m1739_MethodInfo/* get */
	, &CanvasScaler_set_screenMatchMode_m1740_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_matchWidthOrHeight_m1741_MethodInfo;
extern const MethodInfo CanvasScaler_set_matchWidthOrHeight_m1742_MethodInfo;
static const PropertyInfo CanvasScaler_t360____matchWidthOrHeight_PropertyInfo = 
{
	&CanvasScaler_t360_il2cpp_TypeInfo/* parent */
	, "matchWidthOrHeight"/* name */
	, &CanvasScaler_get_matchWidthOrHeight_m1741_MethodInfo/* get */
	, &CanvasScaler_set_matchWidthOrHeight_m1742_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_physicalUnit_m1743_MethodInfo;
extern const MethodInfo CanvasScaler_set_physicalUnit_m1744_MethodInfo;
static const PropertyInfo CanvasScaler_t360____physicalUnit_PropertyInfo = 
{
	&CanvasScaler_t360_il2cpp_TypeInfo/* parent */
	, "physicalUnit"/* name */
	, &CanvasScaler_get_physicalUnit_m1743_MethodInfo/* get */
	, &CanvasScaler_set_physicalUnit_m1744_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_fallbackScreenDPI_m1745_MethodInfo;
extern const MethodInfo CanvasScaler_set_fallbackScreenDPI_m1746_MethodInfo;
static const PropertyInfo CanvasScaler_t360____fallbackScreenDPI_PropertyInfo = 
{
	&CanvasScaler_t360_il2cpp_TypeInfo/* parent */
	, "fallbackScreenDPI"/* name */
	, &CanvasScaler_get_fallbackScreenDPI_m1745_MethodInfo/* get */
	, &CanvasScaler_set_fallbackScreenDPI_m1746_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_defaultSpriteDPI_m1747_MethodInfo;
extern const MethodInfo CanvasScaler_set_defaultSpriteDPI_m1748_MethodInfo;
static const PropertyInfo CanvasScaler_t360____defaultSpriteDPI_PropertyInfo = 
{
	&CanvasScaler_t360_il2cpp_TypeInfo/* parent */
	, "defaultSpriteDPI"/* name */
	, &CanvasScaler_get_defaultSpriteDPI_m1747_MethodInfo/* get */
	, &CanvasScaler_set_defaultSpriteDPI_m1748_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo CanvasScaler_get_dynamicPixelsPerUnit_m1749_MethodInfo;
extern const MethodInfo CanvasScaler_set_dynamicPixelsPerUnit_m1750_MethodInfo;
static const PropertyInfo CanvasScaler_t360____dynamicPixelsPerUnit_PropertyInfo = 
{
	&CanvasScaler_t360_il2cpp_TypeInfo/* parent */
	, "dynamicPixelsPerUnit"/* name */
	, &CanvasScaler_get_dynamicPixelsPerUnit_m1749_MethodInfo/* get */
	, &CanvasScaler_set_dynamicPixelsPerUnit_m1750_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* CanvasScaler_t360_PropertyInfos[] =
{
	&CanvasScaler_t360____uiScaleMode_PropertyInfo,
	&CanvasScaler_t360____referencePixelsPerUnit_PropertyInfo,
	&CanvasScaler_t360____scaleFactor_PropertyInfo,
	&CanvasScaler_t360____referenceResolution_PropertyInfo,
	&CanvasScaler_t360____screenMatchMode_PropertyInfo,
	&CanvasScaler_t360____matchWidthOrHeight_PropertyInfo,
	&CanvasScaler_t360____physicalUnit_PropertyInfo,
	&CanvasScaler_t360____fallbackScreenDPI_PropertyInfo,
	&CanvasScaler_t360____defaultSpriteDPI_PropertyInfo,
	&CanvasScaler_t360____dynamicPixelsPerUnit_PropertyInfo,
	NULL
};
static const Il2CppType* CanvasScaler_t360_il2cpp_TypeInfo__nestedTypes[3] =
{
	&ScaleMode_t357_0_0_0,
	&ScreenMatchMode_t358_0_0_0,
	&Unit_t359_0_0_0,
};
extern const MethodInfo CanvasScaler_OnEnable_m1751_MethodInfo;
extern const MethodInfo CanvasScaler_OnDisable_m1752_MethodInfo;
extern const MethodInfo CanvasScaler_Update_m1753_MethodInfo;
extern const MethodInfo CanvasScaler_Handle_m1754_MethodInfo;
extern const MethodInfo CanvasScaler_HandleWorldCanvas_m1755_MethodInfo;
extern const MethodInfo CanvasScaler_HandleConstantPixelSize_m1756_MethodInfo;
extern const MethodInfo CanvasScaler_HandleScaleWithScreenSize_m1757_MethodInfo;
extern const MethodInfo CanvasScaler_HandleConstantPhysicalSize_m1758_MethodInfo;
static const Il2CppMethodReference CanvasScaler_t360_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&CanvasScaler_OnEnable_m1751_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&CanvasScaler_OnDisable_m1752_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m858_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m861_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&CanvasScaler_Update_m1753_MethodInfo,
	&CanvasScaler_Handle_m1754_MethodInfo,
	&CanvasScaler_HandleWorldCanvas_m1755_MethodInfo,
	&CanvasScaler_HandleConstantPixelSize_m1756_MethodInfo,
	&CanvasScaler_HandleScaleWithScreenSize_m1757_MethodInfo,
	&CanvasScaler_HandleConstantPhysicalSize_m1758_MethodInfo,
};
static bool CanvasScaler_t360_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType CanvasScaler_t360_1_0_0;
struct CanvasScaler_t360;
const Il2CppTypeDefinitionMetadata CanvasScaler_t360_DefinitionMetadata = 
{
	NULL/* declaringType */
	, CanvasScaler_t360_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &UIBehaviour_t199_0_0_0/* parent */
	, CanvasScaler_t360_VTable/* vtableMethods */
	, CanvasScaler_t360_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 538/* fieldStart */

};
TypeInfo CanvasScaler_t360_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasScaler"/* name */
	, "UnityEngine.UI"/* namespaze */
	, CanvasScaler_t360_MethodInfos/* methods */
	, CanvasScaler_t360_PropertyInfos/* properties */
	, NULL/* events */
	, &CanvasScaler_t360_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 270/* custom_attributes_cache */
	, &CanvasScaler_t360_0_0_0/* byval_arg */
	, &CanvasScaler_t360_1_0_0/* this_arg */
	, &CanvasScaler_t360_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasScaler_t360)/* instance_size */
	, sizeof (CanvasScaler_t360)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 31/* method_count */
	, 10/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 22/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
// Metadata Definition UnityEngine.UI.ContentSizeFitter/FitMode
extern TypeInfo FitMode_t361_il2cpp_TypeInfo;
// UnityEngine.UI.ContentSizeFitter/FitMode
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitModeMethodDeclarations.h"
static const MethodInfo* FitMode_t361_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference FitMode_t361_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool FitMode_t361_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair FitMode_t361_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType FitMode_t361_0_0_0;
extern const Il2CppType FitMode_t361_1_0_0;
extern TypeInfo ContentSizeFitter_t362_il2cpp_TypeInfo;
extern const Il2CppType ContentSizeFitter_t362_0_0_0;
const Il2CppTypeDefinitionMetadata FitMode_t361_DefinitionMetadata = 
{
	&ContentSizeFitter_t362_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, FitMode_t361_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, FitMode_t361_VTable/* vtableMethods */
	, FitMode_t361_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 552/* fieldStart */

};
TypeInfo FitMode_t361_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "FitMode"/* name */
	, ""/* namespaze */
	, FitMode_t361_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &FitMode_t361_0_0_0/* byval_arg */
	, &FitMode_t361_1_0_0/* this_arg */
	, &FitMode_t361_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FitMode_t361)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (FitMode_t361)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter.h"
// Metadata Definition UnityEngine.UI.ContentSizeFitter
// UnityEngine.UI.ContentSizeFitter
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitterMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::.ctor()
extern const MethodInfo ContentSizeFitter__ctor_m1761_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ContentSizeFitter__ctor_m1761/* method */
	, &ContentSizeFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1062/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_FitMode_t361 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_horizontalFit()
extern const MethodInfo ContentSizeFitter_get_horizontalFit_m1762_MethodInfo = 
{
	"get_horizontalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_get_horizontalFit_m1762/* method */
	, &ContentSizeFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &FitMode_t361_0_0_0/* return_type */
	, RuntimeInvoker_FitMode_t361/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1063/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FitMode_t361_0_0_0;
static const ParameterInfo ContentSizeFitter_t362_ContentSizeFitter_set_horizontalFit_m1763_ParameterInfos[] = 
{
	{"value", 0, 134218337, 0, &FitMode_t361_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::set_horizontalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern const MethodInfo ContentSizeFitter_set_horizontalFit_m1763_MethodInfo = 
{
	"set_horizontalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_set_horizontalFit_m1763/* method */
	, &ContentSizeFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, ContentSizeFitter_t362_ContentSizeFitter_set_horizontalFit_m1763_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1064/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_FitMode_t361 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::get_verticalFit()
extern const MethodInfo ContentSizeFitter_get_verticalFit_m1764_MethodInfo = 
{
	"get_verticalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_get_verticalFit_m1764/* method */
	, &ContentSizeFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &FitMode_t361_0_0_0/* return_type */
	, RuntimeInvoker_FitMode_t361/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1065/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType FitMode_t361_0_0_0;
static const ParameterInfo ContentSizeFitter_t362_ContentSizeFitter_set_verticalFit_m1765_ParameterInfos[] = 
{
	{"value", 0, 134218338, 0, &FitMode_t361_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::set_verticalFit(UnityEngine.UI.ContentSizeFitter/FitMode)
extern const MethodInfo ContentSizeFitter_set_verticalFit_m1765_MethodInfo = 
{
	"set_verticalFit"/* name */
	, (methodPointerType)&ContentSizeFitter_set_verticalFit_m1765/* method */
	, &ContentSizeFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, ContentSizeFitter_t362_ContentSizeFitter_set_verticalFit_m1765_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1066/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::get_rectTransform()
extern const MethodInfo ContentSizeFitter_get_rectTransform_m1766_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&ContentSizeFitter_get_rectTransform_m1766/* method */
	, &ContentSizeFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t272_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1067/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnEnable()
extern const MethodInfo ContentSizeFitter_OnEnable_m1767_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&ContentSizeFitter_OnEnable_m1767/* method */
	, &ContentSizeFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1068/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnDisable()
extern const MethodInfo ContentSizeFitter_OnDisable_m1768_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&ContentSizeFitter_OnDisable_m1768/* method */
	, &ContentSizeFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1069/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::OnRectTransformDimensionsChange()
extern const MethodInfo ContentSizeFitter_OnRectTransformDimensionsChange_m1769_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&ContentSizeFitter_OnRectTransformDimensionsChange_m1769/* method */
	, &ContentSizeFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1070/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo ContentSizeFitter_t362_ContentSizeFitter_HandleSelfFittingAlongAxis_m1770_ParameterInfos[] = 
{
	{"axis", 0, 134218339, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::HandleSelfFittingAlongAxis(System.Int32)
extern const MethodInfo ContentSizeFitter_HandleSelfFittingAlongAxis_m1770_MethodInfo = 
{
	"HandleSelfFittingAlongAxis"/* name */
	, (methodPointerType)&ContentSizeFitter_HandleSelfFittingAlongAxis_m1770/* method */
	, &ContentSizeFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, ContentSizeFitter_t362_ContentSizeFitter_HandleSelfFittingAlongAxis_m1770_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1071/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutHorizontal()
extern const MethodInfo ContentSizeFitter_SetLayoutHorizontal_m1771_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&ContentSizeFitter_SetLayoutHorizontal_m1771/* method */
	, &ContentSizeFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1072/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetLayoutVertical()
extern const MethodInfo ContentSizeFitter_SetLayoutVertical_m1772_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&ContentSizeFitter_SetLayoutVertical_m1772/* method */
	, &ContentSizeFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1073/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ContentSizeFitter::SetDirty()
extern const MethodInfo ContentSizeFitter_SetDirty_m1773_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&ContentSizeFitter_SetDirty_m1773/* method */
	, &ContentSizeFitter_t362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1074/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ContentSizeFitter_t362_MethodInfos[] =
{
	&ContentSizeFitter__ctor_m1761_MethodInfo,
	&ContentSizeFitter_get_horizontalFit_m1762_MethodInfo,
	&ContentSizeFitter_set_horizontalFit_m1763_MethodInfo,
	&ContentSizeFitter_get_verticalFit_m1764_MethodInfo,
	&ContentSizeFitter_set_verticalFit_m1765_MethodInfo,
	&ContentSizeFitter_get_rectTransform_m1766_MethodInfo,
	&ContentSizeFitter_OnEnable_m1767_MethodInfo,
	&ContentSizeFitter_OnDisable_m1768_MethodInfo,
	&ContentSizeFitter_OnRectTransformDimensionsChange_m1769_MethodInfo,
	&ContentSizeFitter_HandleSelfFittingAlongAxis_m1770_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m1771_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m1772_MethodInfo,
	&ContentSizeFitter_SetDirty_m1773_MethodInfo,
	NULL
};
extern const MethodInfo ContentSizeFitter_get_horizontalFit_m1762_MethodInfo;
extern const MethodInfo ContentSizeFitter_set_horizontalFit_m1763_MethodInfo;
static const PropertyInfo ContentSizeFitter_t362____horizontalFit_PropertyInfo = 
{
	&ContentSizeFitter_t362_il2cpp_TypeInfo/* parent */
	, "horizontalFit"/* name */
	, &ContentSizeFitter_get_horizontalFit_m1762_MethodInfo/* get */
	, &ContentSizeFitter_set_horizontalFit_m1763_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ContentSizeFitter_get_verticalFit_m1764_MethodInfo;
extern const MethodInfo ContentSizeFitter_set_verticalFit_m1765_MethodInfo;
static const PropertyInfo ContentSizeFitter_t362____verticalFit_PropertyInfo = 
{
	&ContentSizeFitter_t362_il2cpp_TypeInfo/* parent */
	, "verticalFit"/* name */
	, &ContentSizeFitter_get_verticalFit_m1764_MethodInfo/* get */
	, &ContentSizeFitter_set_verticalFit_m1765_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ContentSizeFitter_get_rectTransform_m1766_MethodInfo;
static const PropertyInfo ContentSizeFitter_t362____rectTransform_PropertyInfo = 
{
	&ContentSizeFitter_t362_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &ContentSizeFitter_get_rectTransform_m1766_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ContentSizeFitter_t362_PropertyInfos[] =
{
	&ContentSizeFitter_t362____horizontalFit_PropertyInfo,
	&ContentSizeFitter_t362____verticalFit_PropertyInfo,
	&ContentSizeFitter_t362____rectTransform_PropertyInfo,
	NULL
};
static const Il2CppType* ContentSizeFitter_t362_il2cpp_TypeInfo__nestedTypes[1] =
{
	&FitMode_t361_0_0_0,
};
extern const MethodInfo ContentSizeFitter_OnEnable_m1767_MethodInfo;
extern const MethodInfo ContentSizeFitter_OnDisable_m1768_MethodInfo;
extern const MethodInfo ContentSizeFitter_OnRectTransformDimensionsChange_m1769_MethodInfo;
extern const MethodInfo ContentSizeFitter_SetLayoutHorizontal_m1771_MethodInfo;
extern const MethodInfo ContentSizeFitter_SetLayoutVertical_m1772_MethodInfo;
static const Il2CppMethodReference ContentSizeFitter_t362_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&ContentSizeFitter_OnEnable_m1767_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&ContentSizeFitter_OnDisable_m1768_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&ContentSizeFitter_OnRectTransformDimensionsChange_m1769_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m861_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m1771_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m1772_MethodInfo,
	&ContentSizeFitter_SetLayoutHorizontal_m1771_MethodInfo,
	&ContentSizeFitter_SetLayoutVertical_m1772_MethodInfo,
};
static bool ContentSizeFitter_t362_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* ContentSizeFitter_t362_InterfacesTypeInfos[] = 
{
	&ILayoutController_t475_0_0_0,
	&ILayoutSelfController_t476_0_0_0,
};
static Il2CppInterfaceOffsetPair ContentSizeFitter_t362_InterfacesOffsets[] = 
{
	{ &ILayoutController_t475_0_0_0, 16},
	{ &ILayoutSelfController_t476_0_0_0, 18},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ContentSizeFitter_t362_1_0_0;
struct ContentSizeFitter_t362;
const Il2CppTypeDefinitionMetadata ContentSizeFitter_t362_DefinitionMetadata = 
{
	NULL/* declaringType */
	, ContentSizeFitter_t362_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, ContentSizeFitter_t362_InterfacesTypeInfos/* implementedInterfaces */
	, ContentSizeFitter_t362_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t199_0_0_0/* parent */
	, ContentSizeFitter_t362_VTable/* vtableMethods */
	, ContentSizeFitter_t362_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 556/* fieldStart */

};
TypeInfo ContentSizeFitter_t362_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ContentSizeFitter"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ContentSizeFitter_t362_MethodInfos/* methods */
	, ContentSizeFitter_t362_PropertyInfos/* properties */
	, NULL/* events */
	, &ContentSizeFitter_t362_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 281/* custom_attributes_cache */
	, &ContentSizeFitter_t362_0_0_0/* byval_arg */
	, &ContentSizeFitter_t362_1_0_0/* this_arg */
	, &ContentSizeFitter_t362_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ContentSizeFitter_t362)/* instance_size */
	, sizeof (ContentSizeFitter_t362)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 20/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Corner
extern TypeInfo Corner_t363_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_CornerMethodDeclarations.h"
static const MethodInfo* Corner_t363_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Corner_t363_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool Corner_t363_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Corner_t363_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Corner_t363_0_0_0;
extern const Il2CppType Corner_t363_1_0_0;
extern TypeInfo GridLayoutGroup_t366_il2cpp_TypeInfo;
extern const Il2CppType GridLayoutGroup_t366_0_0_0;
const Il2CppTypeDefinitionMetadata Corner_t363_DefinitionMetadata = 
{
	&GridLayoutGroup_t366_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Corner_t363_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, Corner_t363_VTable/* vtableMethods */
	, Corner_t363_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 560/* fieldStart */

};
TypeInfo Corner_t363_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Corner"/* name */
	, ""/* namespaze */
	, Corner_t363_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Corner_t363_0_0_0/* byval_arg */
	, &Corner_t363_1_0_0/* this_arg */
	, &Corner_t363_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Corner_t363)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Corner_t363)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Axis
extern TypeInfo Axis_t364_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_AxisMethodDeclarations.h"
static const MethodInfo* Axis_t364_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Axis_t364_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool Axis_t364_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Axis_t364_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Axis_t364_0_0_0;
extern const Il2CppType Axis_t364_1_0_0;
const Il2CppTypeDefinitionMetadata Axis_t364_DefinitionMetadata = 
{
	&GridLayoutGroup_t366_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Axis_t364_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, Axis_t364_VTable/* vtableMethods */
	, Axis_t364_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 565/* fieldStart */

};
TypeInfo Axis_t364_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Axis"/* name */
	, ""/* namespaze */
	, Axis_t364_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Axis_t364_0_0_0/* byval_arg */
	, &Axis_t364_1_0_0/* this_arg */
	, &Axis_t364_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Axis_t364)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Axis_t364)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup/Constraint
extern TypeInfo Constraint_t365_il2cpp_TypeInfo;
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_ConstraintMethodDeclarations.h"
static const MethodInfo* Constraint_t365_MethodInfos[] =
{
	NULL
};
static const Il2CppMethodReference Constraint_t365_VTable[] =
{
	&Enum_Equals_m514_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Enum_GetHashCode_m516_MethodInfo,
	&Enum_ToString_m517_MethodInfo,
	&Enum_ToString_m518_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m519_MethodInfo,
	&Enum_System_IConvertible_ToByte_m520_MethodInfo,
	&Enum_System_IConvertible_ToChar_m521_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m522_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m523_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m524_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m525_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m526_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m527_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m528_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m529_MethodInfo,
	&Enum_ToString_m530_MethodInfo,
	&Enum_System_IConvertible_ToType_m531_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m532_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m533_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m534_MethodInfo,
	&Enum_CompareTo_m535_MethodInfo,
	&Enum_GetTypeCode_m536_MethodInfo,
};
static bool Constraint_t365_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Constraint_t365_InterfacesOffsets[] = 
{
	{ &IFormattable_t164_0_0_0, 4},
	{ &IConvertible_t165_0_0_0, 5},
	{ &IComparable_t166_0_0_0, 21},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Constraint_t365_0_0_0;
extern const Il2CppType Constraint_t365_1_0_0;
const Il2CppTypeDefinitionMetadata Constraint_t365_DefinitionMetadata = 
{
	&GridLayoutGroup_t366_0_0_0/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Constraint_t365_InterfacesOffsets/* interfaceOffsets */
	, &Enum_t167_0_0_0/* parent */
	, Constraint_t365_VTable/* vtableMethods */
	, Constraint_t365_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 568/* fieldStart */

};
TypeInfo Constraint_t365_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Constraint"/* name */
	, ""/* namespaze */
	, Constraint_t365_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Int32_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &Constraint_t365_0_0_0/* byval_arg */
	, &Constraint_t365_1_0_0/* this_arg */
	, &Constraint_t365_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Constraint_t365)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (Constraint_t365)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup.h"
// Metadata Definition UnityEngine.UI.GridLayoutGroup
// UnityEngine.UI.GridLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::.ctor()
extern const MethodInfo GridLayoutGroup__ctor_m1774_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GridLayoutGroup__ctor_m1774/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1075/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Corner_t363 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::get_startCorner()
extern const MethodInfo GridLayoutGroup_get_startCorner_m1775_MethodInfo = 
{
	"get_startCorner"/* name */
	, (methodPointerType)&GridLayoutGroup_get_startCorner_m1775/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Corner_t363_0_0_0/* return_type */
	, RuntimeInvoker_Corner_t363/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1076/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Corner_t363_0_0_0;
static const ParameterInfo GridLayoutGroup_t366_GridLayoutGroup_set_startCorner_m1776_ParameterInfos[] = 
{
	{"value", 0, 134218340, 0, &Corner_t363_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_startCorner(UnityEngine.UI.GridLayoutGroup/Corner)
extern const MethodInfo GridLayoutGroup_set_startCorner_m1776_MethodInfo = 
{
	"set_startCorner"/* name */
	, (methodPointerType)&GridLayoutGroup_set_startCorner_m1776/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, GridLayoutGroup_t366_GridLayoutGroup_set_startCorner_m1776_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1077/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Axis_t364 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::get_startAxis()
extern const MethodInfo GridLayoutGroup_get_startAxis_m1777_MethodInfo = 
{
	"get_startAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_get_startAxis_m1777/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Axis_t364_0_0_0/* return_type */
	, RuntimeInvoker_Axis_t364/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1078/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Axis_t364_0_0_0;
static const ParameterInfo GridLayoutGroup_t366_GridLayoutGroup_set_startAxis_m1778_ParameterInfos[] = 
{
	{"value", 0, 134218341, 0, &Axis_t364_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_startAxis(UnityEngine.UI.GridLayoutGroup/Axis)
extern const MethodInfo GridLayoutGroup_set_startAxis_m1778_MethodInfo = 
{
	"set_startAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_set_startAxis_m1778/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, GridLayoutGroup_t366_GridLayoutGroup_set_startAxis_m1778_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1079/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_cellSize()
extern const MethodInfo GridLayoutGroup_get_cellSize_m1779_MethodInfo = 
{
	"get_cellSize"/* name */
	, (methodPointerType)&GridLayoutGroup_get_cellSize_m1779/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1080/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo GridLayoutGroup_t366_GridLayoutGroup_set_cellSize_m1780_ParameterInfos[] = 
{
	{"value", 0, 134218342, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_cellSize(UnityEngine.Vector2)
extern const MethodInfo GridLayoutGroup_set_cellSize_m1780_MethodInfo = 
{
	"set_cellSize"/* name */
	, (methodPointerType)&GridLayoutGroup_set_cellSize_m1780/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector2_t10/* invoker_method */
	, GridLayoutGroup_t366_GridLayoutGroup_set_cellSize_m1780_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1081/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_spacing()
extern const MethodInfo GridLayoutGroup_get_spacing_m1781_MethodInfo = 
{
	"get_spacing"/* name */
	, (methodPointerType)&GridLayoutGroup_get_spacing_m1781/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1082/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo GridLayoutGroup_t366_GridLayoutGroup_set_spacing_m1782_ParameterInfos[] = 
{
	{"value", 0, 134218343, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_spacing(UnityEngine.Vector2)
extern const MethodInfo GridLayoutGroup_set_spacing_m1782_MethodInfo = 
{
	"set_spacing"/* name */
	, (methodPointerType)&GridLayoutGroup_set_spacing_m1782/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector2_t10/* invoker_method */
	, GridLayoutGroup_t366_GridLayoutGroup_set_spacing_m1782_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1083/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Constraint_t365 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::get_constraint()
extern const MethodInfo GridLayoutGroup_get_constraint_m1783_MethodInfo = 
{
	"get_constraint"/* name */
	, (methodPointerType)&GridLayoutGroup_get_constraint_m1783/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Constraint_t365_0_0_0/* return_type */
	, RuntimeInvoker_Constraint_t365/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1084/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Constraint_t365_0_0_0;
static const ParameterInfo GridLayoutGroup_t366_GridLayoutGroup_set_constraint_m1784_ParameterInfos[] = 
{
	{"value", 0, 134218344, 0, &Constraint_t365_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraint(UnityEngine.UI.GridLayoutGroup/Constraint)
extern const MethodInfo GridLayoutGroup_set_constraint_m1784_MethodInfo = 
{
	"set_constraint"/* name */
	, (methodPointerType)&GridLayoutGroup_set_constraint_m1784/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, GridLayoutGroup_t366_GridLayoutGroup_set_constraint_m1784_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1085/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.GridLayoutGroup::get_constraintCount()
extern const MethodInfo GridLayoutGroup_get_constraintCount_m1785_MethodInfo = 
{
	"get_constraintCount"/* name */
	, (methodPointerType)&GridLayoutGroup_get_constraintCount_m1785/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1086/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo GridLayoutGroup_t366_GridLayoutGroup_set_constraintCount_m1786_ParameterInfos[] = 
{
	{"value", 0, 134218345, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraintCount(System.Int32)
extern const MethodInfo GridLayoutGroup_set_constraintCount_m1786_MethodInfo = 
{
	"set_constraintCount"/* name */
	, (methodPointerType)&GridLayoutGroup_set_constraintCount_m1786/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, GridLayoutGroup_t366_GridLayoutGroup_set_constraintCount_m1786_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1087/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputHorizontal_m1787_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&GridLayoutGroup_CalculateLayoutInputHorizontal_m1787/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1088/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputVertical_m1788_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&GridLayoutGroup_CalculateLayoutInputVertical_m1788/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1089/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutHorizontal()
extern const MethodInfo GridLayoutGroup_SetLayoutHorizontal_m1789_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&GridLayoutGroup_SetLayoutHorizontal_m1789/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1090/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutVertical()
extern const MethodInfo GridLayoutGroup_SetLayoutVertical_m1790_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&GridLayoutGroup_SetLayoutVertical_m1790/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1091/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo GridLayoutGroup_t366_GridLayoutGroup_SetCellsAlongAxis_m1791_ParameterInfos[] = 
{
	{"axis", 0, 134218346, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.GridLayoutGroup::SetCellsAlongAxis(System.Int32)
extern const MethodInfo GridLayoutGroup_SetCellsAlongAxis_m1791_MethodInfo = 
{
	"SetCellsAlongAxis"/* name */
	, (methodPointerType)&GridLayoutGroup_SetCellsAlongAxis_m1791/* method */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, GridLayoutGroup_t366_GridLayoutGroup_SetCellsAlongAxis_m1791_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1092/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* GridLayoutGroup_t366_MethodInfos[] =
{
	&GridLayoutGroup__ctor_m1774_MethodInfo,
	&GridLayoutGroup_get_startCorner_m1775_MethodInfo,
	&GridLayoutGroup_set_startCorner_m1776_MethodInfo,
	&GridLayoutGroup_get_startAxis_m1777_MethodInfo,
	&GridLayoutGroup_set_startAxis_m1778_MethodInfo,
	&GridLayoutGroup_get_cellSize_m1779_MethodInfo,
	&GridLayoutGroup_set_cellSize_m1780_MethodInfo,
	&GridLayoutGroup_get_spacing_m1781_MethodInfo,
	&GridLayoutGroup_set_spacing_m1782_MethodInfo,
	&GridLayoutGroup_get_constraint_m1783_MethodInfo,
	&GridLayoutGroup_set_constraint_m1784_MethodInfo,
	&GridLayoutGroup_get_constraintCount_m1785_MethodInfo,
	&GridLayoutGroup_set_constraintCount_m1786_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m1787_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m1788_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m1789_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m1790_MethodInfo,
	&GridLayoutGroup_SetCellsAlongAxis_m1791_MethodInfo,
	NULL
};
extern const MethodInfo GridLayoutGroup_get_startCorner_m1775_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_startCorner_m1776_MethodInfo;
static const PropertyInfo GridLayoutGroup_t366____startCorner_PropertyInfo = 
{
	&GridLayoutGroup_t366_il2cpp_TypeInfo/* parent */
	, "startCorner"/* name */
	, &GridLayoutGroup_get_startCorner_m1775_MethodInfo/* get */
	, &GridLayoutGroup_set_startCorner_m1776_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_startAxis_m1777_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_startAxis_m1778_MethodInfo;
static const PropertyInfo GridLayoutGroup_t366____startAxis_PropertyInfo = 
{
	&GridLayoutGroup_t366_il2cpp_TypeInfo/* parent */
	, "startAxis"/* name */
	, &GridLayoutGroup_get_startAxis_m1777_MethodInfo/* get */
	, &GridLayoutGroup_set_startAxis_m1778_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_cellSize_m1779_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_cellSize_m1780_MethodInfo;
static const PropertyInfo GridLayoutGroup_t366____cellSize_PropertyInfo = 
{
	&GridLayoutGroup_t366_il2cpp_TypeInfo/* parent */
	, "cellSize"/* name */
	, &GridLayoutGroup_get_cellSize_m1779_MethodInfo/* get */
	, &GridLayoutGroup_set_cellSize_m1780_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_spacing_m1781_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_spacing_m1782_MethodInfo;
static const PropertyInfo GridLayoutGroup_t366____spacing_PropertyInfo = 
{
	&GridLayoutGroup_t366_il2cpp_TypeInfo/* parent */
	, "spacing"/* name */
	, &GridLayoutGroup_get_spacing_m1781_MethodInfo/* get */
	, &GridLayoutGroup_set_spacing_m1782_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_constraint_m1783_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_constraint_m1784_MethodInfo;
static const PropertyInfo GridLayoutGroup_t366____constraint_PropertyInfo = 
{
	&GridLayoutGroup_t366_il2cpp_TypeInfo/* parent */
	, "constraint"/* name */
	, &GridLayoutGroup_get_constraint_m1783_MethodInfo/* get */
	, &GridLayoutGroup_set_constraint_m1784_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo GridLayoutGroup_get_constraintCount_m1785_MethodInfo;
extern const MethodInfo GridLayoutGroup_set_constraintCount_m1786_MethodInfo;
static const PropertyInfo GridLayoutGroup_t366____constraintCount_PropertyInfo = 
{
	&GridLayoutGroup_t366_il2cpp_TypeInfo/* parent */
	, "constraintCount"/* name */
	, &GridLayoutGroup_get_constraintCount_m1785_MethodInfo/* get */
	, &GridLayoutGroup_set_constraintCount_m1786_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* GridLayoutGroup_t366_PropertyInfos[] =
{
	&GridLayoutGroup_t366____startCorner_PropertyInfo,
	&GridLayoutGroup_t366____startAxis_PropertyInfo,
	&GridLayoutGroup_t366____cellSize_PropertyInfo,
	&GridLayoutGroup_t366____spacing_PropertyInfo,
	&GridLayoutGroup_t366____constraint_PropertyInfo,
	&GridLayoutGroup_t366____constraintCount_PropertyInfo,
	NULL
};
static const Il2CppType* GridLayoutGroup_t366_il2cpp_TypeInfo__nestedTypes[3] =
{
	&Corner_t363_0_0_0,
	&Axis_t364_0_0_0,
	&Constraint_t365_0_0_0,
};
extern const MethodInfo LayoutGroup_OnEnable_m1845_MethodInfo;
extern const MethodInfo LayoutGroup_OnDisable_m1846_MethodInfo;
extern const MethodInfo LayoutGroup_OnRectTransformDimensionsChange_m1855_MethodInfo;
extern const MethodInfo LayoutGroup_OnDidApplyAnimationProperties_m1847_MethodInfo;
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputHorizontal_m1787_MethodInfo;
extern const MethodInfo GridLayoutGroup_CalculateLayoutInputVertical_m1788_MethodInfo;
extern const MethodInfo LayoutGroup_get_minWidth_m1838_MethodInfo;
extern const MethodInfo LayoutGroup_get_preferredWidth_m1839_MethodInfo;
extern const MethodInfo LayoutGroup_get_flexibleWidth_m1840_MethodInfo;
extern const MethodInfo LayoutGroup_get_minHeight_m1841_MethodInfo;
extern const MethodInfo LayoutGroup_get_preferredHeight_m1842_MethodInfo;
extern const MethodInfo LayoutGroup_get_flexibleHeight_m1843_MethodInfo;
extern const MethodInfo LayoutGroup_get_layoutPriority_m1844_MethodInfo;
extern const MethodInfo GridLayoutGroup_SetLayoutHorizontal_m1789_MethodInfo;
extern const MethodInfo GridLayoutGroup_SetLayoutVertical_m1790_MethodInfo;
extern const MethodInfo LayoutGroup_OnTransformChildrenChanged_m1856_MethodInfo;
static const Il2CppMethodReference GridLayoutGroup_t366_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&LayoutGroup_OnEnable_m1845_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&LayoutGroup_OnDisable_m1846_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1855_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1847_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m1787_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m1788_MethodInfo,
	&LayoutGroup_get_minWidth_m1838_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1839_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1840_MethodInfo,
	&LayoutGroup_get_minHeight_m1841_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1842_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1843_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1844_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m1789_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m1790_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputHorizontal_m1787_MethodInfo,
	&GridLayoutGroup_CalculateLayoutInputVertical_m1788_MethodInfo,
	&LayoutGroup_get_minWidth_m1838_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1839_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1840_MethodInfo,
	&LayoutGroup_get_minHeight_m1841_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1842_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1843_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1844_MethodInfo,
	&GridLayoutGroup_SetLayoutHorizontal_m1789_MethodInfo,
	&GridLayoutGroup_SetLayoutVertical_m1790_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m1856_MethodInfo,
};
static bool GridLayoutGroup_t366_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType ILayoutGroup_t473_0_0_0;
static Il2CppInterfaceOffsetPair GridLayoutGroup_t366_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t419_0_0_0, 16},
	{ &ILayoutController_t475_0_0_0, 25},
	{ &ILayoutGroup_t473_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType GridLayoutGroup_t366_1_0_0;
extern const Il2CppType LayoutGroup_t367_0_0_0;
struct GridLayoutGroup_t366;
const Il2CppTypeDefinitionMetadata GridLayoutGroup_t366_DefinitionMetadata = 
{
	NULL/* declaringType */
	, GridLayoutGroup_t366_il2cpp_TypeInfo__nestedTypes/* nestedTypes */
	, NULL/* implementedInterfaces */
	, GridLayoutGroup_t366_InterfacesOffsets/* interfaceOffsets */
	, &LayoutGroup_t367_0_0_0/* parent */
	, GridLayoutGroup_t366_VTable/* vtableMethods */
	, GridLayoutGroup_t366_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 572/* fieldStart */

};
TypeInfo GridLayoutGroup_t366_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "GridLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, GridLayoutGroup_t366_MethodInfos/* methods */
	, GridLayoutGroup_t366_PropertyInfos/* properties */
	, NULL/* events */
	, &GridLayoutGroup_t366_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 284/* custom_attributes_cache */
	, &GridLayoutGroup_t366_0_0_0/* byval_arg */
	, &GridLayoutGroup_t366_1_0_0/* this_arg */
	, &GridLayoutGroup_t366_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GridLayoutGroup_t366)/* instance_size */
	, sizeof (GridLayoutGroup_t366)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 18/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 3/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroup.h"
// Metadata Definition UnityEngine.UI.HorizontalLayoutGroup
extern TypeInfo HorizontalLayoutGroup_t368_il2cpp_TypeInfo;
// UnityEngine.UI.HorizontalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::.ctor()
extern const MethodInfo HorizontalLayoutGroup__ctor_m1792_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HorizontalLayoutGroup__ctor_m1792/* method */
	, &HorizontalLayoutGroup_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1093/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1793_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1793/* method */
	, &HorizontalLayoutGroup_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1094/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputVertical_m1794_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_CalculateLayoutInputVertical_m1794/* method */
	, &HorizontalLayoutGroup_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1095/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutHorizontal()
extern const MethodInfo HorizontalLayoutGroup_SetLayoutHorizontal_m1795_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_SetLayoutHorizontal_m1795/* method */
	, &HorizontalLayoutGroup_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1096/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutVertical()
extern const MethodInfo HorizontalLayoutGroup_SetLayoutVertical_m1796_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&HorizontalLayoutGroup_SetLayoutVertical_m1796/* method */
	, &HorizontalLayoutGroup_t368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1097/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HorizontalLayoutGroup_t368_MethodInfos[] =
{
	&HorizontalLayoutGroup__ctor_m1792_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1793_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m1794_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m1795_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m1796_MethodInfo,
	NULL
};
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1793_MethodInfo;
extern const MethodInfo HorizontalLayoutGroup_CalculateLayoutInputVertical_m1794_MethodInfo;
extern const MethodInfo HorizontalLayoutGroup_SetLayoutHorizontal_m1795_MethodInfo;
extern const MethodInfo HorizontalLayoutGroup_SetLayoutVertical_m1796_MethodInfo;
static const Il2CppMethodReference HorizontalLayoutGroup_t368_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&LayoutGroup_OnEnable_m1845_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&LayoutGroup_OnDisable_m1846_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1855_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1847_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1793_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m1794_MethodInfo,
	&LayoutGroup_get_minWidth_m1838_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1839_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1840_MethodInfo,
	&LayoutGroup_get_minHeight_m1841_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1842_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1843_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1844_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m1795_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m1796_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1793_MethodInfo,
	&HorizontalLayoutGroup_CalculateLayoutInputVertical_m1794_MethodInfo,
	&LayoutGroup_get_minWidth_m1838_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1839_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1840_MethodInfo,
	&LayoutGroup_get_minHeight_m1841_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1842_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1843_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1844_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutHorizontal_m1795_MethodInfo,
	&HorizontalLayoutGroup_SetLayoutVertical_m1796_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m1856_MethodInfo,
};
static bool HorizontalLayoutGroup_t368_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HorizontalLayoutGroup_t368_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t419_0_0_0, 16},
	{ &ILayoutController_t475_0_0_0, 25},
	{ &ILayoutGroup_t473_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType HorizontalLayoutGroup_t368_0_0_0;
extern const Il2CppType HorizontalLayoutGroup_t368_1_0_0;
extern const Il2CppType HorizontalOrVerticalLayoutGroup_t369_0_0_0;
struct HorizontalLayoutGroup_t368;
const Il2CppTypeDefinitionMetadata HorizontalLayoutGroup_t368_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HorizontalLayoutGroup_t368_InterfacesOffsets/* interfaceOffsets */
	, &HorizontalOrVerticalLayoutGroup_t369_0_0_0/* parent */
	, HorizontalLayoutGroup_t368_VTable/* vtableMethods */
	, HorizontalLayoutGroup_t368_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo HorizontalLayoutGroup_t368_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "HorizontalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, HorizontalLayoutGroup_t368_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &HorizontalLayoutGroup_t368_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 291/* custom_attributes_cache */
	, &HorizontalLayoutGroup_t368_0_0_0/* byval_arg */
	, &HorizontalLayoutGroup_t368_1_0_0/* this_arg */
	, &HorizontalLayoutGroup_t368_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HorizontalLayoutGroup_t368)/* instance_size */
	, sizeof (HorizontalLayoutGroup_t368)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrou.h"
// Metadata Definition UnityEngine.UI.HorizontalOrVerticalLayoutGroup
extern TypeInfo HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo;
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalLayoutGrouMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::.ctor()
extern const MethodInfo HorizontalOrVerticalLayoutGroup__ctor_m1797_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup__ctor_m1797/* method */
	, &HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1098/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_spacing()
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_spacing_m1798_MethodInfo = 
{
	"get_spacing"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_spacing_m1798/* method */
	, &HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1099/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t369_HorizontalOrVerticalLayoutGroup_set_spacing_m1799_ParameterInfos[] = 
{
	{"value", 0, 134218347, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_spacing(System.Single)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_spacing_m1799_MethodInfo = 
{
	"set_spacing"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_spacing_m1799/* method */
	, &HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t369_HorizontalOrVerticalLayoutGroup_set_spacing_m1799_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandWidth()
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1800_MethodInfo = 
{
	"get_childForceExpandWidth"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1800/* method */
	, &HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t369_HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1801_ParameterInfos[] = 
{
	{"value", 0, 134218348, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandWidth(System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1801_MethodInfo = 
{
	"set_childForceExpandWidth"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1801/* method */
	, &HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t369_HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1801_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandHeight()
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1802_MethodInfo = 
{
	"get_childForceExpandHeight"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1802/* method */
	, &HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t369_HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1803_ParameterInfos[] = 
{
	{"value", 0, 134218349, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandHeight(System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1803_MethodInfo = 
{
	"set_childForceExpandHeight"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1803/* method */
	, &HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t369_HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1803_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t369_HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1804_ParameterInfos[] = 
{
	{"axis", 0, 134218350, 0, &Int32_t127_0_0_0},
	{"isVertical", 1, 134218351, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::CalcAlongAxis(System.Int32,System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1804_MethodInfo = 
{
	"CalcAlongAxis"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1804/* method */
	, &HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_SByte_t170/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t369_HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1804_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo HorizontalOrVerticalLayoutGroup_t369_HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1805_ParameterInfos[] = 
{
	{"axis", 0, 134218352, 0, &Int32_t127_0_0_0},
	{"isVertical", 1, 134218353, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::SetChildrenAlongAxis(System.Int32,System.Boolean)
extern const MethodInfo HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1805_MethodInfo = 
{
	"SetChildrenAlongAxis"/* name */
	, (methodPointerType)&HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1805/* method */
	, &HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127_SByte_t170/* invoker_method */
	, HorizontalOrVerticalLayoutGroup_t369_HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1805_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* HorizontalOrVerticalLayoutGroup_t369_MethodInfos[] =
{
	&HorizontalOrVerticalLayoutGroup__ctor_m1797_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_spacing_m1798_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_spacing_m1799_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1800_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1801_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1802_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1803_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1804_MethodInfo,
	&HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1805_MethodInfo,
	NULL
};
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_spacing_m1798_MethodInfo;
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_spacing_m1799_MethodInfo;
static const PropertyInfo HorizontalOrVerticalLayoutGroup_t369____spacing_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo/* parent */
	, "spacing"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_spacing_m1798_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_spacing_m1799_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1800_MethodInfo;
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1801_MethodInfo;
static const PropertyInfo HorizontalOrVerticalLayoutGroup_t369____childForceExpandWidth_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo/* parent */
	, "childForceExpandWidth"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1800_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1801_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1802_MethodInfo;
extern const MethodInfo HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1803_MethodInfo;
static const PropertyInfo HorizontalOrVerticalLayoutGroup_t369____childForceExpandHeight_PropertyInfo = 
{
	&HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo/* parent */
	, "childForceExpandHeight"/* name */
	, &HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1802_MethodInfo/* get */
	, &HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1803_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* HorizontalOrVerticalLayoutGroup_t369_PropertyInfos[] =
{
	&HorizontalOrVerticalLayoutGroup_t369____spacing_PropertyInfo,
	&HorizontalOrVerticalLayoutGroup_t369____childForceExpandWidth_PropertyInfo,
	&HorizontalOrVerticalLayoutGroup_t369____childForceExpandHeight_PropertyInfo,
	NULL
};
extern const MethodInfo LayoutGroup_CalculateLayoutInputHorizontal_m1837_MethodInfo;
extern const MethodInfo LayoutGroup_CalculateLayoutInputVertical_m2517_MethodInfo;
extern const MethodInfo LayoutGroup_SetLayoutHorizontal_m2518_MethodInfo;
extern const MethodInfo LayoutGroup_SetLayoutVertical_m2519_MethodInfo;
static const Il2CppMethodReference HorizontalOrVerticalLayoutGroup_t369_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&LayoutGroup_OnEnable_m1845_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&LayoutGroup_OnDisable_m1846_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1855_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1847_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1837_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m2517_MethodInfo,
	&LayoutGroup_get_minWidth_m1838_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1839_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1840_MethodInfo,
	&LayoutGroup_get_minHeight_m1841_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1842_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1843_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1844_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m2518_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m2519_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1837_MethodInfo,
	NULL,
	&LayoutGroup_get_minWidth_m1838_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1839_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1840_MethodInfo,
	&LayoutGroup_get_minHeight_m1841_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1842_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1843_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1844_MethodInfo,
	NULL,
	NULL,
	&LayoutGroup_OnTransformChildrenChanged_m1856_MethodInfo,
};
static bool HorizontalOrVerticalLayoutGroup_t369_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair HorizontalOrVerticalLayoutGroup_t369_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t419_0_0_0, 16},
	{ &ILayoutController_t475_0_0_0, 25},
	{ &ILayoutGroup_t473_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType HorizontalOrVerticalLayoutGroup_t369_1_0_0;
struct HorizontalOrVerticalLayoutGroup_t369;
const Il2CppTypeDefinitionMetadata HorizontalOrVerticalLayoutGroup_t369_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, HorizontalOrVerticalLayoutGroup_t369_InterfacesOffsets/* interfaceOffsets */
	, &LayoutGroup_t367_0_0_0/* parent */
	, HorizontalOrVerticalLayoutGroup_t369_VTable/* vtableMethods */
	, HorizontalOrVerticalLayoutGroup_t369_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 578/* fieldStart */

};
TypeInfo HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "HorizontalOrVerticalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, HorizontalOrVerticalLayoutGroup_t369_MethodInfos/* methods */
	, HorizontalOrVerticalLayoutGroup_t369_PropertyInfos/* properties */
	, NULL/* events */
	, &HorizontalOrVerticalLayoutGroup_t369_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &HorizontalOrVerticalLayoutGroup_t369_0_0_0/* byval_arg */
	, &HorizontalOrVerticalLayoutGroup_t369_1_0_0/* this_arg */
	, &HorizontalOrVerticalLayoutGroup_t369_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HorizontalOrVerticalLayoutGroup_t369)/* instance_size */
	, sizeof (HorizontalOrVerticalLayoutGroup_t369)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutElement
extern TypeInfo ILayoutElement_t419_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputHorizontal()
extern const MethodInfo ILayoutElement_CalculateLayoutInputHorizontal_m2505_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, NULL/* method */
	, &ILayoutElement_t419_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutElement::CalculateLayoutInputVertical()
extern const MethodInfo ILayoutElement_CalculateLayoutInputVertical_m2506_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, NULL/* method */
	, &ILayoutElement_t419_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_minWidth()
extern const MethodInfo ILayoutElement_get_minWidth_m2507_MethodInfo = 
{
	"get_minWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t419_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_preferredWidth()
extern const MethodInfo ILayoutElement_get_preferredWidth_m2508_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t419_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_flexibleWidth()
extern const MethodInfo ILayoutElement_get_flexibleWidth_m2509_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, NULL/* method */
	, &ILayoutElement_t419_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_minHeight()
extern const MethodInfo ILayoutElement_get_minHeight_m2510_MethodInfo = 
{
	"get_minHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t419_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_preferredHeight()
extern const MethodInfo ILayoutElement_get_preferredHeight_m2511_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t419_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.ILayoutElement::get_flexibleHeight()
extern const MethodInfo ILayoutElement_get_flexibleHeight_m2512_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, NULL/* method */
	, &ILayoutElement_t419_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.ILayoutElement::get_layoutPriority()
extern const MethodInfo ILayoutElement_get_layoutPriority_m2513_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, NULL/* method */
	, &ILayoutElement_t419_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILayoutElement_t419_MethodInfos[] =
{
	&ILayoutElement_CalculateLayoutInputHorizontal_m2505_MethodInfo,
	&ILayoutElement_CalculateLayoutInputVertical_m2506_MethodInfo,
	&ILayoutElement_get_minWidth_m2507_MethodInfo,
	&ILayoutElement_get_preferredWidth_m2508_MethodInfo,
	&ILayoutElement_get_flexibleWidth_m2509_MethodInfo,
	&ILayoutElement_get_minHeight_m2510_MethodInfo,
	&ILayoutElement_get_preferredHeight_m2511_MethodInfo,
	&ILayoutElement_get_flexibleHeight_m2512_MethodInfo,
	&ILayoutElement_get_layoutPriority_m2513_MethodInfo,
	NULL
};
extern const MethodInfo ILayoutElement_get_minWidth_m2507_MethodInfo;
static const PropertyInfo ILayoutElement_t419____minWidth_PropertyInfo = 
{
	&ILayoutElement_t419_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &ILayoutElement_get_minWidth_m2507_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_preferredWidth_m2508_MethodInfo;
static const PropertyInfo ILayoutElement_t419____preferredWidth_PropertyInfo = 
{
	&ILayoutElement_t419_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &ILayoutElement_get_preferredWidth_m2508_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_flexibleWidth_m2509_MethodInfo;
static const PropertyInfo ILayoutElement_t419____flexibleWidth_PropertyInfo = 
{
	&ILayoutElement_t419_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &ILayoutElement_get_flexibleWidth_m2509_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_minHeight_m2510_MethodInfo;
static const PropertyInfo ILayoutElement_t419____minHeight_PropertyInfo = 
{
	&ILayoutElement_t419_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &ILayoutElement_get_minHeight_m2510_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_preferredHeight_m2511_MethodInfo;
static const PropertyInfo ILayoutElement_t419____preferredHeight_PropertyInfo = 
{
	&ILayoutElement_t419_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &ILayoutElement_get_preferredHeight_m2511_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_flexibleHeight_m2512_MethodInfo;
static const PropertyInfo ILayoutElement_t419____flexibleHeight_PropertyInfo = 
{
	&ILayoutElement_t419_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &ILayoutElement_get_flexibleHeight_m2512_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ILayoutElement_get_layoutPriority_m2513_MethodInfo;
static const PropertyInfo ILayoutElement_t419____layoutPriority_PropertyInfo = 
{
	&ILayoutElement_t419_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &ILayoutElement_get_layoutPriority_m2513_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILayoutElement_t419_PropertyInfos[] =
{
	&ILayoutElement_t419____minWidth_PropertyInfo,
	&ILayoutElement_t419____preferredWidth_PropertyInfo,
	&ILayoutElement_t419____flexibleWidth_PropertyInfo,
	&ILayoutElement_t419____minHeight_PropertyInfo,
	&ILayoutElement_t419____preferredHeight_PropertyInfo,
	&ILayoutElement_t419____flexibleHeight_PropertyInfo,
	&ILayoutElement_t419____layoutPriority_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutElement_t419_1_0_0;
struct ILayoutElement_t419;
const Il2CppTypeDefinitionMetadata ILayoutElement_t419_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutElement_t419_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutElement"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutElement_t419_MethodInfos/* methods */
	, ILayoutElement_t419_PropertyInfos/* properties */
	, NULL/* events */
	, &ILayoutElement_t419_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutElement_t419_0_0_0/* byval_arg */
	, &ILayoutElement_t419_1_0_0/* this_arg */
	, &ILayoutElement_t419_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 7/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutController
extern TypeInfo ILayoutController_t475_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutController::SetLayoutHorizontal()
extern const MethodInfo ILayoutController_SetLayoutHorizontal_m2514_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, NULL/* method */
	, &ILayoutController_t475_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ILayoutController::SetLayoutVertical()
extern const MethodInfo ILayoutController_SetLayoutVertical_m2515_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, NULL/* method */
	, &ILayoutController_t475_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILayoutController_t475_MethodInfos[] =
{
	&ILayoutController_SetLayoutHorizontal_m2514_MethodInfo,
	&ILayoutController_SetLayoutVertical_m2515_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutController_t475_1_0_0;
struct ILayoutController_t475;
const Il2CppTypeDefinitionMetadata ILayoutController_t475_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutController_t475_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutController"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutController_t475_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ILayoutController_t475_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutController_t475_0_0_0/* byval_arg */
	, &ILayoutController_t475_1_0_0/* this_arg */
	, &ILayoutController_t475_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutGroup
extern TypeInfo ILayoutGroup_t473_il2cpp_TypeInfo;
static const MethodInfo* ILayoutGroup_t473_MethodInfos[] =
{
	NULL
};
static const Il2CppType* ILayoutGroup_t473_InterfacesTypeInfos[] = 
{
	&ILayoutController_t475_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutGroup_t473_1_0_0;
struct ILayoutGroup_t473;
const Il2CppTypeDefinitionMetadata ILayoutGroup_t473_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILayoutGroup_t473_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutGroup_t473_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutGroup_t473_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ILayoutGroup_t473_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutGroup_t473_0_0_0/* byval_arg */
	, &ILayoutGroup_t473_1_0_0/* this_arg */
	, &ILayoutGroup_t473_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutSelfController
extern TypeInfo ILayoutSelfController_t476_il2cpp_TypeInfo;
static const MethodInfo* ILayoutSelfController_t476_MethodInfos[] =
{
	NULL
};
static const Il2CppType* ILayoutSelfController_t476_InterfacesTypeInfos[] = 
{
	&ILayoutController_t475_0_0_0,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutSelfController_t476_1_0_0;
struct ILayoutSelfController_t476;
const Il2CppTypeDefinitionMetadata ILayoutSelfController_t476_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, ILayoutSelfController_t476_InterfacesTypeInfos/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutSelfController_t476_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutSelfController"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutSelfController_t476_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ILayoutSelfController_t476_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutSelfController_t476_0_0_0/* byval_arg */
	, &ILayoutSelfController_t476_1_0_0/* this_arg */
	, &ILayoutSelfController_t476_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ILayoutIgnorer
extern TypeInfo ILayoutIgnorer_t472_il2cpp_TypeInfo;
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.ILayoutIgnorer::get_ignoreLayout()
extern const MethodInfo ILayoutIgnorer_get_ignoreLayout_m2516_MethodInfo = 
{
	"get_ignoreLayout"/* name */
	, NULL/* method */
	, &ILayoutIgnorer_t472_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ILayoutIgnorer_t472_MethodInfos[] =
{
	&ILayoutIgnorer_get_ignoreLayout_m2516_MethodInfo,
	NULL
};
extern const MethodInfo ILayoutIgnorer_get_ignoreLayout_m2516_MethodInfo;
static const PropertyInfo ILayoutIgnorer_t472____ignoreLayout_PropertyInfo = 
{
	&ILayoutIgnorer_t472_il2cpp_TypeInfo/* parent */
	, "ignoreLayout"/* name */
	, &ILayoutIgnorer_get_ignoreLayout_m2516_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ILayoutIgnorer_t472_PropertyInfos[] =
{
	&ILayoutIgnorer_t472____ignoreLayout_PropertyInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ILayoutIgnorer_t472_0_0_0;
extern const Il2CppType ILayoutIgnorer_t472_1_0_0;
struct ILayoutIgnorer_t472;
const Il2CppTypeDefinitionMetadata ILayoutIgnorer_t472_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo ILayoutIgnorer_t472_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILayoutIgnorer"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ILayoutIgnorer_t472_MethodInfos/* methods */
	, ILayoutIgnorer_t472_PropertyInfos/* properties */
	, NULL/* events */
	, &ILayoutIgnorer_t472_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ILayoutIgnorer_t472_0_0_0/* byval_arg */
	, &ILayoutIgnorer_t472_1_0_0/* this_arg */
	, &ILayoutIgnorer_t472_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement.h"
// Metadata Definition UnityEngine.UI.LayoutElement
extern TypeInfo LayoutElement_t370_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElementMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::.ctor()
extern const MethodInfo LayoutElement__ctor_m1806_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutElement__ctor_m1806/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutElement::get_ignoreLayout()
extern const MethodInfo LayoutElement_get_ignoreLayout_m1807_MethodInfo = 
{
	"get_ignoreLayout"/* name */
	, (methodPointerType)&LayoutElement_get_ignoreLayout_m1807/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo LayoutElement_t370_LayoutElement_set_ignoreLayout_m1808_ParameterInfos[] = 
{
	{"value", 0, 134218354, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_ignoreLayout(System.Boolean)
extern const MethodInfo LayoutElement_set_ignoreLayout_m1808_MethodInfo = 
{
	"set_ignoreLayout"/* name */
	, (methodPointerType)&LayoutElement_set_ignoreLayout_m1808/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, LayoutElement_t370_LayoutElement_set_ignoreLayout_m1808_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputHorizontal()
extern const MethodInfo LayoutElement_CalculateLayoutInputHorizontal_m1809_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&LayoutElement_CalculateLayoutInputHorizontal_m1809/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputVertical()
extern const MethodInfo LayoutElement_CalculateLayoutInputVertical_m1810_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&LayoutElement_CalculateLayoutInputVertical_m1810/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_minWidth()
extern const MethodInfo LayoutElement_get_minWidth_m1811_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&LayoutElement_get_minWidth_m1811/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo LayoutElement_t370_LayoutElement_set_minWidth_m1812_ParameterInfos[] = 
{
	{"value", 0, 134218355, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_minWidth(System.Single)
extern const MethodInfo LayoutElement_set_minWidth_m1812_MethodInfo = 
{
	"set_minWidth"/* name */
	, (methodPointerType)&LayoutElement_set_minWidth_m1812/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, LayoutElement_t370_LayoutElement_set_minWidth_m1812_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_minHeight()
extern const MethodInfo LayoutElement_get_minHeight_m1813_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&LayoutElement_get_minHeight_m1813/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo LayoutElement_t370_LayoutElement_set_minHeight_m1814_ParameterInfos[] = 
{
	{"value", 0, 134218356, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_minHeight(System.Single)
extern const MethodInfo LayoutElement_set_minHeight_m1814_MethodInfo = 
{
	"set_minHeight"/* name */
	, (methodPointerType)&LayoutElement_set_minHeight_m1814/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, LayoutElement_t370_LayoutElement_set_minHeight_m1814_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_preferredWidth()
extern const MethodInfo LayoutElement_get_preferredWidth_m1815_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&LayoutElement_get_preferredWidth_m1815/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo LayoutElement_t370_LayoutElement_set_preferredWidth_m1816_ParameterInfos[] = 
{
	{"value", 0, 134218357, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_preferredWidth(System.Single)
extern const MethodInfo LayoutElement_set_preferredWidth_m1816_MethodInfo = 
{
	"set_preferredWidth"/* name */
	, (methodPointerType)&LayoutElement_set_preferredWidth_m1816/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, LayoutElement_t370_LayoutElement_set_preferredWidth_m1816_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_preferredHeight()
extern const MethodInfo LayoutElement_get_preferredHeight_m1817_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&LayoutElement_get_preferredHeight_m1817/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo LayoutElement_t370_LayoutElement_set_preferredHeight_m1818_ParameterInfos[] = 
{
	{"value", 0, 134218358, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_preferredHeight(System.Single)
extern const MethodInfo LayoutElement_set_preferredHeight_m1818_MethodInfo = 
{
	"set_preferredHeight"/* name */
	, (methodPointerType)&LayoutElement_set_preferredHeight_m1818/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, LayoutElement_t370_LayoutElement_set_preferredHeight_m1818_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_flexibleWidth()
extern const MethodInfo LayoutElement_get_flexibleWidth_m1819_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&LayoutElement_get_flexibleWidth_m1819/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 38/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo LayoutElement_t370_LayoutElement_set_flexibleWidth_m1820_ParameterInfos[] = 
{
	{"value", 0, 134218359, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_flexibleWidth(System.Single)
extern const MethodInfo LayoutElement_set_flexibleWidth_m1820_MethodInfo = 
{
	"set_flexibleWidth"/* name */
	, (methodPointerType)&LayoutElement_set_flexibleWidth_m1820/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, LayoutElement_t370_LayoutElement_set_flexibleWidth_m1820_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 39/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutElement::get_flexibleHeight()
extern const MethodInfo LayoutElement_get_flexibleHeight_m1821_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&LayoutElement_get_flexibleHeight_m1821/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 40/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo LayoutElement_t370_LayoutElement_set_flexibleHeight_m1822_ParameterInfos[] = 
{
	{"value", 0, 134218360, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::set_flexibleHeight(System.Single)
extern const MethodInfo LayoutElement_set_flexibleHeight_m1822_MethodInfo = 
{
	"set_flexibleHeight"/* name */
	, (methodPointerType)&LayoutElement_set_flexibleHeight_m1822/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151/* invoker_method */
	, LayoutElement_t370_LayoutElement_set_flexibleHeight_m1822_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 41/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutElement::get_layoutPriority()
extern const MethodInfo LayoutElement_get_layoutPriority_m1823_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&LayoutElement_get_layoutPriority_m1823/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 42/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnEnable()
extern const MethodInfo LayoutElement_OnEnable_m1824_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&LayoutElement_OnEnable_m1824/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnTransformParentChanged()
extern const MethodInfo LayoutElement_OnTransformParentChanged_m1825_MethodInfo = 
{
	"OnTransformParentChanged"/* name */
	, (methodPointerType)&LayoutElement_OnTransformParentChanged_m1825/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnDisable()
extern const MethodInfo LayoutElement_OnDisable_m1826_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&LayoutElement_OnDisable_m1826/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnDidApplyAnimationProperties()
extern const MethodInfo LayoutElement_OnDidApplyAnimationProperties_m1827_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&LayoutElement_OnDidApplyAnimationProperties_m1827/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::OnBeforeTransformParentChanged()
extern const MethodInfo LayoutElement_OnBeforeTransformParentChanged_m1828_MethodInfo = 
{
	"OnBeforeTransformParentChanged"/* name */
	, (methodPointerType)&LayoutElement_OnBeforeTransformParentChanged_m1828/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutElement::SetDirty()
extern const MethodInfo LayoutElement_SetDirty_m1829_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&LayoutElement_SetDirty_m1829/* method */
	, &LayoutElement_t370_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutElement_t370_MethodInfos[] =
{
	&LayoutElement__ctor_m1806_MethodInfo,
	&LayoutElement_get_ignoreLayout_m1807_MethodInfo,
	&LayoutElement_set_ignoreLayout_m1808_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m1809_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m1810_MethodInfo,
	&LayoutElement_get_minWidth_m1811_MethodInfo,
	&LayoutElement_set_minWidth_m1812_MethodInfo,
	&LayoutElement_get_minHeight_m1813_MethodInfo,
	&LayoutElement_set_minHeight_m1814_MethodInfo,
	&LayoutElement_get_preferredWidth_m1815_MethodInfo,
	&LayoutElement_set_preferredWidth_m1816_MethodInfo,
	&LayoutElement_get_preferredHeight_m1817_MethodInfo,
	&LayoutElement_set_preferredHeight_m1818_MethodInfo,
	&LayoutElement_get_flexibleWidth_m1819_MethodInfo,
	&LayoutElement_set_flexibleWidth_m1820_MethodInfo,
	&LayoutElement_get_flexibleHeight_m1821_MethodInfo,
	&LayoutElement_set_flexibleHeight_m1822_MethodInfo,
	&LayoutElement_get_layoutPriority_m1823_MethodInfo,
	&LayoutElement_OnEnable_m1824_MethodInfo,
	&LayoutElement_OnTransformParentChanged_m1825_MethodInfo,
	&LayoutElement_OnDisable_m1826_MethodInfo,
	&LayoutElement_OnDidApplyAnimationProperties_m1827_MethodInfo,
	&LayoutElement_OnBeforeTransformParentChanged_m1828_MethodInfo,
	&LayoutElement_SetDirty_m1829_MethodInfo,
	NULL
};
extern const MethodInfo LayoutElement_get_ignoreLayout_m1807_MethodInfo;
extern const MethodInfo LayoutElement_set_ignoreLayout_m1808_MethodInfo;
static const PropertyInfo LayoutElement_t370____ignoreLayout_PropertyInfo = 
{
	&LayoutElement_t370_il2cpp_TypeInfo/* parent */
	, "ignoreLayout"/* name */
	, &LayoutElement_get_ignoreLayout_m1807_MethodInfo/* get */
	, &LayoutElement_set_ignoreLayout_m1808_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_minWidth_m1811_MethodInfo;
extern const MethodInfo LayoutElement_set_minWidth_m1812_MethodInfo;
static const PropertyInfo LayoutElement_t370____minWidth_PropertyInfo = 
{
	&LayoutElement_t370_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &LayoutElement_get_minWidth_m1811_MethodInfo/* get */
	, &LayoutElement_set_minWidth_m1812_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_minHeight_m1813_MethodInfo;
extern const MethodInfo LayoutElement_set_minHeight_m1814_MethodInfo;
static const PropertyInfo LayoutElement_t370____minHeight_PropertyInfo = 
{
	&LayoutElement_t370_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &LayoutElement_get_minHeight_m1813_MethodInfo/* get */
	, &LayoutElement_set_minHeight_m1814_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_preferredWidth_m1815_MethodInfo;
extern const MethodInfo LayoutElement_set_preferredWidth_m1816_MethodInfo;
static const PropertyInfo LayoutElement_t370____preferredWidth_PropertyInfo = 
{
	&LayoutElement_t370_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &LayoutElement_get_preferredWidth_m1815_MethodInfo/* get */
	, &LayoutElement_set_preferredWidth_m1816_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_preferredHeight_m1817_MethodInfo;
extern const MethodInfo LayoutElement_set_preferredHeight_m1818_MethodInfo;
static const PropertyInfo LayoutElement_t370____preferredHeight_PropertyInfo = 
{
	&LayoutElement_t370_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &LayoutElement_get_preferredHeight_m1817_MethodInfo/* get */
	, &LayoutElement_set_preferredHeight_m1818_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_flexibleWidth_m1819_MethodInfo;
extern const MethodInfo LayoutElement_set_flexibleWidth_m1820_MethodInfo;
static const PropertyInfo LayoutElement_t370____flexibleWidth_PropertyInfo = 
{
	&LayoutElement_t370_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &LayoutElement_get_flexibleWidth_m1819_MethodInfo/* get */
	, &LayoutElement_set_flexibleWidth_m1820_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_flexibleHeight_m1821_MethodInfo;
extern const MethodInfo LayoutElement_set_flexibleHeight_m1822_MethodInfo;
static const PropertyInfo LayoutElement_t370____flexibleHeight_PropertyInfo = 
{
	&LayoutElement_t370_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &LayoutElement_get_flexibleHeight_m1821_MethodInfo/* get */
	, &LayoutElement_set_flexibleHeight_m1822_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutElement_get_layoutPriority_m1823_MethodInfo;
static const PropertyInfo LayoutElement_t370____layoutPriority_PropertyInfo = 
{
	&LayoutElement_t370_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &LayoutElement_get_layoutPriority_m1823_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LayoutElement_t370_PropertyInfos[] =
{
	&LayoutElement_t370____ignoreLayout_PropertyInfo,
	&LayoutElement_t370____minWidth_PropertyInfo,
	&LayoutElement_t370____minHeight_PropertyInfo,
	&LayoutElement_t370____preferredWidth_PropertyInfo,
	&LayoutElement_t370____preferredHeight_PropertyInfo,
	&LayoutElement_t370____flexibleWidth_PropertyInfo,
	&LayoutElement_t370____flexibleHeight_PropertyInfo,
	&LayoutElement_t370____layoutPriority_PropertyInfo,
	NULL
};
extern const MethodInfo LayoutElement_OnEnable_m1824_MethodInfo;
extern const MethodInfo LayoutElement_OnDisable_m1826_MethodInfo;
extern const MethodInfo LayoutElement_OnBeforeTransformParentChanged_m1828_MethodInfo;
extern const MethodInfo LayoutElement_OnTransformParentChanged_m1825_MethodInfo;
extern const MethodInfo LayoutElement_OnDidApplyAnimationProperties_m1827_MethodInfo;
extern const MethodInfo LayoutElement_CalculateLayoutInputHorizontal_m1809_MethodInfo;
extern const MethodInfo LayoutElement_CalculateLayoutInputVertical_m1810_MethodInfo;
static const Il2CppMethodReference LayoutElement_t370_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&LayoutElement_OnEnable_m1824_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&LayoutElement_OnDisable_m1826_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m858_MethodInfo,
	&LayoutElement_OnBeforeTransformParentChanged_m1828_MethodInfo,
	&LayoutElement_OnTransformParentChanged_m1825_MethodInfo,
	&LayoutElement_OnDidApplyAnimationProperties_m1827_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m1809_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m1810_MethodInfo,
	&LayoutElement_get_minWidth_m1811_MethodInfo,
	&LayoutElement_get_preferredWidth_m1815_MethodInfo,
	&LayoutElement_get_flexibleWidth_m1819_MethodInfo,
	&LayoutElement_get_minHeight_m1813_MethodInfo,
	&LayoutElement_get_preferredHeight_m1817_MethodInfo,
	&LayoutElement_get_flexibleHeight_m1821_MethodInfo,
	&LayoutElement_get_layoutPriority_m1823_MethodInfo,
	&LayoutElement_get_ignoreLayout_m1807_MethodInfo,
	&LayoutElement_get_ignoreLayout_m1807_MethodInfo,
	&LayoutElement_set_ignoreLayout_m1808_MethodInfo,
	&LayoutElement_CalculateLayoutInputHorizontal_m1809_MethodInfo,
	&LayoutElement_CalculateLayoutInputVertical_m1810_MethodInfo,
	&LayoutElement_get_minWidth_m1811_MethodInfo,
	&LayoutElement_set_minWidth_m1812_MethodInfo,
	&LayoutElement_get_minHeight_m1813_MethodInfo,
	&LayoutElement_set_minHeight_m1814_MethodInfo,
	&LayoutElement_get_preferredWidth_m1815_MethodInfo,
	&LayoutElement_set_preferredWidth_m1816_MethodInfo,
	&LayoutElement_get_preferredHeight_m1817_MethodInfo,
	&LayoutElement_set_preferredHeight_m1818_MethodInfo,
	&LayoutElement_get_flexibleWidth_m1819_MethodInfo,
	&LayoutElement_set_flexibleWidth_m1820_MethodInfo,
	&LayoutElement_get_flexibleHeight_m1821_MethodInfo,
	&LayoutElement_set_flexibleHeight_m1822_MethodInfo,
	&LayoutElement_get_layoutPriority_m1823_MethodInfo,
};
static bool LayoutElement_t370_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* LayoutElement_t370_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t419_0_0_0,
	&ILayoutIgnorer_t472_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutElement_t370_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t419_0_0_0, 16},
	{ &ILayoutIgnorer_t472_0_0_0, 25},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutElement_t370_0_0_0;
extern const Il2CppType LayoutElement_t370_1_0_0;
struct LayoutElement_t370;
const Il2CppTypeDefinitionMetadata LayoutElement_t370_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutElement_t370_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutElement_t370_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t199_0_0_0/* parent */
	, LayoutElement_t370_VTable/* vtableMethods */
	, LayoutElement_t370_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 581/* fieldStart */

};
TypeInfo LayoutElement_t370_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutElement"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutElement_t370_MethodInfos/* methods */
	, LayoutElement_t370_PropertyInfos/* properties */
	, NULL/* events */
	, &LayoutElement_t370_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 295/* custom_attributes_cache */
	, &LayoutElement_t370_0_0_0/* byval_arg */
	, &LayoutElement_t370_1_0_0/* this_arg */
	, &LayoutElement_t370_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutElement_t370)/* instance_size */
	, sizeof (LayoutElement_t370)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 24/* method_count */
	, 8/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 43/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup.h"
// Metadata Definition UnityEngine.UI.LayoutGroup
extern TypeInfo LayoutGroup_t367_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::.ctor()
extern const MethodInfo LayoutGroup__ctor_m1830_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutGroup__ctor_m1830/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectOffset_t371_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::get_padding()
extern const MethodInfo LayoutGroup_get_padding_m1831_MethodInfo = 
{
	"get_padding"/* name */
	, (methodPointerType)&LayoutGroup_get_padding_m1831/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &RectOffset_t371_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectOffset_t371_0_0_0;
static const ParameterInfo LayoutGroup_t367_LayoutGroup_set_padding_m1832_ParameterInfos[] = 
{
	{"value", 0, 134218361, 0, &RectOffset_t371_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::set_padding(UnityEngine.RectOffset)
extern const MethodInfo LayoutGroup_set_padding_m1832_MethodInfo = 
{
	"set_padding"/* name */
	, (methodPointerType)&LayoutGroup_set_padding_m1832/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LayoutGroup_t367_LayoutGroup_set_padding_m1832_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_TextAnchor_t471 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::get_childAlignment()
extern const MethodInfo LayoutGroup_get_childAlignment_m1833_MethodInfo = 
{
	"get_childAlignment"/* name */
	, (methodPointerType)&LayoutGroup_get_childAlignment_m1833/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &TextAnchor_t471_0_0_0/* return_type */
	, RuntimeInvoker_TextAnchor_t471/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TextAnchor_t471_0_0_0;
static const ParameterInfo LayoutGroup_t367_LayoutGroup_set_childAlignment_m1834_ParameterInfos[] = 
{
	{"value", 0, 134218362, 0, &TextAnchor_t471_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::set_childAlignment(UnityEngine.TextAnchor)
extern const MethodInfo LayoutGroup_set_childAlignment_m1834_MethodInfo = 
{
	"set_childAlignment"/* name */
	, (methodPointerType)&LayoutGroup_set_childAlignment_m1834/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, LayoutGroup_t367_LayoutGroup_set_childAlignment_m1834_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::get_rectTransform()
extern const MethodInfo LayoutGroup_get_rectTransform_m1835_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&LayoutGroup_get_rectTransform_m1835/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t272_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t372_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::get_rectChildren()
extern const MethodInfo LayoutGroup_get_rectChildren_m1836_MethodInfo = 
{
	"get_rectChildren"/* name */
	, (methodPointerType)&LayoutGroup_get_rectChildren_m1836/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t372_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo LayoutGroup_CalculateLayoutInputHorizontal_m1837_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&LayoutGroup_CalculateLayoutInputHorizontal_m1837/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo LayoutGroup_CalculateLayoutInputVertical_m2517_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, NULL/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_minWidth()
extern const MethodInfo LayoutGroup_get_minWidth_m1838_MethodInfo = 
{
	"get_minWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_minWidth_m1838/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_preferredWidth()
extern const MethodInfo LayoutGroup_get_preferredWidth_m1839_MethodInfo = 
{
	"get_preferredWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_preferredWidth_m1839/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleWidth()
extern const MethodInfo LayoutGroup_get_flexibleWidth_m1840_MethodInfo = 
{
	"get_flexibleWidth"/* name */
	, (methodPointerType)&LayoutGroup_get_flexibleWidth_m1840/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_minHeight()
extern const MethodInfo LayoutGroup_get_minHeight_m1841_MethodInfo = 
{
	"get_minHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_minHeight_m1841/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_preferredHeight()
extern const MethodInfo LayoutGroup_get_preferredHeight_m1842_MethodInfo = 
{
	"get_preferredHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_preferredHeight_m1842/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 33/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::get_flexibleHeight()
extern const MethodInfo LayoutGroup_get_flexibleHeight_m1843_MethodInfo = 
{
	"get_flexibleHeight"/* name */
	, (methodPointerType)&LayoutGroup_get_flexibleHeight_m1843/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 34/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutGroup::get_layoutPriority()
extern const MethodInfo LayoutGroup_get_layoutPriority_m1844_MethodInfo = 
{
	"get_layoutPriority"/* name */
	, (methodPointerType)&LayoutGroup_get_layoutPriority_m1844/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2502/* flags */
	, 0/* iflags */
	, 35/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutHorizontal()
extern const MethodInfo LayoutGroup_SetLayoutHorizontal_m2518_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, NULL/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutVertical()
extern const MethodInfo LayoutGroup_SetLayoutVertical_m2519_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, NULL/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnEnable()
extern const MethodInfo LayoutGroup_OnEnable_m1845_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&LayoutGroup_OnEnable_m1845/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnDisable()
extern const MethodInfo LayoutGroup_OnDisable_m1846_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&LayoutGroup_OnDisable_m1846/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnDidApplyAnimationProperties()
extern const MethodInfo LayoutGroup_OnDidApplyAnimationProperties_m1847_MethodInfo = 
{
	"OnDidApplyAnimationProperties"/* name */
	, (methodPointerType)&LayoutGroup_OnDidApplyAnimationProperties_m1847/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo LayoutGroup_t367_LayoutGroup_GetTotalMinSize_m1848_ParameterInfos[] = 
{
	{"axis", 0, 134218363, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalMinSize(System.Int32)
extern const MethodInfo LayoutGroup_GetTotalMinSize_m1848_MethodInfo = 
{
	"GetTotalMinSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalMinSize_m1848/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Int32_t127/* invoker_method */
	, LayoutGroup_t367_LayoutGroup_GetTotalMinSize_m1848_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo LayoutGroup_t367_LayoutGroup_GetTotalPreferredSize_m1849_ParameterInfos[] = 
{
	{"axis", 0, 134218364, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalPreferredSize(System.Int32)
extern const MethodInfo LayoutGroup_GetTotalPreferredSize_m1849_MethodInfo = 
{
	"GetTotalPreferredSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalPreferredSize_m1849/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Int32_t127/* invoker_method */
	, LayoutGroup_t367_LayoutGroup_GetTotalPreferredSize_m1849_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo LayoutGroup_t367_LayoutGroup_GetTotalFlexibleSize_m1850_ParameterInfos[] = 
{
	{"axis", 0, 134218365, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetTotalFlexibleSize(System.Int32)
extern const MethodInfo LayoutGroup_GetTotalFlexibleSize_m1850_MethodInfo = 
{
	"GetTotalFlexibleSize"/* name */
	, (methodPointerType)&LayoutGroup_GetTotalFlexibleSize_m1850/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Int32_t127/* invoker_method */
	, LayoutGroup_t367_LayoutGroup_GetTotalFlexibleSize_m1850_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo LayoutGroup_t367_LayoutGroup_GetStartOffset_m1851_ParameterInfos[] = 
{
	{"axis", 0, 134218366, 0, &Int32_t127_0_0_0},
	{"requiredSpaceWithoutPadding", 1, 134218367, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Int32_t127_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutGroup::GetStartOffset(System.Int32,System.Single)
extern const MethodInfo LayoutGroup_GetStartOffset_m1851_MethodInfo = 
{
	"GetStartOffset"/* name */
	, (methodPointerType)&LayoutGroup_GetStartOffset_m1851/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Int32_t127_Single_t151/* invoker_method */
	, LayoutGroup_t367_LayoutGroup_GetStartOffset_m1851_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo LayoutGroup_t367_LayoutGroup_SetLayoutInputForAxis_m1852_ParameterInfos[] = 
{
	{"totalMin", 0, 134218368, 0, &Single_t151_0_0_0},
	{"totalPreferred", 1, 134218369, 0, &Single_t151_0_0_0},
	{"totalFlexible", 2, 134218370, 0, &Single_t151_0_0_0},
	{"axis", 3, 134218371, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Single_t151_Single_t151_Single_t151_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetLayoutInputForAxis(System.Single,System.Single,System.Single,System.Int32)
extern const MethodInfo LayoutGroup_SetLayoutInputForAxis_m1852_MethodInfo = 
{
	"SetLayoutInputForAxis"/* name */
	, (methodPointerType)&LayoutGroup_SetLayoutInputForAxis_m1852/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Single_t151_Single_t151_Single_t151_Int32_t127/* invoker_method */
	, LayoutGroup_t367_LayoutGroup_SetLayoutInputForAxis_m1852_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo LayoutGroup_t367_LayoutGroup_SetChildAlongAxis_m1853_ParameterInfos[] = 
{
	{"rect", 0, 134218372, 0, &RectTransform_t272_0_0_0},
	{"axis", 1, 134218373, 0, &Int32_t127_0_0_0},
	{"pos", 2, 134218374, 0, &Single_t151_0_0_0},
	{"size", 3, 134218375, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Int32_t127_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetChildAlongAxis(UnityEngine.RectTransform,System.Int32,System.Single,System.Single)
extern const MethodInfo LayoutGroup_SetChildAlongAxis_m1853_MethodInfo = 
{
	"SetChildAlongAxis"/* name */
	, (methodPointerType)&LayoutGroup_SetChildAlongAxis_m1853/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Int32_t127_Single_t151_Single_t151/* invoker_method */
	, LayoutGroup_t367_LayoutGroup_SetChildAlongAxis_m1853_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutGroup::get_isRootLayoutGroup()
extern const MethodInfo LayoutGroup_get_isRootLayoutGroup_m1854_MethodInfo = 
{
	"get_isRootLayoutGroup"/* name */
	, (methodPointerType)&LayoutGroup_get_isRootLayoutGroup_m1854/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnRectTransformDimensionsChange()
extern const MethodInfo LayoutGroup_OnRectTransformDimensionsChange_m1855_MethodInfo = 
{
	"OnRectTransformDimensionsChange"/* name */
	, (methodPointerType)&LayoutGroup_OnRectTransformDimensionsChange_m1855/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::OnTransformChildrenChanged()
extern const MethodInfo LayoutGroup_OnTransformChildrenChanged_m1856_MethodInfo = 
{
	"OnTransformChildrenChanged"/* name */
	, (methodPointerType)&LayoutGroup_OnTransformChildrenChanged_m1856/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 452/* flags */
	, 0/* iflags */
	, 38/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LayoutGroup_SetProperty_m2520_gp_0_1_0_0;
extern const Il2CppType LayoutGroup_SetProperty_m2520_gp_0_1_0_0;
extern const Il2CppType LayoutGroup_SetProperty_m2520_gp_0_0_0_0;
extern const Il2CppType LayoutGroup_SetProperty_m2520_gp_0_0_0_0;
static const ParameterInfo LayoutGroup_t367_LayoutGroup_SetProperty_m2520_ParameterInfos[] = 
{
	{"currentValue", 0, 134218376, 0, &LayoutGroup_SetProperty_m2520_gp_0_1_0_0},
	{"newValue", 1, 134218377, 0, &LayoutGroup_SetProperty_m2520_gp_0_0_0_0},
};
extern const Il2CppGenericContainer LayoutGroup_SetProperty_m2520_Il2CppGenericContainer;
extern TypeInfo LayoutGroup_SetProperty_m2520_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter LayoutGroup_SetProperty_m2520_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &LayoutGroup_SetProperty_m2520_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* LayoutGroup_SetProperty_m2520_Il2CppGenericParametersArray[1] = 
{
	&LayoutGroup_SetProperty_m2520_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const MethodInfo LayoutGroup_SetProperty_m2520_MethodInfo;
extern const Il2CppGenericContainer LayoutGroup_SetProperty_m2520_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&LayoutGroup_SetProperty_m2520_MethodInfo, 1, 1, LayoutGroup_SetProperty_m2520_Il2CppGenericParametersArray };
static Il2CppRGCTXDefinition LayoutGroup_SetProperty_m2520_RGCTXData[2] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&LayoutGroup_SetProperty_m2520_gp_0_0_0_0 }/* Class */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void UnityEngine.UI.LayoutGroup::SetProperty(T&,T)
extern const MethodInfo LayoutGroup_SetProperty_m2520_MethodInfo = 
{
	"SetProperty"/* name */
	, NULL/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, LayoutGroup_t367_LayoutGroup_SetProperty_m2520_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 1173/* token */
	, LayoutGroup_SetProperty_m2520_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LayoutGroup_SetProperty_m2520_Il2CppGenericContainer/* genericContainer */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutGroup::SetDirty()
extern const MethodInfo LayoutGroup_SetDirty_m1857_MethodInfo = 
{
	"SetDirty"/* name */
	, (methodPointerType)&LayoutGroup_SetDirty_m1857/* method */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutGroup_t367_MethodInfos[] =
{
	&LayoutGroup__ctor_m1830_MethodInfo,
	&LayoutGroup_get_padding_m1831_MethodInfo,
	&LayoutGroup_set_padding_m1832_MethodInfo,
	&LayoutGroup_get_childAlignment_m1833_MethodInfo,
	&LayoutGroup_set_childAlignment_m1834_MethodInfo,
	&LayoutGroup_get_rectTransform_m1835_MethodInfo,
	&LayoutGroup_get_rectChildren_m1836_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1837_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m2517_MethodInfo,
	&LayoutGroup_get_minWidth_m1838_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1839_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1840_MethodInfo,
	&LayoutGroup_get_minHeight_m1841_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1842_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1843_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1844_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m2518_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m2519_MethodInfo,
	&LayoutGroup_OnEnable_m1845_MethodInfo,
	&LayoutGroup_OnDisable_m1846_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1847_MethodInfo,
	&LayoutGroup_GetTotalMinSize_m1848_MethodInfo,
	&LayoutGroup_GetTotalPreferredSize_m1849_MethodInfo,
	&LayoutGroup_GetTotalFlexibleSize_m1850_MethodInfo,
	&LayoutGroup_GetStartOffset_m1851_MethodInfo,
	&LayoutGroup_SetLayoutInputForAxis_m1852_MethodInfo,
	&LayoutGroup_SetChildAlongAxis_m1853_MethodInfo,
	&LayoutGroup_get_isRootLayoutGroup_m1854_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1855_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m1856_MethodInfo,
	&LayoutGroup_SetProperty_m2520_MethodInfo,
	&LayoutGroup_SetDirty_m1857_MethodInfo,
	NULL
};
extern const MethodInfo LayoutGroup_get_padding_m1831_MethodInfo;
extern const MethodInfo LayoutGroup_set_padding_m1832_MethodInfo;
static const PropertyInfo LayoutGroup_t367____padding_PropertyInfo = 
{
	&LayoutGroup_t367_il2cpp_TypeInfo/* parent */
	, "padding"/* name */
	, &LayoutGroup_get_padding_m1831_MethodInfo/* get */
	, &LayoutGroup_set_padding_m1832_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_childAlignment_m1833_MethodInfo;
extern const MethodInfo LayoutGroup_set_childAlignment_m1834_MethodInfo;
static const PropertyInfo LayoutGroup_t367____childAlignment_PropertyInfo = 
{
	&LayoutGroup_t367_il2cpp_TypeInfo/* parent */
	, "childAlignment"/* name */
	, &LayoutGroup_get_childAlignment_m1833_MethodInfo/* get */
	, &LayoutGroup_set_childAlignment_m1834_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_rectTransform_m1835_MethodInfo;
static const PropertyInfo LayoutGroup_t367____rectTransform_PropertyInfo = 
{
	&LayoutGroup_t367_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &LayoutGroup_get_rectTransform_m1835_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_rectChildren_m1836_MethodInfo;
static const PropertyInfo LayoutGroup_t367____rectChildren_PropertyInfo = 
{
	&LayoutGroup_t367_il2cpp_TypeInfo/* parent */
	, "rectChildren"/* name */
	, &LayoutGroup_get_rectChildren_m1836_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t367____minWidth_PropertyInfo = 
{
	&LayoutGroup_t367_il2cpp_TypeInfo/* parent */
	, "minWidth"/* name */
	, &LayoutGroup_get_minWidth_m1838_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t367____preferredWidth_PropertyInfo = 
{
	&LayoutGroup_t367_il2cpp_TypeInfo/* parent */
	, "preferredWidth"/* name */
	, &LayoutGroup_get_preferredWidth_m1839_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t367____flexibleWidth_PropertyInfo = 
{
	&LayoutGroup_t367_il2cpp_TypeInfo/* parent */
	, "flexibleWidth"/* name */
	, &LayoutGroup_get_flexibleWidth_m1840_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t367____minHeight_PropertyInfo = 
{
	&LayoutGroup_t367_il2cpp_TypeInfo/* parent */
	, "minHeight"/* name */
	, &LayoutGroup_get_minHeight_m1841_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t367____preferredHeight_PropertyInfo = 
{
	&LayoutGroup_t367_il2cpp_TypeInfo/* parent */
	, "preferredHeight"/* name */
	, &LayoutGroup_get_preferredHeight_m1842_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t367____flexibleHeight_PropertyInfo = 
{
	&LayoutGroup_t367_il2cpp_TypeInfo/* parent */
	, "flexibleHeight"/* name */
	, &LayoutGroup_get_flexibleHeight_m1843_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo LayoutGroup_t367____layoutPriority_PropertyInfo = 
{
	&LayoutGroup_t367_il2cpp_TypeInfo/* parent */
	, "layoutPriority"/* name */
	, &LayoutGroup_get_layoutPriority_m1844_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo LayoutGroup_get_isRootLayoutGroup_m1854_MethodInfo;
static const PropertyInfo LayoutGroup_t367____isRootLayoutGroup_PropertyInfo = 
{
	&LayoutGroup_t367_il2cpp_TypeInfo/* parent */
	, "isRootLayoutGroup"/* name */
	, &LayoutGroup_get_isRootLayoutGroup_m1854_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LayoutGroup_t367_PropertyInfos[] =
{
	&LayoutGroup_t367____padding_PropertyInfo,
	&LayoutGroup_t367____childAlignment_PropertyInfo,
	&LayoutGroup_t367____rectTransform_PropertyInfo,
	&LayoutGroup_t367____rectChildren_PropertyInfo,
	&LayoutGroup_t367____minWidth_PropertyInfo,
	&LayoutGroup_t367____preferredWidth_PropertyInfo,
	&LayoutGroup_t367____flexibleWidth_PropertyInfo,
	&LayoutGroup_t367____minHeight_PropertyInfo,
	&LayoutGroup_t367____preferredHeight_PropertyInfo,
	&LayoutGroup_t367____flexibleHeight_PropertyInfo,
	&LayoutGroup_t367____layoutPriority_PropertyInfo,
	&LayoutGroup_t367____isRootLayoutGroup_PropertyInfo,
	NULL
};
static const Il2CppMethodReference LayoutGroup_t367_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&LayoutGroup_OnEnable_m1845_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&LayoutGroup_OnDisable_m1846_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1855_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1847_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1837_MethodInfo,
	&LayoutGroup_CalculateLayoutInputVertical_m2517_MethodInfo,
	&LayoutGroup_get_minWidth_m1838_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1839_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1840_MethodInfo,
	&LayoutGroup_get_minHeight_m1841_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1842_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1843_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1844_MethodInfo,
	&LayoutGroup_SetLayoutHorizontal_m2518_MethodInfo,
	&LayoutGroup_SetLayoutVertical_m2519_MethodInfo,
	&LayoutGroup_CalculateLayoutInputHorizontal_m1837_MethodInfo,
	NULL,
	&LayoutGroup_get_minWidth_m1838_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1839_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1840_MethodInfo,
	&LayoutGroup_get_minHeight_m1841_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1842_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1843_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1844_MethodInfo,
	NULL,
	NULL,
	&LayoutGroup_OnTransformChildrenChanged_m1856_MethodInfo,
};
static bool LayoutGroup_t367_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static const Il2CppType* LayoutGroup_t367_InterfacesTypeInfos[] = 
{
	&ILayoutElement_t419_0_0_0,
	&ILayoutController_t475_0_0_0,
	&ILayoutGroup_t473_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutGroup_t367_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t419_0_0_0, 16},
	{ &ILayoutController_t475_0_0_0, 25},
	{ &ILayoutGroup_t473_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutGroup_t367_1_0_0;
struct LayoutGroup_t367;
const Il2CppTypeDefinitionMetadata LayoutGroup_t367_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutGroup_t367_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutGroup_t367_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t199_0_0_0/* parent */
	, LayoutGroup_t367_VTable/* vtableMethods */
	, LayoutGroup_t367_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 588/* fieldStart */

};
TypeInfo LayoutGroup_t367_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutGroup_t367_MethodInfos/* methods */
	, LayoutGroup_t367_PropertyInfos/* properties */
	, NULL/* events */
	, &LayoutGroup_t367_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 303/* custom_attributes_cache */
	, &LayoutGroup_t367_0_0_0/* byval_arg */
	, &LayoutGroup_t367_1_0_0/* this_arg */
	, &LayoutGroup_t367_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutGroup_t367)/* instance_size */
	, sizeof (LayoutGroup_t367)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 32/* method_count */
	, 12/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
// Metadata Definition UnityEngine.UI.LayoutRebuilder
extern TypeInfo LayoutRebuilder_t375_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilderMethodDeclarations.h"
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder__ctor_m1858_ParameterInfos[] = 
{
	{"controller", 0, 134218378, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::.ctor(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder__ctor_m1858_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LayoutRebuilder__ctor_m1858/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder__ctor_m1858_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6273/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::.cctor()
extern const MethodInfo LayoutRebuilder__cctor_m1859_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&LayoutRebuilder__cctor_m1859/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType CanvasUpdate_t260_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1860_ParameterInfos[] = 
{
	{"executing", 0, 134218379, 0, &CanvasUpdate_t260_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::UnityEngine.UI.ICanvasElement.Rebuild(UnityEngine.UI.CanvasUpdate)
extern const MethodInfo LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1860_MethodInfo = 
{
	"UnityEngine.UI.ICanvasElement.Rebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1860/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Int32_t127/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1860_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_ReapplyDrivenProperties_m1861_ParameterInfos[] = 
{
	{"driven", 0, 134218380, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::ReapplyDrivenProperties(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_ReapplyDrivenProperties_m1861_MethodInfo = 
{
	"ReapplyDrivenProperties"/* name */
	, (methodPointerType)&LayoutRebuilder_ReapplyDrivenProperties_m1861/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_ReapplyDrivenProperties_m1861_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform UnityEngine.UI.LayoutRebuilder::get_transform()
extern const MethodInfo LayoutRebuilder_get_transform_m1862_MethodInfo = 
{
	"get_transform"/* name */
	, (methodPointerType)&LayoutRebuilder_get_transform_m1862/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::IsDestroyed()
extern const MethodInfo LayoutRebuilder_IsDestroyed_m1863_MethodInfo = 
{
	"IsDestroyed"/* name */
	, (methodPointerType)&LayoutRebuilder_IsDestroyed_m1863/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t418_0_0_0;
extern const Il2CppType List_1_t418_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_StripDisabledBehavioursFromList_m1864_ParameterInfos[] = 
{
	{"components", 0, 134218381, 0, &List_1_t418_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::StripDisabledBehavioursFromList(System.Collections.Generic.List`1<UnityEngine.Component>)
extern const MethodInfo LayoutRebuilder_StripDisabledBehavioursFromList_m1864_MethodInfo = 
{
	"StripDisabledBehavioursFromList"/* name */
	, (methodPointerType)&LayoutRebuilder_StripDisabledBehavioursFromList_m1864/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_StripDisabledBehavioursFromList_m1864_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
extern const Il2CppType UnityAction_1_t373_0_0_0;
extern const Il2CppType UnityAction_1_t373_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_PerformLayoutControl_m1865_ParameterInfos[] = 
{
	{"rect", 0, 134218382, 0, &RectTransform_t272_0_0_0},
	{"action", 1, 134218383, 0, &UnityAction_1_t373_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutControl(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
extern const MethodInfo LayoutRebuilder_PerformLayoutControl_m1865_MethodInfo = 
{
	"PerformLayoutControl"/* name */
	, (methodPointerType)&LayoutRebuilder_PerformLayoutControl_m1865/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_PerformLayoutControl_m1865_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
extern const Il2CppType UnityAction_1_t373_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_PerformLayoutCalculation_m1866_ParameterInfos[] = 
{
	{"rect", 0, 134218384, 0, &RectTransform_t272_0_0_0},
	{"action", 1, 134218385, 0, &UnityAction_1_t373_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::PerformLayoutCalculation(UnityEngine.RectTransform,UnityEngine.Events.UnityAction`1<UnityEngine.Component>)
extern const MethodInfo LayoutRebuilder_PerformLayoutCalculation_m1866_MethodInfo = 
{
	"PerformLayoutCalculation"/* name */
	, (methodPointerType)&LayoutRebuilder_PerformLayoutCalculation_m1866/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Object_t/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_PerformLayoutCalculation_m1866_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_MarkLayoutForRebuild_m1867_ParameterInfos[] = 
{
	{"rect", 0, 134218386, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutForRebuild(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_MarkLayoutForRebuild_m1867_MethodInfo = 
{
	"MarkLayoutForRebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_MarkLayoutForRebuild_m1867/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_MarkLayoutForRebuild_m1867_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_ValidLayoutGroup_m1868_ParameterInfos[] = 
{
	{"parent", 0, 134218387, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidLayoutGroup(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_ValidLayoutGroup_m1868_MethodInfo = 
{
	"ValidLayoutGroup"/* name */
	, (methodPointerType)&LayoutRebuilder_ValidLayoutGroup_m1868/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_ValidLayoutGroup_m1868_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_ValidController_m1869_ParameterInfos[] = 
{
	{"layoutRoot", 0, 134218388, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::ValidController(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_ValidController_m1869_MethodInfo = 
{
	"ValidController"/* name */
	, (methodPointerType)&LayoutRebuilder_ValidController_m1869/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_ValidController_m1869_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_MarkLayoutRootForRebuild_m1870_ParameterInfos[] = 
{
	{"controller", 0, 134218389, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::MarkLayoutRootForRebuild(UnityEngine.RectTransform)
extern const MethodInfo LayoutRebuilder_MarkLayoutRootForRebuild_m1870_MethodInfo = 
{
	"MarkLayoutRootForRebuild"/* name */
	, (methodPointerType)&LayoutRebuilder_MarkLayoutRootForRebuild_m1870/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_MarkLayoutRootForRebuild_m1870_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType LayoutRebuilder_t375_0_0_0;
extern const Il2CppType LayoutRebuilder_t375_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_Equals_m1871_ParameterInfos[] = 
{
	{"other", 0, 134218390, 0, &LayoutRebuilder_t375_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_LayoutRebuilder_t375 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::Equals(UnityEngine.UI.LayoutRebuilder)
extern const MethodInfo LayoutRebuilder_Equals_m1871_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&LayoutRebuilder_Equals_m1871/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_LayoutRebuilder_t375/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_Equals_m1871_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.UI.LayoutRebuilder::GetHashCode()
extern const MethodInfo LayoutRebuilder_GetHashCode_m1872_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&LayoutRebuilder_GetHashCode_m1872/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t127/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.UI.LayoutRebuilder::ToString()
extern const MethodInfo LayoutRebuilder_ToString_m1873_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&LayoutRebuilder_ToString_m1873/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t103_0_0_0;
extern const Il2CppType Component_t103_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_U3CRebuildU3Em__9_m1874_ParameterInfos[] = 
{
	{"e", 0, 134218391, 0, &Component_t103_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__9(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__9_m1874_MethodInfo = 
{
	"<Rebuild>m__9"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__9_m1874/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_U3CRebuildU3Em__9_m1874_ParameterInfos/* parameters */
	, 311/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t103_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_U3CRebuildU3Em__A_m1875_ParameterInfos[] = 
{
	{"e", 0, 134218392, 0, &Component_t103_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__A(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__A_m1875_MethodInfo = 
{
	"<Rebuild>m__A"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__A_m1875/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_U3CRebuildU3Em__A_m1875_ParameterInfos/* parameters */
	, 312/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1192/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t103_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_U3CRebuildU3Em__B_m1876_ParameterInfos[] = 
{
	{"e", 0, 134218393, 0, &Component_t103_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__B(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__B_m1876_MethodInfo = 
{
	"<Rebuild>m__B"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__B_m1876/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_U3CRebuildU3Em__B_m1876_ParameterInfos/* parameters */
	, 313/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1193/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t103_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_U3CRebuildU3Em__C_m1877_ParameterInfos[] = 
{
	{"e", 0, 134218394, 0, &Component_t103_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.LayoutRebuilder::<Rebuild>m__C(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CRebuildU3Em__C_m1877_MethodInfo = 
{
	"<Rebuild>m__C"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CRebuildU3Em__C_m1877/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_U3CRebuildU3Em__C_m1877_ParameterInfos/* parameters */
	, 314/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1194/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Component_t103_0_0_0;
static const ParameterInfo LayoutRebuilder_t375_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1878_ParameterInfos[] = 
{
	{"e", 0, 134218395, 0, &Component_t103_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.LayoutRebuilder::<StripDisabledBehavioursFromList>m__D(UnityEngine.Component)
extern const MethodInfo LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1878_MethodInfo = 
{
	"<StripDisabledBehavioursFromList>m__D"/* name */
	, (methodPointerType)&LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1878/* method */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Object_t/* invoker_method */
	, LayoutRebuilder_t375_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1878_ParameterInfos/* parameters */
	, 315/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1195/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutRebuilder_t375_MethodInfos[] =
{
	&LayoutRebuilder__ctor_m1858_MethodInfo,
	&LayoutRebuilder__cctor_m1859_MethodInfo,
	&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1860_MethodInfo,
	&LayoutRebuilder_ReapplyDrivenProperties_m1861_MethodInfo,
	&LayoutRebuilder_get_transform_m1862_MethodInfo,
	&LayoutRebuilder_IsDestroyed_m1863_MethodInfo,
	&LayoutRebuilder_StripDisabledBehavioursFromList_m1864_MethodInfo,
	&LayoutRebuilder_PerformLayoutControl_m1865_MethodInfo,
	&LayoutRebuilder_PerformLayoutCalculation_m1866_MethodInfo,
	&LayoutRebuilder_MarkLayoutForRebuild_m1867_MethodInfo,
	&LayoutRebuilder_ValidLayoutGroup_m1868_MethodInfo,
	&LayoutRebuilder_ValidController_m1869_MethodInfo,
	&LayoutRebuilder_MarkLayoutRootForRebuild_m1870_MethodInfo,
	&LayoutRebuilder_Equals_m1871_MethodInfo,
	&LayoutRebuilder_GetHashCode_m1872_MethodInfo,
	&LayoutRebuilder_ToString_m1873_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__9_m1874_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__A_m1875_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__B_m1876_MethodInfo,
	&LayoutRebuilder_U3CRebuildU3Em__C_m1877_MethodInfo,
	&LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1878_MethodInfo,
	NULL
};
extern const MethodInfo LayoutRebuilder_get_transform_m1862_MethodInfo;
static const PropertyInfo LayoutRebuilder_t375____transform_PropertyInfo = 
{
	&LayoutRebuilder_t375_il2cpp_TypeInfo/* parent */
	, "transform"/* name */
	, &LayoutRebuilder_get_transform_m1862_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* LayoutRebuilder_t375_PropertyInfos[] =
{
	&LayoutRebuilder_t375____transform_PropertyInfo,
	NULL
};
extern const MethodInfo LayoutRebuilder_GetHashCode_m1872_MethodInfo;
extern const MethodInfo LayoutRebuilder_ToString_m1873_MethodInfo;
extern const MethodInfo LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1860_MethodInfo;
extern const MethodInfo LayoutRebuilder_IsDestroyed_m1863_MethodInfo;
extern const MethodInfo LayoutRebuilder_Equals_m1871_MethodInfo;
static const Il2CppMethodReference LayoutRebuilder_t375_VTable[] =
{
	&ValueType_Equals_m2567_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&LayoutRebuilder_GetHashCode_m1872_MethodInfo,
	&LayoutRebuilder_ToString_m1873_MethodInfo,
	&LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1860_MethodInfo,
	&LayoutRebuilder_get_transform_m1862_MethodInfo,
	&LayoutRebuilder_IsDestroyed_m1863_MethodInfo,
	&LayoutRebuilder_Equals_m1871_MethodInfo,
};
static bool LayoutRebuilder_t375_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IEquatable_1_t540_0_0_0;
static const Il2CppType* LayoutRebuilder_t375_InterfacesTypeInfos[] = 
{
	&ICanvasElement_t411_0_0_0,
	&IEquatable_1_t540_0_0_0,
};
static Il2CppInterfaceOffsetPair LayoutRebuilder_t375_InterfacesOffsets[] = 
{
	{ &ICanvasElement_t411_0_0_0, 4},
	{ &IEquatable_1_t540_0_0_0, 7},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutRebuilder_t375_1_0_0;
const Il2CppTypeDefinitionMetadata LayoutRebuilder_t375_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, LayoutRebuilder_t375_InterfacesTypeInfos/* implementedInterfaces */
	, LayoutRebuilder_t375_InterfacesOffsets/* interfaceOffsets */
	, &ValueType_t524_0_0_0/* parent */
	, LayoutRebuilder_t375_VTable/* vtableMethods */
	, LayoutRebuilder_t375_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 596/* fieldStart */

};
TypeInfo LayoutRebuilder_t375_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutRebuilder"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutRebuilder_t375_MethodInfos/* methods */
	, LayoutRebuilder_t375_PropertyInfos/* properties */
	, NULL/* events */
	, &LayoutRebuilder_t375_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayoutRebuilder_t375_0_0_0/* byval_arg */
	, &LayoutRebuilder_t375_1_0_0/* this_arg */
	, &LayoutRebuilder_t375_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutRebuilder_t375)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (LayoutRebuilder_t375)+ sizeof (Il2CppObject)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LayoutRebuilder_t375_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 265/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, true/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 21/* method_count */
	, 1/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility.h"
// Metadata Definition UnityEngine.UI.LayoutUtility
extern TypeInfo LayoutUtility_t377_il2cpp_TypeInfo;
// UnityEngine.UI.LayoutUtility
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtilityMethodDeclarations.h"
extern const Il2CppType RectTransform_t272_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_GetMinSize_m1879_ParameterInfos[] = 
{
	{"rect", 0, 134218396, 0, &RectTransform_t272_0_0_0},
	{"axis", 1, 134218397, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinSize(UnityEngine.RectTransform,System.Int32)
extern const MethodInfo LayoutUtility_GetMinSize_m1879_MethodInfo = 
{
	"GetMinSize"/* name */
	, (methodPointerType)&LayoutUtility_GetMinSize_m1879/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t_Int32_t127/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_GetMinSize_m1879_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1196/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_GetPreferredSize_m1880_ParameterInfos[] = 
{
	{"rect", 0, 134218398, 0, &RectTransform_t272_0_0_0},
	{"axis", 1, 134218399, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredSize(UnityEngine.RectTransform,System.Int32)
extern const MethodInfo LayoutUtility_GetPreferredSize_m1880_MethodInfo = 
{
	"GetPreferredSize"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredSize_m1880/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t_Int32_t127/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_GetPreferredSize_m1880_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1197/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_GetFlexibleSize_m1881_ParameterInfos[] = 
{
	{"rect", 0, 134218400, 0, &RectTransform_t272_0_0_0},
	{"axis", 1, 134218401, 0, &Int32_t127_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t_Int32_t127 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleSize(UnityEngine.RectTransform,System.Int32)
extern const MethodInfo LayoutUtility_GetFlexibleSize_m1881_MethodInfo = 
{
	"GetFlexibleSize"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleSize_m1881/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t_Int32_t127/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_GetFlexibleSize_m1881_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1198/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_GetMinWidth_m1882_ParameterInfos[] = 
{
	{"rect", 0, 134218402, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinWidth(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetMinWidth_m1882_MethodInfo = 
{
	"GetMinWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetMinWidth_m1882/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_GetMinWidth_m1882_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1199/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_GetPreferredWidth_m1883_ParameterInfos[] = 
{
	{"rect", 0, 134218403, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredWidth(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetPreferredWidth_m1883_MethodInfo = 
{
	"GetPreferredWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredWidth_m1883/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_GetPreferredWidth_m1883_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1200/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_GetFlexibleWidth_m1884_ParameterInfos[] = 
{
	{"rect", 0, 134218404, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleWidth(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetFlexibleWidth_m1884_MethodInfo = 
{
	"GetFlexibleWidth"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleWidth_m1884/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_GetFlexibleWidth_m1884_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1201/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_GetMinHeight_m1885_ParameterInfos[] = 
{
	{"rect", 0, 134218405, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetMinHeight(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetMinHeight_m1885_MethodInfo = 
{
	"GetMinHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetMinHeight_m1885/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_GetMinHeight_m1885_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1202/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_GetPreferredHeight_m1886_ParameterInfos[] = 
{
	{"rect", 0, 134218406, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetPreferredHeight(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetPreferredHeight_m1886_MethodInfo = 
{
	"GetPreferredHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetPreferredHeight_m1886/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_GetPreferredHeight_m1886_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1203/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_GetFlexibleHeight_m1887_ParameterInfos[] = 
{
	{"rect", 0, 134218407, 0, &RectTransform_t272_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetFlexibleHeight(UnityEngine.RectTransform)
extern const MethodInfo LayoutUtility_GetFlexibleHeight_m1887_MethodInfo = 
{
	"GetFlexibleHeight"/* name */
	, (methodPointerType)&LayoutUtility_GetFlexibleHeight_m1887/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_GetFlexibleHeight_m1887_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1204/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
extern const Il2CppType Func_2_t376_0_0_0;
extern const Il2CppType Func_2_t376_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_GetLayoutProperty_m1888_ParameterInfos[] = 
{
	{"rect", 0, 134218408, 0, &RectTransform_t272_0_0_0},
	{"property", 1, 134218409, 0, &Func_2_t376_0_0_0},
	{"defaultValue", 2, 134218410, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t_Object_t_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single)
extern const MethodInfo LayoutUtility_GetLayoutProperty_m1888_MethodInfo = 
{
	"GetLayoutProperty"/* name */
	, (methodPointerType)&LayoutUtility_GetLayoutProperty_m1888/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t_Object_t_Single_t151/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_GetLayoutProperty_m1888_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1205/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType RectTransform_t272_0_0_0;
extern const Il2CppType Func_2_t376_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType ILayoutElement_t419_1_0_2;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_GetLayoutProperty_m1889_ParameterInfos[] = 
{
	{"rect", 0, 134218411, 0, &RectTransform_t272_0_0_0},
	{"property", 1, 134218412, 0, &Func_2_t376_0_0_0},
	{"defaultValue", 2, 134218413, 0, &Single_t151_0_0_0},
	{"source", 3, 134218414, 0, &ILayoutElement_t419_1_0_2},
};
extern void* RuntimeInvoker_Single_t151_Object_t_Object_t_Single_t151_ILayoutElementU26_t541 (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::GetLayoutProperty(UnityEngine.RectTransform,System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>,System.Single,UnityEngine.UI.ILayoutElement&)
extern const MethodInfo LayoutUtility_GetLayoutProperty_m1889_MethodInfo = 
{
	"GetLayoutProperty"/* name */
	, (methodPointerType)&LayoutUtility_GetLayoutProperty_m1889/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t_Object_t_Single_t151_ILayoutElementU26_t541/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_GetLayoutProperty_m1889_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1206/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t419_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_U3CGetMinWidthU3Em__E_m1890_ParameterInfos[] = 
{
	{"e", 0, 134218415, 0, &ILayoutElement_t419_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetMinWidth>m__E(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetMinWidthU3Em__E_m1890_MethodInfo = 
{
	"<GetMinWidth>m__E"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetMinWidthU3Em__E_m1890/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_U3CGetMinWidthU3Em__E_m1890_ParameterInfos/* parameters */
	, 324/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1207/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t419_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_U3CGetPreferredWidthU3Em__F_m1891_ParameterInfos[] = 
{
	{"e", 0, 134218416, 0, &ILayoutElement_t419_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__F(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredWidthU3Em__F_m1891_MethodInfo = 
{
	"<GetPreferredWidth>m__F"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredWidthU3Em__F_m1891/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_U3CGetPreferredWidthU3Em__F_m1891_ParameterInfos/* parameters */
	, 325/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1208/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t419_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_U3CGetPreferredWidthU3Em__10_m1892_ParameterInfos[] = 
{
	{"e", 0, 134218417, 0, &ILayoutElement_t419_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredWidth>m__10(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredWidthU3Em__10_m1892_MethodInfo = 
{
	"<GetPreferredWidth>m__10"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredWidthU3Em__10_m1892/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_U3CGetPreferredWidthU3Em__10_m1892_ParameterInfos/* parameters */
	, 326/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1209/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t419_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1893_ParameterInfos[] = 
{
	{"e", 0, 134218418, 0, &ILayoutElement_t419_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleWidth>m__11(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1893_MethodInfo = 
{
	"<GetFlexibleWidth>m__11"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1893/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1893_ParameterInfos/* parameters */
	, 327/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1210/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t419_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_U3CGetMinHeightU3Em__12_m1894_ParameterInfos[] = 
{
	{"e", 0, 134218419, 0, &ILayoutElement_t419_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetMinHeight>m__12(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetMinHeightU3Em__12_m1894_MethodInfo = 
{
	"<GetMinHeight>m__12"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetMinHeightU3Em__12_m1894/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_U3CGetMinHeightU3Em__12_m1894_ParameterInfos/* parameters */
	, 328/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1211/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t419_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_U3CGetPreferredHeightU3Em__13_m1895_ParameterInfos[] = 
{
	{"e", 0, 134218420, 0, &ILayoutElement_t419_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__13(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredHeightU3Em__13_m1895_MethodInfo = 
{
	"<GetPreferredHeight>m__13"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredHeightU3Em__13_m1895/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_U3CGetPreferredHeightU3Em__13_m1895_ParameterInfos/* parameters */
	, 329/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1212/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t419_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_U3CGetPreferredHeightU3Em__14_m1896_ParameterInfos[] = 
{
	{"e", 0, 134218421, 0, &ILayoutElement_t419_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetPreferredHeight>m__14(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetPreferredHeightU3Em__14_m1896_MethodInfo = 
{
	"<GetPreferredHeight>m__14"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetPreferredHeightU3Em__14_m1896/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_U3CGetPreferredHeightU3Em__14_m1896_ParameterInfos/* parameters */
	, 330/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1213/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ILayoutElement_t419_0_0_0;
static const ParameterInfo LayoutUtility_t377_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1897_ParameterInfos[] = 
{
	{"e", 0, 134218422, 0, &ILayoutElement_t419_0_0_0},
};
extern void* RuntimeInvoker_Single_t151_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.UI.LayoutUtility::<GetFlexibleHeight>m__15(UnityEngine.UI.ILayoutElement)
extern const MethodInfo LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1897_MethodInfo = 
{
	"<GetFlexibleHeight>m__15"/* name */
	, (methodPointerType)&LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1897/* method */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* declaring_type */
	, &Single_t151_0_0_0/* return_type */
	, RuntimeInvoker_Single_t151_Object_t/* invoker_method */
	, LayoutUtility_t377_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1897_ParameterInfos/* parameters */
	, 331/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1214/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* LayoutUtility_t377_MethodInfos[] =
{
	&LayoutUtility_GetMinSize_m1879_MethodInfo,
	&LayoutUtility_GetPreferredSize_m1880_MethodInfo,
	&LayoutUtility_GetFlexibleSize_m1881_MethodInfo,
	&LayoutUtility_GetMinWidth_m1882_MethodInfo,
	&LayoutUtility_GetPreferredWidth_m1883_MethodInfo,
	&LayoutUtility_GetFlexibleWidth_m1884_MethodInfo,
	&LayoutUtility_GetMinHeight_m1885_MethodInfo,
	&LayoutUtility_GetPreferredHeight_m1886_MethodInfo,
	&LayoutUtility_GetFlexibleHeight_m1887_MethodInfo,
	&LayoutUtility_GetLayoutProperty_m1888_MethodInfo,
	&LayoutUtility_GetLayoutProperty_m1889_MethodInfo,
	&LayoutUtility_U3CGetMinWidthU3Em__E_m1890_MethodInfo,
	&LayoutUtility_U3CGetPreferredWidthU3Em__F_m1891_MethodInfo,
	&LayoutUtility_U3CGetPreferredWidthU3Em__10_m1892_MethodInfo,
	&LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1893_MethodInfo,
	&LayoutUtility_U3CGetMinHeightU3Em__12_m1894_MethodInfo,
	&LayoutUtility_U3CGetPreferredHeightU3Em__13_m1895_MethodInfo,
	&LayoutUtility_U3CGetPreferredHeightU3Em__14_m1896_MethodInfo,
	&LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1897_MethodInfo,
	NULL
};
static const Il2CppMethodReference LayoutUtility_t377_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool LayoutUtility_t377_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType LayoutUtility_t377_0_0_0;
extern const Il2CppType LayoutUtility_t377_1_0_0;
struct LayoutUtility_t377;
const Il2CppTypeDefinitionMetadata LayoutUtility_t377_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, LayoutUtility_t377_VTable/* vtableMethods */
	, LayoutUtility_t377_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 603/* fieldStart */

};
TypeInfo LayoutUtility_t377_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutUtility"/* name */
	, "UnityEngine.UI"/* namespaze */
	, LayoutUtility_t377_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &LayoutUtility_t377_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &LayoutUtility_t377_0_0_0/* byval_arg */
	, &LayoutUtility_t377_1_0_0/* this_arg */
	, &LayoutUtility_t377_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LayoutUtility_t377)/* instance_size */
	, sizeof (LayoutUtility_t377)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(LayoutUtility_t377_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 19/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup.h"
// Metadata Definition UnityEngine.UI.VerticalLayoutGroup
extern TypeInfo VerticalLayoutGroup_t378_il2cpp_TypeInfo;
// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroupMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::.ctor()
extern const MethodInfo VerticalLayoutGroup__ctor_m1898_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VerticalLayoutGroup__ctor_m1898/* method */
	, &VerticalLayoutGroup_t378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1215/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputHorizontal()
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1899_MethodInfo = 
{
	"CalculateLayoutInputHorizontal"/* name */
	, (methodPointerType)&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1899/* method */
	, &VerticalLayoutGroup_t378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1216/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputVertical()
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputVertical_m1900_MethodInfo = 
{
	"CalculateLayoutInputVertical"/* name */
	, (methodPointerType)&VerticalLayoutGroup_CalculateLayoutInputVertical_m1900/* method */
	, &VerticalLayoutGroup_t378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1217/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutHorizontal()
extern const MethodInfo VerticalLayoutGroup_SetLayoutHorizontal_m1901_MethodInfo = 
{
	"SetLayoutHorizontal"/* name */
	, (methodPointerType)&VerticalLayoutGroup_SetLayoutHorizontal_m1901/* method */
	, &VerticalLayoutGroup_t378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 36/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1218/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutVertical()
extern const MethodInfo VerticalLayoutGroup_SetLayoutVertical_m1902_MethodInfo = 
{
	"SetLayoutVertical"/* name */
	, (methodPointerType)&VerticalLayoutGroup_SetLayoutVertical_m1902/* method */
	, &VerticalLayoutGroup_t378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 37/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1219/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* VerticalLayoutGroup_t378_MethodInfos[] =
{
	&VerticalLayoutGroup__ctor_m1898_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1899_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m1900_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m1901_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m1902_MethodInfo,
	NULL
};
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1899_MethodInfo;
extern const MethodInfo VerticalLayoutGroup_CalculateLayoutInputVertical_m1900_MethodInfo;
extern const MethodInfo VerticalLayoutGroup_SetLayoutHorizontal_m1901_MethodInfo;
extern const MethodInfo VerticalLayoutGroup_SetLayoutVertical_m1902_MethodInfo;
static const Il2CppMethodReference VerticalLayoutGroup_t378_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&LayoutGroup_OnEnable_m1845_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&LayoutGroup_OnDisable_m1846_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&LayoutGroup_OnRectTransformDimensionsChange_m1855_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&LayoutGroup_OnDidApplyAnimationProperties_m1847_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1899_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m1900_MethodInfo,
	&LayoutGroup_get_minWidth_m1838_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1839_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1840_MethodInfo,
	&LayoutGroup_get_minHeight_m1841_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1842_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1843_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1844_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m1901_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m1902_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1899_MethodInfo,
	&VerticalLayoutGroup_CalculateLayoutInputVertical_m1900_MethodInfo,
	&LayoutGroup_get_minWidth_m1838_MethodInfo,
	&LayoutGroup_get_preferredWidth_m1839_MethodInfo,
	&LayoutGroup_get_flexibleWidth_m1840_MethodInfo,
	&LayoutGroup_get_minHeight_m1841_MethodInfo,
	&LayoutGroup_get_preferredHeight_m1842_MethodInfo,
	&LayoutGroup_get_flexibleHeight_m1843_MethodInfo,
	&LayoutGroup_get_layoutPriority_m1844_MethodInfo,
	&VerticalLayoutGroup_SetLayoutHorizontal_m1901_MethodInfo,
	&VerticalLayoutGroup_SetLayoutVertical_m1902_MethodInfo,
	&LayoutGroup_OnTransformChildrenChanged_m1856_MethodInfo,
};
static bool VerticalLayoutGroup_t378_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair VerticalLayoutGroup_t378_InterfacesOffsets[] = 
{
	{ &ILayoutElement_t419_0_0_0, 16},
	{ &ILayoutController_t475_0_0_0, 25},
	{ &ILayoutGroup_t473_0_0_0, 27},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType VerticalLayoutGroup_t378_0_0_0;
extern const Il2CppType VerticalLayoutGroup_t378_1_0_0;
struct VerticalLayoutGroup_t378;
const Il2CppTypeDefinitionMetadata VerticalLayoutGroup_t378_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, VerticalLayoutGroup_t378_InterfacesOffsets/* interfaceOffsets */
	, &HorizontalOrVerticalLayoutGroup_t369_0_0_0/* parent */
	, VerticalLayoutGroup_t378_VTable/* vtableMethods */
	, VerticalLayoutGroup_t378_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo VerticalLayoutGroup_t378_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "VerticalLayoutGroup"/* name */
	, "UnityEngine.UI"/* namespaze */
	, VerticalLayoutGroup_t378_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &VerticalLayoutGroup_t378_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 332/* custom_attributes_cache */
	, &VerticalLayoutGroup_t378_0_0_0/* byval_arg */
	, &VerticalLayoutGroup_t378_1_0_0/* this_arg */
	, &VerticalLayoutGroup_t378_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VerticalLayoutGroup_t378)/* instance_size */
	, sizeof (VerticalLayoutGroup_t378)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 39/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IMaterialModifier
extern TypeInfo IMaterialModifier_t439_il2cpp_TypeInfo;
extern const Il2CppType Material_t4_0_0_0;
static const ParameterInfo IMaterialModifier_t439_IMaterialModifier_GetModifiedMaterial_m2521_ParameterInfos[] = 
{
	{"baseMaterial", 0, 134218423, 0, &Material_t4_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.IMaterialModifier::GetModifiedMaterial(UnityEngine.Material)
extern const MethodInfo IMaterialModifier_GetModifiedMaterial_m2521_MethodInfo = 
{
	"GetModifiedMaterial"/* name */
	, NULL/* method */
	, &IMaterialModifier_t439_il2cpp_TypeInfo/* declaring_type */
	, &Material_t4_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, IMaterialModifier_t439_IMaterialModifier_GetModifiedMaterial_m2521_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1220/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IMaterialModifier_t439_MethodInfos[] =
{
	&IMaterialModifier_GetModifiedMaterial_m2521_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IMaterialModifier_t439_0_0_0;
extern const Il2CppType IMaterialModifier_t439_1_0_0;
struct IMaterialModifier_t439;
const Il2CppTypeDefinitionMetadata IMaterialModifier_t439_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IMaterialModifier_t439_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IMaterialModifier"/* name */
	, "UnityEngine.UI"/* namespaze */
	, IMaterialModifier_t439_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IMaterialModifier_t439_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IMaterialModifier_t439_0_0_0/* byval_arg */
	, &IMaterialModifier_t439_1_0_0/* this_arg */
	, &IMaterialModifier_t439_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_Mask.h"
// Metadata Definition UnityEngine.UI.Mask
extern TypeInfo Mask_t379_il2cpp_TypeInfo;
// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_MaskMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::.ctor()
extern const MethodInfo Mask__ctor_m1903_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Mask__ctor_m1903/* method */
	, &Mask_t379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1221/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.Mask::get_graphic()
extern const MethodInfo Mask_get_graphic_m1904_MethodInfo = 
{
	"get_graphic"/* name */
	, (methodPointerType)&Mask_get_graphic_m1904/* method */
	, &Mask_t379_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t278_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1222/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::get_showMaskGraphic()
extern const MethodInfo Mask_get_showMaskGraphic_m1905_MethodInfo = 
{
	"get_showMaskGraphic"/* name */
	, (methodPointerType)&Mask_get_showMaskGraphic_m1905/* method */
	, &Mask_t379_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1223/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Mask_t379_Mask_set_showMaskGraphic_m1906_ParameterInfos[] = 
{
	{"value", 0, 134218424, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::set_showMaskGraphic(System.Boolean)
extern const MethodInfo Mask_set_showMaskGraphic_m1906_MethodInfo = 
{
	"set_showMaskGraphic"/* name */
	, (methodPointerType)&Mask_set_showMaskGraphic_m1906/* method */
	, &Mask_t379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Mask_t379_Mask_set_showMaskGraphic_m1906_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1224/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.RectTransform UnityEngine.UI.Mask::get_rectTransform()
extern const MethodInfo Mask_get_rectTransform_m1907_MethodInfo = 
{
	"get_rectTransform"/* name */
	, (methodPointerType)&Mask_get_rectTransform_m1907/* method */
	, &Mask_t379_il2cpp_TypeInfo/* declaring_type */
	, &RectTransform_t272_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1225/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::MaskEnabled()
extern const MethodInfo Mask_MaskEnabled_m1908_MethodInfo = 
{
	"MaskEnabled"/* name */
	, (methodPointerType)&Mask_MaskEnabled_m1908/* method */
	, &Mask_t379_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1226/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnSiblingGraphicEnabledDisabled()
extern const MethodInfo Mask_OnSiblingGraphicEnabledDisabled_m1909_MethodInfo = 
{
	"OnSiblingGraphicEnabledDisabled"/* name */
	, (methodPointerType)&Mask_OnSiblingGraphicEnabledDisabled_m1909/* method */
	, &Mask_t379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1227/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::NotifyMaskStateChanged()
extern const MethodInfo Mask_NotifyMaskStateChanged_m1910_MethodInfo = 
{
	"NotifyMaskStateChanged"/* name */
	, (methodPointerType)&Mask_NotifyMaskStateChanged_m1910/* method */
	, &Mask_t379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1228/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::ClearCachedMaterial()
extern const MethodInfo Mask_ClearCachedMaterial_m1911_MethodInfo = 
{
	"ClearCachedMaterial"/* name */
	, (methodPointerType)&Mask_ClearCachedMaterial_m1911/* method */
	, &Mask_t379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1229/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnEnable()
extern const MethodInfo Mask_OnEnable_m1912_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&Mask_OnEnable_m1912/* method */
	, &Mask_t379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1230/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Mask::OnDisable()
extern const MethodInfo Mask_OnDisable_m1913_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&Mask_OnDisable_m1913/* method */
	, &Mask_t379_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1231/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
extern const Il2CppType Camera_t3_0_0_0;
static const ParameterInfo Mask_t379_Mask_IsRaycastLocationValid_m1914_ParameterInfos[] = 
{
	{"sp", 0, 134218425, 0, &Vector2_t10_0_0_0},
	{"eventCamera", 1, 134218426, 0, &Camera_t3_0_0_0},
};
extern void* RuntimeInvoker_Boolean_t169_Vector2_t10_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Mask::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern const MethodInfo Mask_IsRaycastLocationValid_m1914_MethodInfo = 
{
	"IsRaycastLocationValid"/* name */
	, (methodPointerType)&Mask_IsRaycastLocationValid_m1914/* method */
	, &Mask_t379_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169_Vector2_t10_Object_t/* invoker_method */
	, Mask_t379_Mask_IsRaycastLocationValid_m1914_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1232/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Material_t4_0_0_0;
static const ParameterInfo Mask_t379_Mask_GetModifiedMaterial_m1915_ParameterInfos[] = 
{
	{"baseMaterial", 0, 134218427, 0, &Material_t4_0_0_0},
};
extern void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Material UnityEngine.UI.Mask::GetModifiedMaterial(UnityEngine.Material)
extern const MethodInfo Mask_GetModifiedMaterial_m1915_MethodInfo = 
{
	"GetModifiedMaterial"/* name */
	, (methodPointerType)&Mask_GetModifiedMaterial_m1915/* method */
	, &Mask_t379_il2cpp_TypeInfo/* declaring_type */
	, &Material_t4_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Mask_t379_Mask_GetModifiedMaterial_m1915_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1233/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Mask_t379_MethodInfos[] =
{
	&Mask__ctor_m1903_MethodInfo,
	&Mask_get_graphic_m1904_MethodInfo,
	&Mask_get_showMaskGraphic_m1905_MethodInfo,
	&Mask_set_showMaskGraphic_m1906_MethodInfo,
	&Mask_get_rectTransform_m1907_MethodInfo,
	&Mask_MaskEnabled_m1908_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m1909_MethodInfo,
	&Mask_NotifyMaskStateChanged_m1910_MethodInfo,
	&Mask_ClearCachedMaterial_m1911_MethodInfo,
	&Mask_OnEnable_m1912_MethodInfo,
	&Mask_OnDisable_m1913_MethodInfo,
	&Mask_IsRaycastLocationValid_m1914_MethodInfo,
	&Mask_GetModifiedMaterial_m1915_MethodInfo,
	NULL
};
extern const MethodInfo Mask_get_graphic_m1904_MethodInfo;
static const PropertyInfo Mask_t379____graphic_PropertyInfo = 
{
	&Mask_t379_il2cpp_TypeInfo/* parent */
	, "graphic"/* name */
	, &Mask_get_graphic_m1904_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Mask_get_showMaskGraphic_m1905_MethodInfo;
extern const MethodInfo Mask_set_showMaskGraphic_m1906_MethodInfo;
static const PropertyInfo Mask_t379____showMaskGraphic_PropertyInfo = 
{
	&Mask_t379_il2cpp_TypeInfo/* parent */
	, "showMaskGraphic"/* name */
	, &Mask_get_showMaskGraphic_m1905_MethodInfo/* get */
	, &Mask_set_showMaskGraphic_m1906_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Mask_get_rectTransform_m1907_MethodInfo;
static const PropertyInfo Mask_t379____rectTransform_PropertyInfo = 
{
	&Mask_t379_il2cpp_TypeInfo/* parent */
	, "rectTransform"/* name */
	, &Mask_get_rectTransform_m1907_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Mask_t379_PropertyInfos[] =
{
	&Mask_t379____graphic_PropertyInfo,
	&Mask_t379____showMaskGraphic_PropertyInfo,
	&Mask_t379____rectTransform_PropertyInfo,
	NULL
};
extern const MethodInfo Mask_OnEnable_m1912_MethodInfo;
extern const MethodInfo Mask_OnDisable_m1913_MethodInfo;
extern const MethodInfo Mask_OnSiblingGraphicEnabledDisabled_m1909_MethodInfo;
extern const MethodInfo Mask_MaskEnabled_m1908_MethodInfo;
extern const MethodInfo Mask_IsRaycastLocationValid_m1914_MethodInfo;
extern const MethodInfo Mask_GetModifiedMaterial_m1915_MethodInfo;
static const Il2CppMethodReference Mask_t379_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&Mask_OnEnable_m1912_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&Mask_OnDisable_m1913_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m858_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m861_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m1909_MethodInfo,
	&Mask_MaskEnabled_m1908_MethodInfo,
	&Mask_IsRaycastLocationValid_m1914_MethodInfo,
	&Mask_GetModifiedMaterial_m1915_MethodInfo,
	&Mask_MaskEnabled_m1908_MethodInfo,
	&Mask_OnSiblingGraphicEnabledDisabled_m1909_MethodInfo,
	&Mask_IsRaycastLocationValid_m1914_MethodInfo,
	&Mask_GetModifiedMaterial_m1915_MethodInfo,
};
static bool Mask_t379_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IGraphicEnabledDisabled_t440_0_0_0;
extern const Il2CppType IMask_t463_0_0_0;
extern const Il2CppType ICanvasRaycastFilter_t443_0_0_0;
static const Il2CppType* Mask_t379_InterfacesTypeInfos[] = 
{
	&IGraphicEnabledDisabled_t440_0_0_0,
	&IMask_t463_0_0_0,
	&ICanvasRaycastFilter_t443_0_0_0,
	&IMaterialModifier_t439_0_0_0,
};
static Il2CppInterfaceOffsetPair Mask_t379_InterfacesOffsets[] = 
{
	{ &IGraphicEnabledDisabled_t440_0_0_0, 16},
	{ &IMask_t463_0_0_0, 17},
	{ &ICanvasRaycastFilter_t443_0_0_0, 18},
	{ &IMaterialModifier_t439_0_0_0, 19},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Mask_t379_0_0_0;
extern const Il2CppType Mask_t379_1_0_0;
struct Mask_t379;
const Il2CppTypeDefinitionMetadata Mask_t379_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, Mask_t379_InterfacesTypeInfos/* implementedInterfaces */
	, Mask_t379_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t199_0_0_0/* parent */
	, Mask_t379_VTable/* vtableMethods */
	, Mask_t379_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 611/* fieldStart */

};
TypeInfo Mask_t379_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mask"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Mask_t379_MethodInfos/* methods */
	, Mask_t379_PropertyInfos/* properties */
	, NULL/* events */
	, &Mask_t379_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 333/* custom_attributes_cache */
	, &Mask_t379_0_0_0/* byval_arg */
	, &Mask_t379_1_0_0/* this_arg */
	, &Mask_t379_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mask_t379)/* instance_size */
	, sizeof (Mask_t379)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 13/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 24/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.Collections.IndexedSet`1
extern TypeInfo IndexedSet_1_t507_il2cpp_TypeInfo;
extern const Il2CppGenericContainer IndexedSet_1_t507_Il2CppGenericContainer;
extern TypeInfo IndexedSet_1_t507_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter IndexedSet_1_t507_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &IndexedSet_1_t507_Il2CppGenericContainer, NULL, "T", 0, 0 };
static const Il2CppGenericParameter* IndexedSet_1_t507_Il2CppGenericParametersArray[1] = 
{
	&IndexedSet_1_t507_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer IndexedSet_1_t507_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&IndexedSet_1_t507_il2cpp_TypeInfo, 1, 0, IndexedSet_1_t507_Il2CppGenericParametersArray };
// System.Void UnityEngine.UI.Collections.IndexedSet`1::.ctor()
extern const MethodInfo IndexedSet_1__ctor_m2523_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1234/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_t410_0_0_0;
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1::System.Collections.IEnumerable.GetEnumerator()
extern const MethodInfo IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m2524_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t410_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1235/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t507_gp_0_0_0_0;
extern const Il2CppType IndexedSet_1_t507_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t507_IndexedSet_1_Add_m2525_ParameterInfos[] = 
{
	{"item", 0, 134218428, 0, &IndexedSet_1_t507_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Add(T)
extern const MethodInfo IndexedSet_1_Add_m2525_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t507_IndexedSet_1_Add_m2525_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1236/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t507_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t507_IndexedSet_1_Remove_m2526_ParameterInfos[] = 
{
	{"item", 0, 134218429, 0, &IndexedSet_1_t507_gp_0_0_0_0},
};
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::Remove(T)
extern const MethodInfo IndexedSet_1_Remove_m2526_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t507_IndexedSet_1_Remove_m2526_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1237/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IEnumerator_1_t543_0_0_0;
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1::GetEnumerator()
extern const MethodInfo IndexedSet_1_GetEnumerator_m2527_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t543_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1238/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Clear()
extern const MethodInfo IndexedSet_1_Clear_m2528_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1239/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t507_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t507_IndexedSet_1_Contains_m2529_ParameterInfos[] = 
{
	{"item", 0, 134218430, 0, &IndexedSet_1_t507_gp_0_0_0_0},
};
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::Contains(T)
extern const MethodInfo IndexedSet_1_Contains_m2529_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t507_IndexedSet_1_Contains_m2529_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1240/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType TU5BU5D_t544_0_0_0;
extern const Il2CppType TU5BU5D_t544_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo IndexedSet_1_t507_IndexedSet_1_CopyTo_m2530_ParameterInfos[] = 
{
	{"array", 0, 134218431, 0, &TU5BU5D_t544_0_0_0},
	{"arrayIndex", 1, 134218432, 0, &Int32_t127_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::CopyTo(T[],System.Int32)
extern const MethodInfo IndexedSet_1_CopyTo_m2530_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t507_IndexedSet_1_CopyTo_m2530_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1241/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1::get_Count()
extern const MethodInfo IndexedSet_1_get_Count_m2531_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1242/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1::get_IsReadOnly()
extern const MethodInfo IndexedSet_1_get_IsReadOnly_m2532_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1243/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType IndexedSet_1_t507_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t507_IndexedSet_1_IndexOf_m2533_ParameterInfos[] = 
{
	{"item", 0, 134218433, 0, &IndexedSet_1_t507_gp_0_0_0_0},
};
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1::IndexOf(T)
extern const MethodInfo IndexedSet_1_IndexOf_m2533_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t507_IndexedSet_1_IndexOf_m2533_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1244/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType IndexedSet_1_t507_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t507_IndexedSet_1_Insert_m2534_ParameterInfos[] = 
{
	{"index", 0, 134218434, 0, &Int32_t127_0_0_0},
	{"item", 1, 134218435, 0, &IndexedSet_1_t507_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Insert(System.Int32,T)
extern const MethodInfo IndexedSet_1_Insert_m2534_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t507_IndexedSet_1_Insert_m2534_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1245/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo IndexedSet_1_t507_IndexedSet_1_RemoveAt_m2535_ParameterInfos[] = 
{
	{"index", 0, 134218436, 0, &Int32_t127_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::RemoveAt(System.Int32)
extern const MethodInfo IndexedSet_1_RemoveAt_m2535_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t507_IndexedSet_1_RemoveAt_m2535_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1246/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo IndexedSet_1_t507_IndexedSet_1_get_Item_m2536_ParameterInfos[] = 
{
	{"index", 0, 134218437, 0, &Int32_t127_0_0_0},
};
// T UnityEngine.UI.Collections.IndexedSet`1::get_Item(System.Int32)
extern const MethodInfo IndexedSet_1_get_Item_m2536_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &IndexedSet_1_t507_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t507_IndexedSet_1_get_Item_m2536_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1247/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType IndexedSet_1_t507_gp_0_0_0_0;
static const ParameterInfo IndexedSet_1_t507_IndexedSet_1_set_Item_m2537_ParameterInfos[] = 
{
	{"index", 0, 134218438, 0, &Int32_t127_0_0_0},
	{"value", 1, 134218439, 0, &IndexedSet_1_t507_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::set_Item(System.Int32,T)
extern const MethodInfo IndexedSet_1_set_Item_m2537_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t507_IndexedSet_1_set_Item_m2537_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1248/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Predicate_1_t545_0_0_0;
extern const Il2CppType Predicate_1_t545_0_0_0;
static const ParameterInfo IndexedSet_1_t507_IndexedSet_1_RemoveAll_m2538_ParameterInfos[] = 
{
	{"match", 0, 134218440, 0, &Predicate_1_t545_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::RemoveAll(System.Predicate`1<T>)
extern const MethodInfo IndexedSet_1_RemoveAll_m2538_MethodInfo = 
{
	"RemoveAll"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t507_IndexedSet_1_RemoveAll_m2538_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1249/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Comparison_1_t546_0_0_0;
extern const Il2CppType Comparison_1_t546_0_0_0;
static const ParameterInfo IndexedSet_1_t507_IndexedSet_1_Sort_m2539_ParameterInfos[] = 
{
	{"sortLayoutFunction", 0, 134218441, 0, &Comparison_1_t546_0_0_0},
};
// System.Void UnityEngine.UI.Collections.IndexedSet`1::Sort(System.Comparison`1<T>)
extern const MethodInfo IndexedSet_1_Sort_m2539_MethodInfo = 
{
	"Sort"/* name */
	, NULL/* method */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, IndexedSet_1_t507_IndexedSet_1_Sort_m2539_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1250/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IndexedSet_1_t507_MethodInfos[] =
{
	&IndexedSet_1__ctor_m2523_MethodInfo,
	&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m2524_MethodInfo,
	&IndexedSet_1_Add_m2525_MethodInfo,
	&IndexedSet_1_Remove_m2526_MethodInfo,
	&IndexedSet_1_GetEnumerator_m2527_MethodInfo,
	&IndexedSet_1_Clear_m2528_MethodInfo,
	&IndexedSet_1_Contains_m2529_MethodInfo,
	&IndexedSet_1_CopyTo_m2530_MethodInfo,
	&IndexedSet_1_get_Count_m2531_MethodInfo,
	&IndexedSet_1_get_IsReadOnly_m2532_MethodInfo,
	&IndexedSet_1_IndexOf_m2533_MethodInfo,
	&IndexedSet_1_Insert_m2534_MethodInfo,
	&IndexedSet_1_RemoveAt_m2535_MethodInfo,
	&IndexedSet_1_get_Item_m2536_MethodInfo,
	&IndexedSet_1_set_Item_m2537_MethodInfo,
	&IndexedSet_1_RemoveAll_m2538_MethodInfo,
	&IndexedSet_1_Sort_m2539_MethodInfo,
	NULL
};
extern const MethodInfo IndexedSet_1_get_Count_m2531_MethodInfo;
static const PropertyInfo IndexedSet_1_t507____Count_PropertyInfo = 
{
	&IndexedSet_1_t507_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &IndexedSet_1_get_Count_m2531_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IndexedSet_1_get_IsReadOnly_m2532_MethodInfo;
static const PropertyInfo IndexedSet_1_t507____IsReadOnly_PropertyInfo = 
{
	&IndexedSet_1_t507_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &IndexedSet_1_get_IsReadOnly_m2532_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo IndexedSet_1_get_Item_m2536_MethodInfo;
extern const MethodInfo IndexedSet_1_set_Item_m2537_MethodInfo;
static const PropertyInfo IndexedSet_1_t507____Item_PropertyInfo = 
{
	&IndexedSet_1_t507_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IndexedSet_1_get_Item_m2536_MethodInfo/* get */
	, &IndexedSet_1_set_Item_m2537_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* IndexedSet_1_t507_PropertyInfos[] =
{
	&IndexedSet_1_t507____Count_PropertyInfo,
	&IndexedSet_1_t507____IsReadOnly_PropertyInfo,
	&IndexedSet_1_t507____Item_PropertyInfo,
	NULL
};
extern const MethodInfo IndexedSet_1_IndexOf_m2533_MethodInfo;
extern const MethodInfo IndexedSet_1_Insert_m2534_MethodInfo;
extern const MethodInfo IndexedSet_1_RemoveAt_m2535_MethodInfo;
extern const MethodInfo IndexedSet_1_Add_m2525_MethodInfo;
extern const MethodInfo IndexedSet_1_Clear_m2528_MethodInfo;
extern const MethodInfo IndexedSet_1_Contains_m2529_MethodInfo;
extern const MethodInfo IndexedSet_1_CopyTo_m2530_MethodInfo;
extern const MethodInfo IndexedSet_1_Remove_m2526_MethodInfo;
extern const MethodInfo IndexedSet_1_GetEnumerator_m2527_MethodInfo;
extern const MethodInfo IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m2524_MethodInfo;
static const Il2CppMethodReference IndexedSet_1_t507_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
	&IndexedSet_1_IndexOf_m2533_MethodInfo,
	&IndexedSet_1_Insert_m2534_MethodInfo,
	&IndexedSet_1_RemoveAt_m2535_MethodInfo,
	&IndexedSet_1_get_Item_m2536_MethodInfo,
	&IndexedSet_1_set_Item_m2537_MethodInfo,
	&IndexedSet_1_get_Count_m2531_MethodInfo,
	&IndexedSet_1_get_IsReadOnly_m2532_MethodInfo,
	&IndexedSet_1_Add_m2525_MethodInfo,
	&IndexedSet_1_Clear_m2528_MethodInfo,
	&IndexedSet_1_Contains_m2529_MethodInfo,
	&IndexedSet_1_CopyTo_m2530_MethodInfo,
	&IndexedSet_1_Remove_m2526_MethodInfo,
	&IndexedSet_1_GetEnumerator_m2527_MethodInfo,
	&IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m2524_MethodInfo,
};
static bool IndexedSet_1_t507_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IList_1_t547_0_0_0;
extern const Il2CppType ICollection_1_t548_0_0_0;
extern const Il2CppType IEnumerable_1_t549_0_0_0;
extern const Il2CppType IEnumerable_t550_0_0_0;
static const Il2CppType* IndexedSet_1_t507_InterfacesTypeInfos[] = 
{
	&IList_1_t547_0_0_0,
	&ICollection_1_t548_0_0_0,
	&IEnumerable_1_t549_0_0_0,
	&IEnumerable_t550_0_0_0,
};
static Il2CppInterfaceOffsetPair IndexedSet_1_t507_InterfacesOffsets[] = 
{
	{ &IList_1_t547_0_0_0, 4},
	{ &ICollection_1_t548_0_0_0, 9},
	{ &IEnumerable_1_t549_0_0_0, 16},
	{ &IEnumerable_t550_0_0_0, 17},
};
extern const Il2CppType List_1_t551_0_0_0;
extern const Il2CppGenericMethod List_1__ctor_m2584_GenericMethod;
extern const Il2CppType Dictionary_2_t552_0_0_0;
extern const Il2CppGenericMethod Dictionary_2__ctor_m2585_GenericMethod;
extern const Il2CppGenericMethod IndexedSet_1_GetEnumerator_m2586_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_ContainsKey_m2587_GenericMethod;
extern const Il2CppGenericMethod List_1_Add_m2588_GenericMethod;
extern const Il2CppGenericMethod List_1_get_Count_m2589_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Add_m2590_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_TryGetValue_m2591_GenericMethod;
extern const Il2CppGenericMethod IndexedSet_1_RemoveAt_m2592_GenericMethod;
extern const Il2CppGenericMethod List_1_Clear_m2593_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Clear_m2594_GenericMethod;
extern const Il2CppGenericMethod List_1_CopyTo_m2595_GenericMethod;
extern const Il2CppGenericMethod List_1_get_Item_m2596_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_Remove_m2597_GenericMethod;
extern const Il2CppGenericMethod List_1_RemoveAt_m2598_GenericMethod;
extern const Il2CppGenericMethod List_1_set_Item_m2599_GenericMethod;
extern const Il2CppGenericMethod Dictionary_2_set_Item_m2600_GenericMethod;
extern const Il2CppGenericMethod Predicate_1_Invoke_m2601_GenericMethod;
extern const Il2CppGenericMethod IndexedSet_1_Remove_m2602_GenericMethod;
extern const Il2CppGenericMethod List_1_Sort_m2603_GenericMethod;
static Il2CppRGCTXDefinition IndexedSet_1_t507_RGCTXData[23] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&List_1_t551_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1__ctor_m2584_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Dictionary_2_t552_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2__ctor_m2585_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_GetEnumerator_m2586_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_ContainsKey_m2587_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Add_m2588_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_get_Count_m2589_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Add_m2590_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_TryGetValue_m2591_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_RemoveAt_m2592_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Clear_m2593_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Clear_m2594_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_CopyTo_m2595_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_get_Item_m2596_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_Remove_m2597_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_RemoveAt_m2598_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_set_Item_m2599_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Dictionary_2_set_Item_m2600_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Predicate_1_Invoke_m2601_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &IndexedSet_1_Remove_m2602_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &List_1_Sort_m2603_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IndexedSet_1_t507_0_0_0;
extern const Il2CppType IndexedSet_1_t507_1_0_0;
struct IndexedSet_1_t507;
const Il2CppTypeDefinitionMetadata IndexedSet_1_t507_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, IndexedSet_1_t507_InterfacesTypeInfos/* implementedInterfaces */
	, IndexedSet_1_t507_InterfacesOffsets/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, IndexedSet_1_t507_VTable/* vtableMethods */
	, IndexedSet_1_t507_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, IndexedSet_1_t507_RGCTXData/* rgctxDefinition */
	, 615/* fieldStart */

};
TypeInfo IndexedSet_1_t507_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IndexedSet`1"/* name */
	, "UnityEngine.UI.Collections"/* namespaze */
	, IndexedSet_1_t507_MethodInfos/* methods */
	, IndexedSet_1_t507_PropertyInfos/* properties */
	, NULL/* events */
	, &IndexedSet_1_t507_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 335/* custom_attributes_cache */
	, &IndexedSet_1_t507_0_0_0/* byval_arg */
	, &IndexedSet_1_t507_1_0_0/* this_arg */
	, &IndexedSet_1_t507_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &IndexedSet_1_t507_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 17/* method_count */
	, 3/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 4/* interfaces_count */
	, 4/* interface_offsets_count */

};
// UnityEngine.UI.CanvasListPool
#include "UnityEngine_UI_UnityEngine_UI_CanvasListPool.h"
// Metadata Definition UnityEngine.UI.CanvasListPool
extern TypeInfo CanvasListPool_t382_il2cpp_TypeInfo;
// UnityEngine.UI.CanvasListPool
#include "UnityEngine_UI_UnityEngine_UI_CanvasListPoolMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::.cctor()
extern const MethodInfo CanvasListPool__cctor_m1916_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&CanvasListPool__cctor_m1916/* method */
	, &CanvasListPool_t382_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1251/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t420_0_0_0;
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.Canvas> UnityEngine.UI.CanvasListPool::Get()
extern const MethodInfo CanvasListPool_Get_m1917_MethodInfo = 
{
	"Get"/* name */
	, (methodPointerType)&CanvasListPool_Get_m1917/* method */
	, &CanvasListPool_t382_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t420_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1252/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t420_0_0_0;
static const ParameterInfo CanvasListPool_t382_CanvasListPool_Release_m1918_ParameterInfos[] = 
{
	{"toRelease", 0, 134218442, 0, &List_1_t420_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::Release(System.Collections.Generic.List`1<UnityEngine.Canvas>)
extern const MethodInfo CanvasListPool_Release_m1918_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&CanvasListPool_Release_m1918/* method */
	, &CanvasListPool_t382_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, CanvasListPool_t382_CanvasListPool_Release_m1918_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1253/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t420_0_0_0;
static const ParameterInfo CanvasListPool_t382_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1919_ParameterInfos[] = 
{
	{"l", 0, 134218443, 0, &List_1_t420_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.CanvasListPool::<s_CanvasListPool>m__16(System.Collections.Generic.List`1<UnityEngine.Canvas>)
extern const MethodInfo CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1919_MethodInfo = 
{
	"<s_CanvasListPool>m__16"/* name */
	, (methodPointerType)&CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1919/* method */
	, &CanvasListPool_t382_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, CanvasListPool_t382_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1919_ParameterInfos/* parameters */
	, 337/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1254/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* CanvasListPool_t382_MethodInfos[] =
{
	&CanvasListPool__cctor_m1916_MethodInfo,
	&CanvasListPool_Get_m1917_MethodInfo,
	&CanvasListPool_Release_m1918_MethodInfo,
	&CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1919_MethodInfo,
	NULL
};
static const Il2CppMethodReference CanvasListPool_t382_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool CanvasListPool_t382_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType CanvasListPool_t382_0_0_0;
extern const Il2CppType CanvasListPool_t382_1_0_0;
struct CanvasListPool_t382;
const Il2CppTypeDefinitionMetadata CanvasListPool_t382_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, CanvasListPool_t382_VTable/* vtableMethods */
	, CanvasListPool_t382_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 617/* fieldStart */

};
TypeInfo CanvasListPool_t382_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "CanvasListPool"/* name */
	, "UnityEngine.UI"/* namespaze */
	, CanvasListPool_t382_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &CanvasListPool_t382_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &CanvasListPool_t382_0_0_0/* byval_arg */
	, &CanvasListPool_t382_1_0_0/* this_arg */
	, &CanvasListPool_t382_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CanvasListPool_t382)/* instance_size */
	, sizeof (CanvasListPool_t382)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(CanvasListPool_t382_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.ComponentListPool
#include "UnityEngine_UI_UnityEngine_UI_ComponentListPool.h"
// Metadata Definition UnityEngine.UI.ComponentListPool
extern TypeInfo ComponentListPool_t385_il2cpp_TypeInfo;
// UnityEngine.UI.ComponentListPool
#include "UnityEngine_UI_UnityEngine_UI_ComponentListPoolMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::.cctor()
extern const MethodInfo ComponentListPool__cctor_m1920_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&ComponentListPool__cctor_m1920/* method */
	, &ComponentListPool_t385_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1255/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Collections.Generic.List`1<UnityEngine.Component> UnityEngine.UI.ComponentListPool::Get()
extern const MethodInfo ComponentListPool_Get_m1921_MethodInfo = 
{
	"Get"/* name */
	, (methodPointerType)&ComponentListPool_Get_m1921/* method */
	, &ComponentListPool_t385_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t418_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1256/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t418_0_0_0;
static const ParameterInfo ComponentListPool_t385_ComponentListPool_Release_m1922_ParameterInfos[] = 
{
	{"toRelease", 0, 134218444, 0, &List_1_t418_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::Release(System.Collections.Generic.List`1<UnityEngine.Component>)
extern const MethodInfo ComponentListPool_Release_m1922_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&ComponentListPool_Release_m1922/* method */
	, &ComponentListPool_t385_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ComponentListPool_t385_ComponentListPool_Release_m1922_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1257/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t418_0_0_0;
static const ParameterInfo ComponentListPool_t385_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1923_ParameterInfos[] = 
{
	{"l", 0, 134218445, 0, &List_1_t418_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.ComponentListPool::<s_ComponentListPool>m__17(System.Collections.Generic.List`1<UnityEngine.Component>)
extern const MethodInfo ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1923_MethodInfo = 
{
	"<s_ComponentListPool>m__17"/* name */
	, (methodPointerType)&ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1923/* method */
	, &ComponentListPool_t385_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, ComponentListPool_t385_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1923_ParameterInfos/* parameters */
	, 339/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1258/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ComponentListPool_t385_MethodInfos[] =
{
	&ComponentListPool__cctor_m1920_MethodInfo,
	&ComponentListPool_Get_m1921_MethodInfo,
	&ComponentListPool_Release_m1922_MethodInfo,
	&ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1923_MethodInfo,
	NULL
};
static const Il2CppMethodReference ComponentListPool_t385_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ComponentListPool_t385_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ComponentListPool_t385_0_0_0;
extern const Il2CppType ComponentListPool_t385_1_0_0;
struct ComponentListPool_t385;
const Il2CppTypeDefinitionMetadata ComponentListPool_t385_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ComponentListPool_t385_VTable/* vtableMethods */
	, ComponentListPool_t385_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 619/* fieldStart */

};
TypeInfo ComponentListPool_t385_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComponentListPool"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ComponentListPool_t385_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &ComponentListPool_t385_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ComponentListPool_t385_0_0_0/* byval_arg */
	, &ComponentListPool_t385_1_0_0/* this_arg */
	, &ComponentListPool_t385_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComponentListPool_t385)/* instance_size */
	, sizeof (ComponentListPool_t385)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(ComponentListPool_t385_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048960/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.ObjectPool`1
extern TypeInfo ObjectPool_1_t509_il2cpp_TypeInfo;
extern const Il2CppGenericContainer ObjectPool_1_t509_Il2CppGenericContainer;
extern TypeInfo ObjectPool_1_t509_gp_T_0_il2cpp_TypeInfo;
extern const Il2CppGenericParameter ObjectPool_1_t509_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { &ObjectPool_1_t509_Il2CppGenericContainer, NULL, "T", 0, 16 };
static const Il2CppGenericParameter* ObjectPool_1_t509_Il2CppGenericParametersArray[1] = 
{
	&ObjectPool_1_t509_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern const Il2CppGenericContainer ObjectPool_1_t509_Il2CppGenericContainer = { { NULL, NULL }, NULL, (void*)&ObjectPool_1_t509_il2cpp_TypeInfo, 1, 0, ObjectPool_1_t509_Il2CppGenericParametersArray };
extern const Il2CppType UnityAction_1_t554_0_0_0;
extern const Il2CppType UnityAction_1_t554_0_0_0;
extern const Il2CppType UnityAction_1_t554_0_0_0;
static const ParameterInfo ObjectPool_1_t509_ObjectPool_1__ctor_m2540_ParameterInfos[] = 
{
	{"actionOnGet", 0, 134218446, 0, &UnityAction_1_t554_0_0_0},
	{"actionOnRelease", 1, 134218447, 0, &UnityAction_1_t554_0_0_0},
};
// System.Void UnityEngine.UI.ObjectPool`1::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern const MethodInfo ObjectPool_1__ctor_m2540_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &ObjectPool_1_t509_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t509_ObjectPool_1__ctor_m2540_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1259/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countAll()
extern const MethodInfo ObjectPool_1_get_countAll_m2541_MethodInfo = 
{
	"get_countAll"/* name */
	, NULL/* method */
	, &ObjectPool_1_t509_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 341/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1260/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Int32_t127_0_0_0;
static const ParameterInfo ObjectPool_1_t509_ObjectPool_1_set_countAll_m2542_ParameterInfos[] = 
{
	{"value", 0, 134218448, 0, &Int32_t127_0_0_0},
};
// System.Void UnityEngine.UI.ObjectPool`1::set_countAll(System.Int32)
extern const MethodInfo ObjectPool_1_set_countAll_m2542_MethodInfo = 
{
	"set_countAll"/* name */
	, NULL/* method */
	, &ObjectPool_1_t509_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t509_ObjectPool_1_set_countAll_m2542_ParameterInfos/* parameters */
	, 342/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1261/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countActive()
extern const MethodInfo ObjectPool_1_get_countActive_m2543_MethodInfo = 
{
	"get_countActive"/* name */
	, NULL/* method */
	, &ObjectPool_1_t509_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1262/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
// System.Int32 UnityEngine.UI.ObjectPool`1::get_countInactive()
extern const MethodInfo ObjectPool_1_get_countInactive_m2544_MethodInfo = 
{
	"get_countInactive"/* name */
	, NULL/* method */
	, &ObjectPool_1_t509_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t127_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1263/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectPool_1_t509_gp_0_0_0_0;
// T UnityEngine.UI.ObjectPool`1::Get()
extern const MethodInfo ObjectPool_1_Get_m2545_MethodInfo = 
{
	"Get"/* name */
	, NULL/* method */
	, &ObjectPool_1_t509_il2cpp_TypeInfo/* declaring_type */
	, &ObjectPool_1_t509_gp_0_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1264/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType ObjectPool_1_t509_gp_0_0_0_0;
static const ParameterInfo ObjectPool_1_t509_ObjectPool_1_Release_m2546_ParameterInfos[] = 
{
	{"element", 0, 134218449, 0, &ObjectPool_1_t509_gp_0_0_0_0},
};
// System.Void UnityEngine.UI.ObjectPool`1::Release(T)
extern const MethodInfo ObjectPool_1_Release_m2546_MethodInfo = 
{
	"Release"/* name */
	, NULL/* method */
	, &ObjectPool_1_t509_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, NULL/* invoker_method */
	, ObjectPool_1_t509_ObjectPool_1_Release_m2546_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1265/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* ObjectPool_1_t509_MethodInfos[] =
{
	&ObjectPool_1__ctor_m2540_MethodInfo,
	&ObjectPool_1_get_countAll_m2541_MethodInfo,
	&ObjectPool_1_set_countAll_m2542_MethodInfo,
	&ObjectPool_1_get_countActive_m2543_MethodInfo,
	&ObjectPool_1_get_countInactive_m2544_MethodInfo,
	&ObjectPool_1_Get_m2545_MethodInfo,
	&ObjectPool_1_Release_m2546_MethodInfo,
	NULL
};
extern const MethodInfo ObjectPool_1_get_countAll_m2541_MethodInfo;
extern const MethodInfo ObjectPool_1_set_countAll_m2542_MethodInfo;
static const PropertyInfo ObjectPool_1_t509____countAll_PropertyInfo = 
{
	&ObjectPool_1_t509_il2cpp_TypeInfo/* parent */
	, "countAll"/* name */
	, &ObjectPool_1_get_countAll_m2541_MethodInfo/* get */
	, &ObjectPool_1_set_countAll_m2542_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjectPool_1_get_countActive_m2543_MethodInfo;
static const PropertyInfo ObjectPool_1_t509____countActive_PropertyInfo = 
{
	&ObjectPool_1_t509_il2cpp_TypeInfo/* parent */
	, "countActive"/* name */
	, &ObjectPool_1_get_countActive_m2543_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo ObjectPool_1_get_countInactive_m2544_MethodInfo;
static const PropertyInfo ObjectPool_1_t509____countInactive_PropertyInfo = 
{
	&ObjectPool_1_t509_il2cpp_TypeInfo/* parent */
	, "countInactive"/* name */
	, &ObjectPool_1_get_countInactive_m2544_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* ObjectPool_1_t509_PropertyInfos[] =
{
	&ObjectPool_1_t509____countAll_PropertyInfo,
	&ObjectPool_1_t509____countActive_PropertyInfo,
	&ObjectPool_1_t509____countInactive_PropertyInfo,
	NULL
};
static const Il2CppMethodReference ObjectPool_1_t509_VTable[] =
{
	&Object_Equals_m540_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m541_MethodInfo,
	&Object_ToString_m542_MethodInfo,
};
static bool ObjectPool_1_t509_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
};
extern const Il2CppType Stack_1_t555_0_0_0;
extern const Il2CppGenericMethod Stack_1__ctor_m2604_GenericMethod;
extern const Il2CppGenericMethod ObjectPool_1_get_countAll_m2605_GenericMethod;
extern const Il2CppGenericMethod ObjectPool_1_get_countInactive_m2606_GenericMethod;
extern const Il2CppGenericMethod Stack_1_get_Count_m2607_GenericMethod;
extern const Il2CppGenericMethod Activator_CreateInstance_TisT_t553_m2608_GenericMethod;
extern const Il2CppGenericMethod ObjectPool_1_set_countAll_m2609_GenericMethod;
extern const Il2CppGenericMethod Stack_1_Pop_m2610_GenericMethod;
extern const Il2CppGenericMethod UnityAction_1_Invoke_m2611_GenericMethod;
extern const Il2CppGenericMethod Stack_1_Peek_m2612_GenericMethod;
extern const Il2CppGenericMethod Stack_1_Push_m2613_GenericMethod;
static Il2CppRGCTXDefinition ObjectPool_1_t509_RGCTXData[13] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&Stack_1_t555_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1__ctor_m2604_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_get_countAll_m2605_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_get_countInactive_m2606_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_get_Count_m2607_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_CLASS, (void*)&ObjectPool_1_t509_gp_0_0_0_0 }/* Class */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Activator_CreateInstance_TisT_t553_m2608_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &ObjectPool_1_set_countAll_m2609_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Pop_m2610_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &UnityAction_1_Invoke_m2611_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Peek_m2612_GenericMethod }/* Method */,
	{ IL2CPP_RGCTX_DATA_METHOD, &Stack_1_Push_m2613_GenericMethod }/* Method */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType ObjectPool_1_t509_0_0_0;
extern const Il2CppType ObjectPool_1_t509_1_0_0;
struct ObjectPool_1_t509;
const Il2CppTypeDefinitionMetadata ObjectPool_1_t509_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, &Object_t_0_0_0/* parent */
	, ObjectPool_1_t509_VTable/* vtableMethods */
	, ObjectPool_1_t509_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, ObjectPool_1_t509_RGCTXData/* rgctxDefinition */
	, 621/* fieldStart */

};
TypeInfo ObjectPool_1_t509_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectPool`1"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ObjectPool_1_t509_MethodInfos/* methods */
	, ObjectPool_1_t509_PropertyInfos/* properties */
	, NULL/* events */
	, &ObjectPool_1_t509_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &ObjectPool_1_t509_0_0_0/* byval_arg */
	, &ObjectPool_1_t509_1_0_0/* this_arg */
	, &ObjectPool_1_t509_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, &ObjectPool_1_t509_Il2CppGenericContainer/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, 0/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 7/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect.h"
// Metadata Definition UnityEngine.UI.BaseVertexEffect
extern TypeInfo BaseVertexEffect_t386_il2cpp_TypeInfo;
// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffectMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::.ctor()
extern const MethodInfo BaseVertexEffect__ctor_m1924_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseVertexEffect__ctor_m1924/* method */
	, &BaseVertexEffect_t386_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1266/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args);
// UnityEngine.UI.Graphic UnityEngine.UI.BaseVertexEffect::get_graphic()
extern const MethodInfo BaseVertexEffect_get_graphic_m1925_MethodInfo = 
{
	"get_graphic"/* name */
	, (methodPointerType)&BaseVertexEffect_get_graphic_m1925/* method */
	, &BaseVertexEffect_t386_il2cpp_TypeInfo/* declaring_type */
	, &Graphic_t278_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2180/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1267/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::OnEnable()
extern const MethodInfo BaseVertexEffect_OnEnable_m1926_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&BaseVertexEffect_OnEnable_m1926/* method */
	, &BaseVertexEffect_t386_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1268/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::OnDisable()
extern const MethodInfo BaseVertexEffect_OnDisable_m1927_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&BaseVertexEffect_OnDisable_m1927/* method */
	, &BaseVertexEffect_t386_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1269/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t315_0_0_0;
static const ParameterInfo BaseVertexEffect_t386_BaseVertexEffect_ModifyVertices_m2547_ParameterInfos[] = 
{
	{"verts", 0, 134218450, 0, &List_1_t315_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.BaseVertexEffect::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo BaseVertexEffect_ModifyVertices_m2547_MethodInfo = 
{
	"ModifyVertices"/* name */
	, NULL/* method */
	, &BaseVertexEffect_t386_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, BaseVertexEffect_t386_BaseVertexEffect_ModifyVertices_m2547_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1270/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* BaseVertexEffect_t386_MethodInfos[] =
{
	&BaseVertexEffect__ctor_m1924_MethodInfo,
	&BaseVertexEffect_get_graphic_m1925_MethodInfo,
	&BaseVertexEffect_OnEnable_m1926_MethodInfo,
	&BaseVertexEffect_OnDisable_m1927_MethodInfo,
	&BaseVertexEffect_ModifyVertices_m2547_MethodInfo,
	NULL
};
extern const MethodInfo BaseVertexEffect_get_graphic_m1925_MethodInfo;
static const PropertyInfo BaseVertexEffect_t386____graphic_PropertyInfo = 
{
	&BaseVertexEffect_t386_il2cpp_TypeInfo/* parent */
	, "graphic"/* name */
	, &BaseVertexEffect_get_graphic_m1925_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* BaseVertexEffect_t386_PropertyInfos[] =
{
	&BaseVertexEffect_t386____graphic_PropertyInfo,
	NULL
};
extern const MethodInfo BaseVertexEffect_OnEnable_m1926_MethodInfo;
extern const MethodInfo BaseVertexEffect_OnDisable_m1927_MethodInfo;
extern const MethodInfo BaseVertexEffect_ModifyVertices_m2547_MethodInfo;
static const Il2CppMethodReference BaseVertexEffect_t386_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&BaseVertexEffect_OnEnable_m1926_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&BaseVertexEffect_OnDisable_m1927_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m858_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m861_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&BaseVertexEffect_ModifyVertices_m2547_MethodInfo,
	NULL,
};
static bool BaseVertexEffect_t386_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
extern const Il2CppType IVertexModifier_t441_0_0_0;
static const Il2CppType* BaseVertexEffect_t386_InterfacesTypeInfos[] = 
{
	&IVertexModifier_t441_0_0_0,
};
static Il2CppInterfaceOffsetPair BaseVertexEffect_t386_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t441_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType BaseVertexEffect_t386_0_0_0;
extern const Il2CppType BaseVertexEffect_t386_1_0_0;
struct BaseVertexEffect_t386;
const Il2CppTypeDefinitionMetadata BaseVertexEffect_t386_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, BaseVertexEffect_t386_InterfacesTypeInfos/* implementedInterfaces */
	, BaseVertexEffect_t386_InterfacesOffsets/* interfaceOffsets */
	, &UIBehaviour_t199_0_0_0/* parent */
	, BaseVertexEffect_t386_VTable/* vtableMethods */
	, BaseVertexEffect_t386_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 625/* fieldStart */

};
TypeInfo BaseVertexEffect_t386_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseVertexEffect"/* name */
	, "UnityEngine.UI"/* namespaze */
	, BaseVertexEffect_t386_MethodInfos/* methods */
	, BaseVertexEffect_t386_PropertyInfos/* properties */
	, NULL/* events */
	, &BaseVertexEffect_t386_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 343/* custom_attributes_cache */
	, &BaseVertexEffect_t386_0_0_0/* byval_arg */
	, &BaseVertexEffect_t386_1_0_0/* this_arg */
	, &BaseVertexEffect_t386_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseVertexEffect_t386)/* instance_size */
	, sizeof (BaseVertexEffect_t386)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 5/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Metadata Definition UnityEngine.UI.IVertexModifier
extern TypeInfo IVertexModifier_t441_il2cpp_TypeInfo;
extern const Il2CppType List_1_t315_0_0_0;
static const ParameterInfo IVertexModifier_t441_IVertexModifier_ModifyVertices_m2548_ParameterInfos[] = 
{
	{"verts", 0, 134218451, 0, &List_1_t315_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.IVertexModifier::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo IVertexModifier_ModifyVertices_m2548_MethodInfo = 
{
	"ModifyVertices"/* name */
	, NULL/* method */
	, &IVertexModifier_t441_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, IVertexModifier_t441_IVertexModifier_ModifyVertices_m2548_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1271/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* IVertexModifier_t441_MethodInfos[] =
{
	&IVertexModifier_ModifyVertices_m2548_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType IVertexModifier_t441_1_0_0;
struct IVertexModifier_t441;
const Il2CppTypeDefinitionMetadata IVertexModifier_t441_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, NULL/* interfaceOffsets */
	, NULL/* parent */
	, NULL/* vtableMethods */
	, NULL/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo IVertexModifier_t441_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "IVertexModifier"/* name */
	, "UnityEngine.UI"/* namespaze */
	, IVertexModifier_t441_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &IVertexModifier_t441_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 0/* custom_attributes_cache */
	, &IVertexModifier_t441_0_0_0/* byval_arg */
	, &IVertexModifier_t441_1_0_0/* this_arg */
	, &IVertexModifier_t441_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_Outline.h"
// Metadata Definition UnityEngine.UI.Outline
extern TypeInfo Outline_t387_il2cpp_TypeInfo;
// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_OutlineMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Outline::.ctor()
extern const MethodInfo Outline__ctor_m1928_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Outline__ctor_m1928/* method */
	, &Outline_t387_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1272/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t315_0_0_0;
static const ParameterInfo Outline_t387_Outline_ModifyVertices_m1929_ParameterInfos[] = 
{
	{"verts", 0, 134218452, 0, &List_1_t315_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Outline::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo Outline_ModifyVertices_m1929_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&Outline_ModifyVertices_m1929/* method */
	, &Outline_t387_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Outline_t387_Outline_ModifyVertices_m1929_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1273/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Outline_t387_MethodInfos[] =
{
	&Outline__ctor_m1928_MethodInfo,
	&Outline_ModifyVertices_m1929_MethodInfo,
	NULL
};
extern const MethodInfo Outline_ModifyVertices_m1929_MethodInfo;
static const Il2CppMethodReference Outline_t387_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&BaseVertexEffect_OnEnable_m1926_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&BaseVertexEffect_OnDisable_m1927_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m858_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m861_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&Outline_ModifyVertices_m1929_MethodInfo,
	&Outline_ModifyVertices_m1929_MethodInfo,
};
static bool Outline_t387_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Outline_t387_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t441_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Outline_t387_0_0_0;
extern const Il2CppType Outline_t387_1_0_0;
extern const Il2CppType Shadow_t388_0_0_0;
struct Outline_t387;
const Il2CppTypeDefinitionMetadata Outline_t387_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Outline_t387_InterfacesOffsets/* interfaceOffsets */
	, &Shadow_t388_0_0_0/* parent */
	, Outline_t387_VTable/* vtableMethods */
	, Outline_t387_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo Outline_t387_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Outline"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Outline_t387_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &Outline_t387_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 344/* custom_attributes_cache */
	, &Outline_t387_0_0_0/* byval_arg */
	, &Outline_t387_1_0_0/* this_arg */
	, &Outline_t387_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Outline_t387)/* instance_size */
	, sizeof (Outline_t387)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1.h"
// Metadata Definition UnityEngine.UI.PositionAsUV1
extern TypeInfo PositionAsUV1_t389_il2cpp_TypeInfo;
// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1MethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.PositionAsUV1::.ctor()
extern const MethodInfo PositionAsUV1__ctor_m1930_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PositionAsUV1__ctor_m1930/* method */
	, &PositionAsUV1_t389_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1274/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t315_0_0_0;
static const ParameterInfo PositionAsUV1_t389_PositionAsUV1_ModifyVertices_m1931_ParameterInfos[] = 
{
	{"verts", 0, 134218453, 0, &List_1_t315_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.PositionAsUV1::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo PositionAsUV1_ModifyVertices_m1931_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&PositionAsUV1_ModifyVertices_m1931/* method */
	, &PositionAsUV1_t389_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, PositionAsUV1_t389_PositionAsUV1_ModifyVertices_m1931_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1275/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* PositionAsUV1_t389_MethodInfos[] =
{
	&PositionAsUV1__ctor_m1930_MethodInfo,
	&PositionAsUV1_ModifyVertices_m1931_MethodInfo,
	NULL
};
extern const MethodInfo PositionAsUV1_ModifyVertices_m1931_MethodInfo;
static const Il2CppMethodReference PositionAsUV1_t389_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&BaseVertexEffect_OnEnable_m1926_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&BaseVertexEffect_OnDisable_m1927_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m858_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m861_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&PositionAsUV1_ModifyVertices_m1931_MethodInfo,
	&PositionAsUV1_ModifyVertices_m1931_MethodInfo,
};
static bool PositionAsUV1_t389_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair PositionAsUV1_t389_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t441_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType PositionAsUV1_t389_0_0_0;
extern const Il2CppType PositionAsUV1_t389_1_0_0;
struct PositionAsUV1_t389;
const Il2CppTypeDefinitionMetadata PositionAsUV1_t389_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, PositionAsUV1_t389_InterfacesOffsets/* interfaceOffsets */
	, &BaseVertexEffect_t386_0_0_0/* parent */
	, PositionAsUV1_t389_VTable/* vtableMethods */
	, PositionAsUV1_t389_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, -1/* fieldStart */

};
TypeInfo PositionAsUV1_t389_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "PositionAsUV1"/* name */
	, "UnityEngine.UI"/* namespaze */
	, PositionAsUV1_t389_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* events */
	, &PositionAsUV1_t389_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 345/* custom_attributes_cache */
	, &PositionAsUV1_t389_0_0_0/* byval_arg */
	, &PositionAsUV1_t389_1_0_0/* this_arg */
	, &PositionAsUV1_t389_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PositionAsUV1_t389)/* instance_size */
	, sizeof (PositionAsUV1_t389)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_Shadow.h"
// Metadata Definition UnityEngine.UI.Shadow
extern TypeInfo Shadow_t388_il2cpp_TypeInfo;
// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_ShadowMethodDeclarations.h"
extern void* RuntimeInvoker_Void_t168 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::.ctor()
extern const MethodInfo Shadow__ctor_m1932_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Shadow__ctor_m1932/* method */
	, &Shadow_t388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1276/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Color_t90 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Color UnityEngine.UI.Shadow::get_effectColor()
extern const MethodInfo Shadow_get_effectColor_m1933_MethodInfo = 
{
	"get_effectColor"/* name */
	, (methodPointerType)&Shadow_get_effectColor_m1933/* method */
	, &Shadow_t388_il2cpp_TypeInfo/* declaring_type */
	, &Color_t90_0_0_0/* return_type */
	, RuntimeInvoker_Color_t90/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1277/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Color_t90_0_0_0;
static const ParameterInfo Shadow_t388_Shadow_set_effectColor_m1934_ParameterInfos[] = 
{
	{"value", 0, 134218454, 0, &Color_t90_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Color_t90 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_effectColor(UnityEngine.Color)
extern const MethodInfo Shadow_set_effectColor_m1934_MethodInfo = 
{
	"set_effectColor"/* name */
	, (methodPointerType)&Shadow_set_effectColor_m1934/* method */
	, &Shadow_t388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Color_t90/* invoker_method */
	, Shadow_t388_Shadow_set_effectColor_m1934_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1278/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 UnityEngine.UI.Shadow::get_effectDistance()
extern const MethodInfo Shadow_get_effectDistance_m1935_MethodInfo = 
{
	"get_effectDistance"/* name */
	, (methodPointerType)&Shadow_get_effectDistance_m1935/* method */
	, &Shadow_t388_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t10_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t10/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1279/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Vector2_t10_0_0_0;
static const ParameterInfo Shadow_t388_Shadow_set_effectDistance_m1936_ParameterInfos[] = 
{
	{"value", 0, 134218455, 0, &Vector2_t10_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Vector2_t10 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_effectDistance(UnityEngine.Vector2)
extern const MethodInfo Shadow_set_effectDistance_m1936_MethodInfo = 
{
	"set_effectDistance"/* name */
	, (methodPointerType)&Shadow_set_effectDistance_m1936/* method */
	, &Shadow_t388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Vector2_t10/* invoker_method */
	, Shadow_t388_Shadow_set_effectDistance_m1936_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1280/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern void* RuntimeInvoker_Boolean_t169 (const MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.UI.Shadow::get_useGraphicAlpha()
extern const MethodInfo Shadow_get_useGraphicAlpha_m1937_MethodInfo = 
{
	"get_useGraphicAlpha"/* name */
	, (methodPointerType)&Shadow_get_useGraphicAlpha_m1937/* method */
	, &Shadow_t388_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t169_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t169/* invoker_method */
	, NULL/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1281/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType Boolean_t169_0_0_0;
static const ParameterInfo Shadow_t388_Shadow_set_useGraphicAlpha_m1938_ParameterInfos[] = 
{
	{"value", 0, 134218456, 0, &Boolean_t169_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_SByte_t170 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::set_useGraphicAlpha(System.Boolean)
extern const MethodInfo Shadow_set_useGraphicAlpha_m1938_MethodInfo = 
{
	"set_useGraphicAlpha"/* name */
	, (methodPointerType)&Shadow_set_useGraphicAlpha_m1938/* method */
	, &Shadow_t388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_SByte_t170/* invoker_method */
	, Shadow_t388_Shadow_set_useGraphicAlpha_m1938_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1282/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t315_0_0_0;
extern const Il2CppType Color32_t421_0_0_0;
extern const Il2CppType Color32_t421_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Int32_t127_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
extern const Il2CppType Single_t151_0_0_0;
static const ParameterInfo Shadow_t388_Shadow_ApplyShadow_m1939_ParameterInfos[] = 
{
	{"verts", 0, 134218457, 0, &List_1_t315_0_0_0},
	{"color", 1, 134218458, 0, &Color32_t421_0_0_0},
	{"start", 2, 134218459, 0, &Int32_t127_0_0_0},
	{"end", 3, 134218460, 0, &Int32_t127_0_0_0},
	{"x", 4, 134218461, 0, &Single_t151_0_0_0},
	{"y", 5, 134218462, 0, &Single_t151_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t_Color32_t421_Int32_t127_Int32_t127_Single_t151_Single_t151 (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::ApplyShadow(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color32,System.Int32,System.Int32,System.Single,System.Single)
extern const MethodInfo Shadow_ApplyShadow_m1939_MethodInfo = 
{
	"ApplyShadow"/* name */
	, (methodPointerType)&Shadow_ApplyShadow_m1939/* method */
	, &Shadow_t388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t_Color32_t421_Int32_t127_Int32_t127_Single_t151_Single_t151/* invoker_method */
	, Shadow_t388_Shadow_ApplyShadow_m1939_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1283/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern const Il2CppType List_1_t315_0_0_0;
static const ParameterInfo Shadow_t388_Shadow_ModifyVertices_m1940_ParameterInfos[] = 
{
	{"verts", 0, 134218463, 0, &List_1_t315_0_0_0},
};
extern void* RuntimeInvoker_Void_t168_Object_t (const MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UI.Shadow::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern const MethodInfo Shadow_ModifyVertices_m1940_MethodInfo = 
{
	"ModifyVertices"/* name */
	, (methodPointerType)&Shadow_ModifyVertices_m1940/* method */
	, &Shadow_t388_il2cpp_TypeInfo/* declaring_type */
	, &Void_t168_0_0_0/* return_type */
	, RuntimeInvoker_Void_t168_Object_t/* invoker_method */
	, Shadow_t388_Shadow_ModifyVertices_m1940_ParameterInfos/* parameters */
	, 0/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1284/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static const MethodInfo* Shadow_t388_MethodInfos[] =
{
	&Shadow__ctor_m1932_MethodInfo,
	&Shadow_get_effectColor_m1933_MethodInfo,
	&Shadow_set_effectColor_m1934_MethodInfo,
	&Shadow_get_effectDistance_m1935_MethodInfo,
	&Shadow_set_effectDistance_m1936_MethodInfo,
	&Shadow_get_useGraphicAlpha_m1937_MethodInfo,
	&Shadow_set_useGraphicAlpha_m1938_MethodInfo,
	&Shadow_ApplyShadow_m1939_MethodInfo,
	&Shadow_ModifyVertices_m1940_MethodInfo,
	NULL
};
extern const MethodInfo Shadow_get_effectColor_m1933_MethodInfo;
extern const MethodInfo Shadow_set_effectColor_m1934_MethodInfo;
static const PropertyInfo Shadow_t388____effectColor_PropertyInfo = 
{
	&Shadow_t388_il2cpp_TypeInfo/* parent */
	, "effectColor"/* name */
	, &Shadow_get_effectColor_m1933_MethodInfo/* get */
	, &Shadow_set_effectColor_m1934_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Shadow_get_effectDistance_m1935_MethodInfo;
extern const MethodInfo Shadow_set_effectDistance_m1936_MethodInfo;
static const PropertyInfo Shadow_t388____effectDistance_PropertyInfo = 
{
	&Shadow_t388_il2cpp_TypeInfo/* parent */
	, "effectDistance"/* name */
	, &Shadow_get_effectDistance_m1935_MethodInfo/* get */
	, &Shadow_set_effectDistance_m1936_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
extern const MethodInfo Shadow_get_useGraphicAlpha_m1937_MethodInfo;
extern const MethodInfo Shadow_set_useGraphicAlpha_m1938_MethodInfo;
static const PropertyInfo Shadow_t388____useGraphicAlpha_PropertyInfo = 
{
	&Shadow_t388_il2cpp_TypeInfo/* parent */
	, "useGraphicAlpha"/* name */
	, &Shadow_get_useGraphicAlpha_m1937_MethodInfo/* get */
	, &Shadow_set_useGraphicAlpha_m1938_MethodInfo/* set */
	, 0/* attrs */
	, 0/* custom_attributes_cache */

};
static const PropertyInfo* Shadow_t388_PropertyInfos[] =
{
	&Shadow_t388____effectColor_PropertyInfo,
	&Shadow_t388____effectDistance_PropertyInfo,
	&Shadow_t388____useGraphicAlpha_PropertyInfo,
	NULL
};
extern const MethodInfo Shadow_ModifyVertices_m1940_MethodInfo;
static const Il2CppMethodReference Shadow_t388_VTable[] =
{
	&Object_Equals_m537_MethodInfo,
	&Object_Finalize_m515_MethodInfo,
	&Object_GetHashCode_m538_MethodInfo,
	&Object_ToString_m539_MethodInfo,
	&UIBehaviour_Awake_m852_MethodInfo,
	&BaseVertexEffect_OnEnable_m1926_MethodInfo,
	&UIBehaviour_Start_m854_MethodInfo,
	&BaseVertexEffect_OnDisable_m1927_MethodInfo,
	&UIBehaviour_OnDestroy_m856_MethodInfo,
	&UIBehaviour_IsActive_m857_MethodInfo,
	&UIBehaviour_OnRectTransformDimensionsChange_m858_MethodInfo,
	&UIBehaviour_OnBeforeTransformParentChanged_m859_MethodInfo,
	&UIBehaviour_OnTransformParentChanged_m860_MethodInfo,
	&UIBehaviour_OnDidApplyAnimationProperties_m861_MethodInfo,
	&UIBehaviour_OnCanvasGroupChanged_m862_MethodInfo,
	&UIBehaviour_OnCanvasHierarchyChanged_m863_MethodInfo,
	&Shadow_ModifyVertices_m1940_MethodInfo,
	&Shadow_ModifyVertices_m1940_MethodInfo,
};
static bool Shadow_t388_VTableIsGenericMethod[] =
{
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
	false,
};
static Il2CppInterfaceOffsetPair Shadow_t388_InterfacesOffsets[] = 
{
	{ &IVertexModifier_t441_0_0_0, 16},
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern const Il2CppType Shadow_t388_1_0_0;
struct Shadow_t388;
const Il2CppTypeDefinitionMetadata Shadow_t388_DefinitionMetadata = 
{
	NULL/* declaringType */
	, NULL/* nestedTypes */
	, NULL/* implementedInterfaces */
	, Shadow_t388_InterfacesOffsets/* interfaceOffsets */
	, &BaseVertexEffect_t386_0_0_0/* parent */
	, Shadow_t388_VTable/* vtableMethods */
	, Shadow_t388_VTableIsGenericMethod/* vtableEntryIsGenericMethod */
	, NULL/* rgctxDefinition */
	, 626/* fieldStart */

};
TypeInfo Shadow_t388_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "Shadow"/* name */
	, "UnityEngine.UI"/* namespaze */
	, Shadow_t388_MethodInfos/* methods */
	, Shadow_t388_PropertyInfos/* properties */
	, NULL/* events */
	, &Shadow_t388_il2cpp_TypeInfo/* element_class */
	, NULL/* vtable */
	, 346/* custom_attributes_cache */
	, &Shadow_t388_0_0_0/* byval_arg */
	, &Shadow_t388_1_0_0/* this_arg */
	, &Shadow_t388_DefinitionMetadata/* definitionMetadata */
	, NULL/* runtimeMetadata */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Shadow_t388)/* instance_size */
	, sizeof (Shadow_t388)/* actualSize */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, 0/* minimumAlignment */
	, false/* valuetype */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, 9/* method_count */
	, 3/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
