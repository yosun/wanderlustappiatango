﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t1202;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t616;
// System.Collections.Generic.IEnumerable`1<System.Byte[]>
struct IEnumerable_1_t4322;
// System.Collections.Generic.IEnumerator`1<System.Byte[]>
struct IEnumerator_1_t4323;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t410;
// System.Collections.Generic.ICollection`1<System.Byte[]>
struct ICollection_1_t4324;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Byte[]>
struct ReadOnlyCollection_1_t3743;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t1822;
// System.Predicate`1<System.Byte[]>
struct Predicate_1_t3744;
// System.Comparison`1<System.Byte[]>
struct Comparison_1_t3746;
// System.Collections.Generic.List`1/Enumerator<System.Byte[]>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_55.h"

// System.Void System.Collections.Generic.List`1<System.Byte[]>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m6944(__this, method) (( void (*) (List_1_t1202 *, const MethodInfo*))List_1__ctor_m6974_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m24659(__this, ___collection, method) (( void (*) (List_1_t1202 *, Object_t*, const MethodInfo*))List_1__ctor_m14980_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::.ctor(System.Int32)
#define List_1__ctor_m24660(__this, ___capacity, method) (( void (*) (List_1_t1202 *, int32_t, const MethodInfo*))List_1__ctor_m14982_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::.cctor()
#define List_1__cctor_m24661(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m14984_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Byte[]>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24662(__this, method) (( Object_t* (*) (List_1_t1202 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7209_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m24663(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1202 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7192_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m24664(__this, method) (( Object_t * (*) (List_1_t1202 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7188_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m24665(__this, ___item, method) (( int32_t (*) (List_1_t1202 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m24666(__this, ___item, method) (( bool (*) (List_1_t1202 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7199_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m24667(__this, ___item, method) (( int32_t (*) (List_1_t1202 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7200_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m24668(__this, ___index, ___item, method) (( void (*) (List_1_t1202 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7201_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m24669(__this, ___item, method) (( void (*) (List_1_t1202 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7202_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte[]>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24670(__this, method) (( bool (*) (List_1_t1202 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte[]>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m24671(__this, method) (( bool (*) (List_1_t1202 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7190_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Byte[]>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m24672(__this, method) (( Object_t * (*) (List_1_t1202 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m24673(__this, method) (( bool (*) (List_1_t1202 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7193_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m24674(__this, method) (( bool (*) (List_1_t1202 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7194_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m24675(__this, ___index, method) (( Object_t * (*) (List_1_t1202 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7195_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m24676(__this, ___index, ___value, method) (( void (*) (List_1_t1202 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7196_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Add(T)
#define List_1_Add_m24677(__this, ___item, method) (( void (*) (List_1_t1202 *, ByteU5BU5D_t616*, const MethodInfo*))List_1_Add_m7205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m24678(__this, ___newCount, method) (( void (*) (List_1_t1202 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15002_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m24679(__this, ___collection, method) (( void (*) (List_1_t1202 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15004_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m24680(__this, ___enumerable, method) (( void (*) (List_1_t1202 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15006_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m24681(__this, ___collection, method) (( void (*) (List_1_t1202 *, Object_t*, const MethodInfo*))List_1_AddRange_m15007_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Byte[]>::AsReadOnly()
#define List_1_AsReadOnly_m24682(__this, method) (( ReadOnlyCollection_1_t3743 * (*) (List_1_t1202 *, const MethodInfo*))List_1_AsReadOnly_m15009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Clear()
#define List_1_Clear_m24683(__this, method) (( void (*) (List_1_t1202 *, const MethodInfo*))List_1_Clear_m7198_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte[]>::Contains(T)
#define List_1_Contains_m24684(__this, ___item, method) (( bool (*) (List_1_t1202 *, ByteU5BU5D_t616*, const MethodInfo*))List_1_Contains_m7206_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m24685(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1202 *, ByteU5BU5DU5BU5D_t1822*, int32_t, const MethodInfo*))List_1_CopyTo_m7207_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Byte[]>::Find(System.Predicate`1<T>)
#define List_1_Find_m24686(__this, ___match, method) (( ByteU5BU5D_t616* (*) (List_1_t1202 *, Predicate_1_t3744 *, const MethodInfo*))List_1_Find_m15014_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m24687(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3744 *, const MethodInfo*))List_1_CheckMatch_m15016_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte[]>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m24688(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1202 *, int32_t, int32_t, Predicate_1_t3744 *, const MethodInfo*))List_1_GetIndex_m15018_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Byte[]>::GetEnumerator()
#define List_1_GetEnumerator_m24689(__this, method) (( Enumerator_t3745  (*) (List_1_t1202 *, const MethodInfo*))List_1_GetEnumerator_m15019_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte[]>::IndexOf(T)
#define List_1_IndexOf_m24690(__this, ___item, method) (( int32_t (*) (List_1_t1202 *, ByteU5BU5D_t616*, const MethodInfo*))List_1_IndexOf_m7210_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m24691(__this, ___start, ___delta, method) (( void (*) (List_1_t1202 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15022_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m24692(__this, ___index, method) (( void (*) (List_1_t1202 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15024_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Insert(System.Int32,T)
#define List_1_Insert_m24693(__this, ___index, ___item, method) (( void (*) (List_1_t1202 *, int32_t, ByteU5BU5D_t616*, const MethodInfo*))List_1_Insert_m7211_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m24694(__this, ___collection, method) (( void (*) (List_1_t1202 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15027_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte[]>::Remove(T)
#define List_1_Remove_m24695(__this, ___item, method) (( bool (*) (List_1_t1202 *, ByteU5BU5D_t616*, const MethodInfo*))List_1_Remove_m7208_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte[]>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m24696(__this, ___match, method) (( int32_t (*) (List_1_t1202 *, Predicate_1_t3744 *, const MethodInfo*))List_1_RemoveAll_m15030_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m24697(__this, ___index, method) (( void (*) (List_1_t1202 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7203_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Reverse()
#define List_1_Reverse_m24698(__this, method) (( void (*) (List_1_t1202 *, const MethodInfo*))List_1_Reverse_m15033_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Sort()
#define List_1_Sort_m24699(__this, method) (( void (*) (List_1_t1202 *, const MethodInfo*))List_1_Sort_m15035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m24700(__this, ___comparison, method) (( void (*) (List_1_t1202 *, Comparison_1_t3746 *, const MethodInfo*))List_1_Sort_m15037_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Byte[]>::ToArray()
#define List_1_ToArray_m24701(__this, method) (( ByteU5BU5DU5BU5D_t1822* (*) (List_1_t1202 *, const MethodInfo*))List_1_ToArray_m15039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::TrimExcess()
#define List_1_TrimExcess_m24702(__this, method) (( void (*) (List_1_t1202 *, const MethodInfo*))List_1_TrimExcess_m15041_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte[]>::get_Capacity()
#define List_1_get_Capacity_m24703(__this, method) (( int32_t (*) (List_1_t1202 *, const MethodInfo*))List_1_get_Capacity_m15043_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m24704(__this, ___value, method) (( void (*) (List_1_t1202 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15045_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte[]>::get_Count()
#define List_1_get_Count_m24705(__this, method) (( int32_t (*) (List_1_t1202 *, const MethodInfo*))List_1_get_Count_m7189_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Byte[]>::get_Item(System.Int32)
#define List_1_get_Item_m24706(__this, ___index, method) (( ByteU5BU5D_t616* (*) (List_1_t1202 *, int32_t, const MethodInfo*))List_1_get_Item_m7212_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::set_Item(System.Int32,T)
#define List_1_set_Item_m24707(__this, ___index, ___value, method) (( void (*) (List_1_t1202 *, int32_t, ByteU5BU5D_t616*, const MethodInfo*))List_1_set_Item_m7213_gshared)(__this, ___index, ___value, method)
