﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.Prop>
struct Predicate_1_t3534;
// System.Object
struct Object_t;
// Vuforia.Prop
struct Prop_t97;
// System.IAsyncResult
struct IAsyncResult_t304;
// System.AsyncCallback
struct AsyncCallback_t305;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<Vuforia.Prop>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m21569(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3534 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m15134_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.Prop>::Invoke(T)
#define Predicate_1_Invoke_m21570(__this, ___obj, method) (( bool (*) (Predicate_1_t3534 *, Object_t *, const MethodInfo*))Predicate_1_Invoke_m15135_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.Prop>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m21571(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3534 *, Object_t *, AsyncCallback_t305 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m15136_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.Prop>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m21572(__this, ___result, method) (( bool (*) (Predicate_1_t3534 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m15137_gshared)(__this, ___result, method)
