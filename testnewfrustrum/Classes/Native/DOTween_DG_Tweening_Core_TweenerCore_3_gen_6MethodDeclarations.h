﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct TweenerCore_3_t1034;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::.ctor()
extern "C" void TweenerCore_3__ctor_m23659_gshared (TweenerCore_3_t1034 * __this, const MethodInfo* method);
#define TweenerCore_3__ctor_m23659(__this, method) (( void (*) (TweenerCore_3_t1034 *, const MethodInfo*))TweenerCore_3__ctor_m23659_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset()
extern "C" void TweenerCore_3_Reset_m23660_gshared (TweenerCore_3_t1034 * __this, const MethodInfo* method);
#define TweenerCore_3_Reset_m23660(__this, method) (( void (*) (TweenerCore_3_t1034 *, const MethodInfo*))TweenerCore_3_Reset_m23660_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Validate()
extern "C" bool TweenerCore_3_Validate_m23661_gshared (TweenerCore_3_t1034 * __this, const MethodInfo* method);
#define TweenerCore_3_Validate_m23661(__this, method) (( bool (*) (TweenerCore_3_t1034 *, const MethodInfo*))TweenerCore_3_Validate_m23661_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23662_gshared (TweenerCore_3_t1034 * __this, float ___elapsed, const MethodInfo* method);
#define TweenerCore_3_UpdateDelay_m23662(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1034 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m23662_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23663_gshared (TweenerCore_3_t1034 * __this, const MethodInfo* method);
#define TweenerCore_3_Startup_m23663(__this, method) (( bool (*) (TweenerCore_3_t1034 *, const MethodInfo*))TweenerCore_3_Startup_m23663_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool TweenerCore_3_ApplyTween_m23664_gshared (TweenerCore_3_t1034 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method);
#define TweenerCore_3_ApplyTween_m23664(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1034 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m23664_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
