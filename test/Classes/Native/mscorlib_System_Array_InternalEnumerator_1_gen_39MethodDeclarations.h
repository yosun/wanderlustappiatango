﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>
struct InternalEnumerator_1_t2781;
// System.Object
struct Object_t;
// Vuforia.CloudRecoAbstractBehaviour
struct CloudRecoAbstractBehaviour_t32;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m14307(__this, ___array, method) (void)InternalEnumerator_1__ctor_m13978_gshared((InternalEnumerator_1_t2698 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14308(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m14309(__this, method) (void)InternalEnumerator_1_Dispose_m13982_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m14310(__this, method) (bool)InternalEnumerator_1_MoveNext_m13984_gshared((InternalEnumerator_1_t2698 *)__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m14311(__this, method) (CloudRecoAbstractBehaviour_t32 *)InternalEnumerator_1_get_Current_m13986_gshared((InternalEnumerator_1_t2698 *)__this, method)
