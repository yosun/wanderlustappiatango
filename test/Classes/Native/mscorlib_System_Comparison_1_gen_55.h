﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1075;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Events.BaseInvokableCall>
struct Comparison_1_t4814  : public MulticastDelegate_t325
{
};
