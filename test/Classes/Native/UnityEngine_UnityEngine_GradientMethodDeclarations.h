﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Gradient
struct Gradient_t949;
struct Gradient_t949_marshaled;

// System.Void UnityEngine.Gradient::.ctor()
 void Gradient__ctor_m5572 (Gradient_t949 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
 void Gradient_Init_m5573 (Gradient_t949 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
 void Gradient_Cleanup_m5574 (Gradient_t949 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Finalize()
 void Gradient_Finalize_m5575 (Gradient_t949 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void Gradient_t949_marshal(const Gradient_t949& unmarshaled, Gradient_t949_marshaled& marshaled);
void Gradient_t949_marshal_back(const Gradient_t949_marshaled& marshaled, Gradient_t949& unmarshaled);
void Gradient_t949_marshal_cleanup(Gradient_t949_marshaled& marshaled);
