﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t4942;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t455;
// System.Exception
struct Exception_t152;

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
 void ArrayReadOnlyList_1__ctor_m30291_gshared (ArrayReadOnlyList_1_t4942 * __this, ObjectU5BU5D_t115* ___array, MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m30291(__this, ___array, method) (void)ArrayReadOnlyList_1__ctor_m30291_gshared((ArrayReadOnlyList_1_t4942 *)__this, (ObjectU5BU5D_t115*)___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m30292_gshared (ArrayReadOnlyList_1_t4942 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m30292(__this, method) (Object_t *)ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m30292_gshared((ArrayReadOnlyList_1_t4942 *)__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
 Object_t * ArrayReadOnlyList_1_get_Item_m30293_gshared (ArrayReadOnlyList_1_t4942 * __this, int32_t ___index, MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m30293(__this, ___index, method) (Object_t *)ArrayReadOnlyList_1_get_Item_m30293_gshared((ArrayReadOnlyList_1_t4942 *)__this, (int32_t)___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
 void ArrayReadOnlyList_1_set_Item_m30294_gshared (ArrayReadOnlyList_1_t4942 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m30294(__this, ___index, ___value, method) (void)ArrayReadOnlyList_1_set_Item_m30294_gshared((ArrayReadOnlyList_1_t4942 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
 int32_t ArrayReadOnlyList_1_get_Count_m30295_gshared (ArrayReadOnlyList_1_t4942 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m30295(__this, method) (int32_t)ArrayReadOnlyList_1_get_Count_m30295_gshared((ArrayReadOnlyList_1_t4942 *)__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
 bool ArrayReadOnlyList_1_get_IsReadOnly_m30296_gshared (ArrayReadOnlyList_1_t4942 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m30296(__this, method) (bool)ArrayReadOnlyList_1_get_IsReadOnly_m30296_gshared((ArrayReadOnlyList_1_t4942 *)__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
 void ArrayReadOnlyList_1_Add_m30297_gshared (ArrayReadOnlyList_1_t4942 * __this, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m30297(__this, ___item, method) (void)ArrayReadOnlyList_1_Add_m30297_gshared((ArrayReadOnlyList_1_t4942 *)__this, (Object_t *)___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
 void ArrayReadOnlyList_1_Clear_m30298_gshared (ArrayReadOnlyList_1_t4942 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m30298(__this, method) (void)ArrayReadOnlyList_1_Clear_m30298_gshared((ArrayReadOnlyList_1_t4942 *)__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
 bool ArrayReadOnlyList_1_Contains_m30299_gshared (ArrayReadOnlyList_1_t4942 * __this, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m30299(__this, ___item, method) (bool)ArrayReadOnlyList_1_Contains_m30299_gshared((ArrayReadOnlyList_1_t4942 *)__this, (Object_t *)___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
 void ArrayReadOnlyList_1_CopyTo_m30300_gshared (ArrayReadOnlyList_1_t4942 * __this, ObjectU5BU5D_t115* ___array, int32_t ___index, MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m30300(__this, ___array, ___index, method) (void)ArrayReadOnlyList_1_CopyTo_m30300_gshared((ArrayReadOnlyList_1_t4942 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
 Object_t* ArrayReadOnlyList_1_GetEnumerator_m30301_gshared (ArrayReadOnlyList_1_t4942 * __this, MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m30301(__this, method) (Object_t*)ArrayReadOnlyList_1_GetEnumerator_m30301_gshared((ArrayReadOnlyList_1_t4942 *)__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
 int32_t ArrayReadOnlyList_1_IndexOf_m30302_gshared (ArrayReadOnlyList_1_t4942 * __this, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m30302(__this, ___item, method) (int32_t)ArrayReadOnlyList_1_IndexOf_m30302_gshared((ArrayReadOnlyList_1_t4942 *)__this, (Object_t *)___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
 void ArrayReadOnlyList_1_Insert_m30303_gshared (ArrayReadOnlyList_1_t4942 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m30303(__this, ___index, ___item, method) (void)ArrayReadOnlyList_1_Insert_m30303_gshared((ArrayReadOnlyList_1_t4942 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
 bool ArrayReadOnlyList_1_Remove_m30304_gshared (ArrayReadOnlyList_1_t4942 * __this, Object_t * ___item, MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m30304(__this, ___item, method) (bool)ArrayReadOnlyList_1_Remove_m30304_gshared((ArrayReadOnlyList_1_t4942 *)__this, (Object_t *)___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
 void ArrayReadOnlyList_1_RemoveAt_m30305_gshared (ArrayReadOnlyList_1_t4942 * __this, int32_t ___index, MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m30305(__this, ___index, method) (void)ArrayReadOnlyList_1_RemoveAt_m30305_gshared((ArrayReadOnlyList_1_t4942 *)__this, (int32_t)___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
 Exception_t152 * ArrayReadOnlyList_1_ReadOnlyError_m30306_gshared (Object_t * __this/* static, unused */, MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m30306(__this/* static, unused */, method) (Exception_t152 *)ArrayReadOnlyList_1_ReadOnlyError_m30306_gshared((Object_t *)__this/* static, unused */, method)
