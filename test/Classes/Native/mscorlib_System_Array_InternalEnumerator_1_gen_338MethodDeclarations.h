﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/VirtualButtonData>
struct InternalEnumerator_1_t4197;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m25265 (InternalEnumerator_1_t4197 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25266 (InternalEnumerator_1_t4197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/VirtualButtonData>::Dispose()
 void InternalEnumerator_1_Dispose_m25267 (InternalEnumerator_1_t4197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/VirtualButtonData>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m25268 (InternalEnumerator_1_t4197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/VirtualButtonData>::get_Current()
 VirtualButtonData_t640  InternalEnumerator_1_get_Current_m25269 (InternalEnumerator_1_t4197 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
