﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TextTrackerImpl
struct TextTrackerImpl_t680;
// Vuforia.WordList
struct WordList_t678;
// Vuforia.TextTrackerImpl/UpDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrackerImpl_UpD.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// Vuforia.RectangleIntData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleIntData.h"
// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"

// Vuforia.WordList Vuforia.TextTrackerImpl::get_WordList()
 WordList_t678 * TextTrackerImpl_get_WordList_m3071 (TextTrackerImpl_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TextTrackerImpl::Start()
 bool TextTrackerImpl_Start_m3072 (TextTrackerImpl_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TextTrackerImpl::Stop()
 void TextTrackerImpl_Stop_m3073 (TextTrackerImpl_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TextTrackerImpl::SetRegionOfInterest(UnityEngine.Rect,UnityEngine.Rect)
 bool TextTrackerImpl_SetRegionOfInterest_m3074 (TextTrackerImpl_t680 * __this, Rect_t118  ___detectionRegion, Rect_t118  ___trackingRegion, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TextTrackerImpl::GetRegionOfInterest(UnityEngine.Rect&,UnityEngine.Rect&)
 bool TextTrackerImpl_GetRegionOfInterest_m3075 (TextTrackerImpl_t680 * __this, Rect_t118 * ___detectionRegion, Rect_t118 * ___trackingRegion, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Vuforia.TextTrackerImpl::ScreenSpaceRectFromCamSpaceRectData(Vuforia.RectangleIntData,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
 Rect_t118  TextTrackerImpl_ScreenSpaceRectFromCamSpaceRectData_m3076 (TextTrackerImpl_t680 * __this, RectangleIntData_t589  ___camSpaceRectData, Rect_t118  ___bgTextureViewPortRect, bool ___isTextureMirrored, VideoModeData_t558  ___videoModeData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextTrackerImpl/UpDirection Vuforia.TextTrackerImpl::get_CurrentUpDirection()
 int32_t TextTrackerImpl_get_CurrentUpDirection_m3077 (TextTrackerImpl_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TextTrackerImpl::.ctor()
 void TextTrackerImpl__ctor_m3078 (TextTrackerImpl_t680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
