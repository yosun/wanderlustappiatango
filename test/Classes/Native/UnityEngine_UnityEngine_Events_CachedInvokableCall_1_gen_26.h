﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_22.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.KeepAliveBehaviour>
struct CachedInvokableCall_1_t2878  : public InvokableCall_1_t2879
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.KeepAliveBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
