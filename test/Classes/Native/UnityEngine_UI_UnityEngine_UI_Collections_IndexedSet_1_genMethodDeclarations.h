﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>
struct IndexedSet_1_t275;
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t278;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ICanvasElement>
struct IEnumerator_1_t3221;
// UnityEngine.UI.ICanvasElement[]
struct ICanvasElementU5BU5D_t3222;
// System.Predicate`1<UnityEngine.UI.ICanvasElement>
struct Predicate_1_t277;
// System.Comparison`1<UnityEngine.UI.ICanvasElement>
struct Comparison_1_t276;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m2075(__this, method) (void)IndexedSet_1__ctor_m17125_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17126(__this, method) (Object_t *)IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17127_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Add(T)
#define IndexedSet_1_Add_m2086(__this, ___item, method) (void)IndexedSet_1_Add_m17128_gshared((IndexedSet_1_t3223 *)__this, (Object_t *)___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Remove(T)
#define IndexedSet_1_Remove_m2088(__this, ___item, method) (bool)IndexedSet_1_Remove_m17129_gshared((IndexedSet_1_t3223 *)__this, (Object_t *)___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m17130(__this, method) (Object_t*)IndexedSet_1_GetEnumerator_m17131_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Clear()
#define IndexedSet_1_Clear_m2085(__this, method) (void)IndexedSet_1_Clear_m17132_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Contains(T)
#define IndexedSet_1_Contains_m17133(__this, ___item, method) (bool)IndexedSet_1_Contains_m17134_gshared((IndexedSet_1_t3223 *)__this, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m17135(__this, ___array, ___arrayIndex, method) (void)IndexedSet_1_CopyTo_m17136_gshared((IndexedSet_1_t3223 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Count()
#define IndexedSet_1_get_Count_m2084(__this, method) (int32_t)IndexedSet_1_get_Count_m17137_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m17138(__this, method) (bool)IndexedSet_1_get_IsReadOnly_m17139_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::IndexOf(T)
#define IndexedSet_1_IndexOf_m17140(__this, ___item, method) (int32_t)IndexedSet_1_IndexOf_m17141_gshared((IndexedSet_1_t3223 *)__this, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m17142(__this, ___index, ___item, method) (void)IndexedSet_1_Insert_m17143_gshared((IndexedSet_1_t3223 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m17144(__this, ___index, method) (void)IndexedSet_1_RemoveAt_m17145_gshared((IndexedSet_1_t3223 *)__this, (int32_t)___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m2082(__this, ___index, method) (Object_t *)IndexedSet_1_get_Item_m17146_gshared((IndexedSet_1_t3223 *)__this, (int32_t)___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m17147(__this, ___index, ___value, method) (void)IndexedSet_1_set_Item_m17148_gshared((IndexedSet_1_t3223 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m2080(__this, ___match, method) (void)IndexedSet_1_RemoveAll_m17149_gshared((IndexedSet_1_t3223 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m2081(__this, ___sortLayoutFunction, method) (void)IndexedSet_1_Sort_m17150_gshared((IndexedSet_1_t3223 *)__this, (Comparison_1_t2846 *)___sortLayoutFunction, method)
