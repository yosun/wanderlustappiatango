﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<Vuforia.ITrackableEventHandler>
struct Comparer_1_t3669;
// System.Object
struct Object_t;
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t135;

// System.Void System.Collections.Generic.Comparer`1<Vuforia.ITrackableEventHandler>::.ctor()
// System.Collections.Generic.Comparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_genMethodDeclarations.h"
#define Comparer_1__ctor_m20343(__this, method) (void)Comparer_1__ctor_m14754_gshared((Comparer_1_t2861 *)__this, method)
// System.Void System.Collections.Generic.Comparer`1<Vuforia.ITrackableEventHandler>::.cctor()
#define Comparer_1__cctor_m20344(__this/* static, unused */, method) (void)Comparer_1__cctor_m14755_gshared((Object_t *)__this/* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.ITrackableEventHandler>::System.Collections.IComparer.Compare(System.Object,System.Object)
#define Comparer_1_System_Collections_IComparer_Compare_m20345(__this, ___x, ___y, method) (int32_t)Comparer_1_System_Collections_IComparer_Compare_m14756_gshared((Comparer_1_t2861 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.ITrackableEventHandler>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.ITrackableEventHandler>::get_Default()
#define Comparer_1_get_Default_m20346(__this/* static, unused */, method) (Comparer_1_t3669 *)Comparer_1_get_Default_m14757_gshared((Object_t *)__this/* static, unused */, method)
