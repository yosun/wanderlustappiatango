﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t58;
// Vuforia.Marker
struct Marker_t623;
// UnityEngine.Transform
struct Transform_t10;
// UnityEngine.GameObject
struct GameObject_t2;

// Vuforia.Marker Vuforia.MarkerAbstractBehaviour::get_Marker()
 Object_t * MarkerAbstractBehaviour_get_Marker_m4086 (MarkerAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::.ctor()
 void MarkerAbstractBehaviour__ctor_m475 (MarkerAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::InternalUnregisterTrackable()
 void MarkerAbstractBehaviour_InternalUnregisterTrackable_m481 (MarkerAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::CorrectScaleImpl()
 bool MarkerAbstractBehaviour_CorrectScaleImpl_m482 (MarkerAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.get_MarkerID()
 int32_t MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_get_MarkerID_m483 (MarkerAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.SetMarkerID(System.Int32)
 bool MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_SetMarkerID_m484 (MarkerAbstractBehaviour_t58 * __this, int32_t ___markerID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorMarkerBehaviour.InitializeMarker(Vuforia.Marker)
 void MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_InitializeMarker_m485 (MarkerAbstractBehaviour_t58 * __this, Object_t * ___marker, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m476 (MarkerAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m477 (MarkerAbstractBehaviour_t58 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t10 * MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m478 (MarkerAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.MarkerAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t2 * MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m479 (MarkerAbstractBehaviour_t58 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
