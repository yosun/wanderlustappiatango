﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>
struct Comparer_1_t2874;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>
struct Comparer_1_t2874  : public Object_t
{
};
struct Comparer_1_t2874_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Reflection.MethodInfo>::_default
	Comparer_1_t2874 * ____default_0;
};
