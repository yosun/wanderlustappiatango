﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.VirtualButtonAbstractBehaviour>
struct DefaultComparer_t4216;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t56;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.VirtualButtonAbstractBehaviour>::.ctor()
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_0MethodDeclarations.h"
#define DefaultComparer__ctor_m25482(__this, method) (void)DefaultComparer__ctor_m14758_gshared((DefaultComparer_t2862 *)__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.VirtualButtonAbstractBehaviour>::Compare(T,T)
#define DefaultComparer_Compare_m25483(__this, ___x, ___y, method) (int32_t)DefaultComparer_Compare_m14759_gshared((DefaultComparer_t2862 *)__this, (Object_t *)___x, (Object_t *)___y, method)
