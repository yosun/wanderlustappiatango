﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t445;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>
struct UnityAction_1_t4598  : public MulticastDelegate_t325
{
};
