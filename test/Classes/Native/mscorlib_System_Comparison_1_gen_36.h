﻿#pragma once
#include <stdint.h>
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t711;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.ILoadLevelEventHandler>
struct Comparison_1_t4042  : public MulticastDelegate_t325
{
};
