﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<System.String>
struct List_1_t587;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Vuforia.DataSetLoadAbstractBehaviour
struct DataSetLoadAbstractBehaviour_t36  : public MonoBehaviour_t6
{
	// System.Boolean Vuforia.DataSetLoadAbstractBehaviour::mDatasetsLoaded
	bool ___mDatasetsLoaded_2;
	// System.Collections.Generic.List`1<System.String> Vuforia.DataSetLoadAbstractBehaviour::mDataSetsToLoad
	List_1_t587 * ___mDataSetsToLoad_3;
	// System.Collections.Generic.List`1<System.String> Vuforia.DataSetLoadAbstractBehaviour::mDataSetsToActivate
	List_1_t587 * ___mDataSetsToActivate_4;
	// System.Collections.Generic.List`1<System.String> Vuforia.DataSetLoadAbstractBehaviour::mExternalDatasetRoots
	List_1_t587 * ___mExternalDatasetRoots_5;
};
