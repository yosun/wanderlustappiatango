﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionAbstractBehaviour>
struct UnityAction_1_t4063;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionAbstractBehaviour>
struct InvokableCall_1_t4062  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionAbstractBehaviour>::Delegate
	UnityAction_1_t4063 * ___Delegate_0;
};
