﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t56;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonAbstractBehaviour>
struct UnityAction_1_t4405  : public MulticastDelegate_t325
{
};
