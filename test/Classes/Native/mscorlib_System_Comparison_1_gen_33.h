﻿#pragma once
#include <stdint.h>
// Vuforia.WordResult
struct WordResult_t702;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.WordResult>
struct Comparison_1_t3979  : public MulticastDelegate_t325
{
};
