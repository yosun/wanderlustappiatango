﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PropImpl
struct PropImpl_t674;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t581;
// UnityEngine.Mesh
struct Mesh_t174;
// Vuforia.OrientedBoundingBox3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox_0.h"

// System.Void Vuforia.PropImpl::.ctor(System.Int32,Vuforia.SmartTerrainTrackable)
 void PropImpl__ctor_m3059 (PropImpl_t674 * __this, int32_t ___id, Object_t * ___parent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.OrientedBoundingBox3D Vuforia.PropImpl::get_BoundingBox()
 OrientedBoundingBox3D_t591  PropImpl_get_BoundingBox_m3060 (PropImpl_t674 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropImpl::SetMesh(System.Int32,UnityEngine.Mesh)
 void PropImpl_SetMesh_m3061 (PropImpl_t674 * __this, int32_t ___meshRev, Mesh_t174 * ___mesh, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropImpl::SetObb(Vuforia.OrientedBoundingBox3D)
 void PropImpl_SetObb_m3062 (PropImpl_t674 * __this, OrientedBoundingBox3D_t591  ___obb3D, MethodInfo* method) IL2CPP_METHOD_ATTR;
