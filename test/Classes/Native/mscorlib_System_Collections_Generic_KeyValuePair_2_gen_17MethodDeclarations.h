﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>
struct KeyValuePair_2_t4125;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t69;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m24598 (KeyValuePair_2_t4125 * __this, int32_t ___key, PropAbstractBehaviour_t69 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::get_Key()
 int32_t KeyValuePair_2_get_Key_m24599 (KeyValuePair_2_t4125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m24600 (KeyValuePair_2_t4125 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::get_Value()
 PropAbstractBehaviour_t69 * KeyValuePair_2_get_Value_m24601 (KeyValuePair_2_t4125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m24602 (KeyValuePair_2_t4125 * __this, PropAbstractBehaviour_t69 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>::ToString()
 String_t* KeyValuePair_2_ToString_m24603 (KeyValuePair_2_t4125 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
