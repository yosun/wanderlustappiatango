﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>
struct InternalEnumerator_1_t5012;
// System.Object
struct Object_t;
// System.Reflection.Emit.FieldBuilder
struct FieldBuilder_t1906;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m30642(__this, ___array, method) (void)InternalEnumerator_1__ctor_m13978_gshared((InternalEnumerator_1_t2698 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30643(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::Dispose()
#define InternalEnumerator_1_Dispose_m30644(__this, method) (void)InternalEnumerator_1_Dispose_m13982_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::MoveNext()
#define InternalEnumerator_1_MoveNext_m30645(__this, method) (bool)InternalEnumerator_1_MoveNext_m13984_gshared((InternalEnumerator_1_t2698 *)__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::get_Current()
#define InternalEnumerator_1_get_Current_m30646(__this, method) (FieldBuilder_t1906 *)InternalEnumerator_1_get_Current_m13986_gshared((InternalEnumerator_1_t2698 *)__this, method)
