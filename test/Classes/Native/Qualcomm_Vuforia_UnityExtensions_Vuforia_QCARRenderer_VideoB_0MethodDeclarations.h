﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARRenderer/VideoBGCfgData
struct VideoBGCfgData_t660;
struct VideoBGCfgData_t660_marshaled;

void VideoBGCfgData_t660_marshal(const VideoBGCfgData_t660& unmarshaled, VideoBGCfgData_t660_marshaled& marshaled);
void VideoBGCfgData_t660_marshal_back(const VideoBGCfgData_t660_marshaled& marshaled, VideoBGCfgData_t660& unmarshaled);
void VideoBGCfgData_t660_marshal_cleanup(VideoBGCfgData_t660_marshaled& marshaled);
