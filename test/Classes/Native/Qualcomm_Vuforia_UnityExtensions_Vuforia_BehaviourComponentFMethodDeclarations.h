﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BehaviourComponentFactory
struct BehaviourComponentFactory_t593;
// Vuforia.IBehaviourComponentFactory
struct IBehaviourComponentFactory_t154;

// Vuforia.IBehaviourComponentFactory Vuforia.BehaviourComponentFactory::get_Instance()
 Object_t * BehaviourComponentFactory_get_Instance_m2815 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory::set_Instance(Vuforia.IBehaviourComponentFactory)
 void BehaviourComponentFactory_set_Instance_m460 (Object_t * __this/* static, unused */, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory::.ctor()
 void BehaviourComponentFactory__ctor_m2816 (BehaviourComponentFactory_t593 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
