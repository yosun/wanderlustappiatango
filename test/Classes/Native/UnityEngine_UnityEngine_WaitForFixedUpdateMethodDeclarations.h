﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t918;

// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
 void WaitForFixedUpdate__ctor_m5435 (WaitForFixedUpdate_t918 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
