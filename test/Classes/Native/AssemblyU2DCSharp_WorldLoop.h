﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t3;
// Raycaster
struct Raycaster_t20;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// WorldLoop
struct WorldLoop_t24  : public MonoBehaviour_t6
{
	// UnityEngine.Camera WorldLoop::cam
	Camera_t3 * ___cam_2;
	// Raycaster WorldLoop::raycaster
	Raycaster_t20 * ___raycaster_3;
};
