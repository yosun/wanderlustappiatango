﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AttributeUsageAttribute
struct AttributeUsageAttribute_t1160;
// System.AttributeTargets
#include "mscorlib_System_AttributeTargets.h"

// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
 void AttributeUsageAttribute__ctor_m6500 (AttributeUsageAttribute_t1160 * __this, int32_t ___validOn, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AttributeUsageAttribute::get_AllowMultiple()
 bool AttributeUsageAttribute_get_AllowMultiple_m9003 (AttributeUsageAttribute_t1160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
 void AttributeUsageAttribute_set_AllowMultiple_m6502 (AttributeUsageAttribute_t1160 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.AttributeUsageAttribute::get_Inherited()
 bool AttributeUsageAttribute_get_Inherited_m9004 (AttributeUsageAttribute_t1160 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.AttributeUsageAttribute::set_Inherited(System.Boolean)
 void AttributeUsageAttribute_set_Inherited_m6501 (AttributeUsageAttribute_t1160 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
