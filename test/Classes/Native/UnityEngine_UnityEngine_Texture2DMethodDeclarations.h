﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Texture2D
struct Texture2D_t285;
// UnityEngine.Color[]
struct ColorU5BU5D_t797;
// UnityEngine.Color32[]
struct Color32U5BU5D_t610;
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
 void Texture2D__ctor_m4828 (Texture2D_t285 * __this, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
 void Texture2D__ctor_m5403 (Texture2D_t285 * __this, int32_t ___width, int32_t ___height, int32_t ___format, bool ___mipmap, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
 void Texture2D_Internal_Create_m5553 (Object_t * __this/* static, unused */, Texture2D_t285 * ___mono, int32_t ___width, int32_t ___height, int32_t ___format, bool ___mipmap, bool ___linear, IntPtr_t121 ___nativeTex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
 int32_t Texture2D_get_format_m4724 (Texture2D_t285 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
 Texture2D_t285 * Texture2D_get_whiteTexture_m2120 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
 Color_t19  Texture2D_GetPixelBilinear_m2226 (Texture2D_t285 * __this, float ___u, float ___v, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
 void Texture2D_SetPixels_m4729 (Texture2D_t285 * __this, ColorU5BU5D_t797* ___colors, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[],System.Int32)
 void Texture2D_SetPixels_m5554 (Texture2D_t285 * __this, ColorU5BU5D_t797* ___colors, int32_t ___miplevel, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
 void Texture2D_SetPixels_m5555 (Texture2D_t285 * __this, int32_t ___x, int32_t ___y, int32_t ___blockWidth, int32_t ___blockHeight, ColorU5BU5D_t797* ___colors, int32_t ___miplevel, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)
 void Texture2D_SetAllPixels32_m5556 (Texture2D_t285 * __this, Color32U5BU5D_t610* ___colors, int32_t ___miplevel, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[])
 void Texture2D_SetPixels32_m5314 (Texture2D_t285 * __this, Color32U5BU5D_t610* ___colors, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[],System.Int32)
 void Texture2D_SetPixels32_m5557 (Texture2D_t285 * __this, Color32U5BU5D_t610* ___colors, int32_t ___miplevel, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels()
 ColorU5BU5D_t797* Texture2D_GetPixels_m4726 (Texture2D_t285 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32)
 ColorU5BU5D_t797* Texture2D_GetPixels_m5558 (Texture2D_t285 * __this, int32_t ___miplevel, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 ColorU5BU5D_t797* Texture2D_GetPixels_m5559 (Texture2D_t285 * __this, int32_t ___x, int32_t ___y, int32_t ___blockWidth, int32_t ___blockHeight, int32_t ___miplevel, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32(System.Int32)
 Color32U5BU5D_t610* Texture2D_GetPixels32_m5560 (Texture2D_t285 * __this, int32_t ___miplevel, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32()
 Color32U5BU5D_t610* Texture2D_GetPixels32_m5312 (Texture2D_t285 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
 void Texture2D_Apply_m5561 (Texture2D_t285 * __this, bool ___updateMipmaps, bool ___makeNoLongerReadable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Apply()
 void Texture2D_Apply_m5315 (Texture2D_t285 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Texture2D::Resize(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
 bool Texture2D_Resize_m4725 (Texture2D_t285 * __this, int32_t ___width, int32_t ___height, int32_t ___format, bool ___hasMipMap, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
 void Texture2D_ReadPixels_m5311 (Texture2D_t285 * __this, Rect_t118  ___source, int32_t ___destX, int32_t ___destY, bool ___recalculateMipMaps, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_ReadPixels(UnityEngine.Texture2D,UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)
 void Texture2D_INTERNAL_CALL_ReadPixels_m5562 (Object_t * __this/* static, unused */, Texture2D_t285 * ___self, Rect_t118 * ___source, int32_t ___destX, int32_t ___destY, bool ___recalculateMipMaps, MethodInfo* method) IL2CPP_METHOD_ATTR;
