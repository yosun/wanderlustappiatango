﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.MethodCallDictionary
struct MethodCallDictionary_t2017;
// System.Runtime.Remoting.Messaging.IMethodMessage
struct IMethodMessage_t2018;

// System.Void System.Runtime.Remoting.Messaging.MethodCallDictionary::.ctor(System.Runtime.Remoting.Messaging.IMethodMessage)
 void MethodCallDictionary__ctor_m11556 (MethodCallDictionary_t2017 * __this, Object_t * ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Messaging.MethodCallDictionary::.cctor()
 void MethodCallDictionary__cctor_m11557 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
