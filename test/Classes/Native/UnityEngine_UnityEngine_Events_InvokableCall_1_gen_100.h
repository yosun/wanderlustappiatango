﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.KeepAliveAbstractBehaviour>
struct UnityAction_1_t4035;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveAbstractBehaviour>
struct InvokableCall_1_t4034  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.KeepAliveAbstractBehaviour>::Delegate
	UnityAction_1_t4035 * ___Delegate_0;
};
