﻿#pragma once
#include <stdint.h>
// Raycaster
struct Raycaster_t20;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Raycaster>
struct CastHelper_1_t2759 
{
	// T UnityEngine.CastHelper`1<Raycaster>::t
	Raycaster_t20 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Raycaster>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
