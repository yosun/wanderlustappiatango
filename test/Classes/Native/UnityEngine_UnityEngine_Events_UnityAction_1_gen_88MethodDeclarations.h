﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>
struct UnityAction_1_t3617;
// System.Object
struct Object_t;
// UnityEngine.UI.Mask
struct Mask_t401;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>::.ctor(System.Object,System.IntPtr)
// UnityEngine.Events.UnityAction`1<System.Object>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_6MethodDeclarations.h"
#define UnityAction_1__ctor_m20035(__this, ___object, ___method, method) (void)UnityAction_1__ctor_m14015_gshared((UnityAction_1_t2707 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>::Invoke(T0)
#define UnityAction_1_Invoke_m20036(__this, ___arg0, method) (void)UnityAction_1_Invoke_m14016_gshared((UnityAction_1_t2707 *)__this, (Object_t *)___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m20037(__this, ___arg0, ___callback, ___object, method) (Object_t *)UnityAction_1_BeginInvoke_m14017_gshared((UnityAction_1_t2707 *)__this, (Object_t *)___arg0, (AsyncCallback_t200 *)___callback, (Object_t *)___object, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m20038(__this, ___result, method) (void)UnityAction_1_EndInvoke_m14018_gshared((UnityAction_1_t2707 *)__this, (Object_t *)___result, method)
