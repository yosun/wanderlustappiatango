﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.ISmartTerrainEventHandler>
struct Comparer_1_t4076;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.ISmartTerrainEventHandler>
struct Comparer_1_t4076  : public Object_t
{
};
struct Comparer_1_t4076_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.ISmartTerrainEventHandler>::_default
	Comparer_1_t4076 * ____default_0;
};
