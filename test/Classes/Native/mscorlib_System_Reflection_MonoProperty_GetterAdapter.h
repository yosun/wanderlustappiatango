﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Reflection.MonoProperty/GetterAdapter
struct GetterAdapter_t1945  : public MulticastDelegate_t325
{
};
