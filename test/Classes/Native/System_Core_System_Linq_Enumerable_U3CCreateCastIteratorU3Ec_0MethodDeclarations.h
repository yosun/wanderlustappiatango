﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t3868;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t455;

// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::.ctor()
 void U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m22044_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3868 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m22044(__this, method) (void)U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m22044_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t3868 *)__this, method)
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
 Object_t * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m22045_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3868 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m22045(__this, method) (Object_t *)U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m22045_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t3868 *)__this, method)
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
 Object_t * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m22046_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3868 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m22046(__this, method) (Object_t *)U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m22046_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t3868 *)__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m22047_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3868 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m22047(__this, method) (Object_t *)U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m22047_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t3868 *)__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
 Object_t* U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m22048_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3868 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m22048(__this, method) (Object_t*)U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m22048_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t3868 *)__this, method)
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::MoveNext()
 bool U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m22049_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3868 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m22049(__this, method) (bool)U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m22049_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t3868 *)__this, method)
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::Dispose()
 void U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m22050_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3868 * __this, MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m22050(__this, method) (void)U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m22050_gshared((U3CCreateCastIteratorU3Ec__Iterator0_1_t3868 *)__this, method)
