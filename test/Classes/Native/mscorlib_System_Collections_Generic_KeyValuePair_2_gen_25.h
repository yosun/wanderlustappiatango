﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// UnityEngine.GUIStyle
struct GUIStyle_t953;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>
struct KeyValuePair_2_t4545 
{
	// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::value
	GUIStyle_t953 * ___value_1;
};
