﻿#pragma once
#include <stdint.h>
// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t718;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.ISmartTerrainEventHandler>
struct Predicate_1_t4069  : public MulticastDelegate_t325
{
};
