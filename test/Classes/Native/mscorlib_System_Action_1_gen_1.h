﻿#pragma once
#include <stdint.h>
// Vuforia.Surface
struct Surface_t43;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<Vuforia.Surface>
struct Action_1_t131  : public MulticastDelegate_t325
{
};
