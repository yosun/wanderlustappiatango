﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyTrademarkAttribute
struct AssemblyTrademarkAttribute_t538;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyTrademarkAttribute::.ctor(System.String)
 void AssemblyTrademarkAttribute__ctor_m2589 (AssemblyTrademarkAttribute_t538 * __this, String_t* ___trademark, MethodInfo* method) IL2CPP_METHOD_ATTR;
