﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// System.Array/InternalEnumerator`1<WorldNav>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_31.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo InternalEnumerator_1_t2760_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<WorldNav>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_31MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// WorldNav
#include "AssemblyU2DCSharp_WorldNav.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Array
#include "mscorlib_System_Array.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo WorldNav_t25_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1493_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m14231_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7651_MethodInfo;
extern MethodInfo Array_get_Length_m7656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWorldNav_t25_m31799_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m31436_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m31436(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<WorldNav>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<WorldNav>(System.Int32)
#define Array_InternalArray__get_Item_TisWorldNav_t25_m31799(__this, p0, method) (WorldNav_t25 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<WorldNav>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<WorldNav>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<WorldNav>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<WorldNav>::MoveNext()
// T System.Array/InternalEnumerator`1<WorldNav>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<WorldNav>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2760____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2760_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2760, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2760____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2760_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2760, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2760_FieldInfos[] =
{
	&InternalEnumerator_1_t2760____array_0_FieldInfo,
	&InternalEnumerator_1_t2760____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14228_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2760____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2760_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14228_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2760____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2760_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14231_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2760_PropertyInfos[] =
{
	&InternalEnumerator_1_t2760____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2760____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2760_InternalEnumerator_1__ctor_m14227_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14227_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<WorldNav>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14227_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2760_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2760_InternalEnumerator_1__ctor_m14227_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14227_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14228_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<WorldNav>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14228_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2760_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14228_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14229_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<WorldNav>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14229_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2760_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14229_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14230_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<WorldNav>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14230_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2760_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14230_GenericMethod/* genericMethod */

};
extern Il2CppType WorldNav_t25_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14231_GenericMethod;
// T System.Array/InternalEnumerator`1<WorldNav>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14231_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2760_il2cpp_TypeInfo/* declaring_type */
	, &WorldNav_t25_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14231_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2760_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14227_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14228_MethodInfo,
	&InternalEnumerator_1_Dispose_m14229_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14230_MethodInfo,
	&InternalEnumerator_1_get_Current_m14231_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m14230_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14229_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2760_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14228_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14230_MethodInfo,
	&InternalEnumerator_1_Dispose_m14229_MethodInfo,
	&InternalEnumerator_1_get_Current_m14231_MethodInfo,
};
extern TypeInfo IEnumerator_t266_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t5750_il2cpp_TypeInfo;
static TypeInfo* InternalEnumerator_1_t2760_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5750_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2760_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5750_il2cpp_TypeInfo, 7},
};
extern TypeInfo WorldNav_t25_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2760_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14231_MethodInfo/* Method Usage */,
	&WorldNav_t25_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisWorldNav_t25_m31799_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2760_0_0_0;
extern Il2CppType InternalEnumerator_1_t2760_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t2760_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t2760_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2760_MethodInfos/* methods */
	, InternalEnumerator_1_t2760_PropertyInfos/* properties */
	, InternalEnumerator_1_t2760_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2760_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2760_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2760_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2760_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2760_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2760_1_0_0/* this_arg */
	, InternalEnumerator_1_t2760_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2760_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2760_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2760)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7299_il2cpp_TypeInfo;

#include "Assembly-CSharp_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<WorldNav>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<WorldNav>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<WorldNav>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<WorldNav>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<WorldNav>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<WorldNav>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<WorldNav>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<WorldNav>
extern MethodInfo ICollection_1_get_Count_m41527_MethodInfo;
static PropertyInfo ICollection_1_t7299____Count_PropertyInfo = 
{
	&ICollection_1_t7299_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41527_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41528_MethodInfo;
static PropertyInfo ICollection_1_t7299____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7299_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41528_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7299_PropertyInfos[] =
{
	&ICollection_1_t7299____Count_PropertyInfo,
	&ICollection_1_t7299____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41527_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<WorldNav>::get_Count()
MethodInfo ICollection_1_get_Count_m41527_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7299_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41527_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41528_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<WorldNav>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41528_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7299_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41528_GenericMethod/* genericMethod */

};
extern Il2CppType WorldNav_t25_0_0_0;
extern Il2CppType WorldNav_t25_0_0_0;
static ParameterInfo ICollection_1_t7299_ICollection_1_Add_m41529_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldNav_t25_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41529_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<WorldNav>::Add(T)
MethodInfo ICollection_1_Add_m41529_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7299_ICollection_1_Add_m41529_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41529_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41530_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<WorldNav>::Clear()
MethodInfo ICollection_1_Clear_m41530_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41530_GenericMethod/* genericMethod */

};
extern Il2CppType WorldNav_t25_0_0_0;
static ParameterInfo ICollection_1_t7299_ICollection_1_Contains_m41531_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldNav_t25_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41531_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<WorldNav>::Contains(T)
MethodInfo ICollection_1_Contains_m41531_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7299_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7299_ICollection_1_Contains_m41531_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41531_GenericMethod/* genericMethod */

};
extern Il2CppType WorldNavU5BU5D_t5166_0_0_0;
extern Il2CppType WorldNavU5BU5D_t5166_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7299_ICollection_1_CopyTo_m41532_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WorldNavU5BU5D_t5166_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41532_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<WorldNav>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41532_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7299_ICollection_1_CopyTo_m41532_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41532_GenericMethod/* genericMethod */

};
extern Il2CppType WorldNav_t25_0_0_0;
static ParameterInfo ICollection_1_t7299_ICollection_1_Remove_m41533_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldNav_t25_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41533_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<WorldNav>::Remove(T)
MethodInfo ICollection_1_Remove_m41533_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7299_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7299_ICollection_1_Remove_m41533_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41533_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7299_MethodInfos[] =
{
	&ICollection_1_get_Count_m41527_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41528_MethodInfo,
	&ICollection_1_Add_m41529_MethodInfo,
	&ICollection_1_Clear_m41530_MethodInfo,
	&ICollection_1_Contains_m41531_MethodInfo,
	&ICollection_1_CopyTo_m41532_MethodInfo,
	&ICollection_1_Remove_m41533_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7301_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7299_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7301_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7299_0_0_0;
extern Il2CppType ICollection_1_t7299_1_0_0;
struct ICollection_1_t7299;
extern Il2CppGenericClass ICollection_1_t7299_GenericClass;
TypeInfo ICollection_1_t7299_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7299_MethodInfos/* methods */
	, ICollection_1_t7299_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7299_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7299_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7299_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7299_0_0_0/* byval_arg */
	, &ICollection_1_t7299_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7299_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<WorldNav>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<WorldNav>
extern Il2CppType IEnumerator_1_t5750_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41534_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<WorldNav>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41534_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7301_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5750_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41534_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7301_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41534_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7301_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7301_0_0_0;
extern Il2CppType IEnumerable_1_t7301_1_0_0;
struct IEnumerable_1_t7301;
extern Il2CppGenericClass IEnumerable_1_t7301_GenericClass;
TypeInfo IEnumerable_1_t7301_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7301_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7301_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7301_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7301_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7301_0_0_0/* byval_arg */
	, &IEnumerable_1_t7301_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7301_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7300_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<WorldNav>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<WorldNav>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<WorldNav>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<WorldNav>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<WorldNav>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<WorldNav>
extern MethodInfo IList_1_get_Item_m41535_MethodInfo;
extern MethodInfo IList_1_set_Item_m41536_MethodInfo;
static PropertyInfo IList_1_t7300____Item_PropertyInfo = 
{
	&IList_1_t7300_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41535_MethodInfo/* get */
	, &IList_1_set_Item_m41536_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7300_PropertyInfos[] =
{
	&IList_1_t7300____Item_PropertyInfo,
	NULL
};
extern Il2CppType WorldNav_t25_0_0_0;
static ParameterInfo IList_1_t7300_IList_1_IndexOf_m41537_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldNav_t25_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41537_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<WorldNav>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41537_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7300_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7300_IList_1_IndexOf_m41537_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41537_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType WorldNav_t25_0_0_0;
static ParameterInfo IList_1_t7300_IList_1_Insert_m41538_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WorldNav_t25_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41538_GenericMethod;
// System.Void System.Collections.Generic.IList`1<WorldNav>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41538_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7300_IList_1_Insert_m41538_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41538_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7300_IList_1_RemoveAt_m41539_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41539_GenericMethod;
// System.Void System.Collections.Generic.IList`1<WorldNav>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41539_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7300_IList_1_RemoveAt_m41539_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41539_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7300_IList_1_get_Item_m41535_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType WorldNav_t25_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41535_GenericMethod;
// T System.Collections.Generic.IList`1<WorldNav>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41535_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7300_il2cpp_TypeInfo/* declaring_type */
	, &WorldNav_t25_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7300_IList_1_get_Item_m41535_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41535_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType WorldNav_t25_0_0_0;
static ParameterInfo IList_1_t7300_IList_1_set_Item_m41536_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WorldNav_t25_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41536_GenericMethod;
// System.Void System.Collections.Generic.IList`1<WorldNav>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41536_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7300_IList_1_set_Item_m41536_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41536_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7300_MethodInfos[] =
{
	&IList_1_IndexOf_m41537_MethodInfo,
	&IList_1_Insert_m41538_MethodInfo,
	&IList_1_RemoveAt_m41539_MethodInfo,
	&IList_1_get_Item_m41535_MethodInfo,
	&IList_1_set_Item_m41536_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7300_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7299_il2cpp_TypeInfo,
	&IEnumerable_1_t7301_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7300_0_0_0;
extern Il2CppType IList_1_t7300_1_0_0;
struct IList_1_t7300;
extern Il2CppGenericClass IList_1_t7300_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7300_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7300_MethodInfos/* methods */
	, IList_1_t7300_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7300_il2cpp_TypeInfo/* element_class */
	, IList_1_t7300_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7300_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7300_0_0_0/* byval_arg */
	, &IList_1_t7300_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7300_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<WorldNav>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_12.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2761_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<WorldNav>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_12MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.Events.InvokableCall`1<WorldNav>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_8.h"
extern TypeInfo ObjectU5BU5D_t115_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t2762_il2cpp_TypeInfo;
extern TypeInfo Void_t99_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<WorldNav>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_8MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14234_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14236_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<WorldNav>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<WorldNav>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<WorldNav>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2761____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2761_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2761, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2761_FieldInfos[] =
{
	&CachedInvokableCall_1_t2761____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType WorldNav_t25_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2761_CachedInvokableCall_1__ctor_m14232_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &WorldNav_t25_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14232_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<WorldNav>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14232_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2761_CachedInvokableCall_1__ctor_m14232_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14232_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2761_CachedInvokableCall_1_Invoke_m14233_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14233_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<WorldNav>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14233_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2761_CachedInvokableCall_1_Invoke_m14233_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14233_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2761_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14232_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14233_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m304_MethodInfo;
extern MethodInfo Object_GetHashCode_m305_MethodInfo;
extern MethodInfo Object_ToString_m306_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m14233_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14237_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2761_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14233_MethodInfo,
	&InvokableCall_1_Find_m14237_MethodInfo,
};
extern Il2CppType UnityAction_1_t2763_0_0_0;
extern TypeInfo UnityAction_1_t2763_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisWorldNav_t25_m31809_MethodInfo;
extern TypeInfo WorldNav_t25_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14239_MethodInfo;
extern TypeInfo WorldNav_t25_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2761_RGCTXData[8] = 
{
	&UnityAction_1_t2763_0_0_0/* Type Usage */,
	&UnityAction_1_t2763_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWorldNav_t25_m31809_MethodInfo/* Method Usage */,
	&WorldNav_t25_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14239_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14234_MethodInfo/* Method Usage */,
	&WorldNav_t25_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14236_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2761_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2761_1_0_0;
struct CachedInvokableCall_1_t2761;
extern Il2CppGenericClass CachedInvokableCall_1_t2761_GenericClass;
TypeInfo CachedInvokableCall_1_t2761_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2761_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2761_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2761_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2761_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2761_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2761_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2761_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2761_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2761_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2761)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<WorldNav>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_15.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t2763_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t507_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<WorldNav>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_15MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6358_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m395_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m456_MethodInfo;
extern MethodInfo Delegate_Combine_m2149_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6357_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2468_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6359_MethodInfo;
extern MethodInfo Delegate_get_Target_m6536_MethodInfo;
extern MethodInfo Delegate_get_Method_m6534_MethodInfo;
struct BaseInvokableCall_t1075;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<WorldNav>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<WorldNav>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisWorldNav_t25_m31809(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<WorldNav>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<WorldNav>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<WorldNav>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<WorldNav>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<WorldNav>
extern Il2CppType UnityAction_1_t2763_0_0_1;
FieldInfo InvokableCall_1_t2762____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2763_0_0_1/* type */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2762, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2762_FieldInfos[] =
{
	&InvokableCall_1_t2762____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2762_InvokableCall_1__ctor_m14234_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14234_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<WorldNav>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14234_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2762_InvokableCall_1__ctor_m14234_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14234_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2763_0_0_0;
static ParameterInfo InvokableCall_1_t2762_InvokableCall_1__ctor_m14235_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2763_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14235_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<WorldNav>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14235_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2762_InvokableCall_1__ctor_m14235_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14235_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2762_InvokableCall_1_Invoke_m14236_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14236_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<WorldNav>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14236_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2762_InvokableCall_1_Invoke_m14236_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14236_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2762_InvokableCall_1_Find_m14237_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14237_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<WorldNav>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14237_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2762_InvokableCall_1_Find_m14237_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14237_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2762_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14234_MethodInfo,
	&InvokableCall_1__ctor_m14235_MethodInfo,
	&InvokableCall_1_Invoke_m14236_MethodInfo,
	&InvokableCall_1_Find_m14237_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2762_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14236_MethodInfo,
	&InvokableCall_1_Find_m14237_MethodInfo,
};
extern TypeInfo UnityAction_1_t2763_il2cpp_TypeInfo;
extern TypeInfo WorldNav_t25_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2762_RGCTXData[5] = 
{
	&UnityAction_1_t2763_0_0_0/* Type Usage */,
	&UnityAction_1_t2763_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWorldNav_t25_m31809_MethodInfo/* Method Usage */,
	&WorldNav_t25_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14239_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2762_0_0_0;
extern Il2CppType InvokableCall_1_t2762_1_0_0;
extern TypeInfo BaseInvokableCall_t1075_il2cpp_TypeInfo;
struct InvokableCall_1_t2762;
extern Il2CppGenericClass InvokableCall_1_t2762_GenericClass;
TypeInfo InvokableCall_1_t2762_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2762_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2762_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2762_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2762_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2762_0_0_0/* byval_arg */
	, &InvokableCall_1_t2762_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2762_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2762_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2762)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<WorldNav>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<WorldNav>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<WorldNav>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<WorldNav>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<WorldNav>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2763_UnityAction_1__ctor_m14238_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14238_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<WorldNav>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14238_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2763_UnityAction_1__ctor_m14238_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14238_GenericMethod/* genericMethod */

};
extern Il2CppType WorldNav_t25_0_0_0;
static ParameterInfo UnityAction_1_t2763_UnityAction_1_Invoke_m14239_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WorldNav_t25_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14239_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<WorldNav>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14239_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2763_UnityAction_1_Invoke_m14239_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14239_GenericMethod/* genericMethod */

};
extern Il2CppType WorldNav_t25_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2763_UnityAction_1_BeginInvoke_m14240_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WorldNav_t25_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14240_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<WorldNav>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14240_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2763_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2763_UnityAction_1_BeginInvoke_m14240_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14240_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2763_UnityAction_1_EndInvoke_m14241_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14241_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<WorldNav>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14241_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2763_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2763_UnityAction_1_EndInvoke_m14241_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14241_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2763_MethodInfos[] =
{
	&UnityAction_1__ctor_m14238_MethodInfo,
	&UnityAction_1_Invoke_m14239_MethodInfo,
	&UnityAction_1_BeginInvoke_m14240_MethodInfo,
	&UnityAction_1_EndInvoke_m14241_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2241_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2242_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2243_MethodInfo;
extern MethodInfo Delegate_Clone_m2244_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2245_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2246_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2247_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m14240_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14241_MethodInfo;
static MethodInfo* UnityAction_1_t2763_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14239_MethodInfo,
	&UnityAction_1_BeginInvoke_m14240_MethodInfo,
	&UnityAction_1_EndInvoke_m14241_MethodInfo,
};
extern TypeInfo ICloneable_t481_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t482_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t2763_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2763_1_0_0;
extern TypeInfo MulticastDelegate_t325_il2cpp_TypeInfo;
struct UnityAction_1_t2763;
extern Il2CppGenericClass UnityAction_1_t2763_GenericClass;
TypeInfo UnityAction_1_t2763_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2763_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2763_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2763_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2763_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2763_0_0_0/* byval_arg */
	, &UnityAction_1_t2763_1_0_0/* this_arg */
	, UnityAction_1_t2763_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2763_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2763)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_3.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2764_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_3MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.Camera>
extern Il2CppType Camera_t3_0_0_6;
FieldInfo CastHelper_1_t2764____t_0_FieldInfo = 
{
	"t"/* name */
	, &Camera_t3_0_0_6/* type */
	, &CastHelper_1_t2764_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2764, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2764____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2764_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2764, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2764_FieldInfos[] =
{
	&CastHelper_1_t2764____t_0_FieldInfo,
	&CastHelper_1_t2764____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2764_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2764_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2764_0_0_0;
extern Il2CppType CastHelper_1_t2764_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2764_GenericClass;
TypeInfo CastHelper_1_t2764_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2764_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2764_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2764_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2764_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2764_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2764_0_0_0/* byval_arg */
	, &CastHelper_1_t2764_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2764_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2764)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5752_il2cpp_TypeInfo;

// OrbitCamera
#include "AssemblyU2DCSharp_OrbitCamera.h"


// T System.Collections.Generic.IEnumerator`1<OrbitCamera>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<OrbitCamera>
extern MethodInfo IEnumerator_1_get_Current_m41540_MethodInfo;
static PropertyInfo IEnumerator_1_t5752____Current_PropertyInfo = 
{
	&IEnumerator_1_t5752_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41540_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5752_PropertyInfos[] =
{
	&IEnumerator_1_t5752____Current_PropertyInfo,
	NULL
};
extern Il2CppType OrbitCamera_t26_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41540_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<OrbitCamera>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41540_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5752_il2cpp_TypeInfo/* declaring_type */
	, &OrbitCamera_t26_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41540_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5752_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41540_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5752_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5752_0_0_0;
extern Il2CppType IEnumerator_1_t5752_1_0_0;
struct IEnumerator_1_t5752;
extern Il2CppGenericClass IEnumerator_1_t5752_GenericClass;
TypeInfo IEnumerator_1_t5752_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5752_MethodInfos/* methods */
	, IEnumerator_1_t5752_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5752_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5752_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5752_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5752_0_0_0/* byval_arg */
	, &IEnumerator_1_t5752_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5752_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<OrbitCamera>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_32.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2765_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<OrbitCamera>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_32MethodDeclarations.h"

extern TypeInfo OrbitCamera_t26_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14246_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisOrbitCamera_t26_m31811_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<OrbitCamera>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<OrbitCamera>(System.Int32)
#define Array_InternalArray__get_Item_TisOrbitCamera_t26_m31811(__this, p0, method) (OrbitCamera_t26 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<OrbitCamera>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<OrbitCamera>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<OrbitCamera>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<OrbitCamera>::MoveNext()
// T System.Array/InternalEnumerator`1<OrbitCamera>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<OrbitCamera>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2765____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2765, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2765____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2765, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2765_FieldInfos[] =
{
	&InternalEnumerator_1_t2765____array_0_FieldInfo,
	&InternalEnumerator_1_t2765____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14243_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2765____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2765_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14243_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2765____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2765_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14246_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2765_PropertyInfos[] =
{
	&InternalEnumerator_1_t2765____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2765____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2765_InternalEnumerator_1__ctor_m14242_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14242_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<OrbitCamera>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14242_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2765_InternalEnumerator_1__ctor_m14242_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14242_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14243_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<OrbitCamera>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14243_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14243_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14244_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<OrbitCamera>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14244_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14244_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14245_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<OrbitCamera>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14245_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14245_GenericMethod/* genericMethod */

};
extern Il2CppType OrbitCamera_t26_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14246_GenericMethod;
// T System.Array/InternalEnumerator`1<OrbitCamera>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14246_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* declaring_type */
	, &OrbitCamera_t26_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14246_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2765_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14242_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14243_MethodInfo,
	&InternalEnumerator_1_Dispose_m14244_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14245_MethodInfo,
	&InternalEnumerator_1_get_Current_m14246_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14245_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14244_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2765_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14243_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14245_MethodInfo,
	&InternalEnumerator_1_Dispose_m14244_MethodInfo,
	&InternalEnumerator_1_get_Current_m14246_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2765_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5752_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2765_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5752_il2cpp_TypeInfo, 7},
};
extern TypeInfo OrbitCamera_t26_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2765_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14246_MethodInfo/* Method Usage */,
	&OrbitCamera_t26_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisOrbitCamera_t26_m31811_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2765_0_0_0;
extern Il2CppType InternalEnumerator_1_t2765_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2765_GenericClass;
TypeInfo InternalEnumerator_1_t2765_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2765_MethodInfos/* methods */
	, InternalEnumerator_1_t2765_PropertyInfos/* properties */
	, InternalEnumerator_1_t2765_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2765_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2765_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2765_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2765_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2765_1_0_0/* this_arg */
	, InternalEnumerator_1_t2765_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2765_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2765_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2765)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7302_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<OrbitCamera>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<OrbitCamera>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<OrbitCamera>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<OrbitCamera>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<OrbitCamera>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<OrbitCamera>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<OrbitCamera>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<OrbitCamera>
extern MethodInfo ICollection_1_get_Count_m41541_MethodInfo;
static PropertyInfo ICollection_1_t7302____Count_PropertyInfo = 
{
	&ICollection_1_t7302_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41541_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41542_MethodInfo;
static PropertyInfo ICollection_1_t7302____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7302_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41542_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7302_PropertyInfos[] =
{
	&ICollection_1_t7302____Count_PropertyInfo,
	&ICollection_1_t7302____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41541_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<OrbitCamera>::get_Count()
MethodInfo ICollection_1_get_Count_m41541_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7302_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41541_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41542_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<OrbitCamera>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41542_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7302_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41542_GenericMethod/* genericMethod */

};
extern Il2CppType OrbitCamera_t26_0_0_0;
extern Il2CppType OrbitCamera_t26_0_0_0;
static ParameterInfo ICollection_1_t7302_ICollection_1_Add_m41543_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &OrbitCamera_t26_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41543_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<OrbitCamera>::Add(T)
MethodInfo ICollection_1_Add_m41543_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7302_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7302_ICollection_1_Add_m41543_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41543_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41544_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<OrbitCamera>::Clear()
MethodInfo ICollection_1_Clear_m41544_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7302_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41544_GenericMethod/* genericMethod */

};
extern Il2CppType OrbitCamera_t26_0_0_0;
static ParameterInfo ICollection_1_t7302_ICollection_1_Contains_m41545_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &OrbitCamera_t26_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41545_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<OrbitCamera>::Contains(T)
MethodInfo ICollection_1_Contains_m41545_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7302_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7302_ICollection_1_Contains_m41545_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41545_GenericMethod/* genericMethod */

};
extern Il2CppType OrbitCameraU5BU5D_t5167_0_0_0;
extern Il2CppType OrbitCameraU5BU5D_t5167_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7302_ICollection_1_CopyTo_m41546_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &OrbitCameraU5BU5D_t5167_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41546_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<OrbitCamera>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41546_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7302_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7302_ICollection_1_CopyTo_m41546_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41546_GenericMethod/* genericMethod */

};
extern Il2CppType OrbitCamera_t26_0_0_0;
static ParameterInfo ICollection_1_t7302_ICollection_1_Remove_m41547_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &OrbitCamera_t26_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41547_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<OrbitCamera>::Remove(T)
MethodInfo ICollection_1_Remove_m41547_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7302_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7302_ICollection_1_Remove_m41547_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41547_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7302_MethodInfos[] =
{
	&ICollection_1_get_Count_m41541_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41542_MethodInfo,
	&ICollection_1_Add_m41543_MethodInfo,
	&ICollection_1_Clear_m41544_MethodInfo,
	&ICollection_1_Contains_m41545_MethodInfo,
	&ICollection_1_CopyTo_m41546_MethodInfo,
	&ICollection_1_Remove_m41547_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7304_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7302_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7304_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7302_0_0_0;
extern Il2CppType ICollection_1_t7302_1_0_0;
struct ICollection_1_t7302;
extern Il2CppGenericClass ICollection_1_t7302_GenericClass;
TypeInfo ICollection_1_t7302_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7302_MethodInfos/* methods */
	, ICollection_1_t7302_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7302_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7302_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7302_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7302_0_0_0/* byval_arg */
	, &ICollection_1_t7302_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7302_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<OrbitCamera>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<OrbitCamera>
extern Il2CppType IEnumerator_1_t5752_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41548_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<OrbitCamera>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41548_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7304_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5752_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41548_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7304_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41548_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7304_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7304_0_0_0;
extern Il2CppType IEnumerable_1_t7304_1_0_0;
struct IEnumerable_1_t7304;
extern Il2CppGenericClass IEnumerable_1_t7304_GenericClass;
TypeInfo IEnumerable_1_t7304_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7304_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7304_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7304_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7304_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7304_0_0_0/* byval_arg */
	, &IEnumerable_1_t7304_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7304_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7303_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<OrbitCamera>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<OrbitCamera>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<OrbitCamera>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<OrbitCamera>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<OrbitCamera>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<OrbitCamera>
extern MethodInfo IList_1_get_Item_m41549_MethodInfo;
extern MethodInfo IList_1_set_Item_m41550_MethodInfo;
static PropertyInfo IList_1_t7303____Item_PropertyInfo = 
{
	&IList_1_t7303_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41549_MethodInfo/* get */
	, &IList_1_set_Item_m41550_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7303_PropertyInfos[] =
{
	&IList_1_t7303____Item_PropertyInfo,
	NULL
};
extern Il2CppType OrbitCamera_t26_0_0_0;
static ParameterInfo IList_1_t7303_IList_1_IndexOf_m41551_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &OrbitCamera_t26_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41551_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<OrbitCamera>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41551_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7303_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7303_IList_1_IndexOf_m41551_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41551_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType OrbitCamera_t26_0_0_0;
static ParameterInfo IList_1_t7303_IList_1_Insert_m41552_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &OrbitCamera_t26_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41552_GenericMethod;
// System.Void System.Collections.Generic.IList`1<OrbitCamera>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41552_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7303_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7303_IList_1_Insert_m41552_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41552_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7303_IList_1_RemoveAt_m41553_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41553_GenericMethod;
// System.Void System.Collections.Generic.IList`1<OrbitCamera>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41553_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7303_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7303_IList_1_RemoveAt_m41553_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41553_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7303_IList_1_get_Item_m41549_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType OrbitCamera_t26_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41549_GenericMethod;
// T System.Collections.Generic.IList`1<OrbitCamera>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41549_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7303_il2cpp_TypeInfo/* declaring_type */
	, &OrbitCamera_t26_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7303_IList_1_get_Item_m41549_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41549_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType OrbitCamera_t26_0_0_0;
static ParameterInfo IList_1_t7303_IList_1_set_Item_m41550_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &OrbitCamera_t26_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41550_GenericMethod;
// System.Void System.Collections.Generic.IList`1<OrbitCamera>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41550_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7303_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7303_IList_1_set_Item_m41550_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41550_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7303_MethodInfos[] =
{
	&IList_1_IndexOf_m41551_MethodInfo,
	&IList_1_Insert_m41552_MethodInfo,
	&IList_1_RemoveAt_m41553_MethodInfo,
	&IList_1_get_Item_m41549_MethodInfo,
	&IList_1_set_Item_m41550_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7303_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7302_il2cpp_TypeInfo,
	&IEnumerable_1_t7304_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7303_0_0_0;
extern Il2CppType IList_1_t7303_1_0_0;
struct IList_1_t7303;
extern Il2CppGenericClass IList_1_t7303_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7303_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7303_MethodInfos/* methods */
	, IList_1_t7303_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7303_il2cpp_TypeInfo/* element_class */
	, IList_1_t7303_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7303_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7303_0_0_0/* byval_arg */
	, &IList_1_t7303_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7303_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<OrbitCamera>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_13.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2766_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<OrbitCamera>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_13MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<OrbitCamera>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_9.h"
extern TypeInfo InvokableCall_1_t2767_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<OrbitCamera>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_9MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14249_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14251_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<OrbitCamera>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<OrbitCamera>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<OrbitCamera>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2766____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2766_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2766, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2766_FieldInfos[] =
{
	&CachedInvokableCall_1_t2766____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType OrbitCamera_t26_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2766_CachedInvokableCall_1__ctor_m14247_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &OrbitCamera_t26_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14247_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<OrbitCamera>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14247_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2766_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2766_CachedInvokableCall_1__ctor_m14247_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14247_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2766_CachedInvokableCall_1_Invoke_m14248_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14248_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<OrbitCamera>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14248_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2766_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2766_CachedInvokableCall_1_Invoke_m14248_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14248_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2766_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14247_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14248_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14248_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14252_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2766_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14248_MethodInfo,
	&InvokableCall_1_Find_m14252_MethodInfo,
};
extern Il2CppType UnityAction_1_t2768_0_0_0;
extern TypeInfo UnityAction_1_t2768_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisOrbitCamera_t26_m31821_MethodInfo;
extern TypeInfo OrbitCamera_t26_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14254_MethodInfo;
extern TypeInfo OrbitCamera_t26_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2766_RGCTXData[8] = 
{
	&UnityAction_1_t2768_0_0_0/* Type Usage */,
	&UnityAction_1_t2768_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisOrbitCamera_t26_m31821_MethodInfo/* Method Usage */,
	&OrbitCamera_t26_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14254_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14249_MethodInfo/* Method Usage */,
	&OrbitCamera_t26_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14251_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2766_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2766_1_0_0;
struct CachedInvokableCall_1_t2766;
extern Il2CppGenericClass CachedInvokableCall_1_t2766_GenericClass;
TypeInfo CachedInvokableCall_1_t2766_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2766_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2766_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2767_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2766_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2766_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2766_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2766_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2766_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2766_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2766_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2766)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<OrbitCamera>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_16.h"
extern TypeInfo UnityAction_1_t2768_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<OrbitCamera>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_16MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<OrbitCamera>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<OrbitCamera>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisOrbitCamera_t26_m31821(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<OrbitCamera>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<OrbitCamera>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<OrbitCamera>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<OrbitCamera>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<OrbitCamera>
extern Il2CppType UnityAction_1_t2768_0_0_1;
FieldInfo InvokableCall_1_t2767____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2768_0_0_1/* type */
	, &InvokableCall_1_t2767_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2767, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2767_FieldInfos[] =
{
	&InvokableCall_1_t2767____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2767_InvokableCall_1__ctor_m14249_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14249_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<OrbitCamera>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14249_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2767_InvokableCall_1__ctor_m14249_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14249_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2768_0_0_0;
static ParameterInfo InvokableCall_1_t2767_InvokableCall_1__ctor_m14250_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2768_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14250_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<OrbitCamera>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14250_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2767_InvokableCall_1__ctor_m14250_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14250_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2767_InvokableCall_1_Invoke_m14251_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14251_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<OrbitCamera>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14251_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2767_InvokableCall_1_Invoke_m14251_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14251_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2767_InvokableCall_1_Find_m14252_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14252_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<OrbitCamera>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14252_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2767_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2767_InvokableCall_1_Find_m14252_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14252_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2767_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14249_MethodInfo,
	&InvokableCall_1__ctor_m14250_MethodInfo,
	&InvokableCall_1_Invoke_m14251_MethodInfo,
	&InvokableCall_1_Find_m14252_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2767_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14251_MethodInfo,
	&InvokableCall_1_Find_m14252_MethodInfo,
};
extern TypeInfo UnityAction_1_t2768_il2cpp_TypeInfo;
extern TypeInfo OrbitCamera_t26_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2767_RGCTXData[5] = 
{
	&UnityAction_1_t2768_0_0_0/* Type Usage */,
	&UnityAction_1_t2768_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisOrbitCamera_t26_m31821_MethodInfo/* Method Usage */,
	&OrbitCamera_t26_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14254_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2767_0_0_0;
extern Il2CppType InvokableCall_1_t2767_1_0_0;
struct InvokableCall_1_t2767;
extern Il2CppGenericClass InvokableCall_1_t2767_GenericClass;
TypeInfo InvokableCall_1_t2767_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2767_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2767_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2767_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2767_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2767_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2767_0_0_0/* byval_arg */
	, &InvokableCall_1_t2767_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2767_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2767_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2767)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<OrbitCamera>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<OrbitCamera>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<OrbitCamera>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<OrbitCamera>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<OrbitCamera>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2768_UnityAction_1__ctor_m14253_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14253_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<OrbitCamera>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14253_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2768_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2768_UnityAction_1__ctor_m14253_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14253_GenericMethod/* genericMethod */

};
extern Il2CppType OrbitCamera_t26_0_0_0;
static ParameterInfo UnityAction_1_t2768_UnityAction_1_Invoke_m14254_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &OrbitCamera_t26_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14254_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<OrbitCamera>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14254_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2768_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2768_UnityAction_1_Invoke_m14254_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14254_GenericMethod/* genericMethod */

};
extern Il2CppType OrbitCamera_t26_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2768_UnityAction_1_BeginInvoke_m14255_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &OrbitCamera_t26_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14255_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<OrbitCamera>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14255_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2768_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2768_UnityAction_1_BeginInvoke_m14255_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14255_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2768_UnityAction_1_EndInvoke_m14256_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14256_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<OrbitCamera>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14256_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2768_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2768_UnityAction_1_EndInvoke_m14256_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14256_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2768_MethodInfos[] =
{
	&UnityAction_1__ctor_m14253_MethodInfo,
	&UnityAction_1_Invoke_m14254_MethodInfo,
	&UnityAction_1_BeginInvoke_m14255_MethodInfo,
	&UnityAction_1_EndInvoke_m14256_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14255_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14256_MethodInfo;
static MethodInfo* UnityAction_1_t2768_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14254_MethodInfo,
	&UnityAction_1_BeginInvoke_m14255_MethodInfo,
	&UnityAction_1_EndInvoke_m14256_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2768_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2768_1_0_0;
struct UnityAction_1_t2768;
extern Il2CppGenericClass UnityAction_1_t2768_GenericClass;
TypeInfo UnityAction_1_t2768_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2768_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2768_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2768_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2768_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2768_0_0_0/* byval_arg */
	, &UnityAction_1_t2768_1_0_0/* this_arg */
	, UnityAction_1_t2768_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2768_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2768)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5754_il2cpp_TypeInfo;

// TestParticles
#include "AssemblyU2DCSharp_TestParticles.h"


// T System.Collections.Generic.IEnumerator`1<TestParticles>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<TestParticles>
extern MethodInfo IEnumerator_1_get_Current_m41554_MethodInfo;
static PropertyInfo IEnumerator_1_t5754____Current_PropertyInfo = 
{
	&IEnumerator_1_t5754_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41554_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5754_PropertyInfos[] =
{
	&IEnumerator_1_t5754____Current_PropertyInfo,
	NULL
};
extern Il2CppType TestParticles_t28_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41554_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<TestParticles>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41554_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5754_il2cpp_TypeInfo/* declaring_type */
	, &TestParticles_t28_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41554_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5754_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41554_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5754_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5754_0_0_0;
extern Il2CppType IEnumerator_1_t5754_1_0_0;
struct IEnumerator_1_t5754;
extern Il2CppGenericClass IEnumerator_1_t5754_GenericClass;
TypeInfo IEnumerator_1_t5754_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5754_MethodInfos/* methods */
	, IEnumerator_1_t5754_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5754_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5754_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5754_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5754_0_0_0/* byval_arg */
	, &IEnumerator_1_t5754_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5754_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<TestParticles>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_33.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2769_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<TestParticles>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_33MethodDeclarations.h"

extern TypeInfo TestParticles_t28_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14261_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTestParticles_t28_m31823_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<TestParticles>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<TestParticles>(System.Int32)
#define Array_InternalArray__get_Item_TisTestParticles_t28_m31823(__this, p0, method) (TestParticles_t28 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<TestParticles>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<TestParticles>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<TestParticles>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<TestParticles>::MoveNext()
// T System.Array/InternalEnumerator`1<TestParticles>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<TestParticles>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2769____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2769_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2769, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2769____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2769_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2769, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2769_FieldInfos[] =
{
	&InternalEnumerator_1_t2769____array_0_FieldInfo,
	&InternalEnumerator_1_t2769____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14258_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2769____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2769_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14258_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2769____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2769_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14261_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2769_PropertyInfos[] =
{
	&InternalEnumerator_1_t2769____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2769____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2769_InternalEnumerator_1__ctor_m14257_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14257_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<TestParticles>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14257_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2769_InternalEnumerator_1__ctor_m14257_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14257_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14258_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<TestParticles>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14258_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2769_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14258_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14259_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<TestParticles>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14259_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2769_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14259_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14260_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<TestParticles>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14260_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2769_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14260_GenericMethod/* genericMethod */

};
extern Il2CppType TestParticles_t28_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14261_GenericMethod;
// T System.Array/InternalEnumerator`1<TestParticles>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14261_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2769_il2cpp_TypeInfo/* declaring_type */
	, &TestParticles_t28_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14261_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2769_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14257_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14258_MethodInfo,
	&InternalEnumerator_1_Dispose_m14259_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14260_MethodInfo,
	&InternalEnumerator_1_get_Current_m14261_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14260_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14259_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2769_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14258_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14260_MethodInfo,
	&InternalEnumerator_1_Dispose_m14259_MethodInfo,
	&InternalEnumerator_1_get_Current_m14261_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2769_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5754_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2769_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5754_il2cpp_TypeInfo, 7},
};
extern TypeInfo TestParticles_t28_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2769_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14261_MethodInfo/* Method Usage */,
	&TestParticles_t28_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTestParticles_t28_m31823_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2769_0_0_0;
extern Il2CppType InternalEnumerator_1_t2769_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2769_GenericClass;
TypeInfo InternalEnumerator_1_t2769_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2769_MethodInfos/* methods */
	, InternalEnumerator_1_t2769_PropertyInfos/* properties */
	, InternalEnumerator_1_t2769_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2769_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2769_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2769_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2769_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2769_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2769_1_0_0/* this_arg */
	, InternalEnumerator_1_t2769_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2769_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2769_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2769)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7305_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<TestParticles>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<TestParticles>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<TestParticles>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<TestParticles>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<TestParticles>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<TestParticles>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<TestParticles>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<TestParticles>
extern MethodInfo ICollection_1_get_Count_m41555_MethodInfo;
static PropertyInfo ICollection_1_t7305____Count_PropertyInfo = 
{
	&ICollection_1_t7305_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41555_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41556_MethodInfo;
static PropertyInfo ICollection_1_t7305____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7305_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41556_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7305_PropertyInfos[] =
{
	&ICollection_1_t7305____Count_PropertyInfo,
	&ICollection_1_t7305____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41555_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<TestParticles>::get_Count()
MethodInfo ICollection_1_get_Count_m41555_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7305_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41555_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41556_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<TestParticles>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41556_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7305_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41556_GenericMethod/* genericMethod */

};
extern Il2CppType TestParticles_t28_0_0_0;
extern Il2CppType TestParticles_t28_0_0_0;
static ParameterInfo ICollection_1_t7305_ICollection_1_Add_m41557_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TestParticles_t28_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41557_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<TestParticles>::Add(T)
MethodInfo ICollection_1_Add_m41557_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7305_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7305_ICollection_1_Add_m41557_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41557_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41558_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<TestParticles>::Clear()
MethodInfo ICollection_1_Clear_m41558_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7305_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41558_GenericMethod/* genericMethod */

};
extern Il2CppType TestParticles_t28_0_0_0;
static ParameterInfo ICollection_1_t7305_ICollection_1_Contains_m41559_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TestParticles_t28_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41559_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<TestParticles>::Contains(T)
MethodInfo ICollection_1_Contains_m41559_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7305_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7305_ICollection_1_Contains_m41559_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41559_GenericMethod/* genericMethod */

};
extern Il2CppType TestParticlesU5BU5D_t5168_0_0_0;
extern Il2CppType TestParticlesU5BU5D_t5168_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7305_ICollection_1_CopyTo_m41560_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TestParticlesU5BU5D_t5168_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41560_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<TestParticles>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41560_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7305_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7305_ICollection_1_CopyTo_m41560_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41560_GenericMethod/* genericMethod */

};
extern Il2CppType TestParticles_t28_0_0_0;
static ParameterInfo ICollection_1_t7305_ICollection_1_Remove_m41561_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TestParticles_t28_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41561_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<TestParticles>::Remove(T)
MethodInfo ICollection_1_Remove_m41561_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7305_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7305_ICollection_1_Remove_m41561_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41561_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7305_MethodInfos[] =
{
	&ICollection_1_get_Count_m41555_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41556_MethodInfo,
	&ICollection_1_Add_m41557_MethodInfo,
	&ICollection_1_Clear_m41558_MethodInfo,
	&ICollection_1_Contains_m41559_MethodInfo,
	&ICollection_1_CopyTo_m41560_MethodInfo,
	&ICollection_1_Remove_m41561_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7307_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7305_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7307_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7305_0_0_0;
extern Il2CppType ICollection_1_t7305_1_0_0;
struct ICollection_1_t7305;
extern Il2CppGenericClass ICollection_1_t7305_GenericClass;
TypeInfo ICollection_1_t7305_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7305_MethodInfos/* methods */
	, ICollection_1_t7305_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7305_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7305_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7305_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7305_0_0_0/* byval_arg */
	, &ICollection_1_t7305_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7305_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<TestParticles>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<TestParticles>
extern Il2CppType IEnumerator_1_t5754_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41562_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<TestParticles>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41562_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7307_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5754_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41562_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7307_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41562_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7307_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7307_0_0_0;
extern Il2CppType IEnumerable_1_t7307_1_0_0;
struct IEnumerable_1_t7307;
extern Il2CppGenericClass IEnumerable_1_t7307_GenericClass;
TypeInfo IEnumerable_1_t7307_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7307_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7307_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7307_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7307_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7307_0_0_0/* byval_arg */
	, &IEnumerable_1_t7307_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7307_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7306_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<TestParticles>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<TestParticles>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<TestParticles>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<TestParticles>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<TestParticles>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<TestParticles>
extern MethodInfo IList_1_get_Item_m41563_MethodInfo;
extern MethodInfo IList_1_set_Item_m41564_MethodInfo;
static PropertyInfo IList_1_t7306____Item_PropertyInfo = 
{
	&IList_1_t7306_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41563_MethodInfo/* get */
	, &IList_1_set_Item_m41564_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7306_PropertyInfos[] =
{
	&IList_1_t7306____Item_PropertyInfo,
	NULL
};
extern Il2CppType TestParticles_t28_0_0_0;
static ParameterInfo IList_1_t7306_IList_1_IndexOf_m41565_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TestParticles_t28_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41565_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<TestParticles>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41565_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7306_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7306_IList_1_IndexOf_m41565_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41565_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TestParticles_t28_0_0_0;
static ParameterInfo IList_1_t7306_IList_1_Insert_m41566_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TestParticles_t28_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41566_GenericMethod;
// System.Void System.Collections.Generic.IList`1<TestParticles>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41566_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7306_IList_1_Insert_m41566_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41566_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7306_IList_1_RemoveAt_m41567_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41567_GenericMethod;
// System.Void System.Collections.Generic.IList`1<TestParticles>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41567_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7306_IList_1_RemoveAt_m41567_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41567_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7306_IList_1_get_Item_m41563_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType TestParticles_t28_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41563_GenericMethod;
// T System.Collections.Generic.IList`1<TestParticles>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41563_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7306_il2cpp_TypeInfo/* declaring_type */
	, &TestParticles_t28_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7306_IList_1_get_Item_m41563_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41563_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TestParticles_t28_0_0_0;
static ParameterInfo IList_1_t7306_IList_1_set_Item_m41564_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TestParticles_t28_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41564_GenericMethod;
// System.Void System.Collections.Generic.IList`1<TestParticles>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41564_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7306_IList_1_set_Item_m41564_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41564_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7306_MethodInfos[] =
{
	&IList_1_IndexOf_m41565_MethodInfo,
	&IList_1_Insert_m41566_MethodInfo,
	&IList_1_RemoveAt_m41567_MethodInfo,
	&IList_1_get_Item_m41563_MethodInfo,
	&IList_1_set_Item_m41564_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7306_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7305_il2cpp_TypeInfo,
	&IEnumerable_1_t7307_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7306_0_0_0;
extern Il2CppType IList_1_t7306_1_0_0;
struct IList_1_t7306;
extern Il2CppGenericClass IList_1_t7306_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7306_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7306_MethodInfos/* methods */
	, IList_1_t7306_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7306_il2cpp_TypeInfo/* element_class */
	, IList_1_t7306_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7306_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7306_0_0_0/* byval_arg */
	, &IList_1_t7306_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7306_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<TestParticles>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_14.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2770_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<TestParticles>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_14MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<TestParticles>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_10.h"
extern TypeInfo InvokableCall_1_t2771_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<TestParticles>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_10MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14264_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14266_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<TestParticles>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<TestParticles>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<TestParticles>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2770____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2770_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2770, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2770_FieldInfos[] =
{
	&CachedInvokableCall_1_t2770____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType TestParticles_t28_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2770_CachedInvokableCall_1__ctor_m14262_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &TestParticles_t28_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14262_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<TestParticles>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14262_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2770_CachedInvokableCall_1__ctor_m14262_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14262_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2770_CachedInvokableCall_1_Invoke_m14263_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14263_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<TestParticles>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14263_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2770_CachedInvokableCall_1_Invoke_m14263_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14263_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2770_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14262_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14263_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14263_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14267_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2770_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14263_MethodInfo,
	&InvokableCall_1_Find_m14267_MethodInfo,
};
extern Il2CppType UnityAction_1_t2772_0_0_0;
extern TypeInfo UnityAction_1_t2772_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisTestParticles_t28_m31833_MethodInfo;
extern TypeInfo TestParticles_t28_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14269_MethodInfo;
extern TypeInfo TestParticles_t28_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2770_RGCTXData[8] = 
{
	&UnityAction_1_t2772_0_0_0/* Type Usage */,
	&UnityAction_1_t2772_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTestParticles_t28_m31833_MethodInfo/* Method Usage */,
	&TestParticles_t28_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14269_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14264_MethodInfo/* Method Usage */,
	&TestParticles_t28_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14266_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2770_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2770_1_0_0;
struct CachedInvokableCall_1_t2770;
extern Il2CppGenericClass CachedInvokableCall_1_t2770_GenericClass;
TypeInfo CachedInvokableCall_1_t2770_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2770_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2770_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2771_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2770_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2770_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2770_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2770_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2770_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2770_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2770_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2770)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<TestParticles>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_17.h"
extern TypeInfo UnityAction_1_t2772_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<TestParticles>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_17MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<TestParticles>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<TestParticles>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisTestParticles_t28_m31833(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<TestParticles>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<TestParticles>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<TestParticles>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<TestParticles>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<TestParticles>
extern Il2CppType UnityAction_1_t2772_0_0_1;
FieldInfo InvokableCall_1_t2771____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2772_0_0_1/* type */
	, &InvokableCall_1_t2771_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2771, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2771_FieldInfos[] =
{
	&InvokableCall_1_t2771____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2771_InvokableCall_1__ctor_m14264_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14264_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<TestParticles>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14264_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2771_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2771_InvokableCall_1__ctor_m14264_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14264_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2772_0_0_0;
static ParameterInfo InvokableCall_1_t2771_InvokableCall_1__ctor_m14265_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2772_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14265_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<TestParticles>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14265_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2771_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2771_InvokableCall_1__ctor_m14265_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14265_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2771_InvokableCall_1_Invoke_m14266_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14266_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<TestParticles>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14266_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2771_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2771_InvokableCall_1_Invoke_m14266_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14266_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2771_InvokableCall_1_Find_m14267_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14267_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<TestParticles>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14267_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2771_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2771_InvokableCall_1_Find_m14267_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14267_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2771_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14264_MethodInfo,
	&InvokableCall_1__ctor_m14265_MethodInfo,
	&InvokableCall_1_Invoke_m14266_MethodInfo,
	&InvokableCall_1_Find_m14267_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2771_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14266_MethodInfo,
	&InvokableCall_1_Find_m14267_MethodInfo,
};
extern TypeInfo UnityAction_1_t2772_il2cpp_TypeInfo;
extern TypeInfo TestParticles_t28_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2771_RGCTXData[5] = 
{
	&UnityAction_1_t2772_0_0_0/* Type Usage */,
	&UnityAction_1_t2772_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTestParticles_t28_m31833_MethodInfo/* Method Usage */,
	&TestParticles_t28_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14269_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2771_0_0_0;
extern Il2CppType InvokableCall_1_t2771_1_0_0;
struct InvokableCall_1_t2771;
extern Il2CppGenericClass InvokableCall_1_t2771_GenericClass;
TypeInfo InvokableCall_1_t2771_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2771_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2771_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2771_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2771_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2771_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2771_0_0_0/* byval_arg */
	, &InvokableCall_1_t2771_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2771_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2771_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2771)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<TestParticles>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<TestParticles>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<TestParticles>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<TestParticles>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<TestParticles>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2772_UnityAction_1__ctor_m14268_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14268_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<TestParticles>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14268_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2772_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2772_UnityAction_1__ctor_m14268_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14268_GenericMethod/* genericMethod */

};
extern Il2CppType TestParticles_t28_0_0_0;
static ParameterInfo UnityAction_1_t2772_UnityAction_1_Invoke_m14269_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TestParticles_t28_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14269_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<TestParticles>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14269_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2772_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2772_UnityAction_1_Invoke_m14269_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14269_GenericMethod/* genericMethod */

};
extern Il2CppType TestParticles_t28_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2772_UnityAction_1_BeginInvoke_m14270_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TestParticles_t28_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14270_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<TestParticles>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14270_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2772_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2772_UnityAction_1_BeginInvoke_m14270_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14270_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2772_UnityAction_1_EndInvoke_m14271_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14271_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<TestParticles>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14271_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2772_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2772_UnityAction_1_EndInvoke_m14271_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14271_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2772_MethodInfos[] =
{
	&UnityAction_1__ctor_m14268_MethodInfo,
	&UnityAction_1_Invoke_m14269_MethodInfo,
	&UnityAction_1_BeginInvoke_m14270_MethodInfo,
	&UnityAction_1_EndInvoke_m14271_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14270_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14271_MethodInfo;
static MethodInfo* UnityAction_1_t2772_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14269_MethodInfo,
	&UnityAction_1_BeginInvoke_m14270_MethodInfo,
	&UnityAction_1_EndInvoke_m14271_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2772_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2772_1_0_0;
struct UnityAction_1_t2772;
extern Il2CppGenericClass UnityAction_1_t2772_GenericClass;
TypeInfo UnityAction_1_t2772_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2772_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2772_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2772_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2772_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2772_0_0_0/* byval_arg */
	, &UnityAction_1_t2772_1_0_0/* this_arg */
	, UnityAction_1_t2772_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2772_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2772)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5756_il2cpp_TypeInfo;

// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject>
extern MethodInfo IEnumerator_1_get_Current_m41568_MethodInfo;
static PropertyInfo IEnumerator_1_t5756____Current_PropertyInfo = 
{
	&IEnumerator_1_t5756_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41568_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5756_PropertyInfos[] =
{
	&IEnumerator_1_t5756____Current_PropertyInfo,
	NULL
};
extern Il2CppType GameObject_t2_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41568_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41568_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5756_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t2_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41568_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5756_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41568_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5756_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5756_0_0_0;
extern Il2CppType IEnumerator_1_t5756_1_0_0;
struct IEnumerator_1_t5756;
extern Il2CppGenericClass IEnumerator_1_t5756_GenericClass;
TypeInfo IEnumerator_1_t5756_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5756_MethodInfos/* methods */
	, IEnumerator_1_t5756_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5756_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5756_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5756_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5756_0_0_0/* byval_arg */
	, &IEnumerator_1_t5756_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5756_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.GameObject>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_34.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2773_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.GameObject>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_34MethodDeclarations.h"

extern TypeInfo GameObject_t2_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14276_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGameObject_t2_m31835_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.GameObject>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.GameObject>(System.Int32)
#define Array_InternalArray__get_Item_TisGameObject_t2_m31835(__this, p0, method) (GameObject_t2 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.GameObject>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.GameObject>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GameObject>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.GameObject>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.GameObject>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.GameObject>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2773____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2773, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2773____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2773, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2773_FieldInfos[] =
{
	&InternalEnumerator_1_t2773____array_0_FieldInfo,
	&InternalEnumerator_1_t2773____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14273_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2773____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2773_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14273_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2773____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2773_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14276_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2773_PropertyInfos[] =
{
	&InternalEnumerator_1_t2773____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2773____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2773_InternalEnumerator_1__ctor_m14272_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14272_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GameObject>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14272_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2773_InternalEnumerator_1__ctor_m14272_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14272_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14273_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.GameObject>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14273_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14273_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14274_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GameObject>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14274_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14274_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14275_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.GameObject>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14275_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14275_GenericMethod/* genericMethod */

};
extern Il2CppType GameObject_t2_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14276_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.GameObject>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14276_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t2_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14276_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2773_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14272_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14273_MethodInfo,
	&InternalEnumerator_1_Dispose_m14274_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14275_MethodInfo,
	&InternalEnumerator_1_get_Current_m14276_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14275_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14274_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2773_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14273_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14275_MethodInfo,
	&InternalEnumerator_1_Dispose_m14274_MethodInfo,
	&InternalEnumerator_1_get_Current_m14276_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2773_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5756_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2773_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5756_il2cpp_TypeInfo, 7},
};
extern TypeInfo GameObject_t2_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2773_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14276_MethodInfo/* Method Usage */,
	&GameObject_t2_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisGameObject_t2_m31835_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2773_0_0_0;
extern Il2CppType InternalEnumerator_1_t2773_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2773_GenericClass;
TypeInfo InternalEnumerator_1_t2773_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2773_MethodInfos/* methods */
	, InternalEnumerator_1_t2773_PropertyInfos/* properties */
	, InternalEnumerator_1_t2773_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2773_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2773_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2773_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2773_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2773_1_0_0/* this_arg */
	, InternalEnumerator_1_t2773_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2773_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2773_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2773)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7308_il2cpp_TypeInfo;

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.GameObject>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GameObject>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GameObject>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GameObject>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GameObject>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GameObject>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GameObject>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.GameObject>
extern MethodInfo ICollection_1_get_Count_m41569_MethodInfo;
static PropertyInfo ICollection_1_t7308____Count_PropertyInfo = 
{
	&ICollection_1_t7308_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41569_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41570_MethodInfo;
static PropertyInfo ICollection_1_t7308____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7308_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41570_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7308_PropertyInfos[] =
{
	&ICollection_1_t7308____Count_PropertyInfo,
	&ICollection_1_t7308____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41569_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.GameObject>::get_Count()
MethodInfo ICollection_1_get_Count_m41569_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7308_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41569_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41570_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GameObject>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41570_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7308_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41570_GenericMethod/* genericMethod */

};
extern Il2CppType GameObject_t2_0_0_0;
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo ICollection_1_t7308_ICollection_1_Add_m41571_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41571_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GameObject>::Add(T)
MethodInfo ICollection_1_Add_m41571_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7308_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7308_ICollection_1_Add_m41571_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41571_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41572_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GameObject>::Clear()
MethodInfo ICollection_1_Clear_m41572_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7308_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41572_GenericMethod/* genericMethod */

};
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo ICollection_1_t7308_ICollection_1_Contains_m41573_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41573_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GameObject>::Contains(T)
MethodInfo ICollection_1_Contains_m41573_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7308_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7308_ICollection_1_Contains_m41573_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41573_GenericMethod/* genericMethod */

};
extern Il2CppType GameObjectU5BU5D_t27_0_0_0;
extern Il2CppType GameObjectU5BU5D_t27_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7308_ICollection_1_CopyTo_m41574_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GameObjectU5BU5D_t27_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41574_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.GameObject>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41574_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7308_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7308_ICollection_1_CopyTo_m41574_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41574_GenericMethod/* genericMethod */

};
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo ICollection_1_t7308_ICollection_1_Remove_m41575_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41575_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.GameObject>::Remove(T)
MethodInfo ICollection_1_Remove_m41575_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7308_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7308_ICollection_1_Remove_m41575_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41575_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7308_MethodInfos[] =
{
	&ICollection_1_get_Count_m41569_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41570_MethodInfo,
	&ICollection_1_Add_m41571_MethodInfo,
	&ICollection_1_Clear_m41572_MethodInfo,
	&ICollection_1_Contains_m41573_MethodInfo,
	&ICollection_1_CopyTo_m41574_MethodInfo,
	&ICollection_1_Remove_m41575_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7310_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7308_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7310_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7308_0_0_0;
extern Il2CppType ICollection_1_t7308_1_0_0;
struct ICollection_1_t7308;
extern Il2CppGenericClass ICollection_1_t7308_GenericClass;
TypeInfo ICollection_1_t7308_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7308_MethodInfos/* methods */
	, ICollection_1_t7308_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7308_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7308_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7308_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7308_0_0_0/* byval_arg */
	, &ICollection_1_t7308_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7308_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>
extern Il2CppType IEnumerator_1_t5756_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41576_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41576_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7310_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5756_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41576_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7310_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41576_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7310_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7310_0_0_0;
extern Il2CppType IEnumerable_1_t7310_1_0_0;
struct IEnumerable_1_t7310;
extern Il2CppGenericClass IEnumerable_1_t7310_GenericClass;
TypeInfo IEnumerable_1_t7310_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7310_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7310_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7310_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7310_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7310_0_0_0/* byval_arg */
	, &IEnumerable_1_t7310_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7310_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7309_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.GameObject>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.GameObject>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.GameObject>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.GameObject>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.GameObject>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.GameObject>
extern MethodInfo IList_1_get_Item_m41577_MethodInfo;
extern MethodInfo IList_1_set_Item_m41578_MethodInfo;
static PropertyInfo IList_1_t7309____Item_PropertyInfo = 
{
	&IList_1_t7309_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41577_MethodInfo/* get */
	, &IList_1_set_Item_m41578_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7309_PropertyInfos[] =
{
	&IList_1_t7309____Item_PropertyInfo,
	NULL
};
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo IList_1_t7309_IList_1_IndexOf_m41579_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41579_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.GameObject>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41579_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7309_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7309_IList_1_IndexOf_m41579_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41579_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo IList_1_t7309_IList_1_Insert_m41580_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41580_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.GameObject>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41580_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7309_IList_1_Insert_m41580_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41580_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7309_IList_1_RemoveAt_m41581_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41581_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.GameObject>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41581_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7309_IList_1_RemoveAt_m41581_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41581_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7309_IList_1_get_Item_m41577_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType GameObject_t2_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41577_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.GameObject>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41577_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7309_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t2_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7309_IList_1_get_Item_m41577_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41577_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo IList_1_t7309_IList_1_set_Item_m41578_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41578_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.GameObject>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41578_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7309_IList_1_set_Item_m41578_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41578_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7309_MethodInfos[] =
{
	&IList_1_IndexOf_m41579_MethodInfo,
	&IList_1_Insert_m41580_MethodInfo,
	&IList_1_RemoveAt_m41581_MethodInfo,
	&IList_1_get_Item_m41577_MethodInfo,
	&IList_1_set_Item_m41578_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7309_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7308_il2cpp_TypeInfo,
	&IEnumerable_1_t7310_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7309_0_0_0;
extern Il2CppType IList_1_t7309_1_0_0;
struct IList_1_t7309;
extern Il2CppGenericClass IList_1_t7309_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7309_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7309_MethodInfos/* methods */
	, IList_1_t7309_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7309_il2cpp_TypeInfo/* element_class */
	, IList_1_t7309_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7309_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7309_0_0_0/* byval_arg */
	, &IList_1_t7309_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7309_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5758_il2cpp_TypeInfo;

// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.BackgroundPlaneBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41582_MethodInfo;
static PropertyInfo IEnumerator_1_t5758____Current_PropertyInfo = 
{
	&IEnumerator_1_t5758_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41582_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5758_PropertyInfos[] =
{
	&IEnumerator_1_t5758____Current_PropertyInfo,
	NULL
};
extern Il2CppType BackgroundPlaneBehaviour_t29_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41582_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41582_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5758_il2cpp_TypeInfo/* declaring_type */
	, &BackgroundPlaneBehaviour_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41582_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5758_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41582_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5758_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5758_0_0_0;
extern Il2CppType IEnumerator_1_t5758_1_0_0;
struct IEnumerator_1_t5758;
extern Il2CppGenericClass IEnumerator_1_t5758_GenericClass;
TypeInfo IEnumerator_1_t5758_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5758_MethodInfos/* methods */
	, IEnumerator_1_t5758_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5758_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5758_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5758_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5758_0_0_0/* byval_arg */
	, &IEnumerator_1_t5758_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5758_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_35.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2774_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_35MethodDeclarations.h"

extern TypeInfo BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14281_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisBackgroundPlaneBehaviour_t29_m31846_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.BackgroundPlaneBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.BackgroundPlaneBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisBackgroundPlaneBehaviour_t29_m31846(__this, p0, method) (BackgroundPlaneBehaviour_t29 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2774____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2774, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2774____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2774, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2774_FieldInfos[] =
{
	&InternalEnumerator_1_t2774____array_0_FieldInfo,
	&InternalEnumerator_1_t2774____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14278_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2774____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2774_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14278_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2774____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2774_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14281_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2774_PropertyInfos[] =
{
	&InternalEnumerator_1_t2774____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2774____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2774_InternalEnumerator_1__ctor_m14277_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14277_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14277_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2774_InternalEnumerator_1__ctor_m14277_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14277_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14278_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14278_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14278_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14279_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14279_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14279_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14280_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14280_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14280_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneBehaviour_t29_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14281_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14281_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* declaring_type */
	, &BackgroundPlaneBehaviour_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14281_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2774_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14277_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14278_MethodInfo,
	&InternalEnumerator_1_Dispose_m14279_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14280_MethodInfo,
	&InternalEnumerator_1_get_Current_m14281_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14280_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14279_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2774_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14278_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14280_MethodInfo,
	&InternalEnumerator_1_Dispose_m14279_MethodInfo,
	&InternalEnumerator_1_get_Current_m14281_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2774_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5758_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2774_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5758_il2cpp_TypeInfo, 7},
};
extern TypeInfo BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2774_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14281_MethodInfo/* Method Usage */,
	&BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisBackgroundPlaneBehaviour_t29_m31846_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2774_0_0_0;
extern Il2CppType InternalEnumerator_1_t2774_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2774_GenericClass;
TypeInfo InternalEnumerator_1_t2774_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2774_MethodInfos/* methods */
	, InternalEnumerator_1_t2774_PropertyInfos/* properties */
	, InternalEnumerator_1_t2774_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2774_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2774_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2774_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2774_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2774_1_0_0/* this_arg */
	, InternalEnumerator_1_t2774_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2774_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2774_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2774)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7311_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>
extern MethodInfo ICollection_1_get_Count_m41583_MethodInfo;
static PropertyInfo ICollection_1_t7311____Count_PropertyInfo = 
{
	&ICollection_1_t7311_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41583_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41584_MethodInfo;
static PropertyInfo ICollection_1_t7311____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7311_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41584_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7311_PropertyInfos[] =
{
	&ICollection_1_t7311____Count_PropertyInfo,
	&ICollection_1_t7311____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41583_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41583_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7311_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41583_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41584_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41584_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7311_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41584_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneBehaviour_t29_0_0_0;
extern Il2CppType BackgroundPlaneBehaviour_t29_0_0_0;
static ParameterInfo ICollection_1_t7311_ICollection_1_Add_m41585_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t29_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41585_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41585_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7311_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7311_ICollection_1_Add_m41585_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41585_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41586_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41586_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7311_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41586_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneBehaviour_t29_0_0_0;
static ParameterInfo ICollection_1_t7311_ICollection_1_Contains_m41587_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t29_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41587_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41587_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7311_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7311_ICollection_1_Contains_m41587_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41587_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneBehaviourU5BU5D_t5169_0_0_0;
extern Il2CppType BackgroundPlaneBehaviourU5BU5D_t5169_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7311_ICollection_1_CopyTo_m41588_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviourU5BU5D_t5169_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41588_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41588_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7311_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7311_ICollection_1_CopyTo_m41588_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41588_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneBehaviour_t29_0_0_0;
static ParameterInfo ICollection_1_t7311_ICollection_1_Remove_m41589_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t29_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41589_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41589_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7311_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7311_ICollection_1_Remove_m41589_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41589_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7311_MethodInfos[] =
{
	&ICollection_1_get_Count_m41583_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41584_MethodInfo,
	&ICollection_1_Add_m41585_MethodInfo,
	&ICollection_1_Clear_m41586_MethodInfo,
	&ICollection_1_Contains_m41587_MethodInfo,
	&ICollection_1_CopyTo_m41588_MethodInfo,
	&ICollection_1_Remove_m41589_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7313_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7311_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7313_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7311_0_0_0;
extern Il2CppType ICollection_1_t7311_1_0_0;
struct ICollection_1_t7311;
extern Il2CppGenericClass ICollection_1_t7311_GenericClass;
TypeInfo ICollection_1_t7311_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7311_MethodInfos/* methods */
	, ICollection_1_t7311_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7311_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7311_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7311_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7311_0_0_0/* byval_arg */
	, &ICollection_1_t7311_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7311_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.BackgroundPlaneBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.BackgroundPlaneBehaviour>
extern Il2CppType IEnumerator_1_t5758_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41590_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.BackgroundPlaneBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41590_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7313_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5758_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41590_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7313_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41590_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7313_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7313_0_0_0;
extern Il2CppType IEnumerable_1_t7313_1_0_0;
struct IEnumerable_1_t7313;
extern Il2CppGenericClass IEnumerable_1_t7313_GenericClass;
TypeInfo IEnumerable_1_t7313_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7313_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7313_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7313_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7313_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7313_0_0_0/* byval_arg */
	, &IEnumerable_1_t7313_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7313_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7312_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>
extern MethodInfo IList_1_get_Item_m41591_MethodInfo;
extern MethodInfo IList_1_set_Item_m41592_MethodInfo;
static PropertyInfo IList_1_t7312____Item_PropertyInfo = 
{
	&IList_1_t7312_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41591_MethodInfo/* get */
	, &IList_1_set_Item_m41592_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7312_PropertyInfos[] =
{
	&IList_1_t7312____Item_PropertyInfo,
	NULL
};
extern Il2CppType BackgroundPlaneBehaviour_t29_0_0_0;
static ParameterInfo IList_1_t7312_IList_1_IndexOf_m41593_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t29_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41593_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41593_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7312_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7312_IList_1_IndexOf_m41593_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41593_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType BackgroundPlaneBehaviour_t29_0_0_0;
static ParameterInfo IList_1_t7312_IList_1_Insert_m41594_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t29_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41594_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41594_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7312_IList_1_Insert_m41594_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41594_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7312_IList_1_RemoveAt_m41595_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41595_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41595_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7312_IList_1_RemoveAt_m41595_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41595_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7312_IList_1_get_Item_m41591_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType BackgroundPlaneBehaviour_t29_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41591_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41591_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7312_il2cpp_TypeInfo/* declaring_type */
	, &BackgroundPlaneBehaviour_t29_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7312_IList_1_get_Item_m41591_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41591_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType BackgroundPlaneBehaviour_t29_0_0_0;
static ParameterInfo IList_1_t7312_IList_1_set_Item_m41592_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t29_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41592_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41592_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7312_IList_1_set_Item_m41592_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41592_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7312_MethodInfos[] =
{
	&IList_1_IndexOf_m41593_MethodInfo,
	&IList_1_Insert_m41594_MethodInfo,
	&IList_1_RemoveAt_m41595_MethodInfo,
	&IList_1_get_Item_m41591_MethodInfo,
	&IList_1_set_Item_m41592_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7312_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7311_il2cpp_TypeInfo,
	&IEnumerable_1_t7313_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7312_0_0_0;
extern Il2CppType IList_1_t7312_1_0_0;
struct IList_1_t7312;
extern Il2CppGenericClass IList_1_t7312_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7312_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7312_MethodInfos/* methods */
	, IList_1_t7312_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7312_il2cpp_TypeInfo/* element_class */
	, IList_1_t7312_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7312_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7312_0_0_0/* byval_arg */
	, &IList_1_t7312_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7312_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7314_il2cpp_TypeInfo;

// Vuforia.BackgroundPlaneAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbst.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m41596_MethodInfo;
static PropertyInfo ICollection_1_t7314____Count_PropertyInfo = 
{
	&ICollection_1_t7314_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41596_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41597_MethodInfo;
static PropertyInfo ICollection_1_t7314____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7314_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41597_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7314_PropertyInfos[] =
{
	&ICollection_1_t7314____Count_PropertyInfo,
	&ICollection_1_t7314____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41596_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41596_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7314_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41596_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41597_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41597_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7314_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41597_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t30_0_0_0;
extern Il2CppType BackgroundPlaneAbstractBehaviour_t30_0_0_0;
static ParameterInfo ICollection_1_t7314_ICollection_1_Add_m41598_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneAbstractBehaviour_t30_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41598_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41598_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7314_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7314_ICollection_1_Add_m41598_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41598_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41599_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41599_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7314_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41599_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t30_0_0_0;
static ParameterInfo ICollection_1_t7314_ICollection_1_Contains_m41600_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneAbstractBehaviour_t30_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41600_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41600_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7314_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7314_ICollection_1_Contains_m41600_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41600_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneAbstractBehaviourU5BU5D_t894_0_0_0;
extern Il2CppType BackgroundPlaneAbstractBehaviourU5BU5D_t894_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7314_ICollection_1_CopyTo_m41601_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneAbstractBehaviourU5BU5D_t894_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41601_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41601_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7314_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7314_ICollection_1_CopyTo_m41601_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41601_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t30_0_0_0;
static ParameterInfo ICollection_1_t7314_ICollection_1_Remove_m41602_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneAbstractBehaviour_t30_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41602_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41602_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7314_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7314_ICollection_1_Remove_m41602_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41602_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7314_MethodInfos[] =
{
	&ICollection_1_get_Count_m41596_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41597_MethodInfo,
	&ICollection_1_Add_m41598_MethodInfo,
	&ICollection_1_Clear_m41599_MethodInfo,
	&ICollection_1_Contains_m41600_MethodInfo,
	&ICollection_1_CopyTo_m41601_MethodInfo,
	&ICollection_1_Remove_m41602_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7316_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7314_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7316_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7314_0_0_0;
extern Il2CppType ICollection_1_t7314_1_0_0;
struct ICollection_1_t7314;
extern Il2CppGenericClass ICollection_1_t7314_GenericClass;
TypeInfo ICollection_1_t7314_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7314_MethodInfos/* methods */
	, ICollection_1_t7314_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7314_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7314_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7314_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7314_0_0_0/* byval_arg */
	, &ICollection_1_t7314_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7314_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.BackgroundPlaneAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.BackgroundPlaneAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5760_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41603_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.BackgroundPlaneAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41603_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7316_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5760_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41603_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7316_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41603_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7316_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7316_0_0_0;
extern Il2CppType IEnumerable_1_t7316_1_0_0;
struct IEnumerable_1_t7316;
extern Il2CppGenericClass IEnumerable_1_t7316_GenericClass;
TypeInfo IEnumerable_1_t7316_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7316_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7316_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7316_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7316_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7316_0_0_0/* byval_arg */
	, &IEnumerable_1_t7316_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7316_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5760_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41604_MethodInfo;
static PropertyInfo IEnumerator_1_t5760____Current_PropertyInfo = 
{
	&IEnumerator_1_t5760_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41604_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5760_PropertyInfos[] =
{
	&IEnumerator_1_t5760____Current_PropertyInfo,
	NULL
};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t30_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41604_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41604_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5760_il2cpp_TypeInfo/* declaring_type */
	, &BackgroundPlaneAbstractBehaviour_t30_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41604_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5760_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41604_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5760_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5760_0_0_0;
extern Il2CppType IEnumerator_1_t5760_1_0_0;
struct IEnumerator_1_t5760;
extern Il2CppGenericClass IEnumerator_1_t5760_GenericClass;
TypeInfo IEnumerator_1_t5760_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5760_MethodInfos/* methods */
	, IEnumerator_1_t5760_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5760_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5760_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5760_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5760_0_0_0/* byval_arg */
	, &IEnumerator_1_t5760_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5760_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_36.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2775_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_36MethodDeclarations.h"

extern TypeInfo BackgroundPlaneAbstractBehaviour_t30_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14286_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisBackgroundPlaneAbstractBehaviour_t30_m31857_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.BackgroundPlaneAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.BackgroundPlaneAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisBackgroundPlaneAbstractBehaviour_t30_m31857(__this, p0, method) (BackgroundPlaneAbstractBehaviour_t30 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2775____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2775, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2775____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2775, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2775_FieldInfos[] =
{
	&InternalEnumerator_1_t2775____array_0_FieldInfo,
	&InternalEnumerator_1_t2775____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14283_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2775____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2775_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14283_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2775____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2775_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14286_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2775_PropertyInfos[] =
{
	&InternalEnumerator_1_t2775____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2775____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2775_InternalEnumerator_1__ctor_m14282_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14282_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14282_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2775_InternalEnumerator_1__ctor_m14282_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14282_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14283_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14283_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14283_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14284_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14284_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14284_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14285_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14285_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14285_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t30_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14286_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14286_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* declaring_type */
	, &BackgroundPlaneAbstractBehaviour_t30_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14286_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2775_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14282_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14283_MethodInfo,
	&InternalEnumerator_1_Dispose_m14284_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14285_MethodInfo,
	&InternalEnumerator_1_get_Current_m14286_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14285_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14284_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2775_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14283_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14285_MethodInfo,
	&InternalEnumerator_1_Dispose_m14284_MethodInfo,
	&InternalEnumerator_1_get_Current_m14286_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2775_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5760_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2775_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5760_il2cpp_TypeInfo, 7},
};
extern TypeInfo BackgroundPlaneAbstractBehaviour_t30_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2775_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14286_MethodInfo/* Method Usage */,
	&BackgroundPlaneAbstractBehaviour_t30_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisBackgroundPlaneAbstractBehaviour_t30_m31857_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2775_0_0_0;
extern Il2CppType InternalEnumerator_1_t2775_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2775_GenericClass;
TypeInfo InternalEnumerator_1_t2775_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2775_MethodInfos/* methods */
	, InternalEnumerator_1_t2775_PropertyInfos/* properties */
	, InternalEnumerator_1_t2775_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2775_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2775_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2775_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2775_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2775_1_0_0/* this_arg */
	, InternalEnumerator_1_t2775_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2775_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2775_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2775)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7315_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m41605_MethodInfo;
extern MethodInfo IList_1_set_Item_m41606_MethodInfo;
static PropertyInfo IList_1_t7315____Item_PropertyInfo = 
{
	&IList_1_t7315_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41605_MethodInfo/* get */
	, &IList_1_set_Item_m41606_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7315_PropertyInfos[] =
{
	&IList_1_t7315____Item_PropertyInfo,
	NULL
};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t30_0_0_0;
static ParameterInfo IList_1_t7315_IList_1_IndexOf_m41607_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneAbstractBehaviour_t30_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41607_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41607_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7315_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7315_IList_1_IndexOf_m41607_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41607_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType BackgroundPlaneAbstractBehaviour_t30_0_0_0;
static ParameterInfo IList_1_t7315_IList_1_Insert_m41608_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneAbstractBehaviour_t30_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41608_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41608_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7315_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7315_IList_1_Insert_m41608_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41608_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7315_IList_1_RemoveAt_m41609_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41609_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41609_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7315_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7315_IList_1_RemoveAt_m41609_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41609_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7315_IList_1_get_Item_m41605_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t30_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41605_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41605_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7315_il2cpp_TypeInfo/* declaring_type */
	, &BackgroundPlaneAbstractBehaviour_t30_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7315_IList_1_get_Item_m41605_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41605_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType BackgroundPlaneAbstractBehaviour_t30_0_0_0;
static ParameterInfo IList_1_t7315_IList_1_set_Item_m41606_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneAbstractBehaviour_t30_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41606_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.BackgroundPlaneAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41606_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7315_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7315_IList_1_set_Item_m41606_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41606_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7315_MethodInfos[] =
{
	&IList_1_IndexOf_m41607_MethodInfo,
	&IList_1_Insert_m41608_MethodInfo,
	&IList_1_RemoveAt_m41609_MethodInfo,
	&IList_1_get_Item_m41605_MethodInfo,
	&IList_1_set_Item_m41606_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7315_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7314_il2cpp_TypeInfo,
	&IEnumerable_1_t7316_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7315_0_0_0;
extern Il2CppType IList_1_t7315_1_0_0;
struct IList_1_t7315;
extern Il2CppGenericClass IList_1_t7315_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7315_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7315_MethodInfos/* methods */
	, IList_1_t7315_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7315_il2cpp_TypeInfo/* element_class */
	, IList_1_t7315_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7315_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7315_0_0_0/* byval_arg */
	, &IList_1_t7315_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7315_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t4330_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>
extern MethodInfo ICollection_1_get_Count_m41610_MethodInfo;
static PropertyInfo ICollection_1_t4330____Count_PropertyInfo = 
{
	&ICollection_1_t4330_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41610_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41611_MethodInfo;
static PropertyInfo ICollection_1_t4330____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t4330_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41611_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t4330_PropertyInfos[] =
{
	&ICollection_1_t4330____Count_PropertyInfo,
	&ICollection_1_t4330____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41610_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m41610_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t4330_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41610_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41611_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41611_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t4330_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41611_GenericMethod/* genericMethod */

};
extern Il2CppType IVideoBackgroundEventHandler_t122_0_0_0;
extern Il2CppType IVideoBackgroundEventHandler_t122_0_0_0;
static ParameterInfo ICollection_1_t4330_ICollection_1_Add_m41612_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IVideoBackgroundEventHandler_t122_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41612_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Add(T)
MethodInfo ICollection_1_Add_m41612_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t4330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t4330_ICollection_1_Add_m41612_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41612_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41613_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Clear()
MethodInfo ICollection_1_Clear_m41613_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t4330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41613_GenericMethod/* genericMethod */

};
extern Il2CppType IVideoBackgroundEventHandler_t122_0_0_0;
static ParameterInfo ICollection_1_t4330_ICollection_1_Contains_m41614_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IVideoBackgroundEventHandler_t122_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41614_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m41614_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t4330_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t4330_ICollection_1_Contains_m41614_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41614_GenericMethod/* genericMethod */

};
extern Il2CppType IVideoBackgroundEventHandlerU5BU5D_t4327_0_0_0;
extern Il2CppType IVideoBackgroundEventHandlerU5BU5D_t4327_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t4330_ICollection_1_CopyTo_m41615_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IVideoBackgroundEventHandlerU5BU5D_t4327_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41615_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41615_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t4330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t4330_ICollection_1_CopyTo_m41615_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41615_GenericMethod/* genericMethod */

};
extern Il2CppType IVideoBackgroundEventHandler_t122_0_0_0;
static ParameterInfo ICollection_1_t4330_ICollection_1_Remove_m41616_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IVideoBackgroundEventHandler_t122_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41616_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IVideoBackgroundEventHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m41616_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t4330_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t4330_ICollection_1_Remove_m41616_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41616_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t4330_MethodInfos[] =
{
	&ICollection_1_get_Count_m41610_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41611_MethodInfo,
	&ICollection_1_Add_m41612_MethodInfo,
	&ICollection_1_Clear_m41613_MethodInfo,
	&ICollection_1_Contains_m41614_MethodInfo,
	&ICollection_1_CopyTo_m41615_MethodInfo,
	&ICollection_1_Remove_m41616_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t4328_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t4330_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t4328_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t4330_0_0_0;
extern Il2CppType ICollection_1_t4330_1_0_0;
struct ICollection_1_t4330;
extern Il2CppGenericClass ICollection_1_t4330_GenericClass;
TypeInfo ICollection_1_t4330_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t4330_MethodInfos/* methods */
	, ICollection_1_t4330_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t4330_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t4330_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t4330_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t4330_0_0_0/* byval_arg */
	, &ICollection_1_t4330_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t4330_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IVideoBackgroundEventHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IVideoBackgroundEventHandler>
extern Il2CppType IEnumerator_1_t4329_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41617_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IVideoBackgroundEventHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41617_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t4328_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t4329_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41617_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t4328_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41617_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t4328_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t4328_0_0_0;
extern Il2CppType IEnumerable_1_t4328_1_0_0;
struct IEnumerable_1_t4328;
extern Il2CppGenericClass IEnumerable_1_t4328_GenericClass;
TypeInfo IEnumerable_1_t4328_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t4328_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t4328_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t4328_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t4328_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t4328_0_0_0/* byval_arg */
	, &IEnumerable_1_t4328_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t4328_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t4329_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IVideoBackgroundEventHandler>
extern MethodInfo IEnumerator_1_get_Current_m41618_MethodInfo;
static PropertyInfo IEnumerator_1_t4329____Current_PropertyInfo = 
{
	&IEnumerator_1_t4329_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41618_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t4329_PropertyInfos[] =
{
	&IEnumerator_1_t4329____Current_PropertyInfo,
	NULL
};
extern Il2CppType IVideoBackgroundEventHandler_t122_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41618_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41618_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t4329_il2cpp_TypeInfo/* declaring_type */
	, &IVideoBackgroundEventHandler_t122_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41618_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t4329_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41618_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t4329_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t4329_0_0_0;
extern Il2CppType IEnumerator_1_t4329_1_0_0;
struct IEnumerator_1_t4329;
extern Il2CppGenericClass IEnumerator_1_t4329_GenericClass;
TypeInfo IEnumerator_1_t4329_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t4329_MethodInfos/* methods */
	, IEnumerator_1_t4329_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t4329_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t4329_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t4329_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t4329_0_0_0/* byval_arg */
	, &IEnumerator_1_t4329_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t4329_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_37.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2776_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_37MethodDeclarations.h"

extern TypeInfo IVideoBackgroundEventHandler_t122_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14291_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIVideoBackgroundEventHandler_t122_m31868_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IVideoBackgroundEventHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IVideoBackgroundEventHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisIVideoBackgroundEventHandler_t122_m31868(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2776____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2776, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2776____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2776, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2776_FieldInfos[] =
{
	&InternalEnumerator_1_t2776____array_0_FieldInfo,
	&InternalEnumerator_1_t2776____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14288_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2776____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2776_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14288_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2776____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2776_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14291_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2776_PropertyInfos[] =
{
	&InternalEnumerator_1_t2776____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2776____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2776_InternalEnumerator_1__ctor_m14287_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14287_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14287_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2776_InternalEnumerator_1__ctor_m14287_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14287_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14288_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14288_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14288_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14289_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14289_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14289_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14290_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14290_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14290_GenericMethod/* genericMethod */

};
extern Il2CppType IVideoBackgroundEventHandler_t122_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14291_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IVideoBackgroundEventHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14291_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* declaring_type */
	, &IVideoBackgroundEventHandler_t122_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14291_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2776_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14287_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14288_MethodInfo,
	&InternalEnumerator_1_Dispose_m14289_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14290_MethodInfo,
	&InternalEnumerator_1_get_Current_m14291_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14290_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14289_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2776_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14288_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14290_MethodInfo,
	&InternalEnumerator_1_Dispose_m14289_MethodInfo,
	&InternalEnumerator_1_get_Current_m14291_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2776_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t4329_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2776_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t4329_il2cpp_TypeInfo, 7},
};
extern TypeInfo IVideoBackgroundEventHandler_t122_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2776_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14291_MethodInfo/* Method Usage */,
	&IVideoBackgroundEventHandler_t122_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIVideoBackgroundEventHandler_t122_m31868_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2776_0_0_0;
extern Il2CppType InternalEnumerator_1_t2776_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2776_GenericClass;
TypeInfo InternalEnumerator_1_t2776_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2776_MethodInfos/* methods */
	, InternalEnumerator_1_t2776_PropertyInfos/* properties */
	, InternalEnumerator_1_t2776_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2776_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2776_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2776_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2776_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2776_1_0_0/* this_arg */
	, InternalEnumerator_1_t2776_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2776_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2776_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2776)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t4334_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>
extern MethodInfo IList_1_get_Item_m41619_MethodInfo;
extern MethodInfo IList_1_set_Item_m41620_MethodInfo;
static PropertyInfo IList_1_t4334____Item_PropertyInfo = 
{
	&IList_1_t4334_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41619_MethodInfo/* get */
	, &IList_1_set_Item_m41620_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t4334_PropertyInfos[] =
{
	&IList_1_t4334____Item_PropertyInfo,
	NULL
};
extern Il2CppType IVideoBackgroundEventHandler_t122_0_0_0;
static ParameterInfo IList_1_t4334_IList_1_IndexOf_m41621_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IVideoBackgroundEventHandler_t122_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41621_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41621_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t4334_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t4334_IList_1_IndexOf_m41621_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41621_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IVideoBackgroundEventHandler_t122_0_0_0;
static ParameterInfo IList_1_t4334_IList_1_Insert_m41622_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IVideoBackgroundEventHandler_t122_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41622_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41622_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t4334_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t4334_IList_1_Insert_m41622_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41622_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t4334_IList_1_RemoveAt_m41623_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41623_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41623_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t4334_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t4334_IList_1_RemoveAt_m41623_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41623_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t4334_IList_1_get_Item_m41619_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IVideoBackgroundEventHandler_t122_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41619_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41619_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t4334_il2cpp_TypeInfo/* declaring_type */
	, &IVideoBackgroundEventHandler_t122_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t4334_IList_1_get_Item_m41619_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41619_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IVideoBackgroundEventHandler_t122_0_0_0;
static ParameterInfo IList_1_t4334_IList_1_set_Item_m41620_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IVideoBackgroundEventHandler_t122_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41620_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IVideoBackgroundEventHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41620_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t4334_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t4334_IList_1_set_Item_m41620_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41620_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t4334_MethodInfos[] =
{
	&IList_1_IndexOf_m41621_MethodInfo,
	&IList_1_Insert_m41622_MethodInfo,
	&IList_1_RemoveAt_m41623_MethodInfo,
	&IList_1_get_Item_m41619_MethodInfo,
	&IList_1_set_Item_m41620_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t4334_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t4330_il2cpp_TypeInfo,
	&IEnumerable_1_t4328_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t4334_0_0_0;
extern Il2CppType IList_1_t4334_1_0_0;
struct IList_1_t4334;
extern Il2CppGenericClass IList_1_t4334_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t4334_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t4334_MethodInfos/* methods */
	, IList_1_t4334_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t4334_il2cpp_TypeInfo/* element_class */
	, IList_1_t4334_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t4334_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t4334_0_0_0/* byval_arg */
	, &IList_1_t4334_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t4334_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_15.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2777_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_15MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_11.h"
extern TypeInfo InvokableCall_1_t2778_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_11MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14294_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14296_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2777____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2777_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2777, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2777_FieldInfos[] =
{
	&CachedInvokableCall_1_t2777____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType BackgroundPlaneBehaviour_t29_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2777_CachedInvokableCall_1__ctor_m14292_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t29_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14292_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14292_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2777_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2777_CachedInvokableCall_1__ctor_m14292_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14292_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2777_CachedInvokableCall_1_Invoke_m14293_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14293_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14293_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2777_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2777_CachedInvokableCall_1_Invoke_m14293_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14293_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2777_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14292_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14293_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14293_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14297_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2777_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14293_MethodInfo,
	&InvokableCall_1_Find_m14297_MethodInfo,
};
extern Il2CppType UnityAction_1_t2779_0_0_0;
extern TypeInfo UnityAction_1_t2779_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisBackgroundPlaneBehaviour_t29_m31878_MethodInfo;
extern TypeInfo BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14299_MethodInfo;
extern TypeInfo BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2777_RGCTXData[8] = 
{
	&UnityAction_1_t2779_0_0_0/* Type Usage */,
	&UnityAction_1_t2779_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBackgroundPlaneBehaviour_t29_m31878_MethodInfo/* Method Usage */,
	&BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14299_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14294_MethodInfo/* Method Usage */,
	&BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14296_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2777_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2777_1_0_0;
struct CachedInvokableCall_1_t2777;
extern Il2CppGenericClass CachedInvokableCall_1_t2777_GenericClass;
TypeInfo CachedInvokableCall_1_t2777_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2777_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2777_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2778_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2777_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2777_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2777_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2777_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2777_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2777_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2777_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2777)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_18.h"
extern TypeInfo UnityAction_1_t2779_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_18MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.BackgroundPlaneBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.BackgroundPlaneBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisBackgroundPlaneBehaviour_t29_m31878(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
extern Il2CppType UnityAction_1_t2779_0_0_1;
FieldInfo InvokableCall_1_t2778____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2779_0_0_1/* type */
	, &InvokableCall_1_t2778_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2778, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2778_FieldInfos[] =
{
	&InvokableCall_1_t2778____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2778_InvokableCall_1__ctor_m14294_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14294_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14294_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2778_InvokableCall_1__ctor_m14294_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14294_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2779_0_0_0;
static ParameterInfo InvokableCall_1_t2778_InvokableCall_1__ctor_m14295_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2779_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14295_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14295_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2778_InvokableCall_1__ctor_m14295_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14295_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2778_InvokableCall_1_Invoke_m14296_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14296_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14296_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2778_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2778_InvokableCall_1_Invoke_m14296_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14296_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2778_InvokableCall_1_Find_m14297_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14297_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14297_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2778_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2778_InvokableCall_1_Find_m14297_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14297_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2778_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14294_MethodInfo,
	&InvokableCall_1__ctor_m14295_MethodInfo,
	&InvokableCall_1_Invoke_m14296_MethodInfo,
	&InvokableCall_1_Find_m14297_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2778_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14296_MethodInfo,
	&InvokableCall_1_Find_m14297_MethodInfo,
};
extern TypeInfo UnityAction_1_t2779_il2cpp_TypeInfo;
extern TypeInfo BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2778_RGCTXData[5] = 
{
	&UnityAction_1_t2779_0_0_0/* Type Usage */,
	&UnityAction_1_t2779_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBackgroundPlaneBehaviour_t29_m31878_MethodInfo/* Method Usage */,
	&BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14299_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2778_0_0_0;
extern Il2CppType InvokableCall_1_t2778_1_0_0;
struct InvokableCall_1_t2778;
extern Il2CppGenericClass InvokableCall_1_t2778_GenericClass;
TypeInfo InvokableCall_1_t2778_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2778_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2778_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2778_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2778_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2778_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2778_0_0_0/* byval_arg */
	, &InvokableCall_1_t2778_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2778_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2778_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2778)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2779_UnityAction_1__ctor_m14298_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14298_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14298_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2779_UnityAction_1__ctor_m14298_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14298_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneBehaviour_t29_0_0_0;
static ParameterInfo UnityAction_1_t2779_UnityAction_1_Invoke_m14299_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t29_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14299_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14299_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2779_UnityAction_1_Invoke_m14299_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14299_GenericMethod/* genericMethod */

};
extern Il2CppType BackgroundPlaneBehaviour_t29_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2779_UnityAction_1_BeginInvoke_m14300_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &BackgroundPlaneBehaviour_t29_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14300_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14300_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2779_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2779_UnityAction_1_BeginInvoke_m14300_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14300_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2779_UnityAction_1_EndInvoke_m14301_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14301_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14301_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2779_UnityAction_1_EndInvoke_m14301_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14301_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2779_MethodInfos[] =
{
	&UnityAction_1__ctor_m14298_MethodInfo,
	&UnityAction_1_Invoke_m14299_MethodInfo,
	&UnityAction_1_BeginInvoke_m14300_MethodInfo,
	&UnityAction_1_EndInvoke_m14301_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14300_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14301_MethodInfo;
static MethodInfo* UnityAction_1_t2779_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14299_MethodInfo,
	&UnityAction_1_BeginInvoke_m14300_MethodInfo,
	&UnityAction_1_EndInvoke_m14301_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2779_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2779_1_0_0;
struct UnityAction_1_t2779;
extern Il2CppGenericClass UnityAction_1_t2779_GenericClass;
TypeInfo UnityAction_1_t2779_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2779_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2779_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2779_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2779_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2779_0_0_0/* byval_arg */
	, &UnityAction_1_t2779_1_0_0/* this_arg */
	, UnityAction_1_t2779_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2779_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2779)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5763_il2cpp_TypeInfo;

// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.CloudRecoBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.CloudRecoBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41624_MethodInfo;
static PropertyInfo IEnumerator_1_t5763____Current_PropertyInfo = 
{
	&IEnumerator_1_t5763_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41624_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5763_PropertyInfos[] =
{
	&IEnumerator_1_t5763____Current_PropertyInfo,
	NULL
};
extern Il2CppType CloudRecoBehaviour_t31_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41624_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.CloudRecoBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41624_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5763_il2cpp_TypeInfo/* declaring_type */
	, &CloudRecoBehaviour_t31_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41624_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5763_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41624_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5763_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5763_0_0_0;
extern Il2CppType IEnumerator_1_t5763_1_0_0;
struct IEnumerator_1_t5763;
extern Il2CppGenericClass IEnumerator_1_t5763_GenericClass;
TypeInfo IEnumerator_1_t5763_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5763_MethodInfos/* methods */
	, IEnumerator_1_t5763_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5763_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5763_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5763_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5763_0_0_0/* byval_arg */
	, &IEnumerator_1_t5763_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5763_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_38.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2780_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_38MethodDeclarations.h"

extern TypeInfo CloudRecoBehaviour_t31_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14306_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCloudRecoBehaviour_t31_m31880_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.CloudRecoBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.CloudRecoBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisCloudRecoBehaviour_t31_m31880(__this, p0, method) (CloudRecoBehaviour_t31 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2780____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2780_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2780, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2780____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2780_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2780, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2780_FieldInfos[] =
{
	&InternalEnumerator_1_t2780____array_0_FieldInfo,
	&InternalEnumerator_1_t2780____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14303_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2780____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2780_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14303_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2780____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2780_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14306_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2780_PropertyInfos[] =
{
	&InternalEnumerator_1_t2780____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2780____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2780_InternalEnumerator_1__ctor_m14302_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14302_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14302_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2780_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2780_InternalEnumerator_1__ctor_m14302_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14302_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14303_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14303_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2780_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14303_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14304_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14304_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2780_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14304_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14305_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14305_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2780_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14305_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoBehaviour_t31_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14306_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.CloudRecoBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14306_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2780_il2cpp_TypeInfo/* declaring_type */
	, &CloudRecoBehaviour_t31_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14306_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2780_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14302_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14303_MethodInfo,
	&InternalEnumerator_1_Dispose_m14304_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14305_MethodInfo,
	&InternalEnumerator_1_get_Current_m14306_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14305_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14304_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2780_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14303_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14305_MethodInfo,
	&InternalEnumerator_1_Dispose_m14304_MethodInfo,
	&InternalEnumerator_1_get_Current_m14306_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2780_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5763_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2780_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5763_il2cpp_TypeInfo, 7},
};
extern TypeInfo CloudRecoBehaviour_t31_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2780_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14306_MethodInfo/* Method Usage */,
	&CloudRecoBehaviour_t31_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCloudRecoBehaviour_t31_m31880_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2780_0_0_0;
extern Il2CppType InternalEnumerator_1_t2780_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2780_GenericClass;
TypeInfo InternalEnumerator_1_t2780_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2780_MethodInfos/* methods */
	, InternalEnumerator_1_t2780_PropertyInfos/* properties */
	, InternalEnumerator_1_t2780_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2780_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2780_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2780_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2780_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2780_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2780_1_0_0/* this_arg */
	, InternalEnumerator_1_t2780_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2780_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2780_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2780)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7317_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>
extern MethodInfo ICollection_1_get_Count_m41625_MethodInfo;
static PropertyInfo ICollection_1_t7317____Count_PropertyInfo = 
{
	&ICollection_1_t7317_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41625_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41626_MethodInfo;
static PropertyInfo ICollection_1_t7317____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7317_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41626_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7317_PropertyInfos[] =
{
	&ICollection_1_t7317____Count_PropertyInfo,
	&ICollection_1_t7317____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41625_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41625_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7317_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41625_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41626_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41626_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7317_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41626_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoBehaviour_t31_0_0_0;
extern Il2CppType CloudRecoBehaviour_t31_0_0_0;
static ParameterInfo ICollection_1_t7317_ICollection_1_Add_m41627_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t31_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41627_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41627_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7317_ICollection_1_Add_m41627_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41627_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41628_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41628_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41628_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoBehaviour_t31_0_0_0;
static ParameterInfo ICollection_1_t7317_ICollection_1_Contains_m41629_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t31_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41629_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41629_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7317_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7317_ICollection_1_Contains_m41629_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41629_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoBehaviourU5BU5D_t5170_0_0_0;
extern Il2CppType CloudRecoBehaviourU5BU5D_t5170_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7317_ICollection_1_CopyTo_m41630_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviourU5BU5D_t5170_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41630_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41630_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7317_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7317_ICollection_1_CopyTo_m41630_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41630_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoBehaviour_t31_0_0_0;
static ParameterInfo ICollection_1_t7317_ICollection_1_Remove_m41631_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t31_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41631_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41631_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7317_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7317_ICollection_1_Remove_m41631_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41631_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7317_MethodInfos[] =
{
	&ICollection_1_get_Count_m41625_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41626_MethodInfo,
	&ICollection_1_Add_m41627_MethodInfo,
	&ICollection_1_Clear_m41628_MethodInfo,
	&ICollection_1_Contains_m41629_MethodInfo,
	&ICollection_1_CopyTo_m41630_MethodInfo,
	&ICollection_1_Remove_m41631_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7319_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7317_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7319_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7317_0_0_0;
extern Il2CppType ICollection_1_t7317_1_0_0;
struct ICollection_1_t7317;
extern Il2CppGenericClass ICollection_1_t7317_GenericClass;
TypeInfo ICollection_1_t7317_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7317_MethodInfos/* methods */
	, ICollection_1_t7317_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7317_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7317_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7317_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7317_0_0_0/* byval_arg */
	, &ICollection_1_t7317_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7317_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CloudRecoBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.CloudRecoBehaviour>
extern Il2CppType IEnumerator_1_t5763_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41632_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CloudRecoBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41632_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7319_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5763_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41632_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7319_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41632_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7319_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7319_0_0_0;
extern Il2CppType IEnumerable_1_t7319_1_0_0;
struct IEnumerable_1_t7319;
extern Il2CppGenericClass IEnumerable_1_t7319_GenericClass;
TypeInfo IEnumerable_1_t7319_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7319_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7319_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7319_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7319_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7319_0_0_0/* byval_arg */
	, &IEnumerable_1_t7319_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7319_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7318_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>
extern MethodInfo IList_1_get_Item_m41633_MethodInfo;
extern MethodInfo IList_1_set_Item_m41634_MethodInfo;
static PropertyInfo IList_1_t7318____Item_PropertyInfo = 
{
	&IList_1_t7318_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41633_MethodInfo/* get */
	, &IList_1_set_Item_m41634_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7318_PropertyInfos[] =
{
	&IList_1_t7318____Item_PropertyInfo,
	NULL
};
extern Il2CppType CloudRecoBehaviour_t31_0_0_0;
static ParameterInfo IList_1_t7318_IList_1_IndexOf_m41635_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t31_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41635_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41635_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7318_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7318_IList_1_IndexOf_m41635_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41635_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CloudRecoBehaviour_t31_0_0_0;
static ParameterInfo IList_1_t7318_IList_1_Insert_m41636_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t31_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41636_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41636_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7318_IList_1_Insert_m41636_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41636_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7318_IList_1_RemoveAt_m41637_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41637_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41637_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7318_IList_1_RemoveAt_m41637_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41637_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7318_IList_1_get_Item_m41633_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType CloudRecoBehaviour_t31_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41633_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41633_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7318_il2cpp_TypeInfo/* declaring_type */
	, &CloudRecoBehaviour_t31_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7318_IList_1_get_Item_m41633_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41633_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CloudRecoBehaviour_t31_0_0_0;
static ParameterInfo IList_1_t7318_IList_1_set_Item_m41634_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t31_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41634_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41634_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7318_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7318_IList_1_set_Item_m41634_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41634_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7318_MethodInfos[] =
{
	&IList_1_IndexOf_m41635_MethodInfo,
	&IList_1_Insert_m41636_MethodInfo,
	&IList_1_RemoveAt_m41637_MethodInfo,
	&IList_1_get_Item_m41633_MethodInfo,
	&IList_1_set_Item_m41634_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7318_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7317_il2cpp_TypeInfo,
	&IEnumerable_1_t7319_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7318_0_0_0;
extern Il2CppType IList_1_t7318_1_0_0;
struct IList_1_t7318;
extern Il2CppGenericClass IList_1_t7318_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7318_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7318_MethodInfos/* methods */
	, IList_1_t7318_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7318_il2cpp_TypeInfo/* element_class */
	, IList_1_t7318_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7318_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7318_0_0_0/* byval_arg */
	, &IList_1_t7318_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7318_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7320_il2cpp_TypeInfo;

// Vuforia.CloudRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractBe.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m41638_MethodInfo;
static PropertyInfo ICollection_1_t7320____Count_PropertyInfo = 
{
	&ICollection_1_t7320_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41638_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41639_MethodInfo;
static PropertyInfo ICollection_1_t7320____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7320_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41639_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7320_PropertyInfos[] =
{
	&ICollection_1_t7320____Count_PropertyInfo,
	&ICollection_1_t7320____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41638_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41638_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7320_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41638_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41639_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41639_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7320_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41639_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoAbstractBehaviour_t32_0_0_0;
extern Il2CppType CloudRecoAbstractBehaviour_t32_0_0_0;
static ParameterInfo ICollection_1_t7320_ICollection_1_Add_m41640_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoAbstractBehaviour_t32_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41640_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41640_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7320_ICollection_1_Add_m41640_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41640_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41641_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41641_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41641_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoAbstractBehaviour_t32_0_0_0;
static ParameterInfo ICollection_1_t7320_ICollection_1_Contains_m41642_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoAbstractBehaviour_t32_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41642_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41642_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7320_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7320_ICollection_1_Contains_m41642_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41642_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoAbstractBehaviourU5BU5D_t5492_0_0_0;
extern Il2CppType CloudRecoAbstractBehaviourU5BU5D_t5492_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7320_ICollection_1_CopyTo_m41643_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoAbstractBehaviourU5BU5D_t5492_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41643_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41643_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7320_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7320_ICollection_1_CopyTo_m41643_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41643_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoAbstractBehaviour_t32_0_0_0;
static ParameterInfo ICollection_1_t7320_ICollection_1_Remove_m41644_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoAbstractBehaviour_t32_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41644_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CloudRecoAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41644_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7320_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7320_ICollection_1_Remove_m41644_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41644_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7320_MethodInfos[] =
{
	&ICollection_1_get_Count_m41638_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41639_MethodInfo,
	&ICollection_1_Add_m41640_MethodInfo,
	&ICollection_1_Clear_m41641_MethodInfo,
	&ICollection_1_Contains_m41642_MethodInfo,
	&ICollection_1_CopyTo_m41643_MethodInfo,
	&ICollection_1_Remove_m41644_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7322_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7320_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7322_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7320_0_0_0;
extern Il2CppType ICollection_1_t7320_1_0_0;
struct ICollection_1_t7320;
extern Il2CppGenericClass ICollection_1_t7320_GenericClass;
TypeInfo ICollection_1_t7320_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7320_MethodInfos/* methods */
	, ICollection_1_t7320_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7320_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7320_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7320_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7320_0_0_0/* byval_arg */
	, &ICollection_1_t7320_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7320_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CloudRecoAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.CloudRecoAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5765_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41645_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CloudRecoAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41645_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7322_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5765_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41645_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7322_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41645_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7322_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7322_0_0_0;
extern Il2CppType IEnumerable_1_t7322_1_0_0;
struct IEnumerable_1_t7322;
extern Il2CppGenericClass IEnumerable_1_t7322_GenericClass;
TypeInfo IEnumerable_1_t7322_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7322_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7322_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7322_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7322_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7322_0_0_0/* byval_arg */
	, &IEnumerable_1_t7322_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7322_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5765_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41646_MethodInfo;
static PropertyInfo IEnumerator_1_t5765____Current_PropertyInfo = 
{
	&IEnumerator_1_t5765_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41646_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5765_PropertyInfos[] =
{
	&IEnumerator_1_t5765____Current_PropertyInfo,
	NULL
};
extern Il2CppType CloudRecoAbstractBehaviour_t32_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41646_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41646_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5765_il2cpp_TypeInfo/* declaring_type */
	, &CloudRecoAbstractBehaviour_t32_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41646_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5765_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41646_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5765_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5765_0_0_0;
extern Il2CppType IEnumerator_1_t5765_1_0_0;
struct IEnumerator_1_t5765;
extern Il2CppGenericClass IEnumerator_1_t5765_GenericClass;
TypeInfo IEnumerator_1_t5765_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5765_MethodInfos/* methods */
	, IEnumerator_1_t5765_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5765_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5765_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5765_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5765_0_0_0/* byval_arg */
	, &IEnumerator_1_t5765_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5765_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_39.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2781_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_39MethodDeclarations.h"

extern TypeInfo CloudRecoAbstractBehaviour_t32_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14311_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCloudRecoAbstractBehaviour_t32_m31891_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.CloudRecoAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.CloudRecoAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisCloudRecoAbstractBehaviour_t32_m31891(__this, p0, method) (CloudRecoAbstractBehaviour_t32 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2781____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2781, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2781____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2781, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2781_FieldInfos[] =
{
	&InternalEnumerator_1_t2781____array_0_FieldInfo,
	&InternalEnumerator_1_t2781____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14308_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2781____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2781_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14308_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2781____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2781_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14311_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2781_PropertyInfos[] =
{
	&InternalEnumerator_1_t2781____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2781____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2781_InternalEnumerator_1__ctor_m14307_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14307_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14307_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2781_InternalEnumerator_1__ctor_m14307_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14307_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14308_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14308_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14308_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14309_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14309_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14309_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14310_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14310_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14310_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoAbstractBehaviour_t32_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14311_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.CloudRecoAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14311_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* declaring_type */
	, &CloudRecoAbstractBehaviour_t32_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14311_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2781_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14307_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14308_MethodInfo,
	&InternalEnumerator_1_Dispose_m14309_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14310_MethodInfo,
	&InternalEnumerator_1_get_Current_m14311_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14310_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14309_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2781_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14308_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14310_MethodInfo,
	&InternalEnumerator_1_Dispose_m14309_MethodInfo,
	&InternalEnumerator_1_get_Current_m14311_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2781_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5765_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2781_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5765_il2cpp_TypeInfo, 7},
};
extern TypeInfo CloudRecoAbstractBehaviour_t32_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2781_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14311_MethodInfo/* Method Usage */,
	&CloudRecoAbstractBehaviour_t32_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCloudRecoAbstractBehaviour_t32_m31891_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2781_0_0_0;
extern Il2CppType InternalEnumerator_1_t2781_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2781_GenericClass;
TypeInfo InternalEnumerator_1_t2781_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2781_MethodInfos/* methods */
	, InternalEnumerator_1_t2781_PropertyInfos/* properties */
	, InternalEnumerator_1_t2781_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2781_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2781_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2781_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2781_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2781_1_0_0/* this_arg */
	, InternalEnumerator_1_t2781_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2781_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2781_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2781)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7321_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m41647_MethodInfo;
extern MethodInfo IList_1_set_Item_m41648_MethodInfo;
static PropertyInfo IList_1_t7321____Item_PropertyInfo = 
{
	&IList_1_t7321_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41647_MethodInfo/* get */
	, &IList_1_set_Item_m41648_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7321_PropertyInfos[] =
{
	&IList_1_t7321____Item_PropertyInfo,
	NULL
};
extern Il2CppType CloudRecoAbstractBehaviour_t32_0_0_0;
static ParameterInfo IList_1_t7321_IList_1_IndexOf_m41649_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoAbstractBehaviour_t32_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41649_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41649_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7321_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7321_IList_1_IndexOf_m41649_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41649_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CloudRecoAbstractBehaviour_t32_0_0_0;
static ParameterInfo IList_1_t7321_IList_1_Insert_m41650_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CloudRecoAbstractBehaviour_t32_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41650_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41650_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7321_IList_1_Insert_m41650_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41650_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7321_IList_1_RemoveAt_m41651_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41651_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41651_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7321_IList_1_RemoveAt_m41651_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41651_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7321_IList_1_get_Item_m41647_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType CloudRecoAbstractBehaviour_t32_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41647_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41647_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7321_il2cpp_TypeInfo/* declaring_type */
	, &CloudRecoAbstractBehaviour_t32_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7321_IList_1_get_Item_m41647_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41647_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CloudRecoAbstractBehaviour_t32_0_0_0;
static ParameterInfo IList_1_t7321_IList_1_set_Item_m41648_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CloudRecoAbstractBehaviour_t32_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41648_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CloudRecoAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41648_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7321_IList_1_set_Item_m41648_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41648_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7321_MethodInfos[] =
{
	&IList_1_IndexOf_m41649_MethodInfo,
	&IList_1_Insert_m41650_MethodInfo,
	&IList_1_RemoveAt_m41651_MethodInfo,
	&IList_1_get_Item_m41647_MethodInfo,
	&IList_1_set_Item_m41648_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7321_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7320_il2cpp_TypeInfo,
	&IEnumerable_1_t7322_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7321_0_0_0;
extern Il2CppType IList_1_t7321_1_0_0;
struct IList_1_t7321;
extern Il2CppGenericClass IList_1_t7321_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7321_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7321_MethodInfos/* methods */
	, IList_1_t7321_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7321_il2cpp_TypeInfo/* element_class */
	, IList_1_t7321_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7321_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7321_0_0_0/* byval_arg */
	, &IList_1_t7321_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7321_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_16.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2782_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_16MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_12.h"
extern TypeInfo InvokableCall_1_t2783_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_12MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14314_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14316_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2782____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2782_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2782, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2782_FieldInfos[] =
{
	&CachedInvokableCall_1_t2782____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType CloudRecoBehaviour_t31_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2782_CachedInvokableCall_1__ctor_m14312_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t31_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14312_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14312_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2782_CachedInvokableCall_1__ctor_m14312_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14312_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2782_CachedInvokableCall_1_Invoke_m14313_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14313_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CloudRecoBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14313_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2782_CachedInvokableCall_1_Invoke_m14313_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14313_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2782_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14312_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14313_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14313_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14317_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2782_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14313_MethodInfo,
	&InvokableCall_1_Find_m14317_MethodInfo,
};
extern Il2CppType UnityAction_1_t2784_0_0_0;
extern TypeInfo UnityAction_1_t2784_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisCloudRecoBehaviour_t31_m31901_MethodInfo;
extern TypeInfo CloudRecoBehaviour_t31_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14319_MethodInfo;
extern TypeInfo CloudRecoBehaviour_t31_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2782_RGCTXData[8] = 
{
	&UnityAction_1_t2784_0_0_0/* Type Usage */,
	&UnityAction_1_t2784_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCloudRecoBehaviour_t31_m31901_MethodInfo/* Method Usage */,
	&CloudRecoBehaviour_t31_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14319_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14314_MethodInfo/* Method Usage */,
	&CloudRecoBehaviour_t31_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14316_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2782_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2782_1_0_0;
struct CachedInvokableCall_1_t2782;
extern Il2CppGenericClass CachedInvokableCall_1_t2782_GenericClass;
TypeInfo CachedInvokableCall_1_t2782_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2782_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2782_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2783_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2782_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2782_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2782_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2782_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2782_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2782_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2782_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2782)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_19.h"
extern TypeInfo UnityAction_1_t2784_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_19MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.CloudRecoBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.CloudRecoBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisCloudRecoBehaviour_t31_m31901(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>
extern Il2CppType UnityAction_1_t2784_0_0_1;
FieldInfo InvokableCall_1_t2783____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2784_0_0_1/* type */
	, &InvokableCall_1_t2783_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2783, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2783_FieldInfos[] =
{
	&InvokableCall_1_t2783____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2783_InvokableCall_1__ctor_m14314_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14314_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14314_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2783_InvokableCall_1__ctor_m14314_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14314_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2784_0_0_0;
static ParameterInfo InvokableCall_1_t2783_InvokableCall_1__ctor_m14315_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2784_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14315_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14315_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2783_InvokableCall_1__ctor_m14315_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14315_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2783_InvokableCall_1_Invoke_m14316_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14316_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14316_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2783_InvokableCall_1_Invoke_m14316_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14316_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2783_InvokableCall_1_Find_m14317_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14317_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14317_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2783_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2783_InvokableCall_1_Find_m14317_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14317_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2783_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14314_MethodInfo,
	&InvokableCall_1__ctor_m14315_MethodInfo,
	&InvokableCall_1_Invoke_m14316_MethodInfo,
	&InvokableCall_1_Find_m14317_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2783_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14316_MethodInfo,
	&InvokableCall_1_Find_m14317_MethodInfo,
};
extern TypeInfo UnityAction_1_t2784_il2cpp_TypeInfo;
extern TypeInfo CloudRecoBehaviour_t31_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2783_RGCTXData[5] = 
{
	&UnityAction_1_t2784_0_0_0/* Type Usage */,
	&UnityAction_1_t2784_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCloudRecoBehaviour_t31_m31901_MethodInfo/* Method Usage */,
	&CloudRecoBehaviour_t31_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14319_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2783_0_0_0;
extern Il2CppType InvokableCall_1_t2783_1_0_0;
struct InvokableCall_1_t2783;
extern Il2CppGenericClass InvokableCall_1_t2783_GenericClass;
TypeInfo InvokableCall_1_t2783_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2783_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2783_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2783_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2783_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2783_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2783_0_0_0/* byval_arg */
	, &InvokableCall_1_t2783_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2783_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2783_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2783)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2784_UnityAction_1__ctor_m14318_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14318_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14318_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2784_UnityAction_1__ctor_m14318_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14318_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoBehaviour_t31_0_0_0;
static ParameterInfo UnityAction_1_t2784_UnityAction_1_Invoke_m14319_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t31_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14319_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14319_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2784_UnityAction_1_Invoke_m14319_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14319_GenericMethod/* genericMethod */

};
extern Il2CppType CloudRecoBehaviour_t31_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2784_UnityAction_1_BeginInvoke_m14320_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &CloudRecoBehaviour_t31_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14320_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14320_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2784_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2784_UnityAction_1_BeginInvoke_m14320_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14320_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2784_UnityAction_1_EndInvoke_m14321_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14321_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14321_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2784_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2784_UnityAction_1_EndInvoke_m14321_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14321_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2784_MethodInfos[] =
{
	&UnityAction_1__ctor_m14318_MethodInfo,
	&UnityAction_1_Invoke_m14319_MethodInfo,
	&UnityAction_1_BeginInvoke_m14320_MethodInfo,
	&UnityAction_1_EndInvoke_m14321_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14320_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14321_MethodInfo;
static MethodInfo* UnityAction_1_t2784_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14319_MethodInfo,
	&UnityAction_1_BeginInvoke_m14320_MethodInfo,
	&UnityAction_1_EndInvoke_m14321_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2784_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2784_1_0_0;
struct UnityAction_1_t2784;
extern Il2CppGenericClass UnityAction_1_t2784_GenericClass;
TypeInfo UnityAction_1_t2784_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2784_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2784_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2784_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2784_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2784_0_0_0/* byval_arg */
	, &UnityAction_1_t2784_1_0_0/* this_arg */
	, UnityAction_1_t2784_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2784_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2784)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5767_il2cpp_TypeInfo;

// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.CylinderTargetBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.CylinderTargetBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41652_MethodInfo;
static PropertyInfo IEnumerator_1_t5767____Current_PropertyInfo = 
{
	&IEnumerator_1_t5767_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41652_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5767_PropertyInfos[] =
{
	&IEnumerator_1_t5767____Current_PropertyInfo,
	NULL
};
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41652_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.CylinderTargetBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41652_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5767_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetBehaviour_t33_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41652_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5767_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41652_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5767_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5767_0_0_0;
extern Il2CppType IEnumerator_1_t5767_1_0_0;
struct IEnumerator_1_t5767;
extern Il2CppGenericClass IEnumerator_1_t5767_GenericClass;
TypeInfo IEnumerator_1_t5767_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5767_MethodInfos/* methods */
	, IEnumerator_1_t5767_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5767_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5767_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5767_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5767_0_0_0/* byval_arg */
	, &IEnumerator_1_t5767_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5767_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_40.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2785_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_40MethodDeclarations.h"

extern TypeInfo CylinderTargetBehaviour_t33_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14326_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCylinderTargetBehaviour_t33_m31903_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.CylinderTargetBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.CylinderTargetBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisCylinderTargetBehaviour_t33_m31903(__this, p0, method) (CylinderTargetBehaviour_t33 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2785____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2785_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2785, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2785____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2785_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2785, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2785_FieldInfos[] =
{
	&InternalEnumerator_1_t2785____array_0_FieldInfo,
	&InternalEnumerator_1_t2785____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14323_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2785____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2785_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14323_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2785____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2785_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14326_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2785_PropertyInfos[] =
{
	&InternalEnumerator_1_t2785____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2785____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2785_InternalEnumerator_1__ctor_m14322_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14322_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14322_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2785_InternalEnumerator_1__ctor_m14322_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14322_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14323_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14323_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2785_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14323_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14324_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14324_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14324_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14325_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14325_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2785_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14325_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14326_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.CylinderTargetBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14326_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2785_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetBehaviour_t33_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14326_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2785_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14322_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14323_MethodInfo,
	&InternalEnumerator_1_Dispose_m14324_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14325_MethodInfo,
	&InternalEnumerator_1_get_Current_m14326_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14325_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14324_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2785_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14323_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14325_MethodInfo,
	&InternalEnumerator_1_Dispose_m14324_MethodInfo,
	&InternalEnumerator_1_get_Current_m14326_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2785_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5767_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2785_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5767_il2cpp_TypeInfo, 7},
};
extern TypeInfo CylinderTargetBehaviour_t33_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2785_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14326_MethodInfo/* Method Usage */,
	&CylinderTargetBehaviour_t33_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCylinderTargetBehaviour_t33_m31903_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2785_0_0_0;
extern Il2CppType InternalEnumerator_1_t2785_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2785_GenericClass;
TypeInfo InternalEnumerator_1_t2785_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2785_MethodInfos/* methods */
	, InternalEnumerator_1_t2785_PropertyInfos/* properties */
	, InternalEnumerator_1_t2785_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2785_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2785_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2785_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2785_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2785_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2785_1_0_0/* this_arg */
	, InternalEnumerator_1_t2785_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2785_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2785_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2785)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7323_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>
extern MethodInfo ICollection_1_get_Count_m41653_MethodInfo;
static PropertyInfo ICollection_1_t7323____Count_PropertyInfo = 
{
	&ICollection_1_t7323_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41653_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41654_MethodInfo;
static PropertyInfo ICollection_1_t7323____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7323_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41654_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7323_PropertyInfos[] =
{
	&ICollection_1_t7323____Count_PropertyInfo,
	&ICollection_1_t7323____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41653_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41653_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7323_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41653_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41654_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41654_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7323_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41654_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
static ParameterInfo ICollection_1_t7323_ICollection_1_Add_m41655_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t33_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41655_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41655_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7323_ICollection_1_Add_m41655_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41655_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41656_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41656_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41656_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
static ParameterInfo ICollection_1_t7323_ICollection_1_Contains_m41657_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t33_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41657_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41657_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7323_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7323_ICollection_1_Contains_m41657_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41657_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetBehaviourU5BU5D_t5171_0_0_0;
extern Il2CppType CylinderTargetBehaviourU5BU5D_t5171_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7323_ICollection_1_CopyTo_m41658_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviourU5BU5D_t5171_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41658_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41658_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7323_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7323_ICollection_1_CopyTo_m41658_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41658_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
static ParameterInfo ICollection_1_t7323_ICollection_1_Remove_m41659_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t33_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41659_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41659_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7323_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7323_ICollection_1_Remove_m41659_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41659_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7323_MethodInfos[] =
{
	&ICollection_1_get_Count_m41653_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41654_MethodInfo,
	&ICollection_1_Add_m41655_MethodInfo,
	&ICollection_1_Clear_m41656_MethodInfo,
	&ICollection_1_Contains_m41657_MethodInfo,
	&ICollection_1_CopyTo_m41658_MethodInfo,
	&ICollection_1_Remove_m41659_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7325_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7323_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7325_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7323_0_0_0;
extern Il2CppType ICollection_1_t7323_1_0_0;
struct ICollection_1_t7323;
extern Il2CppGenericClass ICollection_1_t7323_GenericClass;
TypeInfo ICollection_1_t7323_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7323_MethodInfos/* methods */
	, ICollection_1_t7323_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7323_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7323_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7323_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7323_0_0_0/* byval_arg */
	, &ICollection_1_t7323_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7323_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CylinderTargetBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.CylinderTargetBehaviour>
extern Il2CppType IEnumerator_1_t5767_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41660_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CylinderTargetBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41660_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7325_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5767_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41660_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7325_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41660_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7325_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7325_0_0_0;
extern Il2CppType IEnumerable_1_t7325_1_0_0;
struct IEnumerable_1_t7325;
extern Il2CppGenericClass IEnumerable_1_t7325_GenericClass;
TypeInfo IEnumerable_1_t7325_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7325_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7325_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7325_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7325_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7325_0_0_0/* byval_arg */
	, &IEnumerable_1_t7325_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7325_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7324_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>
extern MethodInfo IList_1_get_Item_m41661_MethodInfo;
extern MethodInfo IList_1_set_Item_m41662_MethodInfo;
static PropertyInfo IList_1_t7324____Item_PropertyInfo = 
{
	&IList_1_t7324_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41661_MethodInfo/* get */
	, &IList_1_set_Item_m41662_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7324_PropertyInfos[] =
{
	&IList_1_t7324____Item_PropertyInfo,
	NULL
};
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
static ParameterInfo IList_1_t7324_IList_1_IndexOf_m41663_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t33_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41663_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41663_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7324_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7324_IList_1_IndexOf_m41663_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41663_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
static ParameterInfo IList_1_t7324_IList_1_Insert_m41664_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t33_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41664_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41664_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7324_IList_1_Insert_m41664_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41664_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7324_IList_1_RemoveAt_m41665_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41665_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41665_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7324_IList_1_RemoveAt_m41665_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41665_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7324_IList_1_get_Item_m41661_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41661_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41661_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7324_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetBehaviour_t33_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7324_IList_1_get_Item_m41661_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41661_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
static ParameterInfo IList_1_t7324_IList_1_set_Item_m41662_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t33_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41662_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41662_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7324_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7324_IList_1_set_Item_m41662_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41662_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7324_MethodInfos[] =
{
	&IList_1_IndexOf_m41663_MethodInfo,
	&IList_1_Insert_m41664_MethodInfo,
	&IList_1_RemoveAt_m41665_MethodInfo,
	&IList_1_get_Item_m41661_MethodInfo,
	&IList_1_set_Item_m41662_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7324_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7323_il2cpp_TypeInfo,
	&IEnumerable_1_t7325_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7324_0_0_0;
extern Il2CppType IList_1_t7324_1_0_0;
struct IList_1_t7324;
extern Il2CppGenericClass IList_1_t7324_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7324_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7324_MethodInfos/* methods */
	, IList_1_t7324_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7324_il2cpp_TypeInfo/* element_class */
	, IList_1_t7324_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7324_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7324_0_0_0/* byval_arg */
	, &IList_1_t7324_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7324_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7326_il2cpp_TypeInfo;

// Vuforia.CylinderTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m41666_MethodInfo;
static PropertyInfo ICollection_1_t7326____Count_PropertyInfo = 
{
	&ICollection_1_t7326_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41666_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41667_MethodInfo;
static PropertyInfo ICollection_1_t7326____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7326_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41667_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7326_PropertyInfos[] =
{
	&ICollection_1_t7326____Count_PropertyInfo,
	&ICollection_1_t7326____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41666_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41666_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7326_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41666_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41667_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41667_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7326_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41667_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetAbstractBehaviour_t34_0_0_0;
extern Il2CppType CylinderTargetAbstractBehaviour_t34_0_0_0;
static ParameterInfo ICollection_1_t7326_ICollection_1_Add_m41668_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetAbstractBehaviour_t34_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41668_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41668_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7326_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7326_ICollection_1_Add_m41668_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41668_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41669_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41669_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7326_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41669_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetAbstractBehaviour_t34_0_0_0;
static ParameterInfo ICollection_1_t7326_ICollection_1_Contains_m41670_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetAbstractBehaviour_t34_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41670_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41670_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7326_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7326_ICollection_1_Contains_m41670_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41670_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetAbstractBehaviourU5BU5D_t5493_0_0_0;
extern Il2CppType CylinderTargetAbstractBehaviourU5BU5D_t5493_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7326_ICollection_1_CopyTo_m41671_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetAbstractBehaviourU5BU5D_t5493_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41671_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41671_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7326_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7326_ICollection_1_CopyTo_m41671_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41671_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetAbstractBehaviour_t34_0_0_0;
static ParameterInfo ICollection_1_t7326_ICollection_1_Remove_m41672_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetAbstractBehaviour_t34_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41672_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.CylinderTargetAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41672_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7326_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7326_ICollection_1_Remove_m41672_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41672_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7326_MethodInfos[] =
{
	&ICollection_1_get_Count_m41666_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41667_MethodInfo,
	&ICollection_1_Add_m41668_MethodInfo,
	&ICollection_1_Clear_m41669_MethodInfo,
	&ICollection_1_Contains_m41670_MethodInfo,
	&ICollection_1_CopyTo_m41671_MethodInfo,
	&ICollection_1_Remove_m41672_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7328_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7326_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7328_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7326_0_0_0;
extern Il2CppType ICollection_1_t7326_1_0_0;
struct ICollection_1_t7326;
extern Il2CppGenericClass ICollection_1_t7326_GenericClass;
TypeInfo ICollection_1_t7326_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7326_MethodInfos/* methods */
	, ICollection_1_t7326_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7326_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7326_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7326_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7326_0_0_0/* byval_arg */
	, &ICollection_1_t7326_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7326_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CylinderTargetAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.CylinderTargetAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5769_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41673_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.CylinderTargetAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41673_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7328_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5769_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41673_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7328_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41673_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7328_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7328_0_0_0;
extern Il2CppType IEnumerable_1_t7328_1_0_0;
struct IEnumerable_1_t7328;
extern Il2CppGenericClass IEnumerable_1_t7328_GenericClass;
TypeInfo IEnumerable_1_t7328_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7328_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7328_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7328_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7328_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7328_0_0_0/* byval_arg */
	, &IEnumerable_1_t7328_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7328_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5769_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41674_MethodInfo;
static PropertyInfo IEnumerator_1_t5769____Current_PropertyInfo = 
{
	&IEnumerator_1_t5769_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41674_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5769_PropertyInfos[] =
{
	&IEnumerator_1_t5769____Current_PropertyInfo,
	NULL
};
extern Il2CppType CylinderTargetAbstractBehaviour_t34_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41674_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41674_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5769_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetAbstractBehaviour_t34_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41674_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5769_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41674_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5769_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5769_0_0_0;
extern Il2CppType IEnumerator_1_t5769_1_0_0;
struct IEnumerator_1_t5769;
extern Il2CppGenericClass IEnumerator_1_t5769_GenericClass;
TypeInfo IEnumerator_1_t5769_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5769_MethodInfos/* methods */
	, IEnumerator_1_t5769_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5769_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5769_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5769_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5769_0_0_0/* byval_arg */
	, &IEnumerator_1_t5769_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5769_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_41.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2786_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_41MethodDeclarations.h"

extern TypeInfo CylinderTargetAbstractBehaviour_t34_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14331_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCylinderTargetAbstractBehaviour_t34_m31914_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.CylinderTargetAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.CylinderTargetAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisCylinderTargetAbstractBehaviour_t34_m31914(__this, p0, method) (CylinderTargetAbstractBehaviour_t34 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2786____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2786, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2786____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2786, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2786_FieldInfos[] =
{
	&InternalEnumerator_1_t2786____array_0_FieldInfo,
	&InternalEnumerator_1_t2786____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14328_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2786____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2786_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14328_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2786____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2786_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14331_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2786_PropertyInfos[] =
{
	&InternalEnumerator_1_t2786____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2786____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2786_InternalEnumerator_1__ctor_m14327_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14327_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14327_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2786_InternalEnumerator_1__ctor_m14327_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14327_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14328_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14328_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14328_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14329_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14329_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14329_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14330_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14330_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14330_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetAbstractBehaviour_t34_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14331_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14331_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetAbstractBehaviour_t34_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14331_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2786_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14327_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14328_MethodInfo,
	&InternalEnumerator_1_Dispose_m14329_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14330_MethodInfo,
	&InternalEnumerator_1_get_Current_m14331_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14330_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14329_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2786_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14328_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14330_MethodInfo,
	&InternalEnumerator_1_Dispose_m14329_MethodInfo,
	&InternalEnumerator_1_get_Current_m14331_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2786_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5769_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2786_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5769_il2cpp_TypeInfo, 7},
};
extern TypeInfo CylinderTargetAbstractBehaviour_t34_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2786_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14331_MethodInfo/* Method Usage */,
	&CylinderTargetAbstractBehaviour_t34_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCylinderTargetAbstractBehaviour_t34_m31914_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2786_0_0_0;
extern Il2CppType InternalEnumerator_1_t2786_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2786_GenericClass;
TypeInfo InternalEnumerator_1_t2786_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2786_MethodInfos/* methods */
	, InternalEnumerator_1_t2786_PropertyInfos/* properties */
	, InternalEnumerator_1_t2786_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2786_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2786_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2786_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2786_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2786_1_0_0/* this_arg */
	, InternalEnumerator_1_t2786_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2786_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2786_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2786)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7327_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m41675_MethodInfo;
extern MethodInfo IList_1_set_Item_m41676_MethodInfo;
static PropertyInfo IList_1_t7327____Item_PropertyInfo = 
{
	&IList_1_t7327_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41675_MethodInfo/* get */
	, &IList_1_set_Item_m41676_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7327_PropertyInfos[] =
{
	&IList_1_t7327____Item_PropertyInfo,
	NULL
};
extern Il2CppType CylinderTargetAbstractBehaviour_t34_0_0_0;
static ParameterInfo IList_1_t7327_IList_1_IndexOf_m41677_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetAbstractBehaviour_t34_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41677_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41677_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7327_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7327_IList_1_IndexOf_m41677_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41677_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CylinderTargetAbstractBehaviour_t34_0_0_0;
static ParameterInfo IList_1_t7327_IList_1_Insert_m41678_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CylinderTargetAbstractBehaviour_t34_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41678_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41678_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7327_IList_1_Insert_m41678_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41678_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7327_IList_1_RemoveAt_m41679_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41679_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41679_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7327_IList_1_RemoveAt_m41679_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41679_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7327_IList_1_get_Item_m41675_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType CylinderTargetAbstractBehaviour_t34_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41675_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41675_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7327_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetAbstractBehaviour_t34_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7327_IList_1_get_Item_m41675_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41675_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CylinderTargetAbstractBehaviour_t34_0_0_0;
static ParameterInfo IList_1_t7327_IList_1_set_Item_m41676_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CylinderTargetAbstractBehaviour_t34_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41676_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.CylinderTargetAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41676_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7327_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7327_IList_1_set_Item_m41676_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41676_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7327_MethodInfos[] =
{
	&IList_1_IndexOf_m41677_MethodInfo,
	&IList_1_Insert_m41678_MethodInfo,
	&IList_1_RemoveAt_m41679_MethodInfo,
	&IList_1_get_Item_m41675_MethodInfo,
	&IList_1_set_Item_m41676_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7327_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7326_il2cpp_TypeInfo,
	&IEnumerable_1_t7328_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7327_0_0_0;
extern Il2CppType IList_1_t7327_1_0_0;
struct IList_1_t7327;
extern Il2CppGenericClass IList_1_t7327_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7327_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7327_MethodInfos/* methods */
	, IList_1_t7327_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7327_il2cpp_TypeInfo/* element_class */
	, IList_1_t7327_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7327_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7327_0_0_0/* byval_arg */
	, &IList_1_t7327_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7327_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7329_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>
extern MethodInfo ICollection_1_get_Count_m41680_MethodInfo;
static PropertyInfo ICollection_1_t7329____Count_PropertyInfo = 
{
	&ICollection_1_t7329_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41680_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41681_MethodInfo;
static PropertyInfo ICollection_1_t7329____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7329_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41681_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7329_PropertyInfos[] =
{
	&ICollection_1_t7329____Count_PropertyInfo,
	&ICollection_1_t7329____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41680_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41680_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7329_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41680_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41681_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41681_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7329_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41681_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorCylinderTargetBehaviour_t123_0_0_0;
extern Il2CppType IEditorCylinderTargetBehaviour_t123_0_0_0;
static ParameterInfo ICollection_1_t7329_ICollection_1_Add_m41682_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorCylinderTargetBehaviour_t123_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41682_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41682_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7329_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7329_ICollection_1_Add_m41682_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41682_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41683_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41683_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7329_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41683_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorCylinderTargetBehaviour_t123_0_0_0;
static ParameterInfo ICollection_1_t7329_ICollection_1_Contains_m41684_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorCylinderTargetBehaviour_t123_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41684_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41684_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7329_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7329_ICollection_1_Contains_m41684_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41684_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorCylinderTargetBehaviourU5BU5D_t5494_0_0_0;
extern Il2CppType IEditorCylinderTargetBehaviourU5BU5D_t5494_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7329_ICollection_1_CopyTo_m41685_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorCylinderTargetBehaviourU5BU5D_t5494_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41685_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41685_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7329_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7329_ICollection_1_CopyTo_m41685_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41685_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorCylinderTargetBehaviour_t123_0_0_0;
static ParameterInfo ICollection_1_t7329_ICollection_1_Remove_m41686_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorCylinderTargetBehaviour_t123_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41686_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorCylinderTargetBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41686_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7329_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7329_ICollection_1_Remove_m41686_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41686_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7329_MethodInfos[] =
{
	&ICollection_1_get_Count_m41680_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41681_MethodInfo,
	&ICollection_1_Add_m41682_MethodInfo,
	&ICollection_1_Clear_m41683_MethodInfo,
	&ICollection_1_Contains_m41684_MethodInfo,
	&ICollection_1_CopyTo_m41685_MethodInfo,
	&ICollection_1_Remove_m41686_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7331_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7329_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7331_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7329_0_0_0;
extern Il2CppType ICollection_1_t7329_1_0_0;
struct ICollection_1_t7329;
extern Il2CppGenericClass ICollection_1_t7329_GenericClass;
TypeInfo ICollection_1_t7329_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7329_MethodInfos/* methods */
	, ICollection_1_t7329_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7329_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7329_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7329_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7329_0_0_0/* byval_arg */
	, &ICollection_1_t7329_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7329_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorCylinderTargetBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorCylinderTargetBehaviour>
extern Il2CppType IEnumerator_1_t5771_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41687_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorCylinderTargetBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41687_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7331_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5771_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41687_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7331_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41687_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7331_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7331_0_0_0;
extern Il2CppType IEnumerable_1_t7331_1_0_0;
struct IEnumerable_1_t7331;
extern Il2CppGenericClass IEnumerable_1_t7331_GenericClass;
TypeInfo IEnumerable_1_t7331_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7331_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7331_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7331_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7331_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7331_0_0_0/* byval_arg */
	, &IEnumerable_1_t7331_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7331_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5771_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41688_MethodInfo;
static PropertyInfo IEnumerator_1_t5771____Current_PropertyInfo = 
{
	&IEnumerator_1_t5771_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41688_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5771_PropertyInfos[] =
{
	&IEnumerator_1_t5771____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorCylinderTargetBehaviour_t123_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41688_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41688_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5771_il2cpp_TypeInfo/* declaring_type */
	, &IEditorCylinderTargetBehaviour_t123_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41688_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5771_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41688_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5771_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5771_0_0_0;
extern Il2CppType IEnumerator_1_t5771_1_0_0;
struct IEnumerator_1_t5771;
extern Il2CppGenericClass IEnumerator_1_t5771_GenericClass;
TypeInfo IEnumerator_1_t5771_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5771_MethodInfos/* methods */
	, IEnumerator_1_t5771_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5771_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5771_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5771_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5771_0_0_0/* byval_arg */
	, &IEnumerator_1_t5771_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5771_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_42.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2787_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_42MethodDeclarations.h"

extern TypeInfo IEditorCylinderTargetBehaviour_t123_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14336_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorCylinderTargetBehaviour_t123_m31925_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorCylinderTargetBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorCylinderTargetBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorCylinderTargetBehaviour_t123_m31925(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2787____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2787_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2787, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2787____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2787_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2787, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2787_FieldInfos[] =
{
	&InternalEnumerator_1_t2787____array_0_FieldInfo,
	&InternalEnumerator_1_t2787____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14333_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2787____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2787_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14333_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2787____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2787_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14336_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2787_PropertyInfos[] =
{
	&InternalEnumerator_1_t2787____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2787____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2787_InternalEnumerator_1__ctor_m14332_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14332_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14332_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2787_InternalEnumerator_1__ctor_m14332_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14332_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14333_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14333_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2787_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14333_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14334_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14334_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2787_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14334_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14335_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14335_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2787_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14335_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorCylinderTargetBehaviour_t123_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14336_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14336_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2787_il2cpp_TypeInfo/* declaring_type */
	, &IEditorCylinderTargetBehaviour_t123_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14336_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2787_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14332_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14333_MethodInfo,
	&InternalEnumerator_1_Dispose_m14334_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14335_MethodInfo,
	&InternalEnumerator_1_get_Current_m14336_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14335_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14334_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2787_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14333_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14335_MethodInfo,
	&InternalEnumerator_1_Dispose_m14334_MethodInfo,
	&InternalEnumerator_1_get_Current_m14336_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2787_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5771_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2787_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5771_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorCylinderTargetBehaviour_t123_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2787_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14336_MethodInfo/* Method Usage */,
	&IEditorCylinderTargetBehaviour_t123_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorCylinderTargetBehaviour_t123_m31925_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2787_0_0_0;
extern Il2CppType InternalEnumerator_1_t2787_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2787_GenericClass;
TypeInfo InternalEnumerator_1_t2787_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2787_MethodInfos/* methods */
	, InternalEnumerator_1_t2787_PropertyInfos/* properties */
	, InternalEnumerator_1_t2787_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2787_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2787_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2787_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2787_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2787_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2787_1_0_0/* this_arg */
	, InternalEnumerator_1_t2787_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2787_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2787_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2787)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7330_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>
extern MethodInfo IList_1_get_Item_m41689_MethodInfo;
extern MethodInfo IList_1_set_Item_m41690_MethodInfo;
static PropertyInfo IList_1_t7330____Item_PropertyInfo = 
{
	&IList_1_t7330_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41689_MethodInfo/* get */
	, &IList_1_set_Item_m41690_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7330_PropertyInfos[] =
{
	&IList_1_t7330____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorCylinderTargetBehaviour_t123_0_0_0;
static ParameterInfo IList_1_t7330_IList_1_IndexOf_m41691_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorCylinderTargetBehaviour_t123_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41691_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41691_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7330_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7330_IList_1_IndexOf_m41691_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41691_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorCylinderTargetBehaviour_t123_0_0_0;
static ParameterInfo IList_1_t7330_IList_1_Insert_m41692_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorCylinderTargetBehaviour_t123_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41692_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41692_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7330_IList_1_Insert_m41692_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41692_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7330_IList_1_RemoveAt_m41693_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41693_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41693_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7330_IList_1_RemoveAt_m41693_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41693_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7330_IList_1_get_Item_m41689_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEditorCylinderTargetBehaviour_t123_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41689_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41689_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7330_il2cpp_TypeInfo/* declaring_type */
	, &IEditorCylinderTargetBehaviour_t123_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7330_IList_1_get_Item_m41689_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41689_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorCylinderTargetBehaviour_t123_0_0_0;
static ParameterInfo IList_1_t7330_IList_1_set_Item_m41690_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorCylinderTargetBehaviour_t123_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41690_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorCylinderTargetBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41690_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7330_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7330_IList_1_set_Item_m41690_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41690_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7330_MethodInfos[] =
{
	&IList_1_IndexOf_m41691_MethodInfo,
	&IList_1_Insert_m41692_MethodInfo,
	&IList_1_RemoveAt_m41693_MethodInfo,
	&IList_1_get_Item_m41689_MethodInfo,
	&IList_1_set_Item_m41690_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7330_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7329_il2cpp_TypeInfo,
	&IEnumerable_1_t7331_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7330_0_0_0;
extern Il2CppType IList_1_t7330_1_0_0;
struct IList_1_t7330;
extern Il2CppGenericClass IList_1_t7330_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7330_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7330_MethodInfos/* methods */
	, IList_1_t7330_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7330_il2cpp_TypeInfo/* element_class */
	, IList_1_t7330_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7330_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7330_0_0_0/* byval_arg */
	, &IList_1_t7330_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7330_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7332_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>
extern MethodInfo ICollection_1_get_Count_m41694_MethodInfo;
static PropertyInfo ICollection_1_t7332____Count_PropertyInfo = 
{
	&ICollection_1_t7332_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41694_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41695_MethodInfo;
static PropertyInfo ICollection_1_t7332____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7332_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41695_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7332_PropertyInfos[] =
{
	&ICollection_1_t7332____Count_PropertyInfo,
	&ICollection_1_t7332____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41694_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41694_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7332_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41694_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41695_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41695_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7332_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41695_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorDataSetTrackableBehaviour_t124_0_0_0;
extern Il2CppType IEditorDataSetTrackableBehaviour_t124_0_0_0;
static ParameterInfo ICollection_1_t7332_ICollection_1_Add_m41696_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorDataSetTrackableBehaviour_t124_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41696_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41696_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7332_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7332_ICollection_1_Add_m41696_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41696_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41697_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41697_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7332_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41697_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorDataSetTrackableBehaviour_t124_0_0_0;
static ParameterInfo ICollection_1_t7332_ICollection_1_Contains_m41698_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorDataSetTrackableBehaviour_t124_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41698_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41698_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7332_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7332_ICollection_1_Contains_m41698_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41698_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorDataSetTrackableBehaviourU5BU5D_t5495_0_0_0;
extern Il2CppType IEditorDataSetTrackableBehaviourU5BU5D_t5495_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7332_ICollection_1_CopyTo_m41699_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorDataSetTrackableBehaviourU5BU5D_t5495_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41699_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41699_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7332_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7332_ICollection_1_CopyTo_m41699_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41699_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorDataSetTrackableBehaviour_t124_0_0_0;
static ParameterInfo ICollection_1_t7332_ICollection_1_Remove_m41700_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorDataSetTrackableBehaviour_t124_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41700_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorDataSetTrackableBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41700_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7332_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7332_ICollection_1_Remove_m41700_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41700_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7332_MethodInfos[] =
{
	&ICollection_1_get_Count_m41694_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41695_MethodInfo,
	&ICollection_1_Add_m41696_MethodInfo,
	&ICollection_1_Clear_m41697_MethodInfo,
	&ICollection_1_Contains_m41698_MethodInfo,
	&ICollection_1_CopyTo_m41699_MethodInfo,
	&ICollection_1_Remove_m41700_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7334_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7332_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7334_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7332_0_0_0;
extern Il2CppType ICollection_1_t7332_1_0_0;
struct ICollection_1_t7332;
extern Il2CppGenericClass ICollection_1_t7332_GenericClass;
TypeInfo ICollection_1_t7332_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7332_MethodInfos/* methods */
	, ICollection_1_t7332_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7332_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7332_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7332_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7332_0_0_0/* byval_arg */
	, &ICollection_1_t7332_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7332_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorDataSetTrackableBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorDataSetTrackableBehaviour>
extern Il2CppType IEnumerator_1_t5773_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41701_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorDataSetTrackableBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41701_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7334_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5773_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41701_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7334_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41701_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7334_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7334_0_0_0;
extern Il2CppType IEnumerable_1_t7334_1_0_0;
struct IEnumerable_1_t7334;
extern Il2CppGenericClass IEnumerable_1_t7334_GenericClass;
TypeInfo IEnumerable_1_t7334_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7334_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7334_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7334_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7334_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7334_0_0_0/* byval_arg */
	, &IEnumerable_1_t7334_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7334_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5773_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41702_MethodInfo;
static PropertyInfo IEnumerator_1_t5773____Current_PropertyInfo = 
{
	&IEnumerator_1_t5773_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41702_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5773_PropertyInfos[] =
{
	&IEnumerator_1_t5773____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorDataSetTrackableBehaviour_t124_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41702_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41702_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5773_il2cpp_TypeInfo/* declaring_type */
	, &IEditorDataSetTrackableBehaviour_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41702_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5773_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41702_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5773_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5773_0_0_0;
extern Il2CppType IEnumerator_1_t5773_1_0_0;
struct IEnumerator_1_t5773;
extern Il2CppGenericClass IEnumerator_1_t5773_GenericClass;
TypeInfo IEnumerator_1_t5773_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5773_MethodInfos/* methods */
	, IEnumerator_1_t5773_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5773_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5773_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5773_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5773_0_0_0/* byval_arg */
	, &IEnumerator_1_t5773_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5773_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_43.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2788_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_43MethodDeclarations.h"

extern TypeInfo IEditorDataSetTrackableBehaviour_t124_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14341_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorDataSetTrackableBehaviour_t124_m31936_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorDataSetTrackableBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorDataSetTrackableBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorDataSetTrackableBehaviour_t124_m31936(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2788____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2788_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2788, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2788____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2788_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2788, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2788_FieldInfos[] =
{
	&InternalEnumerator_1_t2788____array_0_FieldInfo,
	&InternalEnumerator_1_t2788____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14338_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2788____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2788_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14338_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2788____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2788_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14341_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2788_PropertyInfos[] =
{
	&InternalEnumerator_1_t2788____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2788____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2788_InternalEnumerator_1__ctor_m14337_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14337_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14337_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2788_InternalEnumerator_1__ctor_m14337_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14337_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14338_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14338_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2788_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14338_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14339_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14339_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14339_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14340_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14340_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14340_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorDataSetTrackableBehaviour_t124_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14341_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14341_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2788_il2cpp_TypeInfo/* declaring_type */
	, &IEditorDataSetTrackableBehaviour_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14341_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2788_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14337_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14338_MethodInfo,
	&InternalEnumerator_1_Dispose_m14339_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14340_MethodInfo,
	&InternalEnumerator_1_get_Current_m14341_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14340_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14339_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2788_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14338_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14340_MethodInfo,
	&InternalEnumerator_1_Dispose_m14339_MethodInfo,
	&InternalEnumerator_1_get_Current_m14341_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2788_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5773_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2788_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5773_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorDataSetTrackableBehaviour_t124_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2788_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14341_MethodInfo/* Method Usage */,
	&IEditorDataSetTrackableBehaviour_t124_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorDataSetTrackableBehaviour_t124_m31936_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2788_0_0_0;
extern Il2CppType InternalEnumerator_1_t2788_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2788_GenericClass;
TypeInfo InternalEnumerator_1_t2788_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2788_MethodInfos/* methods */
	, InternalEnumerator_1_t2788_PropertyInfos/* properties */
	, InternalEnumerator_1_t2788_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2788_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2788_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2788_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2788_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2788_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2788_1_0_0/* this_arg */
	, InternalEnumerator_1_t2788_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2788_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2788_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2788)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7333_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>
extern MethodInfo IList_1_get_Item_m41703_MethodInfo;
extern MethodInfo IList_1_set_Item_m41704_MethodInfo;
static PropertyInfo IList_1_t7333____Item_PropertyInfo = 
{
	&IList_1_t7333_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41703_MethodInfo/* get */
	, &IList_1_set_Item_m41704_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7333_PropertyInfos[] =
{
	&IList_1_t7333____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorDataSetTrackableBehaviour_t124_0_0_0;
static ParameterInfo IList_1_t7333_IList_1_IndexOf_m41705_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorDataSetTrackableBehaviour_t124_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41705_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41705_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7333_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7333_IList_1_IndexOf_m41705_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41705_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorDataSetTrackableBehaviour_t124_0_0_0;
static ParameterInfo IList_1_t7333_IList_1_Insert_m41706_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorDataSetTrackableBehaviour_t124_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41706_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41706_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7333_IList_1_Insert_m41706_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41706_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7333_IList_1_RemoveAt_m41707_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41707_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41707_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7333_IList_1_RemoveAt_m41707_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41707_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7333_IList_1_get_Item_m41703_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEditorDataSetTrackableBehaviour_t124_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41703_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41703_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7333_il2cpp_TypeInfo/* declaring_type */
	, &IEditorDataSetTrackableBehaviour_t124_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7333_IList_1_get_Item_m41703_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41703_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorDataSetTrackableBehaviour_t124_0_0_0;
static ParameterInfo IList_1_t7333_IList_1_set_Item_m41704_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorDataSetTrackableBehaviour_t124_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41704_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorDataSetTrackableBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41704_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7333_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7333_IList_1_set_Item_m41704_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41704_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7333_MethodInfos[] =
{
	&IList_1_IndexOf_m41705_MethodInfo,
	&IList_1_Insert_m41706_MethodInfo,
	&IList_1_RemoveAt_m41707_MethodInfo,
	&IList_1_get_Item_m41703_MethodInfo,
	&IList_1_set_Item_m41704_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7333_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7332_il2cpp_TypeInfo,
	&IEnumerable_1_t7334_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7333_0_0_0;
extern Il2CppType IList_1_t7333_1_0_0;
struct IList_1_t7333;
extern Il2CppGenericClass IList_1_t7333_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7333_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7333_MethodInfos/* methods */
	, IList_1_t7333_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7333_il2cpp_TypeInfo/* element_class */
	, IList_1_t7333_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7333_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7333_0_0_0/* byval_arg */
	, &IList_1_t7333_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7333_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7335_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>
extern MethodInfo ICollection_1_get_Count_m41708_MethodInfo;
static PropertyInfo ICollection_1_t7335____Count_PropertyInfo = 
{
	&ICollection_1_t7335_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41708_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41709_MethodInfo;
static PropertyInfo ICollection_1_t7335____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7335_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41709_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7335_PropertyInfos[] =
{
	&ICollection_1_t7335____Count_PropertyInfo,
	&ICollection_1_t7335____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41708_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41708_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7335_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41708_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41709_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41709_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7335_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41709_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTrackableBehaviour_t125_0_0_0;
extern Il2CppType IEditorTrackableBehaviour_t125_0_0_0;
static ParameterInfo ICollection_1_t7335_ICollection_1_Add_m41710_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTrackableBehaviour_t125_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41710_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41710_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7335_ICollection_1_Add_m41710_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41710_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41711_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41711_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41711_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTrackableBehaviour_t125_0_0_0;
static ParameterInfo ICollection_1_t7335_ICollection_1_Contains_m41712_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTrackableBehaviour_t125_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41712_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41712_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7335_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7335_ICollection_1_Contains_m41712_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41712_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTrackableBehaviourU5BU5D_t5496_0_0_0;
extern Il2CppType IEditorTrackableBehaviourU5BU5D_t5496_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7335_ICollection_1_CopyTo_m41713_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTrackableBehaviourU5BU5D_t5496_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41713_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41713_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7335_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7335_ICollection_1_CopyTo_m41713_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41713_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTrackableBehaviour_t125_0_0_0;
static ParameterInfo ICollection_1_t7335_ICollection_1_Remove_m41714_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTrackableBehaviour_t125_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41714_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTrackableBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41714_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7335_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7335_ICollection_1_Remove_m41714_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41714_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7335_MethodInfos[] =
{
	&ICollection_1_get_Count_m41708_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41709_MethodInfo,
	&ICollection_1_Add_m41710_MethodInfo,
	&ICollection_1_Clear_m41711_MethodInfo,
	&ICollection_1_Contains_m41712_MethodInfo,
	&ICollection_1_CopyTo_m41713_MethodInfo,
	&ICollection_1_Remove_m41714_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7337_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7335_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7337_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7335_0_0_0;
extern Il2CppType ICollection_1_t7335_1_0_0;
struct ICollection_1_t7335;
extern Il2CppGenericClass ICollection_1_t7335_GenericClass;
TypeInfo ICollection_1_t7335_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7335_MethodInfos/* methods */
	, ICollection_1_t7335_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7335_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7335_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7335_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7335_0_0_0/* byval_arg */
	, &ICollection_1_t7335_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7335_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorTrackableBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorTrackableBehaviour>
extern Il2CppType IEnumerator_1_t5775_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41715_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorTrackableBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41715_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7337_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5775_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41715_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7337_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41715_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7337_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7337_0_0_0;
extern Il2CppType IEnumerable_1_t7337_1_0_0;
struct IEnumerable_1_t7337;
extern Il2CppGenericClass IEnumerable_1_t7337_GenericClass;
TypeInfo IEnumerable_1_t7337_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7337_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7337_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7337_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7337_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7337_0_0_0/* byval_arg */
	, &IEnumerable_1_t7337_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7337_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5775_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorTrackableBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorTrackableBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41716_MethodInfo;
static PropertyInfo IEnumerator_1_t5775____Current_PropertyInfo = 
{
	&IEnumerator_1_t5775_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41716_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5775_PropertyInfos[] =
{
	&IEnumerator_1_t5775____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorTrackableBehaviour_t125_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41716_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorTrackableBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41716_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5775_il2cpp_TypeInfo/* declaring_type */
	, &IEditorTrackableBehaviour_t125_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41716_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5775_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41716_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5775_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5775_0_0_0;
extern Il2CppType IEnumerator_1_t5775_1_0_0;
struct IEnumerator_1_t5775;
extern Il2CppGenericClass IEnumerator_1_t5775_GenericClass;
TypeInfo IEnumerator_1_t5775_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5775_MethodInfos/* methods */
	, IEnumerator_1_t5775_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5775_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5775_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5775_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5775_0_0_0/* byval_arg */
	, &IEnumerator_1_t5775_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5775_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_44.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2789_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_44MethodDeclarations.h"

extern TypeInfo IEditorTrackableBehaviour_t125_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14346_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorTrackableBehaviour_t125_m31947_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorTrackableBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorTrackableBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorTrackableBehaviour_t125_m31947(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2789____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2789_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2789, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2789____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2789_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2789, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2789_FieldInfos[] =
{
	&InternalEnumerator_1_t2789____array_0_FieldInfo,
	&InternalEnumerator_1_t2789____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14343_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2789____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2789_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14343_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2789____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2789_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14346_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2789_PropertyInfos[] =
{
	&InternalEnumerator_1_t2789____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2789____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2789_InternalEnumerator_1__ctor_m14342_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14342_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14342_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2789_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2789_InternalEnumerator_1__ctor_m14342_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14342_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14343_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14343_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2789_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14343_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14344_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14344_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2789_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14344_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14345_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14345_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2789_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14345_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTrackableBehaviour_t125_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14346_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorTrackableBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14346_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2789_il2cpp_TypeInfo/* declaring_type */
	, &IEditorTrackableBehaviour_t125_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14346_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2789_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14342_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14343_MethodInfo,
	&InternalEnumerator_1_Dispose_m14344_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14345_MethodInfo,
	&InternalEnumerator_1_get_Current_m14346_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14345_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14344_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2789_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14343_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14345_MethodInfo,
	&InternalEnumerator_1_Dispose_m14344_MethodInfo,
	&InternalEnumerator_1_get_Current_m14346_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2789_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5775_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2789_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5775_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorTrackableBehaviour_t125_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2789_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14346_MethodInfo/* Method Usage */,
	&IEditorTrackableBehaviour_t125_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorTrackableBehaviour_t125_m31947_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2789_0_0_0;
extern Il2CppType InternalEnumerator_1_t2789_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2789_GenericClass;
TypeInfo InternalEnumerator_1_t2789_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2789_MethodInfos/* methods */
	, InternalEnumerator_1_t2789_PropertyInfos/* properties */
	, InternalEnumerator_1_t2789_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2789_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2789_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2789_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2789_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2789_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2789_1_0_0/* this_arg */
	, InternalEnumerator_1_t2789_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2789_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2789_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2789)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7336_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>
extern MethodInfo IList_1_get_Item_m41717_MethodInfo;
extern MethodInfo IList_1_set_Item_m41718_MethodInfo;
static PropertyInfo IList_1_t7336____Item_PropertyInfo = 
{
	&IList_1_t7336_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41717_MethodInfo/* get */
	, &IList_1_set_Item_m41718_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7336_PropertyInfos[] =
{
	&IList_1_t7336____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorTrackableBehaviour_t125_0_0_0;
static ParameterInfo IList_1_t7336_IList_1_IndexOf_m41719_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTrackableBehaviour_t125_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41719_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41719_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7336_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7336_IList_1_IndexOf_m41719_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41719_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorTrackableBehaviour_t125_0_0_0;
static ParameterInfo IList_1_t7336_IList_1_Insert_m41720_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorTrackableBehaviour_t125_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41720_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41720_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7336_IList_1_Insert_m41720_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41720_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7336_IList_1_RemoveAt_m41721_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41721_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41721_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7336_IList_1_RemoveAt_m41721_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41721_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7336_IList_1_get_Item_m41717_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEditorTrackableBehaviour_t125_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41717_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41717_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7336_il2cpp_TypeInfo/* declaring_type */
	, &IEditorTrackableBehaviour_t125_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7336_IList_1_get_Item_m41717_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41717_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorTrackableBehaviour_t125_0_0_0;
static ParameterInfo IList_1_t7336_IList_1_set_Item_m41718_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorTrackableBehaviour_t125_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41718_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTrackableBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41718_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7336_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7336_IList_1_set_Item_m41718_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41718_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7336_MethodInfos[] =
{
	&IList_1_IndexOf_m41719_MethodInfo,
	&IList_1_Insert_m41720_MethodInfo,
	&IList_1_RemoveAt_m41721_MethodInfo,
	&IList_1_get_Item_m41717_MethodInfo,
	&IList_1_set_Item_m41718_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7336_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7335_il2cpp_TypeInfo,
	&IEnumerable_1_t7337_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7336_0_0_0;
extern Il2CppType IList_1_t7336_1_0_0;
struct IList_1_t7336;
extern Il2CppGenericClass IList_1_t7336_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7336_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7336_MethodInfos/* methods */
	, IList_1_t7336_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7336_il2cpp_TypeInfo/* element_class */
	, IList_1_t7336_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7336_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7336_0_0_0/* byval_arg */
	, &IList_1_t7336_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7336_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7338_il2cpp_TypeInfo;

// Vuforia.DataSetTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetTrackableBeh.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>
extern MethodInfo ICollection_1_get_Count_m41722_MethodInfo;
static PropertyInfo ICollection_1_t7338____Count_PropertyInfo = 
{
	&ICollection_1_t7338_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41722_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41723_MethodInfo;
static PropertyInfo ICollection_1_t7338____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7338_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41723_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7338_PropertyInfos[] =
{
	&ICollection_1_t7338____Count_PropertyInfo,
	&ICollection_1_t7338____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41722_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41722_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7338_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41722_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41723_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41723_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7338_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41723_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetTrackableBehaviour_t552_0_0_0;
extern Il2CppType DataSetTrackableBehaviour_t552_0_0_0;
static ParameterInfo ICollection_1_t7338_ICollection_1_Add_m41724_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetTrackableBehaviour_t552_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41724_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41724_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7338_ICollection_1_Add_m41724_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41724_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41725_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41725_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41725_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetTrackableBehaviour_t552_0_0_0;
static ParameterInfo ICollection_1_t7338_ICollection_1_Contains_m41726_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetTrackableBehaviour_t552_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41726_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41726_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7338_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7338_ICollection_1_Contains_m41726_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41726_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetTrackableBehaviourU5BU5D_t865_0_0_0;
extern Il2CppType DataSetTrackableBehaviourU5BU5D_t865_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7338_ICollection_1_CopyTo_m41727_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DataSetTrackableBehaviourU5BU5D_t865_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41727_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41727_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7338_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7338_ICollection_1_CopyTo_m41727_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41727_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetTrackableBehaviour_t552_0_0_0;
static ParameterInfo ICollection_1_t7338_ICollection_1_Remove_m41728_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetTrackableBehaviour_t552_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41728_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetTrackableBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41728_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7338_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7338_ICollection_1_Remove_m41728_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41728_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7338_MethodInfos[] =
{
	&ICollection_1_get_Count_m41722_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41723_MethodInfo,
	&ICollection_1_Add_m41724_MethodInfo,
	&ICollection_1_Clear_m41725_MethodInfo,
	&ICollection_1_Contains_m41726_MethodInfo,
	&ICollection_1_CopyTo_m41727_MethodInfo,
	&ICollection_1_Remove_m41728_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7340_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7338_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7340_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7338_0_0_0;
extern Il2CppType ICollection_1_t7338_1_0_0;
struct ICollection_1_t7338;
extern Il2CppGenericClass ICollection_1_t7338_GenericClass;
TypeInfo ICollection_1_t7338_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7338_MethodInfos/* methods */
	, ICollection_1_t7338_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7338_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7338_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7338_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7338_0_0_0/* byval_arg */
	, &ICollection_1_t7338_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7338_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DataSetTrackableBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.DataSetTrackableBehaviour>
extern Il2CppType IEnumerator_1_t5777_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41729_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DataSetTrackableBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41729_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7340_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5777_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41729_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7340_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41729_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7340_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7340_0_0_0;
extern Il2CppType IEnumerable_1_t7340_1_0_0;
struct IEnumerable_1_t7340;
extern Il2CppGenericClass IEnumerable_1_t7340_GenericClass;
TypeInfo IEnumerable_1_t7340_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7340_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7340_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7340_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7340_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7340_0_0_0/* byval_arg */
	, &IEnumerable_1_t7340_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7340_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5777_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.DataSetTrackableBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.DataSetTrackableBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41730_MethodInfo;
static PropertyInfo IEnumerator_1_t5777____Current_PropertyInfo = 
{
	&IEnumerator_1_t5777_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41730_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5777_PropertyInfos[] =
{
	&IEnumerator_1_t5777____Current_PropertyInfo,
	NULL
};
extern Il2CppType DataSetTrackableBehaviour_t552_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41730_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.DataSetTrackableBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41730_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5777_il2cpp_TypeInfo/* declaring_type */
	, &DataSetTrackableBehaviour_t552_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41730_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5777_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41730_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5777_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5777_0_0_0;
extern Il2CppType IEnumerator_1_t5777_1_0_0;
struct IEnumerator_1_t5777;
extern Il2CppGenericClass IEnumerator_1_t5777_GenericClass;
TypeInfo IEnumerator_1_t5777_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5777_MethodInfos/* methods */
	, IEnumerator_1_t5777_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5777_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5777_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5777_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5777_0_0_0/* byval_arg */
	, &IEnumerator_1_t5777_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5777_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_45.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2790_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_45MethodDeclarations.h"

extern TypeInfo DataSetTrackableBehaviour_t552_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14351_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDataSetTrackableBehaviour_t552_m31958_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.DataSetTrackableBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.DataSetTrackableBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisDataSetTrackableBehaviour_t552_m31958(__this, p0, method) (DataSetTrackableBehaviour_t552 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2790____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2790, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2790____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2790, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2790_FieldInfos[] =
{
	&InternalEnumerator_1_t2790____array_0_FieldInfo,
	&InternalEnumerator_1_t2790____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14348_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2790____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2790_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14348_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2790____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2790_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14351_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2790_PropertyInfos[] =
{
	&InternalEnumerator_1_t2790____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2790____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2790_InternalEnumerator_1__ctor_m14347_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14347_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14347_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2790_InternalEnumerator_1__ctor_m14347_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14347_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14348_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14348_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14348_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14349_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14349_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14349_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14350_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14350_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14350_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetTrackableBehaviour_t552_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14351_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.DataSetTrackableBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14351_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* declaring_type */
	, &DataSetTrackableBehaviour_t552_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14351_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2790_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14347_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14348_MethodInfo,
	&InternalEnumerator_1_Dispose_m14349_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14350_MethodInfo,
	&InternalEnumerator_1_get_Current_m14351_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14350_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14349_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2790_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14348_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14350_MethodInfo,
	&InternalEnumerator_1_Dispose_m14349_MethodInfo,
	&InternalEnumerator_1_get_Current_m14351_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2790_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5777_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2790_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5777_il2cpp_TypeInfo, 7},
};
extern TypeInfo DataSetTrackableBehaviour_t552_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2790_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14351_MethodInfo/* Method Usage */,
	&DataSetTrackableBehaviour_t552_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDataSetTrackableBehaviour_t552_m31958_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2790_0_0_0;
extern Il2CppType InternalEnumerator_1_t2790_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2790_GenericClass;
TypeInfo InternalEnumerator_1_t2790_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2790_MethodInfos/* methods */
	, InternalEnumerator_1_t2790_PropertyInfos/* properties */
	, InternalEnumerator_1_t2790_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2790_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2790_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2790_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2790_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2790_1_0_0/* this_arg */
	, InternalEnumerator_1_t2790_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2790_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2790_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2790)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7339_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>
extern MethodInfo IList_1_get_Item_m41731_MethodInfo;
extern MethodInfo IList_1_set_Item_m41732_MethodInfo;
static PropertyInfo IList_1_t7339____Item_PropertyInfo = 
{
	&IList_1_t7339_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41731_MethodInfo/* get */
	, &IList_1_set_Item_m41732_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7339_PropertyInfos[] =
{
	&IList_1_t7339____Item_PropertyInfo,
	NULL
};
extern Il2CppType DataSetTrackableBehaviour_t552_0_0_0;
static ParameterInfo IList_1_t7339_IList_1_IndexOf_m41733_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetTrackableBehaviour_t552_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41733_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41733_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7339_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7339_IList_1_IndexOf_m41733_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41733_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DataSetTrackableBehaviour_t552_0_0_0;
static ParameterInfo IList_1_t7339_IList_1_Insert_m41734_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DataSetTrackableBehaviour_t552_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41734_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41734_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7339_IList_1_Insert_m41734_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41734_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7339_IList_1_RemoveAt_m41735_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41735_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41735_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7339_IList_1_RemoveAt_m41735_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41735_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7339_IList_1_get_Item_m41731_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType DataSetTrackableBehaviour_t552_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41731_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41731_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7339_il2cpp_TypeInfo/* declaring_type */
	, &DataSetTrackableBehaviour_t552_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7339_IList_1_get_Item_m41731_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41731_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DataSetTrackableBehaviour_t552_0_0_0;
static ParameterInfo IList_1_t7339_IList_1_set_Item_m41732_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DataSetTrackableBehaviour_t552_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41732_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetTrackableBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41732_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7339_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7339_IList_1_set_Item_m41732_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41732_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7339_MethodInfos[] =
{
	&IList_1_IndexOf_m41733_MethodInfo,
	&IList_1_Insert_m41734_MethodInfo,
	&IList_1_RemoveAt_m41735_MethodInfo,
	&IList_1_get_Item_m41731_MethodInfo,
	&IList_1_set_Item_m41732_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7339_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7338_il2cpp_TypeInfo,
	&IEnumerable_1_t7340_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7339_0_0_0;
extern Il2CppType IList_1_t7339_1_0_0;
struct IList_1_t7339;
extern Il2CppGenericClass IList_1_t7339_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7339_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7339_MethodInfos/* methods */
	, IList_1_t7339_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7339_il2cpp_TypeInfo/* element_class */
	, IList_1_t7339_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7339_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7339_0_0_0/* byval_arg */
	, &IList_1_t7339_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7339_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7341_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>
extern MethodInfo ICollection_1_get_Count_m41736_MethodInfo;
static PropertyInfo ICollection_1_t7341____Count_PropertyInfo = 
{
	&ICollection_1_t7341_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41736_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41737_MethodInfo;
static PropertyInfo ICollection_1_t7341____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7341_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41737_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7341_PropertyInfos[] =
{
	&ICollection_1_t7341____Count_PropertyInfo,
	&ICollection_1_t7341____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41736_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41736_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7341_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41736_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41737_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41737_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7341_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41737_GenericMethod/* genericMethod */

};
extern Il2CppType WorldCenterTrackableBehaviour_t126_0_0_0;
extern Il2CppType WorldCenterTrackableBehaviour_t126_0_0_0;
static ParameterInfo ICollection_1_t7341_ICollection_1_Add_m41738_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldCenterTrackableBehaviour_t126_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41738_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41738_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7341_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7341_ICollection_1_Add_m41738_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41738_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41739_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41739_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7341_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41739_GenericMethod/* genericMethod */

};
extern Il2CppType WorldCenterTrackableBehaviour_t126_0_0_0;
static ParameterInfo ICollection_1_t7341_ICollection_1_Contains_m41740_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldCenterTrackableBehaviour_t126_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41740_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41740_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7341_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7341_ICollection_1_Contains_m41740_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41740_GenericMethod/* genericMethod */

};
extern Il2CppType WorldCenterTrackableBehaviourU5BU5D_t5497_0_0_0;
extern Il2CppType WorldCenterTrackableBehaviourU5BU5D_t5497_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7341_ICollection_1_CopyTo_m41741_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WorldCenterTrackableBehaviourU5BU5D_t5497_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41741_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41741_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7341_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7341_ICollection_1_CopyTo_m41741_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41741_GenericMethod/* genericMethod */

};
extern Il2CppType WorldCenterTrackableBehaviour_t126_0_0_0;
static ParameterInfo ICollection_1_t7341_ICollection_1_Remove_m41742_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldCenterTrackableBehaviour_t126_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41742_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WorldCenterTrackableBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41742_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7341_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7341_ICollection_1_Remove_m41742_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41742_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7341_MethodInfos[] =
{
	&ICollection_1_get_Count_m41736_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41737_MethodInfo,
	&ICollection_1_Add_m41738_MethodInfo,
	&ICollection_1_Clear_m41739_MethodInfo,
	&ICollection_1_Contains_m41740_MethodInfo,
	&ICollection_1_CopyTo_m41741_MethodInfo,
	&ICollection_1_Remove_m41742_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7343_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7341_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7343_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7341_0_0_0;
extern Il2CppType ICollection_1_t7341_1_0_0;
struct ICollection_1_t7341;
extern Il2CppGenericClass ICollection_1_t7341_GenericClass;
TypeInfo ICollection_1_t7341_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7341_MethodInfos/* methods */
	, ICollection_1_t7341_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7341_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7341_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7341_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7341_0_0_0/* byval_arg */
	, &ICollection_1_t7341_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7341_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WorldCenterTrackableBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.WorldCenterTrackableBehaviour>
extern Il2CppType IEnumerator_1_t5779_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41743_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WorldCenterTrackableBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41743_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7343_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5779_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41743_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7343_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41743_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7343_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7343_0_0_0;
extern Il2CppType IEnumerable_1_t7343_1_0_0;
struct IEnumerable_1_t7343;
extern Il2CppGenericClass IEnumerable_1_t7343_GenericClass;
TypeInfo IEnumerable_1_t7343_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7343_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7343_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7343_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7343_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7343_0_0_0/* byval_arg */
	, &IEnumerable_1_t7343_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7343_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5779_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41744_MethodInfo;
static PropertyInfo IEnumerator_1_t5779____Current_PropertyInfo = 
{
	&IEnumerator_1_t5779_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41744_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5779_PropertyInfos[] =
{
	&IEnumerator_1_t5779____Current_PropertyInfo,
	NULL
};
extern Il2CppType WorldCenterTrackableBehaviour_t126_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41744_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41744_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5779_il2cpp_TypeInfo/* declaring_type */
	, &WorldCenterTrackableBehaviour_t126_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41744_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5779_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41744_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5779_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5779_0_0_0;
extern Il2CppType IEnumerator_1_t5779_1_0_0;
struct IEnumerator_1_t5779;
extern Il2CppGenericClass IEnumerator_1_t5779_GenericClass;
TypeInfo IEnumerator_1_t5779_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5779_MethodInfos/* methods */
	, IEnumerator_1_t5779_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5779_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5779_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5779_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5779_0_0_0/* byval_arg */
	, &IEnumerator_1_t5779_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5779_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_46.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2791_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_46MethodDeclarations.h"

extern TypeInfo WorldCenterTrackableBehaviour_t126_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14356_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWorldCenterTrackableBehaviour_t126_m31969_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.WorldCenterTrackableBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.WorldCenterTrackableBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisWorldCenterTrackableBehaviour_t126_m31969(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2791____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2791, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2791____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2791, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2791_FieldInfos[] =
{
	&InternalEnumerator_1_t2791____array_0_FieldInfo,
	&InternalEnumerator_1_t2791____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14353_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2791____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2791_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14353_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2791____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2791_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14356_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2791_PropertyInfos[] =
{
	&InternalEnumerator_1_t2791____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2791____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2791_InternalEnumerator_1__ctor_m14352_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14352_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14352_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2791_InternalEnumerator_1__ctor_m14352_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14352_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14353_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14353_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14353_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14354_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14354_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14354_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14355_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14355_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14355_GenericMethod/* genericMethod */

};
extern Il2CppType WorldCenterTrackableBehaviour_t126_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14356_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.WorldCenterTrackableBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14356_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* declaring_type */
	, &WorldCenterTrackableBehaviour_t126_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14356_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2791_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14352_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14353_MethodInfo,
	&InternalEnumerator_1_Dispose_m14354_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14355_MethodInfo,
	&InternalEnumerator_1_get_Current_m14356_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14355_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14354_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2791_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14353_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14355_MethodInfo,
	&InternalEnumerator_1_Dispose_m14354_MethodInfo,
	&InternalEnumerator_1_get_Current_m14356_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2791_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5779_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2791_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5779_il2cpp_TypeInfo, 7},
};
extern TypeInfo WorldCenterTrackableBehaviour_t126_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2791_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14356_MethodInfo/* Method Usage */,
	&WorldCenterTrackableBehaviour_t126_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisWorldCenterTrackableBehaviour_t126_m31969_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2791_0_0_0;
extern Il2CppType InternalEnumerator_1_t2791_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2791_GenericClass;
TypeInfo InternalEnumerator_1_t2791_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2791_MethodInfos/* methods */
	, InternalEnumerator_1_t2791_PropertyInfos/* properties */
	, InternalEnumerator_1_t2791_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2791_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2791_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2791_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2791_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2791_1_0_0/* this_arg */
	, InternalEnumerator_1_t2791_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2791_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2791_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2791)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7342_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>
extern MethodInfo IList_1_get_Item_m41745_MethodInfo;
extern MethodInfo IList_1_set_Item_m41746_MethodInfo;
static PropertyInfo IList_1_t7342____Item_PropertyInfo = 
{
	&IList_1_t7342_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41745_MethodInfo/* get */
	, &IList_1_set_Item_m41746_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7342_PropertyInfos[] =
{
	&IList_1_t7342____Item_PropertyInfo,
	NULL
};
extern Il2CppType WorldCenterTrackableBehaviour_t126_0_0_0;
static ParameterInfo IList_1_t7342_IList_1_IndexOf_m41747_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldCenterTrackableBehaviour_t126_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41747_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41747_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7342_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7342_IList_1_IndexOf_m41747_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41747_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType WorldCenterTrackableBehaviour_t126_0_0_0;
static ParameterInfo IList_1_t7342_IList_1_Insert_m41748_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WorldCenterTrackableBehaviour_t126_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41748_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41748_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7342_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7342_IList_1_Insert_m41748_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41748_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7342_IList_1_RemoveAt_m41749_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41749_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41749_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7342_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7342_IList_1_RemoveAt_m41749_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41749_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7342_IList_1_get_Item_m41745_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType WorldCenterTrackableBehaviour_t126_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41745_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41745_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7342_il2cpp_TypeInfo/* declaring_type */
	, &WorldCenterTrackableBehaviour_t126_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7342_IList_1_get_Item_m41745_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41745_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType WorldCenterTrackableBehaviour_t126_0_0_0;
static ParameterInfo IList_1_t7342_IList_1_set_Item_m41746_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WorldCenterTrackableBehaviour_t126_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41746_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WorldCenterTrackableBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41746_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7342_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7342_IList_1_set_Item_m41746_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41746_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7342_MethodInfos[] =
{
	&IList_1_IndexOf_m41747_MethodInfo,
	&IList_1_Insert_m41748_MethodInfo,
	&IList_1_RemoveAt_m41749_MethodInfo,
	&IList_1_get_Item_m41745_MethodInfo,
	&IList_1_set_Item_m41746_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7342_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7341_il2cpp_TypeInfo,
	&IEnumerable_1_t7343_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7342_0_0_0;
extern Il2CppType IList_1_t7342_1_0_0;
struct IList_1_t7342;
extern Il2CppGenericClass IList_1_t7342_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7342_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7342_MethodInfos/* methods */
	, IList_1_t7342_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7342_il2cpp_TypeInfo/* element_class */
	, IList_1_t7342_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7342_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7342_0_0_0/* byval_arg */
	, &IList_1_t7342_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7342_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t4050_il2cpp_TypeInfo;

// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>
extern MethodInfo ICollection_1_get_Count_m36179_MethodInfo;
static PropertyInfo ICollection_1_t4050____Count_PropertyInfo = 
{
	&ICollection_1_t4050_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m36179_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41750_MethodInfo;
static PropertyInfo ICollection_1_t4050____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t4050_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41750_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t4050_PropertyInfos[] =
{
	&ICollection_1_t4050____Count_PropertyInfo,
	&ICollection_1_t4050____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m36179_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m36179_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t4050_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m36179_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41750_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41750_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t4050_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41750_GenericMethod/* genericMethod */

};
extern Il2CppType TrackableBehaviour_t44_0_0_0;
extern Il2CppType TrackableBehaviour_t44_0_0_0;
static ParameterInfo ICollection_1_t4050_ICollection_1_Add_m41751_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TrackableBehaviour_t44_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41751_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41751_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t4050_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t4050_ICollection_1_Add_m41751_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41751_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41752_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41752_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t4050_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41752_GenericMethod/* genericMethod */

};
extern Il2CppType TrackableBehaviour_t44_0_0_0;
static ParameterInfo ICollection_1_t4050_ICollection_1_Contains_m35953_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TrackableBehaviour_t44_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m35953_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m35953_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t4050_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t4050_ICollection_1_Contains_m35953_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m35953_GenericMethod/* genericMethod */

};
extern Il2CppType TrackableBehaviourU5BU5D_t813_0_0_0;
extern Il2CppType TrackableBehaviourU5BU5D_t813_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t4050_ICollection_1_CopyTo_m36180_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TrackableBehaviourU5BU5D_t813_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m36180_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m36180_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t4050_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t4050_ICollection_1_CopyTo_m36180_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m36180_GenericMethod/* genericMethod */

};
extern Il2CppType TrackableBehaviour_t44_0_0_0;
static ParameterInfo ICollection_1_t4050_ICollection_1_Remove_m41753_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TrackableBehaviour_t44_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41753_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TrackableBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41753_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t4050_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t4050_ICollection_1_Remove_m41753_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41753_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t4050_MethodInfos[] =
{
	&ICollection_1_get_Count_m36179_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41750_MethodInfo,
	&ICollection_1_Add_m41751_MethodInfo,
	&ICollection_1_Clear_m41752_MethodInfo,
	&ICollection_1_Contains_m35953_MethodInfo,
	&ICollection_1_CopyTo_m36180_MethodInfo,
	&ICollection_1_Remove_m41753_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t725_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t4050_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t725_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t4050_0_0_0;
extern Il2CppType ICollection_1_t4050_1_0_0;
struct ICollection_1_t4050;
extern Il2CppGenericClass ICollection_1_t4050_GenericClass;
TypeInfo ICollection_1_t4050_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t4050_MethodInfos/* methods */
	, ICollection_1_t4050_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t4050_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t4050_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t4050_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t4050_0_0_0/* byval_arg */
	, &ICollection_1_t4050_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t4050_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>
extern Il2CppType IEnumerator_1_t892_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m5363_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m5363_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t725_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t892_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m5363_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t725_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m5363_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t725_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t725_0_0_0;
extern Il2CppType IEnumerable_1_t725_1_0_0;
struct IEnumerable_1_t725;
extern Il2CppGenericClass IEnumerable_1_t725_GenericClass;
TypeInfo IEnumerable_1_t725_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t725_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t725_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t725_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t725_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t725_0_0_0/* byval_arg */
	, &IEnumerable_1_t725_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t725_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t892_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.TrackableBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.TrackableBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m5364_MethodInfo;
static PropertyInfo IEnumerator_1_t892____Current_PropertyInfo = 
{
	&IEnumerator_1_t892_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m5364_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t892_PropertyInfos[] =
{
	&IEnumerator_1_t892____Current_PropertyInfo,
	NULL
};
extern Il2CppType TrackableBehaviour_t44_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m5364_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.TrackableBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m5364_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t892_il2cpp_TypeInfo/* declaring_type */
	, &TrackableBehaviour_t44_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m5364_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t892_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m5364_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t892_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t892_0_0_0;
extern Il2CppType IEnumerator_1_t892_1_0_0;
struct IEnumerator_1_t892;
extern Il2CppGenericClass IEnumerator_1_t892_GenericClass;
TypeInfo IEnumerator_1_t892_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t892_MethodInfos/* methods */
	, IEnumerator_1_t892_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t892_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t892_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t892_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t892_0_0_0/* byval_arg */
	, &IEnumerator_1_t892_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t892_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_47.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2792_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_47MethodDeclarations.h"

extern TypeInfo TrackableBehaviour_t44_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14361_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTrackableBehaviour_t44_m31980_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.TrackableBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.TrackableBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisTrackableBehaviour_t44_m31980(__this, p0, method) (TrackableBehaviour_t44 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2792____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2792, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2792____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2792, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2792_FieldInfos[] =
{
	&InternalEnumerator_1_t2792____array_0_FieldInfo,
	&InternalEnumerator_1_t2792____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14358_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2792____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2792_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14358_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2792____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2792_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14361_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2792_PropertyInfos[] =
{
	&InternalEnumerator_1_t2792____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2792____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2792_InternalEnumerator_1__ctor_m14357_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14357_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14357_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2792_InternalEnumerator_1__ctor_m14357_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14357_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14358_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14358_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14358_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14359_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14359_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14359_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14360_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14360_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14360_GenericMethod/* genericMethod */

};
extern Il2CppType TrackableBehaviour_t44_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14361_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.TrackableBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14361_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* declaring_type */
	, &TrackableBehaviour_t44_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14361_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2792_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14357_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14358_MethodInfo,
	&InternalEnumerator_1_Dispose_m14359_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14360_MethodInfo,
	&InternalEnumerator_1_get_Current_m14361_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14360_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14359_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2792_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14358_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14360_MethodInfo,
	&InternalEnumerator_1_Dispose_m14359_MethodInfo,
	&InternalEnumerator_1_get_Current_m14361_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2792_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t892_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2792_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t892_il2cpp_TypeInfo, 7},
};
extern TypeInfo TrackableBehaviour_t44_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2792_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14361_MethodInfo/* Method Usage */,
	&TrackableBehaviour_t44_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTrackableBehaviour_t44_m31980_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2792_0_0_0;
extern Il2CppType InternalEnumerator_1_t2792_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2792_GenericClass;
TypeInfo InternalEnumerator_1_t2792_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2792_MethodInfos/* methods */
	, InternalEnumerator_1_t2792_PropertyInfos/* properties */
	, InternalEnumerator_1_t2792_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2792_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2792_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2792_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2792_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2792_1_0_0/* this_arg */
	, InternalEnumerator_1_t2792_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2792_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2792_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2792)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t4055_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>
extern MethodInfo IList_1_get_Item_m41754_MethodInfo;
extern MethodInfo IList_1_set_Item_m41755_MethodInfo;
static PropertyInfo IList_1_t4055____Item_PropertyInfo = 
{
	&IList_1_t4055_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41754_MethodInfo/* get */
	, &IList_1_set_Item_m41755_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t4055_PropertyInfos[] =
{
	&IList_1_t4055____Item_PropertyInfo,
	NULL
};
extern Il2CppType TrackableBehaviour_t44_0_0_0;
static ParameterInfo IList_1_t4055_IList_1_IndexOf_m41756_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TrackableBehaviour_t44_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41756_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41756_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t4055_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t4055_IList_1_IndexOf_m41756_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41756_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TrackableBehaviour_t44_0_0_0;
static ParameterInfo IList_1_t4055_IList_1_Insert_m41757_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TrackableBehaviour_t44_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41757_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41757_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t4055_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t4055_IList_1_Insert_m41757_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41757_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t4055_IList_1_RemoveAt_m41758_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41758_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41758_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t4055_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t4055_IList_1_RemoveAt_m41758_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41758_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t4055_IList_1_get_Item_m41754_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType TrackableBehaviour_t44_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41754_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41754_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t4055_il2cpp_TypeInfo/* declaring_type */
	, &TrackableBehaviour_t44_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t4055_IList_1_get_Item_m41754_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41754_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TrackableBehaviour_t44_0_0_0;
static ParameterInfo IList_1_t4055_IList_1_set_Item_m41755_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TrackableBehaviour_t44_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41755_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41755_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t4055_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t4055_IList_1_set_Item_m41755_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41755_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t4055_MethodInfos[] =
{
	&IList_1_IndexOf_m41756_MethodInfo,
	&IList_1_Insert_m41757_MethodInfo,
	&IList_1_RemoveAt_m41758_MethodInfo,
	&IList_1_get_Item_m41754_MethodInfo,
	&IList_1_set_Item_m41755_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t4055_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t4050_il2cpp_TypeInfo,
	&IEnumerable_1_t725_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t4055_0_0_0;
extern Il2CppType IList_1_t4055_1_0_0;
struct IList_1_t4055;
extern Il2CppGenericClass IList_1_t4055_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t4055_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t4055_MethodInfos/* methods */
	, IList_1_t4055_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t4055_il2cpp_TypeInfo/* element_class */
	, IList_1_t4055_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t4055_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t4055_0_0_0/* byval_arg */
	, &IList_1_t4055_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t4055_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
