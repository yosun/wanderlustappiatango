﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>
struct KeyValuePair_2_t4563;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m28037 (KeyValuePair_2_t4563 * __this, String_t* ___key, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Key()
 String_t* KeyValuePair_2_get_Key_m28038 (KeyValuePair_2_t4563 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m28039 (KeyValuePair_2_t4563 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Value()
 int32_t KeyValuePair_2_get_Value_m28040 (KeyValuePair_2_t4563 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m28041 (KeyValuePair_2_t4563 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::ToString()
 String_t* KeyValuePair_2_ToString_m28042 (KeyValuePair_2_t4563 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
