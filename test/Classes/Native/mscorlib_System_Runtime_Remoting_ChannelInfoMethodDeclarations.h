﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ChannelInfo
struct ChannelInfo_t1991;
// System.Object[]
struct ObjectU5BU5D_t115;

// System.Void System.Runtime.Remoting.ChannelInfo::.ctor()
 void ChannelInfo__ctor_m11451 (ChannelInfo_t1991 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System.Runtime.Remoting.ChannelInfo::get_ChannelData()
 ObjectU5BU5D_t115* ChannelInfo_get_ChannelData_m11452 (ChannelInfo_t1991 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
