﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<SetRenderQueue>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_5.h"
// UnityEngine.Events.CachedInvokableCall`1<SetRenderQueue>
struct CachedInvokableCall_1_t2743  : public InvokableCall_1_t2744
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<SetRenderQueue>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
