﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.AssemblyBuilder
struct AssemblyBuilder_t1897;
// System.String
struct String_t;
// System.Reflection.Module[]
struct ModuleU5BU5D_t1898;
// System.Exception
struct Exception_t152;
// System.Reflection.AssemblyName
struct AssemblyName_t1899;

// System.String System.Reflection.Emit.AssemblyBuilder::get_Location()
 String_t* AssemblyBuilder_get_Location_m10936 (AssemblyBuilder_t1897 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Module[] System.Reflection.Emit.AssemblyBuilder::GetModulesInternal()
 ModuleU5BU5D_t1898* AssemblyBuilder_GetModulesInternal_m10937 (AssemblyBuilder_t1897 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Reflection.Emit.AssemblyBuilder::get_IsCompilerContext()
 bool AssemblyBuilder_get_IsCompilerContext_m10938 (AssemblyBuilder_t1897 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Reflection.Emit.AssemblyBuilder::not_supported()
 Exception_t152 * AssemblyBuilder_not_supported_m10939 (AssemblyBuilder_t1897 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.AssemblyName System.Reflection.Emit.AssemblyBuilder::UnprotectedGetName()
 AssemblyName_t1899 * AssemblyBuilder_UnprotectedGetName_m10940 (AssemblyBuilder_t1897 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
