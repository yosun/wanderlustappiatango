﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackerBehaviour
struct SmartTerrainTrackerBehaviour_t75;

// System.Void Vuforia.SmartTerrainTrackerBehaviour::.ctor()
 void SmartTerrainTrackerBehaviour__ctor_m169 (SmartTerrainTrackerBehaviour_t75 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
