﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<AnimateTexture>
struct UnityAction_1_t2718;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<AnimateTexture>
struct InvokableCall_1_t2717  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<AnimateTexture>::Delegate
	UnityAction_1_t2718 * ___Delegate_0;
};
