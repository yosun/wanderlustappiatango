﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t1308;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.IntervalCollection
struct IntervalCollection_t1438  : public Object_t
{
	// System.Collections.ArrayList System.Text.RegularExpressions.IntervalCollection::intervals
	ArrayList_t1308 * ___intervals_0;
};
