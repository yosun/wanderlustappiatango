﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3035;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t455;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
 void Stack_1__ctor_m15717_gshared (Stack_1_t3035 * __this, MethodInfo* method);
#define Stack_1__ctor_m15717(__this, method) (void)Stack_1__ctor_m15717_gshared((Stack_1_t3035 *)__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
 bool Stack_1_System_Collections_ICollection_get_IsSynchronized_m15718_gshared (Stack_1_t3035 * __this, MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m15718(__this, method) (bool)Stack_1_System_Collections_ICollection_get_IsSynchronized_m15718_gshared((Stack_1_t3035 *)__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
 Object_t * Stack_1_System_Collections_ICollection_get_SyncRoot_m15719_gshared (Stack_1_t3035 * __this, MethodInfo* method);
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m15719(__this, method) (Object_t *)Stack_1_System_Collections_ICollection_get_SyncRoot_m15719_gshared((Stack_1_t3035 *)__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void Stack_1_System_Collections_ICollection_CopyTo_m15720_gshared (Stack_1_t3035 * __this, Array_t * ___dest, int32_t ___idx, MethodInfo* method);
#define Stack_1_System_Collections_ICollection_CopyTo_m15720(__this, ___dest, ___idx, method) (void)Stack_1_System_Collections_ICollection_CopyTo_m15720_gshared((Stack_1_t3035 *)__this, (Array_t *)___dest, (int32_t)___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
 Object_t* Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15721_gshared (Stack_1_t3035 * __this, MethodInfo* method);
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15721(__this, method) (Object_t*)Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15721_gshared((Stack_1_t3035 *)__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * Stack_1_System_Collections_IEnumerable_GetEnumerator_m15722_gshared (Stack_1_t3035 * __this, MethodInfo* method);
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m15722(__this, method) (Object_t *)Stack_1_System_Collections_IEnumerable_GetEnumerator_m15722_gshared((Stack_1_t3035 *)__this, method)
// T System.Collections.Generic.Stack`1<System.Object>::Peek()
 Object_t * Stack_1_Peek_m15723_gshared (Stack_1_t3035 * __this, MethodInfo* method);
#define Stack_1_Peek_m15723(__this, method) (Object_t *)Stack_1_Peek_m15723_gshared((Stack_1_t3035 *)__this, method)
// T System.Collections.Generic.Stack`1<System.Object>::Pop()
 Object_t * Stack_1_Pop_m15724_gshared (Stack_1_t3035 * __this, MethodInfo* method);
#define Stack_1_Pop_m15724(__this, method) (Object_t *)Stack_1_Pop_m15724_gshared((Stack_1_t3035 *)__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(T)
 void Stack_1_Push_m15725_gshared (Stack_1_t3035 * __this, Object_t * ___t, MethodInfo* method);
#define Stack_1_Push_m15725(__this, ___t, method) (void)Stack_1_Push_m15725_gshared((Stack_1_t3035 *)__this, (Object_t *)___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
 int32_t Stack_1_get_Count_m15726_gshared (Stack_1_t3035 * __this, MethodInfo* method);
#define Stack_1_get_Count_m15726(__this, method) (int32_t)Stack_1_get_Count_m15726_gshared((Stack_1_t3035 *)__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Object>::GetEnumerator()
 Enumerator_t3036  Stack_1_GetEnumerator_m15727 (Stack_1_t3035 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
