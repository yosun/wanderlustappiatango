﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture
struct Texture_t294;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.UserAuthorizationDialog
struct UserAuthorizationDialog_t1093  : public MonoBehaviour_t6
{
	// UnityEngine.Rect UnityEngine.UserAuthorizationDialog::windowRect
	Rect_t118  ___windowRect_4;
	// UnityEngine.Texture UnityEngine.UserAuthorizationDialog::warningIcon
	Texture_t294 * ___warningIcon_5;
};
