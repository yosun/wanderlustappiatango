﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.ImageTarget>
struct EqualityComparer_1_t4248;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.ImageTarget>
struct EqualityComparer_1_t4248  : public Object_t
{
};
struct EqualityComparer_1_t4248_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.ImageTarget>::_default
	EqualityComparer_1_t4248 * ____default_0;
};
