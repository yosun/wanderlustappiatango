﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t21;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// SetRenderQueue
struct SetRenderQueue_t22  : public MonoBehaviour_t6
{
	// System.Int32[] SetRenderQueue::m_queues
	Int32U5BU5D_t21* ___m_queues_2;
};
