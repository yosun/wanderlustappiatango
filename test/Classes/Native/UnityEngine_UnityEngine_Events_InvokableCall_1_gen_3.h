﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Input2>
struct UnityAction_1_t2729;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Input2>
struct InvokableCall_1_t2728  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Input2>::Delegate
	UnityAction_1_t2729 * ___Delegate_0;
};
