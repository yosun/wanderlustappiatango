﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_t187;

// System.Void UnityEngine.EventSystems.UIBehaviour::.ctor()
 void UIBehaviour__ctor_m773 (UIBehaviour_t187 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::Awake()
 void UIBehaviour_Awake_m774 (UIBehaviour_t187 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnEnable()
 void UIBehaviour_OnEnable_m775 (UIBehaviour_t187 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::Start()
 void UIBehaviour_Start_m776 (UIBehaviour_t187 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDisable()
 void UIBehaviour_OnDisable_m777 (UIBehaviour_t187 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDestroy()
 void UIBehaviour_OnDestroy_m778 (UIBehaviour_t187 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive()
 bool UIBehaviour_IsActive_m779 (UIBehaviour_t187 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnRectTransformDimensionsChange()
 void UIBehaviour_OnRectTransformDimensionsChange_m780 (UIBehaviour_t187 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnBeforeTransformParentChanged()
 void UIBehaviour_OnBeforeTransformParentChanged_m781 (UIBehaviour_t187 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnTransformParentChanged()
 void UIBehaviour_OnTransformParentChanged_m782 (UIBehaviour_t187 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDidApplyAnimationProperties()
 void UIBehaviour_OnDidApplyAnimationProperties_m783 (UIBehaviour_t187 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnCanvasGroupChanged()
 void UIBehaviour_OnCanvasGroupChanged_m784 (UIBehaviour_t187 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.UIBehaviour::IsDestroyed()
 bool UIBehaviour_IsDestroyed_m785 (UIBehaviour_t187 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
