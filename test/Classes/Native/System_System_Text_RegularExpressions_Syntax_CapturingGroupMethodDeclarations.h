﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.Syntax.CapturingGroup
struct CapturingGroup_t1449;
// System.String
struct String_t;
// System.Text.RegularExpressions.ICompiler
struct ICompiler_t1446;
// System.Object
struct Object_t;

// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::.ctor()
 void CapturingGroup__ctor_m7432 (CapturingGroup_t1449 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.RegularExpressions.Syntax.CapturingGroup::get_Index()
 int32_t CapturingGroup_get_Index_m7433 (CapturingGroup_t1449 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::set_Index(System.Int32)
 void CapturingGroup_set_Index_m7434 (CapturingGroup_t1449 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.RegularExpressions.Syntax.CapturingGroup::get_Name()
 String_t* CapturingGroup_get_Name_m7435 (CapturingGroup_t1449 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::set_Name(System.String)
 void CapturingGroup_set_Name_m7436 (CapturingGroup_t1449 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.Syntax.CapturingGroup::get_IsNamed()
 bool CapturingGroup_get_IsNamed_m7437 (CapturingGroup_t1449 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.CapturingGroup::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
 void CapturingGroup_Compile_m7438 (CapturingGroup_t1449 * __this, Object_t * ___cmp, bool ___reverse, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.Syntax.CapturingGroup::IsComplex()
 bool CapturingGroup_IsComplex_m7439 (CapturingGroup_t1449 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.RegularExpressions.Syntax.CapturingGroup::CompareTo(System.Object)
 int32_t CapturingGroup_CompareTo_m7440 (CapturingGroup_t1449 * __this, Object_t * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
