﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>>
struct InternalEnumerator_1_t3835;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m21686 (InternalEnumerator_1_t3835 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21687 (InternalEnumerator_1_t3835 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>>::Dispose()
 void InternalEnumerator_1_Dispose_m21688 (InternalEnumerator_1_t3835 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m21689 (InternalEnumerator_1_t3835 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>>::get_Current()
 KeyValuePair_2_t3832  InternalEnumerator_1_get_Current_m21690 (InternalEnumerator_1_t3835 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
