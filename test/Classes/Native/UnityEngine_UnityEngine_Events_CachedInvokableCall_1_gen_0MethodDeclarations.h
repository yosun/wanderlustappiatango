﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct CachedInvokableCall_1_t1189;
// UnityEngine.Object
struct Object_t117;
struct Object_t117_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t142;
// System.Object[]
struct ObjectU5BU5D_t115;

// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
 void CachedInvokableCall_1__ctor_m6557 (CachedInvokableCall_1_t1189 * __this, Object_t117 * ___target, MethodInfo_t142 * ___theFunction, int32_t ___argument, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::Invoke(System.Object[])
 void CachedInvokableCall_1_Invoke_m29348 (CachedInvokableCall_1_t1189 * __this, ObjectU5BU5D_t115* ___args, MethodInfo* method) IL2CPP_METHOD_ATTR;
