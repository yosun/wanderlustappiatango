﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$136
struct $ArrayType$136_t1227;
struct $ArrayType$136_t1227_marshaled;

void $ArrayType$136_t1227_marshal(const $ArrayType$136_t1227& unmarshaled, $ArrayType$136_t1227_marshaled& marshaled);
void $ArrayType$136_t1227_marshal_back(const $ArrayType$136_t1227_marshaled& marshaled, $ArrayType$136_t1227& unmarshaled);
void $ArrayType$136_t1227_marshal_cleanup($ArrayType$136_t1227_marshaled& marshaled);
