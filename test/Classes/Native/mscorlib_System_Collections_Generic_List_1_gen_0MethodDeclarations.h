﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Object>
struct List_1_t150;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2847;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t455;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t2848;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t2849;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.Predicate`1<System.Object>
struct Predicate_1_t2845;
// System.Comparison`1<System.Object>
struct Comparison_1_t2846;
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25.h"

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
 void List_1__ctor_m14543_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1__ctor_m14543(__this, method) (void)List_1__ctor_m14543_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
 void List_1__ctor_m14545_gshared (List_1_t150 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1__ctor_m14545(__this, ___collection, method) (void)List_1__ctor_m14545_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
 void List_1__ctor_m14547_gshared (List_1_t150 * __this, int32_t ___capacity, MethodInfo* method);
#define List_1__ctor_m14547(__this, ___capacity, method) (void)List_1__ctor_m14547_gshared((List_1_t150 *)__this, (int32_t)___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
 void List_1__cctor_m14549_gshared (Object_t * __this/* static, unused */, MethodInfo* method);
#define List_1__cctor_m14549(__this/* static, unused */, method) (void)List_1__cctor_m14549_gshared((Object_t *)__this/* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
 Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14551_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14551(__this, method) (Object_t*)List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14551_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void List_1_System_Collections_ICollection_CopyTo_m14553_gshared (List_1_t150 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m14553(__this, ___array, ___arrayIndex, method) (void)List_1_System_Collections_ICollection_CopyTo_m14553_gshared((List_1_t150 *)__this, (Array_t *)___array, (int32_t)___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m14555_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m14555(__this, method) (Object_t *)List_1_System_Collections_IEnumerable_GetEnumerator_m14555_gshared((List_1_t150 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
 int32_t List_1_System_Collections_IList_Add_m14557_gshared (List_1_t150 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Add_m14557(__this, ___item, method) (int32_t)List_1_System_Collections_IList_Add_m14557_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
 bool List_1_System_Collections_IList_Contains_m14559_gshared (List_1_t150 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m14559(__this, ___item, method) (bool)List_1_System_Collections_IList_Contains_m14559_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
 int32_t List_1_System_Collections_IList_IndexOf_m14561_gshared (List_1_t150 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m14561(__this, ___item, method) (int32_t)List_1_System_Collections_IList_IndexOf_m14561_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
 void List_1_System_Collections_IList_Insert_m14563_gshared (List_1_t150 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m14563(__this, ___index, ___item, method) (void)List_1_System_Collections_IList_Insert_m14563_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
 void List_1_System_Collections_IList_Remove_m14565_gshared (List_1_t150 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m14565(__this, ___item, method) (void)List_1_System_Collections_IList_Remove_m14565_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
 bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14567_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14567(__this, method) (bool)List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14567_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
 bool List_1_System_Collections_ICollection_get_IsSynchronized_m14569_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m14569(__this, method) (bool)List_1_System_Collections_ICollection_get_IsSynchronized_m14569_gshared((List_1_t150 *)__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
 Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m14571_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m14571(__this, method) (Object_t *)List_1_System_Collections_ICollection_get_SyncRoot_m14571_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
 bool List_1_System_Collections_IList_get_IsFixedSize_m14573_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m14573(__this, method) (bool)List_1_System_Collections_IList_get_IsFixedSize_m14573_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsReadOnly()
 bool List_1_System_Collections_IList_get_IsReadOnly_m14575_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m14575(__this, method) (bool)List_1_System_Collections_IList_get_IsReadOnly_m14575_gshared((List_1_t150 *)__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
 Object_t * List_1_System_Collections_IList_get_Item_m14577_gshared (List_1_t150 * __this, int32_t ___index, MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m14577(__this, ___index, method) (Object_t *)List_1_System_Collections_IList_get_Item_m14577_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
 void List_1_System_Collections_IList_set_Item_m14579_gshared (List_1_t150 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m14579(__this, ___index, ___value, method) (void)List_1_System_Collections_IList_set_Item_m14579_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
 void List_1_Add_m14581_gshared (List_1_t150 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_Add_m14581(__this, ___item, method) (void)List_1_Add_m14581_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
 void List_1_GrowIfNeeded_m14583_gshared (List_1_t150 * __this, int32_t ___newCount, MethodInfo* method);
#define List_1_GrowIfNeeded_m14583(__this, ___newCount, method) (void)List_1_GrowIfNeeded_m14583_gshared((List_1_t150 *)__this, (int32_t)___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
 void List_1_AddCollection_m14585_gshared (List_1_t150 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddCollection_m14585(__this, ___collection, method) (void)List_1_AddCollection_m14585_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
 void List_1_AddEnumerable_m14587_gshared (List_1_t150 * __this, Object_t* ___enumerable, MethodInfo* method);
#define List_1_AddEnumerable_m14587(__this, ___enumerable, method) (void)List_1_AddEnumerable_m14587_gshared((List_1_t150 *)__this, (Object_t*)___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
 void List_1_AddRange_m14588_gshared (List_1_t150 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_AddRange_m14588(__this, ___collection, method) (void)List_1_AddRange_m14588_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
 ReadOnlyCollection_1_t2849 * List_1_AsReadOnly_m14590_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_AsReadOnly_m14590(__this, method) (ReadOnlyCollection_1_t2849 *)List_1_AsReadOnly_m14590_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
 void List_1_Clear_m14592_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_Clear_m14592(__this, method) (void)List_1_Clear_m14592_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
 bool List_1_Contains_m14594_gshared (List_1_t150 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_Contains_m14594(__this, ___item, method) (bool)List_1_Contains_m14594_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
 void List_1_CopyTo_m14596_gshared (List_1_t150 * __this, ObjectU5BU5D_t115* ___array, int32_t ___arrayIndex, MethodInfo* method);
#define List_1_CopyTo_m14596(__this, ___array, ___arrayIndex, method) (void)List_1_CopyTo_m14596_gshared((List_1_t150 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<T>)
 Object_t * List_1_Find_m14598_gshared (List_1_t150 * __this, Predicate_1_t2845 * ___match, MethodInfo* method);
#define List_1_Find_m14598(__this, ___match, method) (Object_t *)List_1_Find_m14598_gshared((List_1_t150 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckMatch(System.Predicate`1<T>)
 void List_1_CheckMatch_m14600_gshared (Object_t * __this/* static, unused */, Predicate_1_t2845 * ___match, MethodInfo* method);
#define List_1_CheckMatch_m14600(__this/* static, unused */, ___match, method) (void)List_1_CheckMatch_m14600_gshared((Object_t *)__this/* static, unused */, (Predicate_1_t2845 *)___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
 int32_t List_1_GetIndex_m14602_gshared (List_1_t150 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2845 * ___match, MethodInfo* method);
#define List_1_GetIndex_m14602(__this, ___startIndex, ___count, ___match, method) (int32_t)List_1_GetIndex_m14602_gshared((List_1_t150 *)__this, (int32_t)___startIndex, (int32_t)___count, (Predicate_1_t2845 *)___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
 Enumerator_t2850  List_1_GetEnumerator_m14639 (List_1_t150 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
 int32_t List_1_IndexOf_m14604_gshared (List_1_t150 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_IndexOf_m14604(__this, ___item, method) (int32_t)List_1_IndexOf_m14604_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
 void List_1_Shift_m14606_gshared (List_1_t150 * __this, int32_t ___start, int32_t ___delta, MethodInfo* method);
#define List_1_Shift_m14606(__this, ___start, ___delta, method) (void)List_1_Shift_m14606_gshared((List_1_t150 *)__this, (int32_t)___start, (int32_t)___delta, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
 void List_1_CheckIndex_m14608_gshared (List_1_t150 * __this, int32_t ___index, MethodInfo* method);
#define List_1_CheckIndex_m14608(__this, ___index, method) (void)List_1_CheckIndex_m14608_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
 void List_1_Insert_m14610_gshared (List_1_t150 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define List_1_Insert_m14610(__this, ___index, ___item, method) (void)List_1_Insert_m14610_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
 void List_1_CheckCollection_m14612_gshared (List_1_t150 * __this, Object_t* ___collection, MethodInfo* method);
#define List_1_CheckCollection_m14612(__this, ___collection, method) (void)List_1_CheckCollection_m14612_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
 bool List_1_Remove_m14614_gshared (List_1_t150 * __this, Object_t * ___item, MethodInfo* method);
#define List_1_Remove_m14614(__this, ___item, method) (bool)List_1_Remove_m14614_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<T>)
 int32_t List_1_RemoveAll_m14616_gshared (List_1_t150 * __this, Predicate_1_t2845 * ___match, MethodInfo* method);
#define List_1_RemoveAll_m14616(__this, ___match, method) (int32_t)List_1_RemoveAll_m14616_gshared((List_1_t150 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
 void List_1_RemoveAt_m14618_gshared (List_1_t150 * __this, int32_t ___index, MethodInfo* method);
#define List_1_RemoveAt_m14618(__this, ___index, method) (void)List_1_RemoveAt_m14618_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Reverse()
 void List_1_Reverse_m14620_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_Reverse_m14620(__this, method) (void)List_1_Reverse_m14620_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort()
 void List_1_Sort_m14622_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_Sort_m14622(__this, method) (void)List_1_Sort_m14622_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<T>)
 void List_1_Sort_m14624_gshared (List_1_t150 * __this, Comparison_1_t2846 * ___comparison, MethodInfo* method);
#define List_1_Sort_m14624(__this, ___comparison, method) (void)List_1_Sort_m14624_gshared((List_1_t150 *)__this, (Comparison_1_t2846 *)___comparison, method)
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
 ObjectU5BU5D_t115* List_1_ToArray_m14626_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_ToArray_m14626(__this, method) (ObjectU5BU5D_t115*)List_1_ToArray_m14626_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::TrimExcess()
 void List_1_TrimExcess_m14628_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_TrimExcess_m14628(__this, method) (void)List_1_TrimExcess_m14628_gshared((List_1_t150 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
 int32_t List_1_get_Capacity_m14630_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_get_Capacity_m14630(__this, method) (int32_t)List_1_get_Capacity_m14630_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
 void List_1_set_Capacity_m14632_gshared (List_1_t150 * __this, int32_t ___value, MethodInfo* method);
#define List_1_set_Capacity_m14632(__this, ___value, method) (void)List_1_set_Capacity_m14632_gshared((List_1_t150 *)__this, (int32_t)___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
 int32_t List_1_get_Count_m14634_gshared (List_1_t150 * __this, MethodInfo* method);
#define List_1_get_Count_m14634(__this, method) (int32_t)List_1_get_Count_m14634_gshared((List_1_t150 *)__this, method)
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
 Object_t * List_1_get_Item_m14636_gshared (List_1_t150 * __this, int32_t ___index, MethodInfo* method);
#define List_1_get_Item_m14636(__this, ___index, method) (Object_t *)List_1_get_Item_m14636_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
 void List_1_set_Item_m14638_gshared (List_1_t150 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define List_1_set_Item_m14638(__this, ___index, ___value, method) (void)List_1_set_Item_m14638_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___value, method)
