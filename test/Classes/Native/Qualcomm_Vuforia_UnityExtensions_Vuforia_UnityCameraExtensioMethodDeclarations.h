﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.UnityCameraExtensions
struct UnityCameraExtensions_t549;
// UnityEngine.Camera
struct Camera_t3;

// System.Int32 Vuforia.UnityCameraExtensions::GetPixelHeightInt(UnityEngine.Camera)
 int32_t UnityCameraExtensions_GetPixelHeightInt_m2628 (Object_t * __this/* static, unused */, Camera_t3 * ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.UnityCameraExtensions::GetPixelWidthInt(UnityEngine.Camera)
 int32_t UnityCameraExtensions_GetPixelWidthInt_m2629 (Object_t * __this/* static, unused */, Camera_t3 * ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.UnityCameraExtensions::GetMaxDepthForVideoBackground(UnityEngine.Camera)
 float UnityCameraExtensions_GetMaxDepthForVideoBackground_m2630 (Object_t * __this/* static, unused */, Camera_t3 * ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.UnityCameraExtensions::GetMinDepthForVideoBackground(UnityEngine.Camera)
 float UnityCameraExtensions_GetMinDepthForVideoBackground_m2631 (Object_t * __this/* static, unused */, Camera_t3 * ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
