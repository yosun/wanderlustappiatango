﻿#pragma once
#include <stdint.h>
// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t718;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.ISmartTerrainEventHandler>
struct Comparison_1_t4070  : public MulticastDelegate_t325
{
};
