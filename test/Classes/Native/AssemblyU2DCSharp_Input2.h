﻿#pragma once
#include <stdint.h>
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Input2
struct Input2_t14  : public MonoBehaviour_t6
{
};
struct Input2_t14_StaticFields{
	// UnityEngine.Vector2 Input2::offScreen
	Vector2_t9  ___offScreen_2;
	// System.Single Input2::oldAngle
	float ___oldAngle_3;
	// UnityEngine.Vector2 Input2::lastPos
	Vector2_t9  ___lastPos_4;
};
