﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$20
struct $ArrayType$20_t2266;
struct $ArrayType$20_t2266_marshaled;

void $ArrayType$20_t2266_marshal(const $ArrayType$20_t2266& unmarshaled, $ArrayType$20_t2266_marshaled& marshaled);
void $ArrayType$20_t2266_marshal_back(const $ArrayType$20_t2266_marshaled& marshaled, $ArrayType$20_t2266& unmarshaled);
void $ArrayType$20_t2266_marshal_cleanup($ArrayType$20_t2266_marshaled& marshaled);
