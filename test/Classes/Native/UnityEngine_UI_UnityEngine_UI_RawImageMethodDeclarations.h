﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.RawImage
struct RawImage_t338;
// UnityEngine.Texture
struct Texture_t294;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t295;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void UnityEngine.UI.RawImage::.ctor()
 void RawImage__ctor_m1325 (RawImage_t338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.UI.RawImage::get_mainTexture()
 Texture_t294 * RawImage_get_mainTexture_m1326 (RawImage_t338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.UI.RawImage::get_texture()
 Texture_t294 * RawImage_get_texture_m1327 (RawImage_t338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.RawImage::set_texture(UnityEngine.Texture)
 void RawImage_set_texture_m1328 (RawImage_t338 * __this, Texture_t294 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.UI.RawImage::get_uvRect()
 Rect_t118  RawImage_get_uvRect_m1329 (RawImage_t338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.RawImage::set_uvRect(UnityEngine.Rect)
 void RawImage_set_uvRect_m1330 (RawImage_t338 * __this, Rect_t118  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.RawImage::SetNativeSize()
 void RawImage_SetNativeSize_m1331 (RawImage_t338 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.RawImage::OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
 void RawImage_OnFillVBO_m1332 (RawImage_t338 * __this, List_1_t295 * ___vbo, MethodInfo* method) IL2CPP_METHOD_ATTR;
