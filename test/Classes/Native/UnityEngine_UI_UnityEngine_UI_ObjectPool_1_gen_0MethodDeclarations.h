﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct ObjectPool_1_t286;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct UnityAction_1_t292;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t295;

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// UnityEngine.UI.ObjectPool`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"
#define ObjectPool_1__ctor_m2115(__this, ___actionOnGet, ___actionOnRelease, method) (void)ObjectPool_1__ctor_m15704_gshared((ObjectPool_1_t3034 *)__this, (UnityAction_1_t2707 *)___actionOnGet, (UnityAction_1_t2707 *)___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countAll()
#define ObjectPool_1_get_countAll_m18005(__this, method) (int32_t)ObjectPool_1_get_countAll_m15706_gshared((ObjectPool_1_t3034 *)__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m18006(__this, ___value, method) (void)ObjectPool_1_set_countAll_m15708_gshared((ObjectPool_1_t3034 *)__this, (int32_t)___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countActive()
#define ObjectPool_1_get_countActive_m18007(__this, method) (int32_t)ObjectPool_1_get_countActive_m15710_gshared((ObjectPool_1_t3034 *)__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m18008(__this, method) (int32_t)ObjectPool_1_get_countInactive_m15712_gshared((ObjectPool_1_t3034 *)__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Get()
#define ObjectPool_1_Get_m2123(__this, method) (List_1_t295 *)ObjectPool_1_Get_m15714_gshared((ObjectPool_1_t3034 *)__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Release(T)
#define ObjectPool_1_Release_m2129(__this, ___element, method) (void)ObjectPool_1_Release_m15716_gshared((ObjectPool_1_t3034 *)__this, (Object_t *)___element, method)
