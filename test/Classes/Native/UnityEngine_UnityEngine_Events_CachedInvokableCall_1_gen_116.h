﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.MeshFilter>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_118.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MeshFilter>
struct CachedInvokableCall_1_t4465  : public InvokableCall_1_t4466
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MeshFilter>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
