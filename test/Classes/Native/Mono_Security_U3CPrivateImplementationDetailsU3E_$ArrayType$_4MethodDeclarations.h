﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$64
struct $ArrayType$64_t1652;
struct $ArrayType$64_t1652_marshaled;

void $ArrayType$64_t1652_marshal(const $ArrayType$64_t1652& unmarshaled, $ArrayType$64_t1652_marshaled& marshaled);
void $ArrayType$64_t1652_marshal_back(const $ArrayType$64_t1652_marshaled& marshaled, $ArrayType$64_t1652& unmarshaled);
void $ArrayType$64_t1652_marshal_cleanup($ArrayType$64_t1652_marshaled& marshaled);
