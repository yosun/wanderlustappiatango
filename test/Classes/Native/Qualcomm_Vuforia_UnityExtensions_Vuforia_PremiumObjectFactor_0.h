﻿#pragma once
#include <stdint.h>
// Vuforia.IPremiumObjectFactory
struct IPremiumObjectFactory_t636;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.PremiumObjectFactory
struct PremiumObjectFactory_t637  : public Object_t
{
};
struct PremiumObjectFactory_t637_StaticFields{
	// Vuforia.IPremiumObjectFactory Vuforia.PremiumObjectFactory::sInstance
	Object_t * ___sInstance_0;
};
