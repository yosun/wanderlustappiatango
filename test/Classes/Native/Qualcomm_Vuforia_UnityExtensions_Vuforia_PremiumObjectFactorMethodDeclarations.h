﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PremiumObjectFactory/NullPremiumObjectFactory
struct NullPremiumObjectFactory_t635;

// System.Void Vuforia.PremiumObjectFactory/NullPremiumObjectFactory::.ctor()
 void NullPremiumObjectFactory__ctor_m2973 (NullPremiumObjectFactory_t635 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
