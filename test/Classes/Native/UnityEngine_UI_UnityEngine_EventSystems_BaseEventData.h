﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t186;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t184  : public Object_t
{
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t186 * ___m_EventSystem_0;
	// System.Boolean UnityEngine.EventSystems.BaseEventData::m_Used
	bool ___m_Used_1;
};
