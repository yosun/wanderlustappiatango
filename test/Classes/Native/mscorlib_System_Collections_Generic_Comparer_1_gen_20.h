﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.ICloudRecoEventHandler>
struct Comparer_1_t3698;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.ICloudRecoEventHandler>
struct Comparer_1_t3698  : public Object_t
{
};
struct Comparer_1_t3698_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.ICloudRecoEventHandler>::_default
	Comparer_1_t3698 * ____default_0;
};
