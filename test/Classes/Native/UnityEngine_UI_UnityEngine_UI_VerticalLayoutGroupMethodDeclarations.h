﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.VerticalLayoutGroup
struct VerticalLayoutGroup_t400;

// System.Void UnityEngine.UI.VerticalLayoutGroup::.ctor()
 void VerticalLayoutGroup__ctor_m1813 (VerticalLayoutGroup_t400 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputHorizontal()
 void VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1814 (VerticalLayoutGroup_t400 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::CalculateLayoutInputVertical()
 void VerticalLayoutGroup_CalculateLayoutInputVertical_m1815 (VerticalLayoutGroup_t400 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutHorizontal()
 void VerticalLayoutGroup_SetLayoutHorizontal_m1816 (VerticalLayoutGroup_t400 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.VerticalLayoutGroup::SetLayoutVertical()
 void VerticalLayoutGroup_SetLayoutVertical_m1817 (VerticalLayoutGroup_t400 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
