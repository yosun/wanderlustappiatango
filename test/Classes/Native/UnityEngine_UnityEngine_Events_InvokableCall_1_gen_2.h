﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<GyroCameraYo>
struct UnityAction_1_t2724;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<GyroCameraYo>
struct InvokableCall_1_t2723  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<GyroCameraYo>::Delegate
	UnityAction_1_t2724 * ___Delegate_0;
};
