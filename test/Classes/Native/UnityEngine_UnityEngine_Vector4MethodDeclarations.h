﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Vector4
struct Vector4_t314;
// System.Object
struct Object_t;
// System.String
struct String_t;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
 void Vector4__ctor_m2133 (Vector4_t314 * __this, float ___x, float ___y, float ___z, float ___w, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::get_Item(System.Int32)
 float Vector4_get_Item_m2214 (Vector4_t314 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
 void Vector4_set_Item_m2216 (Vector4_t314 * __this, int32_t ___index, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector4::GetHashCode()
 int32_t Vector4_GetHashCode_m5876 (Vector4_t314 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
 bool Vector4_Equals_m5877 (Vector4_t314 * __this, Object_t * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector4::ToString()
 String_t* Vector4_ToString_m5878 (Vector4_t314 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
 float Vector4_Dot_m5879 (Object_t * __this/* static, unused */, Vector4_t314  ___a, Vector4_t314  ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::SqrMagnitude(UnityEngine.Vector4)
 float Vector4_SqrMagnitude_m5880 (Object_t * __this/* static, unused */, Vector4_t314  ___a, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::get_sqrMagnitude()
 float Vector4_get_sqrMagnitude_m2192 (Vector4_t314 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
 Vector4_t314  Vector4_get_zero_m2198 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Subtraction(UnityEngine.Vector4,UnityEngine.Vector4)
 Vector4_t314  Vector4_op_Subtraction_m5881 (Object_t * __this/* static, unused */, Vector4_t314  ___a, Vector4_t314  ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Division(UnityEngine.Vector4,System.Single)
 Vector4_t314  Vector4_op_Division_m2211 (Object_t * __this/* static, unused */, Vector4_t314  ___a, float ___d, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
 bool Vector4_op_Equality_m5882 (Object_t * __this/* static, unused */, Vector4_t314  ___lhs, Vector4_t314  ___rhs, MethodInfo* method) IL2CPP_METHOD_ATTR;
