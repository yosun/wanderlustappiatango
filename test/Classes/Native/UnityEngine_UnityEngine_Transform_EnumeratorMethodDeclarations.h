﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Transform/Enumerator
struct Enumerator_t1001;
// System.Object
struct Object_t;
// UnityEngine.Transform
struct Transform_t10;

// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
 void Enumerator__ctor_m6048 (Enumerator_t1001 * __this, Transform_t10 * ___outer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.Transform/Enumerator::get_Current()
 Object_t * Enumerator_get_Current_m6049 (Enumerator_t1001 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
 bool Enumerator_MoveNext_m6050 (Enumerator_t1001 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
