﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.UIntPtr
struct UIntPtr_t1690;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1066;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void System.UIntPtr::.ctor(System.UInt64)
 void UIntPtr__ctor_m9493 (UIntPtr_t1690 * __this, uint64_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UIntPtr::.ctor(System.UInt32)
 void UIntPtr__ctor_m9494 (UIntPtr_t1690 * __this, uint32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UIntPtr::.ctor(System.Void*)
 void UIntPtr__ctor_m9495 (UIntPtr_t1690 * __this, void* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UIntPtr::.cctor()
 void UIntPtr__cctor_m9496 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.UIntPtr::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void UIntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m9497 (UIntPtr_t1690 * __this, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UIntPtr::Equals(System.Object)
 bool UIntPtr_Equals_m9498 (UIntPtr_t1690 * __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UIntPtr::GetHashCode()
 int32_t UIntPtr_GetHashCode_m9499 (UIntPtr_t1690 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.UIntPtr::ToUInt32()
 uint32_t UIntPtr_ToUInt32_m9500 (UIntPtr_t1690 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.UIntPtr::ToUInt64()
 uint64_t UIntPtr_ToUInt64_m9501 (UIntPtr_t1690 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void* System.UIntPtr::ToPointer()
 void* UIntPtr_ToPointer_m9502 (UIntPtr_t1690 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UIntPtr::ToString()
 String_t* UIntPtr_ToString_m9503 (UIntPtr_t1690 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UIntPtr::get_Size()
 int32_t UIntPtr_get_Size_m9504 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UIntPtr::op_Equality(System.UIntPtr,System.UIntPtr)
 bool UIntPtr_op_Equality_m9505 (Object_t * __this/* static, unused */, UIntPtr_t1690  ___value1, UIntPtr_t1690  ___value2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UIntPtr::op_Inequality(System.UIntPtr,System.UIntPtr)
 bool UIntPtr_op_Inequality_m9506 (Object_t * __this/* static, unused */, UIntPtr_t1690  ___value1, UIntPtr_t1690  ___value2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.UIntPtr::op_Explicit(System.UIntPtr)
 uint64_t UIntPtr_op_Explicit_m9507 (Object_t * __this/* static, unused */, UIntPtr_t1690  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.UIntPtr::op_Explicit(System.UIntPtr)
 uint32_t UIntPtr_op_Explicit_m9508 (Object_t * __this/* static, unused */, UIntPtr_t1690  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr System.UIntPtr::op_Explicit(System.UInt64)
 UIntPtr_t1690  UIntPtr_op_Explicit_m9509 (Object_t * __this/* static, unused */, uint64_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr System.UIntPtr::op_Explicit(System.Void*)
 UIntPtr_t1690  UIntPtr_op_Explicit_m9510 (Object_t * __this/* static, unused */, void* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void* System.UIntPtr::op_Explicit(System.UIntPtr)
 void* UIntPtr_op_Explicit_m9511 (Object_t * __this/* static, unused */, UIntPtr_t1690  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UIntPtr System.UIntPtr::op_Explicit(System.UInt32)
 UIntPtr_t1690  UIntPtr_op_Explicit_m9512 (Object_t * __this/* static, unused */, uint32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
