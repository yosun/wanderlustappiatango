﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Terrain>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_151.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Terrain>
struct CachedInvokableCall_1_t4681  : public InvokableCall_1_t4682
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Terrain>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
