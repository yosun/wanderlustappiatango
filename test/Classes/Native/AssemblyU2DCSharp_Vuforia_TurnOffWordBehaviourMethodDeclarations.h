﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TurnOffWordBehaviour
struct TurnOffWordBehaviour_t80;

// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
 void TurnOffWordBehaviour__ctor_m174 (TurnOffWordBehaviour_t80 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
 void TurnOffWordBehaviour_Awake_m175 (TurnOffWordBehaviour_t80 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
