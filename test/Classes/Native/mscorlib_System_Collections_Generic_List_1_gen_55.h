﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ICanvasElement[]
struct ICanvasElementU5BU5D_t3222;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>
struct List_1_t3220  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::_items
	ICanvasElementU5BU5D_t3222* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::_version
	int32_t ____version_3;
};
struct List_1_t3220_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::EmptyArray
	ICanvasElementU5BU5D_t3222* ___EmptyArray_4;
};
