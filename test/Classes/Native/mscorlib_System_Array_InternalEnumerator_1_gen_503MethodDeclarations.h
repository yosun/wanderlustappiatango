﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509NameType>
struct InternalEnumerator_1_t4881;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Security.Cryptography.X509Certificates.X509NameType
#include "System_System_Security_Cryptography_X509Certificates_X509Nam.h"

// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509NameType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29987 (InternalEnumerator_1_t4881 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509NameType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29988 (InternalEnumerator_1_t4881 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509NameType>::Dispose()
 void InternalEnumerator_1_Dispose_m29989 (InternalEnumerator_1_t4881 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509NameType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29990 (InternalEnumerator_1_t4881 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509NameType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29991 (InternalEnumerator_1_t4881 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
