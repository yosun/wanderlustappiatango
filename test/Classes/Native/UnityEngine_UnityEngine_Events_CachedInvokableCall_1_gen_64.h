﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.RawImage>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_63.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.RawImage>
struct CachedInvokableCall_1_t3445  : public InvokableCall_1_t3446
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.RawImage>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
