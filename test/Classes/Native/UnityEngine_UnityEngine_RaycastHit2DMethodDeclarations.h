﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RaycastHit2D
struct RaycastHit2D_t447;
// UnityEngine.Collider2D
struct Collider2D_t448;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1008;
// UnityEngine.Transform
struct Transform_t10;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
 Vector2_t9  RaycastHit2D_get_point_m2026 (RaycastHit2D_t447 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
 Vector2_t9  RaycastHit2D_get_normal_m2027 (RaycastHit2D_t447 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit2D::get_fraction()
 float RaycastHit2D_get_fraction_m2165 (RaycastHit2D_t447 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
 Collider2D_t448 * RaycastHit2D_get_collider_m2028 (RaycastHit2D_t447 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
 Rigidbody2D_t1008 * RaycastHit2D_get_rigidbody_m6105 (RaycastHit2D_t447 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
 Transform_t10 * RaycastHit2D_get_transform_m2030 (RaycastHit2D_t447 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
