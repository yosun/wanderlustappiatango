﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo Int32_t93_il2cpp_TypeInfo;
extern TypeInfo Char_t109_il2cpp_TypeInfo;
// UnityEngine.UI.InputField/OnValidateInput
struct OnValidateInput_t324  : public MulticastDelegate_t325
{
};
