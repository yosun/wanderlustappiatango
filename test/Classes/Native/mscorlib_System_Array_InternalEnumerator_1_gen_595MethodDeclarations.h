﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.SortedList/EnumeratorMode>
struct InternalEnumerator_1_t4978;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.SortedList/EnumeratorMode
#include "mscorlib_System_Collections_SortedList_EnumeratorMode.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/EnumeratorMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30472 (InternalEnumerator_1_t4978 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/EnumeratorMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30473 (InternalEnumerator_1_t4978 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/EnumeratorMode>::Dispose()
 void InternalEnumerator_1_Dispose_m30474 (InternalEnumerator_1_t4978 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/EnumeratorMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30475 (InternalEnumerator_1_t4978 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/EnumeratorMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30476 (InternalEnumerator_1_t4978 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
