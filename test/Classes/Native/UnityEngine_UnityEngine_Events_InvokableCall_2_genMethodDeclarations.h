﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
struct InvokableCall_2_t4787;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t142;
// System.Object[]
struct ObjectU5BU5D_t115;

// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
 void InvokableCall_2__ctor_m29321_gshared (InvokableCall_2_t4787 * __this, Object_t * ___target, MethodInfo_t142 * ___theFunction, MethodInfo* method);
#define InvokableCall_2__ctor_m29321(__this, ___target, ___theFunction, method) (void)InvokableCall_2__ctor_m29321_gshared((InvokableCall_2_t4787 *)__this, (Object_t *)___target, (MethodInfo_t142 *)___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Invoke(System.Object[])
 void InvokableCall_2_Invoke_m29322_gshared (InvokableCall_2_t4787 * __this, ObjectU5BU5D_t115* ___args, MethodInfo* method);
#define InvokableCall_2_Invoke_m29322(__this, ___args, method) (void)InvokableCall_2_Invoke_m29322_gshared((InvokableCall_2_t4787 *)__this, (ObjectU5BU5D_t115*)___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`2<System.Object,System.Object>::Find(System.Object,System.Reflection.MethodInfo)
 bool InvokableCall_2_Find_m29323_gshared (InvokableCall_2_t4787 * __this, Object_t * ___targetObj, MethodInfo_t142 * ___method, MethodInfo* method);
#define InvokableCall_2_Find_m29323(__this, ___targetObj, ___method, method) (bool)InvokableCall_2_Find_m29323_gshared((InvokableCall_2_t4787 *)__this, (Object_t *)___targetObj, (MethodInfo_t142 *)___method, method)
