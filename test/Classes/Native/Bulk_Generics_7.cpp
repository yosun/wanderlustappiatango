﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_33.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo InvokableCall_1_t2942_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_33MethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_40.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
#include "mscorlib_ArrayTypes.h"
// System.String
#include "mscorlib_System_String.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour.h"
extern TypeInfo UnityAction_1_t2943_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t507_il2cpp_TypeInfo;
extern TypeInfo TextRecoBehaviour_t78_il2cpp_TypeInfo;
extern TypeInfo Void_t99_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_40MethodDeclarations.h"
extern Il2CppType UnityAction_1_t2943_0_0_0;
extern MethodInfo BaseInvokableCall__ctor_m6358_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m395_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m456_MethodInfo;
extern MethodInfo Delegate_Combine_m2149_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6357_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2468_MethodInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisTextRecoBehaviour_t78_m32711_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6359_MethodInfo;
extern MethodInfo UnityAction_1_Invoke_m15168_MethodInfo;
extern MethodInfo Delegate_get_Target_m6536_MethodInfo;
extern MethodInfo Delegate_get_Method_m6534_MethodInfo;
struct BaseInvokableCall_t1075;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.TextRecoBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.TextRecoBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisTextRecoBehaviour_t78_m32711(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)

// System.Array
#include "mscorlib_System_Array.h"

// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>
extern Il2CppType UnityAction_1_t2943_0_0_1;
FieldInfo InvokableCall_1_t2942____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2943_0_0_1/* type */
	, &InvokableCall_1_t2942_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2942, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2942_FieldInfos[] =
{
	&InvokableCall_1_t2942____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2942_InvokableCall_1__ctor_m15163_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15163_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15163_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2942_InvokableCall_1__ctor_m15163_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15163_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2943_0_0_0;
static ParameterInfo InvokableCall_1_t2942_InvokableCall_1__ctor_m15164_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2943_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15164_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15164_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2942_InvokableCall_1__ctor_m15164_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15164_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2942_InvokableCall_1_Invoke_m15165_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15165_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15165_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2942_InvokableCall_1_Invoke_m15165_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15165_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2942_InvokableCall_1_Find_m15166_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15166_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15166_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2942_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2942_InvokableCall_1_Find_m15166_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15166_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2942_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15163_MethodInfo,
	&InvokableCall_1__ctor_m15164_MethodInfo,
	&InvokableCall_1_Invoke_m15165_MethodInfo,
	&InvokableCall_1_Find_m15166_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m304_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo Object_GetHashCode_m305_MethodInfo;
extern MethodInfo Object_ToString_m306_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15165_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15166_MethodInfo;
static MethodInfo* InvokableCall_1_t2942_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m15165_MethodInfo,
	&InvokableCall_1_Find_m15166_MethodInfo,
};
extern TypeInfo UnityAction_1_t2943_il2cpp_TypeInfo;
extern TypeInfo TextRecoBehaviour_t78_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2942_RGCTXData[5] = 
{
	&UnityAction_1_t2943_0_0_0/* Type Usage */,
	&UnityAction_1_t2943_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTextRecoBehaviour_t78_m32711_MethodInfo/* Method Usage */,
	&TextRecoBehaviour_t78_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15168_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2942_0_0_0;
extern Il2CppType InvokableCall_1_t2942_1_0_0;
extern TypeInfo BaseInvokableCall_t1075_il2cpp_TypeInfo;
struct InvokableCall_1_t2942;
extern Il2CppGenericClass InvokableCall_1_t2942_GenericClass;
TypeInfo InvokableCall_1_t2942_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2942_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2942_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2942_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2942_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2942_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2942_0_0_0/* byval_arg */
	, &InvokableCall_1_t2942_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2942_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2942_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2942)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2943_UnityAction_1__ctor_m15167_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15167_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15167_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2943_UnityAction_1__ctor_m15167_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15167_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
static ParameterInfo UnityAction_1_t2943_UnityAction_1_Invoke_m15168_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t78_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15168_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15168_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2943_UnityAction_1_Invoke_m15168_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15168_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2943_UnityAction_1_BeginInvoke_m15169_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t78_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15169_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15169_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2943_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2943_UnityAction_1_BeginInvoke_m15169_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15169_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2943_UnityAction_1_EndInvoke_m15170_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15170_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TextRecoBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15170_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2943_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2943_UnityAction_1_EndInvoke_m15170_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15170_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2943_MethodInfos[] =
{
	&UnityAction_1__ctor_m15167_MethodInfo,
	&UnityAction_1_Invoke_m15168_MethodInfo,
	&UnityAction_1_BeginInvoke_m15169_MethodInfo,
	&UnityAction_1_EndInvoke_m15170_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2241_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2242_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2243_MethodInfo;
extern MethodInfo Delegate_Clone_m2244_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2245_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2246_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2247_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m15169_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15170_MethodInfo;
static MethodInfo* UnityAction_1_t2943_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m15168_MethodInfo,
	&UnityAction_1_BeginInvoke_m15169_MethodInfo,
	&UnityAction_1_EndInvoke_m15170_MethodInfo,
};
extern TypeInfo ICloneable_t481_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t482_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t2943_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2943_1_0_0;
extern TypeInfo MulticastDelegate_t325_il2cpp_TypeInfo;
struct UnityAction_1_t2943;
extern Il2CppGenericClass UnityAction_1_t2943_GenericClass;
TypeInfo UnityAction_1_t2943_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2943_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2943_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2943_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2943_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2943_0_0_0/* byval_arg */
	, &UnityAction_1_t2943_1_0_0/* this_arg */
	, UnityAction_1_t2943_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2943_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2943)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5899_il2cpp_TypeInfo;

// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42619_MethodInfo;
static PropertyInfo IEnumerator_1_t5899____Current_PropertyInfo = 
{
	&IEnumerator_1_t5899_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42619_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5899_PropertyInfos[] =
{
	&IEnumerator_1_t5899____Current_PropertyInfo,
	NULL
};
extern Il2CppType TurnOffBehaviour_t79_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42619_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42619_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5899_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffBehaviour_t79_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42619_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5899_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42619_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t266_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t5899_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5899_0_0_0;
extern Il2CppType IEnumerator_1_t5899_1_0_0;
struct IEnumerator_1_t5899;
extern Il2CppGenericClass IEnumerator_1_t5899_GenericClass;
TypeInfo IEnumerator_1_t5899_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5899_MethodInfos/* methods */
	, IEnumerator_1_t5899_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5899_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5899_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5899_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5899_0_0_0/* byval_arg */
	, &IEnumerator_1_t5899_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5899_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_109.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2944_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_109MethodDeclarations.h"

// System.Int32
#include "mscorlib_System_Int32.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
extern TypeInfo TurnOffBehaviour_t79_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1493_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m15175_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7651_MethodInfo;
extern MethodInfo Array_get_Length_m7656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTurnOffBehaviour_t79_m32713_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m31436_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m31436(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.TurnOffBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.TurnOffBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisTurnOffBehaviour_t79_m32713(__this, p0, method) (TurnOffBehaviour_t79 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2944____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2944, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2944____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2944, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2944_FieldInfos[] =
{
	&InternalEnumerator_1_t2944____array_0_FieldInfo,
	&InternalEnumerator_1_t2944____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15172_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2944____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2944_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15172_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2944____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2944_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15175_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2944_PropertyInfos[] =
{
	&InternalEnumerator_1_t2944____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2944____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2944_InternalEnumerator_1__ctor_m15171_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15171_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15171_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2944_InternalEnumerator_1__ctor_m15171_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15171_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15172_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15172_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15172_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15173_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15173_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15173_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15174_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15174_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15174_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffBehaviour_t79_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15175_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.TurnOffBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15175_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffBehaviour_t79_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15175_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2944_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15171_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15172_MethodInfo,
	&InternalEnumerator_1_Dispose_m15173_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15174_MethodInfo,
	&InternalEnumerator_1_get_Current_m15175_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m15174_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15173_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2944_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15172_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15174_MethodInfo,
	&InternalEnumerator_1_Dispose_m15173_MethodInfo,
	&InternalEnumerator_1_get_Current_m15175_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2944_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5899_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2944_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5899_il2cpp_TypeInfo, 7},
};
extern TypeInfo TurnOffBehaviour_t79_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2944_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15175_MethodInfo/* Method Usage */,
	&TurnOffBehaviour_t79_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTurnOffBehaviour_t79_m32713_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2944_0_0_0;
extern Il2CppType InternalEnumerator_1_t2944_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t2944_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t2944_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2944_MethodInfos/* methods */
	, InternalEnumerator_1_t2944_PropertyInfos/* properties */
	, InternalEnumerator_1_t2944_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2944_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2944_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2944_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2944_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2944_1_0_0/* this_arg */
	, InternalEnumerator_1_t2944_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2944_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2944_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2944)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7515_il2cpp_TypeInfo;

#include "Assembly-CSharp_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>
extern MethodInfo ICollection_1_get_Count_m42620_MethodInfo;
static PropertyInfo ICollection_1_t7515____Count_PropertyInfo = 
{
	&ICollection_1_t7515_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42620_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42621_MethodInfo;
static PropertyInfo ICollection_1_t7515____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7515_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42621_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7515_PropertyInfos[] =
{
	&ICollection_1_t7515____Count_PropertyInfo,
	&ICollection_1_t7515____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42620_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42620_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7515_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42620_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42621_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42621_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7515_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42621_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffBehaviour_t79_0_0_0;
extern Il2CppType TurnOffBehaviour_t79_0_0_0;
static ParameterInfo ICollection_1_t7515_ICollection_1_Add_m42622_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t79_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42622_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42622_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7515_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7515_ICollection_1_Add_m42622_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42622_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42623_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42623_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7515_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42623_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffBehaviour_t79_0_0_0;
static ParameterInfo ICollection_1_t7515_ICollection_1_Contains_m42624_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t79_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42624_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42624_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7515_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7515_ICollection_1_Contains_m42624_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42624_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffBehaviourU5BU5D_t5192_0_0_0;
extern Il2CppType TurnOffBehaviourU5BU5D_t5192_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7515_ICollection_1_CopyTo_m42625_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviourU5BU5D_t5192_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42625_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42625_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7515_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7515_ICollection_1_CopyTo_m42625_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42625_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffBehaviour_t79_0_0_0;
static ParameterInfo ICollection_1_t7515_ICollection_1_Remove_m42626_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t79_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42626_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42626_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7515_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7515_ICollection_1_Remove_m42626_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42626_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7515_MethodInfos[] =
{
	&ICollection_1_get_Count_m42620_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42621_MethodInfo,
	&ICollection_1_Add_m42622_MethodInfo,
	&ICollection_1_Clear_m42623_MethodInfo,
	&ICollection_1_Contains_m42624_MethodInfo,
	&ICollection_1_CopyTo_m42625_MethodInfo,
	&ICollection_1_Remove_m42626_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7517_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7515_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7517_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7515_0_0_0;
extern Il2CppType ICollection_1_t7515_1_0_0;
struct ICollection_1_t7515;
extern Il2CppGenericClass ICollection_1_t7515_GenericClass;
TypeInfo ICollection_1_t7515_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7515_MethodInfos/* methods */
	, ICollection_1_t7515_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7515_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7515_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7515_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7515_0_0_0/* byval_arg */
	, &ICollection_1_t7515_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7515_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffBehaviour>
extern Il2CppType IEnumerator_1_t5899_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42627_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42627_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7517_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5899_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42627_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7517_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42627_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7517_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7517_0_0_0;
extern Il2CppType IEnumerable_1_t7517_1_0_0;
struct IEnumerable_1_t7517;
extern Il2CppGenericClass IEnumerable_1_t7517_GenericClass;
TypeInfo IEnumerable_1_t7517_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7517_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7517_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7517_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7517_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7517_0_0_0/* byval_arg */
	, &IEnumerable_1_t7517_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7517_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7516_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>
extern MethodInfo IList_1_get_Item_m42628_MethodInfo;
extern MethodInfo IList_1_set_Item_m42629_MethodInfo;
static PropertyInfo IList_1_t7516____Item_PropertyInfo = 
{
	&IList_1_t7516_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42628_MethodInfo/* get */
	, &IList_1_set_Item_m42629_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7516_PropertyInfos[] =
{
	&IList_1_t7516____Item_PropertyInfo,
	NULL
};
extern Il2CppType TurnOffBehaviour_t79_0_0_0;
static ParameterInfo IList_1_t7516_IList_1_IndexOf_m42630_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t79_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42630_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42630_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7516_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7516_IList_1_IndexOf_m42630_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42630_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TurnOffBehaviour_t79_0_0_0;
static ParameterInfo IList_1_t7516_IList_1_Insert_m42631_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t79_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42631_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42631_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7516_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7516_IList_1_Insert_m42631_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42631_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7516_IList_1_RemoveAt_m42632_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42632_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42632_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7516_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7516_IList_1_RemoveAt_m42632_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42632_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7516_IList_1_get_Item_m42628_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType TurnOffBehaviour_t79_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42628_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42628_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7516_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffBehaviour_t79_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7516_IList_1_get_Item_m42628_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42628_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TurnOffBehaviour_t79_0_0_0;
static ParameterInfo IList_1_t7516_IList_1_set_Item_m42629_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t79_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42629_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42629_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7516_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7516_IList_1_set_Item_m42629_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42629_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7516_MethodInfos[] =
{
	&IList_1_IndexOf_m42630_MethodInfo,
	&IList_1_Insert_m42631_MethodInfo,
	&IList_1_RemoveAt_m42632_MethodInfo,
	&IList_1_get_Item_m42628_MethodInfo,
	&IList_1_set_Item_m42629_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7516_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7515_il2cpp_TypeInfo,
	&IEnumerable_1_t7517_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7516_0_0_0;
extern Il2CppType IList_1_t7516_1_0_0;
struct IList_1_t7516;
extern Il2CppGenericClass IList_1_t7516_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7516_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7516_MethodInfos/* methods */
	, IList_1_t7516_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7516_il2cpp_TypeInfo/* element_class */
	, IList_1_t7516_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7516_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7516_0_0_0/* byval_arg */
	, &IList_1_t7516_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7516_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7518_il2cpp_TypeInfo;

// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeha.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42633_MethodInfo;
static PropertyInfo ICollection_1_t7518____Count_PropertyInfo = 
{
	&ICollection_1_t7518_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42633_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42634_MethodInfo;
static PropertyInfo ICollection_1_t7518____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7518_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42634_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7518_PropertyInfos[] =
{
	&ICollection_1_t7518____Count_PropertyInfo,
	&ICollection_1_t7518____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42633_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42633_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7518_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42633_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42634_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42634_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7518_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42634_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffAbstractBehaviour_t57_0_0_0;
extern Il2CppType TurnOffAbstractBehaviour_t57_0_0_0;
static ParameterInfo ICollection_1_t7518_ICollection_1_Add_m42635_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffAbstractBehaviour_t57_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42635_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42635_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7518_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7518_ICollection_1_Add_m42635_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42635_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42636_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42636_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7518_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42636_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffAbstractBehaviour_t57_0_0_0;
static ParameterInfo ICollection_1_t7518_ICollection_1_Contains_m42637_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffAbstractBehaviour_t57_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42637_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42637_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7518_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7518_ICollection_1_Contains_m42637_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42637_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffAbstractBehaviourU5BU5D_t5519_0_0_0;
extern Il2CppType TurnOffAbstractBehaviourU5BU5D_t5519_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7518_ICollection_1_CopyTo_m42638_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffAbstractBehaviourU5BU5D_t5519_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42638_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42638_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7518_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7518_ICollection_1_CopyTo_m42638_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42638_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffAbstractBehaviour_t57_0_0_0;
static ParameterInfo ICollection_1_t7518_ICollection_1_Remove_m42639_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffAbstractBehaviour_t57_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42639_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42639_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7518_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7518_ICollection_1_Remove_m42639_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42639_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7518_MethodInfos[] =
{
	&ICollection_1_get_Count_m42633_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42634_MethodInfo,
	&ICollection_1_Add_m42635_MethodInfo,
	&ICollection_1_Clear_m42636_MethodInfo,
	&ICollection_1_Contains_m42637_MethodInfo,
	&ICollection_1_CopyTo_m42638_MethodInfo,
	&ICollection_1_Remove_m42639_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7520_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7518_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7520_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7518_0_0_0;
extern Il2CppType ICollection_1_t7518_1_0_0;
struct ICollection_1_t7518;
extern Il2CppGenericClass ICollection_1_t7518_GenericClass;
TypeInfo ICollection_1_t7518_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7518_MethodInfos/* methods */
	, ICollection_1_t7518_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7518_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7518_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7518_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7518_0_0_0/* byval_arg */
	, &ICollection_1_t7518_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7518_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5901_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42640_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42640_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7520_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5901_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42640_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7520_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42640_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7520_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7520_0_0_0;
extern Il2CppType IEnumerable_1_t7520_1_0_0;
struct IEnumerable_1_t7520;
extern Il2CppGenericClass IEnumerable_1_t7520_GenericClass;
TypeInfo IEnumerable_1_t7520_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7520_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7520_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7520_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7520_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7520_0_0_0/* byval_arg */
	, &IEnumerable_1_t7520_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7520_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5901_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42641_MethodInfo;
static PropertyInfo IEnumerator_1_t5901____Current_PropertyInfo = 
{
	&IEnumerator_1_t5901_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42641_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5901_PropertyInfos[] =
{
	&IEnumerator_1_t5901____Current_PropertyInfo,
	NULL
};
extern Il2CppType TurnOffAbstractBehaviour_t57_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42641_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42641_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5901_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffAbstractBehaviour_t57_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42641_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5901_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42641_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5901_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5901_0_0_0;
extern Il2CppType IEnumerator_1_t5901_1_0_0;
struct IEnumerator_1_t5901;
extern Il2CppGenericClass IEnumerator_1_t5901_GenericClass;
TypeInfo IEnumerator_1_t5901_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5901_MethodInfos/* methods */
	, IEnumerator_1_t5901_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5901_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5901_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5901_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5901_0_0_0/* byval_arg */
	, &IEnumerator_1_t5901_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5901_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_110.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2945_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_110MethodDeclarations.h"

extern TypeInfo TurnOffAbstractBehaviour_t57_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15180_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTurnOffAbstractBehaviour_t57_m32724_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.TurnOffAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.TurnOffAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisTurnOffAbstractBehaviour_t57_m32724(__this, p0, method) (TurnOffAbstractBehaviour_t57 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2945____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2945_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2945, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2945____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2945_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2945, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2945_FieldInfos[] =
{
	&InternalEnumerator_1_t2945____array_0_FieldInfo,
	&InternalEnumerator_1_t2945____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15177_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2945____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2945_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15177_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2945____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2945_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15180_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2945_PropertyInfos[] =
{
	&InternalEnumerator_1_t2945____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2945____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2945_InternalEnumerator_1__ctor_m15176_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15176_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15176_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2945_InternalEnumerator_1__ctor_m15176_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15176_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15177_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15177_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2945_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15177_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15178_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15178_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15178_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15179_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15179_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2945_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15179_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffAbstractBehaviour_t57_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15180_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.TurnOffAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15180_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2945_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffAbstractBehaviour_t57_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15180_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2945_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15176_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15177_MethodInfo,
	&InternalEnumerator_1_Dispose_m15178_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15179_MethodInfo,
	&InternalEnumerator_1_get_Current_m15180_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15179_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15178_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2945_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15177_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15179_MethodInfo,
	&InternalEnumerator_1_Dispose_m15178_MethodInfo,
	&InternalEnumerator_1_get_Current_m15180_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2945_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5901_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2945_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5901_il2cpp_TypeInfo, 7},
};
extern TypeInfo TurnOffAbstractBehaviour_t57_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2945_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15180_MethodInfo/* Method Usage */,
	&TurnOffAbstractBehaviour_t57_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTurnOffAbstractBehaviour_t57_m32724_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2945_0_0_0;
extern Il2CppType InternalEnumerator_1_t2945_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2945_GenericClass;
TypeInfo InternalEnumerator_1_t2945_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2945_MethodInfos/* methods */
	, InternalEnumerator_1_t2945_PropertyInfos/* properties */
	, InternalEnumerator_1_t2945_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2945_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2945_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2945_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2945_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2945_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2945_1_0_0/* this_arg */
	, InternalEnumerator_1_t2945_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2945_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2945_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2945)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7519_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42642_MethodInfo;
extern MethodInfo IList_1_set_Item_m42643_MethodInfo;
static PropertyInfo IList_1_t7519____Item_PropertyInfo = 
{
	&IList_1_t7519_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42642_MethodInfo/* get */
	, &IList_1_set_Item_m42643_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7519_PropertyInfos[] =
{
	&IList_1_t7519____Item_PropertyInfo,
	NULL
};
extern Il2CppType TurnOffAbstractBehaviour_t57_0_0_0;
static ParameterInfo IList_1_t7519_IList_1_IndexOf_m42644_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffAbstractBehaviour_t57_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42644_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42644_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7519_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7519_IList_1_IndexOf_m42644_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42644_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TurnOffAbstractBehaviour_t57_0_0_0;
static ParameterInfo IList_1_t7519_IList_1_Insert_m42645_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TurnOffAbstractBehaviour_t57_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42645_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42645_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7519_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7519_IList_1_Insert_m42645_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42645_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7519_IList_1_RemoveAt_m42646_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42646_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42646_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7519_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7519_IList_1_RemoveAt_m42646_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42646_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7519_IList_1_get_Item_m42642_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType TurnOffAbstractBehaviour_t57_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42642_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42642_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7519_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffAbstractBehaviour_t57_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7519_IList_1_get_Item_m42642_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42642_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TurnOffAbstractBehaviour_t57_0_0_0;
static ParameterInfo IList_1_t7519_IList_1_set_Item_m42643_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TurnOffAbstractBehaviour_t57_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42643_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42643_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7519_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7519_IList_1_set_Item_m42643_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42643_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7519_MethodInfos[] =
{
	&IList_1_IndexOf_m42644_MethodInfo,
	&IList_1_Insert_m42645_MethodInfo,
	&IList_1_RemoveAt_m42646_MethodInfo,
	&IList_1_get_Item_m42642_MethodInfo,
	&IList_1_set_Item_m42643_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7519_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7518_il2cpp_TypeInfo,
	&IEnumerable_1_t7520_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7519_0_0_0;
extern Il2CppType IList_1_t7519_1_0_0;
struct IList_1_t7519;
extern Il2CppGenericClass IList_1_t7519_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7519_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7519_MethodInfos/* methods */
	, IList_1_t7519_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7519_il2cpp_TypeInfo/* element_class */
	, IList_1_t7519_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7519_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7519_0_0_0/* byval_arg */
	, &IList_1_t7519_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7519_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_38.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2946_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_38MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_34.h"
extern TypeInfo ObjectU5BU5D_t115_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t2947_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_34MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15183_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15185_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2946____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2946_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2946, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2946_FieldInfos[] =
{
	&CachedInvokableCall_1_t2946____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType TurnOffBehaviour_t79_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2946_CachedInvokableCall_1__ctor_m15181_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t79_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15181_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15181_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2946_CachedInvokableCall_1__ctor_m15181_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15181_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2946_CachedInvokableCall_1_Invoke_m15182_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15182_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15182_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2946_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2946_CachedInvokableCall_1_Invoke_m15182_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15182_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2946_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15181_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15182_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15182_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15186_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2946_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15182_MethodInfo,
	&InvokableCall_1_Find_m15186_MethodInfo,
};
extern Il2CppType UnityAction_1_t2948_0_0_0;
extern TypeInfo UnityAction_1_t2948_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffBehaviour_t79_m32734_MethodInfo;
extern TypeInfo TurnOffBehaviour_t79_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15188_MethodInfo;
extern TypeInfo TurnOffBehaviour_t79_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2946_RGCTXData[8] = 
{
	&UnityAction_1_t2948_0_0_0/* Type Usage */,
	&UnityAction_1_t2948_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffBehaviour_t79_m32734_MethodInfo/* Method Usage */,
	&TurnOffBehaviour_t79_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15188_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15183_MethodInfo/* Method Usage */,
	&TurnOffBehaviour_t79_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15185_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2946_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2946_1_0_0;
struct CachedInvokableCall_1_t2946;
extern Il2CppGenericClass CachedInvokableCall_1_t2946_GenericClass;
TypeInfo CachedInvokableCall_1_t2946_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2946_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2946_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2947_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2946_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2946_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2946_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2946_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2946_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2946_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2946_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2946)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_41.h"
extern TypeInfo UnityAction_1_t2948_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_41MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.TurnOffBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.TurnOffBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffBehaviour_t79_m32734(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>
extern Il2CppType UnityAction_1_t2948_0_0_1;
FieldInfo InvokableCall_1_t2947____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2948_0_0_1/* type */
	, &InvokableCall_1_t2947_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2947, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2947_FieldInfos[] =
{
	&InvokableCall_1_t2947____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2947_InvokableCall_1__ctor_m15183_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15183_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15183_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2947_InvokableCall_1__ctor_m15183_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15183_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2948_0_0_0;
static ParameterInfo InvokableCall_1_t2947_InvokableCall_1__ctor_m15184_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2948_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15184_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15184_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2947_InvokableCall_1__ctor_m15184_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15184_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2947_InvokableCall_1_Invoke_m15185_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15185_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15185_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2947_InvokableCall_1_Invoke_m15185_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15185_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2947_InvokableCall_1_Find_m15186_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15186_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15186_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2947_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2947_InvokableCall_1_Find_m15186_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15186_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2947_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15183_MethodInfo,
	&InvokableCall_1__ctor_m15184_MethodInfo,
	&InvokableCall_1_Invoke_m15185_MethodInfo,
	&InvokableCall_1_Find_m15186_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2947_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m15185_MethodInfo,
	&InvokableCall_1_Find_m15186_MethodInfo,
};
extern TypeInfo UnityAction_1_t2948_il2cpp_TypeInfo;
extern TypeInfo TurnOffBehaviour_t79_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2947_RGCTXData[5] = 
{
	&UnityAction_1_t2948_0_0_0/* Type Usage */,
	&UnityAction_1_t2948_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffBehaviour_t79_m32734_MethodInfo/* Method Usage */,
	&TurnOffBehaviour_t79_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15188_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2947_0_0_0;
extern Il2CppType InvokableCall_1_t2947_1_0_0;
struct InvokableCall_1_t2947;
extern Il2CppGenericClass InvokableCall_1_t2947_GenericClass;
TypeInfo InvokableCall_1_t2947_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2947_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2947_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2947_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2947_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2947_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2947_0_0_0/* byval_arg */
	, &InvokableCall_1_t2947_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2947_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2947_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2947)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2948_UnityAction_1__ctor_m15187_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15187_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15187_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2948_UnityAction_1__ctor_m15187_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15187_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffBehaviour_t79_0_0_0;
static ParameterInfo UnityAction_1_t2948_UnityAction_1_Invoke_m15188_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t79_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15188_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15188_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2948_UnityAction_1_Invoke_m15188_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15188_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffBehaviour_t79_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2948_UnityAction_1_BeginInvoke_m15189_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffBehaviour_t79_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15189_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15189_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2948_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2948_UnityAction_1_BeginInvoke_m15189_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15189_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2948_UnityAction_1_EndInvoke_m15190_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15190_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15190_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2948_UnityAction_1_EndInvoke_m15190_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15190_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2948_MethodInfos[] =
{
	&UnityAction_1__ctor_m15187_MethodInfo,
	&UnityAction_1_Invoke_m15188_MethodInfo,
	&UnityAction_1_BeginInvoke_m15189_MethodInfo,
	&UnityAction_1_EndInvoke_m15190_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15189_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15190_MethodInfo;
static MethodInfo* UnityAction_1_t2948_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m15188_MethodInfo,
	&UnityAction_1_BeginInvoke_m15189_MethodInfo,
	&UnityAction_1_EndInvoke_m15190_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2948_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2948_1_0_0;
struct UnityAction_1_t2948;
extern Il2CppGenericClass UnityAction_1_t2948_GenericClass;
TypeInfo UnityAction_1_t2948_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2948_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2948_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2948_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2948_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2948_0_0_0/* byval_arg */
	, &UnityAction_1_t2948_1_0_0/* this_arg */
	, UnityAction_1_t2948_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2948_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2948)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.MeshRenderer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_6.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2949_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.MeshRenderer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_6MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.MeshRenderer>
extern Il2CppType MeshRenderer_t168_0_0_6;
FieldInfo CastHelper_1_t2949____t_0_FieldInfo = 
{
	"t"/* name */
	, &MeshRenderer_t168_0_0_6/* type */
	, &CastHelper_1_t2949_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2949, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2949____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2949_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2949, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2949_FieldInfos[] =
{
	&CastHelper_1_t2949____t_0_FieldInfo,
	&CastHelper_1_t2949____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2949_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2949_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2949_0_0_0;
extern Il2CppType CastHelper_1_t2949_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2949_GenericClass;
TypeInfo CastHelper_1_t2949_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2949_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2949_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2949_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2949_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2949_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2949_0_0_0/* byval_arg */
	, &CastHelper_1_t2949_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2949_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2949)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.MeshFilter>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_7.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2950_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.MeshFilter>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_7MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.MeshFilter>
extern Il2CppType MeshFilter_t169_0_0_6;
FieldInfo CastHelper_1_t2950____t_0_FieldInfo = 
{
	"t"/* name */
	, &MeshFilter_t169_0_0_6/* type */
	, &CastHelper_1_t2950_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2950, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2950____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2950_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2950, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2950_FieldInfos[] =
{
	&CastHelper_1_t2950____t_0_FieldInfo,
	&CastHelper_1_t2950____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2950_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2950_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2950_0_0_0;
extern Il2CppType CastHelper_1_t2950_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2950_GenericClass;
TypeInfo CastHelper_1_t2950_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2950_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2950_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2950_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2950_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2950_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2950_0_0_0/* byval_arg */
	, &CastHelper_1_t2950_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2950_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2950)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5903_il2cpp_TypeInfo;

// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffWordBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffWordBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42647_MethodInfo;
static PropertyInfo IEnumerator_1_t5903____Current_PropertyInfo = 
{
	&IEnumerator_1_t5903_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42647_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5903_PropertyInfos[] =
{
	&IEnumerator_1_t5903____Current_PropertyInfo,
	NULL
};
extern Il2CppType TurnOffWordBehaviour_t80_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42647_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.TurnOffWordBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42647_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5903_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffWordBehaviour_t80_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42647_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5903_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42647_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5903_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5903_0_0_0;
extern Il2CppType IEnumerator_1_t5903_1_0_0;
struct IEnumerator_1_t5903;
extern Il2CppGenericClass IEnumerator_1_t5903_GenericClass;
TypeInfo IEnumerator_1_t5903_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5903_MethodInfos/* methods */
	, IEnumerator_1_t5903_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5903_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5903_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5903_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5903_0_0_0/* byval_arg */
	, &IEnumerator_1_t5903_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5903_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_111.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2951_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_111MethodDeclarations.h"

extern TypeInfo TurnOffWordBehaviour_t80_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15195_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTurnOffWordBehaviour_t80_m32736_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.TurnOffWordBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.TurnOffWordBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisTurnOffWordBehaviour_t80_m32736(__this, p0, method) (TurnOffWordBehaviour_t80 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2951____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2951_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2951, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2951____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2951_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2951, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2951_FieldInfos[] =
{
	&InternalEnumerator_1_t2951____array_0_FieldInfo,
	&InternalEnumerator_1_t2951____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15192_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2951____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2951_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15192_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2951____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2951_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15195_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2951_PropertyInfos[] =
{
	&InternalEnumerator_1_t2951____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2951____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2951_InternalEnumerator_1__ctor_m15191_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15191_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15191_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2951_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2951_InternalEnumerator_1__ctor_m15191_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15191_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15192_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15192_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2951_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15192_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15193_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15193_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2951_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15193_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15194_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15194_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2951_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15194_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffWordBehaviour_t80_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15195_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.TurnOffWordBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15195_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2951_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffWordBehaviour_t80_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15195_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2951_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15191_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15192_MethodInfo,
	&InternalEnumerator_1_Dispose_m15193_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15194_MethodInfo,
	&InternalEnumerator_1_get_Current_m15195_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15194_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15193_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2951_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15192_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15194_MethodInfo,
	&InternalEnumerator_1_Dispose_m15193_MethodInfo,
	&InternalEnumerator_1_get_Current_m15195_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2951_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5903_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2951_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5903_il2cpp_TypeInfo, 7},
};
extern TypeInfo TurnOffWordBehaviour_t80_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2951_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15195_MethodInfo/* Method Usage */,
	&TurnOffWordBehaviour_t80_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTurnOffWordBehaviour_t80_m32736_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2951_0_0_0;
extern Il2CppType InternalEnumerator_1_t2951_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2951_GenericClass;
TypeInfo InternalEnumerator_1_t2951_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2951_MethodInfos/* methods */
	, InternalEnumerator_1_t2951_PropertyInfos/* properties */
	, InternalEnumerator_1_t2951_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2951_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2951_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2951_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2951_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2951_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2951_1_0_0/* this_arg */
	, InternalEnumerator_1_t2951_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2951_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2951_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2951)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7521_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>
extern MethodInfo ICollection_1_get_Count_m42648_MethodInfo;
static PropertyInfo ICollection_1_t7521____Count_PropertyInfo = 
{
	&ICollection_1_t7521_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42648_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42649_MethodInfo;
static PropertyInfo ICollection_1_t7521____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7521_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42649_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7521_PropertyInfos[] =
{
	&ICollection_1_t7521____Count_PropertyInfo,
	&ICollection_1_t7521____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42648_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42648_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7521_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42648_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42649_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42649_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7521_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42649_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffWordBehaviour_t80_0_0_0;
extern Il2CppType TurnOffWordBehaviour_t80_0_0_0;
static ParameterInfo ICollection_1_t7521_ICollection_1_Add_m42650_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t80_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42650_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42650_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7521_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7521_ICollection_1_Add_m42650_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42650_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42651_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42651_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7521_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42651_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffWordBehaviour_t80_0_0_0;
static ParameterInfo ICollection_1_t7521_ICollection_1_Contains_m42652_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t80_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42652_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42652_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7521_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7521_ICollection_1_Contains_m42652_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42652_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffWordBehaviourU5BU5D_t5193_0_0_0;
extern Il2CppType TurnOffWordBehaviourU5BU5D_t5193_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7521_ICollection_1_CopyTo_m42653_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviourU5BU5D_t5193_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42653_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42653_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7521_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7521_ICollection_1_CopyTo_m42653_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42653_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffWordBehaviour_t80_0_0_0;
static ParameterInfo ICollection_1_t7521_ICollection_1_Remove_m42654_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t80_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42654_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TurnOffWordBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42654_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7521_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7521_ICollection_1_Remove_m42654_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42654_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7521_MethodInfos[] =
{
	&ICollection_1_get_Count_m42648_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42649_MethodInfo,
	&ICollection_1_Add_m42650_MethodInfo,
	&ICollection_1_Clear_m42651_MethodInfo,
	&ICollection_1_Contains_m42652_MethodInfo,
	&ICollection_1_CopyTo_m42653_MethodInfo,
	&ICollection_1_Remove_m42654_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7523_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7521_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7523_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7521_0_0_0;
extern Il2CppType ICollection_1_t7521_1_0_0;
struct ICollection_1_t7521;
extern Il2CppGenericClass ICollection_1_t7521_GenericClass;
TypeInfo ICollection_1_t7521_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7521_MethodInfos/* methods */
	, ICollection_1_t7521_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7521_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7521_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7521_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7521_0_0_0/* byval_arg */
	, &ICollection_1_t7521_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7521_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffWordBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffWordBehaviour>
extern Il2CppType IEnumerator_1_t5903_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42655_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TurnOffWordBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42655_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7523_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5903_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42655_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7523_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42655_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7523_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7523_0_0_0;
extern Il2CppType IEnumerable_1_t7523_1_0_0;
struct IEnumerable_1_t7523;
extern Il2CppGenericClass IEnumerable_1_t7523_GenericClass;
TypeInfo IEnumerable_1_t7523_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7523_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7523_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7523_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7523_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7523_0_0_0/* byval_arg */
	, &IEnumerable_1_t7523_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7523_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7522_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>
extern MethodInfo IList_1_get_Item_m42656_MethodInfo;
extern MethodInfo IList_1_set_Item_m42657_MethodInfo;
static PropertyInfo IList_1_t7522____Item_PropertyInfo = 
{
	&IList_1_t7522_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42656_MethodInfo/* get */
	, &IList_1_set_Item_m42657_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7522_PropertyInfos[] =
{
	&IList_1_t7522____Item_PropertyInfo,
	NULL
};
extern Il2CppType TurnOffWordBehaviour_t80_0_0_0;
static ParameterInfo IList_1_t7522_IList_1_IndexOf_m42658_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t80_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42658_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42658_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7522_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7522_IList_1_IndexOf_m42658_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42658_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TurnOffWordBehaviour_t80_0_0_0;
static ParameterInfo IList_1_t7522_IList_1_Insert_m42659_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t80_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42659_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42659_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7522_IList_1_Insert_m42659_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42659_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7522_IList_1_RemoveAt_m42660_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42660_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42660_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7522_IList_1_RemoveAt_m42660_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42660_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7522_IList_1_get_Item_m42656_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType TurnOffWordBehaviour_t80_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42656_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42656_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7522_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffWordBehaviour_t80_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7522_IList_1_get_Item_m42656_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42656_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TurnOffWordBehaviour_t80_0_0_0;
static ParameterInfo IList_1_t7522_IList_1_set_Item_m42657_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t80_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42657_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TurnOffWordBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42657_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7522_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7522_IList_1_set_Item_m42657_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42657_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7522_MethodInfos[] =
{
	&IList_1_IndexOf_m42658_MethodInfo,
	&IList_1_Insert_m42659_MethodInfo,
	&IList_1_RemoveAt_m42660_MethodInfo,
	&IList_1_get_Item_m42656_MethodInfo,
	&IList_1_set_Item_m42657_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7522_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7521_il2cpp_TypeInfo,
	&IEnumerable_1_t7523_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7522_0_0_0;
extern Il2CppType IList_1_t7522_1_0_0;
struct IList_1_t7522;
extern Il2CppGenericClass IList_1_t7522_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7522_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7522_MethodInfos/* methods */
	, IList_1_t7522_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7522_il2cpp_TypeInfo/* element_class */
	, IList_1_t7522_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7522_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7522_0_0_0/* byval_arg */
	, &IList_1_t7522_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7522_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_39.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2952_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_39MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_35.h"
extern TypeInfo InvokableCall_1_t2953_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_35MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15198_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15200_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2952____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2952_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2952, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2952_FieldInfos[] =
{
	&CachedInvokableCall_1_t2952____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType TurnOffWordBehaviour_t80_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2952_CachedInvokableCall_1__ctor_m15196_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t80_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15196_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15196_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2952_CachedInvokableCall_1__ctor_m15196_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15196_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2952_CachedInvokableCall_1_Invoke_m15197_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15197_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15197_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2952_CachedInvokableCall_1_Invoke_m15197_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15197_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2952_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15196_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15197_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15197_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15201_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2952_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15197_MethodInfo,
	&InvokableCall_1_Find_m15201_MethodInfo,
};
extern Il2CppType UnityAction_1_t2954_0_0_0;
extern TypeInfo UnityAction_1_t2954_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffWordBehaviour_t80_m32746_MethodInfo;
extern TypeInfo TurnOffWordBehaviour_t80_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15203_MethodInfo;
extern TypeInfo TurnOffWordBehaviour_t80_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2952_RGCTXData[8] = 
{
	&UnityAction_1_t2954_0_0_0/* Type Usage */,
	&UnityAction_1_t2954_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffWordBehaviour_t80_m32746_MethodInfo/* Method Usage */,
	&TurnOffWordBehaviour_t80_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15203_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15198_MethodInfo/* Method Usage */,
	&TurnOffWordBehaviour_t80_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15200_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2952_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2952_1_0_0;
struct CachedInvokableCall_1_t2952;
extern Il2CppGenericClass CachedInvokableCall_1_t2952_GenericClass;
TypeInfo CachedInvokableCall_1_t2952_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2952_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2952_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2953_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2952_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2952_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2952_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2952_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2952_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2952_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2952_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2952)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_42.h"
extern TypeInfo UnityAction_1_t2954_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_42MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.TurnOffWordBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.TurnOffWordBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffWordBehaviour_t80_m32746(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>
extern Il2CppType UnityAction_1_t2954_0_0_1;
FieldInfo InvokableCall_1_t2953____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2954_0_0_1/* type */
	, &InvokableCall_1_t2953_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2953, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2953_FieldInfos[] =
{
	&InvokableCall_1_t2953____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2953_InvokableCall_1__ctor_m15198_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15198_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15198_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2953_InvokableCall_1__ctor_m15198_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15198_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2954_0_0_0;
static ParameterInfo InvokableCall_1_t2953_InvokableCall_1__ctor_m15199_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2954_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15199_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15199_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2953_InvokableCall_1__ctor_m15199_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15199_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2953_InvokableCall_1_Invoke_m15200_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15200_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15200_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2953_InvokableCall_1_Invoke_m15200_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15200_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2953_InvokableCall_1_Find_m15201_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15201_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15201_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2953_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2953_InvokableCall_1_Find_m15201_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15201_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2953_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15198_MethodInfo,
	&InvokableCall_1__ctor_m15199_MethodInfo,
	&InvokableCall_1_Invoke_m15200_MethodInfo,
	&InvokableCall_1_Find_m15201_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2953_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m15200_MethodInfo,
	&InvokableCall_1_Find_m15201_MethodInfo,
};
extern TypeInfo UnityAction_1_t2954_il2cpp_TypeInfo;
extern TypeInfo TurnOffWordBehaviour_t80_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2953_RGCTXData[5] = 
{
	&UnityAction_1_t2954_0_0_0/* Type Usage */,
	&UnityAction_1_t2954_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTurnOffWordBehaviour_t80_m32746_MethodInfo/* Method Usage */,
	&TurnOffWordBehaviour_t80_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15203_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2953_0_0_0;
extern Il2CppType InvokableCall_1_t2953_1_0_0;
struct InvokableCall_1_t2953;
extern Il2CppGenericClass InvokableCall_1_t2953_GenericClass;
TypeInfo InvokableCall_1_t2953_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2953_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2953_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2953_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2953_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2953_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2953_0_0_0/* byval_arg */
	, &InvokableCall_1_t2953_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2953_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2953_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2953)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2954_UnityAction_1__ctor_m15202_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15202_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15202_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2954_UnityAction_1__ctor_m15202_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15202_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffWordBehaviour_t80_0_0_0;
static ParameterInfo UnityAction_1_t2954_UnityAction_1_Invoke_m15203_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t80_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15203_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15203_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2954_UnityAction_1_Invoke_m15203_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15203_GenericMethod/* genericMethod */

};
extern Il2CppType TurnOffWordBehaviour_t80_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2954_UnityAction_1_BeginInvoke_m15204_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &TurnOffWordBehaviour_t80_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15204_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15204_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2954_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2954_UnityAction_1_BeginInvoke_m15204_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15204_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2954_UnityAction_1_EndInvoke_m15205_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15205_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15205_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2954_UnityAction_1_EndInvoke_m15205_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15205_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2954_MethodInfos[] =
{
	&UnityAction_1__ctor_m15202_MethodInfo,
	&UnityAction_1_Invoke_m15203_MethodInfo,
	&UnityAction_1_BeginInvoke_m15204_MethodInfo,
	&UnityAction_1_EndInvoke_m15205_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15204_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15205_MethodInfo;
static MethodInfo* UnityAction_1_t2954_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m15203_MethodInfo,
	&UnityAction_1_BeginInvoke_m15204_MethodInfo,
	&UnityAction_1_EndInvoke_m15205_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2954_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2954_1_0_0;
struct UnityAction_1_t2954;
extern Il2CppGenericClass UnityAction_1_t2954_GenericClass;
TypeInfo UnityAction_1_t2954_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2954_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2954_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2954_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2954_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2954_0_0_0/* byval_arg */
	, &UnityAction_1_t2954_1_0_0/* this_arg */
	, UnityAction_1_t2954_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2954_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2954)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5905_il2cpp_TypeInfo;

// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42661_MethodInfo;
static PropertyInfo IEnumerator_1_t5905____Current_PropertyInfo = 
{
	&IEnumerator_1_t5905_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42661_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5905_PropertyInfos[] =
{
	&IEnumerator_1_t5905____Current_PropertyInfo,
	NULL
};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42661_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42661_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5905_il2cpp_TypeInfo/* declaring_type */
	, &UserDefinedTargetBuildingBehaviour_t81_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42661_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5905_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42661_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5905_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5905_0_0_0;
extern Il2CppType IEnumerator_1_t5905_1_0_0;
struct IEnumerator_1_t5905;
extern Il2CppGenericClass IEnumerator_1_t5905_GenericClass;
TypeInfo IEnumerator_1_t5905_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5905_MethodInfos/* methods */
	, IEnumerator_1_t5905_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5905_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5905_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5905_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5905_0_0_0/* byval_arg */
	, &IEnumerator_1_t5905_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5905_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_112.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2955_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_112MethodDeclarations.h"

extern TypeInfo UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15210_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUserDefinedTargetBuildingBehaviour_t81_m32748_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.UserDefinedTargetBuildingBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.UserDefinedTargetBuildingBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisUserDefinedTargetBuildingBehaviour_t81_m32748(__this, p0, method) (UserDefinedTargetBuildingBehaviour_t81 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2955____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2955, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2955____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2955, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2955_FieldInfos[] =
{
	&InternalEnumerator_1_t2955____array_0_FieldInfo,
	&InternalEnumerator_1_t2955____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15207_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2955____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2955_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15207_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2955____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2955_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15210_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2955_PropertyInfos[] =
{
	&InternalEnumerator_1_t2955____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2955____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2955_InternalEnumerator_1__ctor_m15206_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15206_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15206_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2955_InternalEnumerator_1__ctor_m15206_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15206_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15207_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15207_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15207_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15208_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15208_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15208_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15209_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15209_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15209_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15210_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15210_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* declaring_type */
	, &UserDefinedTargetBuildingBehaviour_t81_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15210_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2955_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15206_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15207_MethodInfo,
	&InternalEnumerator_1_Dispose_m15208_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15209_MethodInfo,
	&InternalEnumerator_1_get_Current_m15210_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15209_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15208_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2955_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15207_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15209_MethodInfo,
	&InternalEnumerator_1_Dispose_m15208_MethodInfo,
	&InternalEnumerator_1_get_Current_m15210_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2955_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5905_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2955_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5905_il2cpp_TypeInfo, 7},
};
extern TypeInfo UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2955_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15210_MethodInfo/* Method Usage */,
	&UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisUserDefinedTargetBuildingBehaviour_t81_m32748_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2955_0_0_0;
extern Il2CppType InternalEnumerator_1_t2955_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2955_GenericClass;
TypeInfo InternalEnumerator_1_t2955_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2955_MethodInfos/* methods */
	, InternalEnumerator_1_t2955_PropertyInfos/* properties */
	, InternalEnumerator_1_t2955_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2955_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2955_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2955_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2955_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2955_1_0_0/* this_arg */
	, InternalEnumerator_1_t2955_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2955_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2955_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2955)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7524_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern MethodInfo ICollection_1_get_Count_m42662_MethodInfo;
static PropertyInfo ICollection_1_t7524____Count_PropertyInfo = 
{
	&ICollection_1_t7524_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42662_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42663_MethodInfo;
static PropertyInfo ICollection_1_t7524____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7524_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42663_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7524_PropertyInfos[] =
{
	&ICollection_1_t7524____Count_PropertyInfo,
	&ICollection_1_t7524____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42662_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42662_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7524_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42662_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42663_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42663_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7524_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42663_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_0_0_0;
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_0_0_0;
static ParameterInfo ICollection_1_t7524_ICollection_1_Add_m42664_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t81_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42664_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42664_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7524_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7524_ICollection_1_Add_m42664_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42664_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42665_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42665_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7524_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42665_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_0_0_0;
static ParameterInfo ICollection_1_t7524_ICollection_1_Contains_m42666_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t81_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42666_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42666_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7524_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7524_ICollection_1_Contains_m42666_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42666_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingBehaviourU5BU5D_t5194_0_0_0;
extern Il2CppType UserDefinedTargetBuildingBehaviourU5BU5D_t5194_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7524_ICollection_1_CopyTo_m42667_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviourU5BU5D_t5194_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42667_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42667_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7524_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7524_ICollection_1_CopyTo_m42667_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42667_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_0_0_0;
static ParameterInfo ICollection_1_t7524_ICollection_1_Remove_m42668_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t81_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42668_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42668_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7524_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7524_ICollection_1_Remove_m42668_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42668_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7524_MethodInfos[] =
{
	&ICollection_1_get_Count_m42662_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42663_MethodInfo,
	&ICollection_1_Add_m42664_MethodInfo,
	&ICollection_1_Clear_m42665_MethodInfo,
	&ICollection_1_Contains_m42666_MethodInfo,
	&ICollection_1_CopyTo_m42667_MethodInfo,
	&ICollection_1_Remove_m42668_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7526_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7524_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7526_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7524_0_0_0;
extern Il2CppType ICollection_1_t7524_1_0_0;
struct ICollection_1_t7524;
extern Il2CppGenericClass ICollection_1_t7524_GenericClass;
TypeInfo ICollection_1_t7524_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7524_MethodInfos/* methods */
	, ICollection_1_t7524_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7524_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7524_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7524_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7524_0_0_0/* byval_arg */
	, &ICollection_1_t7524_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7524_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.UserDefinedTargetBuildingBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern Il2CppType IEnumerator_1_t5905_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42669_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.UserDefinedTargetBuildingBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42669_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7526_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5905_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42669_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7526_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42669_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7526_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7526_0_0_0;
extern Il2CppType IEnumerable_1_t7526_1_0_0;
struct IEnumerable_1_t7526;
extern Il2CppGenericClass IEnumerable_1_t7526_GenericClass;
TypeInfo IEnumerable_1_t7526_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7526_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7526_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7526_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7526_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7526_0_0_0/* byval_arg */
	, &IEnumerable_1_t7526_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7526_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7525_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern MethodInfo IList_1_get_Item_m42670_MethodInfo;
extern MethodInfo IList_1_set_Item_m42671_MethodInfo;
static PropertyInfo IList_1_t7525____Item_PropertyInfo = 
{
	&IList_1_t7525_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42670_MethodInfo/* get */
	, &IList_1_set_Item_m42671_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7525_PropertyInfos[] =
{
	&IList_1_t7525____Item_PropertyInfo,
	NULL
};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_0_0_0;
static ParameterInfo IList_1_t7525_IList_1_IndexOf_m42672_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t81_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42672_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42672_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7525_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7525_IList_1_IndexOf_m42672_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42672_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_0_0_0;
static ParameterInfo IList_1_t7525_IList_1_Insert_m42673_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t81_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42673_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42673_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7525_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7525_IList_1_Insert_m42673_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42673_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7525_IList_1_RemoveAt_m42674_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42674_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42674_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7525_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7525_IList_1_RemoveAt_m42674_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42674_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7525_IList_1_get_Item_m42670_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42670_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42670_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7525_il2cpp_TypeInfo/* declaring_type */
	, &UserDefinedTargetBuildingBehaviour_t81_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7525_IList_1_get_Item_m42670_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42670_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_0_0_0;
static ParameterInfo IList_1_t7525_IList_1_set_Item_m42671_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t81_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42671_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42671_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7525_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7525_IList_1_set_Item_m42671_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42671_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7525_MethodInfos[] =
{
	&IList_1_IndexOf_m42672_MethodInfo,
	&IList_1_Insert_m42673_MethodInfo,
	&IList_1_RemoveAt_m42674_MethodInfo,
	&IList_1_get_Item_m42670_MethodInfo,
	&IList_1_set_Item_m42671_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7525_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7524_il2cpp_TypeInfo,
	&IEnumerable_1_t7526_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7525_0_0_0;
extern Il2CppType IList_1_t7525_1_0_0;
struct IList_1_t7525;
extern Il2CppGenericClass IList_1_t7525_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7525_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7525_MethodInfos/* methods */
	, IList_1_t7525_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7525_il2cpp_TypeInfo/* element_class */
	, IList_1_t7525_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7525_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7525_0_0_0/* byval_arg */
	, &IList_1_t7525_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7525_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7527_il2cpp_TypeInfo;

// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBu.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42675_MethodInfo;
static PropertyInfo ICollection_1_t7527____Count_PropertyInfo = 
{
	&ICollection_1_t7527_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42675_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42676_MethodInfo;
static PropertyInfo ICollection_1_t7527____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7527_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42676_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7527_PropertyInfos[] =
{
	&ICollection_1_t7527____Count_PropertyInfo,
	&ICollection_1_t7527____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42675_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42675_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7527_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42675_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42676_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42676_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7527_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42676_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0;
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0;
static ParameterInfo ICollection_1_t7527_ICollection_1_Add_m42677_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42677_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42677_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7527_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7527_ICollection_1_Add_m42677_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42677_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42678_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42678_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7527_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42678_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0;
static ParameterInfo ICollection_1_t7527_ICollection_1_Contains_m42679_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42679_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42679_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7527_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7527_ICollection_1_Contains_m42679_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42679_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviourU5BU5D_t5520_0_0_0;
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviourU5BU5D_t5520_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7527_ICollection_1_CopyTo_m42680_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingAbstractBehaviourU5BU5D_t5520_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42680_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42680_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7527_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7527_ICollection_1_CopyTo_m42680_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42680_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0;
static ParameterInfo ICollection_1_t7527_ICollection_1_Remove_m42681_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42681_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42681_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7527_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7527_ICollection_1_Remove_m42681_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42681_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7527_MethodInfos[] =
{
	&ICollection_1_get_Count_m42675_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42676_MethodInfo,
	&ICollection_1_Add_m42677_MethodInfo,
	&ICollection_1_Clear_m42678_MethodInfo,
	&ICollection_1_Contains_m42679_MethodInfo,
	&ICollection_1_CopyTo_m42680_MethodInfo,
	&ICollection_1_Remove_m42681_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7529_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7527_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7529_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7527_0_0_0;
extern Il2CppType ICollection_1_t7527_1_0_0;
struct ICollection_1_t7527;
extern Il2CppGenericClass ICollection_1_t7527_GenericClass;
TypeInfo ICollection_1_t7527_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7527_MethodInfos/* methods */
	, ICollection_1_t7527_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7527_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7527_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7527_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7527_0_0_0/* byval_arg */
	, &ICollection_1_t7527_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7527_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5907_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42682_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42682_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7529_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5907_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42682_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7529_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42682_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7529_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7529_0_0_0;
extern Il2CppType IEnumerable_1_t7529_1_0_0;
struct IEnumerable_1_t7529;
extern Il2CppGenericClass IEnumerable_1_t7529_GenericClass;
TypeInfo IEnumerable_1_t7529_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7529_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7529_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7529_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7529_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7529_0_0_0/* byval_arg */
	, &IEnumerable_1_t7529_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7529_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5907_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42683_MethodInfo;
static PropertyInfo IEnumerator_1_t5907____Current_PropertyInfo = 
{
	&IEnumerator_1_t5907_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42683_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5907_PropertyInfos[] =
{
	&IEnumerator_1_t5907____Current_PropertyInfo,
	NULL
};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42683_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42683_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5907_il2cpp_TypeInfo/* declaring_type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42683_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5907_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42683_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5907_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5907_0_0_0;
extern Il2CppType IEnumerator_1_t5907_1_0_0;
struct IEnumerator_1_t5907;
extern Il2CppGenericClass IEnumerator_1_t5907_GenericClass;
TypeInfo IEnumerator_1_t5907_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5907_MethodInfos/* methods */
	, IEnumerator_1_t5907_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5907_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5907_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5907_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5907_0_0_0/* byval_arg */
	, &IEnumerator_1_t5907_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5907_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_113.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2956_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_113MethodDeclarations.h"

extern TypeInfo UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15215_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUserDefinedTargetBuildingAbstractBehaviour_t82_m32759_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisUserDefinedTargetBuildingAbstractBehaviour_t82_m32759(__this, p0, method) (UserDefinedTargetBuildingAbstractBehaviour_t82 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2956____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2956_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2956, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2956____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2956_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2956, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2956_FieldInfos[] =
{
	&InternalEnumerator_1_t2956____array_0_FieldInfo,
	&InternalEnumerator_1_t2956____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15212_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2956____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2956_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15212_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2956____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2956_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15215_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2956_PropertyInfos[] =
{
	&InternalEnumerator_1_t2956____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2956____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2956_InternalEnumerator_1__ctor_m15211_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15211_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15211_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2956_InternalEnumerator_1__ctor_m15211_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15211_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15212_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15212_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2956_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15212_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15213_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15213_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15213_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15214_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15214_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2956_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15214_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15215_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15215_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2956_il2cpp_TypeInfo/* declaring_type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15215_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2956_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15211_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15212_MethodInfo,
	&InternalEnumerator_1_Dispose_m15213_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15214_MethodInfo,
	&InternalEnumerator_1_get_Current_m15215_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15214_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15213_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2956_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15212_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15214_MethodInfo,
	&InternalEnumerator_1_Dispose_m15213_MethodInfo,
	&InternalEnumerator_1_get_Current_m15215_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2956_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5907_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2956_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5907_il2cpp_TypeInfo, 7},
};
extern TypeInfo UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2956_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15215_MethodInfo/* Method Usage */,
	&UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisUserDefinedTargetBuildingAbstractBehaviour_t82_m32759_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2956_0_0_0;
extern Il2CppType InternalEnumerator_1_t2956_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2956_GenericClass;
TypeInfo InternalEnumerator_1_t2956_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2956_MethodInfos/* methods */
	, InternalEnumerator_1_t2956_PropertyInfos/* properties */
	, InternalEnumerator_1_t2956_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2956_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2956_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2956_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2956_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2956_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2956_1_0_0/* this_arg */
	, InternalEnumerator_1_t2956_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2956_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2956_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2956)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7528_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42684_MethodInfo;
extern MethodInfo IList_1_set_Item_m42685_MethodInfo;
static PropertyInfo IList_1_t7528____Item_PropertyInfo = 
{
	&IList_1_t7528_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42684_MethodInfo/* get */
	, &IList_1_set_Item_m42685_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7528_PropertyInfos[] =
{
	&IList_1_t7528____Item_PropertyInfo,
	NULL
};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0;
static ParameterInfo IList_1_t7528_IList_1_IndexOf_m42686_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42686_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42686_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7528_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7528_IList_1_IndexOf_m42686_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42686_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0;
static ParameterInfo IList_1_t7528_IList_1_Insert_m42687_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42687_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42687_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7528_IList_1_Insert_m42687_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42687_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7528_IList_1_RemoveAt_m42688_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42688_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42688_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7528_IList_1_RemoveAt_m42688_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42688_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7528_IList_1_get_Item_m42684_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42684_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42684_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7528_il2cpp_TypeInfo/* declaring_type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7528_IList_1_get_Item_m42684_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42684_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0;
static ParameterInfo IList_1_t7528_IList_1_set_Item_m42685_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42685_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42685_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7528_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7528_IList_1_set_Item_m42685_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42685_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7528_MethodInfos[] =
{
	&IList_1_IndexOf_m42686_MethodInfo,
	&IList_1_Insert_m42687_MethodInfo,
	&IList_1_RemoveAt_m42688_MethodInfo,
	&IList_1_get_Item_m42684_MethodInfo,
	&IList_1_set_Item_m42685_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7528_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7527_il2cpp_TypeInfo,
	&IEnumerable_1_t7529_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7528_0_0_0;
extern Il2CppType IList_1_t7528_1_0_0;
struct IList_1_t7528;
extern Il2CppGenericClass IList_1_t7528_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7528_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7528_MethodInfos/* methods */
	, IList_1_t7528_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7528_il2cpp_TypeInfo/* element_class */
	, IList_1_t7528_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7528_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7528_0_0_0/* byval_arg */
	, &IList_1_t7528_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7528_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_40.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2957_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_40MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_36.h"
extern TypeInfo InvokableCall_1_t2958_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_36MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15218_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15220_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2957____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2957_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2957, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2957_FieldInfos[] =
{
	&CachedInvokableCall_1_t2957____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2957_CachedInvokableCall_1__ctor_m15216_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t81_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15216_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15216_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2957_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2957_CachedInvokableCall_1__ctor_m15216_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15216_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2957_CachedInvokableCall_1_Invoke_m15217_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15217_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15217_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2957_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2957_CachedInvokableCall_1_Invoke_m15217_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15217_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2957_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15216_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15217_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15217_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15221_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2957_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15217_MethodInfo,
	&InvokableCall_1_Find_m15221_MethodInfo,
};
extern Il2CppType UnityAction_1_t2959_0_0_0;
extern TypeInfo UnityAction_1_t2959_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisUserDefinedTargetBuildingBehaviour_t81_m32769_MethodInfo;
extern TypeInfo UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15223_MethodInfo;
extern TypeInfo UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2957_RGCTXData[8] = 
{
	&UnityAction_1_t2959_0_0_0/* Type Usage */,
	&UnityAction_1_t2959_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisUserDefinedTargetBuildingBehaviour_t81_m32769_MethodInfo/* Method Usage */,
	&UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15223_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15218_MethodInfo/* Method Usage */,
	&UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15220_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2957_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2957_1_0_0;
struct CachedInvokableCall_1_t2957;
extern Il2CppGenericClass CachedInvokableCall_1_t2957_GenericClass;
TypeInfo CachedInvokableCall_1_t2957_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2957_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2957_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2958_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2957_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2957_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2957_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2957_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2957_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2957_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2957_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2957)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_43.h"
extern TypeInfo UnityAction_1_t2959_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_43MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.UserDefinedTargetBuildingBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.UserDefinedTargetBuildingBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisUserDefinedTargetBuildingBehaviour_t81_m32769(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern Il2CppType UnityAction_1_t2959_0_0_1;
FieldInfo InvokableCall_1_t2958____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2959_0_0_1/* type */
	, &InvokableCall_1_t2958_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2958, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2958_FieldInfos[] =
{
	&InvokableCall_1_t2958____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2958_InvokableCall_1__ctor_m15218_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15218_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15218_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2958_InvokableCall_1__ctor_m15218_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15218_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2959_0_0_0;
static ParameterInfo InvokableCall_1_t2958_InvokableCall_1__ctor_m15219_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2959_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15219_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15219_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2958_InvokableCall_1__ctor_m15219_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15219_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2958_InvokableCall_1_Invoke_m15220_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15220_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15220_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2958_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2958_InvokableCall_1_Invoke_m15220_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15220_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2958_InvokableCall_1_Find_m15221_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15221_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15221_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2958_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2958_InvokableCall_1_Find_m15221_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15221_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2958_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15218_MethodInfo,
	&InvokableCall_1__ctor_m15219_MethodInfo,
	&InvokableCall_1_Invoke_m15220_MethodInfo,
	&InvokableCall_1_Find_m15221_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2958_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m15220_MethodInfo,
	&InvokableCall_1_Find_m15221_MethodInfo,
};
extern TypeInfo UnityAction_1_t2959_il2cpp_TypeInfo;
extern TypeInfo UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2958_RGCTXData[5] = 
{
	&UnityAction_1_t2959_0_0_0/* Type Usage */,
	&UnityAction_1_t2959_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisUserDefinedTargetBuildingBehaviour_t81_m32769_MethodInfo/* Method Usage */,
	&UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15223_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2958_0_0_0;
extern Il2CppType InvokableCall_1_t2958_1_0_0;
struct InvokableCall_1_t2958;
extern Il2CppGenericClass InvokableCall_1_t2958_GenericClass;
TypeInfo InvokableCall_1_t2958_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2958_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2958_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2958_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2958_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2958_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2958_0_0_0/* byval_arg */
	, &InvokableCall_1_t2958_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2958_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2958_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2958)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2959_UnityAction_1__ctor_m15222_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15222_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15222_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2959_UnityAction_1__ctor_m15222_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15222_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_0_0_0;
static ParameterInfo UnityAction_1_t2959_UnityAction_1_Invoke_m15223_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t81_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15223_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15223_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2959_UnityAction_1_Invoke_m15223_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15223_GenericMethod/* genericMethod */

};
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2959_UnityAction_1_BeginInvoke_m15224_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &UserDefinedTargetBuildingBehaviour_t81_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15224_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15224_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2959_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2959_UnityAction_1_BeginInvoke_m15224_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15224_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2959_UnityAction_1_EndInvoke_m15225_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15225_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15225_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2959_UnityAction_1_EndInvoke_m15225_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15225_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2959_MethodInfos[] =
{
	&UnityAction_1__ctor_m15222_MethodInfo,
	&UnityAction_1_Invoke_m15223_MethodInfo,
	&UnityAction_1_BeginInvoke_m15224_MethodInfo,
	&UnityAction_1_EndInvoke_m15225_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15224_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15225_MethodInfo;
static MethodInfo* UnityAction_1_t2959_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m15223_MethodInfo,
	&UnityAction_1_BeginInvoke_m15224_MethodInfo,
	&UnityAction_1_EndInvoke_m15225_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2959_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2959_1_0_0;
struct UnityAction_1_t2959;
extern Il2CppGenericClass UnityAction_1_t2959_GenericClass;
TypeInfo UnityAction_1_t2959_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2959_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2959_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2959_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2959_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2959_0_0_0/* byval_arg */
	, &UnityAction_1_t2959_1_0_0/* this_arg */
	, UnityAction_1_t2959_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2959_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2959)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5909_il2cpp_TypeInfo;

// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoBackgroundBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.VideoBackgroundBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42689_MethodInfo;
static PropertyInfo IEnumerator_1_t5909____Current_PropertyInfo = 
{
	&IEnumerator_1_t5909_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42689_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5909_PropertyInfos[] =
{
	&IEnumerator_1_t5909____Current_PropertyInfo,
	NULL
};
extern Il2CppType VideoBackgroundBehaviour_t83_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42689_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoBackgroundBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42689_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5909_il2cpp_TypeInfo/* declaring_type */
	, &VideoBackgroundBehaviour_t83_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42689_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5909_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42689_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5909_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5909_0_0_0;
extern Il2CppType IEnumerator_1_t5909_1_0_0;
struct IEnumerator_1_t5909;
extern Il2CppGenericClass IEnumerator_1_t5909_GenericClass;
TypeInfo IEnumerator_1_t5909_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5909_MethodInfos/* methods */
	, IEnumerator_1_t5909_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5909_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5909_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5909_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5909_0_0_0/* byval_arg */
	, &IEnumerator_1_t5909_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5909_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_114.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2960_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_114MethodDeclarations.h"

extern TypeInfo VideoBackgroundBehaviour_t83_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15230_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVideoBackgroundBehaviour_t83_m32771_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.VideoBackgroundBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VideoBackgroundBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisVideoBackgroundBehaviour_t83_m32771(__this, p0, method) (VideoBackgroundBehaviour_t83 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2960____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2960, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2960____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2960, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2960_FieldInfos[] =
{
	&InternalEnumerator_1_t2960____array_0_FieldInfo,
	&InternalEnumerator_1_t2960____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15227_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2960____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2960_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15227_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2960____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2960_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15230_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2960_PropertyInfos[] =
{
	&InternalEnumerator_1_t2960____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2960____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2960_InternalEnumerator_1__ctor_m15226_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15226_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15226_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2960_InternalEnumerator_1__ctor_m15226_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15226_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15227_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15227_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15227_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15228_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15228_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15228_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15229_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15229_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15229_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundBehaviour_t83_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15230_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15230_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* declaring_type */
	, &VideoBackgroundBehaviour_t83_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15230_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2960_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15226_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15227_MethodInfo,
	&InternalEnumerator_1_Dispose_m15228_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15229_MethodInfo,
	&InternalEnumerator_1_get_Current_m15230_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15229_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15228_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2960_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15227_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15229_MethodInfo,
	&InternalEnumerator_1_Dispose_m15228_MethodInfo,
	&InternalEnumerator_1_get_Current_m15230_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2960_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5909_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2960_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5909_il2cpp_TypeInfo, 7},
};
extern TypeInfo VideoBackgroundBehaviour_t83_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2960_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15230_MethodInfo/* Method Usage */,
	&VideoBackgroundBehaviour_t83_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisVideoBackgroundBehaviour_t83_m32771_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2960_0_0_0;
extern Il2CppType InternalEnumerator_1_t2960_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2960_GenericClass;
TypeInfo InternalEnumerator_1_t2960_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2960_MethodInfos/* methods */
	, InternalEnumerator_1_t2960_PropertyInfos/* properties */
	, InternalEnumerator_1_t2960_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2960_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2960_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2960_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2960_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2960_1_0_0/* this_arg */
	, InternalEnumerator_1_t2960_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2960_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2960_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2960)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7530_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>
extern MethodInfo ICollection_1_get_Count_m42690_MethodInfo;
static PropertyInfo ICollection_1_t7530____Count_PropertyInfo = 
{
	&ICollection_1_t7530_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42690_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42691_MethodInfo;
static PropertyInfo ICollection_1_t7530____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7530_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42691_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7530_PropertyInfos[] =
{
	&ICollection_1_t7530____Count_PropertyInfo,
	&ICollection_1_t7530____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42690_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42690_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7530_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42690_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42691_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42691_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7530_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42691_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundBehaviour_t83_0_0_0;
extern Il2CppType VideoBackgroundBehaviour_t83_0_0_0;
static ParameterInfo ICollection_1_t7530_ICollection_1_Add_m42692_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t83_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42692_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42692_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7530_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7530_ICollection_1_Add_m42692_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42692_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42693_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42693_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7530_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42693_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundBehaviour_t83_0_0_0;
static ParameterInfo ICollection_1_t7530_ICollection_1_Contains_m42694_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t83_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42694_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42694_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7530_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7530_ICollection_1_Contains_m42694_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42694_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundBehaviourU5BU5D_t5195_0_0_0;
extern Il2CppType VideoBackgroundBehaviourU5BU5D_t5195_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7530_ICollection_1_CopyTo_m42695_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviourU5BU5D_t5195_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42695_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42695_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7530_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7530_ICollection_1_CopyTo_m42695_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42695_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundBehaviour_t83_0_0_0;
static ParameterInfo ICollection_1_t7530_ICollection_1_Remove_m42696_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t83_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42696_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42696_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7530_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7530_ICollection_1_Remove_m42696_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42696_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7530_MethodInfos[] =
{
	&ICollection_1_get_Count_m42690_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42691_MethodInfo,
	&ICollection_1_Add_m42692_MethodInfo,
	&ICollection_1_Clear_m42693_MethodInfo,
	&ICollection_1_Contains_m42694_MethodInfo,
	&ICollection_1_CopyTo_m42695_MethodInfo,
	&ICollection_1_Remove_m42696_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7532_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7530_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7532_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7530_0_0_0;
extern Il2CppType ICollection_1_t7530_1_0_0;
struct ICollection_1_t7530;
extern Il2CppGenericClass ICollection_1_t7530_GenericClass;
TypeInfo ICollection_1_t7530_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7530_MethodInfos/* methods */
	, ICollection_1_t7530_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7530_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7530_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7530_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7530_0_0_0/* byval_arg */
	, &ICollection_1_t7530_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7530_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoBackgroundBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.VideoBackgroundBehaviour>
extern Il2CppType IEnumerator_1_t5909_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42697_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoBackgroundBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42697_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7532_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5909_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42697_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7532_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42697_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7532_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7532_0_0_0;
extern Il2CppType IEnumerable_1_t7532_1_0_0;
struct IEnumerable_1_t7532;
extern Il2CppGenericClass IEnumerable_1_t7532_GenericClass;
TypeInfo IEnumerable_1_t7532_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7532_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7532_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7532_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7532_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7532_0_0_0/* byval_arg */
	, &IEnumerable_1_t7532_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7532_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7531_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>
extern MethodInfo IList_1_get_Item_m42698_MethodInfo;
extern MethodInfo IList_1_set_Item_m42699_MethodInfo;
static PropertyInfo IList_1_t7531____Item_PropertyInfo = 
{
	&IList_1_t7531_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42698_MethodInfo/* get */
	, &IList_1_set_Item_m42699_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7531_PropertyInfos[] =
{
	&IList_1_t7531____Item_PropertyInfo,
	NULL
};
extern Il2CppType VideoBackgroundBehaviour_t83_0_0_0;
static ParameterInfo IList_1_t7531_IList_1_IndexOf_m42700_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t83_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42700_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42700_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7531_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7531_IList_1_IndexOf_m42700_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42700_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType VideoBackgroundBehaviour_t83_0_0_0;
static ParameterInfo IList_1_t7531_IList_1_Insert_m42701_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t83_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42701_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42701_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7531_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7531_IList_1_Insert_m42701_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42701_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7531_IList_1_RemoveAt_m42702_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42702_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42702_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7531_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7531_IList_1_RemoveAt_m42702_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42702_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7531_IList_1_get_Item_m42698_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType VideoBackgroundBehaviour_t83_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42698_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42698_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7531_il2cpp_TypeInfo/* declaring_type */
	, &VideoBackgroundBehaviour_t83_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7531_IList_1_get_Item_m42698_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42698_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType VideoBackgroundBehaviour_t83_0_0_0;
static ParameterInfo IList_1_t7531_IList_1_set_Item_m42699_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t83_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42699_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42699_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7531_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7531_IList_1_set_Item_m42699_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42699_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7531_MethodInfos[] =
{
	&IList_1_IndexOf_m42700_MethodInfo,
	&IList_1_Insert_m42701_MethodInfo,
	&IList_1_RemoveAt_m42702_MethodInfo,
	&IList_1_get_Item_m42698_MethodInfo,
	&IList_1_set_Item_m42699_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7531_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7530_il2cpp_TypeInfo,
	&IEnumerable_1_t7532_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7531_0_0_0;
extern Il2CppType IList_1_t7531_1_0_0;
struct IList_1_t7531;
extern Il2CppGenericClass IList_1_t7531_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7531_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7531_MethodInfos/* methods */
	, IList_1_t7531_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7531_il2cpp_TypeInfo/* element_class */
	, IList_1_t7531_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7531_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7531_0_0_0/* byval_arg */
	, &IList_1_t7531_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7531_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7533_il2cpp_TypeInfo;

// Vuforia.VideoBackgroundAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42703_MethodInfo;
static PropertyInfo ICollection_1_t7533____Count_PropertyInfo = 
{
	&ICollection_1_t7533_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42703_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42704_MethodInfo;
static PropertyInfo ICollection_1_t7533____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7533_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42704_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7533_PropertyInfos[] =
{
	&ICollection_1_t7533____Count_PropertyInfo,
	&ICollection_1_t7533____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42703_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42703_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42703_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42704_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42704_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42704_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundAbstractBehaviour_t84_0_0_0;
extern Il2CppType VideoBackgroundAbstractBehaviour_t84_0_0_0;
static ParameterInfo ICollection_1_t7533_ICollection_1_Add_m42705_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundAbstractBehaviour_t84_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42705_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42705_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7533_ICollection_1_Add_m42705_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42705_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42706_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42706_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42706_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundAbstractBehaviour_t84_0_0_0;
static ParameterInfo ICollection_1_t7533_ICollection_1_Contains_m42707_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundAbstractBehaviour_t84_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42707_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42707_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7533_ICollection_1_Contains_m42707_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42707_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundAbstractBehaviourU5BU5D_t759_0_0_0;
extern Il2CppType VideoBackgroundAbstractBehaviourU5BU5D_t759_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7533_ICollection_1_CopyTo_m42708_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundAbstractBehaviourU5BU5D_t759_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42708_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42708_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7533_ICollection_1_CopyTo_m42708_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42708_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundAbstractBehaviour_t84_0_0_0;
static ParameterInfo ICollection_1_t7533_ICollection_1_Remove_m42709_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundAbstractBehaviour_t84_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42709_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoBackgroundAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42709_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7533_ICollection_1_Remove_m42709_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42709_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7533_MethodInfos[] =
{
	&ICollection_1_get_Count_m42703_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42704_MethodInfo,
	&ICollection_1_Add_m42705_MethodInfo,
	&ICollection_1_Clear_m42706_MethodInfo,
	&ICollection_1_Contains_m42707_MethodInfo,
	&ICollection_1_CopyTo_m42708_MethodInfo,
	&ICollection_1_Remove_m42709_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7535_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7533_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7535_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7533_0_0_0;
extern Il2CppType ICollection_1_t7533_1_0_0;
struct ICollection_1_t7533;
extern Il2CppGenericClass ICollection_1_t7533_GenericClass;
TypeInfo ICollection_1_t7533_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7533_MethodInfos/* methods */
	, ICollection_1_t7533_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7533_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7533_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7533_0_0_0/* byval_arg */
	, &ICollection_1_t7533_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7533_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoBackgroundAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.VideoBackgroundAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5911_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42710_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoBackgroundAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42710_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7535_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5911_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42710_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7535_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42710_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7535_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7535_0_0_0;
extern Il2CppType IEnumerable_1_t7535_1_0_0;
struct IEnumerable_1_t7535;
extern Il2CppGenericClass IEnumerable_1_t7535_GenericClass;
TypeInfo IEnumerable_1_t7535_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7535_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7535_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7535_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7535_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7535_0_0_0/* byval_arg */
	, &IEnumerable_1_t7535_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7535_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5911_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42711_MethodInfo;
static PropertyInfo IEnumerator_1_t5911____Current_PropertyInfo = 
{
	&IEnumerator_1_t5911_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42711_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5911_PropertyInfos[] =
{
	&IEnumerator_1_t5911____Current_PropertyInfo,
	NULL
};
extern Il2CppType VideoBackgroundAbstractBehaviour_t84_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42711_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42711_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5911_il2cpp_TypeInfo/* declaring_type */
	, &VideoBackgroundAbstractBehaviour_t84_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42711_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5911_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42711_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5911_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5911_0_0_0;
extern Il2CppType IEnumerator_1_t5911_1_0_0;
struct IEnumerator_1_t5911;
extern Il2CppGenericClass IEnumerator_1_t5911_GenericClass;
TypeInfo IEnumerator_1_t5911_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5911_MethodInfos/* methods */
	, IEnumerator_1_t5911_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5911_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5911_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5911_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5911_0_0_0/* byval_arg */
	, &IEnumerator_1_t5911_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5911_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_115.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2961_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_115MethodDeclarations.h"

extern TypeInfo VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15235_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVideoBackgroundAbstractBehaviour_t84_m32782_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.VideoBackgroundAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VideoBackgroundAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisVideoBackgroundAbstractBehaviour_t84_m32782(__this, p0, method) (VideoBackgroundAbstractBehaviour_t84 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2961____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2961_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2961, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2961____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2961_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2961, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2961_FieldInfos[] =
{
	&InternalEnumerator_1_t2961____array_0_FieldInfo,
	&InternalEnumerator_1_t2961____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15232_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2961____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2961_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15232_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2961____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2961_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15235_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2961_PropertyInfos[] =
{
	&InternalEnumerator_1_t2961____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2961____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2961_InternalEnumerator_1__ctor_m15231_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15231_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15231_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2961_InternalEnumerator_1__ctor_m15231_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15231_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15232_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15232_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2961_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15232_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15233_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15233_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2961_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15233_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15234_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15234_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2961_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15234_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundAbstractBehaviour_t84_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15235_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15235_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2961_il2cpp_TypeInfo/* declaring_type */
	, &VideoBackgroundAbstractBehaviour_t84_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15235_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2961_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15231_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15232_MethodInfo,
	&InternalEnumerator_1_Dispose_m15233_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15234_MethodInfo,
	&InternalEnumerator_1_get_Current_m15235_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15234_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15233_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2961_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15232_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15234_MethodInfo,
	&InternalEnumerator_1_Dispose_m15233_MethodInfo,
	&InternalEnumerator_1_get_Current_m15235_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2961_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5911_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2961_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5911_il2cpp_TypeInfo, 7},
};
extern TypeInfo VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2961_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15235_MethodInfo/* Method Usage */,
	&VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisVideoBackgroundAbstractBehaviour_t84_m32782_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2961_0_0_0;
extern Il2CppType InternalEnumerator_1_t2961_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2961_GenericClass;
TypeInfo InternalEnumerator_1_t2961_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2961_MethodInfos/* methods */
	, InternalEnumerator_1_t2961_PropertyInfos/* properties */
	, InternalEnumerator_1_t2961_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2961_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2961_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2961_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2961_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2961_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2961_1_0_0/* this_arg */
	, InternalEnumerator_1_t2961_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2961_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2961_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2961)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7534_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42712_MethodInfo;
extern MethodInfo IList_1_set_Item_m42713_MethodInfo;
static PropertyInfo IList_1_t7534____Item_PropertyInfo = 
{
	&IList_1_t7534_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42712_MethodInfo/* get */
	, &IList_1_set_Item_m42713_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7534_PropertyInfos[] =
{
	&IList_1_t7534____Item_PropertyInfo,
	NULL
};
extern Il2CppType VideoBackgroundAbstractBehaviour_t84_0_0_0;
static ParameterInfo IList_1_t7534_IList_1_IndexOf_m42714_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundAbstractBehaviour_t84_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42714_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42714_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7534_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7534_IList_1_IndexOf_m42714_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42714_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType VideoBackgroundAbstractBehaviour_t84_0_0_0;
static ParameterInfo IList_1_t7534_IList_1_Insert_m42715_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundAbstractBehaviour_t84_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42715_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42715_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7534_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7534_IList_1_Insert_m42715_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42715_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7534_IList_1_RemoveAt_m42716_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42716_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42716_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7534_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7534_IList_1_RemoveAt_m42716_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42716_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7534_IList_1_get_Item_m42712_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType VideoBackgroundAbstractBehaviour_t84_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42712_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42712_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7534_il2cpp_TypeInfo/* declaring_type */
	, &VideoBackgroundAbstractBehaviour_t84_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7534_IList_1_get_Item_m42712_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42712_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType VideoBackgroundAbstractBehaviour_t84_0_0_0;
static ParameterInfo IList_1_t7534_IList_1_set_Item_m42713_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundAbstractBehaviour_t84_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42713_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoBackgroundAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42713_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7534_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7534_IList_1_set_Item_m42713_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42713_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7534_MethodInfos[] =
{
	&IList_1_IndexOf_m42714_MethodInfo,
	&IList_1_Insert_m42715_MethodInfo,
	&IList_1_RemoveAt_m42716_MethodInfo,
	&IList_1_get_Item_m42712_MethodInfo,
	&IList_1_set_Item_m42713_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7534_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7533_il2cpp_TypeInfo,
	&IEnumerable_1_t7535_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7534_0_0_0;
extern Il2CppType IList_1_t7534_1_0_0;
struct IList_1_t7534;
extern Il2CppGenericClass IList_1_t7534_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7534_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7534_MethodInfos/* methods */
	, IList_1_t7534_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7534_il2cpp_TypeInfo/* element_class */
	, IList_1_t7534_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7534_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7534_0_0_0/* byval_arg */
	, &IList_1_t7534_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7534_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_41.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2962_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_41MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_37.h"
extern TypeInfo InvokableCall_1_t2963_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_37MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15238_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15240_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2962____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2962_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2962, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2962_FieldInfos[] =
{
	&CachedInvokableCall_1_t2962____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType VideoBackgroundBehaviour_t83_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2962_CachedInvokableCall_1__ctor_m15236_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t83_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15236_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15236_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2962_CachedInvokableCall_1__ctor_m15236_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15236_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2962_CachedInvokableCall_1_Invoke_m15237_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15237_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoBackgroundBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15237_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2962_CachedInvokableCall_1_Invoke_m15237_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15237_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2962_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15236_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15237_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15237_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15241_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2962_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15237_MethodInfo,
	&InvokableCall_1_Find_m15241_MethodInfo,
};
extern Il2CppType UnityAction_1_t2964_0_0_0;
extern TypeInfo UnityAction_1_t2964_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisVideoBackgroundBehaviour_t83_m32792_MethodInfo;
extern TypeInfo VideoBackgroundBehaviour_t83_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15243_MethodInfo;
extern TypeInfo VideoBackgroundBehaviour_t83_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2962_RGCTXData[8] = 
{
	&UnityAction_1_t2964_0_0_0/* Type Usage */,
	&UnityAction_1_t2964_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisVideoBackgroundBehaviour_t83_m32792_MethodInfo/* Method Usage */,
	&VideoBackgroundBehaviour_t83_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15243_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15238_MethodInfo/* Method Usage */,
	&VideoBackgroundBehaviour_t83_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15240_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2962_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2962_1_0_0;
struct CachedInvokableCall_1_t2962;
extern Il2CppGenericClass CachedInvokableCall_1_t2962_GenericClass;
TypeInfo CachedInvokableCall_1_t2962_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2962_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2962_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2963_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2962_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2962_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2962_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2962_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2962_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2962_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2962_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2962)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_44.h"
extern TypeInfo UnityAction_1_t2964_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_44MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.VideoBackgroundBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.VideoBackgroundBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisVideoBackgroundBehaviour_t83_m32792(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>
extern Il2CppType UnityAction_1_t2964_0_0_1;
FieldInfo InvokableCall_1_t2963____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2964_0_0_1/* type */
	, &InvokableCall_1_t2963_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2963, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2963_FieldInfos[] =
{
	&InvokableCall_1_t2963____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2963_InvokableCall_1__ctor_m15238_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15238_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15238_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2963_InvokableCall_1__ctor_m15238_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15238_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2964_0_0_0;
static ParameterInfo InvokableCall_1_t2963_InvokableCall_1__ctor_m15239_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2964_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15239_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15239_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2963_InvokableCall_1__ctor_m15239_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15239_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2963_InvokableCall_1_Invoke_m15240_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15240_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15240_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2963_InvokableCall_1_Invoke_m15240_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15240_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2963_InvokableCall_1_Find_m15241_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15241_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15241_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2963_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2963_InvokableCall_1_Find_m15241_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15241_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2963_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15238_MethodInfo,
	&InvokableCall_1__ctor_m15239_MethodInfo,
	&InvokableCall_1_Invoke_m15240_MethodInfo,
	&InvokableCall_1_Find_m15241_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2963_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m15240_MethodInfo,
	&InvokableCall_1_Find_m15241_MethodInfo,
};
extern TypeInfo UnityAction_1_t2964_il2cpp_TypeInfo;
extern TypeInfo VideoBackgroundBehaviour_t83_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2963_RGCTXData[5] = 
{
	&UnityAction_1_t2964_0_0_0/* Type Usage */,
	&UnityAction_1_t2964_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisVideoBackgroundBehaviour_t83_m32792_MethodInfo/* Method Usage */,
	&VideoBackgroundBehaviour_t83_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15243_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2963_0_0_0;
extern Il2CppType InvokableCall_1_t2963_1_0_0;
struct InvokableCall_1_t2963;
extern Il2CppGenericClass InvokableCall_1_t2963_GenericClass;
TypeInfo InvokableCall_1_t2963_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2963_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2963_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2963_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2963_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2963_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2963_0_0_0/* byval_arg */
	, &InvokableCall_1_t2963_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2963_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2963_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2963)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2964_UnityAction_1__ctor_m15242_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15242_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15242_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2964_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2964_UnityAction_1__ctor_m15242_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15242_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundBehaviour_t83_0_0_0;
static ParameterInfo UnityAction_1_t2964_UnityAction_1_Invoke_m15243_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t83_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15243_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15243_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2964_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2964_UnityAction_1_Invoke_m15243_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15243_GenericMethod/* genericMethod */

};
extern Il2CppType VideoBackgroundBehaviour_t83_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2964_UnityAction_1_BeginInvoke_m15244_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &VideoBackgroundBehaviour_t83_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15244_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15244_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2964_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2964_UnityAction_1_BeginInvoke_m15244_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15244_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2964_UnityAction_1_EndInvoke_m15245_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15245_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15245_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2964_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2964_UnityAction_1_EndInvoke_m15245_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15245_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2964_MethodInfos[] =
{
	&UnityAction_1__ctor_m15242_MethodInfo,
	&UnityAction_1_Invoke_m15243_MethodInfo,
	&UnityAction_1_BeginInvoke_m15244_MethodInfo,
	&UnityAction_1_EndInvoke_m15245_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15244_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15245_MethodInfo;
static MethodInfo* UnityAction_1_t2964_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m15243_MethodInfo,
	&UnityAction_1_BeginInvoke_m15244_MethodInfo,
	&UnityAction_1_EndInvoke_m15245_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2964_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2964_1_0_0;
struct UnityAction_1_t2964;
extern Il2CppGenericClass UnityAction_1_t2964_GenericClass;
TypeInfo UnityAction_1_t2964_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2964_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2964_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2964_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2964_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2964_0_0_0/* byval_arg */
	, &UnityAction_1_t2964_1_0_0/* this_arg */
	, UnityAction_1_t2964_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2964_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2964)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5913_il2cpp_TypeInfo;

// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoTextureRenderer>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.VideoTextureRenderer>
extern MethodInfo IEnumerator_1_get_Current_m42717_MethodInfo;
static PropertyInfo IEnumerator_1_t5913____Current_PropertyInfo = 
{
	&IEnumerator_1_t5913_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42717_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5913_PropertyInfos[] =
{
	&IEnumerator_1_t5913____Current_PropertyInfo,
	NULL
};
extern Il2CppType VideoTextureRenderer_t85_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42717_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoTextureRenderer>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42717_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5913_il2cpp_TypeInfo/* declaring_type */
	, &VideoTextureRenderer_t85_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42717_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5913_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42717_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5913_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5913_0_0_0;
extern Il2CppType IEnumerator_1_t5913_1_0_0;
struct IEnumerator_1_t5913;
extern Il2CppGenericClass IEnumerator_1_t5913_GenericClass;
TypeInfo IEnumerator_1_t5913_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5913_MethodInfos/* methods */
	, IEnumerator_1_t5913_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5913_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5913_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5913_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5913_0_0_0/* byval_arg */
	, &IEnumerator_1_t5913_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5913_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_116.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2965_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_116MethodDeclarations.h"

extern TypeInfo VideoTextureRenderer_t85_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15250_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVideoTextureRenderer_t85_m32794_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.VideoTextureRenderer>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VideoTextureRenderer>(System.Int32)
#define Array_InternalArray__get_Item_TisVideoTextureRenderer_t85_m32794(__this, p0, method) (VideoTextureRenderer_t85 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2965____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2965, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2965____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2965, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2965_FieldInfos[] =
{
	&InternalEnumerator_1_t2965____array_0_FieldInfo,
	&InternalEnumerator_1_t2965____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15247_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2965____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2965_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15247_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2965____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2965_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15250_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2965_PropertyInfos[] =
{
	&InternalEnumerator_1_t2965____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2965____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2965_InternalEnumerator_1__ctor_m15246_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15246_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15246_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2965_InternalEnumerator_1__ctor_m15246_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15246_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15247_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15247_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15247_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15248_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15248_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15248_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15249_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15249_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15249_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRenderer_t85_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15250_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15250_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* declaring_type */
	, &VideoTextureRenderer_t85_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15250_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2965_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15246_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15247_MethodInfo,
	&InternalEnumerator_1_Dispose_m15248_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15249_MethodInfo,
	&InternalEnumerator_1_get_Current_m15250_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15249_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15248_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2965_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15247_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15249_MethodInfo,
	&InternalEnumerator_1_Dispose_m15248_MethodInfo,
	&InternalEnumerator_1_get_Current_m15250_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2965_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5913_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2965_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5913_il2cpp_TypeInfo, 7},
};
extern TypeInfo VideoTextureRenderer_t85_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2965_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15250_MethodInfo/* Method Usage */,
	&VideoTextureRenderer_t85_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisVideoTextureRenderer_t85_m32794_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2965_0_0_0;
extern Il2CppType InternalEnumerator_1_t2965_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2965_GenericClass;
TypeInfo InternalEnumerator_1_t2965_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2965_MethodInfos/* methods */
	, InternalEnumerator_1_t2965_PropertyInfos/* properties */
	, InternalEnumerator_1_t2965_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2965_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2965_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2965_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2965_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2965_1_0_0/* this_arg */
	, InternalEnumerator_1_t2965_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2965_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2965_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2965)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7536_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>
extern MethodInfo ICollection_1_get_Count_m42718_MethodInfo;
static PropertyInfo ICollection_1_t7536____Count_PropertyInfo = 
{
	&ICollection_1_t7536_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42718_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42719_MethodInfo;
static PropertyInfo ICollection_1_t7536____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7536_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42719_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7536_PropertyInfos[] =
{
	&ICollection_1_t7536____Count_PropertyInfo,
	&ICollection_1_t7536____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42718_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::get_Count()
MethodInfo ICollection_1_get_Count_m42718_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42718_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42719_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42719_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42719_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRenderer_t85_0_0_0;
extern Il2CppType VideoTextureRenderer_t85_0_0_0;
static ParameterInfo ICollection_1_t7536_ICollection_1_Add_m42720_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t85_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42720_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Add(T)
MethodInfo ICollection_1_Add_m42720_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7536_ICollection_1_Add_m42720_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42720_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42721_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Clear()
MethodInfo ICollection_1_Clear_m42721_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42721_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRenderer_t85_0_0_0;
static ParameterInfo ICollection_1_t7536_ICollection_1_Contains_m42722_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t85_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42722_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Contains(T)
MethodInfo ICollection_1_Contains_m42722_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7536_ICollection_1_Contains_m42722_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42722_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRendererU5BU5D_t5196_0_0_0;
extern Il2CppType VideoTextureRendererU5BU5D_t5196_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7536_ICollection_1_CopyTo_m42723_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererU5BU5D_t5196_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42723_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42723_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7536_ICollection_1_CopyTo_m42723_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42723_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRenderer_t85_0_0_0;
static ParameterInfo ICollection_1_t7536_ICollection_1_Remove_m42724_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t85_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42724_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRenderer>::Remove(T)
MethodInfo ICollection_1_Remove_m42724_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7536_ICollection_1_Remove_m42724_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42724_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7536_MethodInfos[] =
{
	&ICollection_1_get_Count_m42718_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42719_MethodInfo,
	&ICollection_1_Add_m42720_MethodInfo,
	&ICollection_1_Clear_m42721_MethodInfo,
	&ICollection_1_Contains_m42722_MethodInfo,
	&ICollection_1_CopyTo_m42723_MethodInfo,
	&ICollection_1_Remove_m42724_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7538_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7536_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7538_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7536_0_0_0;
extern Il2CppType ICollection_1_t7536_1_0_0;
struct ICollection_1_t7536;
extern Il2CppGenericClass ICollection_1_t7536_GenericClass;
TypeInfo ICollection_1_t7536_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7536_MethodInfos/* methods */
	, ICollection_1_t7536_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7536_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7536_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7536_0_0_0/* byval_arg */
	, &ICollection_1_t7536_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7536_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoTextureRenderer>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.VideoTextureRenderer>
extern Il2CppType IEnumerator_1_t5913_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42725_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoTextureRenderer>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42725_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7538_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5913_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42725_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7538_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42725_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7538_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7538_0_0_0;
extern Il2CppType IEnumerable_1_t7538_1_0_0;
struct IEnumerable_1_t7538;
extern Il2CppGenericClass IEnumerable_1_t7538_GenericClass;
TypeInfo IEnumerable_1_t7538_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7538_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7538_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7538_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7538_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7538_0_0_0/* byval_arg */
	, &IEnumerable_1_t7538_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7538_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7537_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>
extern MethodInfo IList_1_get_Item_m42726_MethodInfo;
extern MethodInfo IList_1_set_Item_m42727_MethodInfo;
static PropertyInfo IList_1_t7537____Item_PropertyInfo = 
{
	&IList_1_t7537_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42726_MethodInfo/* get */
	, &IList_1_set_Item_m42727_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7537_PropertyInfos[] =
{
	&IList_1_t7537____Item_PropertyInfo,
	NULL
};
extern Il2CppType VideoTextureRenderer_t85_0_0_0;
static ParameterInfo IList_1_t7537_IList_1_IndexOf_m42728_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t85_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42728_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42728_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7537_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7537_IList_1_IndexOf_m42728_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42728_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType VideoTextureRenderer_t85_0_0_0;
static ParameterInfo IList_1_t7537_IList_1_Insert_m42729_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t85_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42729_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42729_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7537_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7537_IList_1_Insert_m42729_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42729_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7537_IList_1_RemoveAt_m42730_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42730_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42730_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7537_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7537_IList_1_RemoveAt_m42730_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42730_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7537_IList_1_get_Item_m42726_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType VideoTextureRenderer_t85_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42726_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42726_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7537_il2cpp_TypeInfo/* declaring_type */
	, &VideoTextureRenderer_t85_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7537_IList_1_get_Item_m42726_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42726_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType VideoTextureRenderer_t85_0_0_0;
static ParameterInfo IList_1_t7537_IList_1_set_Item_m42727_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t85_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42727_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRenderer>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42727_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7537_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7537_IList_1_set_Item_m42727_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42727_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7537_MethodInfos[] =
{
	&IList_1_IndexOf_m42728_MethodInfo,
	&IList_1_Insert_m42729_MethodInfo,
	&IList_1_RemoveAt_m42730_MethodInfo,
	&IList_1_get_Item_m42726_MethodInfo,
	&IList_1_set_Item_m42727_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7537_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7536_il2cpp_TypeInfo,
	&IEnumerable_1_t7538_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7537_0_0_0;
extern Il2CppType IList_1_t7537_1_0_0;
struct IList_1_t7537;
extern Il2CppGenericClass IList_1_t7537_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7537_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7537_MethodInfos/* methods */
	, IList_1_t7537_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7537_il2cpp_TypeInfo/* element_class */
	, IList_1_t7537_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7537_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7537_0_0_0/* byval_arg */
	, &IList_1_t7537_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7537_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7539_il2cpp_TypeInfo;

// Vuforia.VideoTextureRendererAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendere.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42731_MethodInfo;
static PropertyInfo ICollection_1_t7539____Count_PropertyInfo = 
{
	&ICollection_1_t7539_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42731_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42732_MethodInfo;
static PropertyInfo ICollection_1_t7539____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7539_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42732_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7539_PropertyInfos[] =
{
	&ICollection_1_t7539____Count_PropertyInfo,
	&ICollection_1_t7539____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42731_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42731_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42731_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42732_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42732_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42732_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRendererAbstractBehaviour_t86_0_0_0;
extern Il2CppType VideoTextureRendererAbstractBehaviour_t86_0_0_0;
static ParameterInfo ICollection_1_t7539_ICollection_1_Add_m42733_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererAbstractBehaviour_t86_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42733_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42733_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7539_ICollection_1_Add_m42733_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42733_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42734_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42734_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42734_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRendererAbstractBehaviour_t86_0_0_0;
static ParameterInfo ICollection_1_t7539_ICollection_1_Contains_m42735_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererAbstractBehaviour_t86_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42735_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42735_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7539_ICollection_1_Contains_m42735_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42735_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRendererAbstractBehaviourU5BU5D_t5521_0_0_0;
extern Il2CppType VideoTextureRendererAbstractBehaviourU5BU5D_t5521_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7539_ICollection_1_CopyTo_m42736_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererAbstractBehaviourU5BU5D_t5521_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42736_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42736_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7539_ICollection_1_CopyTo_m42736_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42736_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRendererAbstractBehaviour_t86_0_0_0;
static ParameterInfo ICollection_1_t7539_ICollection_1_Remove_m42737_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererAbstractBehaviour_t86_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42737_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42737_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7539_ICollection_1_Remove_m42737_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42737_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7539_MethodInfos[] =
{
	&ICollection_1_get_Count_m42731_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42732_MethodInfo,
	&ICollection_1_Add_m42733_MethodInfo,
	&ICollection_1_Clear_m42734_MethodInfo,
	&ICollection_1_Contains_m42735_MethodInfo,
	&ICollection_1_CopyTo_m42736_MethodInfo,
	&ICollection_1_Remove_m42737_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7541_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7539_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7541_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7539_0_0_0;
extern Il2CppType ICollection_1_t7539_1_0_0;
struct ICollection_1_t7539;
extern Il2CppGenericClass ICollection_1_t7539_GenericClass;
TypeInfo ICollection_1_t7539_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7539_MethodInfos/* methods */
	, ICollection_1_t7539_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7539_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7539_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7539_0_0_0/* byval_arg */
	, &ICollection_1_t7539_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7539_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoTextureRendererAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.VideoTextureRendererAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5915_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42738_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VideoTextureRendererAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42738_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7541_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5915_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42738_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7541_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42738_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7541_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7541_0_0_0;
extern Il2CppType IEnumerable_1_t7541_1_0_0;
struct IEnumerable_1_t7541;
extern Il2CppGenericClass IEnumerable_1_t7541_GenericClass;
TypeInfo IEnumerable_1_t7541_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7541_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7541_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7541_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7541_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7541_0_0_0/* byval_arg */
	, &IEnumerable_1_t7541_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7541_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5915_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42739_MethodInfo;
static PropertyInfo IEnumerator_1_t5915____Current_PropertyInfo = 
{
	&IEnumerator_1_t5915_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42739_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5915_PropertyInfos[] =
{
	&IEnumerator_1_t5915____Current_PropertyInfo,
	NULL
};
extern Il2CppType VideoTextureRendererAbstractBehaviour_t86_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42739_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42739_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5915_il2cpp_TypeInfo/* declaring_type */
	, &VideoTextureRendererAbstractBehaviour_t86_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42739_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5915_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42739_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5915_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5915_0_0_0;
extern Il2CppType IEnumerator_1_t5915_1_0_0;
struct IEnumerator_1_t5915;
extern Il2CppGenericClass IEnumerator_1_t5915_GenericClass;
TypeInfo IEnumerator_1_t5915_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5915_MethodInfos/* methods */
	, IEnumerator_1_t5915_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5915_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5915_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5915_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5915_0_0_0/* byval_arg */
	, &IEnumerator_1_t5915_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5915_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_117.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2966_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_117MethodDeclarations.h"

extern TypeInfo VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15255_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVideoTextureRendererAbstractBehaviour_t86_m32805_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.VideoTextureRendererAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VideoTextureRendererAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisVideoTextureRendererAbstractBehaviour_t86_m32805(__this, p0, method) (VideoTextureRendererAbstractBehaviour_t86 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2966____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2966_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2966, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2966____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2966_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2966, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2966_FieldInfos[] =
{
	&InternalEnumerator_1_t2966____array_0_FieldInfo,
	&InternalEnumerator_1_t2966____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15252_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2966____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2966_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15252_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2966____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2966_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15255_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2966_PropertyInfos[] =
{
	&InternalEnumerator_1_t2966____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2966____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2966_InternalEnumerator_1__ctor_m15251_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15251_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15251_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2966_InternalEnumerator_1__ctor_m15251_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15251_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15252_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15252_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2966_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15252_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15253_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15253_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15253_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15254_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15254_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2966_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15254_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRendererAbstractBehaviour_t86_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15255_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15255_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2966_il2cpp_TypeInfo/* declaring_type */
	, &VideoTextureRendererAbstractBehaviour_t86_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15255_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2966_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15251_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15252_MethodInfo,
	&InternalEnumerator_1_Dispose_m15253_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15254_MethodInfo,
	&InternalEnumerator_1_get_Current_m15255_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15254_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15253_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2966_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15252_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15254_MethodInfo,
	&InternalEnumerator_1_Dispose_m15253_MethodInfo,
	&InternalEnumerator_1_get_Current_m15255_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2966_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5915_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2966_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5915_il2cpp_TypeInfo, 7},
};
extern TypeInfo VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2966_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15255_MethodInfo/* Method Usage */,
	&VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisVideoTextureRendererAbstractBehaviour_t86_m32805_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2966_0_0_0;
extern Il2CppType InternalEnumerator_1_t2966_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2966_GenericClass;
TypeInfo InternalEnumerator_1_t2966_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2966_MethodInfos/* methods */
	, InternalEnumerator_1_t2966_PropertyInfos/* properties */
	, InternalEnumerator_1_t2966_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2966_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2966_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2966_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2966_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2966_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2966_1_0_0/* this_arg */
	, InternalEnumerator_1_t2966_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2966_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2966_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2966)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7540_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42740_MethodInfo;
extern MethodInfo IList_1_set_Item_m42741_MethodInfo;
static PropertyInfo IList_1_t7540____Item_PropertyInfo = 
{
	&IList_1_t7540_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42740_MethodInfo/* get */
	, &IList_1_set_Item_m42741_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7540_PropertyInfos[] =
{
	&IList_1_t7540____Item_PropertyInfo,
	NULL
};
extern Il2CppType VideoTextureRendererAbstractBehaviour_t86_0_0_0;
static ParameterInfo IList_1_t7540_IList_1_IndexOf_m42742_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererAbstractBehaviour_t86_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42742_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42742_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7540_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7540_IList_1_IndexOf_m42742_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42742_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType VideoTextureRendererAbstractBehaviour_t86_0_0_0;
static ParameterInfo IList_1_t7540_IList_1_Insert_m42743_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererAbstractBehaviour_t86_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42743_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42743_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7540_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7540_IList_1_Insert_m42743_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42743_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7540_IList_1_RemoveAt_m42744_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42744_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42744_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7540_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7540_IList_1_RemoveAt_m42744_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42744_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7540_IList_1_get_Item_m42740_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType VideoTextureRendererAbstractBehaviour_t86_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42740_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42740_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7540_il2cpp_TypeInfo/* declaring_type */
	, &VideoTextureRendererAbstractBehaviour_t86_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7540_IList_1_get_Item_m42740_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42740_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType VideoTextureRendererAbstractBehaviour_t86_0_0_0;
static ParameterInfo IList_1_t7540_IList_1_set_Item_m42741_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &VideoTextureRendererAbstractBehaviour_t86_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42741_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VideoTextureRendererAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42741_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7540_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7540_IList_1_set_Item_m42741_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42741_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7540_MethodInfos[] =
{
	&IList_1_IndexOf_m42742_MethodInfo,
	&IList_1_Insert_m42743_MethodInfo,
	&IList_1_RemoveAt_m42744_MethodInfo,
	&IList_1_get_Item_m42740_MethodInfo,
	&IList_1_set_Item_m42741_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7540_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7539_il2cpp_TypeInfo,
	&IEnumerable_1_t7541_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7540_0_0_0;
extern Il2CppType IList_1_t7540_1_0_0;
struct IList_1_t7540;
extern Il2CppGenericClass IList_1_t7540_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7540_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7540_MethodInfos/* methods */
	, IList_1_t7540_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7540_il2cpp_TypeInfo/* element_class */
	, IList_1_t7540_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7540_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7540_0_0_0/* byval_arg */
	, &IList_1_t7540_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7540_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_42.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2967_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_42MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_38.h"
extern TypeInfo InvokableCall_1_t2968_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_38MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15258_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15260_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2967____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2967_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2967, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2967_FieldInfos[] =
{
	&CachedInvokableCall_1_t2967____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType VideoTextureRenderer_t85_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2967_CachedInvokableCall_1__ctor_m15256_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t85_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15256_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15256_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2967_CachedInvokableCall_1__ctor_m15256_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15256_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2967_CachedInvokableCall_1_Invoke_m15257_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15257_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VideoTextureRenderer>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15257_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2967_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2967_CachedInvokableCall_1_Invoke_m15257_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15257_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2967_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15256_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15257_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15257_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15261_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2967_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15257_MethodInfo,
	&InvokableCall_1_Find_m15261_MethodInfo,
};
extern Il2CppType UnityAction_1_t2969_0_0_0;
extern TypeInfo UnityAction_1_t2969_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisVideoTextureRenderer_t85_m32815_MethodInfo;
extern TypeInfo VideoTextureRenderer_t85_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15263_MethodInfo;
extern TypeInfo VideoTextureRenderer_t85_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2967_RGCTXData[8] = 
{
	&UnityAction_1_t2969_0_0_0/* Type Usage */,
	&UnityAction_1_t2969_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisVideoTextureRenderer_t85_m32815_MethodInfo/* Method Usage */,
	&VideoTextureRenderer_t85_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15263_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15258_MethodInfo/* Method Usage */,
	&VideoTextureRenderer_t85_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15260_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2967_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2967_1_0_0;
struct CachedInvokableCall_1_t2967;
extern Il2CppGenericClass CachedInvokableCall_1_t2967_GenericClass;
TypeInfo CachedInvokableCall_1_t2967_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2967_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2967_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2968_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2967_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2967_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2967_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2967_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2967_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2967_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2967_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2967)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_45.h"
extern TypeInfo UnityAction_1_t2969_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_45MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.VideoTextureRenderer>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.VideoTextureRenderer>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisVideoTextureRenderer_t85_m32815(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>
extern Il2CppType UnityAction_1_t2969_0_0_1;
FieldInfo InvokableCall_1_t2968____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2969_0_0_1/* type */
	, &InvokableCall_1_t2968_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2968, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2968_FieldInfos[] =
{
	&InvokableCall_1_t2968____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2968_InvokableCall_1__ctor_m15258_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15258_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15258_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2968_InvokableCall_1__ctor_m15258_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15258_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2969_0_0_0;
static ParameterInfo InvokableCall_1_t2968_InvokableCall_1__ctor_m15259_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2969_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15259_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15259_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2968_InvokableCall_1__ctor_m15259_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15259_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2968_InvokableCall_1_Invoke_m15260_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15260_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15260_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2968_InvokableCall_1_Invoke_m15260_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15260_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2968_InvokableCall_1_Find_m15261_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15261_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRenderer>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15261_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2968_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2968_InvokableCall_1_Find_m15261_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15261_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2968_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15258_MethodInfo,
	&InvokableCall_1__ctor_m15259_MethodInfo,
	&InvokableCall_1_Invoke_m15260_MethodInfo,
	&InvokableCall_1_Find_m15261_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2968_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m15260_MethodInfo,
	&InvokableCall_1_Find_m15261_MethodInfo,
};
extern TypeInfo UnityAction_1_t2969_il2cpp_TypeInfo;
extern TypeInfo VideoTextureRenderer_t85_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2968_RGCTXData[5] = 
{
	&UnityAction_1_t2969_0_0_0/* Type Usage */,
	&UnityAction_1_t2969_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisVideoTextureRenderer_t85_m32815_MethodInfo/* Method Usage */,
	&VideoTextureRenderer_t85_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15263_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2968_0_0_0;
extern Il2CppType InvokableCall_1_t2968_1_0_0;
struct InvokableCall_1_t2968;
extern Il2CppGenericClass InvokableCall_1_t2968_GenericClass;
TypeInfo InvokableCall_1_t2968_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2968_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2968_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2968_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2968_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2968_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2968_0_0_0/* byval_arg */
	, &InvokableCall_1_t2968_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2968_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2968_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2968)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2969_UnityAction_1__ctor_m15262_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15262_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15262_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2969_UnityAction_1__ctor_m15262_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15262_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRenderer_t85_0_0_0;
static ParameterInfo UnityAction_1_t2969_UnityAction_1_Invoke_m15263_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t85_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15263_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15263_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2969_UnityAction_1_Invoke_m15263_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15263_GenericMethod/* genericMethod */

};
extern Il2CppType VideoTextureRenderer_t85_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2969_UnityAction_1_BeginInvoke_m15264_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &VideoTextureRenderer_t85_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15264_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15264_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2969_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2969_UnityAction_1_BeginInvoke_m15264_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15264_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2969_UnityAction_1_EndInvoke_m15265_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15265_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRenderer>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15265_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2969_UnityAction_1_EndInvoke_m15265_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15265_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2969_MethodInfos[] =
{
	&UnityAction_1__ctor_m15262_MethodInfo,
	&UnityAction_1_Invoke_m15263_MethodInfo,
	&UnityAction_1_BeginInvoke_m15264_MethodInfo,
	&UnityAction_1_EndInvoke_m15265_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15264_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15265_MethodInfo;
static MethodInfo* UnityAction_1_t2969_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m15263_MethodInfo,
	&UnityAction_1_BeginInvoke_m15264_MethodInfo,
	&UnityAction_1_EndInvoke_m15265_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2969_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2969_1_0_0;
struct UnityAction_1_t2969;
extern Il2CppGenericClass UnityAction_1_t2969_GenericClass;
TypeInfo UnityAction_1_t2969_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2969_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2969_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2969_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2969_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2969_0_0_0/* byval_arg */
	, &UnityAction_1_t2969_1_0_0/* this_arg */
	, UnityAction_1_t2969_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2969_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2969)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5917_il2cpp_TypeInfo;

// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42745_MethodInfo;
static PropertyInfo IEnumerator_1_t5917____Current_PropertyInfo = 
{
	&IEnumerator_1_t5917_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42745_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5917_PropertyInfos[] =
{
	&IEnumerator_1_t5917____Current_PropertyInfo,
	NULL
};
extern Il2CppType VirtualButtonBehaviour_t87_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42745_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42745_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5917_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonBehaviour_t87_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42745_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5917_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42745_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5917_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5917_0_0_0;
extern Il2CppType IEnumerator_1_t5917_1_0_0;
struct IEnumerator_1_t5917;
extern Il2CppGenericClass IEnumerator_1_t5917_GenericClass;
TypeInfo IEnumerator_1_t5917_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5917_MethodInfos/* methods */
	, IEnumerator_1_t5917_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5917_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5917_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5917_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5917_0_0_0/* byval_arg */
	, &IEnumerator_1_t5917_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5917_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_118.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2970_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_118MethodDeclarations.h"

extern TypeInfo VirtualButtonBehaviour_t87_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15270_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVirtualButtonBehaviour_t87_m32817_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.VirtualButtonBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VirtualButtonBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisVirtualButtonBehaviour_t87_m32817(__this, p0, method) (VirtualButtonBehaviour_t87 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2970____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2970, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2970____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2970, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2970_FieldInfos[] =
{
	&InternalEnumerator_1_t2970____array_0_FieldInfo,
	&InternalEnumerator_1_t2970____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15267_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2970____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2970_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15267_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2970____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2970_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15270_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2970_PropertyInfos[] =
{
	&InternalEnumerator_1_t2970____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2970____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2970_InternalEnumerator_1__ctor_m15266_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15266_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15266_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2970_InternalEnumerator_1__ctor_m15266_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15266_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15267_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15267_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15267_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15268_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15268_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15268_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15269_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15269_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15269_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonBehaviour_t87_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15270_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.VirtualButtonBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15270_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonBehaviour_t87_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15270_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2970_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15266_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15267_MethodInfo,
	&InternalEnumerator_1_Dispose_m15268_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15269_MethodInfo,
	&InternalEnumerator_1_get_Current_m15270_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15269_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15268_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2970_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15267_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15269_MethodInfo,
	&InternalEnumerator_1_Dispose_m15268_MethodInfo,
	&InternalEnumerator_1_get_Current_m15270_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2970_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5917_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2970_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5917_il2cpp_TypeInfo, 7},
};
extern TypeInfo VirtualButtonBehaviour_t87_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2970_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15270_MethodInfo/* Method Usage */,
	&VirtualButtonBehaviour_t87_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisVirtualButtonBehaviour_t87_m32817_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2970_0_0_0;
extern Il2CppType InternalEnumerator_1_t2970_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2970_GenericClass;
TypeInfo InternalEnumerator_1_t2970_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2970_MethodInfos/* methods */
	, InternalEnumerator_1_t2970_PropertyInfos/* properties */
	, InternalEnumerator_1_t2970_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2970_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2970_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2970_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2970_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2970_1_0_0/* this_arg */
	, InternalEnumerator_1_t2970_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2970_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2970_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2970)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7542_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>
extern MethodInfo ICollection_1_get_Count_m42746_MethodInfo;
static PropertyInfo ICollection_1_t7542____Count_PropertyInfo = 
{
	&ICollection_1_t7542_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42746_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42747_MethodInfo;
static PropertyInfo ICollection_1_t7542____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7542_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42747_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7542_PropertyInfos[] =
{
	&ICollection_1_t7542____Count_PropertyInfo,
	&ICollection_1_t7542____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42746_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42746_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42746_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42747_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42747_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42747_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonBehaviour_t87_0_0_0;
extern Il2CppType VirtualButtonBehaviour_t87_0_0_0;
static ParameterInfo ICollection_1_t7542_ICollection_1_Add_m42748_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t87_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42748_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42748_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7542_ICollection_1_Add_m42748_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42748_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42749_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42749_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42749_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonBehaviour_t87_0_0_0;
static ParameterInfo ICollection_1_t7542_ICollection_1_Contains_m42750_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t87_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42750_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42750_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7542_ICollection_1_Contains_m42750_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42750_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonBehaviourU5BU5D_t5197_0_0_0;
extern Il2CppType VirtualButtonBehaviourU5BU5D_t5197_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7542_ICollection_1_CopyTo_m42751_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviourU5BU5D_t5197_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42751_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42751_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7542_ICollection_1_CopyTo_m42751_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42751_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonBehaviour_t87_0_0_0;
static ParameterInfo ICollection_1_t7542_ICollection_1_Remove_m42752_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t87_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42752_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42752_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7542_ICollection_1_Remove_m42752_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42752_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7542_MethodInfos[] =
{
	&ICollection_1_get_Count_m42746_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42747_MethodInfo,
	&ICollection_1_Add_m42748_MethodInfo,
	&ICollection_1_Clear_m42749_MethodInfo,
	&ICollection_1_Contains_m42750_MethodInfo,
	&ICollection_1_CopyTo_m42751_MethodInfo,
	&ICollection_1_Remove_m42752_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7544_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7542_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7544_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7542_0_0_0;
extern Il2CppType ICollection_1_t7542_1_0_0;
struct ICollection_1_t7542;
extern Il2CppGenericClass ICollection_1_t7542_GenericClass;
TypeInfo ICollection_1_t7542_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7542_MethodInfos/* methods */
	, ICollection_1_t7542_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7542_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7542_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7542_0_0_0/* byval_arg */
	, &ICollection_1_t7542_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7542_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonBehaviour>
extern Il2CppType IEnumerator_1_t5917_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42753_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42753_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7544_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5917_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42753_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7544_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42753_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7544_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7544_0_0_0;
extern Il2CppType IEnumerable_1_t7544_1_0_0;
struct IEnumerable_1_t7544;
extern Il2CppGenericClass IEnumerable_1_t7544_GenericClass;
TypeInfo IEnumerable_1_t7544_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7544_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7544_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7544_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7544_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7544_0_0_0/* byval_arg */
	, &IEnumerable_1_t7544_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7544_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7543_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>
extern MethodInfo IList_1_get_Item_m42754_MethodInfo;
extern MethodInfo IList_1_set_Item_m42755_MethodInfo;
static PropertyInfo IList_1_t7543____Item_PropertyInfo = 
{
	&IList_1_t7543_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42754_MethodInfo/* get */
	, &IList_1_set_Item_m42755_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7543_PropertyInfos[] =
{
	&IList_1_t7543____Item_PropertyInfo,
	NULL
};
extern Il2CppType VirtualButtonBehaviour_t87_0_0_0;
static ParameterInfo IList_1_t7543_IList_1_IndexOf_m42756_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t87_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42756_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42756_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7543_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7543_IList_1_IndexOf_m42756_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42756_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType VirtualButtonBehaviour_t87_0_0_0;
static ParameterInfo IList_1_t7543_IList_1_Insert_m42757_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t87_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42757_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42757_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7543_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7543_IList_1_Insert_m42757_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42757_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7543_IList_1_RemoveAt_m42758_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42758_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42758_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7543_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7543_IList_1_RemoveAt_m42758_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42758_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7543_IList_1_get_Item_m42754_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType VirtualButtonBehaviour_t87_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42754_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42754_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7543_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonBehaviour_t87_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7543_IList_1_get_Item_m42754_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42754_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType VirtualButtonBehaviour_t87_0_0_0;
static ParameterInfo IList_1_t7543_IList_1_set_Item_m42755_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t87_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42755_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42755_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7543_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7543_IList_1_set_Item_m42755_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42755_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7543_MethodInfos[] =
{
	&IList_1_IndexOf_m42756_MethodInfo,
	&IList_1_Insert_m42757_MethodInfo,
	&IList_1_RemoveAt_m42758_MethodInfo,
	&IList_1_get_Item_m42754_MethodInfo,
	&IList_1_set_Item_m42755_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7543_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7542_il2cpp_TypeInfo,
	&IEnumerable_1_t7544_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7543_0_0_0;
extern Il2CppType IList_1_t7543_1_0_0;
struct IList_1_t7543;
extern Il2CppGenericClass IList_1_t7543_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7543_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7543_MethodInfos/* methods */
	, IList_1_t7543_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7543_il2cpp_TypeInfo/* element_class */
	, IList_1_t7543_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7543_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7543_0_0_0/* byval_arg */
	, &IList_1_t7543_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7543_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t4207_il2cpp_TypeInfo;

// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42759_MethodInfo;
static PropertyInfo ICollection_1_t4207____Count_PropertyInfo = 
{
	&ICollection_1_t4207_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42759_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42760_MethodInfo;
static PropertyInfo ICollection_1_t4207____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t4207_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42760_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t4207_PropertyInfos[] =
{
	&ICollection_1_t4207____Count_PropertyInfo,
	&ICollection_1_t4207____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42759_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42759_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t4207_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42759_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42760_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42760_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t4207_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42760_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonAbstractBehaviour_t56_0_0_0;
extern Il2CppType VirtualButtonAbstractBehaviour_t56_0_0_0;
static ParameterInfo ICollection_1_t4207_ICollection_1_Add_m42761_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonAbstractBehaviour_t56_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42761_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42761_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t4207_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t4207_ICollection_1_Add_m42761_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42761_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42762_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42762_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t4207_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42762_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonAbstractBehaviour_t56_0_0_0;
static ParameterInfo ICollection_1_t4207_ICollection_1_Contains_m42763_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonAbstractBehaviour_t56_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42763_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42763_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t4207_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t4207_ICollection_1_Contains_m42763_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42763_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonAbstractBehaviourU5BU5D_t729_0_0_0;
extern Il2CppType VirtualButtonAbstractBehaviourU5BU5D_t729_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t4207_ICollection_1_CopyTo_m42764_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonAbstractBehaviourU5BU5D_t729_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42764_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42764_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t4207_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t4207_ICollection_1_CopyTo_m42764_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42764_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonAbstractBehaviour_t56_0_0_0;
static ParameterInfo ICollection_1_t4207_ICollection_1_Remove_m42765_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonAbstractBehaviour_t56_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42765_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.VirtualButtonAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42765_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t4207_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t4207_ICollection_1_Remove_m42765_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42765_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t4207_MethodInfos[] =
{
	&ICollection_1_get_Count_m42759_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42760_MethodInfo,
	&ICollection_1_Add_m42761_MethodInfo,
	&ICollection_1_Clear_m42762_MethodInfo,
	&ICollection_1_Contains_m42763_MethodInfo,
	&ICollection_1_CopyTo_m42764_MethodInfo,
	&ICollection_1_Remove_m42765_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t751_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t4207_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t751_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t4207_0_0_0;
extern Il2CppType ICollection_1_t4207_1_0_0;
struct ICollection_1_t4207;
extern Il2CppGenericClass ICollection_1_t4207_GenericClass;
TypeInfo ICollection_1_t4207_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t4207_MethodInfos/* methods */
	, ICollection_1_t4207_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t4207_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t4207_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t4207_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t4207_0_0_0/* byval_arg */
	, &ICollection_1_t4207_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t4207_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonAbstractBehaviour>
extern Il2CppType IEnumerator_1_t870_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m5248_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m5248_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t751_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t870_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m5248_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t751_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m5248_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t751_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t751_0_0_0;
extern Il2CppType IEnumerable_1_t751_1_0_0;
struct IEnumerable_1_t751;
extern Il2CppGenericClass IEnumerable_1_t751_GenericClass;
TypeInfo IEnumerable_1_t751_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t751_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t751_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t751_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t751_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t751_0_0_0/* byval_arg */
	, &IEnumerable_1_t751_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t751_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t870_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m5249_MethodInfo;
static PropertyInfo IEnumerator_1_t870____Current_PropertyInfo = 
{
	&IEnumerator_1_t870_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m5249_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t870_PropertyInfos[] =
{
	&IEnumerator_1_t870____Current_PropertyInfo,
	NULL
};
extern Il2CppType VirtualButtonAbstractBehaviour_t56_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m5249_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m5249_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t870_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonAbstractBehaviour_t56_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m5249_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t870_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m5249_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t870_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t870_0_0_0;
extern Il2CppType IEnumerator_1_t870_1_0_0;
struct IEnumerator_1_t870;
extern Il2CppGenericClass IEnumerator_1_t870_GenericClass;
TypeInfo IEnumerator_1_t870_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t870_MethodInfos/* methods */
	, IEnumerator_1_t870_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t870_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t870_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t870_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t870_0_0_0/* byval_arg */
	, &IEnumerator_1_t870_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t870_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_119.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2971_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_119MethodDeclarations.h"

extern TypeInfo VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15275_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVirtualButtonAbstractBehaviour_t56_m32828_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.VirtualButtonAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.VirtualButtonAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisVirtualButtonAbstractBehaviour_t56_m32828(__this, p0, method) (VirtualButtonAbstractBehaviour_t56 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2971____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2971, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2971____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2971, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2971_FieldInfos[] =
{
	&InternalEnumerator_1_t2971____array_0_FieldInfo,
	&InternalEnumerator_1_t2971____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15272_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2971____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2971_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15272_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2971____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2971_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15275_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2971_PropertyInfos[] =
{
	&InternalEnumerator_1_t2971____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2971____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2971_InternalEnumerator_1__ctor_m15271_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15271_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15271_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2971_InternalEnumerator_1__ctor_m15271_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15271_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15272_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15272_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15272_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15273_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15273_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15273_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15274_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15274_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15274_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonAbstractBehaviour_t56_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15275_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15275_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonAbstractBehaviour_t56_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15275_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2971_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15271_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15272_MethodInfo,
	&InternalEnumerator_1_Dispose_m15273_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15274_MethodInfo,
	&InternalEnumerator_1_get_Current_m15275_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15274_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15273_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2971_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15272_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15274_MethodInfo,
	&InternalEnumerator_1_Dispose_m15273_MethodInfo,
	&InternalEnumerator_1_get_Current_m15275_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2971_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t870_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2971_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t870_il2cpp_TypeInfo, 7},
};
extern TypeInfo VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2971_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15275_MethodInfo/* Method Usage */,
	&VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisVirtualButtonAbstractBehaviour_t56_m32828_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2971_0_0_0;
extern Il2CppType InternalEnumerator_1_t2971_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2971_GenericClass;
TypeInfo InternalEnumerator_1_t2971_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2971_MethodInfos/* methods */
	, InternalEnumerator_1_t2971_PropertyInfos/* properties */
	, InternalEnumerator_1_t2971_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2971_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2971_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2971_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2971_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2971_1_0_0/* this_arg */
	, InternalEnumerator_1_t2971_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2971_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2971_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2971)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t4211_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42766_MethodInfo;
extern MethodInfo IList_1_set_Item_m42767_MethodInfo;
static PropertyInfo IList_1_t4211____Item_PropertyInfo = 
{
	&IList_1_t4211_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42766_MethodInfo/* get */
	, &IList_1_set_Item_m42767_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t4211_PropertyInfos[] =
{
	&IList_1_t4211____Item_PropertyInfo,
	NULL
};
extern Il2CppType VirtualButtonAbstractBehaviour_t56_0_0_0;
static ParameterInfo IList_1_t4211_IList_1_IndexOf_m42768_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonAbstractBehaviour_t56_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42768_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42768_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t4211_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t4211_IList_1_IndexOf_m42768_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42768_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType VirtualButtonAbstractBehaviour_t56_0_0_0;
static ParameterInfo IList_1_t4211_IList_1_Insert_m42769_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &VirtualButtonAbstractBehaviour_t56_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42769_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42769_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t4211_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t4211_IList_1_Insert_m42769_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42769_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t4211_IList_1_RemoveAt_m42770_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42770_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42770_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t4211_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t4211_IList_1_RemoveAt_m42770_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42770_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t4211_IList_1_get_Item_m42766_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType VirtualButtonAbstractBehaviour_t56_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42766_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42766_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t4211_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonAbstractBehaviour_t56_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t4211_IList_1_get_Item_m42766_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42766_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType VirtualButtonAbstractBehaviour_t56_0_0_0;
static ParameterInfo IList_1_t4211_IList_1_set_Item_m42767_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &VirtualButtonAbstractBehaviour_t56_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42767_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.VirtualButtonAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42767_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t4211_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t4211_IList_1_set_Item_m42767_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42767_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t4211_MethodInfos[] =
{
	&IList_1_IndexOf_m42768_MethodInfo,
	&IList_1_Insert_m42769_MethodInfo,
	&IList_1_RemoveAt_m42770_MethodInfo,
	&IList_1_get_Item_m42766_MethodInfo,
	&IList_1_set_Item_m42767_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t4211_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t4207_il2cpp_TypeInfo,
	&IEnumerable_1_t751_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t4211_0_0_0;
extern Il2CppType IList_1_t4211_1_0_0;
struct IList_1_t4211;
extern Il2CppGenericClass IList_1_t4211_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t4211_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t4211_MethodInfos/* methods */
	, IList_1_t4211_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t4211_il2cpp_TypeInfo/* element_class */
	, IList_1_t4211_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t4211_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t4211_0_0_0/* byval_arg */
	, &IList_1_t4211_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t4211_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7545_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>
extern MethodInfo ICollection_1_get_Count_m42771_MethodInfo;
static PropertyInfo ICollection_1_t7545____Count_PropertyInfo = 
{
	&ICollection_1_t7545_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42771_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42772_MethodInfo;
static PropertyInfo ICollection_1_t7545____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7545_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42772_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7545_PropertyInfos[] =
{
	&ICollection_1_t7545____Count_PropertyInfo,
	&ICollection_1_t7545____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42771_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42771_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42771_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42772_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42772_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42772_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorVirtualButtonBehaviour_t170_0_0_0;
extern Il2CppType IEditorVirtualButtonBehaviour_t170_0_0_0;
static ParameterInfo ICollection_1_t7545_ICollection_1_Add_m42773_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorVirtualButtonBehaviour_t170_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42773_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42773_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7545_ICollection_1_Add_m42773_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42773_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42774_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42774_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42774_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorVirtualButtonBehaviour_t170_0_0_0;
static ParameterInfo ICollection_1_t7545_ICollection_1_Contains_m42775_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorVirtualButtonBehaviour_t170_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42775_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42775_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7545_ICollection_1_Contains_m42775_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42775_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorVirtualButtonBehaviourU5BU5D_t5522_0_0_0;
extern Il2CppType IEditorVirtualButtonBehaviourU5BU5D_t5522_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7545_ICollection_1_CopyTo_m42776_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorVirtualButtonBehaviourU5BU5D_t5522_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42776_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42776_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7545_ICollection_1_CopyTo_m42776_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42776_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorVirtualButtonBehaviour_t170_0_0_0;
static ParameterInfo ICollection_1_t7545_ICollection_1_Remove_m42777_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorVirtualButtonBehaviour_t170_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42777_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorVirtualButtonBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42777_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7545_ICollection_1_Remove_m42777_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42777_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7545_MethodInfos[] =
{
	&ICollection_1_get_Count_m42771_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42772_MethodInfo,
	&ICollection_1_Add_m42773_MethodInfo,
	&ICollection_1_Clear_m42774_MethodInfo,
	&ICollection_1_Contains_m42775_MethodInfo,
	&ICollection_1_CopyTo_m42776_MethodInfo,
	&ICollection_1_Remove_m42777_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7547_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7545_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7547_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7545_0_0_0;
extern Il2CppType ICollection_1_t7545_1_0_0;
struct ICollection_1_t7545;
extern Il2CppGenericClass ICollection_1_t7545_GenericClass;
TypeInfo ICollection_1_t7545_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7545_MethodInfos/* methods */
	, ICollection_1_t7545_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7545_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7545_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7545_0_0_0/* byval_arg */
	, &ICollection_1_t7545_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7545_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorVirtualButtonBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorVirtualButtonBehaviour>
extern Il2CppType IEnumerator_1_t5919_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42778_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorVirtualButtonBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42778_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7547_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5919_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42778_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7547_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42778_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7547_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7547_0_0_0;
extern Il2CppType IEnumerable_1_t7547_1_0_0;
struct IEnumerable_1_t7547;
extern Il2CppGenericClass IEnumerable_1_t7547_GenericClass;
TypeInfo IEnumerable_1_t7547_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7547_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7547_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7547_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7547_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7547_0_0_0/* byval_arg */
	, &IEnumerable_1_t7547_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7547_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5919_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42779_MethodInfo;
static PropertyInfo IEnumerator_1_t5919____Current_PropertyInfo = 
{
	&IEnumerator_1_t5919_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42779_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5919_PropertyInfos[] =
{
	&IEnumerator_1_t5919____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorVirtualButtonBehaviour_t170_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42779_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42779_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5919_il2cpp_TypeInfo/* declaring_type */
	, &IEditorVirtualButtonBehaviour_t170_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42779_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5919_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42779_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5919_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5919_0_0_0;
extern Il2CppType IEnumerator_1_t5919_1_0_0;
struct IEnumerator_1_t5919;
extern Il2CppGenericClass IEnumerator_1_t5919_GenericClass;
TypeInfo IEnumerator_1_t5919_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5919_MethodInfos/* methods */
	, IEnumerator_1_t5919_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5919_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5919_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5919_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5919_0_0_0/* byval_arg */
	, &IEnumerator_1_t5919_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5919_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_120.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2972_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_120MethodDeclarations.h"

extern TypeInfo IEditorVirtualButtonBehaviour_t170_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15280_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorVirtualButtonBehaviour_t170_m32839_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorVirtualButtonBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorVirtualButtonBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorVirtualButtonBehaviour_t170_m32839(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2972____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2972_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2972, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2972____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2972_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2972, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2972_FieldInfos[] =
{
	&InternalEnumerator_1_t2972____array_0_FieldInfo,
	&InternalEnumerator_1_t2972____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15277_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2972____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2972_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15277_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2972____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2972_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15280_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2972_PropertyInfos[] =
{
	&InternalEnumerator_1_t2972____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2972____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2972_InternalEnumerator_1__ctor_m15276_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15276_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15276_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2972_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2972_InternalEnumerator_1__ctor_m15276_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15276_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15277_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15277_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2972_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15277_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15278_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15278_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2972_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15278_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15279_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15279_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2972_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15279_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorVirtualButtonBehaviour_t170_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15280_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15280_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2972_il2cpp_TypeInfo/* declaring_type */
	, &IEditorVirtualButtonBehaviour_t170_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15280_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2972_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15276_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15277_MethodInfo,
	&InternalEnumerator_1_Dispose_m15278_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15279_MethodInfo,
	&InternalEnumerator_1_get_Current_m15280_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15279_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15278_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2972_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15277_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15279_MethodInfo,
	&InternalEnumerator_1_Dispose_m15278_MethodInfo,
	&InternalEnumerator_1_get_Current_m15280_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2972_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5919_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2972_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5919_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorVirtualButtonBehaviour_t170_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2972_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15280_MethodInfo/* Method Usage */,
	&IEditorVirtualButtonBehaviour_t170_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorVirtualButtonBehaviour_t170_m32839_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2972_0_0_0;
extern Il2CppType InternalEnumerator_1_t2972_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2972_GenericClass;
TypeInfo InternalEnumerator_1_t2972_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2972_MethodInfos/* methods */
	, InternalEnumerator_1_t2972_PropertyInfos/* properties */
	, InternalEnumerator_1_t2972_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2972_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2972_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2972_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2972_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2972_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2972_1_0_0/* this_arg */
	, InternalEnumerator_1_t2972_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2972_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2972_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2972)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7546_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>
extern MethodInfo IList_1_get_Item_m42780_MethodInfo;
extern MethodInfo IList_1_set_Item_m42781_MethodInfo;
static PropertyInfo IList_1_t7546____Item_PropertyInfo = 
{
	&IList_1_t7546_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42780_MethodInfo/* get */
	, &IList_1_set_Item_m42781_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7546_PropertyInfos[] =
{
	&IList_1_t7546____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorVirtualButtonBehaviour_t170_0_0_0;
static ParameterInfo IList_1_t7546_IList_1_IndexOf_m42782_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorVirtualButtonBehaviour_t170_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42782_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42782_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7546_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7546_IList_1_IndexOf_m42782_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42782_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorVirtualButtonBehaviour_t170_0_0_0;
static ParameterInfo IList_1_t7546_IList_1_Insert_m42783_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorVirtualButtonBehaviour_t170_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42783_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42783_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7546_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7546_IList_1_Insert_m42783_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42783_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7546_IList_1_RemoveAt_m42784_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42784_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42784_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7546_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7546_IList_1_RemoveAt_m42784_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42784_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7546_IList_1_get_Item_m42780_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEditorVirtualButtonBehaviour_t170_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42780_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42780_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7546_il2cpp_TypeInfo/* declaring_type */
	, &IEditorVirtualButtonBehaviour_t170_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7546_IList_1_get_Item_m42780_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42780_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorVirtualButtonBehaviour_t170_0_0_0;
static ParameterInfo IList_1_t7546_IList_1_set_Item_m42781_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorVirtualButtonBehaviour_t170_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42781_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorVirtualButtonBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42781_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7546_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7546_IList_1_set_Item_m42781_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42781_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7546_MethodInfos[] =
{
	&IList_1_IndexOf_m42782_MethodInfo,
	&IList_1_Insert_m42783_MethodInfo,
	&IList_1_RemoveAt_m42784_MethodInfo,
	&IList_1_get_Item_m42780_MethodInfo,
	&IList_1_set_Item_m42781_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7546_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7545_il2cpp_TypeInfo,
	&IEnumerable_1_t7547_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7546_0_0_0;
extern Il2CppType IList_1_t7546_1_0_0;
struct IList_1_t7546;
extern Il2CppGenericClass IList_1_t7546_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7546_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7546_MethodInfos/* methods */
	, IList_1_t7546_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7546_il2cpp_TypeInfo/* element_class */
	, IList_1_t7546_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7546_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7546_0_0_0/* byval_arg */
	, &IList_1_t7546_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7546_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_43.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2973_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_43MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_39.h"
extern TypeInfo InvokableCall_1_t2974_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_39MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15283_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15285_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2973____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2973_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2973, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2973_FieldInfos[] =
{
	&CachedInvokableCall_1_t2973____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType VirtualButtonBehaviour_t87_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2973_CachedInvokableCall_1__ctor_m15281_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t87_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15281_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15281_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2973_CachedInvokableCall_1__ctor_m15281_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15281_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2973_CachedInvokableCall_1_Invoke_m15282_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15282_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.VirtualButtonBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15282_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2973_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2973_CachedInvokableCall_1_Invoke_m15282_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15282_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2973_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15281_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15282_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15282_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15286_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2973_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15282_MethodInfo,
	&InvokableCall_1_Find_m15286_MethodInfo,
};
extern Il2CppType UnityAction_1_t2975_0_0_0;
extern TypeInfo UnityAction_1_t2975_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisVirtualButtonBehaviour_t87_m32849_MethodInfo;
extern TypeInfo VirtualButtonBehaviour_t87_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15288_MethodInfo;
extern TypeInfo VirtualButtonBehaviour_t87_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2973_RGCTXData[8] = 
{
	&UnityAction_1_t2975_0_0_0/* Type Usage */,
	&UnityAction_1_t2975_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisVirtualButtonBehaviour_t87_m32849_MethodInfo/* Method Usage */,
	&VirtualButtonBehaviour_t87_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15288_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15283_MethodInfo/* Method Usage */,
	&VirtualButtonBehaviour_t87_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15285_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2973_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2973_1_0_0;
struct CachedInvokableCall_1_t2973;
extern Il2CppGenericClass CachedInvokableCall_1_t2973_GenericClass;
TypeInfo CachedInvokableCall_1_t2973_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2973_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2973_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2974_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2973_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2973_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2973_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2973_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2973_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2973_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2973_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2973)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_46.h"
extern TypeInfo UnityAction_1_t2975_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_46MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.VirtualButtonBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.VirtualButtonBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisVirtualButtonBehaviour_t87_m32849(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>
extern Il2CppType UnityAction_1_t2975_0_0_1;
FieldInfo InvokableCall_1_t2974____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2975_0_0_1/* type */
	, &InvokableCall_1_t2974_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2974, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2974_FieldInfos[] =
{
	&InvokableCall_1_t2974____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2974_InvokableCall_1__ctor_m15283_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15283_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15283_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2974_InvokableCall_1__ctor_m15283_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15283_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2975_0_0_0;
static ParameterInfo InvokableCall_1_t2974_InvokableCall_1__ctor_m15284_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2975_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15284_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15284_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2974_InvokableCall_1__ctor_m15284_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15284_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2974_InvokableCall_1_Invoke_m15285_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15285_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15285_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2974_InvokableCall_1_Invoke_m15285_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15285_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2974_InvokableCall_1_Find_m15286_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15286_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15286_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2974_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2974_InvokableCall_1_Find_m15286_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15286_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2974_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15283_MethodInfo,
	&InvokableCall_1__ctor_m15284_MethodInfo,
	&InvokableCall_1_Invoke_m15285_MethodInfo,
	&InvokableCall_1_Find_m15286_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2974_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m15285_MethodInfo,
	&InvokableCall_1_Find_m15286_MethodInfo,
};
extern TypeInfo UnityAction_1_t2975_il2cpp_TypeInfo;
extern TypeInfo VirtualButtonBehaviour_t87_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2974_RGCTXData[5] = 
{
	&UnityAction_1_t2975_0_0_0/* Type Usage */,
	&UnityAction_1_t2975_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisVirtualButtonBehaviour_t87_m32849_MethodInfo/* Method Usage */,
	&VirtualButtonBehaviour_t87_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15288_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2974_0_0_0;
extern Il2CppType InvokableCall_1_t2974_1_0_0;
struct InvokableCall_1_t2974;
extern Il2CppGenericClass InvokableCall_1_t2974_GenericClass;
TypeInfo InvokableCall_1_t2974_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2974_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2974_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2974_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2974_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2974_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2974_0_0_0/* byval_arg */
	, &InvokableCall_1_t2974_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2974_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2974_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2974)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2975_UnityAction_1__ctor_m15287_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15287_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15287_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2975_UnityAction_1__ctor_m15287_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15287_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonBehaviour_t87_0_0_0;
static ParameterInfo UnityAction_1_t2975_UnityAction_1_Invoke_m15288_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t87_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15288_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15288_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2975_UnityAction_1_Invoke_m15288_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15288_GenericMethod/* genericMethod */

};
extern Il2CppType VirtualButtonBehaviour_t87_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2975_UnityAction_1_BeginInvoke_m15289_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &VirtualButtonBehaviour_t87_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15289_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15289_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2975_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2975_UnityAction_1_BeginInvoke_m15289_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15289_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2975_UnityAction_1_EndInvoke_m15290_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15290_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15290_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2975_UnityAction_1_EndInvoke_m15290_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15290_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2975_MethodInfos[] =
{
	&UnityAction_1__ctor_m15287_MethodInfo,
	&UnityAction_1_Invoke_m15288_MethodInfo,
	&UnityAction_1_BeginInvoke_m15289_MethodInfo,
	&UnityAction_1_EndInvoke_m15290_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15289_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15290_MethodInfo;
static MethodInfo* UnityAction_1_t2975_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m15288_MethodInfo,
	&UnityAction_1_BeginInvoke_m15289_MethodInfo,
	&UnityAction_1_EndInvoke_m15290_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2975_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2975_1_0_0;
struct UnityAction_1_t2975;
extern Il2CppGenericClass UnityAction_1_t2975_GenericClass;
TypeInfo UnityAction_1_t2975_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2975_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2975_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2975_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2975_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2975_0_0_0/* byval_arg */
	, &UnityAction_1_t2975_1_0_0/* this_arg */
	, UnityAction_1_t2975_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2975_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2975)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5921_il2cpp_TypeInfo;

// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.WebCamBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.WebCamBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42785_MethodInfo;
static PropertyInfo IEnumerator_1_t5921____Current_PropertyInfo = 
{
	&IEnumerator_1_t5921_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42785_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5921_PropertyInfos[] =
{
	&IEnumerator_1_t5921____Current_PropertyInfo,
	NULL
};
extern Il2CppType WebCamBehaviour_t88_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42785_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.WebCamBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42785_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5921_il2cpp_TypeInfo/* declaring_type */
	, &WebCamBehaviour_t88_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42785_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5921_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42785_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5921_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5921_0_0_0;
extern Il2CppType IEnumerator_1_t5921_1_0_0;
struct IEnumerator_1_t5921;
extern Il2CppGenericClass IEnumerator_1_t5921_GenericClass;
TypeInfo IEnumerator_1_t5921_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5921_MethodInfos/* methods */
	, IEnumerator_1_t5921_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5921_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5921_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5921_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5921_0_0_0/* byval_arg */
	, &IEnumerator_1_t5921_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5921_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_121.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2976_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_121MethodDeclarations.h"

extern TypeInfo WebCamBehaviour_t88_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15295_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWebCamBehaviour_t88_m32851_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.WebCamBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.WebCamBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisWebCamBehaviour_t88_m32851(__this, p0, method) (WebCamBehaviour_t88 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2976____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2976, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2976____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2976, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2976_FieldInfos[] =
{
	&InternalEnumerator_1_t2976____array_0_FieldInfo,
	&InternalEnumerator_1_t2976____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15292_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2976____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2976_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15292_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2976____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2976_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15295_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2976_PropertyInfos[] =
{
	&InternalEnumerator_1_t2976____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2976____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2976_InternalEnumerator_1__ctor_m15291_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15291_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15291_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2976_InternalEnumerator_1__ctor_m15291_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15291_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15292_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15292_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15292_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15293_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15293_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15293_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15294_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15294_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15294_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamBehaviour_t88_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15295_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.WebCamBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15295_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* declaring_type */
	, &WebCamBehaviour_t88_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15295_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2976_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15291_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15292_MethodInfo,
	&InternalEnumerator_1_Dispose_m15293_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15294_MethodInfo,
	&InternalEnumerator_1_get_Current_m15295_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15294_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15293_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2976_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15292_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15294_MethodInfo,
	&InternalEnumerator_1_Dispose_m15293_MethodInfo,
	&InternalEnumerator_1_get_Current_m15295_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2976_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5921_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2976_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5921_il2cpp_TypeInfo, 7},
};
extern TypeInfo WebCamBehaviour_t88_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2976_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15295_MethodInfo/* Method Usage */,
	&WebCamBehaviour_t88_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisWebCamBehaviour_t88_m32851_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2976_0_0_0;
extern Il2CppType InternalEnumerator_1_t2976_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2976_GenericClass;
TypeInfo InternalEnumerator_1_t2976_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2976_MethodInfos/* methods */
	, InternalEnumerator_1_t2976_PropertyInfos/* properties */
	, InternalEnumerator_1_t2976_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2976_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2976_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2976_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2976_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2976_1_0_0/* this_arg */
	, InternalEnumerator_1_t2976_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2976_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2976_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2976)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7548_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>
extern MethodInfo ICollection_1_get_Count_m42786_MethodInfo;
static PropertyInfo ICollection_1_t7548____Count_PropertyInfo = 
{
	&ICollection_1_t7548_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42786_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42787_MethodInfo;
static PropertyInfo ICollection_1_t7548____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7548_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42787_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7548_PropertyInfos[] =
{
	&ICollection_1_t7548____Count_PropertyInfo,
	&ICollection_1_t7548____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42786_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42786_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42786_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42787_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42787_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42787_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamBehaviour_t88_0_0_0;
extern Il2CppType WebCamBehaviour_t88_0_0_0;
static ParameterInfo ICollection_1_t7548_ICollection_1_Add_m42788_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t88_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42788_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42788_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7548_ICollection_1_Add_m42788_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42788_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42789_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42789_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42789_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamBehaviour_t88_0_0_0;
static ParameterInfo ICollection_1_t7548_ICollection_1_Contains_m42790_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t88_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42790_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42790_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7548_ICollection_1_Contains_m42790_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42790_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamBehaviourU5BU5D_t5198_0_0_0;
extern Il2CppType WebCamBehaviourU5BU5D_t5198_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7548_ICollection_1_CopyTo_m42791_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviourU5BU5D_t5198_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42791_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42791_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7548_ICollection_1_CopyTo_m42791_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42791_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamBehaviour_t88_0_0_0;
static ParameterInfo ICollection_1_t7548_ICollection_1_Remove_m42792_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t88_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42792_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42792_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7548_ICollection_1_Remove_m42792_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42792_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7548_MethodInfos[] =
{
	&ICollection_1_get_Count_m42786_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42787_MethodInfo,
	&ICollection_1_Add_m42788_MethodInfo,
	&ICollection_1_Clear_m42789_MethodInfo,
	&ICollection_1_Contains_m42790_MethodInfo,
	&ICollection_1_CopyTo_m42791_MethodInfo,
	&ICollection_1_Remove_m42792_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7550_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7548_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7550_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7548_0_0_0;
extern Il2CppType ICollection_1_t7548_1_0_0;
struct ICollection_1_t7548;
extern Il2CppGenericClass ICollection_1_t7548_GenericClass;
TypeInfo ICollection_1_t7548_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7548_MethodInfos/* methods */
	, ICollection_1_t7548_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7548_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7548_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7548_0_0_0/* byval_arg */
	, &ICollection_1_t7548_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7548_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WebCamBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.WebCamBehaviour>
extern Il2CppType IEnumerator_1_t5921_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42793_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WebCamBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42793_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7550_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5921_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42793_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7550_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42793_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7550_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7550_0_0_0;
extern Il2CppType IEnumerable_1_t7550_1_0_0;
struct IEnumerable_1_t7550;
extern Il2CppGenericClass IEnumerable_1_t7550_GenericClass;
TypeInfo IEnumerable_1_t7550_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7550_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7550_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7550_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7550_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7550_0_0_0/* byval_arg */
	, &IEnumerable_1_t7550_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7550_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7549_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>
extern MethodInfo IList_1_get_Item_m42794_MethodInfo;
extern MethodInfo IList_1_set_Item_m42795_MethodInfo;
static PropertyInfo IList_1_t7549____Item_PropertyInfo = 
{
	&IList_1_t7549_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42794_MethodInfo/* get */
	, &IList_1_set_Item_m42795_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7549_PropertyInfos[] =
{
	&IList_1_t7549____Item_PropertyInfo,
	NULL
};
extern Il2CppType WebCamBehaviour_t88_0_0_0;
static ParameterInfo IList_1_t7549_IList_1_IndexOf_m42796_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t88_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42796_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42796_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7549_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7549_IList_1_IndexOf_m42796_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42796_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType WebCamBehaviour_t88_0_0_0;
static ParameterInfo IList_1_t7549_IList_1_Insert_m42797_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t88_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42797_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42797_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7549_IList_1_Insert_m42797_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42797_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7549_IList_1_RemoveAt_m42798_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42798_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42798_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7549_IList_1_RemoveAt_m42798_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42798_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7549_IList_1_get_Item_m42794_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType WebCamBehaviour_t88_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42794_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42794_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7549_il2cpp_TypeInfo/* declaring_type */
	, &WebCamBehaviour_t88_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7549_IList_1_get_Item_m42794_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42794_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType WebCamBehaviour_t88_0_0_0;
static ParameterInfo IList_1_t7549_IList_1_set_Item_m42795_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t88_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42795_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42795_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7549_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7549_IList_1_set_Item_m42795_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42795_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7549_MethodInfos[] =
{
	&IList_1_IndexOf_m42796_MethodInfo,
	&IList_1_Insert_m42797_MethodInfo,
	&IList_1_RemoveAt_m42798_MethodInfo,
	&IList_1_get_Item_m42794_MethodInfo,
	&IList_1_set_Item_m42795_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7549_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7548_il2cpp_TypeInfo,
	&IEnumerable_1_t7550_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7549_0_0_0;
extern Il2CppType IList_1_t7549_1_0_0;
struct IList_1_t7549;
extern Il2CppGenericClass IList_1_t7549_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7549_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7549_MethodInfos/* methods */
	, IList_1_t7549_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7549_il2cpp_TypeInfo/* element_class */
	, IList_1_t7549_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7549_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7549_0_0_0/* byval_arg */
	, &IList_1_t7549_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7549_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7551_il2cpp_TypeInfo;

// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehav.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42799_MethodInfo;
static PropertyInfo ICollection_1_t7551____Count_PropertyInfo = 
{
	&ICollection_1_t7551_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42799_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42800_MethodInfo;
static PropertyInfo ICollection_1_t7551____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7551_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42800_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7551_PropertyInfos[] =
{
	&ICollection_1_t7551____Count_PropertyInfo,
	&ICollection_1_t7551____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42799_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42799_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42799_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42800_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42800_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42800_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamAbstractBehaviour_t89_0_0_0;
extern Il2CppType WebCamAbstractBehaviour_t89_0_0_0;
static ParameterInfo ICollection_1_t7551_ICollection_1_Add_m42801_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamAbstractBehaviour_t89_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42801_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42801_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7551_ICollection_1_Add_m42801_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42801_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42802_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42802_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42802_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamAbstractBehaviour_t89_0_0_0;
static ParameterInfo ICollection_1_t7551_ICollection_1_Contains_m42803_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamAbstractBehaviour_t89_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42803_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42803_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7551_ICollection_1_Contains_m42803_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42803_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamAbstractBehaviourU5BU5D_t5523_0_0_0;
extern Il2CppType WebCamAbstractBehaviourU5BU5D_t5523_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7551_ICollection_1_CopyTo_m42804_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WebCamAbstractBehaviourU5BU5D_t5523_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42804_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42804_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7551_ICollection_1_CopyTo_m42804_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42804_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamAbstractBehaviour_t89_0_0_0;
static ParameterInfo ICollection_1_t7551_ICollection_1_Remove_m42805_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamAbstractBehaviour_t89_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42805_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WebCamAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42805_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7551_ICollection_1_Remove_m42805_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42805_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7551_MethodInfos[] =
{
	&ICollection_1_get_Count_m42799_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42800_MethodInfo,
	&ICollection_1_Add_m42801_MethodInfo,
	&ICollection_1_Clear_m42802_MethodInfo,
	&ICollection_1_Contains_m42803_MethodInfo,
	&ICollection_1_CopyTo_m42804_MethodInfo,
	&ICollection_1_Remove_m42805_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7553_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7551_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7553_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7551_0_0_0;
extern Il2CppType ICollection_1_t7551_1_0_0;
struct ICollection_1_t7551;
extern Il2CppGenericClass ICollection_1_t7551_GenericClass;
TypeInfo ICollection_1_t7551_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7551_MethodInfos/* methods */
	, ICollection_1_t7551_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7551_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7551_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7551_0_0_0/* byval_arg */
	, &ICollection_1_t7551_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7551_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WebCamAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.WebCamAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5923_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42806_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WebCamAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42806_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7553_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5923_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42806_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7553_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42806_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7553_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7553_0_0_0;
extern Il2CppType IEnumerable_1_t7553_1_0_0;
struct IEnumerable_1_t7553;
extern Il2CppGenericClass IEnumerable_1_t7553_GenericClass;
TypeInfo IEnumerable_1_t7553_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7553_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7553_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7553_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7553_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7553_0_0_0/* byval_arg */
	, &IEnumerable_1_t7553_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7553_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5923_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.WebCamAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.WebCamAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42807_MethodInfo;
static PropertyInfo IEnumerator_1_t5923____Current_PropertyInfo = 
{
	&IEnumerator_1_t5923_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42807_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5923_PropertyInfos[] =
{
	&IEnumerator_1_t5923____Current_PropertyInfo,
	NULL
};
extern Il2CppType WebCamAbstractBehaviour_t89_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42807_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.WebCamAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42807_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5923_il2cpp_TypeInfo/* declaring_type */
	, &WebCamAbstractBehaviour_t89_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42807_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5923_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42807_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5923_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5923_0_0_0;
extern Il2CppType IEnumerator_1_t5923_1_0_0;
struct IEnumerator_1_t5923;
extern Il2CppGenericClass IEnumerator_1_t5923_GenericClass;
TypeInfo IEnumerator_1_t5923_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5923_MethodInfos/* methods */
	, IEnumerator_1_t5923_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5923_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5923_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5923_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5923_0_0_0/* byval_arg */
	, &IEnumerator_1_t5923_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5923_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_122.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2977_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_122MethodDeclarations.h"

extern TypeInfo WebCamAbstractBehaviour_t89_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15300_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWebCamAbstractBehaviour_t89_m32862_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.WebCamAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.WebCamAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisWebCamAbstractBehaviour_t89_m32862(__this, p0, method) (WebCamAbstractBehaviour_t89 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2977____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2977_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2977, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2977____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2977_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2977, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2977_FieldInfos[] =
{
	&InternalEnumerator_1_t2977____array_0_FieldInfo,
	&InternalEnumerator_1_t2977____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15297_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2977____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2977_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15297_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2977____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2977_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15300_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2977_PropertyInfos[] =
{
	&InternalEnumerator_1_t2977____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2977____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2977_InternalEnumerator_1__ctor_m15296_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15296_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15296_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2977_InternalEnumerator_1__ctor_m15296_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15296_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15297_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15297_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2977_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15297_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15298_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15298_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15298_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15299_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15299_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2977_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15299_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamAbstractBehaviour_t89_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15300_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.WebCamAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15300_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2977_il2cpp_TypeInfo/* declaring_type */
	, &WebCamAbstractBehaviour_t89_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15300_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2977_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15296_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15297_MethodInfo,
	&InternalEnumerator_1_Dispose_m15298_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15299_MethodInfo,
	&InternalEnumerator_1_get_Current_m15300_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15299_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15298_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2977_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15297_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15299_MethodInfo,
	&InternalEnumerator_1_Dispose_m15298_MethodInfo,
	&InternalEnumerator_1_get_Current_m15300_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2977_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5923_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2977_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5923_il2cpp_TypeInfo, 7},
};
extern TypeInfo WebCamAbstractBehaviour_t89_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2977_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15300_MethodInfo/* Method Usage */,
	&WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisWebCamAbstractBehaviour_t89_m32862_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2977_0_0_0;
extern Il2CppType InternalEnumerator_1_t2977_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2977_GenericClass;
TypeInfo InternalEnumerator_1_t2977_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2977_MethodInfos/* methods */
	, InternalEnumerator_1_t2977_PropertyInfos/* properties */
	, InternalEnumerator_1_t2977_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2977_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2977_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2977_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2977_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2977_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2977_1_0_0/* this_arg */
	, InternalEnumerator_1_t2977_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2977_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2977_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2977)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7552_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42808_MethodInfo;
extern MethodInfo IList_1_set_Item_m42809_MethodInfo;
static PropertyInfo IList_1_t7552____Item_PropertyInfo = 
{
	&IList_1_t7552_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42808_MethodInfo/* get */
	, &IList_1_set_Item_m42809_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7552_PropertyInfos[] =
{
	&IList_1_t7552____Item_PropertyInfo,
	NULL
};
extern Il2CppType WebCamAbstractBehaviour_t89_0_0_0;
static ParameterInfo IList_1_t7552_IList_1_IndexOf_m42810_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WebCamAbstractBehaviour_t89_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42810_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42810_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7552_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7552_IList_1_IndexOf_m42810_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42810_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType WebCamAbstractBehaviour_t89_0_0_0;
static ParameterInfo IList_1_t7552_IList_1_Insert_m42811_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WebCamAbstractBehaviour_t89_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42811_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42811_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7552_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7552_IList_1_Insert_m42811_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42811_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7552_IList_1_RemoveAt_m42812_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42812_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42812_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7552_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7552_IList_1_RemoveAt_m42812_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42812_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7552_IList_1_get_Item_m42808_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType WebCamAbstractBehaviour_t89_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42808_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42808_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7552_il2cpp_TypeInfo/* declaring_type */
	, &WebCamAbstractBehaviour_t89_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7552_IList_1_get_Item_m42808_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42808_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType WebCamAbstractBehaviour_t89_0_0_0;
static ParameterInfo IList_1_t7552_IList_1_set_Item_m42809_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WebCamAbstractBehaviour_t89_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42809_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WebCamAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42809_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7552_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7552_IList_1_set_Item_m42809_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42809_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7552_MethodInfos[] =
{
	&IList_1_IndexOf_m42810_MethodInfo,
	&IList_1_Insert_m42811_MethodInfo,
	&IList_1_RemoveAt_m42812_MethodInfo,
	&IList_1_get_Item_m42808_MethodInfo,
	&IList_1_set_Item_m42809_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7552_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7551_il2cpp_TypeInfo,
	&IEnumerable_1_t7553_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7552_0_0_0;
extern Il2CppType IList_1_t7552_1_0_0;
struct IList_1_t7552;
extern Il2CppGenericClass IList_1_t7552_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7552_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7552_MethodInfos/* methods */
	, IList_1_t7552_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7552_il2cpp_TypeInfo/* element_class */
	, IList_1_t7552_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7552_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7552_0_0_0/* byval_arg */
	, &IList_1_t7552_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7552_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_44.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2978_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_44MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_40.h"
extern TypeInfo InvokableCall_1_t2979_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_40MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15303_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15305_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2978____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2978_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2978, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2978_FieldInfos[] =
{
	&CachedInvokableCall_1_t2978____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType WebCamBehaviour_t88_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2978_CachedInvokableCall_1__ctor_m15301_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t88_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15301_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15301_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2978_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2978_CachedInvokableCall_1__ctor_m15301_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15301_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2978_CachedInvokableCall_1_Invoke_m15302_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15302_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.WebCamBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15302_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2978_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2978_CachedInvokableCall_1_Invoke_m15302_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15302_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2978_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15301_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15302_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15302_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15306_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2978_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15302_MethodInfo,
	&InvokableCall_1_Find_m15306_MethodInfo,
};
extern Il2CppType UnityAction_1_t2980_0_0_0;
extern TypeInfo UnityAction_1_t2980_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisWebCamBehaviour_t88_m32872_MethodInfo;
extern TypeInfo WebCamBehaviour_t88_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15308_MethodInfo;
extern TypeInfo WebCamBehaviour_t88_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2978_RGCTXData[8] = 
{
	&UnityAction_1_t2980_0_0_0/* Type Usage */,
	&UnityAction_1_t2980_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWebCamBehaviour_t88_m32872_MethodInfo/* Method Usage */,
	&WebCamBehaviour_t88_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15308_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15303_MethodInfo/* Method Usage */,
	&WebCamBehaviour_t88_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15305_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2978_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2978_1_0_0;
struct CachedInvokableCall_1_t2978;
extern Il2CppGenericClass CachedInvokableCall_1_t2978_GenericClass;
TypeInfo CachedInvokableCall_1_t2978_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2978_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2978_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2979_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2978_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2978_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2978_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2978_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2978_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2978_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2978_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2978)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_47.h"
extern TypeInfo UnityAction_1_t2980_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_47MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.WebCamBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.WebCamBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisWebCamBehaviour_t88_m32872(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>
extern Il2CppType UnityAction_1_t2980_0_0_1;
FieldInfo InvokableCall_1_t2979____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2980_0_0_1/* type */
	, &InvokableCall_1_t2979_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2979, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2979_FieldInfos[] =
{
	&InvokableCall_1_t2979____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2979_InvokableCall_1__ctor_m15303_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15303_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15303_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2979_InvokableCall_1__ctor_m15303_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15303_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2980_0_0_0;
static ParameterInfo InvokableCall_1_t2979_InvokableCall_1__ctor_m15304_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2980_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15304_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15304_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2979_InvokableCall_1__ctor_m15304_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15304_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2979_InvokableCall_1_Invoke_m15305_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15305_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15305_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2979_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2979_InvokableCall_1_Invoke_m15305_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15305_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2979_InvokableCall_1_Find_m15306_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15306_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15306_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2979_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2979_InvokableCall_1_Find_m15306_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15306_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2979_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15303_MethodInfo,
	&InvokableCall_1__ctor_m15304_MethodInfo,
	&InvokableCall_1_Invoke_m15305_MethodInfo,
	&InvokableCall_1_Find_m15306_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2979_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m15305_MethodInfo,
	&InvokableCall_1_Find_m15306_MethodInfo,
};
extern TypeInfo UnityAction_1_t2980_il2cpp_TypeInfo;
extern TypeInfo WebCamBehaviour_t88_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2979_RGCTXData[5] = 
{
	&UnityAction_1_t2980_0_0_0/* Type Usage */,
	&UnityAction_1_t2980_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWebCamBehaviour_t88_m32872_MethodInfo/* Method Usage */,
	&WebCamBehaviour_t88_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15308_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2979_0_0_0;
extern Il2CppType InvokableCall_1_t2979_1_0_0;
struct InvokableCall_1_t2979;
extern Il2CppGenericClass InvokableCall_1_t2979_GenericClass;
TypeInfo InvokableCall_1_t2979_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2979_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2979_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2979_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2979_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2979_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2979_0_0_0/* byval_arg */
	, &InvokableCall_1_t2979_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2979_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2979_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2979)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2980_UnityAction_1__ctor_m15307_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15307_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15307_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2980_UnityAction_1__ctor_m15307_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15307_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamBehaviour_t88_0_0_0;
static ParameterInfo UnityAction_1_t2980_UnityAction_1_Invoke_m15308_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t88_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15308_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15308_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2980_UnityAction_1_Invoke_m15308_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15308_GenericMethod/* genericMethod */

};
extern Il2CppType WebCamBehaviour_t88_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2980_UnityAction_1_BeginInvoke_m15309_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WebCamBehaviour_t88_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15309_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15309_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2980_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2980_UnityAction_1_BeginInvoke_m15309_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15309_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2980_UnityAction_1_EndInvoke_m15310_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15310_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15310_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2980_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2980_UnityAction_1_EndInvoke_m15310_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15310_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2980_MethodInfos[] =
{
	&UnityAction_1__ctor_m15307_MethodInfo,
	&UnityAction_1_Invoke_m15308_MethodInfo,
	&UnityAction_1_BeginInvoke_m15309_MethodInfo,
	&UnityAction_1_EndInvoke_m15310_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15309_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15310_MethodInfo;
static MethodInfo* UnityAction_1_t2980_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m15308_MethodInfo,
	&UnityAction_1_BeginInvoke_m15309_MethodInfo,
	&UnityAction_1_EndInvoke_m15310_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2980_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2980_1_0_0;
struct UnityAction_1_t2980;
extern Il2CppGenericClass UnityAction_1_t2980_GenericClass;
TypeInfo UnityAction_1_t2980_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2980_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2980_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2980_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2980_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2980_0_0_0/* byval_arg */
	, &UnityAction_1_t2980_1_0_0/* this_arg */
	, UnityAction_1_t2980_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2980_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2980)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5925_il2cpp_TypeInfo;

// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.WireframeBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.WireframeBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42813_MethodInfo;
static PropertyInfo IEnumerator_1_t5925____Current_PropertyInfo = 
{
	&IEnumerator_1_t5925_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42813_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5925_PropertyInfos[] =
{
	&IEnumerator_1_t5925____Current_PropertyInfo,
	NULL
};
extern Il2CppType WireframeBehaviour_t90_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42813_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.WireframeBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42813_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5925_il2cpp_TypeInfo/* declaring_type */
	, &WireframeBehaviour_t90_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42813_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5925_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42813_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5925_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5925_0_0_0;
extern Il2CppType IEnumerator_1_t5925_1_0_0;
struct IEnumerator_1_t5925;
extern Il2CppGenericClass IEnumerator_1_t5925_GenericClass;
TypeInfo IEnumerator_1_t5925_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5925_MethodInfos/* methods */
	, IEnumerator_1_t5925_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5925_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5925_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5925_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5925_0_0_0/* byval_arg */
	, &IEnumerator_1_t5925_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5925_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_123.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2981_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_123MethodDeclarations.h"

extern TypeInfo WireframeBehaviour_t90_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15315_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWireframeBehaviour_t90_m32874_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.WireframeBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.WireframeBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisWireframeBehaviour_t90_m32874(__this, p0, method) (WireframeBehaviour_t90 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2981____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2981_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2981, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2981____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2981_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2981, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2981_FieldInfos[] =
{
	&InternalEnumerator_1_t2981____array_0_FieldInfo,
	&InternalEnumerator_1_t2981____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15312_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2981____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2981_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15312_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2981____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2981_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15315_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2981_PropertyInfos[] =
{
	&InternalEnumerator_1_t2981____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2981____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2981_InternalEnumerator_1__ctor_m15311_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15311_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15311_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2981_InternalEnumerator_1__ctor_m15311_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15311_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15312_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15312_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2981_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15312_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15313_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15313_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2981_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15313_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15314_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15314_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2981_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15314_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeBehaviour_t90_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15315_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.WireframeBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15315_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2981_il2cpp_TypeInfo/* declaring_type */
	, &WireframeBehaviour_t90_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15315_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2981_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15311_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15312_MethodInfo,
	&InternalEnumerator_1_Dispose_m15313_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15314_MethodInfo,
	&InternalEnumerator_1_get_Current_m15315_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15314_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15313_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2981_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15312_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15314_MethodInfo,
	&InternalEnumerator_1_Dispose_m15313_MethodInfo,
	&InternalEnumerator_1_get_Current_m15315_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2981_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5925_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2981_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5925_il2cpp_TypeInfo, 7},
};
extern TypeInfo WireframeBehaviour_t90_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2981_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15315_MethodInfo/* Method Usage */,
	&WireframeBehaviour_t90_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisWireframeBehaviour_t90_m32874_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2981_0_0_0;
extern Il2CppType InternalEnumerator_1_t2981_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2981_GenericClass;
TypeInfo InternalEnumerator_1_t2981_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2981_MethodInfos/* methods */
	, InternalEnumerator_1_t2981_PropertyInfos/* properties */
	, InternalEnumerator_1_t2981_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2981_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2981_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2981_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2981_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2981_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2981_1_0_0/* this_arg */
	, InternalEnumerator_1_t2981_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2981_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2981_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2981)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7554_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>
extern MethodInfo ICollection_1_get_Count_m42814_MethodInfo;
static PropertyInfo ICollection_1_t7554____Count_PropertyInfo = 
{
	&ICollection_1_t7554_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42814_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42815_MethodInfo;
static PropertyInfo ICollection_1_t7554____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7554_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42815_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7554_PropertyInfos[] =
{
	&ICollection_1_t7554____Count_PropertyInfo,
	&ICollection_1_t7554____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42814_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42814_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42814_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42815_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42815_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42815_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeBehaviour_t90_0_0_0;
extern Il2CppType WireframeBehaviour_t90_0_0_0;
static ParameterInfo ICollection_1_t7554_ICollection_1_Add_m42816_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t90_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42816_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42816_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7554_ICollection_1_Add_m42816_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42816_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42817_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42817_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42817_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeBehaviour_t90_0_0_0;
static ParameterInfo ICollection_1_t7554_ICollection_1_Contains_m42818_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t90_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42818_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42818_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7554_ICollection_1_Contains_m42818_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42818_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeBehaviourU5BU5D_t177_0_0_0;
extern Il2CppType WireframeBehaviourU5BU5D_t177_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7554_ICollection_1_CopyTo_m42819_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviourU5BU5D_t177_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42819_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42819_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7554_ICollection_1_CopyTo_m42819_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42819_GenericMethod/* genericMethod */

};
extern Il2CppType WireframeBehaviour_t90_0_0_0;
static ParameterInfo ICollection_1_t7554_ICollection_1_Remove_m42820_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t90_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42820_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.WireframeBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42820_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7554_ICollection_1_Remove_m42820_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42820_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7554_MethodInfos[] =
{
	&ICollection_1_get_Count_m42814_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42815_MethodInfo,
	&ICollection_1_Add_m42816_MethodInfo,
	&ICollection_1_Clear_m42817_MethodInfo,
	&ICollection_1_Contains_m42818_MethodInfo,
	&ICollection_1_CopyTo_m42819_MethodInfo,
	&ICollection_1_Remove_m42820_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7556_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7554_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7556_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7554_0_0_0;
extern Il2CppType ICollection_1_t7554_1_0_0;
struct ICollection_1_t7554;
extern Il2CppGenericClass ICollection_1_t7554_GenericClass;
TypeInfo ICollection_1_t7554_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7554_MethodInfos/* methods */
	, ICollection_1_t7554_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7554_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7554_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7554_0_0_0/* byval_arg */
	, &ICollection_1_t7554_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7554_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WireframeBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.WireframeBehaviour>
extern Il2CppType IEnumerator_1_t5925_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42821_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.WireframeBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42821_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7556_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5925_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42821_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7556_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42821_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7556_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7556_0_0_0;
extern Il2CppType IEnumerable_1_t7556_1_0_0;
struct IEnumerable_1_t7556;
extern Il2CppGenericClass IEnumerable_1_t7556_GenericClass;
TypeInfo IEnumerable_1_t7556_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7556_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7556_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7556_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7556_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7556_0_0_0/* byval_arg */
	, &IEnumerable_1_t7556_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7556_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7555_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>
extern MethodInfo IList_1_get_Item_m42822_MethodInfo;
extern MethodInfo IList_1_set_Item_m42823_MethodInfo;
static PropertyInfo IList_1_t7555____Item_PropertyInfo = 
{
	&IList_1_t7555_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42822_MethodInfo/* get */
	, &IList_1_set_Item_m42823_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7555_PropertyInfos[] =
{
	&IList_1_t7555____Item_PropertyInfo,
	NULL
};
extern Il2CppType WireframeBehaviour_t90_0_0_0;
static ParameterInfo IList_1_t7555_IList_1_IndexOf_m42824_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t90_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42824_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42824_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7555_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7555_IList_1_IndexOf_m42824_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42824_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType WireframeBehaviour_t90_0_0_0;
static ParameterInfo IList_1_t7555_IList_1_Insert_m42825_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t90_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42825_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42825_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7555_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7555_IList_1_Insert_m42825_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42825_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7555_IList_1_RemoveAt_m42826_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42826_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42826_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7555_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7555_IList_1_RemoveAt_m42826_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42826_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7555_IList_1_get_Item_m42822_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType WireframeBehaviour_t90_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42822_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42822_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7555_il2cpp_TypeInfo/* declaring_type */
	, &WireframeBehaviour_t90_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7555_IList_1_get_Item_m42822_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42822_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType WireframeBehaviour_t90_0_0_0;
static ParameterInfo IList_1_t7555_IList_1_set_Item_m42823_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WireframeBehaviour_t90_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42823_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.WireframeBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42823_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7555_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7555_IList_1_set_Item_m42823_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42823_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7555_MethodInfos[] =
{
	&IList_1_IndexOf_m42824_MethodInfo,
	&IList_1_Insert_m42825_MethodInfo,
	&IList_1_RemoveAt_m42826_MethodInfo,
	&IList_1_get_Item_m42822_MethodInfo,
	&IList_1_set_Item_m42823_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7555_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7554_il2cpp_TypeInfo,
	&IEnumerable_1_t7556_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7555_0_0_0;
extern Il2CppType IList_1_t7555_1_0_0;
struct IList_1_t7555;
extern Il2CppGenericClass IList_1_t7555_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7555_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7555_MethodInfos/* methods */
	, IList_1_t7555_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7555_il2cpp_TypeInfo/* element_class */
	, IList_1_t7555_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7555_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7555_0_0_0/* byval_arg */
	, &IList_1_t7555_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7555_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
