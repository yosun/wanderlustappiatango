﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoTextureRenderer
struct VideoTextureRenderer_t85;

// System.Void Vuforia.VideoTextureRenderer::.ctor()
 void VideoTextureRenderer__ctor_m178 (VideoTextureRenderer_t85 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
