﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t2707;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<System.Object>
struct InvokableCall_1_t2706  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<System.Object>::Delegate
	UnityAction_1_t2707 * ___Delegate_0;
};
