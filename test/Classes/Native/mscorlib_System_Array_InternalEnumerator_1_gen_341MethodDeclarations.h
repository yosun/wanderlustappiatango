﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct InternalEnumerator_1_t4225;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m25546 (InternalEnumerator_1_t4225 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25547 (InternalEnumerator_1_t4225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
 void InternalEnumerator_1_Dispose_m25548 (InternalEnumerator_1_t4225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m25549 (InternalEnumerator_1_t4225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
 TargetSearchResult_t732  InternalEnumerator_1_get_Current_m25550 (InternalEnumerator_1_t4225 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
