﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Math
struct Math_t2223;
// System.Decimal
#include "mscorlib_System_Decimal.h"

// System.Single System.Math::Abs(System.Single)
 float Math_Abs_m4533 (Object_t * __this/* static, unused */, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Abs(System.Int32)
 int32_t Math_Abs_m12957 (Object_t * __this/* static, unused */, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.Math::Abs(System.Int64)
 int64_t Math_Abs_m12958 (Object_t * __this/* static, unused */, int64_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Ceiling(System.Double)
 double Math_Ceiling_m6460 (Object_t * __this/* static, unused */, double ___a, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Floor(System.Double)
 double Math_Floor_m6458 (Object_t * __this/* static, unused */, double ___d, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Log(System.Double,System.Double)
 double Math_Log_m6457 (Object_t * __this/* static, unused */, double ___a, double ___newBase, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.Math::Max(System.Single,System.Single)
 float Math_Max_m4534 (Object_t * __this/* static, unused */, float ___val1, float ___val2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Max(System.Int32,System.Int32)
 int32_t Math_Max_m8818 (Object_t * __this/* static, unused */, int32_t ___val1, int32_t ___val2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Min(System.Int32,System.Int32)
 int32_t Math_Min_m12959 (Object_t * __this/* static, unused */, int32_t ___val1, int32_t ___val2, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.Math::Round(System.Decimal)
 Decimal_t1687  Math_Round_m12960 (Object_t * __this/* static, unused */, Decimal_t1687  ___d, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Round(System.Double)
 double Math_Round_m6459 (Object_t * __this/* static, unused */, double ___a, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Sin(System.Double)
 double Math_Sin_m6452 (Object_t * __this/* static, unused */, double ___a, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Cos(System.Double)
 double Math_Cos_m6453 (Object_t * __this/* static, unused */, double ___d, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Atan2(System.Double,System.Double)
 double Math_Atan2_m6454 (Object_t * __this/* static, unused */, double ___y, double ___x, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Log(System.Double)
 double Math_Log_m12961 (Object_t * __this/* static, unused */, double ___d, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Pow(System.Double,System.Double)
 double Math_Pow_m6456 (Object_t * __this/* static, unused */, double ___x, double ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.Math::Sqrt(System.Double)
 double Math_Sqrt_m6455 (Object_t * __this/* static, unused */, double ___d, MethodInfo* method) IL2CPP_METHOD_ATTR;
