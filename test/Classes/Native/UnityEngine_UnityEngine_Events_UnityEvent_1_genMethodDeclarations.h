﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>
struct UnityEvent_1_t192;
// UnityEngine.Events.UnityAction`1<UnityEngine.EventSystems.BaseEventData>
struct UnityAction_1_t3121;
// System.Reflection.MethodInfo
struct MethodInfo_t142;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1075;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t184;

// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::.ctor()
// UnityEngine.Events.UnityEvent`1<System.Object>
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen_5MethodDeclarations.h"
#define UnityEvent_1__ctor_m1902(__this, method) (void)UnityEvent_1__ctor_m16438_gshared((UnityEvent_1_t3122 *)__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_AddListener_m16439(__this, ___call, method) (void)UnityEvent_1_AddListener_m16440_gshared((UnityEvent_1_t3122 *)__this, (UnityAction_1_t2707 *)___call, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_RemoveListener_m16441(__this, ___call, method) (void)UnityEvent_1_RemoveListener_m16442_gshared((UnityEvent_1_t3122 *)__this, (UnityAction_1_t2707 *)___call, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::FindMethod_Impl(System.String,System.Object)
#define UnityEvent_1_FindMethod_Impl_m1906(__this, ___name, ___targetObj, method) (MethodInfo_t142 *)UnityEvent_1_FindMethod_Impl_m16443_gshared((UnityEvent_1_t3122 *)__this, (String_t*)___name, (Object_t *)___targetObj, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::GetDelegate(System.Object,System.Reflection.MethodInfo)
#define UnityEvent_1_GetDelegate_m1907(__this, ___target, ___theFunction, method) (BaseInvokableCall_t1075 *)UnityEvent_1_GetDelegate_m16444_gshared((UnityEvent_1_t3122 *)__this, (Object_t *)___target, (MethodInfo_t142 *)___theFunction, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_GetDelegate_m1908(__this/* static, unused */, ___action, method) (BaseInvokableCall_t1075 *)UnityEvent_1_GetDelegate_m16445_gshared((Object_t *)__this/* static, unused */, (UnityAction_1_t2707 *)___action, method)
// System.Void UnityEngine.Events.UnityEvent`1<UnityEngine.EventSystems.BaseEventData>::Invoke(T0)
#define UnityEvent_1_Invoke_m1913(__this, ___arg0, method) (void)UnityEvent_1_Invoke_m16446_gshared((UnityEvent_1_t3122 *)__this, (Object_t *)___arg0, method)
