﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Material>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_136.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo UnityAction_1_t4590_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Material>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_136MethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Material>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Material>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Material>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Material>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Material>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4590_UnityAction_1__ctor_m28191_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28191_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Material>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28191_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t4590_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4590_UnityAction_1__ctor_m28191_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28191_GenericMethod/* genericMethod */

};
extern Il2CppType Material_t4_0_0_0;
extern Il2CppType Material_t4_0_0_0;
static ParameterInfo UnityAction_1_t4590_UnityAction_1_Invoke_m28192_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Material_t4_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28192_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Material>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28192_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t4590_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4590_UnityAction_1_Invoke_m28192_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28192_GenericMethod/* genericMethod */

};
extern Il2CppType Material_t4_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4590_UnityAction_1_BeginInvoke_m28193_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Material_t4_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28193_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Material>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28193_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t4590_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4590_UnityAction_1_BeginInvoke_m28193_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28193_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t4590_UnityAction_1_EndInvoke_m28194_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28194_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Material>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28194_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t4590_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4590_UnityAction_1_EndInvoke_m28194_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28194_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4590_MethodInfos[] =
{
	&UnityAction_1__ctor_m28191_MethodInfo,
	&UnityAction_1_Invoke_m28192_MethodInfo,
	&UnityAction_1_BeginInvoke_m28193_MethodInfo,
	&UnityAction_1_EndInvoke_m28194_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2241_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2242_MethodInfo;
extern MethodInfo Object_ToString_m306_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2243_MethodInfo;
extern MethodInfo Delegate_Clone_m2244_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2245_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2246_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2247_MethodInfo;
extern MethodInfo UnityAction_1_Invoke_m28192_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m28193_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28194_MethodInfo;
static MethodInfo* UnityAction_1_t4590_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m28192_MethodInfo,
	&UnityAction_1_BeginInvoke_m28193_MethodInfo,
	&UnityAction_1_EndInvoke_m28194_MethodInfo,
};
extern TypeInfo ICloneable_t481_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t482_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t4590_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4590_0_0_0;
extern Il2CppType UnityAction_1_t4590_1_0_0;
extern TypeInfo MulticastDelegate_t325_il2cpp_TypeInfo;
struct UnityAction_1_t4590;
extern Il2CppGenericClass UnityAction_1_t4590_GenericClass;
TypeInfo UnityAction_1_t4590_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4590_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4590_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4590_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4590_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4590_0_0_0/* byval_arg */
	, &UnityAction_1_t4590_1_0_0/* this_arg */
	, UnityAction_1_t4590_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4590_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4590)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6554_il2cpp_TypeInfo;

// UnityEngine.Sprite
#include "UnityEngine_UnityEngine_Sprite.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Sprite>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Sprite>
extern MethodInfo IEnumerator_1_get_Current_m47241_MethodInfo;
static PropertyInfo IEnumerator_1_t6554____Current_PropertyInfo = 
{
	&IEnumerator_1_t6554_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m47241_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6554_PropertyInfos[] =
{
	&IEnumerator_1_t6554____Current_PropertyInfo,
	NULL
};
extern Il2CppType Sprite_t310_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m47241_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Sprite>::get_Current()
MethodInfo IEnumerator_1_get_Current_m47241_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6554_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t310_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m47241_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6554_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m47241_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t266_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t6554_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6554_0_0_0;
extern Il2CppType IEnumerator_1_t6554_1_0_0;
struct IEnumerator_1_t6554;
extern Il2CppGenericClass IEnumerator_1_t6554_GenericClass;
TypeInfo IEnumerator_1_t6554_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6554_MethodInfos/* methods */
	, IEnumerator_1_t6554_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6554_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6554_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6554_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6554_0_0_0/* byval_arg */
	, &IEnumerator_1_t6554_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6554_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Sprite>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_409.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4591_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Sprite>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_409MethodDeclarations.h"

// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo Sprite_t310_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1493_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m28199_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7651_MethodInfo;
extern MethodInfo Array_get_Length_m7656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSprite_t310_m37228_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m31436_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m31436(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Sprite>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Sprite>(System.Int32)
#define Array_InternalArray__get_Item_TisSprite_t310_m37228(__this, p0, method) (Sprite_t310 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Sprite>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Sprite>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Sprite>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Sprite>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Sprite>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Sprite>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4591____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4591_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4591, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4591____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4591_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4591, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4591_FieldInfos[] =
{
	&InternalEnumerator_1_t4591____array_0_FieldInfo,
	&InternalEnumerator_1_t4591____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28196_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4591____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4591_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28196_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4591____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4591_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28199_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4591_PropertyInfos[] =
{
	&InternalEnumerator_1_t4591____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4591____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4591_InternalEnumerator_1__ctor_m28195_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28195_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Sprite>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28195_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4591_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4591_InternalEnumerator_1__ctor_m28195_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28195_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28196_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Sprite>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28196_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4591_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28196_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28197_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Sprite>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28197_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4591_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28197_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28198_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Sprite>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28198_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4591_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28198_GenericMethod/* genericMethod */

};
extern Il2CppType Sprite_t310_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28199_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Sprite>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28199_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4591_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t310_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28199_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4591_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28195_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28196_MethodInfo,
	&InternalEnumerator_1_Dispose_m28197_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28198_MethodInfo,
	&InternalEnumerator_1_get_Current_m28199_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m28198_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28197_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4591_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28196_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28198_MethodInfo,
	&InternalEnumerator_1_Dispose_m28197_MethodInfo,
	&InternalEnumerator_1_get_Current_m28199_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4591_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6554_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4591_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6554_il2cpp_TypeInfo, 7},
};
extern TypeInfo Sprite_t310_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4591_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28199_MethodInfo/* Method Usage */,
	&Sprite_t310_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSprite_t310_m37228_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4591_0_0_0;
extern Il2CppType InternalEnumerator_1_t4591_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t4591_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t4591_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4591_MethodInfos/* methods */
	, InternalEnumerator_1_t4591_PropertyInfos/* properties */
	, InternalEnumerator_1_t4591_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4591_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4591_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4591_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4591_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4591_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4591_1_0_0/* this_arg */
	, InternalEnumerator_1_t4591_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4591_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4591_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4591)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8284_il2cpp_TypeInfo;

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Sprite>
extern MethodInfo ICollection_1_get_Count_m47242_MethodInfo;
static PropertyInfo ICollection_1_t8284____Count_PropertyInfo = 
{
	&ICollection_1_t8284_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m47242_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m47243_MethodInfo;
static PropertyInfo ICollection_1_t8284____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8284_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m47243_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8284_PropertyInfos[] =
{
	&ICollection_1_t8284____Count_PropertyInfo,
	&ICollection_1_t8284____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m47242_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::get_Count()
MethodInfo ICollection_1_get_Count_m47242_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8284_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m47242_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m47243_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m47243_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8284_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m47243_GenericMethod/* genericMethod */

};
extern Il2CppType Sprite_t310_0_0_0;
extern Il2CppType Sprite_t310_0_0_0;
static ParameterInfo ICollection_1_t8284_ICollection_1_Add_m47244_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sprite_t310_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m47244_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Add(T)
MethodInfo ICollection_1_Add_m47244_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8284_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8284_ICollection_1_Add_m47244_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m47244_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m47245_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Clear()
MethodInfo ICollection_1_Clear_m47245_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8284_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m47245_GenericMethod/* genericMethod */

};
extern Il2CppType Sprite_t310_0_0_0;
static ParameterInfo ICollection_1_t8284_ICollection_1_Contains_m47246_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sprite_t310_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m47246_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Contains(T)
MethodInfo ICollection_1_Contains_m47246_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8284_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8284_ICollection_1_Contains_m47246_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m47246_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteU5BU5D_t5440_0_0_0;
extern Il2CppType SpriteU5BU5D_t5440_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8284_ICollection_1_CopyTo_m47247_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SpriteU5BU5D_t5440_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m47247_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m47247_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8284_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8284_ICollection_1_CopyTo_m47247_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m47247_GenericMethod/* genericMethod */

};
extern Il2CppType Sprite_t310_0_0_0;
static ParameterInfo ICollection_1_t8284_ICollection_1_Remove_m47248_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sprite_t310_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m47248_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Sprite>::Remove(T)
MethodInfo ICollection_1_Remove_m47248_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8284_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8284_ICollection_1_Remove_m47248_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m47248_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8284_MethodInfos[] =
{
	&ICollection_1_get_Count_m47242_MethodInfo,
	&ICollection_1_get_IsReadOnly_m47243_MethodInfo,
	&ICollection_1_Add_m47244_MethodInfo,
	&ICollection_1_Clear_m47245_MethodInfo,
	&ICollection_1_Contains_m47246_MethodInfo,
	&ICollection_1_CopyTo_m47247_MethodInfo,
	&ICollection_1_Remove_m47248_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8286_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8284_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8286_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8284_0_0_0;
extern Il2CppType ICollection_1_t8284_1_0_0;
struct ICollection_1_t8284;
extern Il2CppGenericClass ICollection_1_t8284_GenericClass;
TypeInfo ICollection_1_t8284_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8284_MethodInfos/* methods */
	, ICollection_1_t8284_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8284_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8284_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8284_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8284_0_0_0/* byval_arg */
	, &ICollection_1_t8284_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8284_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Sprite>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Sprite>
extern Il2CppType IEnumerator_1_t6554_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m47249_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Sprite>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m47249_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8286_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6554_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m47249_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8286_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m47249_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8286_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8286_0_0_0;
extern Il2CppType IEnumerable_1_t8286_1_0_0;
struct IEnumerable_1_t8286;
extern Il2CppGenericClass IEnumerable_1_t8286_GenericClass;
TypeInfo IEnumerable_1_t8286_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8286_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8286_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8286_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8286_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8286_0_0_0/* byval_arg */
	, &IEnumerable_1_t8286_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8286_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8285_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Sprite>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Sprite>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Sprite>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Sprite>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Sprite>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Sprite>
extern MethodInfo IList_1_get_Item_m47250_MethodInfo;
extern MethodInfo IList_1_set_Item_m47251_MethodInfo;
static PropertyInfo IList_1_t8285____Item_PropertyInfo = 
{
	&IList_1_t8285_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m47250_MethodInfo/* get */
	, &IList_1_set_Item_m47251_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8285_PropertyInfos[] =
{
	&IList_1_t8285____Item_PropertyInfo,
	NULL
};
extern Il2CppType Sprite_t310_0_0_0;
static ParameterInfo IList_1_t8285_IList_1_IndexOf_m47252_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sprite_t310_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m47252_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Sprite>::IndexOf(T)
MethodInfo IList_1_IndexOf_m47252_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8285_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8285_IList_1_IndexOf_m47252_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m47252_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Sprite_t310_0_0_0;
static ParameterInfo IList_1_t8285_IList_1_Insert_m47253_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Sprite_t310_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m47253_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Sprite>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m47253_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8285_IList_1_Insert_m47253_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m47253_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8285_IList_1_RemoveAt_m47254_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m47254_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Sprite>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m47254_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8285_IList_1_RemoveAt_m47254_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m47254_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8285_IList_1_get_Item_m47250_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Sprite_t310_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m47250_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Sprite>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m47250_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8285_il2cpp_TypeInfo/* declaring_type */
	, &Sprite_t310_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8285_IList_1_get_Item_m47250_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m47250_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Sprite_t310_0_0_0;
static ParameterInfo IList_1_t8285_IList_1_set_Item_m47251_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Sprite_t310_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m47251_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Sprite>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m47251_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8285_IList_1_set_Item_m47251_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m47251_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8285_MethodInfos[] =
{
	&IList_1_IndexOf_m47252_MethodInfo,
	&IList_1_Insert_m47253_MethodInfo,
	&IList_1_RemoveAt_m47254_MethodInfo,
	&IList_1_get_Item_m47250_MethodInfo,
	&IList_1_set_Item_m47251_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8285_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8284_il2cpp_TypeInfo,
	&IEnumerable_1_t8286_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8285_0_0_0;
extern Il2CppType IList_1_t8285_1_0_0;
struct IList_1_t8285;
extern Il2CppGenericClass IList_1_t8285_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8285_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8285_MethodInfos/* methods */
	, IList_1_t8285_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8285_il2cpp_TypeInfo/* element_class */
	, IList_1_t8285_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8285_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8285_0_0_0/* byval_arg */
	, &IList_1_t8285_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8285_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_130.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4592_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_130MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_132.h"
extern TypeInfo ObjectU5BU5D_t115_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4593_il2cpp_TypeInfo;
extern TypeInfo Void_t99_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_132MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28202_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28204_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t4592____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t4592_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4592, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4592_FieldInfos[] =
{
	&CachedInvokableCall_1_t4592____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType Sprite_t310_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4592_CachedInvokableCall_1__ctor_m28200_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Sprite_t310_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28200_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28200_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t4592_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4592_CachedInvokableCall_1__ctor_m28200_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28200_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4592_CachedInvokableCall_1_Invoke_m28201_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28201_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Sprite>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28201_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t4592_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4592_CachedInvokableCall_1_Invoke_m28201_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28201_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4592_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28200_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28201_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m304_MethodInfo;
extern MethodInfo Object_GetHashCode_m305_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m28201_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28205_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4592_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28201_MethodInfo,
	&InvokableCall_1_Find_m28205_MethodInfo,
};
extern Il2CppType UnityAction_1_t4594_0_0_0;
extern TypeInfo UnityAction_1_t4594_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisSprite_t310_m37238_MethodInfo;
extern TypeInfo Sprite_t310_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28207_MethodInfo;
extern TypeInfo Sprite_t310_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4592_RGCTXData[8] = 
{
	&UnityAction_1_t4594_0_0_0/* Type Usage */,
	&UnityAction_1_t4594_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSprite_t310_m37238_MethodInfo/* Method Usage */,
	&Sprite_t310_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28207_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28202_MethodInfo/* Method Usage */,
	&Sprite_t310_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28204_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4592_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4592_1_0_0;
struct CachedInvokableCall_1_t4592;
extern Il2CppGenericClass CachedInvokableCall_1_t4592_GenericClass;
TypeInfo CachedInvokableCall_1_t4592_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4592_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4592_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4593_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4592_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4592_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4592_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4592_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4592_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4592_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4592_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4592)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_137.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t4594_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t507_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_137MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6358_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m395_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m456_MethodInfo;
extern MethodInfo Delegate_Combine_m2149_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6357_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2468_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6359_MethodInfo;
extern MethodInfo Delegate_get_Target_m6536_MethodInfo;
extern MethodInfo Delegate_get_Method_m6534_MethodInfo;
struct BaseInvokableCall_t1075;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Sprite>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Sprite>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisSprite_t310_m37238(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>
extern Il2CppType UnityAction_1_t4594_0_0_1;
FieldInfo InvokableCall_1_t4593____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4594_0_0_1/* type */
	, &InvokableCall_1_t4593_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4593, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4593_FieldInfos[] =
{
	&InvokableCall_1_t4593____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4593_InvokableCall_1__ctor_m28202_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28202_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28202_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t4593_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4593_InvokableCall_1__ctor_m28202_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28202_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4594_0_0_0;
static ParameterInfo InvokableCall_1_t4593_InvokableCall_1__ctor_m28203_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4594_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28203_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28203_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t4593_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4593_InvokableCall_1__ctor_m28203_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28203_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t4593_InvokableCall_1_Invoke_m28204_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28204_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28204_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t4593_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4593_InvokableCall_1_Invoke_m28204_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28204_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4593_InvokableCall_1_Find_m28205_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28205_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Sprite>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28205_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t4593_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4593_InvokableCall_1_Find_m28205_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28205_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4593_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28202_MethodInfo,
	&InvokableCall_1__ctor_m28203_MethodInfo,
	&InvokableCall_1_Invoke_m28204_MethodInfo,
	&InvokableCall_1_Find_m28205_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4593_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m28204_MethodInfo,
	&InvokableCall_1_Find_m28205_MethodInfo,
};
extern TypeInfo UnityAction_1_t4594_il2cpp_TypeInfo;
extern TypeInfo Sprite_t310_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4593_RGCTXData[5] = 
{
	&UnityAction_1_t4594_0_0_0/* Type Usage */,
	&UnityAction_1_t4594_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSprite_t310_m37238_MethodInfo/* Method Usage */,
	&Sprite_t310_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28207_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4593_0_0_0;
extern Il2CppType InvokableCall_1_t4593_1_0_0;
extern TypeInfo BaseInvokableCall_t1075_il2cpp_TypeInfo;
struct InvokableCall_1_t4593;
extern Il2CppGenericClass InvokableCall_1_t4593_GenericClass;
TypeInfo InvokableCall_1_t4593_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4593_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4593_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4593_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4593_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4593_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4593_0_0_0/* byval_arg */
	, &InvokableCall_1_t4593_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4593_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4593_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4593)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4594_UnityAction_1__ctor_m28206_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28206_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28206_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t4594_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4594_UnityAction_1__ctor_m28206_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28206_GenericMethod/* genericMethod */

};
extern Il2CppType Sprite_t310_0_0_0;
static ParameterInfo UnityAction_1_t4594_UnityAction_1_Invoke_m28207_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Sprite_t310_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28207_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28207_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t4594_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4594_UnityAction_1_Invoke_m28207_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28207_GenericMethod/* genericMethod */

};
extern Il2CppType Sprite_t310_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4594_UnityAction_1_BeginInvoke_m28208_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Sprite_t310_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28208_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28208_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t4594_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4594_UnityAction_1_BeginInvoke_m28208_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28208_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t4594_UnityAction_1_EndInvoke_m28209_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28209_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Sprite>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28209_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t4594_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4594_UnityAction_1_EndInvoke_m28209_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28209_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4594_MethodInfos[] =
{
	&UnityAction_1__ctor_m28206_MethodInfo,
	&UnityAction_1_Invoke_m28207_MethodInfo,
	&UnityAction_1_BeginInvoke_m28208_MethodInfo,
	&UnityAction_1_EndInvoke_m28209_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28208_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28209_MethodInfo;
static MethodInfo* UnityAction_1_t4594_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m28207_MethodInfo,
	&UnityAction_1_BeginInvoke_m28208_MethodInfo,
	&UnityAction_1_EndInvoke_m28209_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4594_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4594_1_0_0;
struct UnityAction_1_t4594;
extern Il2CppGenericClass UnityAction_1_t4594_GenericClass;
TypeInfo UnityAction_1_t4594_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4594_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4594_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4594_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4594_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4594_0_0_0/* byval_arg */
	, &UnityAction_1_t4594_1_0_0/* this_arg */
	, UnityAction_1_t4594_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4594_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4594)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6556_il2cpp_TypeInfo;

// UnityEngine.SpriteRenderer
#include "UnityEngine_UnityEngine_SpriteRenderer.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.SpriteRenderer>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.SpriteRenderer>
extern MethodInfo IEnumerator_1_get_Current_m47255_MethodInfo;
static PropertyInfo IEnumerator_1_t6556____Current_PropertyInfo = 
{
	&IEnumerator_1_t6556_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m47255_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6556_PropertyInfos[] =
{
	&IEnumerator_1_t6556____Current_PropertyInfo,
	NULL
};
extern Il2CppType SpriteRenderer_t445_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m47255_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.SpriteRenderer>::get_Current()
MethodInfo IEnumerator_1_get_Current_m47255_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6556_il2cpp_TypeInfo/* declaring_type */
	, &SpriteRenderer_t445_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m47255_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6556_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m47255_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6556_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6556_0_0_0;
extern Il2CppType IEnumerator_1_t6556_1_0_0;
struct IEnumerator_1_t6556;
extern Il2CppGenericClass IEnumerator_1_t6556_GenericClass;
TypeInfo IEnumerator_1_t6556_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6556_MethodInfos/* methods */
	, IEnumerator_1_t6556_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6556_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6556_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6556_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6556_0_0_0/* byval_arg */
	, &IEnumerator_1_t6556_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6556_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_410.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4595_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_410MethodDeclarations.h"

extern TypeInfo SpriteRenderer_t445_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28214_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSpriteRenderer_t445_m37240_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.SpriteRenderer>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SpriteRenderer>(System.Int32)
#define Array_InternalArray__get_Item_TisSpriteRenderer_t445_m37240(__this, p0, method) (SpriteRenderer_t445 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4595____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4595_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4595, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4595____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4595_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4595, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4595_FieldInfos[] =
{
	&InternalEnumerator_1_t4595____array_0_FieldInfo,
	&InternalEnumerator_1_t4595____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28211_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4595____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4595_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28211_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4595____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4595_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28214_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4595_PropertyInfos[] =
{
	&InternalEnumerator_1_t4595____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4595____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4595_InternalEnumerator_1__ctor_m28210_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28210_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28210_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4595_InternalEnumerator_1__ctor_m28210_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28210_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28211_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28211_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4595_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28211_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28212_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28212_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4595_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28212_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28213_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28213_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4595_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28213_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteRenderer_t445_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28214_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.SpriteRenderer>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28214_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4595_il2cpp_TypeInfo/* declaring_type */
	, &SpriteRenderer_t445_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28214_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4595_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28210_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28211_MethodInfo,
	&InternalEnumerator_1_Dispose_m28212_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28213_MethodInfo,
	&InternalEnumerator_1_get_Current_m28214_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m28213_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28212_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4595_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28211_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28213_MethodInfo,
	&InternalEnumerator_1_Dispose_m28212_MethodInfo,
	&InternalEnumerator_1_get_Current_m28214_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4595_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6556_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4595_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6556_il2cpp_TypeInfo, 7},
};
extern TypeInfo SpriteRenderer_t445_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4595_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28214_MethodInfo/* Method Usage */,
	&SpriteRenderer_t445_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSpriteRenderer_t445_m37240_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4595_0_0_0;
extern Il2CppType InternalEnumerator_1_t4595_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4595_GenericClass;
TypeInfo InternalEnumerator_1_t4595_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4595_MethodInfos/* methods */
	, InternalEnumerator_1_t4595_PropertyInfos/* properties */
	, InternalEnumerator_1_t4595_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4595_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4595_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4595_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4595_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4595_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4595_1_0_0/* this_arg */
	, InternalEnumerator_1_t4595_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4595_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4595_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4595)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8287_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>
extern MethodInfo ICollection_1_get_Count_m47256_MethodInfo;
static PropertyInfo ICollection_1_t8287____Count_PropertyInfo = 
{
	&ICollection_1_t8287_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m47256_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m47257_MethodInfo;
static PropertyInfo ICollection_1_t8287____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8287_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m47257_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8287_PropertyInfos[] =
{
	&ICollection_1_t8287____Count_PropertyInfo,
	&ICollection_1_t8287____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m47256_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::get_Count()
MethodInfo ICollection_1_get_Count_m47256_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8287_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m47256_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m47257_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m47257_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8287_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m47257_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteRenderer_t445_0_0_0;
extern Il2CppType SpriteRenderer_t445_0_0_0;
static ParameterInfo ICollection_1_t8287_ICollection_1_Add_m47258_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t445_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m47258_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Add(T)
MethodInfo ICollection_1_Add_m47258_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8287_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8287_ICollection_1_Add_m47258_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m47258_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m47259_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Clear()
MethodInfo ICollection_1_Clear_m47259_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8287_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m47259_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteRenderer_t445_0_0_0;
static ParameterInfo ICollection_1_t8287_ICollection_1_Contains_m47260_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t445_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m47260_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Contains(T)
MethodInfo ICollection_1_Contains_m47260_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8287_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8287_ICollection_1_Contains_m47260_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m47260_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteRendererU5BU5D_t5441_0_0_0;
extern Il2CppType SpriteRendererU5BU5D_t5441_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8287_ICollection_1_CopyTo_m47261_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SpriteRendererU5BU5D_t5441_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m47261_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m47261_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8287_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8287_ICollection_1_CopyTo_m47261_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m47261_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteRenderer_t445_0_0_0;
static ParameterInfo ICollection_1_t8287_ICollection_1_Remove_m47262_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t445_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m47262_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.SpriteRenderer>::Remove(T)
MethodInfo ICollection_1_Remove_m47262_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8287_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8287_ICollection_1_Remove_m47262_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m47262_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8287_MethodInfos[] =
{
	&ICollection_1_get_Count_m47256_MethodInfo,
	&ICollection_1_get_IsReadOnly_m47257_MethodInfo,
	&ICollection_1_Add_m47258_MethodInfo,
	&ICollection_1_Clear_m47259_MethodInfo,
	&ICollection_1_Contains_m47260_MethodInfo,
	&ICollection_1_CopyTo_m47261_MethodInfo,
	&ICollection_1_Remove_m47262_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8289_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8287_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8289_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8287_0_0_0;
extern Il2CppType ICollection_1_t8287_1_0_0;
struct ICollection_1_t8287;
extern Il2CppGenericClass ICollection_1_t8287_GenericClass;
TypeInfo ICollection_1_t8287_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8287_MethodInfos/* methods */
	, ICollection_1_t8287_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8287_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8287_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8287_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8287_0_0_0/* byval_arg */
	, &ICollection_1_t8287_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8287_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SpriteRenderer>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.SpriteRenderer>
extern Il2CppType IEnumerator_1_t6556_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m47263_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.SpriteRenderer>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m47263_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8289_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6556_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m47263_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8289_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m47263_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8289_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8289_0_0_0;
extern Il2CppType IEnumerable_1_t8289_1_0_0;
struct IEnumerable_1_t8289;
extern Il2CppGenericClass IEnumerable_1_t8289_GenericClass;
TypeInfo IEnumerable_1_t8289_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8289_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8289_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8289_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8289_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8289_0_0_0/* byval_arg */
	, &IEnumerable_1_t8289_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8289_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8288_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>
extern MethodInfo IList_1_get_Item_m47264_MethodInfo;
extern MethodInfo IList_1_set_Item_m47265_MethodInfo;
static PropertyInfo IList_1_t8288____Item_PropertyInfo = 
{
	&IList_1_t8288_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m47264_MethodInfo/* get */
	, &IList_1_set_Item_m47265_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8288_PropertyInfos[] =
{
	&IList_1_t8288____Item_PropertyInfo,
	NULL
};
extern Il2CppType SpriteRenderer_t445_0_0_0;
static ParameterInfo IList_1_t8288_IList_1_IndexOf_m47266_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t445_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m47266_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::IndexOf(T)
MethodInfo IList_1_IndexOf_m47266_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8288_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8288_IList_1_IndexOf_m47266_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m47266_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SpriteRenderer_t445_0_0_0;
static ParameterInfo IList_1_t8288_IList_1_Insert_m47267_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t445_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m47267_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m47267_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8288_IList_1_Insert_m47267_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m47267_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8288_IList_1_RemoveAt_m47268_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m47268_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m47268_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8288_IList_1_RemoveAt_m47268_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m47268_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8288_IList_1_get_Item_m47264_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType SpriteRenderer_t445_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m47264_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m47264_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8288_il2cpp_TypeInfo/* declaring_type */
	, &SpriteRenderer_t445_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8288_IList_1_get_Item_m47264_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m47264_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SpriteRenderer_t445_0_0_0;
static ParameterInfo IList_1_t8288_IList_1_set_Item_m47265_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t445_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m47265_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.SpriteRenderer>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m47265_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8288_IList_1_set_Item_m47265_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m47265_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8288_MethodInfos[] =
{
	&IList_1_IndexOf_m47266_MethodInfo,
	&IList_1_Insert_m47267_MethodInfo,
	&IList_1_RemoveAt_m47268_MethodInfo,
	&IList_1_get_Item_m47264_MethodInfo,
	&IList_1_set_Item_m47265_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8288_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8287_il2cpp_TypeInfo,
	&IEnumerable_1_t8289_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8288_0_0_0;
extern Il2CppType IList_1_t8288_1_0_0;
struct IList_1_t8288;
extern Il2CppGenericClass IList_1_t8288_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8288_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8288_MethodInfos/* methods */
	, IList_1_t8288_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8288_il2cpp_TypeInfo/* element_class */
	, IList_1_t8288_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8288_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8288_0_0_0/* byval_arg */
	, &IList_1_t8288_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8288_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_131.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4596_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_131MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_133.h"
extern TypeInfo InvokableCall_1_t4597_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_133MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28217_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28219_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t4596____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t4596_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4596, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4596_FieldInfos[] =
{
	&CachedInvokableCall_1_t4596____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType SpriteRenderer_t445_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4596_CachedInvokableCall_1__ctor_m28215_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t445_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28215_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28215_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t4596_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4596_CachedInvokableCall_1__ctor_m28215_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28215_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4596_CachedInvokableCall_1_Invoke_m28216_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28216_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.SpriteRenderer>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28216_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t4596_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4596_CachedInvokableCall_1_Invoke_m28216_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28216_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4596_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28215_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28216_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28216_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28220_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4596_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28216_MethodInfo,
	&InvokableCall_1_Find_m28220_MethodInfo,
};
extern Il2CppType UnityAction_1_t4598_0_0_0;
extern TypeInfo UnityAction_1_t4598_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisSpriteRenderer_t445_m37250_MethodInfo;
extern TypeInfo SpriteRenderer_t445_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28222_MethodInfo;
extern TypeInfo SpriteRenderer_t445_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4596_RGCTXData[8] = 
{
	&UnityAction_1_t4598_0_0_0/* Type Usage */,
	&UnityAction_1_t4598_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSpriteRenderer_t445_m37250_MethodInfo/* Method Usage */,
	&SpriteRenderer_t445_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28222_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28217_MethodInfo/* Method Usage */,
	&SpriteRenderer_t445_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28219_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4596_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4596_1_0_0;
struct CachedInvokableCall_1_t4596;
extern Il2CppGenericClass CachedInvokableCall_1_t4596_GenericClass;
TypeInfo CachedInvokableCall_1_t4596_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4596_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4596_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4597_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4596_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4596_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4596_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4596_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4596_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4596_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4596_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4596)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_138.h"
extern TypeInfo UnityAction_1_t4598_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_138MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.SpriteRenderer>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.SpriteRenderer>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisSpriteRenderer_t445_m37250(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>
extern Il2CppType UnityAction_1_t4598_0_0_1;
FieldInfo InvokableCall_1_t4597____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4598_0_0_1/* type */
	, &InvokableCall_1_t4597_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4597, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4597_FieldInfos[] =
{
	&InvokableCall_1_t4597____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4597_InvokableCall_1__ctor_m28217_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28217_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28217_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t4597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4597_InvokableCall_1__ctor_m28217_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28217_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4598_0_0_0;
static ParameterInfo InvokableCall_1_t4597_InvokableCall_1__ctor_m28218_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4598_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28218_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28218_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t4597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4597_InvokableCall_1__ctor_m28218_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28218_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t4597_InvokableCall_1_Invoke_m28219_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28219_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28219_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t4597_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4597_InvokableCall_1_Invoke_m28219_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28219_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4597_InvokableCall_1_Find_m28220_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28220_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28220_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t4597_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4597_InvokableCall_1_Find_m28220_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28220_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4597_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28217_MethodInfo,
	&InvokableCall_1__ctor_m28218_MethodInfo,
	&InvokableCall_1_Invoke_m28219_MethodInfo,
	&InvokableCall_1_Find_m28220_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4597_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m28219_MethodInfo,
	&InvokableCall_1_Find_m28220_MethodInfo,
};
extern TypeInfo UnityAction_1_t4598_il2cpp_TypeInfo;
extern TypeInfo SpriteRenderer_t445_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4597_RGCTXData[5] = 
{
	&UnityAction_1_t4598_0_0_0/* Type Usage */,
	&UnityAction_1_t4598_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSpriteRenderer_t445_m37250_MethodInfo/* Method Usage */,
	&SpriteRenderer_t445_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28222_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4597_0_0_0;
extern Il2CppType InvokableCall_1_t4597_1_0_0;
struct InvokableCall_1_t4597;
extern Il2CppGenericClass InvokableCall_1_t4597_GenericClass;
TypeInfo InvokableCall_1_t4597_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4597_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4597_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4597_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4597_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4597_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4597_0_0_0/* byval_arg */
	, &InvokableCall_1_t4597_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4597_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4597_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4597)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4598_UnityAction_1__ctor_m28221_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28221_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28221_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t4598_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4598_UnityAction_1__ctor_m28221_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28221_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteRenderer_t445_0_0_0;
static ParameterInfo UnityAction_1_t4598_UnityAction_1_Invoke_m28222_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t445_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28222_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28222_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t4598_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4598_UnityAction_1_Invoke_m28222_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28222_GenericMethod/* genericMethod */

};
extern Il2CppType SpriteRenderer_t445_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4598_UnityAction_1_BeginInvoke_m28223_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SpriteRenderer_t445_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28223_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28223_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t4598_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4598_UnityAction_1_BeginInvoke_m28223_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28223_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t4598_UnityAction_1_EndInvoke_m28224_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28224_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28224_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t4598_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4598_UnityAction_1_EndInvoke_m28224_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28224_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4598_MethodInfos[] =
{
	&UnityAction_1__ctor_m28221_MethodInfo,
	&UnityAction_1_Invoke_m28222_MethodInfo,
	&UnityAction_1_BeginInvoke_m28223_MethodInfo,
	&UnityAction_1_EndInvoke_m28224_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28223_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28224_MethodInfo;
static MethodInfo* UnityAction_1_t4598_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m28222_MethodInfo,
	&UnityAction_1_BeginInvoke_m28223_MethodInfo,
	&UnityAction_1_EndInvoke_m28224_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4598_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4598_1_0_0;
struct UnityAction_1_t4598;
extern Il2CppGenericClass UnityAction_1_t4598_GenericClass;
TypeInfo UnityAction_1_t4598_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4598_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4598_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4598_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4598_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4598_0_0_0/* byval_arg */
	, &UnityAction_1_t4598_1_0_0/* this_arg */
	, UnityAction_1_t4598_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4598_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4598)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_132.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4599_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_132MethodDeclarations.h"

// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_134.h"
extern TypeInfo Behaviour_t515_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4600_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_134MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28227_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28229_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t4599____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t4599_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4599, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4599_FieldInfos[] =
{
	&CachedInvokableCall_1_t4599____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType Behaviour_t515_0_0_0;
extern Il2CppType Behaviour_t515_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4599_CachedInvokableCall_1__ctor_m28225_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Behaviour_t515_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28225_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28225_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t4599_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4599_CachedInvokableCall_1__ctor_m28225_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28225_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4599_CachedInvokableCall_1_Invoke_m28226_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28226_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Behaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28226_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t4599_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4599_CachedInvokableCall_1_Invoke_m28226_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28226_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4599_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28225_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28226_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28226_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28230_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4599_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28226_MethodInfo,
	&InvokableCall_1_Find_m28230_MethodInfo,
};
extern Il2CppType UnityAction_1_t4601_0_0_0;
extern TypeInfo UnityAction_1_t4601_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisBehaviour_t515_m37251_MethodInfo;
extern TypeInfo Behaviour_t515_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28232_MethodInfo;
extern TypeInfo Behaviour_t515_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4599_RGCTXData[8] = 
{
	&UnityAction_1_t4601_0_0_0/* Type Usage */,
	&UnityAction_1_t4601_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBehaviour_t515_m37251_MethodInfo/* Method Usage */,
	&Behaviour_t515_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28232_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28227_MethodInfo/* Method Usage */,
	&Behaviour_t515_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28229_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4599_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4599_1_0_0;
struct CachedInvokableCall_1_t4599;
extern Il2CppGenericClass CachedInvokableCall_1_t4599_GenericClass;
TypeInfo CachedInvokableCall_1_t4599_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4599_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4599_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4600_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4599_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4599_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4599_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4599_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4599_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4599_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4599_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4599)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_139.h"
extern TypeInfo UnityAction_1_t4601_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_139MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Behaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Behaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisBehaviour_t515_m37251(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>
extern Il2CppType UnityAction_1_t4601_0_0_1;
FieldInfo InvokableCall_1_t4600____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4601_0_0_1/* type */
	, &InvokableCall_1_t4600_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4600, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4600_FieldInfos[] =
{
	&InvokableCall_1_t4600____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4600_InvokableCall_1__ctor_m28227_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28227_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28227_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t4600_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4600_InvokableCall_1__ctor_m28227_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28227_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4601_0_0_0;
static ParameterInfo InvokableCall_1_t4600_InvokableCall_1__ctor_m28228_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4601_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28228_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28228_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t4600_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4600_InvokableCall_1__ctor_m28228_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28228_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t4600_InvokableCall_1_Invoke_m28229_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28229_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28229_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t4600_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4600_InvokableCall_1_Invoke_m28229_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28229_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4600_InvokableCall_1_Find_m28230_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28230_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Behaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28230_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t4600_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4600_InvokableCall_1_Find_m28230_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28230_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4600_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28227_MethodInfo,
	&InvokableCall_1__ctor_m28228_MethodInfo,
	&InvokableCall_1_Invoke_m28229_MethodInfo,
	&InvokableCall_1_Find_m28230_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4600_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m28229_MethodInfo,
	&InvokableCall_1_Find_m28230_MethodInfo,
};
extern TypeInfo UnityAction_1_t4601_il2cpp_TypeInfo;
extern TypeInfo Behaviour_t515_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4600_RGCTXData[5] = 
{
	&UnityAction_1_t4601_0_0_0/* Type Usage */,
	&UnityAction_1_t4601_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBehaviour_t515_m37251_MethodInfo/* Method Usage */,
	&Behaviour_t515_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28232_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4600_0_0_0;
extern Il2CppType InvokableCall_1_t4600_1_0_0;
struct InvokableCall_1_t4600;
extern Il2CppGenericClass InvokableCall_1_t4600_GenericClass;
TypeInfo InvokableCall_1_t4600_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4600_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4600_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4600_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4600_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4600_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4600_0_0_0/* byval_arg */
	, &InvokableCall_1_t4600_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4600_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4600_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4600)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4601_UnityAction_1__ctor_m28231_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28231_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28231_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t4601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4601_UnityAction_1__ctor_m28231_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28231_GenericMethod/* genericMethod */

};
extern Il2CppType Behaviour_t515_0_0_0;
static ParameterInfo UnityAction_1_t4601_UnityAction_1_Invoke_m28232_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Behaviour_t515_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28232_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28232_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t4601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4601_UnityAction_1_Invoke_m28232_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28232_GenericMethod/* genericMethod */

};
extern Il2CppType Behaviour_t515_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4601_UnityAction_1_BeginInvoke_m28233_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Behaviour_t515_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28233_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28233_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t4601_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4601_UnityAction_1_BeginInvoke_m28233_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28233_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t4601_UnityAction_1_EndInvoke_m28234_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28234_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Behaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28234_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t4601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4601_UnityAction_1_EndInvoke_m28234_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28234_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4601_MethodInfos[] =
{
	&UnityAction_1__ctor_m28231_MethodInfo,
	&UnityAction_1_Invoke_m28232_MethodInfo,
	&UnityAction_1_BeginInvoke_m28233_MethodInfo,
	&UnityAction_1_EndInvoke_m28234_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28233_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28234_MethodInfo;
static MethodInfo* UnityAction_1_t4601_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m28232_MethodInfo,
	&UnityAction_1_BeginInvoke_m28233_MethodInfo,
	&UnityAction_1_EndInvoke_m28234_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4601_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4601_1_0_0;
struct UnityAction_1_t4601;
extern Il2CppGenericClass UnityAction_1_t4601_GenericClass;
TypeInfo UnityAction_1_t4601_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4601_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4601_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4601_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4601_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4601_0_0_0/* byval_arg */
	, &UnityAction_1_t4601_1_0_0/* this_arg */
	, UnityAction_1_t4601_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4601_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4601)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_133.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4602_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_133MethodDeclarations.h"

// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_135.h"
extern TypeInfo Camera_t3_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4603_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_135MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28237_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28239_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t4602____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t4602_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4602, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4602_FieldInfos[] =
{
	&CachedInvokableCall_1_t4602____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType Camera_t3_0_0_0;
extern Il2CppType Camera_t3_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4602_CachedInvokableCall_1__ctor_m28235_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Camera_t3_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28235_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28235_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t4602_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4602_CachedInvokableCall_1__ctor_m28235_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28235_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4602_CachedInvokableCall_1_Invoke_m28236_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28236_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Camera>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28236_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t4602_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4602_CachedInvokableCall_1_Invoke_m28236_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28236_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4602_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28235_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28236_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28236_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28240_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4602_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28236_MethodInfo,
	&InvokableCall_1_Find_m28240_MethodInfo,
};
extern Il2CppType UnityAction_1_t4604_0_0_0;
extern TypeInfo UnityAction_1_t4604_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisCamera_t3_m37252_MethodInfo;
extern TypeInfo Camera_t3_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28242_MethodInfo;
extern TypeInfo Camera_t3_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4602_RGCTXData[8] = 
{
	&UnityAction_1_t4604_0_0_0/* Type Usage */,
	&UnityAction_1_t4604_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCamera_t3_m37252_MethodInfo/* Method Usage */,
	&Camera_t3_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28242_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28237_MethodInfo/* Method Usage */,
	&Camera_t3_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28239_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4602_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4602_1_0_0;
struct CachedInvokableCall_1_t4602;
extern Il2CppGenericClass CachedInvokableCall_1_t4602_GenericClass;
TypeInfo CachedInvokableCall_1_t4602_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4602_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4602_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4603_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4602_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4602_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4602_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4602_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4602_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4602_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4602_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4602)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_140.h"
extern TypeInfo UnityAction_1_t4604_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_140MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Camera>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Camera>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisCamera_t3_m37252(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>
extern Il2CppType UnityAction_1_t4604_0_0_1;
FieldInfo InvokableCall_1_t4603____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4604_0_0_1/* type */
	, &InvokableCall_1_t4603_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4603, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4603_FieldInfos[] =
{
	&InvokableCall_1_t4603____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4603_InvokableCall_1__ctor_m28237_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28237_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28237_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t4603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4603_InvokableCall_1__ctor_m28237_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28237_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4604_0_0_0;
static ParameterInfo InvokableCall_1_t4603_InvokableCall_1__ctor_m28238_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4604_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28238_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28238_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t4603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4603_InvokableCall_1__ctor_m28238_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28238_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t4603_InvokableCall_1_Invoke_m28239_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28239_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28239_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t4603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4603_InvokableCall_1_Invoke_m28239_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28239_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4603_InvokableCall_1_Find_m28240_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28240_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Camera>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28240_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t4603_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4603_InvokableCall_1_Find_m28240_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28240_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4603_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28237_MethodInfo,
	&InvokableCall_1__ctor_m28238_MethodInfo,
	&InvokableCall_1_Invoke_m28239_MethodInfo,
	&InvokableCall_1_Find_m28240_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4603_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m28239_MethodInfo,
	&InvokableCall_1_Find_m28240_MethodInfo,
};
extern TypeInfo UnityAction_1_t4604_il2cpp_TypeInfo;
extern TypeInfo Camera_t3_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4603_RGCTXData[5] = 
{
	&UnityAction_1_t4604_0_0_0/* Type Usage */,
	&UnityAction_1_t4604_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCamera_t3_m37252_MethodInfo/* Method Usage */,
	&Camera_t3_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28242_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4603_0_0_0;
extern Il2CppType InvokableCall_1_t4603_1_0_0;
struct InvokableCall_1_t4603;
extern Il2CppGenericClass InvokableCall_1_t4603_GenericClass;
TypeInfo InvokableCall_1_t4603_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4603_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4603_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4603_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4603_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4603_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4603_0_0_0/* byval_arg */
	, &InvokableCall_1_t4603_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4603_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4603_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4603)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Camera>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4604_UnityAction_1__ctor_m28241_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28241_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28241_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t4604_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4604_UnityAction_1__ctor_m28241_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28241_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_t3_0_0_0;
static ParameterInfo UnityAction_1_t4604_UnityAction_1_Invoke_m28242_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Camera_t3_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28242_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28242_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t4604_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4604_UnityAction_1_Invoke_m28242_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28242_GenericMethod/* genericMethod */

};
extern Il2CppType Camera_t3_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4604_UnityAction_1_BeginInvoke_m28243_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Camera_t3_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28243_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28243_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t4604_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4604_UnityAction_1_BeginInvoke_m28243_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28243_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t4604_UnityAction_1_EndInvoke_m28244_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28244_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Camera>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28244_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t4604_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4604_UnityAction_1_EndInvoke_m28244_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28244_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4604_MethodInfos[] =
{
	&UnityAction_1__ctor_m28241_MethodInfo,
	&UnityAction_1_Invoke_m28242_MethodInfo,
	&UnityAction_1_BeginInvoke_m28243_MethodInfo,
	&UnityAction_1_EndInvoke_m28244_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28243_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28244_MethodInfo;
static MethodInfo* UnityAction_1_t4604_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m28242_MethodInfo,
	&UnityAction_1_BeginInvoke_m28243_MethodInfo,
	&UnityAction_1_EndInvoke_m28244_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4604_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4604_1_0_0;
struct UnityAction_1_t4604;
extern Il2CppGenericClass UnityAction_1_t4604_GenericClass;
TypeInfo UnityAction_1_t4604_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4604_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4604_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4604_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4604_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4604_0_0_0/* byval_arg */
	, &UnityAction_1_t4604_1_0_0/* this_arg */
	, UnityAction_1_t4604_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4604_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4604)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6558_il2cpp_TypeInfo;

// UnityEngine.Display
#include "UnityEngine_UnityEngine_Display.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Display>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Display>
extern MethodInfo IEnumerator_1_get_Current_m47269_MethodInfo;
static PropertyInfo IEnumerator_1_t6558____Current_PropertyInfo = 
{
	&IEnumerator_1_t6558_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m47269_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6558_PropertyInfos[] =
{
	&IEnumerator_1_t6558____Current_PropertyInfo,
	NULL
};
extern Il2CppType Display_t995_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m47269_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Display>::get_Current()
MethodInfo IEnumerator_1_get_Current_m47269_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6558_il2cpp_TypeInfo/* declaring_type */
	, &Display_t995_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m47269_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6558_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m47269_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6558_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6558_0_0_0;
extern Il2CppType IEnumerator_1_t6558_1_0_0;
struct IEnumerator_1_t6558;
extern Il2CppGenericClass IEnumerator_1_t6558_GenericClass;
TypeInfo IEnumerator_1_t6558_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6558_MethodInfos/* methods */
	, IEnumerator_1_t6558_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6558_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6558_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6558_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6558_0_0_0/* byval_arg */
	, &IEnumerator_1_t6558_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6558_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Display>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_411.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4605_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Display>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_411MethodDeclarations.h"

extern TypeInfo Display_t995_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28249_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDisplay_t995_m37254_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Display>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Display>(System.Int32)
#define Array_InternalArray__get_Item_TisDisplay_t995_m37254(__this, p0, method) (Display_t995 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Display>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Display>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Display>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Display>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Display>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Display>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4605____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4605_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4605, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4605____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4605_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4605, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4605_FieldInfos[] =
{
	&InternalEnumerator_1_t4605____array_0_FieldInfo,
	&InternalEnumerator_1_t4605____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28246_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4605____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4605_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28246_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4605____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4605_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28249_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4605_PropertyInfos[] =
{
	&InternalEnumerator_1_t4605____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4605____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4605_InternalEnumerator_1__ctor_m28245_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28245_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Display>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28245_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4605_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4605_InternalEnumerator_1__ctor_m28245_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28245_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28246_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Display>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28246_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4605_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28246_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28247_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Display>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28247_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4605_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28247_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28248_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Display>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28248_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4605_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28248_GenericMethod/* genericMethod */

};
extern Il2CppType Display_t995_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28249_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Display>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28249_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4605_il2cpp_TypeInfo/* declaring_type */
	, &Display_t995_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28249_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4605_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28245_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28246_MethodInfo,
	&InternalEnumerator_1_Dispose_m28247_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28248_MethodInfo,
	&InternalEnumerator_1_get_Current_m28249_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m28248_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28247_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4605_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28246_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28248_MethodInfo,
	&InternalEnumerator_1_Dispose_m28247_MethodInfo,
	&InternalEnumerator_1_get_Current_m28249_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4605_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6558_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4605_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6558_il2cpp_TypeInfo, 7},
};
extern TypeInfo Display_t995_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4605_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28249_MethodInfo/* Method Usage */,
	&Display_t995_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDisplay_t995_m37254_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4605_0_0_0;
extern Il2CppType InternalEnumerator_1_t4605_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4605_GenericClass;
TypeInfo InternalEnumerator_1_t4605_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4605_MethodInfos/* methods */
	, InternalEnumerator_1_t4605_PropertyInfos/* properties */
	, InternalEnumerator_1_t4605_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4605_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4605_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4605_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4605_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4605_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4605_1_0_0/* this_arg */
	, InternalEnumerator_1_t4605_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4605_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4605_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4605)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8290_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Display>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Display>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Display>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Display>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Display>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Display>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Display>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Display>
extern MethodInfo ICollection_1_get_Count_m47270_MethodInfo;
static PropertyInfo ICollection_1_t8290____Count_PropertyInfo = 
{
	&ICollection_1_t8290_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m47270_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m47271_MethodInfo;
static PropertyInfo ICollection_1_t8290____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8290_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m47271_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8290_PropertyInfos[] =
{
	&ICollection_1_t8290____Count_PropertyInfo,
	&ICollection_1_t8290____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m47270_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Display>::get_Count()
MethodInfo ICollection_1_get_Count_m47270_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8290_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m47270_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m47271_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Display>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m47271_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8290_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m47271_GenericMethod/* genericMethod */

};
extern Il2CppType Display_t995_0_0_0;
extern Il2CppType Display_t995_0_0_0;
static ParameterInfo ICollection_1_t8290_ICollection_1_Add_m47272_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Display_t995_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m47272_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Display>::Add(T)
MethodInfo ICollection_1_Add_m47272_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8290_ICollection_1_Add_m47272_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m47272_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m47273_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Display>::Clear()
MethodInfo ICollection_1_Clear_m47273_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m47273_GenericMethod/* genericMethod */

};
extern Il2CppType Display_t995_0_0_0;
static ParameterInfo ICollection_1_t8290_ICollection_1_Contains_m47274_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Display_t995_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m47274_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Display>::Contains(T)
MethodInfo ICollection_1_Contains_m47274_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8290_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8290_ICollection_1_Contains_m47274_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m47274_GenericMethod/* genericMethod */

};
extern Il2CppType DisplayU5BU5D_t994_0_0_0;
extern Il2CppType DisplayU5BU5D_t994_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8290_ICollection_1_CopyTo_m47275_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DisplayU5BU5D_t994_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m47275_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Display>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m47275_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8290_ICollection_1_CopyTo_m47275_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m47275_GenericMethod/* genericMethod */

};
extern Il2CppType Display_t995_0_0_0;
static ParameterInfo ICollection_1_t8290_ICollection_1_Remove_m47276_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Display_t995_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m47276_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Display>::Remove(T)
MethodInfo ICollection_1_Remove_m47276_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8290_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8290_ICollection_1_Remove_m47276_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m47276_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8290_MethodInfos[] =
{
	&ICollection_1_get_Count_m47270_MethodInfo,
	&ICollection_1_get_IsReadOnly_m47271_MethodInfo,
	&ICollection_1_Add_m47272_MethodInfo,
	&ICollection_1_Clear_m47273_MethodInfo,
	&ICollection_1_Contains_m47274_MethodInfo,
	&ICollection_1_CopyTo_m47275_MethodInfo,
	&ICollection_1_Remove_m47276_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8292_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8290_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8292_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8290_0_0_0;
extern Il2CppType ICollection_1_t8290_1_0_0;
struct ICollection_1_t8290;
extern Il2CppGenericClass ICollection_1_t8290_GenericClass;
TypeInfo ICollection_1_t8290_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8290_MethodInfos/* methods */
	, ICollection_1_t8290_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8290_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8290_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8290_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8290_0_0_0/* byval_arg */
	, &ICollection_1_t8290_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8290_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Display>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Display>
extern Il2CppType IEnumerator_1_t6558_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m47277_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Display>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m47277_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8292_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6558_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m47277_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8292_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m47277_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8292_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8292_0_0_0;
extern Il2CppType IEnumerable_1_t8292_1_0_0;
struct IEnumerable_1_t8292;
extern Il2CppGenericClass IEnumerable_1_t8292_GenericClass;
TypeInfo IEnumerable_1_t8292_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8292_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8292_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8292_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8292_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8292_0_0_0/* byval_arg */
	, &IEnumerable_1_t8292_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8292_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8291_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Display>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Display>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Display>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Display>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Display>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Display>
extern MethodInfo IList_1_get_Item_m47278_MethodInfo;
extern MethodInfo IList_1_set_Item_m47279_MethodInfo;
static PropertyInfo IList_1_t8291____Item_PropertyInfo = 
{
	&IList_1_t8291_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m47278_MethodInfo/* get */
	, &IList_1_set_Item_m47279_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8291_PropertyInfos[] =
{
	&IList_1_t8291____Item_PropertyInfo,
	NULL
};
extern Il2CppType Display_t995_0_0_0;
static ParameterInfo IList_1_t8291_IList_1_IndexOf_m47280_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Display_t995_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m47280_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Display>::IndexOf(T)
MethodInfo IList_1_IndexOf_m47280_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8291_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8291_IList_1_IndexOf_m47280_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m47280_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Display_t995_0_0_0;
static ParameterInfo IList_1_t8291_IList_1_Insert_m47281_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Display_t995_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m47281_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Display>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m47281_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8291_IList_1_Insert_m47281_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m47281_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8291_IList_1_RemoveAt_m47282_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m47282_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Display>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m47282_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8291_IList_1_RemoveAt_m47282_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m47282_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8291_IList_1_get_Item_m47278_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Display_t995_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m47278_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Display>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m47278_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8291_il2cpp_TypeInfo/* declaring_type */
	, &Display_t995_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8291_IList_1_get_Item_m47278_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m47278_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Display_t995_0_0_0;
static ParameterInfo IList_1_t8291_IList_1_set_Item_m47279_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Display_t995_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m47279_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Display>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m47279_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8291_IList_1_set_Item_m47279_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m47279_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8291_MethodInfos[] =
{
	&IList_1_IndexOf_m47280_MethodInfo,
	&IList_1_Insert_m47281_MethodInfo,
	&IList_1_RemoveAt_m47282_MethodInfo,
	&IList_1_get_Item_m47278_MethodInfo,
	&IList_1_set_Item_m47279_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8291_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8290_il2cpp_TypeInfo,
	&IEnumerable_1_t8292_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8291_0_0_0;
extern Il2CppType IList_1_t8291_1_0_0;
struct IList_1_t8291;
extern Il2CppGenericClass IList_1_t8291_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8291_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8291_MethodInfos/* methods */
	, IList_1_t8291_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8291_il2cpp_TypeInfo/* element_class */
	, IList_1_t8291_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8291_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8291_0_0_0/* byval_arg */
	, &IList_1_t8291_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8291_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6560_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IntPtr>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IntPtr>
extern MethodInfo IEnumerator_1_get_Current_m47283_MethodInfo;
static PropertyInfo IEnumerator_1_t6560____Current_PropertyInfo = 
{
	&IEnumerator_1_t6560_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m47283_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6560_PropertyInfos[] =
{
	&IEnumerator_1_t6560____Current_PropertyInfo,
	NULL
};
extern Il2CppType IntPtr_t121_0_0_0;
extern void* RuntimeInvoker_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m47283_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IntPtr>::get_Current()
MethodInfo IEnumerator_1_get_Current_m47283_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6560_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t121_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t121/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m47283_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6560_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m47283_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6560_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6560_0_0_0;
extern Il2CppType IEnumerator_1_t6560_1_0_0;
struct IEnumerator_1_t6560;
extern Il2CppGenericClass IEnumerator_1_t6560_GenericClass;
TypeInfo IEnumerator_1_t6560_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6560_MethodInfos/* methods */
	, IEnumerator_1_t6560_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6560_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6560_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6560_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6560_0_0_0/* byval_arg */
	, &IEnumerator_1_t6560_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6560_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IntPtr>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_412.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4606_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IntPtr>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_412MethodDeclarations.h"

extern TypeInfo IntPtr_t121_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28254_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIntPtr_t121_m37265_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
 IntPtr_t121 Array_InternalArray__get_Item_TisIntPtr_t121_m37265 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m28250_MethodInfo;
 void InternalEnumerator_1__ctor_m28250 (InternalEnumerator_1_t4606 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28251_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28251 (InternalEnumerator_1_t4606 * __this, MethodInfo* method){
	{
		IntPtr_t121 L_0 = InternalEnumerator_1_get_Current_m28254(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m28254_MethodInfo);
		IntPtr_t121 L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&IntPtr_t121_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m28252_MethodInfo;
 void InternalEnumerator_1_Dispose_m28252 (InternalEnumerator_1_t4606 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m28253_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m28253 (InternalEnumerator_1_t4606 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
 IntPtr_t121 InternalEnumerator_1_get_Current_m28254 (InternalEnumerator_1_t4606 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		IntPtr_t121 L_8 = Array_InternalArray__get_Item_TisIntPtr_t121_m37265(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisIntPtr_t121_m37265_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IntPtr>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4606____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4606_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4606, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4606____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4606_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4606, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4606_FieldInfos[] =
{
	&InternalEnumerator_1_t4606____array_0_FieldInfo,
	&InternalEnumerator_1_t4606____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4606____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4606_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28251_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4606____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4606_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28254_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4606_PropertyInfos[] =
{
	&InternalEnumerator_1_t4606____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4606____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4606_InternalEnumerator_1__ctor_m28250_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28250_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28250_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m28250/* method */
	, &InternalEnumerator_1_t4606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4606_InternalEnumerator_1__ctor_m28250_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28250_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28251_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28251_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28251/* method */
	, &InternalEnumerator_1_t4606_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28251_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28252_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28252_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m28252/* method */
	, &InternalEnumerator_1_t4606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28252_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28253_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28253_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m28253/* method */
	, &InternalEnumerator_1_t4606_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28253_GenericMethod/* genericMethod */

};
extern Il2CppType IntPtr_t121_0_0_0;
extern void* RuntimeInvoker_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28254_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28254_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m28254/* method */
	, &InternalEnumerator_1_t4606_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t121_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t121/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28254_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4606_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28250_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28251_MethodInfo,
	&InternalEnumerator_1_Dispose_m28252_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28253_MethodInfo,
	&InternalEnumerator_1_get_Current_m28254_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4606_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28251_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28253_MethodInfo,
	&InternalEnumerator_1_Dispose_m28252_MethodInfo,
	&InternalEnumerator_1_get_Current_m28254_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4606_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6560_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4606_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6560_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4606_0_0_0;
extern Il2CppType InternalEnumerator_1_t4606_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4606_GenericClass;
TypeInfo InternalEnumerator_1_t4606_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4606_MethodInfos/* methods */
	, InternalEnumerator_1_t4606_PropertyInfos/* properties */
	, InternalEnumerator_1_t4606_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4606_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4606_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4606_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4606_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4606_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4606_1_0_0/* this_arg */
	, InternalEnumerator_1_t4606_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4606_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4606)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8293_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IntPtr>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IntPtr>
extern MethodInfo ICollection_1_get_Count_m47284_MethodInfo;
static PropertyInfo ICollection_1_t8293____Count_PropertyInfo = 
{
	&ICollection_1_t8293_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m47284_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m47285_MethodInfo;
static PropertyInfo ICollection_1_t8293____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8293_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m47285_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8293_PropertyInfos[] =
{
	&ICollection_1_t8293____Count_PropertyInfo,
	&ICollection_1_t8293____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m47284_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IntPtr>::get_Count()
MethodInfo ICollection_1_get_Count_m47284_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8293_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m47284_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m47285_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m47285_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8293_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m47285_GenericMethod/* genericMethod */

};
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo ICollection_1_t8293_ICollection_1_Add_m47286_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m47286_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::Add(T)
MethodInfo ICollection_1_Add_m47286_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_IntPtr_t121/* invoker_method */
	, ICollection_1_t8293_ICollection_1_Add_m47286_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m47286_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m47287_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::Clear()
MethodInfo ICollection_1_Clear_m47287_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m47287_GenericMethod/* genericMethod */

};
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo ICollection_1_t8293_ICollection_1_Contains_m47288_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m47288_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::Contains(T)
MethodInfo ICollection_1_Contains_m47288_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8293_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_IntPtr_t121/* invoker_method */
	, ICollection_1_t8293_ICollection_1_Contains_m47288_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m47288_GenericMethod/* genericMethod */

};
extern Il2CppType IntPtrU5BU5D_t996_0_0_0;
extern Il2CppType IntPtrU5BU5D_t996_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8293_ICollection_1_CopyTo_m47289_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IntPtrU5BU5D_t996_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m47289_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IntPtr>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m47289_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8293_ICollection_1_CopyTo_m47289_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m47289_GenericMethod/* genericMethod */

};
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo ICollection_1_t8293_ICollection_1_Remove_m47290_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m47290_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IntPtr>::Remove(T)
MethodInfo ICollection_1_Remove_m47290_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8293_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_IntPtr_t121/* invoker_method */
	, ICollection_1_t8293_ICollection_1_Remove_m47290_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m47290_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8293_MethodInfos[] =
{
	&ICollection_1_get_Count_m47284_MethodInfo,
	&ICollection_1_get_IsReadOnly_m47285_MethodInfo,
	&ICollection_1_Add_m47286_MethodInfo,
	&ICollection_1_Clear_m47287_MethodInfo,
	&ICollection_1_Contains_m47288_MethodInfo,
	&ICollection_1_CopyTo_m47289_MethodInfo,
	&ICollection_1_Remove_m47290_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8295_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8293_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8295_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8293_0_0_0;
extern Il2CppType ICollection_1_t8293_1_0_0;
struct ICollection_1_t8293;
extern Il2CppGenericClass ICollection_1_t8293_GenericClass;
TypeInfo ICollection_1_t8293_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8293_MethodInfos/* methods */
	, ICollection_1_t8293_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8293_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8293_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8293_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8293_0_0_0/* byval_arg */
	, &ICollection_1_t8293_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8293_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IntPtr>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IntPtr>
extern Il2CppType IEnumerator_1_t6560_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m47291_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IntPtr>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m47291_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8295_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6560_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m47291_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8295_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m47291_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8295_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8295_0_0_0;
extern Il2CppType IEnumerable_1_t8295_1_0_0;
struct IEnumerable_1_t8295;
extern Il2CppGenericClass IEnumerable_1_t8295_GenericClass;
TypeInfo IEnumerable_1_t8295_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8295_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8295_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8295_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8295_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8295_0_0_0/* byval_arg */
	, &IEnumerable_1_t8295_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8295_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8294_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IntPtr>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IntPtr>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IntPtr>
extern MethodInfo IList_1_get_Item_m47292_MethodInfo;
extern MethodInfo IList_1_set_Item_m47293_MethodInfo;
static PropertyInfo IList_1_t8294____Item_PropertyInfo = 
{
	&IList_1_t8294_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m47292_MethodInfo/* get */
	, &IList_1_set_Item_m47293_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8294_PropertyInfos[] =
{
	&IList_1_t8294____Item_PropertyInfo,
	NULL
};
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo IList_1_t8294_IList_1_IndexOf_m47294_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m47294_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IntPtr>::IndexOf(T)
MethodInfo IList_1_IndexOf_m47294_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8294_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_IntPtr_t121/* invoker_method */
	, IList_1_t8294_IList_1_IndexOf_m47294_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m47294_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo IList_1_t8294_IList_1_Insert_m47295_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m47295_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m47295_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_IntPtr_t121/* invoker_method */
	, IList_1_t8294_IList_1_Insert_m47295_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m47295_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8294_IList_1_RemoveAt_m47296_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m47296_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m47296_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8294_IList_1_RemoveAt_m47296_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m47296_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8294_IList_1_get_Item_m47292_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IntPtr_t121_0_0_0;
extern void* RuntimeInvoker_IntPtr_t121_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m47292_GenericMethod;
// T System.Collections.Generic.IList`1<System.IntPtr>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m47292_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8294_il2cpp_TypeInfo/* declaring_type */
	, &IntPtr_t121_0_0_0/* return_type */
	, RuntimeInvoker_IntPtr_t121_Int32_t93/* invoker_method */
	, IList_1_t8294_IList_1_get_Item_m47292_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m47292_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo IList_1_t8294_IList_1_set_Item_m47293_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m47293_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IntPtr>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m47293_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_IntPtr_t121/* invoker_method */
	, IList_1_t8294_IList_1_set_Item_m47293_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m47293_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8294_MethodInfos[] =
{
	&IList_1_IndexOf_m47294_MethodInfo,
	&IList_1_Insert_m47295_MethodInfo,
	&IList_1_RemoveAt_m47296_MethodInfo,
	&IList_1_get_Item_m47292_MethodInfo,
	&IList_1_set_Item_m47293_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8294_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8293_il2cpp_TypeInfo,
	&IEnumerable_1_t8295_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8294_0_0_0;
extern Il2CppType IList_1_t8294_1_0_0;
struct IList_1_t8294;
extern Il2CppGenericClass IList_1_t8294_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8294_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8294_MethodInfos/* methods */
	, IList_1_t8294_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8294_il2cpp_TypeInfo/* element_class */
	, IList_1_t8294_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8294_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8294_0_0_0/* byval_arg */
	, &IList_1_t8294_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8294_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8296_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>
extern MethodInfo ICollection_1_get_Count_m47297_MethodInfo;
static PropertyInfo ICollection_1_t8296____Count_PropertyInfo = 
{
	&ICollection_1_t8296_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m47297_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m47298_MethodInfo;
static PropertyInfo ICollection_1_t8296____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8296_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m47298_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8296_PropertyInfos[] =
{
	&ICollection_1_t8296____Count_PropertyInfo,
	&ICollection_1_t8296____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m47297_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::get_Count()
MethodInfo ICollection_1_get_Count_m47297_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8296_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m47297_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m47298_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m47298_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8296_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m47298_GenericMethod/* genericMethod */

};
extern Il2CppType ISerializable_t482_0_0_0;
extern Il2CppType ISerializable_t482_0_0_0;
static ParameterInfo ICollection_1_t8296_ICollection_1_Add_m47299_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISerializable_t482_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m47299_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Add(T)
MethodInfo ICollection_1_Add_m47299_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8296_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8296_ICollection_1_Add_m47299_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m47299_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m47300_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Clear()
MethodInfo ICollection_1_Clear_m47300_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8296_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m47300_GenericMethod/* genericMethod */

};
extern Il2CppType ISerializable_t482_0_0_0;
static ParameterInfo ICollection_1_t8296_ICollection_1_Contains_m47301_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISerializable_t482_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m47301_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Contains(T)
MethodInfo ICollection_1_Contains_m47301_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8296_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8296_ICollection_1_Contains_m47301_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m47301_GenericMethod/* genericMethod */

};
extern Il2CppType ISerializableU5BU5D_t5233_0_0_0;
extern Il2CppType ISerializableU5BU5D_t5233_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8296_ICollection_1_CopyTo_m47302_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ISerializableU5BU5D_t5233_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m47302_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m47302_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8296_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8296_ICollection_1_CopyTo_m47302_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m47302_GenericMethod/* genericMethod */

};
extern Il2CppType ISerializable_t482_0_0_0;
static ParameterInfo ICollection_1_t8296_ICollection_1_Remove_m47303_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISerializable_t482_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m47303_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.Serialization.ISerializable>::Remove(T)
MethodInfo ICollection_1_Remove_m47303_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8296_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8296_ICollection_1_Remove_m47303_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m47303_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8296_MethodInfos[] =
{
	&ICollection_1_get_Count_m47297_MethodInfo,
	&ICollection_1_get_IsReadOnly_m47298_MethodInfo,
	&ICollection_1_Add_m47299_MethodInfo,
	&ICollection_1_Clear_m47300_MethodInfo,
	&ICollection_1_Contains_m47301_MethodInfo,
	&ICollection_1_CopyTo_m47302_MethodInfo,
	&ICollection_1_Remove_m47303_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8298_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8296_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8298_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8296_0_0_0;
extern Il2CppType ICollection_1_t8296_1_0_0;
struct ICollection_1_t8296;
extern Il2CppGenericClass ICollection_1_t8296_GenericClass;
TypeInfo ICollection_1_t8296_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8296_MethodInfos/* methods */
	, ICollection_1_t8296_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8296_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8296_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8296_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8296_0_0_0/* byval_arg */
	, &ICollection_1_t8296_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8296_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.ISerializable>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.ISerializable>
extern Il2CppType IEnumerator_1_t6562_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m47304_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.Serialization.ISerializable>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m47304_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8298_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6562_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m47304_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8298_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m47304_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8298_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8298_0_0_0;
extern Il2CppType IEnumerable_1_t8298_1_0_0;
struct IEnumerable_1_t8298;
extern Il2CppGenericClass IEnumerable_1_t8298_GenericClass;
TypeInfo IEnumerable_1_t8298_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8298_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8298_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8298_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8298_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8298_0_0_0/* byval_arg */
	, &IEnumerable_1_t8298_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8298_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6562_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.ISerializable>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.ISerializable>
extern MethodInfo IEnumerator_1_get_Current_m47305_MethodInfo;
static PropertyInfo IEnumerator_1_t6562____Current_PropertyInfo = 
{
	&IEnumerator_1_t6562_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m47305_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6562_PropertyInfos[] =
{
	&IEnumerator_1_t6562____Current_PropertyInfo,
	NULL
};
extern Il2CppType ISerializable_t482_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m47305_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.Serialization.ISerializable>::get_Current()
MethodInfo IEnumerator_1_get_Current_m47305_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6562_il2cpp_TypeInfo/* declaring_type */
	, &ISerializable_t482_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m47305_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6562_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m47305_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6562_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6562_0_0_0;
extern Il2CppType IEnumerator_1_t6562_1_0_0;
struct IEnumerator_1_t6562;
extern Il2CppGenericClass IEnumerator_1_t6562_GenericClass;
TypeInfo IEnumerator_1_t6562_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6562_MethodInfos/* methods */
	, IEnumerator_1_t6562_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6562_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6562_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6562_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6562_0_0_0/* byval_arg */
	, &IEnumerator_1_t6562_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6562_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_413.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4607_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_413MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m28259_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisISerializable_t482_m37276_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.ISerializable>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.ISerializable>(System.Int32)
#define Array_InternalArray__get_Item_TisISerializable_t482_m37276(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4607____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4607_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4607, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4607____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4607_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4607, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4607_FieldInfos[] =
{
	&InternalEnumerator_1_t4607____array_0_FieldInfo,
	&InternalEnumerator_1_t4607____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28256_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4607____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4607_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28256_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4607____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4607_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28259_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4607_PropertyInfos[] =
{
	&InternalEnumerator_1_t4607____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4607____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4607_InternalEnumerator_1__ctor_m28255_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28255_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28255_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4607_InternalEnumerator_1__ctor_m28255_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28255_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28256_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28256_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4607_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28256_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28257_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28257_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28257_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28258_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28258_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4607_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28258_GenericMethod/* genericMethod */

};
extern Il2CppType ISerializable_t482_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28259_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.ISerializable>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28259_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4607_il2cpp_TypeInfo/* declaring_type */
	, &ISerializable_t482_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28259_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4607_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28255_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28256_MethodInfo,
	&InternalEnumerator_1_Dispose_m28257_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28258_MethodInfo,
	&InternalEnumerator_1_get_Current_m28259_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m28258_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28257_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4607_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28256_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28258_MethodInfo,
	&InternalEnumerator_1_Dispose_m28257_MethodInfo,
	&InternalEnumerator_1_get_Current_m28259_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4607_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6562_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4607_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6562_il2cpp_TypeInfo, 7},
};
extern TypeInfo ISerializable_t482_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4607_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28259_MethodInfo/* Method Usage */,
	&ISerializable_t482_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisISerializable_t482_m37276_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4607_0_0_0;
extern Il2CppType InternalEnumerator_1_t4607_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4607_GenericClass;
TypeInfo InternalEnumerator_1_t4607_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4607_MethodInfos/* methods */
	, InternalEnumerator_1_t4607_PropertyInfos/* properties */
	, InternalEnumerator_1_t4607_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4607_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4607_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4607_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4607_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4607_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4607_1_0_0/* this_arg */
	, InternalEnumerator_1_t4607_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4607_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4607_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4607)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8297_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>
extern MethodInfo IList_1_get_Item_m47306_MethodInfo;
extern MethodInfo IList_1_set_Item_m47307_MethodInfo;
static PropertyInfo IList_1_t8297____Item_PropertyInfo = 
{
	&IList_1_t8297_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m47306_MethodInfo/* get */
	, &IList_1_set_Item_m47307_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8297_PropertyInfos[] =
{
	&IList_1_t8297____Item_PropertyInfo,
	NULL
};
extern Il2CppType ISerializable_t482_0_0_0;
static ParameterInfo IList_1_t8297_IList_1_IndexOf_m47308_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ISerializable_t482_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m47308_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::IndexOf(T)
MethodInfo IList_1_IndexOf_m47308_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8297_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8297_IList_1_IndexOf_m47308_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m47308_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ISerializable_t482_0_0_0;
static ParameterInfo IList_1_t8297_IList_1_Insert_m47309_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ISerializable_t482_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m47309_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m47309_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8297_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8297_IList_1_Insert_m47309_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m47309_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8297_IList_1_RemoveAt_m47310_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m47310_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m47310_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8297_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8297_IList_1_RemoveAt_m47310_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m47310_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8297_IList_1_get_Item_m47306_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ISerializable_t482_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m47306_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m47306_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8297_il2cpp_TypeInfo/* declaring_type */
	, &ISerializable_t482_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8297_IList_1_get_Item_m47306_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m47306_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ISerializable_t482_0_0_0;
static ParameterInfo IList_1_t8297_IList_1_set_Item_m47307_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ISerializable_t482_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m47307_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.Serialization.ISerializable>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m47307_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8297_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8297_IList_1_set_Item_m47307_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m47307_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8297_MethodInfos[] =
{
	&IList_1_IndexOf_m47308_MethodInfo,
	&IList_1_Insert_m47309_MethodInfo,
	&IList_1_RemoveAt_m47310_MethodInfo,
	&IList_1_get_Item_m47306_MethodInfo,
	&IList_1_set_Item_m47307_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8297_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8296_il2cpp_TypeInfo,
	&IEnumerable_1_t8298_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8297_0_0_0;
extern Il2CppType IList_1_t8297_1_0_0;
struct IList_1_t8297;
extern Il2CppGenericClass IList_1_t8297_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8297_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8297_MethodInfos/* methods */
	, IList_1_t8297_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8297_il2cpp_TypeInfo/* element_class */
	, IList_1_t8297_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8297_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8297_0_0_0/* byval_arg */
	, &IList_1_t8297_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8297_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_134.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4608_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_134MethodDeclarations.h"

// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_136.h"
extern TypeInfo MonoBehaviour_t6_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4609_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_136MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28262_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28264_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t4608____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t4608_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4608, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4608_FieldInfos[] =
{
	&CachedInvokableCall_1_t4608____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType MonoBehaviour_t6_0_0_0;
extern Il2CppType MonoBehaviour_t6_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4608_CachedInvokableCall_1__ctor_m28260_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t6_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28260_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28260_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t4608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4608_CachedInvokableCall_1__ctor_m28260_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28260_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4608_CachedInvokableCall_1_Invoke_m28261_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28261_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28261_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t4608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4608_CachedInvokableCall_1_Invoke_m28261_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28261_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4608_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28260_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28261_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28261_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28265_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4608_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28261_MethodInfo,
	&InvokableCall_1_Find_m28265_MethodInfo,
};
extern Il2CppType UnityAction_1_t4610_0_0_0;
extern TypeInfo UnityAction_1_t4610_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisMonoBehaviour_t6_m37286_MethodInfo;
extern TypeInfo MonoBehaviour_t6_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28267_MethodInfo;
extern TypeInfo MonoBehaviour_t6_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4608_RGCTXData[8] = 
{
	&UnityAction_1_t4610_0_0_0/* Type Usage */,
	&UnityAction_1_t4610_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMonoBehaviour_t6_m37286_MethodInfo/* Method Usage */,
	&MonoBehaviour_t6_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28267_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28262_MethodInfo/* Method Usage */,
	&MonoBehaviour_t6_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28264_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4608_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4608_1_0_0;
struct CachedInvokableCall_1_t4608;
extern Il2CppGenericClass CachedInvokableCall_1_t4608_GenericClass;
TypeInfo CachedInvokableCall_1_t4608_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4608_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4608_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4609_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4608_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4608_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4608_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4608_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4608_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4608_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4608_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4608)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_141.h"
extern TypeInfo UnityAction_1_t4610_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_141MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.MonoBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.MonoBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisMonoBehaviour_t6_m37286(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>
extern Il2CppType UnityAction_1_t4610_0_0_1;
FieldInfo InvokableCall_1_t4609____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4610_0_0_1/* type */
	, &InvokableCall_1_t4609_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4609, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4609_FieldInfos[] =
{
	&InvokableCall_1_t4609____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4609_InvokableCall_1__ctor_m28262_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28262_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28262_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t4609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4609_InvokableCall_1__ctor_m28262_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28262_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4610_0_0_0;
static ParameterInfo InvokableCall_1_t4609_InvokableCall_1__ctor_m28263_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4610_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28263_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28263_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t4609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4609_InvokableCall_1__ctor_m28263_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28263_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t4609_InvokableCall_1_Invoke_m28264_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28264_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28264_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t4609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4609_InvokableCall_1_Invoke_m28264_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28264_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4609_InvokableCall_1_Find_m28265_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28265_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28265_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t4609_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4609_InvokableCall_1_Find_m28265_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28265_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4609_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28262_MethodInfo,
	&InvokableCall_1__ctor_m28263_MethodInfo,
	&InvokableCall_1_Invoke_m28264_MethodInfo,
	&InvokableCall_1_Find_m28265_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4609_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m28264_MethodInfo,
	&InvokableCall_1_Find_m28265_MethodInfo,
};
extern TypeInfo UnityAction_1_t4610_il2cpp_TypeInfo;
extern TypeInfo MonoBehaviour_t6_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4609_RGCTXData[5] = 
{
	&UnityAction_1_t4610_0_0_0/* Type Usage */,
	&UnityAction_1_t4610_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMonoBehaviour_t6_m37286_MethodInfo/* Method Usage */,
	&MonoBehaviour_t6_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28267_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4609_0_0_0;
extern Il2CppType InvokableCall_1_t4609_1_0_0;
struct InvokableCall_1_t4609;
extern Il2CppGenericClass InvokableCall_1_t4609_GenericClass;
TypeInfo InvokableCall_1_t4609_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4609_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4609_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4609_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4609_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4609_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4609_0_0_0/* byval_arg */
	, &InvokableCall_1_t4609_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4609_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4609_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4609)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4610_UnityAction_1__ctor_m28266_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28266_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28266_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t4610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4610_UnityAction_1__ctor_m28266_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28266_GenericMethod/* genericMethod */

};
extern Il2CppType MonoBehaviour_t6_0_0_0;
static ParameterInfo UnityAction_1_t4610_UnityAction_1_Invoke_m28267_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t6_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28267_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28267_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t4610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4610_UnityAction_1_Invoke_m28267_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28267_GenericMethod/* genericMethod */

};
extern Il2CppType MonoBehaviour_t6_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4610_UnityAction_1_BeginInvoke_m28268_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t6_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28268_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28268_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t4610_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4610_UnityAction_1_BeginInvoke_m28268_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28268_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t4610_UnityAction_1_EndInvoke_m28269_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28269_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MonoBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28269_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t4610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4610_UnityAction_1_EndInvoke_m28269_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28269_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4610_MethodInfos[] =
{
	&UnityAction_1__ctor_m28266_MethodInfo,
	&UnityAction_1_Invoke_m28267_MethodInfo,
	&UnityAction_1_BeginInvoke_m28268_MethodInfo,
	&UnityAction_1_EndInvoke_m28269_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28268_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28269_MethodInfo;
static MethodInfo* UnityAction_1_t4610_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m28267_MethodInfo,
	&UnityAction_1_BeginInvoke_m28268_MethodInfo,
	&UnityAction_1_EndInvoke_m28269_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4610_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4610_1_0_0;
struct UnityAction_1_t4610;
extern Il2CppGenericClass UnityAction_1_t4610_GenericClass;
TypeInfo UnityAction_1_t4610_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4610_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4610_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4610_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4610_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4610_0_0_0/* byval_arg */
	, &UnityAction_1_t4610_1_0_0/* this_arg */
	, UnityAction_1_t4610_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4610_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4610)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6564_il2cpp_TypeInfo;

// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.TouchPhase>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.TouchPhase>
extern MethodInfo IEnumerator_1_get_Current_m47311_MethodInfo;
static PropertyInfo IEnumerator_1_t6564____Current_PropertyInfo = 
{
	&IEnumerator_1_t6564_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m47311_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6564_PropertyInfos[] =
{
	&IEnumerator_1_t6564____Current_PropertyInfo,
	NULL
};
extern Il2CppType TouchPhase_t998_0_0_0;
extern void* RuntimeInvoker_TouchPhase_t998 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m47311_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.TouchPhase>::get_Current()
MethodInfo IEnumerator_1_get_Current_m47311_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6564_il2cpp_TypeInfo/* declaring_type */
	, &TouchPhase_t998_0_0_0/* return_type */
	, RuntimeInvoker_TouchPhase_t998/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m47311_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6564_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m47311_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6564_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6564_0_0_0;
extern Il2CppType IEnumerator_1_t6564_1_0_0;
struct IEnumerator_1_t6564;
extern Il2CppGenericClass IEnumerator_1_t6564_GenericClass;
TypeInfo IEnumerator_1_t6564_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6564_MethodInfos/* methods */
	, IEnumerator_1_t6564_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6564_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6564_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6564_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6564_0_0_0/* byval_arg */
	, &IEnumerator_1_t6564_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6564_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_414.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4611_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_414MethodDeclarations.h"

extern TypeInfo TouchPhase_t998_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28274_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTouchPhase_t998_m37288_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.TouchPhase>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.TouchPhase>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisTouchPhase_t998_m37288 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m28270_MethodInfo;
 void InternalEnumerator_1__ctor_m28270 (InternalEnumerator_1_t4611 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28271_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28271 (InternalEnumerator_1_t4611 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m28274(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m28274_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&TouchPhase_t998_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m28272_MethodInfo;
 void InternalEnumerator_1_Dispose_m28272 (InternalEnumerator_1_t4611 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m28273_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m28273 (InternalEnumerator_1_t4611 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28274 (InternalEnumerator_1_t4611 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisTouchPhase_t998_m37288(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisTouchPhase_t998_m37288_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4611____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4611_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4611, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4611____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4611_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4611, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4611_FieldInfos[] =
{
	&InternalEnumerator_1_t4611____array_0_FieldInfo,
	&InternalEnumerator_1_t4611____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4611____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4611_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28271_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4611____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4611_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28274_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4611_PropertyInfos[] =
{
	&InternalEnumerator_1_t4611____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4611____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4611_InternalEnumerator_1__ctor_m28270_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28270_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28270_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m28270/* method */
	, &InternalEnumerator_1_t4611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4611_InternalEnumerator_1__ctor_m28270_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28270_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28271_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28271_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28271/* method */
	, &InternalEnumerator_1_t4611_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28271_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28272_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28272_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m28272/* method */
	, &InternalEnumerator_1_t4611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28272_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28273_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28273_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m28273/* method */
	, &InternalEnumerator_1_t4611_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28273_GenericMethod/* genericMethod */

};
extern Il2CppType TouchPhase_t998_0_0_0;
extern void* RuntimeInvoker_TouchPhase_t998 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28274_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.TouchPhase>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28274_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m28274/* method */
	, &InternalEnumerator_1_t4611_il2cpp_TypeInfo/* declaring_type */
	, &TouchPhase_t998_0_0_0/* return_type */
	, RuntimeInvoker_TouchPhase_t998/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28274_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4611_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28270_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28271_MethodInfo,
	&InternalEnumerator_1_Dispose_m28272_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28273_MethodInfo,
	&InternalEnumerator_1_get_Current_m28274_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4611_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28271_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28273_MethodInfo,
	&InternalEnumerator_1_Dispose_m28272_MethodInfo,
	&InternalEnumerator_1_get_Current_m28274_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4611_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6564_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4611_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6564_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4611_0_0_0;
extern Il2CppType InternalEnumerator_1_t4611_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4611_GenericClass;
TypeInfo InternalEnumerator_1_t4611_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4611_MethodInfos/* methods */
	, InternalEnumerator_1_t4611_PropertyInfos/* properties */
	, InternalEnumerator_1_t4611_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4611_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4611_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4611_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4611_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4611_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4611_1_0_0/* this_arg */
	, InternalEnumerator_1_t4611_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4611_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4611)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8299_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>
extern MethodInfo ICollection_1_get_Count_m47312_MethodInfo;
static PropertyInfo ICollection_1_t8299____Count_PropertyInfo = 
{
	&ICollection_1_t8299_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m47312_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m47313_MethodInfo;
static PropertyInfo ICollection_1_t8299____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8299_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m47313_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8299_PropertyInfos[] =
{
	&ICollection_1_t8299____Count_PropertyInfo,
	&ICollection_1_t8299____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m47312_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::get_Count()
MethodInfo ICollection_1_get_Count_m47312_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8299_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m47312_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m47313_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m47313_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8299_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m47313_GenericMethod/* genericMethod */

};
extern Il2CppType TouchPhase_t998_0_0_0;
extern Il2CppType TouchPhase_t998_0_0_0;
static ParameterInfo ICollection_1_t8299_ICollection_1_Add_m47314_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TouchPhase_t998_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m47314_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Add(T)
MethodInfo ICollection_1_Add_m47314_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8299_ICollection_1_Add_m47314_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m47314_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m47315_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Clear()
MethodInfo ICollection_1_Clear_m47315_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m47315_GenericMethod/* genericMethod */

};
extern Il2CppType TouchPhase_t998_0_0_0;
static ParameterInfo ICollection_1_t8299_ICollection_1_Contains_m47316_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TouchPhase_t998_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m47316_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Contains(T)
MethodInfo ICollection_1_Contains_m47316_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8299_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8299_ICollection_1_Contains_m47316_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m47316_GenericMethod/* genericMethod */

};
extern Il2CppType TouchPhaseU5BU5D_t5442_0_0_0;
extern Il2CppType TouchPhaseU5BU5D_t5442_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8299_ICollection_1_CopyTo_m47317_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TouchPhaseU5BU5D_t5442_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m47317_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m47317_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8299_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8299_ICollection_1_CopyTo_m47317_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m47317_GenericMethod/* genericMethod */

};
extern Il2CppType TouchPhase_t998_0_0_0;
static ParameterInfo ICollection_1_t8299_ICollection_1_Remove_m47318_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TouchPhase_t998_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m47318_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.TouchPhase>::Remove(T)
MethodInfo ICollection_1_Remove_m47318_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8299_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8299_ICollection_1_Remove_m47318_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m47318_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8299_MethodInfos[] =
{
	&ICollection_1_get_Count_m47312_MethodInfo,
	&ICollection_1_get_IsReadOnly_m47313_MethodInfo,
	&ICollection_1_Add_m47314_MethodInfo,
	&ICollection_1_Clear_m47315_MethodInfo,
	&ICollection_1_Contains_m47316_MethodInfo,
	&ICollection_1_CopyTo_m47317_MethodInfo,
	&ICollection_1_Remove_m47318_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8301_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8299_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8301_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8299_0_0_0;
extern Il2CppType ICollection_1_t8299_1_0_0;
struct ICollection_1_t8299;
extern Il2CppGenericClass ICollection_1_t8299_GenericClass;
TypeInfo ICollection_1_t8299_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8299_MethodInfos/* methods */
	, ICollection_1_t8299_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8299_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8299_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8299_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8299_0_0_0/* byval_arg */
	, &ICollection_1_t8299_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8299_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.TouchPhase>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.TouchPhase>
extern Il2CppType IEnumerator_1_t6564_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m47319_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.TouchPhase>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m47319_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8301_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6564_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m47319_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8301_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m47319_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8301_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8301_0_0_0;
extern Il2CppType IEnumerable_1_t8301_1_0_0;
struct IEnumerable_1_t8301;
extern Il2CppGenericClass IEnumerable_1_t8301_GenericClass;
TypeInfo IEnumerable_1_t8301_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8301_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8301_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8301_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8301_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8301_0_0_0/* byval_arg */
	, &IEnumerable_1_t8301_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8301_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8300_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.TouchPhase>
extern MethodInfo IList_1_get_Item_m47320_MethodInfo;
extern MethodInfo IList_1_set_Item_m47321_MethodInfo;
static PropertyInfo IList_1_t8300____Item_PropertyInfo = 
{
	&IList_1_t8300_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m47320_MethodInfo/* get */
	, &IList_1_set_Item_m47321_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8300_PropertyInfos[] =
{
	&IList_1_t8300____Item_PropertyInfo,
	NULL
};
extern Il2CppType TouchPhase_t998_0_0_0;
static ParameterInfo IList_1_t8300_IList_1_IndexOf_m47322_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TouchPhase_t998_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m47322_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::IndexOf(T)
MethodInfo IList_1_IndexOf_m47322_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8300_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8300_IList_1_IndexOf_m47322_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m47322_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TouchPhase_t998_0_0_0;
static ParameterInfo IList_1_t8300_IList_1_Insert_m47323_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TouchPhase_t998_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m47323_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m47323_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8300_IList_1_Insert_m47323_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m47323_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8300_IList_1_RemoveAt_m47324_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m47324_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m47324_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8300_IList_1_RemoveAt_m47324_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m47324_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8300_IList_1_get_Item_m47320_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType TouchPhase_t998_0_0_0;
extern void* RuntimeInvoker_TouchPhase_t998_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m47320_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m47320_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8300_il2cpp_TypeInfo/* declaring_type */
	, &TouchPhase_t998_0_0_0/* return_type */
	, RuntimeInvoker_TouchPhase_t998_Int32_t93/* invoker_method */
	, IList_1_t8300_IList_1_get_Item_m47320_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m47320_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TouchPhase_t998_0_0_0;
static ParameterInfo IList_1_t8300_IList_1_set_Item_m47321_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TouchPhase_t998_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m47321_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.TouchPhase>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m47321_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8300_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8300_IList_1_set_Item_m47321_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m47321_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8300_MethodInfos[] =
{
	&IList_1_IndexOf_m47322_MethodInfo,
	&IList_1_Insert_m47323_MethodInfo,
	&IList_1_RemoveAt_m47324_MethodInfo,
	&IList_1_get_Item_m47320_MethodInfo,
	&IList_1_set_Item_m47321_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8300_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8299_il2cpp_TypeInfo,
	&IEnumerable_1_t8301_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8300_0_0_0;
extern Il2CppType IList_1_t8300_1_0_0;
struct IList_1_t8300;
extern Il2CppGenericClass IList_1_t8300_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8300_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8300_MethodInfos/* methods */
	, IList_1_t8300_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8300_il2cpp_TypeInfo/* element_class */
	, IList_1_t8300_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8300_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8300_0_0_0/* byval_arg */
	, &IList_1_t8300_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8300_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6566_il2cpp_TypeInfo;

// UnityEngine.IMECompositionMode
#include "UnityEngine_UnityEngine_IMECompositionMode.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.IMECompositionMode>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.IMECompositionMode>
extern MethodInfo IEnumerator_1_get_Current_m47325_MethodInfo;
static PropertyInfo IEnumerator_1_t6566____Current_PropertyInfo = 
{
	&IEnumerator_1_t6566_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m47325_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6566_PropertyInfos[] =
{
	&IEnumerator_1_t6566____Current_PropertyInfo,
	NULL
};
extern Il2CppType IMECompositionMode_t999_0_0_0;
extern void* RuntimeInvoker_IMECompositionMode_t999 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m47325_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.IMECompositionMode>::get_Current()
MethodInfo IEnumerator_1_get_Current_m47325_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6566_il2cpp_TypeInfo/* declaring_type */
	, &IMECompositionMode_t999_0_0_0/* return_type */
	, RuntimeInvoker_IMECompositionMode_t999/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m47325_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6566_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m47325_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6566_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6566_0_0_0;
extern Il2CppType IEnumerator_1_t6566_1_0_0;
struct IEnumerator_1_t6566;
extern Il2CppGenericClass IEnumerator_1_t6566_GenericClass;
TypeInfo IEnumerator_1_t6566_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6566_MethodInfos/* methods */
	, IEnumerator_1_t6566_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6566_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6566_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6566_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6566_0_0_0/* byval_arg */
	, &IEnumerator_1_t6566_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6566_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_415.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4612_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_415MethodDeclarations.h"

extern TypeInfo IMECompositionMode_t999_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28279_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIMECompositionMode_t999_m37299_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.IMECompositionMode>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.IMECompositionMode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisIMECompositionMode_t999_m37299 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m28275_MethodInfo;
 void InternalEnumerator_1__ctor_m28275 (InternalEnumerator_1_t4612 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28276_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28276 (InternalEnumerator_1_t4612 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m28279(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m28279_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&IMECompositionMode_t999_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m28277_MethodInfo;
 void InternalEnumerator_1_Dispose_m28277 (InternalEnumerator_1_t4612 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m28278_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m28278 (InternalEnumerator_1_t4612 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28279 (InternalEnumerator_1_t4612 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisIMECompositionMode_t999_m37299(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisIMECompositionMode_t999_m37299_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4612____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4612_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4612, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4612____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4612_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4612, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4612_FieldInfos[] =
{
	&InternalEnumerator_1_t4612____array_0_FieldInfo,
	&InternalEnumerator_1_t4612____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4612____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4612_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28276_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4612____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4612_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28279_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4612_PropertyInfos[] =
{
	&InternalEnumerator_1_t4612____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4612____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4612_InternalEnumerator_1__ctor_m28275_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28275_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28275_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m28275/* method */
	, &InternalEnumerator_1_t4612_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4612_InternalEnumerator_1__ctor_m28275_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28275_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28276_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28276_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28276/* method */
	, &InternalEnumerator_1_t4612_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28276_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28277_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28277_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m28277/* method */
	, &InternalEnumerator_1_t4612_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28277_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28278_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28278_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m28278/* method */
	, &InternalEnumerator_1_t4612_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28278_GenericMethod/* genericMethod */

};
extern Il2CppType IMECompositionMode_t999_0_0_0;
extern void* RuntimeInvoker_IMECompositionMode_t999 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28279_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.IMECompositionMode>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28279_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m28279/* method */
	, &InternalEnumerator_1_t4612_il2cpp_TypeInfo/* declaring_type */
	, &IMECompositionMode_t999_0_0_0/* return_type */
	, RuntimeInvoker_IMECompositionMode_t999/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28279_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4612_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28275_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28276_MethodInfo,
	&InternalEnumerator_1_Dispose_m28277_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28278_MethodInfo,
	&InternalEnumerator_1_get_Current_m28279_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4612_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28276_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28278_MethodInfo,
	&InternalEnumerator_1_Dispose_m28277_MethodInfo,
	&InternalEnumerator_1_get_Current_m28279_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4612_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6566_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4612_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6566_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4612_0_0_0;
extern Il2CppType InternalEnumerator_1_t4612_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4612_GenericClass;
TypeInfo InternalEnumerator_1_t4612_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4612_MethodInfos/* methods */
	, InternalEnumerator_1_t4612_PropertyInfos/* properties */
	, InternalEnumerator_1_t4612_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4612_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4612_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4612_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4612_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4612_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4612_1_0_0/* this_arg */
	, InternalEnumerator_1_t4612_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4612_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4612)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8302_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>
extern MethodInfo ICollection_1_get_Count_m47326_MethodInfo;
static PropertyInfo ICollection_1_t8302____Count_PropertyInfo = 
{
	&ICollection_1_t8302_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m47326_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m47327_MethodInfo;
static PropertyInfo ICollection_1_t8302____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8302_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m47327_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8302_PropertyInfos[] =
{
	&ICollection_1_t8302____Count_PropertyInfo,
	&ICollection_1_t8302____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m47326_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::get_Count()
MethodInfo ICollection_1_get_Count_m47326_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8302_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m47326_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m47327_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m47327_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8302_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m47327_GenericMethod/* genericMethod */

};
extern Il2CppType IMECompositionMode_t999_0_0_0;
extern Il2CppType IMECompositionMode_t999_0_0_0;
static ParameterInfo ICollection_1_t8302_ICollection_1_Add_m47328_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMECompositionMode_t999_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m47328_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Add(T)
MethodInfo ICollection_1_Add_m47328_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8302_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8302_ICollection_1_Add_m47328_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m47328_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m47329_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Clear()
MethodInfo ICollection_1_Clear_m47329_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8302_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m47329_GenericMethod/* genericMethod */

};
extern Il2CppType IMECompositionMode_t999_0_0_0;
static ParameterInfo ICollection_1_t8302_ICollection_1_Contains_m47330_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMECompositionMode_t999_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m47330_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Contains(T)
MethodInfo ICollection_1_Contains_m47330_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8302_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8302_ICollection_1_Contains_m47330_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m47330_GenericMethod/* genericMethod */

};
extern Il2CppType IMECompositionModeU5BU5D_t5443_0_0_0;
extern Il2CppType IMECompositionModeU5BU5D_t5443_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8302_ICollection_1_CopyTo_m47331_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IMECompositionModeU5BU5D_t5443_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m47331_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m47331_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8302_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8302_ICollection_1_CopyTo_m47331_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m47331_GenericMethod/* genericMethod */

};
extern Il2CppType IMECompositionMode_t999_0_0_0;
static ParameterInfo ICollection_1_t8302_ICollection_1_Remove_m47332_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMECompositionMode_t999_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m47332_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.IMECompositionMode>::Remove(T)
MethodInfo ICollection_1_Remove_m47332_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8302_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8302_ICollection_1_Remove_m47332_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m47332_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8302_MethodInfos[] =
{
	&ICollection_1_get_Count_m47326_MethodInfo,
	&ICollection_1_get_IsReadOnly_m47327_MethodInfo,
	&ICollection_1_Add_m47328_MethodInfo,
	&ICollection_1_Clear_m47329_MethodInfo,
	&ICollection_1_Contains_m47330_MethodInfo,
	&ICollection_1_CopyTo_m47331_MethodInfo,
	&ICollection_1_Remove_m47332_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8304_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8302_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8304_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8302_0_0_0;
extern Il2CppType ICollection_1_t8302_1_0_0;
struct ICollection_1_t8302;
extern Il2CppGenericClass ICollection_1_t8302_GenericClass;
TypeInfo ICollection_1_t8302_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8302_MethodInfos/* methods */
	, ICollection_1_t8302_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8302_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8302_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8302_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8302_0_0_0/* byval_arg */
	, &ICollection_1_t8302_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8302_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.IMECompositionMode>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.IMECompositionMode>
extern Il2CppType IEnumerator_1_t6566_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m47333_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.IMECompositionMode>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m47333_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8304_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6566_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m47333_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8304_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m47333_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8304_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8304_0_0_0;
extern Il2CppType IEnumerable_1_t8304_1_0_0;
struct IEnumerable_1_t8304;
extern Il2CppGenericClass IEnumerable_1_t8304_GenericClass;
TypeInfo IEnumerable_1_t8304_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8304_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8304_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8304_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8304_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8304_0_0_0/* byval_arg */
	, &IEnumerable_1_t8304_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8304_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8303_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>
extern MethodInfo IList_1_get_Item_m47334_MethodInfo;
extern MethodInfo IList_1_set_Item_m47335_MethodInfo;
static PropertyInfo IList_1_t8303____Item_PropertyInfo = 
{
	&IList_1_t8303_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m47334_MethodInfo/* get */
	, &IList_1_set_Item_m47335_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8303_PropertyInfos[] =
{
	&IList_1_t8303____Item_PropertyInfo,
	NULL
};
extern Il2CppType IMECompositionMode_t999_0_0_0;
static ParameterInfo IList_1_t8303_IList_1_IndexOf_m47336_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMECompositionMode_t999_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m47336_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::IndexOf(T)
MethodInfo IList_1_IndexOf_m47336_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8303_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8303_IList_1_IndexOf_m47336_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m47336_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IMECompositionMode_t999_0_0_0;
static ParameterInfo IList_1_t8303_IList_1_Insert_m47337_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IMECompositionMode_t999_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m47337_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m47337_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8303_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8303_IList_1_Insert_m47337_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m47337_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8303_IList_1_RemoveAt_m47338_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m47338_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m47338_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8303_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8303_IList_1_RemoveAt_m47338_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m47338_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8303_IList_1_get_Item_m47334_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IMECompositionMode_t999_0_0_0;
extern void* RuntimeInvoker_IMECompositionMode_t999_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m47334_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m47334_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8303_il2cpp_TypeInfo/* declaring_type */
	, &IMECompositionMode_t999_0_0_0/* return_type */
	, RuntimeInvoker_IMECompositionMode_t999_Int32_t93/* invoker_method */
	, IList_1_t8303_IList_1_get_Item_m47334_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m47334_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IMECompositionMode_t999_0_0_0;
static ParameterInfo IList_1_t8303_IList_1_set_Item_m47335_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IMECompositionMode_t999_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m47335_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.IMECompositionMode>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m47335_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8303_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8303_IList_1_set_Item_m47335_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m47335_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8303_MethodInfos[] =
{
	&IList_1_IndexOf_m47336_MethodInfo,
	&IList_1_Insert_m47337_MethodInfo,
	&IList_1_RemoveAt_m47338_MethodInfo,
	&IList_1_get_Item_m47334_MethodInfo,
	&IList_1_set_Item_m47335_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8303_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8302_il2cpp_TypeInfo,
	&IEnumerable_1_t8304_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8303_0_0_0;
extern Il2CppType IList_1_t8303_1_0_0;
struct IList_1_t8303;
extern Il2CppGenericClass IList_1_t8303_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8303_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8303_MethodInfos/* methods */
	, IList_1_t8303_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8303_il2cpp_TypeInfo/* element_class */
	, IList_1_t8303_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8303_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8303_0_0_0/* byval_arg */
	, &IList_1_t8303_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8303_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6568_il2cpp_TypeInfo;

// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.HideFlags>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.HideFlags>
extern MethodInfo IEnumerator_1_get_Current_m47339_MethodInfo;
static PropertyInfo IEnumerator_1_t6568____Current_PropertyInfo = 
{
	&IEnumerator_1_t6568_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m47339_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6568_PropertyInfos[] =
{
	&IEnumerator_1_t6568____Current_PropertyInfo,
	NULL
};
extern Il2CppType HideFlags_t1000_0_0_0;
extern void* RuntimeInvoker_HideFlags_t1000 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m47339_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.HideFlags>::get_Current()
MethodInfo IEnumerator_1_get_Current_m47339_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6568_il2cpp_TypeInfo/* declaring_type */
	, &HideFlags_t1000_0_0_0/* return_type */
	, RuntimeInvoker_HideFlags_t1000/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m47339_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6568_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m47339_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6568_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6568_0_0_0;
extern Il2CppType IEnumerator_1_t6568_1_0_0;
struct IEnumerator_1_t6568;
extern Il2CppGenericClass IEnumerator_1_t6568_GenericClass;
TypeInfo IEnumerator_1_t6568_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6568_MethodInfos/* methods */
	, IEnumerator_1_t6568_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6568_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6568_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6568_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6568_0_0_0/* byval_arg */
	, &IEnumerator_1_t6568_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6568_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.HideFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_416.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4613_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.HideFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_416MethodDeclarations.h"

extern TypeInfo HideFlags_t1000_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28284_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisHideFlags_t1000_m37310_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.HideFlags>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.HideFlags>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisHideFlags_t1000_m37310 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.HideFlags>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m28280_MethodInfo;
 void InternalEnumerator_1__ctor_m28280 (InternalEnumerator_1_t4613 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.HideFlags>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28281_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28281 (InternalEnumerator_1_t4613 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m28284(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m28284_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&HideFlags_t1000_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.HideFlags>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m28282_MethodInfo;
 void InternalEnumerator_1_Dispose_m28282 (InternalEnumerator_1_t4613 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.HideFlags>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m28283_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m28283 (InternalEnumerator_1_t4613 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.HideFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28284 (InternalEnumerator_1_t4613 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisHideFlags_t1000_m37310(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisHideFlags_t1000_m37310_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.HideFlags>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4613____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4613_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4613, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4613____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4613_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4613, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4613_FieldInfos[] =
{
	&InternalEnumerator_1_t4613____array_0_FieldInfo,
	&InternalEnumerator_1_t4613____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4613____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4613_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28281_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4613____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4613_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28284_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4613_PropertyInfos[] =
{
	&InternalEnumerator_1_t4613____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4613____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4613_InternalEnumerator_1__ctor_m28280_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28280_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.HideFlags>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28280_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m28280/* method */
	, &InternalEnumerator_1_t4613_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4613_InternalEnumerator_1__ctor_m28280_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28280_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28281_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.HideFlags>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28281_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28281/* method */
	, &InternalEnumerator_1_t4613_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28281_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28282_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.HideFlags>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28282_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m28282/* method */
	, &InternalEnumerator_1_t4613_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28282_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28283_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.HideFlags>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28283_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m28283/* method */
	, &InternalEnumerator_1_t4613_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28283_GenericMethod/* genericMethod */

};
extern Il2CppType HideFlags_t1000_0_0_0;
extern void* RuntimeInvoker_HideFlags_t1000 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28284_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.HideFlags>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28284_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m28284/* method */
	, &InternalEnumerator_1_t4613_il2cpp_TypeInfo/* declaring_type */
	, &HideFlags_t1000_0_0_0/* return_type */
	, RuntimeInvoker_HideFlags_t1000/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28284_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4613_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28280_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28281_MethodInfo,
	&InternalEnumerator_1_Dispose_m28282_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28283_MethodInfo,
	&InternalEnumerator_1_get_Current_m28284_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4613_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28281_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28283_MethodInfo,
	&InternalEnumerator_1_Dispose_m28282_MethodInfo,
	&InternalEnumerator_1_get_Current_m28284_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4613_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6568_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4613_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6568_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4613_0_0_0;
extern Il2CppType InternalEnumerator_1_t4613_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4613_GenericClass;
TypeInfo InternalEnumerator_1_t4613_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4613_MethodInfos/* methods */
	, InternalEnumerator_1_t4613_PropertyInfos/* properties */
	, InternalEnumerator_1_t4613_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4613_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4613_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4613_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4613_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4613_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4613_1_0_0/* this_arg */
	, InternalEnumerator_1_t4613_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4613_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4613)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8305_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>
extern MethodInfo ICollection_1_get_Count_m47340_MethodInfo;
static PropertyInfo ICollection_1_t8305____Count_PropertyInfo = 
{
	&ICollection_1_t8305_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m47340_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m47341_MethodInfo;
static PropertyInfo ICollection_1_t8305____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8305_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m47341_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8305_PropertyInfos[] =
{
	&ICollection_1_t8305____Count_PropertyInfo,
	&ICollection_1_t8305____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m47340_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>::get_Count()
MethodInfo ICollection_1_get_Count_m47340_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8305_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m47340_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m47341_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m47341_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8305_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m47341_GenericMethod/* genericMethod */

};
extern Il2CppType HideFlags_t1000_0_0_0;
extern Il2CppType HideFlags_t1000_0_0_0;
static ParameterInfo ICollection_1_t8305_ICollection_1_Add_m47342_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideFlags_t1000_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m47342_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>::Add(T)
MethodInfo ICollection_1_Add_m47342_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8305_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8305_ICollection_1_Add_m47342_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m47342_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m47343_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>::Clear()
MethodInfo ICollection_1_Clear_m47343_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8305_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m47343_GenericMethod/* genericMethod */

};
extern Il2CppType HideFlags_t1000_0_0_0;
static ParameterInfo ICollection_1_t8305_ICollection_1_Contains_m47344_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideFlags_t1000_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m47344_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>::Contains(T)
MethodInfo ICollection_1_Contains_m47344_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8305_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8305_ICollection_1_Contains_m47344_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m47344_GenericMethod/* genericMethod */

};
extern Il2CppType HideFlagsU5BU5D_t5444_0_0_0;
extern Il2CppType HideFlagsU5BU5D_t5444_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8305_ICollection_1_CopyTo_m47345_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &HideFlagsU5BU5D_t5444_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m47345_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m47345_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8305_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8305_ICollection_1_CopyTo_m47345_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m47345_GenericMethod/* genericMethod */

};
extern Il2CppType HideFlags_t1000_0_0_0;
static ParameterInfo ICollection_1_t8305_ICollection_1_Remove_m47346_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideFlags_t1000_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m47346_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.HideFlags>::Remove(T)
MethodInfo ICollection_1_Remove_m47346_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8305_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8305_ICollection_1_Remove_m47346_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m47346_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8305_MethodInfos[] =
{
	&ICollection_1_get_Count_m47340_MethodInfo,
	&ICollection_1_get_IsReadOnly_m47341_MethodInfo,
	&ICollection_1_Add_m47342_MethodInfo,
	&ICollection_1_Clear_m47343_MethodInfo,
	&ICollection_1_Contains_m47344_MethodInfo,
	&ICollection_1_CopyTo_m47345_MethodInfo,
	&ICollection_1_Remove_m47346_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8307_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8305_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8307_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8305_0_0_0;
extern Il2CppType ICollection_1_t8305_1_0_0;
struct ICollection_1_t8305;
extern Il2CppGenericClass ICollection_1_t8305_GenericClass;
TypeInfo ICollection_1_t8305_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8305_MethodInfos/* methods */
	, ICollection_1_t8305_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8305_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8305_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8305_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8305_0_0_0/* byval_arg */
	, &ICollection_1_t8305_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8305_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.HideFlags>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.HideFlags>
extern Il2CppType IEnumerator_1_t6568_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m47347_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.HideFlags>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m47347_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8307_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6568_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m47347_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8307_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m47347_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8307_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8307_0_0_0;
extern Il2CppType IEnumerable_1_t8307_1_0_0;
struct IEnumerable_1_t8307;
extern Il2CppGenericClass IEnumerable_1_t8307_GenericClass;
TypeInfo IEnumerable_1_t8307_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8307_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8307_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8307_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8307_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8307_0_0_0/* byval_arg */
	, &IEnumerable_1_t8307_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8307_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8306_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.HideFlags>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.HideFlags>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.HideFlags>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.HideFlags>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.HideFlags>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.HideFlags>
extern MethodInfo IList_1_get_Item_m47348_MethodInfo;
extern MethodInfo IList_1_set_Item_m47349_MethodInfo;
static PropertyInfo IList_1_t8306____Item_PropertyInfo = 
{
	&IList_1_t8306_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m47348_MethodInfo/* get */
	, &IList_1_set_Item_m47349_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8306_PropertyInfos[] =
{
	&IList_1_t8306____Item_PropertyInfo,
	NULL
};
extern Il2CppType HideFlags_t1000_0_0_0;
static ParameterInfo IList_1_t8306_IList_1_IndexOf_m47350_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideFlags_t1000_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m47350_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.HideFlags>::IndexOf(T)
MethodInfo IList_1_IndexOf_m47350_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8306_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8306_IList_1_IndexOf_m47350_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m47350_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType HideFlags_t1000_0_0_0;
static ParameterInfo IList_1_t8306_IList_1_Insert_m47351_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &HideFlags_t1000_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m47351_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.HideFlags>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m47351_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8306_IList_1_Insert_m47351_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m47351_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8306_IList_1_RemoveAt_m47352_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m47352_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.HideFlags>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m47352_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8306_IList_1_RemoveAt_m47352_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m47352_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8306_IList_1_get_Item_m47348_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType HideFlags_t1000_0_0_0;
extern void* RuntimeInvoker_HideFlags_t1000_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m47348_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.HideFlags>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m47348_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8306_il2cpp_TypeInfo/* declaring_type */
	, &HideFlags_t1000_0_0_0/* return_type */
	, RuntimeInvoker_HideFlags_t1000_Int32_t93/* invoker_method */
	, IList_1_t8306_IList_1_get_Item_m47348_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m47348_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType HideFlags_t1000_0_0_0;
static ParameterInfo IList_1_t8306_IList_1_set_Item_m47349_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &HideFlags_t1000_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m47349_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.HideFlags>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m47349_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8306_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8306_IList_1_set_Item_m47349_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m47349_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8306_MethodInfos[] =
{
	&IList_1_IndexOf_m47350_MethodInfo,
	&IList_1_Insert_m47351_MethodInfo,
	&IList_1_RemoveAt_m47352_MethodInfo,
	&IList_1_get_Item_m47348_MethodInfo,
	&IList_1_set_Item_m47349_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8306_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8305_il2cpp_TypeInfo,
	&IEnumerable_1_t8307_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8306_0_0_0;
extern Il2CppType IList_1_t8306_1_0_0;
struct IList_1_t8306;
extern Il2CppGenericClass IList_1_t8306_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8306_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8306_MethodInfos/* methods */
	, IList_1_t8306_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8306_il2cpp_TypeInfo/* element_class */
	, IList_1_t8306_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8306_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8306_0_0_0/* byval_arg */
	, &IList_1_t8306_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8306_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Object>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_135.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4614_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Object>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_135MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_137.h"
extern TypeInfo Object_t117_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4615_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_137MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28287_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28289_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Object>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Object>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t4614____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t4614_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4614, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4614_FieldInfos[] =
{
	&CachedInvokableCall_1_t4614____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType Object_t117_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4614_CachedInvokableCall_1__ctor_m28285_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28285_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28285_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t4614_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4614_CachedInvokableCall_1__ctor_m28285_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28285_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4614_CachedInvokableCall_1_Invoke_m28286_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28286_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Object>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28286_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t4614_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4614_CachedInvokableCall_1_Invoke_m28286_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28286_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4614_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28285_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28286_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28286_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28290_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4614_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28286_MethodInfo,
	&InvokableCall_1_Find_m28290_MethodInfo,
};
extern Il2CppType UnityAction_1_t4616_0_0_0;
extern TypeInfo UnityAction_1_t4616_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisObject_t117_m37320_MethodInfo;
extern TypeInfo Object_t117_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28292_MethodInfo;
extern TypeInfo Object_t117_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4614_RGCTXData[8] = 
{
	&UnityAction_1_t4616_0_0_0/* Type Usage */,
	&UnityAction_1_t4616_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisObject_t117_m37320_MethodInfo/* Method Usage */,
	&Object_t117_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28292_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28287_MethodInfo/* Method Usage */,
	&Object_t117_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28289_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4614_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4614_1_0_0;
struct CachedInvokableCall_1_t4614;
extern Il2CppGenericClass CachedInvokableCall_1_t4614_GenericClass;
TypeInfo CachedInvokableCall_1_t4614_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4614_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4614_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4615_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4614_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4614_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4614_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4614_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4614_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4614_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4614_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4614)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Object>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_142.h"
extern TypeInfo UnityAction_1_t4616_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Object>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_142MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Object>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t117_m37320(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Object>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Object>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Object>
extern Il2CppType UnityAction_1_t4616_0_0_1;
FieldInfo InvokableCall_1_t4615____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4616_0_0_1/* type */
	, &InvokableCall_1_t4615_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4615, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4615_FieldInfos[] =
{
	&InvokableCall_1_t4615____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4615_InvokableCall_1__ctor_m28287_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28287_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28287_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t4615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4615_InvokableCall_1__ctor_m28287_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28287_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4616_0_0_0;
static ParameterInfo InvokableCall_1_t4615_InvokableCall_1__ctor_m28288_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4616_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28288_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28288_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t4615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4615_InvokableCall_1__ctor_m28288_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28288_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t4615_InvokableCall_1_Invoke_m28289_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28289_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Object>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28289_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t4615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4615_InvokableCall_1_Invoke_m28289_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28289_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4615_InvokableCall_1_Find_m28290_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28290_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Object>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28290_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t4615_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4615_InvokableCall_1_Find_m28290_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28290_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4615_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28287_MethodInfo,
	&InvokableCall_1__ctor_m28288_MethodInfo,
	&InvokableCall_1_Invoke_m28289_MethodInfo,
	&InvokableCall_1_Find_m28290_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4615_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m28289_MethodInfo,
	&InvokableCall_1_Find_m28290_MethodInfo,
};
extern TypeInfo UnityAction_1_t4616_il2cpp_TypeInfo;
extern TypeInfo Object_t117_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4615_RGCTXData[5] = 
{
	&UnityAction_1_t4616_0_0_0/* Type Usage */,
	&UnityAction_1_t4616_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisObject_t117_m37320_MethodInfo/* Method Usage */,
	&Object_t117_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28292_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4615_0_0_0;
extern Il2CppType InvokableCall_1_t4615_1_0_0;
struct InvokableCall_1_t4615;
extern Il2CppGenericClass InvokableCall_1_t4615_GenericClass;
TypeInfo InvokableCall_1_t4615_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4615_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4615_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4615_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4615_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4615_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4615_0_0_0/* byval_arg */
	, &InvokableCall_1_t4615_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4615_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4615_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4615)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Object>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Object>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Object>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Object>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Object>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4616_UnityAction_1__ctor_m28291_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28291_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Object>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28291_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t4616_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4616_UnityAction_1__ctor_m28291_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28291_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t117_0_0_0;
static ParameterInfo UnityAction_1_t4616_UnityAction_1_Invoke_m28292_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28292_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Object>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28292_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t4616_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4616_UnityAction_1_Invoke_m28292_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28292_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4616_UnityAction_1_BeginInvoke_m28293_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28293_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Object>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28293_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t4616_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4616_UnityAction_1_BeginInvoke_m28293_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28293_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t4616_UnityAction_1_EndInvoke_m28294_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28294_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Object>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28294_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t4616_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4616_UnityAction_1_EndInvoke_m28294_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28294_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4616_MethodInfos[] =
{
	&UnityAction_1__ctor_m28291_MethodInfo,
	&UnityAction_1_Invoke_m28292_MethodInfo,
	&UnityAction_1_BeginInvoke_m28293_MethodInfo,
	&UnityAction_1_EndInvoke_m28294_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28293_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28294_MethodInfo;
static MethodInfo* UnityAction_1_t4616_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m28292_MethodInfo,
	&UnityAction_1_BeginInvoke_m28293_MethodInfo,
	&UnityAction_1_EndInvoke_m28294_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4616_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4616_1_0_0;
struct UnityAction_1_t4616;
extern Il2CppGenericClass UnityAction_1_t4616_GenericClass;
TypeInfo UnityAction_1_t4616_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4616_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4616_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4616_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4616_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4616_0_0_0/* byval_arg */
	, &UnityAction_1_t4616_1_0_0/* this_arg */
	, UnityAction_1_t4616_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4616_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4616)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_136.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4617_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_136MethodDeclarations.h"

// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_138.h"
extern TypeInfo Component_t100_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4618_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_138MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28297_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28299_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Component>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Component>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Component>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t4617____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t4617_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4617, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4617_FieldInfos[] =
{
	&CachedInvokableCall_1_t4617____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType Component_t100_0_0_0;
extern Il2CppType Component_t100_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4617_CachedInvokableCall_1__ctor_m28295_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Component_t100_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28295_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Component>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28295_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t4617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4617_CachedInvokableCall_1__ctor_m28295_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28295_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4617_CachedInvokableCall_1_Invoke_m28296_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28296_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Component>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28296_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t4617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4617_CachedInvokableCall_1_Invoke_m28296_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28296_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4617_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28295_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28296_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28296_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28300_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4617_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28296_MethodInfo,
	&InvokableCall_1_Find_m28300_MethodInfo,
};
extern Il2CppType UnityAction_1_t393_0_0_0;
extern TypeInfo UnityAction_1_t393_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisComponent_t100_m37321_MethodInfo;
extern TypeInfo Component_t100_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m2535_MethodInfo;
extern TypeInfo Component_t100_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4617_RGCTXData[8] = 
{
	&UnityAction_1_t393_0_0_0/* Type Usage */,
	&UnityAction_1_t393_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisComponent_t100_m37321_MethodInfo/* Method Usage */,
	&Component_t100_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m2535_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28297_MethodInfo/* Method Usage */,
	&Component_t100_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28299_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4617_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4617_1_0_0;
struct CachedInvokableCall_1_t4617;
extern Il2CppGenericClass CachedInvokableCall_1_t4617_GenericClass;
TypeInfo CachedInvokableCall_1_t4617_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4617_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4617_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4618_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4617_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4617_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4617_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4617_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4617_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4617_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4617_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4617)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_3.h"
extern TypeInfo UnityAction_1_t393_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_3MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Component>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Component>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisComponent_t100_m37321(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Component>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Component>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Component>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Component>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Component>
extern Il2CppType UnityAction_1_t393_0_0_1;
FieldInfo InvokableCall_1_t4618____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t393_0_0_1/* type */
	, &InvokableCall_1_t4618_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4618, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4618_FieldInfos[] =
{
	&InvokableCall_1_t4618____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4618_InvokableCall_1__ctor_m28297_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28297_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Component>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28297_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t4618_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4618_InvokableCall_1__ctor_m28297_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28297_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t393_0_0_0;
static ParameterInfo InvokableCall_1_t4618_InvokableCall_1__ctor_m28298_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t393_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28298_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Component>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28298_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t4618_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4618_InvokableCall_1__ctor_m28298_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28298_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t4618_InvokableCall_1_Invoke_m28299_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28299_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Component>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28299_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t4618_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4618_InvokableCall_1_Invoke_m28299_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28299_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4618_InvokableCall_1_Find_m28300_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28300_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Component>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28300_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t4618_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4618_InvokableCall_1_Find_m28300_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28300_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4618_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28297_MethodInfo,
	&InvokableCall_1__ctor_m28298_MethodInfo,
	&InvokableCall_1_Invoke_m28299_MethodInfo,
	&InvokableCall_1_Find_m28300_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4618_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m28299_MethodInfo,
	&InvokableCall_1_Find_m28300_MethodInfo,
};
extern TypeInfo UnityAction_1_t393_il2cpp_TypeInfo;
extern TypeInfo Component_t100_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4618_RGCTXData[5] = 
{
	&UnityAction_1_t393_0_0_0/* Type Usage */,
	&UnityAction_1_t393_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisComponent_t100_m37321_MethodInfo/* Method Usage */,
	&Component_t100_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m2535_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4618_0_0_0;
extern Il2CppType InvokableCall_1_t4618_1_0_0;
struct InvokableCall_1_t4618;
extern Il2CppGenericClass InvokableCall_1_t4618_GenericClass;
TypeInfo InvokableCall_1_t4618_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4618_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4618_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4618_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4618_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4618_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4618_0_0_0/* byval_arg */
	, &InvokableCall_1_t4618_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4618_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4618_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4618)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GameObject>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_137.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4619_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GameObject>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_137MethodDeclarations.h"

// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_139.h"
extern TypeInfo GameObject_t2_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4620_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_139MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28303_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28305_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GameObject>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GameObject>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GameObject>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t4619____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t4619_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4619, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4619_FieldInfos[] =
{
	&CachedInvokableCall_1_t4619____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType GameObject_t2_0_0_0;
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4619_CachedInvokableCall_1__ctor_m28301_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28301_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GameObject>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28301_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t4619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4619_CachedInvokableCall_1__ctor_m28301_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28301_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4619_CachedInvokableCall_1_Invoke_m28302_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28302_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GameObject>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28302_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t4619_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4619_CachedInvokableCall_1_Invoke_m28302_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28302_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4619_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28301_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28302_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28302_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28306_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4619_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28302_MethodInfo,
	&InvokableCall_1_Find_m28306_MethodInfo,
};
extern Il2CppType UnityAction_1_t4621_0_0_0;
extern TypeInfo UnityAction_1_t4621_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisGameObject_t2_m37322_MethodInfo;
extern TypeInfo GameObject_t2_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28308_MethodInfo;
extern TypeInfo GameObject_t2_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4619_RGCTXData[8] = 
{
	&UnityAction_1_t4621_0_0_0/* Type Usage */,
	&UnityAction_1_t4621_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGameObject_t2_m37322_MethodInfo/* Method Usage */,
	&GameObject_t2_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28308_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28303_MethodInfo/* Method Usage */,
	&GameObject_t2_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28305_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4619_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4619_1_0_0;
struct CachedInvokableCall_1_t4619;
extern Il2CppGenericClass CachedInvokableCall_1_t4619_GenericClass;
TypeInfo CachedInvokableCall_1_t4619_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4619_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4619_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4620_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4619_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4619_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4619_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4619_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4619_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4619_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4619_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4619)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_143.h"
extern TypeInfo UnityAction_1_t4621_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_143MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.GameObject>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.GameObject>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisGameObject_t2_m37322(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>
extern Il2CppType UnityAction_1_t4621_0_0_1;
FieldInfo InvokableCall_1_t4620____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4621_0_0_1/* type */
	, &InvokableCall_1_t4620_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4620, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4620_FieldInfos[] =
{
	&InvokableCall_1_t4620____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4620_InvokableCall_1__ctor_m28303_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28303_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28303_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t4620_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4620_InvokableCall_1__ctor_m28303_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28303_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4621_0_0_0;
static ParameterInfo InvokableCall_1_t4620_InvokableCall_1__ctor_m28304_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4621_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28304_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28304_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t4620_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4620_InvokableCall_1__ctor_m28304_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28304_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t4620_InvokableCall_1_Invoke_m28305_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28305_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28305_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t4620_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4620_InvokableCall_1_Invoke_m28305_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28305_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4620_InvokableCall_1_Find_m28306_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28306_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.GameObject>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28306_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t4620_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4620_InvokableCall_1_Find_m28306_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28306_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4620_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28303_MethodInfo,
	&InvokableCall_1__ctor_m28304_MethodInfo,
	&InvokableCall_1_Invoke_m28305_MethodInfo,
	&InvokableCall_1_Find_m28306_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4620_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m28305_MethodInfo,
	&InvokableCall_1_Find_m28306_MethodInfo,
};
extern TypeInfo UnityAction_1_t4621_il2cpp_TypeInfo;
extern TypeInfo GameObject_t2_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4620_RGCTXData[5] = 
{
	&UnityAction_1_t4621_0_0_0/* Type Usage */,
	&UnityAction_1_t4621_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGameObject_t2_m37322_MethodInfo/* Method Usage */,
	&GameObject_t2_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28308_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4620_0_0_0;
extern Il2CppType InvokableCall_1_t4620_1_0_0;
struct InvokableCall_1_t4620;
extern Il2CppGenericClass InvokableCall_1_t4620_GenericClass;
TypeInfo InvokableCall_1_t4620_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4620_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4620_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4620_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4620_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4620_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4620_0_0_0/* byval_arg */
	, &InvokableCall_1_t4620_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4620_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4620_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4620)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4621_UnityAction_1__ctor_m28307_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28307_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28307_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t4621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4621_UnityAction_1__ctor_m28307_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28307_GenericMethod/* genericMethod */

};
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo UnityAction_1_t4621_UnityAction_1_Invoke_m28308_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28308_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28308_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t4621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4621_UnityAction_1_Invoke_m28308_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28308_GenericMethod/* genericMethod */

};
extern Il2CppType GameObject_t2_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4621_UnityAction_1_BeginInvoke_m28309_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28309_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28309_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t4621_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4621_UnityAction_1_BeginInvoke_m28309_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28309_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t4621_UnityAction_1_EndInvoke_m28310_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28310_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28310_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t4621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4621_UnityAction_1_EndInvoke_m28310_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28310_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4621_MethodInfos[] =
{
	&UnityAction_1__ctor_m28307_MethodInfo,
	&UnityAction_1_Invoke_m28308_MethodInfo,
	&UnityAction_1_BeginInvoke_m28309_MethodInfo,
	&UnityAction_1_EndInvoke_m28310_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28309_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28310_MethodInfo;
static MethodInfo* UnityAction_1_t4621_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m28308_MethodInfo,
	&UnityAction_1_BeginInvoke_m28309_MethodInfo,
	&UnityAction_1_EndInvoke_m28310_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4621_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4621_1_0_0;
struct UnityAction_1_t4621;
extern Il2CppGenericClass UnityAction_1_t4621_GenericClass;
TypeInfo UnityAction_1_t4621_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4621_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4621_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4621_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4621_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4621_0_0_0/* byval_arg */
	, &UnityAction_1_t4621_1_0_0/* this_arg */
	, UnityAction_1_t4621_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4621_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4621)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Transform>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_138.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4622_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Transform>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_138MethodDeclarations.h"

// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.Transform>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_140.h"
extern TypeInfo Transform_t10_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4623_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Transform>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_140MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28313_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28315_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Transform>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Transform>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Transform>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t4622____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t4622_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4622, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4622_FieldInfos[] =
{
	&CachedInvokableCall_1_t4622____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType Transform_t10_0_0_0;
extern Il2CppType Transform_t10_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4622_CachedInvokableCall_1__ctor_m28311_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Transform_t10_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28311_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Transform>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28311_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t4622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4622_CachedInvokableCall_1__ctor_m28311_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28311_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4622_CachedInvokableCall_1_Invoke_m28312_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28312_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Transform>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28312_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t4622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4622_CachedInvokableCall_1_Invoke_m28312_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28312_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4622_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28311_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28312_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28312_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28316_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4622_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28312_MethodInfo,
	&InvokableCall_1_Find_m28316_MethodInfo,
};
extern Il2CppType UnityAction_1_t4624_0_0_0;
extern TypeInfo UnityAction_1_t4624_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisTransform_t10_m37323_MethodInfo;
extern TypeInfo Transform_t10_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28318_MethodInfo;
extern TypeInfo Transform_t10_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4622_RGCTXData[8] = 
{
	&UnityAction_1_t4624_0_0_0/* Type Usage */,
	&UnityAction_1_t4624_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTransform_t10_m37323_MethodInfo/* Method Usage */,
	&Transform_t10_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28318_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28313_MethodInfo/* Method Usage */,
	&Transform_t10_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28315_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4622_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4622_1_0_0;
struct CachedInvokableCall_1_t4622;
extern Il2CppGenericClass CachedInvokableCall_1_t4622_GenericClass;
TypeInfo CachedInvokableCall_1_t4622_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4622_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4622_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4623_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4622_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4622_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4622_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4622_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4622_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4622_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4622_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4622)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Transform>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_144.h"
extern TypeInfo UnityAction_1_t4624_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Transform>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_144MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Transform>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Transform>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisTransform_t10_m37323(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Transform>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Transform>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Transform>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Transform>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Transform>
extern Il2CppType UnityAction_1_t4624_0_0_1;
FieldInfo InvokableCall_1_t4623____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4624_0_0_1/* type */
	, &InvokableCall_1_t4623_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4623, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4623_FieldInfos[] =
{
	&InvokableCall_1_t4623____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4623_InvokableCall_1__ctor_m28313_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28313_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Transform>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28313_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t4623_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4623_InvokableCall_1__ctor_m28313_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28313_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4624_0_0_0;
static ParameterInfo InvokableCall_1_t4623_InvokableCall_1__ctor_m28314_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4624_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28314_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Transform>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28314_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t4623_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4623_InvokableCall_1__ctor_m28314_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28314_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t4623_InvokableCall_1_Invoke_m28315_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28315_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Transform>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28315_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t4623_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4623_InvokableCall_1_Invoke_m28315_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28315_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4623_InvokableCall_1_Find_m28316_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28316_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Transform>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28316_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t4623_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4623_InvokableCall_1_Find_m28316_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28316_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4623_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28313_MethodInfo,
	&InvokableCall_1__ctor_m28314_MethodInfo,
	&InvokableCall_1_Invoke_m28315_MethodInfo,
	&InvokableCall_1_Find_m28316_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4623_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m28315_MethodInfo,
	&InvokableCall_1_Find_m28316_MethodInfo,
};
extern TypeInfo UnityAction_1_t4624_il2cpp_TypeInfo;
extern TypeInfo Transform_t10_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4623_RGCTXData[5] = 
{
	&UnityAction_1_t4624_0_0_0/* Type Usage */,
	&UnityAction_1_t4624_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTransform_t10_m37323_MethodInfo/* Method Usage */,
	&Transform_t10_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28318_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4623_0_0_0;
extern Il2CppType InvokableCall_1_t4623_1_0_0;
struct InvokableCall_1_t4623;
extern Il2CppGenericClass InvokableCall_1_t4623_GenericClass;
TypeInfo InvokableCall_1_t4623_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4623_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4623_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4623_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4623_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4623_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4623_0_0_0/* byval_arg */
	, &InvokableCall_1_t4623_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4623_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4623_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4623)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Transform>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4624_UnityAction_1__ctor_m28317_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28317_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28317_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t4624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4624_UnityAction_1__ctor_m28317_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28317_GenericMethod/* genericMethod */

};
extern Il2CppType Transform_t10_0_0_0;
static ParameterInfo UnityAction_1_t4624_UnityAction_1_Invoke_m28318_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Transform_t10_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28318_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28318_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t4624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4624_UnityAction_1_Invoke_m28318_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28318_GenericMethod/* genericMethod */

};
extern Il2CppType Transform_t10_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4624_UnityAction_1_BeginInvoke_m28319_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Transform_t10_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28319_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28319_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t4624_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4624_UnityAction_1_BeginInvoke_m28319_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28319_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t4624_UnityAction_1_EndInvoke_m28320_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28320_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Transform>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28320_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t4624_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4624_UnityAction_1_EndInvoke_m28320_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28320_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4624_MethodInfos[] =
{
	&UnityAction_1__ctor_m28317_MethodInfo,
	&UnityAction_1_Invoke_m28318_MethodInfo,
	&UnityAction_1_BeginInvoke_m28319_MethodInfo,
	&UnityAction_1_EndInvoke_m28320_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28319_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28320_MethodInfo;
static MethodInfo* UnityAction_1_t4624_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m28318_MethodInfo,
	&UnityAction_1_BeginInvoke_m28319_MethodInfo,
	&UnityAction_1_EndInvoke_m28320_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4624_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4624_1_0_0;
struct UnityAction_1_t4624;
extern Il2CppGenericClass UnityAction_1_t4624_GenericClass;
TypeInfo UnityAction_1_t4624_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4624_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4624_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4624_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4624_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4624_0_0_0/* byval_arg */
	, &UnityAction_1_t4624_1_0_0/* this_arg */
	, UnityAction_1_t4624_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4624_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4624)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6570_il2cpp_TypeInfo;

// UnityEngine.Rigidbody
#include "UnityEngine_UnityEngine_Rigidbody.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Rigidbody>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Rigidbody>
extern MethodInfo IEnumerator_1_get_Current_m47353_MethodInfo;
static PropertyInfo IEnumerator_1_t6570____Current_PropertyInfo = 
{
	&IEnumerator_1_t6570_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m47353_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6570_PropertyInfos[] =
{
	&IEnumerator_1_t6570____Current_PropertyInfo,
	NULL
};
extern Il2CppType Rigidbody_t1006_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m47353_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Rigidbody>::get_Current()
MethodInfo IEnumerator_1_get_Current_m47353_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6570_il2cpp_TypeInfo/* declaring_type */
	, &Rigidbody_t1006_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m47353_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6570_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m47353_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6570_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6570_0_0_0;
extern Il2CppType IEnumerator_1_t6570_1_0_0;
struct IEnumerator_1_t6570;
extern Il2CppGenericClass IEnumerator_1_t6570_GenericClass;
TypeInfo IEnumerator_1_t6570_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6570_MethodInfos/* methods */
	, IEnumerator_1_t6570_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6570_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6570_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6570_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6570_0_0_0/* byval_arg */
	, &IEnumerator_1_t6570_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6570_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Rigidbody>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_417.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4625_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Rigidbody>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_417MethodDeclarations.h"

extern TypeInfo Rigidbody_t1006_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28325_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRigidbody_t1006_m37325_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Rigidbody>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Rigidbody>(System.Int32)
#define Array_InternalArray__get_Item_TisRigidbody_t1006_m37325(__this, p0, method) (Rigidbody_t1006 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rigidbody>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Rigidbody>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rigidbody>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Rigidbody>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Rigidbody>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Rigidbody>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4625____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4625_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4625, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4625____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4625_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4625, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4625_FieldInfos[] =
{
	&InternalEnumerator_1_t4625____array_0_FieldInfo,
	&InternalEnumerator_1_t4625____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28322_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4625____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4625_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28322_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4625____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4625_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28325_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4625_PropertyInfos[] =
{
	&InternalEnumerator_1_t4625____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4625____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4625_InternalEnumerator_1__ctor_m28321_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28321_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rigidbody>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28321_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4625_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4625_InternalEnumerator_1__ctor_m28321_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28321_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28322_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Rigidbody>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28322_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4625_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28322_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28323_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rigidbody>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28323_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4625_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28323_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28324_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Rigidbody>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28324_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4625_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28324_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody_t1006_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28325_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Rigidbody>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28325_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4625_il2cpp_TypeInfo/* declaring_type */
	, &Rigidbody_t1006_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28325_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4625_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28321_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28322_MethodInfo,
	&InternalEnumerator_1_Dispose_m28323_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28324_MethodInfo,
	&InternalEnumerator_1_get_Current_m28325_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m28324_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28323_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4625_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28322_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28324_MethodInfo,
	&InternalEnumerator_1_Dispose_m28323_MethodInfo,
	&InternalEnumerator_1_get_Current_m28325_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4625_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6570_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4625_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6570_il2cpp_TypeInfo, 7},
};
extern TypeInfo Rigidbody_t1006_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4625_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28325_MethodInfo/* Method Usage */,
	&Rigidbody_t1006_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisRigidbody_t1006_m37325_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4625_0_0_0;
extern Il2CppType InternalEnumerator_1_t4625_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4625_GenericClass;
TypeInfo InternalEnumerator_1_t4625_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4625_MethodInfos/* methods */
	, InternalEnumerator_1_t4625_PropertyInfos/* properties */
	, InternalEnumerator_1_t4625_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4625_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4625_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4625_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4625_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4625_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4625_1_0_0/* this_arg */
	, InternalEnumerator_1_t4625_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4625_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4625_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4625)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8308_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>
extern MethodInfo ICollection_1_get_Count_m47354_MethodInfo;
static PropertyInfo ICollection_1_t8308____Count_PropertyInfo = 
{
	&ICollection_1_t8308_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m47354_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m47355_MethodInfo;
static PropertyInfo ICollection_1_t8308____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8308_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m47355_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8308_PropertyInfos[] =
{
	&ICollection_1_t8308____Count_PropertyInfo,
	&ICollection_1_t8308____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m47354_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>::get_Count()
MethodInfo ICollection_1_get_Count_m47354_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8308_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m47354_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m47355_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m47355_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8308_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m47355_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody_t1006_0_0_0;
extern Il2CppType Rigidbody_t1006_0_0_0;
static ParameterInfo ICollection_1_t8308_ICollection_1_Add_m47356_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody_t1006_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m47356_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>::Add(T)
MethodInfo ICollection_1_Add_m47356_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8308_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8308_ICollection_1_Add_m47356_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m47356_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m47357_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>::Clear()
MethodInfo ICollection_1_Clear_m47357_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8308_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m47357_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody_t1006_0_0_0;
static ParameterInfo ICollection_1_t8308_ICollection_1_Contains_m47358_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody_t1006_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m47358_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>::Contains(T)
MethodInfo ICollection_1_Contains_m47358_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8308_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8308_ICollection_1_Contains_m47358_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m47358_GenericMethod/* genericMethod */

};
extern Il2CppType RigidbodyU5BU5D_t5445_0_0_0;
extern Il2CppType RigidbodyU5BU5D_t5445_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8308_ICollection_1_CopyTo_m47359_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &RigidbodyU5BU5D_t5445_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m47359_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m47359_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8308_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8308_ICollection_1_CopyTo_m47359_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m47359_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody_t1006_0_0_0;
static ParameterInfo ICollection_1_t8308_ICollection_1_Remove_m47360_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody_t1006_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m47360_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody>::Remove(T)
MethodInfo ICollection_1_Remove_m47360_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8308_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8308_ICollection_1_Remove_m47360_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m47360_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8308_MethodInfos[] =
{
	&ICollection_1_get_Count_m47354_MethodInfo,
	&ICollection_1_get_IsReadOnly_m47355_MethodInfo,
	&ICollection_1_Add_m47356_MethodInfo,
	&ICollection_1_Clear_m47357_MethodInfo,
	&ICollection_1_Contains_m47358_MethodInfo,
	&ICollection_1_CopyTo_m47359_MethodInfo,
	&ICollection_1_Remove_m47360_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8310_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8308_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8310_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8308_0_0_0;
extern Il2CppType ICollection_1_t8308_1_0_0;
struct ICollection_1_t8308;
extern Il2CppGenericClass ICollection_1_t8308_GenericClass;
TypeInfo ICollection_1_t8308_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8308_MethodInfos/* methods */
	, ICollection_1_t8308_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8308_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8308_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8308_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8308_0_0_0/* byval_arg */
	, &ICollection_1_t8308_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8308_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Rigidbody>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Rigidbody>
extern Il2CppType IEnumerator_1_t6570_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m47361_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Rigidbody>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m47361_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8310_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6570_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m47361_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8310_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m47361_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8310_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8310_0_0_0;
extern Il2CppType IEnumerable_1_t8310_1_0_0;
struct IEnumerable_1_t8310;
extern Il2CppGenericClass IEnumerable_1_t8310_GenericClass;
TypeInfo IEnumerable_1_t8310_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8310_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8310_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8310_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8310_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8310_0_0_0/* byval_arg */
	, &IEnumerable_1_t8310_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8310_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8309_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Rigidbody>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Rigidbody>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Rigidbody>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Rigidbody>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Rigidbody>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Rigidbody>
extern MethodInfo IList_1_get_Item_m47362_MethodInfo;
extern MethodInfo IList_1_set_Item_m47363_MethodInfo;
static PropertyInfo IList_1_t8309____Item_PropertyInfo = 
{
	&IList_1_t8309_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m47362_MethodInfo/* get */
	, &IList_1_set_Item_m47363_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8309_PropertyInfos[] =
{
	&IList_1_t8309____Item_PropertyInfo,
	NULL
};
extern Il2CppType Rigidbody_t1006_0_0_0;
static ParameterInfo IList_1_t8309_IList_1_IndexOf_m47364_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody_t1006_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m47364_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Rigidbody>::IndexOf(T)
MethodInfo IList_1_IndexOf_m47364_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8309_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8309_IList_1_IndexOf_m47364_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m47364_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Rigidbody_t1006_0_0_0;
static ParameterInfo IList_1_t8309_IList_1_Insert_m47365_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Rigidbody_t1006_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m47365_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Rigidbody>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m47365_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8309_IList_1_Insert_m47365_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m47365_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8309_IList_1_RemoveAt_m47366_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m47366_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Rigidbody>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m47366_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8309_IList_1_RemoveAt_m47366_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m47366_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8309_IList_1_get_Item_m47362_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Rigidbody_t1006_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m47362_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Rigidbody>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m47362_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8309_il2cpp_TypeInfo/* declaring_type */
	, &Rigidbody_t1006_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8309_IList_1_get_Item_m47362_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m47362_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Rigidbody_t1006_0_0_0;
static ParameterInfo IList_1_t8309_IList_1_set_Item_m47363_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Rigidbody_t1006_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m47363_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Rigidbody>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m47363_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8309_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8309_IList_1_set_Item_m47363_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m47363_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8309_MethodInfos[] =
{
	&IList_1_IndexOf_m47364_MethodInfo,
	&IList_1_Insert_m47365_MethodInfo,
	&IList_1_RemoveAt_m47366_MethodInfo,
	&IList_1_get_Item_m47362_MethodInfo,
	&IList_1_set_Item_m47363_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8309_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8308_il2cpp_TypeInfo,
	&IEnumerable_1_t8310_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8309_0_0_0;
extern Il2CppType IList_1_t8309_1_0_0;
struct IList_1_t8309;
extern Il2CppGenericClass IList_1_t8309_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8309_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8309_MethodInfos/* methods */
	, IList_1_t8309_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8309_il2cpp_TypeInfo/* element_class */
	, IList_1_t8309_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8309_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8309_0_0_0/* byval_arg */
	, &IList_1_t8309_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8309_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Rigidbody>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_139.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4626_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Rigidbody>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_139MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.Rigidbody>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_141.h"
extern TypeInfo InvokableCall_1_t4627_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Rigidbody>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_141MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28328_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28330_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Rigidbody>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Rigidbody>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Rigidbody>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t4626____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t4626_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4626, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4626_FieldInfos[] =
{
	&CachedInvokableCall_1_t4626____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType Rigidbody_t1006_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4626_CachedInvokableCall_1__ctor_m28326_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Rigidbody_t1006_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28326_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Rigidbody>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28326_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t4626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4626_CachedInvokableCall_1__ctor_m28326_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28326_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4626_CachedInvokableCall_1_Invoke_m28327_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28327_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Rigidbody>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28327_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t4626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4626_CachedInvokableCall_1_Invoke_m28327_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28327_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4626_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28326_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28327_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28327_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28331_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4626_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28327_MethodInfo,
	&InvokableCall_1_Find_m28331_MethodInfo,
};
extern Il2CppType UnityAction_1_t4628_0_0_0;
extern TypeInfo UnityAction_1_t4628_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisRigidbody_t1006_m37335_MethodInfo;
extern TypeInfo Rigidbody_t1006_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28333_MethodInfo;
extern TypeInfo Rigidbody_t1006_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4626_RGCTXData[8] = 
{
	&UnityAction_1_t4628_0_0_0/* Type Usage */,
	&UnityAction_1_t4628_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisRigidbody_t1006_m37335_MethodInfo/* Method Usage */,
	&Rigidbody_t1006_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28333_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28328_MethodInfo/* Method Usage */,
	&Rigidbody_t1006_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28330_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4626_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4626_1_0_0;
struct CachedInvokableCall_1_t4626;
extern Il2CppGenericClass CachedInvokableCall_1_t4626_GenericClass;
TypeInfo CachedInvokableCall_1_t4626_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4626_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4626_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4627_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4626_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4626_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4626_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4626_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4626_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4626_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4626_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4626)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Rigidbody>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_145.h"
extern TypeInfo UnityAction_1_t4628_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Rigidbody>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_145MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Rigidbody>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Rigidbody>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisRigidbody_t1006_m37335(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Rigidbody>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Rigidbody>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Rigidbody>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Rigidbody>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Rigidbody>
extern Il2CppType UnityAction_1_t4628_0_0_1;
FieldInfo InvokableCall_1_t4627____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4628_0_0_1/* type */
	, &InvokableCall_1_t4627_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4627, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4627_FieldInfos[] =
{
	&InvokableCall_1_t4627____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4627_InvokableCall_1__ctor_m28328_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28328_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Rigidbody>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28328_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t4627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4627_InvokableCall_1__ctor_m28328_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28328_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4628_0_0_0;
static ParameterInfo InvokableCall_1_t4627_InvokableCall_1__ctor_m28329_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4628_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28329_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Rigidbody>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28329_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t4627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4627_InvokableCall_1__ctor_m28329_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28329_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t4627_InvokableCall_1_Invoke_m28330_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28330_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Rigidbody>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28330_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t4627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4627_InvokableCall_1_Invoke_m28330_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28330_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4627_InvokableCall_1_Find_m28331_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28331_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Rigidbody>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28331_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t4627_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4627_InvokableCall_1_Find_m28331_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28331_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4627_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28328_MethodInfo,
	&InvokableCall_1__ctor_m28329_MethodInfo,
	&InvokableCall_1_Invoke_m28330_MethodInfo,
	&InvokableCall_1_Find_m28331_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4627_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m28330_MethodInfo,
	&InvokableCall_1_Find_m28331_MethodInfo,
};
extern TypeInfo UnityAction_1_t4628_il2cpp_TypeInfo;
extern TypeInfo Rigidbody_t1006_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4627_RGCTXData[5] = 
{
	&UnityAction_1_t4628_0_0_0/* Type Usage */,
	&UnityAction_1_t4628_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisRigidbody_t1006_m37335_MethodInfo/* Method Usage */,
	&Rigidbody_t1006_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28333_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4627_0_0_0;
extern Il2CppType InvokableCall_1_t4627_1_0_0;
struct InvokableCall_1_t4627;
extern Il2CppGenericClass InvokableCall_1_t4627_GenericClass;
TypeInfo InvokableCall_1_t4627_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4627_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4627_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4627_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4627_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4627_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4627_0_0_0/* byval_arg */
	, &InvokableCall_1_t4627_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4627_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4627_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4627)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Rigidbody>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Rigidbody>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Rigidbody>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Rigidbody>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Rigidbody>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4628_UnityAction_1__ctor_m28332_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28332_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Rigidbody>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28332_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t4628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4628_UnityAction_1__ctor_m28332_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28332_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody_t1006_0_0_0;
static ParameterInfo UnityAction_1_t4628_UnityAction_1_Invoke_m28333_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody_t1006_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28333_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Rigidbody>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28333_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t4628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4628_UnityAction_1_Invoke_m28333_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28333_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody_t1006_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4628_UnityAction_1_BeginInvoke_m28334_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody_t1006_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28334_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Rigidbody>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28334_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t4628_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4628_UnityAction_1_BeginInvoke_m28334_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28334_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t4628_UnityAction_1_EndInvoke_m28335_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28335_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Rigidbody>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28335_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t4628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4628_UnityAction_1_EndInvoke_m28335_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28335_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4628_MethodInfos[] =
{
	&UnityAction_1__ctor_m28332_MethodInfo,
	&UnityAction_1_Invoke_m28333_MethodInfo,
	&UnityAction_1_BeginInvoke_m28334_MethodInfo,
	&UnityAction_1_EndInvoke_m28335_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28334_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28335_MethodInfo;
static MethodInfo* UnityAction_1_t4628_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m28333_MethodInfo,
	&UnityAction_1_BeginInvoke_m28334_MethodInfo,
	&UnityAction_1_EndInvoke_m28335_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4628_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4628_1_0_0;
struct UnityAction_1_t4628;
extern Il2CppGenericClass UnityAction_1_t4628_GenericClass;
TypeInfo UnityAction_1_t4628_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4628_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4628_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4628_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4628_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4628_0_0_0/* byval_arg */
	, &UnityAction_1_t4628_1_0_0/* this_arg */
	, UnityAction_1_t4628_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4628_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4628)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Collider>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_140.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4629_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Collider>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_140MethodDeclarations.h"

// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.Collider>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_142.h"
extern TypeInfo Collider_t132_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t4630_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Collider>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_142MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28338_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28340_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Collider>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Collider>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Collider>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t4629____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t4629_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4629, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4629_FieldInfos[] =
{
	&CachedInvokableCall_1_t4629____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType Collider_t132_0_0_0;
extern Il2CppType Collider_t132_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4629_CachedInvokableCall_1__ctor_m28336_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Collider_t132_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28336_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Collider>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28336_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t4629_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4629_CachedInvokableCall_1__ctor_m28336_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28336_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4629_CachedInvokableCall_1_Invoke_m28337_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28337_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Collider>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28337_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t4629_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4629_CachedInvokableCall_1_Invoke_m28337_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28337_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4629_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28336_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28337_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28337_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28341_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4629_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28337_MethodInfo,
	&InvokableCall_1_Find_m28341_MethodInfo,
};
extern Il2CppType UnityAction_1_t4631_0_0_0;
extern TypeInfo UnityAction_1_t4631_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisCollider_t132_m37336_MethodInfo;
extern TypeInfo Collider_t132_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28343_MethodInfo;
extern TypeInfo Collider_t132_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4629_RGCTXData[8] = 
{
	&UnityAction_1_t4631_0_0_0/* Type Usage */,
	&UnityAction_1_t4631_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCollider_t132_m37336_MethodInfo/* Method Usage */,
	&Collider_t132_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28343_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28338_MethodInfo/* Method Usage */,
	&Collider_t132_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28340_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4629_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4629_1_0_0;
struct CachedInvokableCall_1_t4629;
extern Il2CppGenericClass CachedInvokableCall_1_t4629_GenericClass;
TypeInfo CachedInvokableCall_1_t4629_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4629_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4629_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4630_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4629_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4629_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4629_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4629_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4629_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4629_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4629_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4629)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.Collider>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_146.h"
extern TypeInfo UnityAction_1_t4631_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Collider>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_146MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Collider>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.Collider>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisCollider_t132_m37336(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Collider>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Collider>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Collider>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Collider>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.Collider>
extern Il2CppType UnityAction_1_t4631_0_0_1;
FieldInfo InvokableCall_1_t4630____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4631_0_0_1/* type */
	, &InvokableCall_1_t4630_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4630, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4630_FieldInfos[] =
{
	&InvokableCall_1_t4630____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4630_InvokableCall_1__ctor_m28338_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28338_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Collider>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28338_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t4630_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4630_InvokableCall_1__ctor_m28338_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28338_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4631_0_0_0;
static ParameterInfo InvokableCall_1_t4630_InvokableCall_1__ctor_m28339_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4631_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28339_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Collider>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28339_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t4630_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4630_InvokableCall_1__ctor_m28339_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28339_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t4630_InvokableCall_1_Invoke_m28340_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28340_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Collider>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28340_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t4630_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4630_InvokableCall_1_Invoke_m28340_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28340_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4630_InvokableCall_1_Find_m28341_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28341_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Collider>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28341_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t4630_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4630_InvokableCall_1_Find_m28341_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28341_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4630_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28338_MethodInfo,
	&InvokableCall_1__ctor_m28339_MethodInfo,
	&InvokableCall_1_Invoke_m28340_MethodInfo,
	&InvokableCall_1_Find_m28341_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4630_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m28340_MethodInfo,
	&InvokableCall_1_Find_m28341_MethodInfo,
};
extern TypeInfo UnityAction_1_t4631_il2cpp_TypeInfo;
extern TypeInfo Collider_t132_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4630_RGCTXData[5] = 
{
	&UnityAction_1_t4631_0_0_0/* Type Usage */,
	&UnityAction_1_t4631_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCollider_t132_m37336_MethodInfo/* Method Usage */,
	&Collider_t132_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28343_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4630_0_0_0;
extern Il2CppType InvokableCall_1_t4630_1_0_0;
struct InvokableCall_1_t4630;
extern Il2CppGenericClass InvokableCall_1_t4630_GenericClass;
TypeInfo InvokableCall_1_t4630_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4630_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4630_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4630_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4630_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4630_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4630_0_0_0/* byval_arg */
	, &InvokableCall_1_t4630_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4630_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4630_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4630)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Collider>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Collider>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Collider>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Collider>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Collider>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4631_UnityAction_1__ctor_m28342_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28342_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Collider>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28342_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t4631_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4631_UnityAction_1__ctor_m28342_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28342_GenericMethod/* genericMethod */

};
extern Il2CppType Collider_t132_0_0_0;
static ParameterInfo UnityAction_1_t4631_UnityAction_1_Invoke_m28343_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Collider_t132_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28343_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Collider>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28343_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t4631_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4631_UnityAction_1_Invoke_m28343_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28343_GenericMethod/* genericMethod */

};
extern Il2CppType Collider_t132_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4631_UnityAction_1_BeginInvoke_m28344_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Collider_t132_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28344_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Collider>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28344_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t4631_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4631_UnityAction_1_BeginInvoke_m28344_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28344_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t4631_UnityAction_1_EndInvoke_m28345_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28345_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Collider>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28345_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t4631_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4631_UnityAction_1_EndInvoke_m28345_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28345_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4631_MethodInfos[] =
{
	&UnityAction_1__ctor_m28342_MethodInfo,
	&UnityAction_1_Invoke_m28343_MethodInfo,
	&UnityAction_1_BeginInvoke_m28344_MethodInfo,
	&UnityAction_1_EndInvoke_m28345_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28344_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28345_MethodInfo;
static MethodInfo* UnityAction_1_t4631_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m28343_MethodInfo,
	&UnityAction_1_BeginInvoke_m28344_MethodInfo,
	&UnityAction_1_EndInvoke_m28345_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4631_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4631_1_0_0;
struct UnityAction_1_t4631;
extern Il2CppGenericClass UnityAction_1_t4631_GenericClass;
TypeInfo UnityAction_1_t4631_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4631_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4631_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4631_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4631_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4631_0_0_0/* byval_arg */
	, &UnityAction_1_t4631_1_0_0/* this_arg */
	, UnityAction_1_t4631_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4631_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4631)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6572_il2cpp_TypeInfo;

// UnityEngine.BoxCollider
#include "UnityEngine_UnityEngine_BoxCollider.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.BoxCollider>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.BoxCollider>
extern MethodInfo IEnumerator_1_get_Current_m47367_MethodInfo;
static PropertyInfo IEnumerator_1_t6572____Current_PropertyInfo = 
{
	&IEnumerator_1_t6572_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m47367_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6572_PropertyInfos[] =
{
	&IEnumerator_1_t6572____Current_PropertyInfo,
	NULL
};
extern Il2CppType BoxCollider_t723_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m47367_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.BoxCollider>::get_Current()
MethodInfo IEnumerator_1_get_Current_m47367_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6572_il2cpp_TypeInfo/* declaring_type */
	, &BoxCollider_t723_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m47367_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6572_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m47367_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6572_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6572_0_0_0;
extern Il2CppType IEnumerator_1_t6572_1_0_0;
struct IEnumerator_1_t6572;
extern Il2CppGenericClass IEnumerator_1_t6572_GenericClass;
TypeInfo IEnumerator_1_t6572_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6572_MethodInfos/* methods */
	, IEnumerator_1_t6572_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6572_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6572_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6572_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6572_0_0_0/* byval_arg */
	, &IEnumerator_1_t6572_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6572_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.BoxCollider>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_418.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4632_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.BoxCollider>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_418MethodDeclarations.h"

extern TypeInfo BoxCollider_t723_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28350_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisBoxCollider_t723_m37338_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.BoxCollider>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.BoxCollider>(System.Int32)
#define Array_InternalArray__get_Item_TisBoxCollider_t723_m37338(__this, p0, method) (BoxCollider_t723 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.BoxCollider>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.BoxCollider>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.BoxCollider>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.BoxCollider>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.BoxCollider>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.BoxCollider>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4632____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4632_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4632, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4632____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4632_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4632, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4632_FieldInfos[] =
{
	&InternalEnumerator_1_t4632____array_0_FieldInfo,
	&InternalEnumerator_1_t4632____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28347_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4632____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4632_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28347_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4632____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4632_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28350_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4632_PropertyInfos[] =
{
	&InternalEnumerator_1_t4632____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4632____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4632_InternalEnumerator_1__ctor_m28346_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28346_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.BoxCollider>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28346_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4632_InternalEnumerator_1__ctor_m28346_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28346_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28347_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.BoxCollider>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28347_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4632_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28347_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28348_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.BoxCollider>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28348_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28348_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28349_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.BoxCollider>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28349_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4632_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28349_GenericMethod/* genericMethod */

};
extern Il2CppType BoxCollider_t723_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28350_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.BoxCollider>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28350_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4632_il2cpp_TypeInfo/* declaring_type */
	, &BoxCollider_t723_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28350_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4632_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28346_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28347_MethodInfo,
	&InternalEnumerator_1_Dispose_m28348_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28349_MethodInfo,
	&InternalEnumerator_1_get_Current_m28350_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m28349_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28348_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4632_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28347_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28349_MethodInfo,
	&InternalEnumerator_1_Dispose_m28348_MethodInfo,
	&InternalEnumerator_1_get_Current_m28350_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4632_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6572_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4632_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6572_il2cpp_TypeInfo, 7},
};
extern TypeInfo BoxCollider_t723_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4632_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28350_MethodInfo/* Method Usage */,
	&BoxCollider_t723_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisBoxCollider_t723_m37338_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4632_0_0_0;
extern Il2CppType InternalEnumerator_1_t4632_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4632_GenericClass;
TypeInfo InternalEnumerator_1_t4632_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4632_MethodInfos/* methods */
	, InternalEnumerator_1_t4632_PropertyInfos/* properties */
	, InternalEnumerator_1_t4632_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4632_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4632_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4632_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4632_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4632_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4632_1_0_0/* this_arg */
	, InternalEnumerator_1_t4632_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4632_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4632_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4632)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8311_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>
extern MethodInfo ICollection_1_get_Count_m47368_MethodInfo;
static PropertyInfo ICollection_1_t8311____Count_PropertyInfo = 
{
	&ICollection_1_t8311_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m47368_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m47369_MethodInfo;
static PropertyInfo ICollection_1_t8311____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8311_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m47369_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8311_PropertyInfos[] =
{
	&ICollection_1_t8311____Count_PropertyInfo,
	&ICollection_1_t8311____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m47368_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>::get_Count()
MethodInfo ICollection_1_get_Count_m47368_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8311_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m47368_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m47369_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m47369_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8311_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m47369_GenericMethod/* genericMethod */

};
extern Il2CppType BoxCollider_t723_0_0_0;
extern Il2CppType BoxCollider_t723_0_0_0;
static ParameterInfo ICollection_1_t8311_ICollection_1_Add_m47370_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BoxCollider_t723_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m47370_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>::Add(T)
MethodInfo ICollection_1_Add_m47370_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8311_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8311_ICollection_1_Add_m47370_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m47370_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m47371_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>::Clear()
MethodInfo ICollection_1_Clear_m47371_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8311_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m47371_GenericMethod/* genericMethod */

};
extern Il2CppType BoxCollider_t723_0_0_0;
static ParameterInfo ICollection_1_t8311_ICollection_1_Contains_m47372_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BoxCollider_t723_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m47372_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>::Contains(T)
MethodInfo ICollection_1_Contains_m47372_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8311_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8311_ICollection_1_Contains_m47372_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m47372_GenericMethod/* genericMethod */

};
extern Il2CppType BoxColliderU5BU5D_t5446_0_0_0;
extern Il2CppType BoxColliderU5BU5D_t5446_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8311_ICollection_1_CopyTo_m47373_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BoxColliderU5BU5D_t5446_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m47373_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m47373_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8311_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8311_ICollection_1_CopyTo_m47373_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m47373_GenericMethod/* genericMethod */

};
extern Il2CppType BoxCollider_t723_0_0_0;
static ParameterInfo ICollection_1_t8311_ICollection_1_Remove_m47374_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BoxCollider_t723_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m47374_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.BoxCollider>::Remove(T)
MethodInfo ICollection_1_Remove_m47374_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8311_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8311_ICollection_1_Remove_m47374_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m47374_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8311_MethodInfos[] =
{
	&ICollection_1_get_Count_m47368_MethodInfo,
	&ICollection_1_get_IsReadOnly_m47369_MethodInfo,
	&ICollection_1_Add_m47370_MethodInfo,
	&ICollection_1_Clear_m47371_MethodInfo,
	&ICollection_1_Contains_m47372_MethodInfo,
	&ICollection_1_CopyTo_m47373_MethodInfo,
	&ICollection_1_Remove_m47374_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8313_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8311_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8313_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8311_0_0_0;
extern Il2CppType ICollection_1_t8311_1_0_0;
struct ICollection_1_t8311;
extern Il2CppGenericClass ICollection_1_t8311_GenericClass;
TypeInfo ICollection_1_t8311_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8311_MethodInfos/* methods */
	, ICollection_1_t8311_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8311_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8311_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8311_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8311_0_0_0/* byval_arg */
	, &ICollection_1_t8311_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8311_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.BoxCollider>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.BoxCollider>
extern Il2CppType IEnumerator_1_t6572_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m47375_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.BoxCollider>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m47375_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8313_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6572_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m47375_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8313_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m47375_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8313_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8313_0_0_0;
extern Il2CppType IEnumerable_1_t8313_1_0_0;
struct IEnumerable_1_t8313;
extern Il2CppGenericClass IEnumerable_1_t8313_GenericClass;
TypeInfo IEnumerable_1_t8313_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8313_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8313_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8313_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8313_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8313_0_0_0/* byval_arg */
	, &IEnumerable_1_t8313_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8313_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8312_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.BoxCollider>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.BoxCollider>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.BoxCollider>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.BoxCollider>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.BoxCollider>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.BoxCollider>
extern MethodInfo IList_1_get_Item_m47376_MethodInfo;
extern MethodInfo IList_1_set_Item_m47377_MethodInfo;
static PropertyInfo IList_1_t8312____Item_PropertyInfo = 
{
	&IList_1_t8312_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m47376_MethodInfo/* get */
	, &IList_1_set_Item_m47377_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8312_PropertyInfos[] =
{
	&IList_1_t8312____Item_PropertyInfo,
	NULL
};
extern Il2CppType BoxCollider_t723_0_0_0;
static ParameterInfo IList_1_t8312_IList_1_IndexOf_m47378_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BoxCollider_t723_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m47378_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.BoxCollider>::IndexOf(T)
MethodInfo IList_1_IndexOf_m47378_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8312_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8312_IList_1_IndexOf_m47378_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m47378_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType BoxCollider_t723_0_0_0;
static ParameterInfo IList_1_t8312_IList_1_Insert_m47379_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &BoxCollider_t723_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m47379_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.BoxCollider>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m47379_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8312_IList_1_Insert_m47379_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m47379_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8312_IList_1_RemoveAt_m47380_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m47380_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.BoxCollider>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m47380_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8312_IList_1_RemoveAt_m47380_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m47380_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8312_IList_1_get_Item_m47376_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType BoxCollider_t723_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m47376_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.BoxCollider>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m47376_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8312_il2cpp_TypeInfo/* declaring_type */
	, &BoxCollider_t723_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8312_IList_1_get_Item_m47376_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m47376_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType BoxCollider_t723_0_0_0;
static ParameterInfo IList_1_t8312_IList_1_set_Item_m47377_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &BoxCollider_t723_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m47377_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.BoxCollider>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m47377_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8312_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8312_IList_1_set_Item_m47377_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m47377_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8312_MethodInfos[] =
{
	&IList_1_IndexOf_m47378_MethodInfo,
	&IList_1_Insert_m47379_MethodInfo,
	&IList_1_RemoveAt_m47380_MethodInfo,
	&IList_1_get_Item_m47376_MethodInfo,
	&IList_1_set_Item_m47377_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8312_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8311_il2cpp_TypeInfo,
	&IEnumerable_1_t8313_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8312_0_0_0;
extern Il2CppType IList_1_t8312_1_0_0;
struct IList_1_t8312;
extern Il2CppGenericClass IList_1_t8312_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8312_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8312_MethodInfos/* methods */
	, IList_1_t8312_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8312_il2cpp_TypeInfo/* element_class */
	, IList_1_t8312_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8312_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8312_0_0_0/* byval_arg */
	, &IList_1_t8312_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8312_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_141.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4633_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_141MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_143.h"
extern TypeInfo InvokableCall_1_t4634_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_143MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28353_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28355_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t4633____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t4633_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4633, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4633_FieldInfos[] =
{
	&CachedInvokableCall_1_t4633____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType BoxCollider_t723_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4633_CachedInvokableCall_1__ctor_m28351_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &BoxCollider_t723_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28351_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28351_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t4633_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4633_CachedInvokableCall_1__ctor_m28351_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28351_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4633_CachedInvokableCall_1_Invoke_m28352_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28352_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28352_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t4633_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4633_CachedInvokableCall_1_Invoke_m28352_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28352_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4633_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28351_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28352_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28352_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28356_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4633_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28352_MethodInfo,
	&InvokableCall_1_Find_m28356_MethodInfo,
};
extern Il2CppType UnityAction_1_t4635_0_0_0;
extern TypeInfo UnityAction_1_t4635_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisBoxCollider_t723_m37348_MethodInfo;
extern TypeInfo BoxCollider_t723_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28358_MethodInfo;
extern TypeInfo BoxCollider_t723_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4633_RGCTXData[8] = 
{
	&UnityAction_1_t4635_0_0_0/* Type Usage */,
	&UnityAction_1_t4635_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBoxCollider_t723_m37348_MethodInfo/* Method Usage */,
	&BoxCollider_t723_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28358_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28353_MethodInfo/* Method Usage */,
	&BoxCollider_t723_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28355_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4633_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4633_1_0_0;
struct CachedInvokableCall_1_t4633;
extern Il2CppGenericClass CachedInvokableCall_1_t4633_GenericClass;
TypeInfo CachedInvokableCall_1_t4633_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4633_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4633_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4634_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4633_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4633_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4633_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4633_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4633_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4633_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4633_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4633)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_147.h"
extern TypeInfo UnityAction_1_t4635_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_147MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.BoxCollider>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.BoxCollider>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisBoxCollider_t723_m37348(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider>
extern Il2CppType UnityAction_1_t4635_0_0_1;
FieldInfo InvokableCall_1_t4634____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4635_0_0_1/* type */
	, &InvokableCall_1_t4634_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4634, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4634_FieldInfos[] =
{
	&InvokableCall_1_t4634____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4634_InvokableCall_1__ctor_m28353_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28353_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28353_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t4634_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4634_InvokableCall_1__ctor_m28353_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28353_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4635_0_0_0;
static ParameterInfo InvokableCall_1_t4634_InvokableCall_1__ctor_m28354_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4635_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28354_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28354_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t4634_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4634_InvokableCall_1__ctor_m28354_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28354_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t4634_InvokableCall_1_Invoke_m28355_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28355_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28355_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t4634_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4634_InvokableCall_1_Invoke_m28355_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28355_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4634_InvokableCall_1_Find_m28356_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28356_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28356_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t4634_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4634_InvokableCall_1_Find_m28356_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28356_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4634_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28353_MethodInfo,
	&InvokableCall_1__ctor_m28354_MethodInfo,
	&InvokableCall_1_Invoke_m28355_MethodInfo,
	&InvokableCall_1_Find_m28356_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4634_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m28355_MethodInfo,
	&InvokableCall_1_Find_m28356_MethodInfo,
};
extern TypeInfo UnityAction_1_t4635_il2cpp_TypeInfo;
extern TypeInfo BoxCollider_t723_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4634_RGCTXData[5] = 
{
	&UnityAction_1_t4635_0_0_0/* Type Usage */,
	&UnityAction_1_t4635_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBoxCollider_t723_m37348_MethodInfo/* Method Usage */,
	&BoxCollider_t723_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28358_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4634_0_0_0;
extern Il2CppType InvokableCall_1_t4634_1_0_0;
struct InvokableCall_1_t4634;
extern Il2CppGenericClass InvokableCall_1_t4634_GenericClass;
TypeInfo InvokableCall_1_t4634_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4634_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4634_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4634_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4634_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4634_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4634_0_0_0/* byval_arg */
	, &InvokableCall_1_t4634_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4634_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4634_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4634)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4635_UnityAction_1__ctor_m28357_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28357_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28357_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t4635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4635_UnityAction_1__ctor_m28357_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28357_GenericMethod/* genericMethod */

};
extern Il2CppType BoxCollider_t723_0_0_0;
static ParameterInfo UnityAction_1_t4635_UnityAction_1_Invoke_m28358_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &BoxCollider_t723_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28358_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28358_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t4635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4635_UnityAction_1_Invoke_m28358_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28358_GenericMethod/* genericMethod */

};
extern Il2CppType BoxCollider_t723_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4635_UnityAction_1_BeginInvoke_m28359_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &BoxCollider_t723_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28359_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28359_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t4635_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4635_UnityAction_1_BeginInvoke_m28359_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28359_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t4635_UnityAction_1_EndInvoke_m28360_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28360_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.BoxCollider>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28360_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t4635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4635_UnityAction_1_EndInvoke_m28360_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28360_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4635_MethodInfos[] =
{
	&UnityAction_1__ctor_m28357_MethodInfo,
	&UnityAction_1_Invoke_m28358_MethodInfo,
	&UnityAction_1_BeginInvoke_m28359_MethodInfo,
	&UnityAction_1_EndInvoke_m28360_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28359_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28360_MethodInfo;
static MethodInfo* UnityAction_1_t4635_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m28358_MethodInfo,
	&UnityAction_1_BeginInvoke_m28359_MethodInfo,
	&UnityAction_1_EndInvoke_m28360_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4635_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4635_1_0_0;
struct UnityAction_1_t4635;
extern Il2CppGenericClass UnityAction_1_t4635_GenericClass;
TypeInfo UnityAction_1_t4635_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4635_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4635_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4635_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4635_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4635_0_0_0/* byval_arg */
	, &UnityAction_1_t4635_1_0_0/* this_arg */
	, UnityAction_1_t4635_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4635_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4635)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6574_il2cpp_TypeInfo;

// UnityEngine.MeshCollider
#include "UnityEngine_UnityEngine_MeshCollider.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.MeshCollider>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.MeshCollider>
extern MethodInfo IEnumerator_1_get_Current_m47381_MethodInfo;
static PropertyInfo IEnumerator_1_t6574____Current_PropertyInfo = 
{
	&IEnumerator_1_t6574_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m47381_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6574_PropertyInfos[] =
{
	&IEnumerator_1_t6574____Current_PropertyInfo,
	NULL
};
extern Il2CppType MeshCollider_t582_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m47381_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.MeshCollider>::get_Current()
MethodInfo IEnumerator_1_get_Current_m47381_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6574_il2cpp_TypeInfo/* declaring_type */
	, &MeshCollider_t582_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m47381_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6574_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m47381_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6574_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6574_0_0_0;
extern Il2CppType IEnumerator_1_t6574_1_0_0;
struct IEnumerator_1_t6574;
extern Il2CppGenericClass IEnumerator_1_t6574_GenericClass;
TypeInfo IEnumerator_1_t6574_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6574_MethodInfos/* methods */
	, IEnumerator_1_t6574_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6574_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6574_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6574_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6574_0_0_0/* byval_arg */
	, &IEnumerator_1_t6574_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6574_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.MeshCollider>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_419.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4636_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.MeshCollider>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_419MethodDeclarations.h"

extern TypeInfo MeshCollider_t582_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m28365_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMeshCollider_t582_m37350_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.MeshCollider>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.MeshCollider>(System.Int32)
#define Array_InternalArray__get_Item_TisMeshCollider_t582_m37350(__this, p0, method) (MeshCollider_t582 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshCollider>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.MeshCollider>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshCollider>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.MeshCollider>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.MeshCollider>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.MeshCollider>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4636____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4636_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4636, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4636____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4636_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4636, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4636_FieldInfos[] =
{
	&InternalEnumerator_1_t4636____array_0_FieldInfo,
	&InternalEnumerator_1_t4636____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28362_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4636____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4636_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28362_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4636____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4636_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28365_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4636_PropertyInfos[] =
{
	&InternalEnumerator_1_t4636____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4636____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4636_InternalEnumerator_1__ctor_m28361_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28361_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshCollider>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28361_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4636_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4636_InternalEnumerator_1__ctor_m28361_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28361_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28362_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.MeshCollider>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28362_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4636_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28362_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28363_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MeshCollider>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28363_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4636_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28363_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28364_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.MeshCollider>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28364_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4636_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28364_GenericMethod/* genericMethod */

};
extern Il2CppType MeshCollider_t582_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28365_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.MeshCollider>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28365_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4636_il2cpp_TypeInfo/* declaring_type */
	, &MeshCollider_t582_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28365_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4636_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28361_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28362_MethodInfo,
	&InternalEnumerator_1_Dispose_m28363_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28364_MethodInfo,
	&InternalEnumerator_1_get_Current_m28365_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m28364_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28363_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4636_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28362_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28364_MethodInfo,
	&InternalEnumerator_1_Dispose_m28363_MethodInfo,
	&InternalEnumerator_1_get_Current_m28365_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4636_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6574_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4636_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6574_il2cpp_TypeInfo, 7},
};
extern TypeInfo MeshCollider_t582_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4636_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28365_MethodInfo/* Method Usage */,
	&MeshCollider_t582_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMeshCollider_t582_m37350_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4636_0_0_0;
extern Il2CppType InternalEnumerator_1_t4636_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4636_GenericClass;
TypeInfo InternalEnumerator_1_t4636_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4636_MethodInfos/* methods */
	, InternalEnumerator_1_t4636_PropertyInfos/* properties */
	, InternalEnumerator_1_t4636_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4636_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4636_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4636_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4636_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4636_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4636_1_0_0/* this_arg */
	, InternalEnumerator_1_t4636_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4636_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4636_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4636)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8314_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>
extern MethodInfo ICollection_1_get_Count_m47382_MethodInfo;
static PropertyInfo ICollection_1_t8314____Count_PropertyInfo = 
{
	&ICollection_1_t8314_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m47382_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m47383_MethodInfo;
static PropertyInfo ICollection_1_t8314____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8314_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m47383_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8314_PropertyInfos[] =
{
	&ICollection_1_t8314____Count_PropertyInfo,
	&ICollection_1_t8314____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m47382_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>::get_Count()
MethodInfo ICollection_1_get_Count_m47382_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8314_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m47382_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m47383_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m47383_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8314_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m47383_GenericMethod/* genericMethod */

};
extern Il2CppType MeshCollider_t582_0_0_0;
extern Il2CppType MeshCollider_t582_0_0_0;
static ParameterInfo ICollection_1_t8314_ICollection_1_Add_m47384_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MeshCollider_t582_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m47384_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>::Add(T)
MethodInfo ICollection_1_Add_m47384_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8314_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8314_ICollection_1_Add_m47384_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m47384_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m47385_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>::Clear()
MethodInfo ICollection_1_Clear_m47385_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8314_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m47385_GenericMethod/* genericMethod */

};
extern Il2CppType MeshCollider_t582_0_0_0;
static ParameterInfo ICollection_1_t8314_ICollection_1_Contains_m47386_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MeshCollider_t582_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m47386_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>::Contains(T)
MethodInfo ICollection_1_Contains_m47386_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8314_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8314_ICollection_1_Contains_m47386_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m47386_GenericMethod/* genericMethod */

};
extern Il2CppType MeshColliderU5BU5D_t5447_0_0_0;
extern Il2CppType MeshColliderU5BU5D_t5447_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8314_ICollection_1_CopyTo_m47387_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MeshColliderU5BU5D_t5447_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m47387_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m47387_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8314_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8314_ICollection_1_CopyTo_m47387_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m47387_GenericMethod/* genericMethod */

};
extern Il2CppType MeshCollider_t582_0_0_0;
static ParameterInfo ICollection_1_t8314_ICollection_1_Remove_m47388_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MeshCollider_t582_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m47388_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MeshCollider>::Remove(T)
MethodInfo ICollection_1_Remove_m47388_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8314_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8314_ICollection_1_Remove_m47388_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m47388_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8314_MethodInfos[] =
{
	&ICollection_1_get_Count_m47382_MethodInfo,
	&ICollection_1_get_IsReadOnly_m47383_MethodInfo,
	&ICollection_1_Add_m47384_MethodInfo,
	&ICollection_1_Clear_m47385_MethodInfo,
	&ICollection_1_Contains_m47386_MethodInfo,
	&ICollection_1_CopyTo_m47387_MethodInfo,
	&ICollection_1_Remove_m47388_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8316_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8314_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8316_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8314_0_0_0;
extern Il2CppType ICollection_1_t8314_1_0_0;
struct ICollection_1_t8314;
extern Il2CppGenericClass ICollection_1_t8314_GenericClass;
TypeInfo ICollection_1_t8314_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8314_MethodInfos/* methods */
	, ICollection_1_t8314_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8314_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8314_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8314_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8314_0_0_0/* byval_arg */
	, &ICollection_1_t8314_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8314_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.MeshCollider>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.MeshCollider>
extern Il2CppType IEnumerator_1_t6574_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m47389_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.MeshCollider>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m47389_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8316_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6574_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m47389_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8316_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m47389_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8316_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8316_0_0_0;
extern Il2CppType IEnumerable_1_t8316_1_0_0;
struct IEnumerable_1_t8316;
extern Il2CppGenericClass IEnumerable_1_t8316_GenericClass;
TypeInfo IEnumerable_1_t8316_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8316_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8316_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8316_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8316_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8316_0_0_0/* byval_arg */
	, &IEnumerable_1_t8316_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8316_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8315_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.MeshCollider>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.MeshCollider>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.MeshCollider>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.MeshCollider>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.MeshCollider>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.MeshCollider>
extern MethodInfo IList_1_get_Item_m47390_MethodInfo;
extern MethodInfo IList_1_set_Item_m47391_MethodInfo;
static PropertyInfo IList_1_t8315____Item_PropertyInfo = 
{
	&IList_1_t8315_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m47390_MethodInfo/* get */
	, &IList_1_set_Item_m47391_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8315_PropertyInfos[] =
{
	&IList_1_t8315____Item_PropertyInfo,
	NULL
};
extern Il2CppType MeshCollider_t582_0_0_0;
static ParameterInfo IList_1_t8315_IList_1_IndexOf_m47392_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MeshCollider_t582_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m47392_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.MeshCollider>::IndexOf(T)
MethodInfo IList_1_IndexOf_m47392_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8315_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8315_IList_1_IndexOf_m47392_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m47392_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType MeshCollider_t582_0_0_0;
static ParameterInfo IList_1_t8315_IList_1_Insert_m47393_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MeshCollider_t582_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m47393_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.MeshCollider>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m47393_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8315_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8315_IList_1_Insert_m47393_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m47393_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8315_IList_1_RemoveAt_m47394_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m47394_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.MeshCollider>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m47394_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8315_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8315_IList_1_RemoveAt_m47394_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m47394_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8315_IList_1_get_Item_m47390_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType MeshCollider_t582_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m47390_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.MeshCollider>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m47390_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8315_il2cpp_TypeInfo/* declaring_type */
	, &MeshCollider_t582_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8315_IList_1_get_Item_m47390_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m47390_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType MeshCollider_t582_0_0_0;
static ParameterInfo IList_1_t8315_IList_1_set_Item_m47391_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MeshCollider_t582_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m47391_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.MeshCollider>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m47391_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8315_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8315_IList_1_set_Item_m47391_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m47391_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8315_MethodInfos[] =
{
	&IList_1_IndexOf_m47392_MethodInfo,
	&IList_1_Insert_m47393_MethodInfo,
	&IList_1_RemoveAt_m47394_MethodInfo,
	&IList_1_get_Item_m47390_MethodInfo,
	&IList_1_set_Item_m47391_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8315_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8314_il2cpp_TypeInfo,
	&IEnumerable_1_t8316_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8315_0_0_0;
extern Il2CppType IList_1_t8315_1_0_0;
struct IList_1_t8315;
extern Il2CppGenericClass IList_1_t8315_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8315_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8315_MethodInfos/* methods */
	, IList_1_t8315_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8315_il2cpp_TypeInfo/* element_class */
	, IList_1_t8315_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8315_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8315_0_0_0/* byval_arg */
	, &IList_1_t8315_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8315_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MeshCollider>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_142.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4637_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MeshCollider>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_142MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.MeshCollider>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_144.h"
extern TypeInfo InvokableCall_1_t4638_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.MeshCollider>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_144MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m28368_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m28370_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MeshCollider>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MeshCollider>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MeshCollider>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t4637____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t4637_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4637, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4637_FieldInfos[] =
{
	&CachedInvokableCall_1_t4637____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType MeshCollider_t582_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4637_CachedInvokableCall_1__ctor_m28366_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &MeshCollider_t582_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m28366_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MeshCollider>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m28366_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t4637_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4637_CachedInvokableCall_1__ctor_m28366_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m28366_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4637_CachedInvokableCall_1_Invoke_m28367_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m28367_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MeshCollider>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m28367_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t4637_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4637_CachedInvokableCall_1_Invoke_m28367_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m28367_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4637_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m28366_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28367_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m28367_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m28371_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4637_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m28367_MethodInfo,
	&InvokableCall_1_Find_m28371_MethodInfo,
};
extern Il2CppType UnityAction_1_t4639_0_0_0;
extern TypeInfo UnityAction_1_t4639_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisMeshCollider_t582_m37360_MethodInfo;
extern TypeInfo MeshCollider_t582_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m28373_MethodInfo;
extern TypeInfo MeshCollider_t582_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4637_RGCTXData[8] = 
{
	&UnityAction_1_t4639_0_0_0/* Type Usage */,
	&UnityAction_1_t4639_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMeshCollider_t582_m37360_MethodInfo/* Method Usage */,
	&MeshCollider_t582_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28373_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m28368_MethodInfo/* Method Usage */,
	&MeshCollider_t582_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m28370_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4637_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4637_1_0_0;
struct CachedInvokableCall_1_t4637;
extern Il2CppGenericClass CachedInvokableCall_1_t4637_GenericClass;
TypeInfo CachedInvokableCall_1_t4637_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4637_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4637_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4638_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4637_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4637_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4637_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4637_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4637_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4637_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4637_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4637)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.MeshCollider>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_148.h"
extern TypeInfo UnityAction_1_t4639_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.MeshCollider>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_148MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.MeshCollider>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.MeshCollider>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisMeshCollider_t582_m37360(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MeshCollider>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MeshCollider>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MeshCollider>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.MeshCollider>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.MeshCollider>
extern Il2CppType UnityAction_1_t4639_0_0_1;
FieldInfo InvokableCall_1_t4638____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4639_0_0_1/* type */
	, &InvokableCall_1_t4638_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4638, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4638_FieldInfos[] =
{
	&InvokableCall_1_t4638____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4638_InvokableCall_1__ctor_m28368_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28368_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MeshCollider>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m28368_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t4638_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4638_InvokableCall_1__ctor_m28368_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28368_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4639_0_0_0;
static ParameterInfo InvokableCall_1_t4638_InvokableCall_1__ctor_m28369_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4639_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m28369_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MeshCollider>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m28369_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t4638_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4638_InvokableCall_1__ctor_m28369_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m28369_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t4638_InvokableCall_1_Invoke_m28370_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m28370_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.MeshCollider>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m28370_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t4638_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4638_InvokableCall_1_Invoke_m28370_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m28370_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4638_InvokableCall_1_Find_m28371_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m28371_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.MeshCollider>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m28371_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t4638_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4638_InvokableCall_1_Find_m28371_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m28371_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4638_MethodInfos[] =
{
	&InvokableCall_1__ctor_m28368_MethodInfo,
	&InvokableCall_1__ctor_m28369_MethodInfo,
	&InvokableCall_1_Invoke_m28370_MethodInfo,
	&InvokableCall_1_Find_m28371_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4638_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m28370_MethodInfo,
	&InvokableCall_1_Find_m28371_MethodInfo,
};
extern TypeInfo UnityAction_1_t4639_il2cpp_TypeInfo;
extern TypeInfo MeshCollider_t582_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4638_RGCTXData[5] = 
{
	&UnityAction_1_t4639_0_0_0/* Type Usage */,
	&UnityAction_1_t4639_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMeshCollider_t582_m37360_MethodInfo/* Method Usage */,
	&MeshCollider_t582_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m28373_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4638_0_0_0;
extern Il2CppType InvokableCall_1_t4638_1_0_0;
struct InvokableCall_1_t4638;
extern Il2CppGenericClass InvokableCall_1_t4638_GenericClass;
TypeInfo InvokableCall_1_t4638_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4638_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4638_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4638_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4638_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4638_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4638_0_0_0/* byval_arg */
	, &InvokableCall_1_t4638_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4638_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4638_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4638)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MeshCollider>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MeshCollider>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.MeshCollider>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MeshCollider>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.MeshCollider>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4639_UnityAction_1__ctor_m28372_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m28372_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MeshCollider>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m28372_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t4639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4639_UnityAction_1__ctor_m28372_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m28372_GenericMethod/* genericMethod */

};
extern Il2CppType MeshCollider_t582_0_0_0;
static ParameterInfo UnityAction_1_t4639_UnityAction_1_Invoke_m28373_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &MeshCollider_t582_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m28373_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MeshCollider>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m28373_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t4639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4639_UnityAction_1_Invoke_m28373_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m28373_GenericMethod/* genericMethod */

};
extern Il2CppType MeshCollider_t582_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4639_UnityAction_1_BeginInvoke_m28374_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &MeshCollider_t582_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m28374_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.MeshCollider>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m28374_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t4639_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4639_UnityAction_1_BeginInvoke_m28374_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m28374_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t4639_UnityAction_1_EndInvoke_m28375_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m28375_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.MeshCollider>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m28375_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t4639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4639_UnityAction_1_EndInvoke_m28375_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m28375_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4639_MethodInfos[] =
{
	&UnityAction_1__ctor_m28372_MethodInfo,
	&UnityAction_1_Invoke_m28373_MethodInfo,
	&UnityAction_1_BeginInvoke_m28374_MethodInfo,
	&UnityAction_1_EndInvoke_m28375_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m28374_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m28375_MethodInfo;
static MethodInfo* UnityAction_1_t4639_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m28373_MethodInfo,
	&UnityAction_1_BeginInvoke_m28374_MethodInfo,
	&UnityAction_1_EndInvoke_m28375_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t4639_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4639_1_0_0;
struct UnityAction_1_t4639;
extern Il2CppGenericClass UnityAction_1_t4639_GenericClass;
TypeInfo UnityAction_1_t4639_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4639_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4639_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4639_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4639_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4639_0_0_0/* byval_arg */
	, &UnityAction_1_t4639_1_0_0/* this_arg */
	, UnityAction_1_t4639_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4639_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4639)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_Generic_List_1_gen_49.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo List_1_t1007_il2cpp_TypeInfo;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_Generic_List_1_gen_49MethodDeclarations.h"

// UnityEngine.Rigidbody2D
#include "UnityEngine_UnityEngine_Rigidbody2D.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_51.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_51.h"
// System.Predicate`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Predicate_1_gen_52.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_50.h"
// System.Comparison`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Comparison_1_gen_51.h"
extern TypeInfo Rigidbody2D_t1008_il2cpp_TypeInfo;
extern TypeInfo Int32_t93_il2cpp_TypeInfo;
extern TypeInfo NullReferenceException_t791_il2cpp_TypeInfo;
extern TypeInfo InvalidCastException_t2221_il2cpp_TypeInfo;
extern TypeInfo ArgumentOutOfRangeException_t1494_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t4643_il2cpp_TypeInfo;
extern TypeInfo Rigidbody2DU5BU5D_t4640_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t4647_il2cpp_TypeInfo;
extern TypeInfo Boolean_t106_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t4641_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t4642_il2cpp_TypeInfo;
extern TypeInfo ReadOnlyCollection_1_t4644_il2cpp_TypeInfo;
extern TypeInfo ArgumentNullException_t1171_il2cpp_TypeInfo;
extern TypeInfo Predicate_1_t4645_il2cpp_TypeInfo;
extern TypeInfo Comparer_1_t4653_il2cpp_TypeInfo;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_51MethodDeclarations.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
// System.Predicate`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Predicate_1_gen_52MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_51MethodDeclarations.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_Generic_Comparer_1_gen_50MethodDeclarations.h"
extern MethodInfo List_1_get_Item_m28423_MethodInfo;
extern MethodInfo List_1_set_Item_m28424_MethodInfo;
extern MethodInfo ArgumentOutOfRangeException__ctor_m7878_MethodInfo;
extern MethodInfo Array_Resize_TisRigidbody2D_t1008_m37373_MethodInfo;
extern MethodInfo ArgumentOutOfRangeException__ctor_m7660_MethodInfo;
extern MethodInfo List_1_CheckIndex_m28409_MethodInfo;
extern MethodInfo Object__ctor_m271_MethodInfo;
extern MethodInfo List_1_CheckCollection_m28411_MethodInfo;
extern MethodInfo List_1_AddEnumerable_m28397_MethodInfo;
extern MethodInfo ICollection_1_get_Count_m47395_MethodInfo;
extern MethodInfo List_1_AddCollection_m28396_MethodInfo;
extern MethodInfo List_1_GetEnumerator_m28406_MethodInfo;
extern MethodInfo Array_Copy_m9624_MethodInfo;
extern MethodInfo List_1_Add_m28394_MethodInfo;
extern MethodInfo List_1_Contains_m28401_MethodInfo;
extern MethodInfo List_1_IndexOf_m28407_MethodInfo;
extern MethodInfo List_1_Insert_m28410_MethodInfo;
extern MethodInfo List_1_Remove_m28412_MethodInfo;
extern MethodInfo List_1_GrowIfNeeded_m28395_MethodInfo;
extern MethodInfo List_1_get_Capacity_m28420_MethodInfo;
extern MethodInfo Math_Max_m8818_MethodInfo;
extern MethodInfo List_1_set_Capacity_m28421_MethodInfo;
extern MethodInfo ICollection_1_CopyTo_m47396_MethodInfo;
extern MethodInfo IEnumerable_1_GetEnumerator_m47397_MethodInfo;
extern MethodInfo IEnumerator_1_get_Current_m47398_MethodInfo;
extern MethodInfo IEnumerator_MoveNext_m4433_MethodInfo;
extern MethodInfo IDisposable_Dispose_m459_MethodInfo;
extern MethodInfo ReadOnlyCollection_1__ctor_m28436_MethodInfo;
extern MethodInfo Array_Clear_m8791_MethodInfo;
extern MethodInfo Array_IndexOf_TisRigidbody2D_t1008_m37375_MethodInfo;
extern MethodInfo List_1_CheckMatch_m28404_MethodInfo;
extern MethodInfo List_1_GetIndex_m28405_MethodInfo;
extern MethodInfo ArgumentNullException__ctor_m6533_MethodInfo;
extern MethodInfo Predicate_1_Invoke_m28511_MethodInfo;
extern MethodInfo Enumerator__ctor_m28430_MethodInfo;
extern MethodInfo List_1_Shift_m28408_MethodInfo;
extern MethodInfo List_1_RemoveAt_m28414_MethodInfo;
extern MethodInfo Array_Reverse_m8849_MethodInfo;
extern MethodInfo Comparer_1_get_Default_m28517_MethodInfo;
extern MethodInfo Array_Sort_TisRigidbody2D_t1008_m37377_MethodInfo;
extern MethodInfo Array_Sort_TisRigidbody2D_t1008_m37385_MethodInfo;
extern MethodInfo Array_Copy_m9623_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
 void Array_Resize_TisObject_t_m32177_gshared (Object_t * __this/* static, unused */, ObjectU5BU5D_t115** p0, int32_t p1, MethodInfo* method);
#define Array_Resize_TisObject_t_m32177(__this/* static, unused */, p0, p1, method) (void)Array_Resize_TisObject_t_m32177_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t115**)p0, (int32_t)p1, method)
// Declaration System.Void System.Array::Resize<UnityEngine.Rigidbody2D>(!!0[]&,System.Int32)
// System.Void System.Array::Resize<UnityEngine.Rigidbody2D>(!!0[]&,System.Int32)
#define Array_Resize_TisRigidbody2D_t1008_m37373(__this/* static, unused */, p0, p1, method) (void)Array_Resize_TisObject_t_m32177_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t115**)p0, (int32_t)p1, method)
struct Array_t;
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen_65.h"
struct Array_t;
// System.Collections.Generic.EqualityComparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_gen.h"
// Declaration System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0,System.Int32,System.Int32)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0,System.Int32,System.Int32)
 int32_t Array_IndexOf_TisObject_t_m13809_gshared (Object_t * __this/* static, unused */, ObjectU5BU5D_t115* p0, Object_t * p1, int32_t p2, int32_t p3, MethodInfo* method);
#define Array_IndexOf_TisObject_t_m13809(__this/* static, unused */, p0, p1, p2, p3, method) (int32_t)Array_IndexOf_TisObject_t_m13809_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t115*)p0, (Object_t *)p1, (int32_t)p2, (int32_t)p3, method)
// Declaration System.Int32 System.Array::IndexOf<UnityEngine.Rigidbody2D>(!!0[],!!0,System.Int32,System.Int32)
// System.Int32 System.Array::IndexOf<UnityEngine.Rigidbody2D>(!!0[],!!0,System.Int32,System.Int32)
#define Array_IndexOf_TisRigidbody2D_t1008_m37375(__this/* static, unused */, p0, p1, p2, p3, method) (int32_t)Array_IndexOf_TisObject_t_m13809_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t115*)p0, (Object_t *)p1, (int32_t)p2, (int32_t)p3, method)
struct Array_t;
struct Array_t;
// Declaration System.Void System.Array::Sort<System.Object>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
// System.Void System.Array::Sort<System.Object>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
 void Array_Sort_TisObject_t_m32246_gshared (Object_t * __this/* static, unused */, ObjectU5BU5D_t115* p0, int32_t p1, int32_t p2, Object_t* p3, MethodInfo* method);
#define Array_Sort_TisObject_t_m32246(__this/* static, unused */, p0, p1, p2, p3, method) (void)Array_Sort_TisObject_t_m32246_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t115*)p0, (int32_t)p1, (int32_t)p2, (Object_t*)p3, method)
// Declaration System.Void System.Array::Sort<UnityEngine.Rigidbody2D>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
// System.Void System.Array::Sort<UnityEngine.Rigidbody2D>(!!0[],System.Int32,System.Int32,System.Collections.Generic.IComparer`1<!!0>)
#define Array_Sort_TisRigidbody2D_t1008_m37377(__this/* static, unused */, p0, p1, p2, p3, method) (void)Array_Sort_TisObject_t_m32246_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t115*)p0, (int32_t)p1, (int32_t)p2, (Object_t*)p3, method)
struct Array_t;
// System.Exception
#include "mscorlib_System_Exception.h"
struct Array_t;
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3.h"
// Declaration System.Void System.Array::Sort<System.Object>(!!0[],System.Int32,System.Comparison`1<!!0>)
// System.Void System.Array::Sort<System.Object>(!!0[],System.Int32,System.Comparison`1<!!0>)
 void Array_Sort_TisObject_t_m32287_gshared (Object_t * __this/* static, unused */, ObjectU5BU5D_t115* p0, int32_t p1, Comparison_1_t2846 * p2, MethodInfo* method);
#define Array_Sort_TisObject_t_m32287(__this/* static, unused */, p0, p1, p2, method) (void)Array_Sort_TisObject_t_m32287_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t115*)p0, (int32_t)p1, (Comparison_1_t2846 *)p2, method)
// Declaration System.Void System.Array::Sort<UnityEngine.Rigidbody2D>(!!0[],System.Int32,System.Comparison`1<!!0>)
// System.Void System.Array::Sort<UnityEngine.Rigidbody2D>(!!0[],System.Int32,System.Comparison`1<!!0>)
#define Array_Sort_TisRigidbody2D_t1008_m37385(__this/* static, unused */, p0, p1, p2, method) (void)Array_Sort_TisObject_t_m32287_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t115*)p0, (int32_t)p1, (Comparison_1_t2846 *)p2, method)


// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor()
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor(System.Int32)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.cctor()
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IEnumerable.GetEnumerator()
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Add(System.Object)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Contains(System.Object)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.IndexOf(System.Object)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Insert(System.Int32,System.Object)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Remove(System.Object)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.ICollection.get_IsSynchronized()
// System.Object System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.ICollection.get_SyncRoot()
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.get_IsFixedSize()
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.get_IsReadOnly()
// System.Object System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.get_Item(System.Int32)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.set_Item(System.Int32,System.Object)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Add(T)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::GrowIfNeeded(System.Int32)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AddCollection(System.Collections.Generic.ICollection`1<T>)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AsReadOnly()
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Clear()
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Contains(T)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CopyTo(T[],System.Int32)
// T System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Find(System.Predicate`1<T>)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CheckMatch(System.Predicate`1<T>)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::GetEnumerator()
 Enumerator_t4647  List_1_GetEnumerator_m28406 (List_1_t1007 * __this, MethodInfo* method){
	{
		Enumerator_t4647  L_0 = {0};
		Enumerator__ctor_m28430(&L_0, __this, /*hidden argument*/&Enumerator__ctor_m28430_MethodInfo);
		return L_0;
	}
}
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::IndexOf(T)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Shift(System.Int32,System.Int32)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CheckIndex(System.Int32)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Remove(T)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::RemoveAll(System.Predicate`1<T>)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::RemoveAt(System.Int32)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Reverse()
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Sort()
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Sort(System.Comparison`1<T>)
// T[] System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::ToArray()
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::TrimExcess()
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::get_Capacity()
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::set_Capacity(System.Int32)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::get_Count()
// T System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::get_Item(System.Int32)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
extern Il2CppType Int32_t93_0_0_32849;
FieldInfo List_1_t1007____DefaultCapacity_0_FieldInfo = 
{
	"DefaultCapacity"/* name */
	, &Int32_t93_0_0_32849/* type */
	, &List_1_t1007_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Rigidbody2DU5BU5D_t4640_0_0_1;
FieldInfo List_1_t1007_____items_1_FieldInfo = 
{
	"_items"/* name */
	, &Rigidbody2DU5BU5D_t4640_0_0_1/* type */
	, &List_1_t1007_il2cpp_TypeInfo/* parent */
	, offsetof(List_1_t1007, ____items_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo List_1_t1007_____size_2_FieldInfo = 
{
	"_size"/* name */
	, &Int32_t93_0_0_1/* type */
	, &List_1_t1007_il2cpp_TypeInfo/* parent */
	, offsetof(List_1_t1007, ____size_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo List_1_t1007_____version_3_FieldInfo = 
{
	"_version"/* name */
	, &Int32_t93_0_0_1/* type */
	, &List_1_t1007_il2cpp_TypeInfo/* parent */
	, offsetof(List_1_t1007, ____version_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Rigidbody2DU5BU5D_t4640_0_0_49;
FieldInfo List_1_t1007____EmptyArray_4_FieldInfo = 
{
	"EmptyArray"/* name */
	, &Rigidbody2DU5BU5D_t4640_0_0_49/* type */
	, &List_1_t1007_il2cpp_TypeInfo/* parent */
	, offsetof(List_1_t1007_StaticFields, ___EmptyArray_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* List_1_t1007_FieldInfos[] =
{
	&List_1_t1007____DefaultCapacity_0_FieldInfo,
	&List_1_t1007_____items_1_FieldInfo,
	&List_1_t1007_____size_2_FieldInfo,
	&List_1_t1007_____version_3_FieldInfo,
	&List_1_t1007____EmptyArray_4_FieldInfo,
	NULL
};
static const int32_t List_1_t1007____DefaultCapacity_0_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry List_1_t1007____DefaultCapacity_0_DefaultValue = 
{
	&List_1_t1007____DefaultCapacity_0_FieldInfo/* field */
	, { (char*)&List_1_t1007____DefaultCapacity_0_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* List_1_t1007_FieldDefaultValues[] = 
{
	&List_1_t1007____DefaultCapacity_0_DefaultValue,
	NULL
};
extern MethodInfo List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28387_MethodInfo;
static PropertyInfo List_1_t1007____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&List_1_t1007_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.ICollection<T>.IsReadOnly"/* name */
	, &List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28387_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo List_1_System_Collections_ICollection_get_IsSynchronized_m28388_MethodInfo;
static PropertyInfo List_1_t1007____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&List_1_t1007_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.IsSynchronized"/* name */
	, &List_1_System_Collections_ICollection_get_IsSynchronized_m28388_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo List_1_System_Collections_ICollection_get_SyncRoot_m28389_MethodInfo;
static PropertyInfo List_1_t1007____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&List_1_t1007_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.SyncRoot"/* name */
	, &List_1_System_Collections_ICollection_get_SyncRoot_m28389_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo List_1_System_Collections_IList_get_IsFixedSize_m28390_MethodInfo;
static PropertyInfo List_1_t1007____System_Collections_IList_IsFixedSize_PropertyInfo = 
{
	&List_1_t1007_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.IsFixedSize"/* name */
	, &List_1_System_Collections_IList_get_IsFixedSize_m28390_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo List_1_System_Collections_IList_get_IsReadOnly_m28391_MethodInfo;
static PropertyInfo List_1_t1007____System_Collections_IList_IsReadOnly_PropertyInfo = 
{
	&List_1_t1007_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.IsReadOnly"/* name */
	, &List_1_System_Collections_IList_get_IsReadOnly_m28391_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo List_1_System_Collections_IList_get_Item_m28392_MethodInfo;
extern MethodInfo List_1_System_Collections_IList_set_Item_m28393_MethodInfo;
static PropertyInfo List_1_t1007____System_Collections_IList_Item_PropertyInfo = 
{
	&List_1_t1007_il2cpp_TypeInfo/* parent */
	, "System.Collections.IList.Item"/* name */
	, &List_1_System_Collections_IList_get_Item_m28392_MethodInfo/* get */
	, &List_1_System_Collections_IList_set_Item_m28393_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo List_1_t1007____Capacity_PropertyInfo = 
{
	&List_1_t1007_il2cpp_TypeInfo/* parent */
	, "Capacity"/* name */
	, &List_1_get_Capacity_m28420_MethodInfo/* get */
	, &List_1_set_Capacity_m28421_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo List_1_get_Count_m28422_MethodInfo;
static PropertyInfo List_1_t1007____Count_PropertyInfo = 
{
	&List_1_t1007_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &List_1_get_Count_m28422_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo List_1_t1007____Item_PropertyInfo = 
{
	&List_1_t1007_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &List_1_get_Item_m28423_MethodInfo/* get */
	, &List_1_set_Item_m28424_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* List_1_t1007_PropertyInfos[] =
{
	&List_1_t1007____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&List_1_t1007____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&List_1_t1007____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&List_1_t1007____System_Collections_IList_IsFixedSize_PropertyInfo,
	&List_1_t1007____System_Collections_IList_IsReadOnly_PropertyInfo,
	&List_1_t1007____System_Collections_IList_Item_PropertyInfo,
	&List_1_t1007____Capacity_PropertyInfo,
	&List_1_t1007____Count_PropertyInfo,
	&List_1_t1007____Item_PropertyInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1__ctor_m6487_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor()
MethodInfo List_1__ctor_m6487_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&List_1__ctor_m14543_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1__ctor_m6487_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_1_t4641_0_0_0;
extern Il2CppType IEnumerable_1_t4641_0_0_0;
static ParameterInfo List_1_t1007_List_1__ctor_m28376_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_1_t4641_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1__ctor_m28376_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
MethodInfo List_1__ctor_m28376_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&List_1__ctor_m14545_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, List_1_t1007_List_1__ctor_m28376_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1__ctor_m28376_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo List_1_t1007_List_1__ctor_m28377_ParameterInfos[] = 
{
	{"capacity", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1__ctor_m28377_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor(System.Int32)
MethodInfo List_1__ctor_m28377_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&List_1__ctor_m14547_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, List_1_t1007_List_1__ctor_m28377_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1__ctor_m28377_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1__cctor_m28378_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.cctor()
MethodInfo List_1__cctor_m28378_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&List_1__cctor_m14549_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1__cctor_m28378_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_1_t4642_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m28379_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
MethodInfo List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m28379_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator"/* name */
	, (methodPointerType)&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14551_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t4642_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m28379_GenericMethod/* genericMethod */

};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo List_1_t1007_List_1_System_Collections_ICollection_CopyTo_m28380_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_ICollection_CopyTo_m28380_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
MethodInfo List_1_System_Collections_ICollection_CopyTo_m28380_MethodInfo = 
{
	"System.Collections.ICollection.CopyTo"/* name */
	, (methodPointerType)&List_1_System_Collections_ICollection_CopyTo_m14553_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, List_1_t1007_List_1_System_Collections_ICollection_CopyTo_m28380_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_ICollection_CopyTo_m28380_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_t266_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IEnumerable_GetEnumerator_m28381_GenericMethod;
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IEnumerable.GetEnumerator()
MethodInfo List_1_System_Collections_IEnumerable_GetEnumerator_m28381_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, (methodPointerType)&List_1_System_Collections_IEnumerable_GetEnumerator_m14555_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IEnumerable_GetEnumerator_m28381_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo List_1_t1007_List_1_System_Collections_IList_Add_m28382_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_Add_m28382_GenericMethod;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Add(System.Object)
MethodInfo List_1_System_Collections_IList_Add_m28382_MethodInfo = 
{
	"System.Collections.IList.Add"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_Add_m14557_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, List_1_t1007_List_1_System_Collections_IList_Add_m28382_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_Add_m28382_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo List_1_t1007_List_1_System_Collections_IList_Contains_m28383_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_Contains_m28383_GenericMethod;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Contains(System.Object)
MethodInfo List_1_System_Collections_IList_Contains_m28383_MethodInfo = 
{
	"System.Collections.IList.Contains"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_Contains_m14559_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, List_1_t1007_List_1_System_Collections_IList_Contains_m28383_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_Contains_m28383_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo List_1_t1007_List_1_System_Collections_IList_IndexOf_m28384_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_IndexOf_m28384_GenericMethod;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.IndexOf(System.Object)
MethodInfo List_1_System_Collections_IList_IndexOf_m28384_MethodInfo = 
{
	"System.Collections.IList.IndexOf"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_IndexOf_m14561_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, List_1_t1007_List_1_System_Collections_IList_IndexOf_m28384_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_IndexOf_m28384_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo List_1_t1007_List_1_System_Collections_IList_Insert_m28385_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_Insert_m28385_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Insert(System.Int32,System.Object)
MethodInfo List_1_System_Collections_IList_Insert_m28385_MethodInfo = 
{
	"System.Collections.IList.Insert"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_Insert_m14563_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, List_1_t1007_List_1_System_Collections_IList_Insert_m28385_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_Insert_m28385_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo List_1_t1007_List_1_System_Collections_IList_Remove_m28386_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_Remove_m28386_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Remove(System.Object)
MethodInfo List_1_System_Collections_IList_Remove_m28386_MethodInfo = 
{
	"System.Collections.IList.Remove"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_Remove_m14565_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, List_1_t1007_List_1_System_Collections_IList_Remove_m28386_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_Remove_m28386_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28387_GenericMethod;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
MethodInfo List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28387_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly"/* name */
	, (methodPointerType)&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14567_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28387_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_ICollection_get_IsSynchronized_m28388_GenericMethod;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.ICollection.get_IsSynchronized()
MethodInfo List_1_System_Collections_ICollection_get_IsSynchronized_m28388_MethodInfo = 
{
	"System.Collections.ICollection.get_IsSynchronized"/* name */
	, (methodPointerType)&List_1_System_Collections_ICollection_get_IsSynchronized_m14569_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_ICollection_get_IsSynchronized_m28388_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_ICollection_get_SyncRoot_m28389_GenericMethod;
// System.Object System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.ICollection.get_SyncRoot()
MethodInfo List_1_System_Collections_ICollection_get_SyncRoot_m28389_MethodInfo = 
{
	"System.Collections.ICollection.get_SyncRoot"/* name */
	, (methodPointerType)&List_1_System_Collections_ICollection_get_SyncRoot_m14571_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_ICollection_get_SyncRoot_m28389_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_get_IsFixedSize_m28390_GenericMethod;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.get_IsFixedSize()
MethodInfo List_1_System_Collections_IList_get_IsFixedSize_m28390_MethodInfo = 
{
	"System.Collections.IList.get_IsFixedSize"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_get_IsFixedSize_m14573_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_get_IsFixedSize_m28390_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_get_IsReadOnly_m28391_GenericMethod;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.get_IsReadOnly()
MethodInfo List_1_System_Collections_IList_get_IsReadOnly_m28391_MethodInfo = 
{
	"System.Collections.IList.get_IsReadOnly"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_get_IsReadOnly_m14575_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_get_IsReadOnly_m28391_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo List_1_t1007_List_1_System_Collections_IList_get_Item_m28392_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_get_Item_m28392_GenericMethod;
// System.Object System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.get_Item(System.Int32)
MethodInfo List_1_System_Collections_IList_get_Item_m28392_MethodInfo = 
{
	"System.Collections.IList.get_Item"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_get_Item_m14577_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, List_1_t1007_List_1_System_Collections_IList_get_Item_m28392_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_get_Item_m28392_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo List_1_t1007_List_1_System_Collections_IList_set_Item_m28393_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_System_Collections_IList_set_Item_m28393_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.set_Item(System.Int32,System.Object)
MethodInfo List_1_System_Collections_IList_set_Item_m28393_MethodInfo = 
{
	"System.Collections.IList.set_Item"/* name */
	, (methodPointerType)&List_1_System_Collections_IList_set_Item_m14579_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, List_1_t1007_List_1_System_Collections_IList_set_Item_m28393_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_System_Collections_IList_set_Item_m28393_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody2D_t1008_0_0_0;
extern Il2CppType Rigidbody2D_t1008_0_0_0;
static ParameterInfo List_1_t1007_List_1_Add_m28394_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody2D_t1008_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Add_m28394_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Add(T)
MethodInfo List_1_Add_m28394_MethodInfo = 
{
	"Add"/* name */
	, (methodPointerType)&List_1_Add_m14581_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, List_1_t1007_List_1_Add_m28394_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Add_m28394_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo List_1_t1007_List_1_GrowIfNeeded_m28395_ParameterInfos[] = 
{
	{"newCount", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_GrowIfNeeded_m28395_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::GrowIfNeeded(System.Int32)
MethodInfo List_1_GrowIfNeeded_m28395_MethodInfo = 
{
	"GrowIfNeeded"/* name */
	, (methodPointerType)&List_1_GrowIfNeeded_m14583_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, List_1_t1007_List_1_GrowIfNeeded_m28395_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_GrowIfNeeded_m28395_GenericMethod/* genericMethod */

};
extern Il2CppType ICollection_1_t4643_0_0_0;
extern Il2CppType ICollection_1_t4643_0_0_0;
static ParameterInfo List_1_t1007_List_1_AddCollection_m28396_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &ICollection_1_t4643_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_AddCollection_m28396_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AddCollection(System.Collections.Generic.ICollection`1<T>)
MethodInfo List_1_AddCollection_m28396_MethodInfo = 
{
	"AddCollection"/* name */
	, (methodPointerType)&List_1_AddCollection_m14585_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, List_1_t1007_List_1_AddCollection_m28396_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_AddCollection_m28396_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_1_t4641_0_0_0;
static ParameterInfo List_1_t1007_List_1_AddEnumerable_m28397_ParameterInfos[] = 
{
	{"enumerable", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_1_t4641_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_AddEnumerable_m28397_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
MethodInfo List_1_AddEnumerable_m28397_MethodInfo = 
{
	"AddEnumerable"/* name */
	, (methodPointerType)&List_1_AddEnumerable_m14587_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, List_1_t1007_List_1_AddEnumerable_m28397_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_AddEnumerable_m28397_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_1_t4641_0_0_0;
static ParameterInfo List_1_t1007_List_1_AddRange_m28398_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_1_t4641_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_AddRange_m28398_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
MethodInfo List_1_AddRange_m28398_MethodInfo = 
{
	"AddRange"/* name */
	, (methodPointerType)&List_1_AddRange_m14588_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, List_1_t1007_List_1_AddRange_m28398_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_AddRange_m28398_GenericMethod/* genericMethod */

};
extern Il2CppType ReadOnlyCollection_1_t4644_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_AsReadOnly_m28399_GenericMethod;
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AsReadOnly()
MethodInfo List_1_AsReadOnly_m28399_MethodInfo = 
{
	"AsReadOnly"/* name */
	, (methodPointerType)&List_1_AsReadOnly_m14590_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &ReadOnlyCollection_1_t4644_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_AsReadOnly_m28399_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Clear_m28400_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Clear()
MethodInfo List_1_Clear_m28400_MethodInfo = 
{
	"Clear"/* name */
	, (methodPointerType)&List_1_Clear_m14592_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Clear_m28400_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody2D_t1008_0_0_0;
static ParameterInfo List_1_t1007_List_1_Contains_m28401_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody2D_t1008_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Contains_m28401_GenericMethod;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Contains(T)
MethodInfo List_1_Contains_m28401_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&List_1_Contains_m14594_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, List_1_t1007_List_1_Contains_m28401_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 24/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Contains_m28401_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody2DU5BU5D_t4640_0_0_0;
extern Il2CppType Rigidbody2DU5BU5D_t4640_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo List_1_t1007_List_1_CopyTo_m28402_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody2DU5BU5D_t4640_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_CopyTo_m28402_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CopyTo(T[],System.Int32)
MethodInfo List_1_CopyTo_m28402_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&List_1_CopyTo_m14596_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, List_1_t1007_List_1_CopyTo_m28402_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_CopyTo_m28402_GenericMethod/* genericMethod */

};
extern Il2CppType Predicate_1_t4645_0_0_0;
extern Il2CppType Predicate_1_t4645_0_0_0;
static ParameterInfo List_1_t1007_List_1_Find_m28403_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &Predicate_1_t4645_0_0_0},
};
extern Il2CppType Rigidbody2D_t1008_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Find_m28403_GenericMethod;
// T System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Find(System.Predicate`1<T>)
MethodInfo List_1_Find_m28403_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&List_1_Find_m14598_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Rigidbody2D_t1008_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, List_1_t1007_List_1_Find_m28403_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Find_m28403_GenericMethod/* genericMethod */

};
extern Il2CppType Predicate_1_t4645_0_0_0;
static ParameterInfo List_1_t1007_List_1_CheckMatch_m28404_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &Predicate_1_t4645_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_CheckMatch_m28404_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CheckMatch(System.Predicate`1<T>)
MethodInfo List_1_CheckMatch_m28404_MethodInfo = 
{
	"CheckMatch"/* name */
	, (methodPointerType)&List_1_CheckMatch_m14600_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, List_1_t1007_List_1_CheckMatch_m28404_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_CheckMatch_m28404_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Predicate_1_t4645_0_0_0;
static ParameterInfo List_1_t1007_List_1_GetIndex_m28405_ParameterInfos[] = 
{
	{"startIndex", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"count", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"match", 2, 134217728, &EmptyCustomAttributesCache, &Predicate_1_t4645_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_GetIndex_m28405_GenericMethod;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
MethodInfo List_1_GetIndex_m28405_MethodInfo = 
{
	"GetIndex"/* name */
	, (methodPointerType)&List_1_GetIndex_m14602_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93_Int32_t93_Object_t/* invoker_method */
	, List_1_t1007_List_1_GetIndex_m28405_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_GetIndex_m28405_GenericMethod/* genericMethod */

};
extern Il2CppType Enumerator_t4647_0_0_0;
extern void* RuntimeInvoker_Enumerator_t4647 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_GetEnumerator_m28406_GenericMethod;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::GetEnumerator()
MethodInfo List_1_GetEnumerator_m28406_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&List_1_GetEnumerator_m28406/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Enumerator_t4647_0_0_0/* return_type */
	, RuntimeInvoker_Enumerator_t4647/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_GetEnumerator_m28406_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody2D_t1008_0_0_0;
static ParameterInfo List_1_t1007_List_1_IndexOf_m28407_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody2D_t1008_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_IndexOf_m28407_GenericMethod;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::IndexOf(T)
MethodInfo List_1_IndexOf_m28407_MethodInfo = 
{
	"IndexOf"/* name */
	, (methodPointerType)&List_1_IndexOf_m14604_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, List_1_t1007_List_1_IndexOf_m28407_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_IndexOf_m28407_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo List_1_t1007_List_1_Shift_m28408_ParameterInfos[] = 
{
	{"start", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"delta", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Shift_m28408_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Shift(System.Int32,System.Int32)
MethodInfo List_1_Shift_m28408_MethodInfo = 
{
	"Shift"/* name */
	, (methodPointerType)&List_1_Shift_m14606_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, List_1_t1007_List_1_Shift_m28408_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Shift_m28408_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo List_1_t1007_List_1_CheckIndex_m28409_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_CheckIndex_m28409_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CheckIndex(System.Int32)
MethodInfo List_1_CheckIndex_m28409_MethodInfo = 
{
	"CheckIndex"/* name */
	, (methodPointerType)&List_1_CheckIndex_m14608_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, List_1_t1007_List_1_CheckIndex_m28409_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_CheckIndex_m28409_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Rigidbody2D_t1008_0_0_0;
static ParameterInfo List_1_t1007_List_1_Insert_m28410_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Rigidbody2D_t1008_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Insert_m28410_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Insert(System.Int32,T)
MethodInfo List_1_Insert_m28410_MethodInfo = 
{
	"Insert"/* name */
	, (methodPointerType)&List_1_Insert_m14610_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, List_1_t1007_List_1_Insert_m28410_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Insert_m28410_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_1_t4641_0_0_0;
static ParameterInfo List_1_t1007_List_1_CheckCollection_m28411_ParameterInfos[] = 
{
	{"collection", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_1_t4641_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_CheckCollection_m28411_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
MethodInfo List_1_CheckCollection_m28411_MethodInfo = 
{
	"CheckCollection"/* name */
	, (methodPointerType)&List_1_CheckCollection_m14612_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, List_1_t1007_List_1_CheckCollection_m28411_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_CheckCollection_m28411_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody2D_t1008_0_0_0;
static ParameterInfo List_1_t1007_List_1_Remove_m28412_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody2D_t1008_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Remove_m28412_GenericMethod;
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Remove(T)
MethodInfo List_1_Remove_m28412_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&List_1_Remove_m14614_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, List_1_t1007_List_1_Remove_m28412_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Remove_m28412_GenericMethod/* genericMethod */

};
extern Il2CppType Predicate_1_t4645_0_0_0;
static ParameterInfo List_1_t1007_List_1_RemoveAll_m28413_ParameterInfos[] = 
{
	{"match", 0, 134217728, &EmptyCustomAttributesCache, &Predicate_1_t4645_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_RemoveAll_m28413_GenericMethod;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::RemoveAll(System.Predicate`1<T>)
MethodInfo List_1_RemoveAll_m28413_MethodInfo = 
{
	"RemoveAll"/* name */
	, (methodPointerType)&List_1_RemoveAll_m14616_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, List_1_t1007_List_1_RemoveAll_m28413_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_RemoveAll_m28413_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo List_1_t1007_List_1_RemoveAt_m28414_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_RemoveAt_m28414_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::RemoveAt(System.Int32)
MethodInfo List_1_RemoveAt_m28414_MethodInfo = 
{
	"RemoveAt"/* name */
	, (methodPointerType)&List_1_RemoveAt_m14618_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, List_1_t1007_List_1_RemoveAt_m28414_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_RemoveAt_m28414_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Reverse_m28415_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Reverse()
MethodInfo List_1_Reverse_m28415_MethodInfo = 
{
	"Reverse"/* name */
	, (methodPointerType)&List_1_Reverse_m14620_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Reverse_m28415_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Sort_m28416_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Sort()
MethodInfo List_1_Sort_m28416_MethodInfo = 
{
	"Sort"/* name */
	, (methodPointerType)&List_1_Sort_m14622_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Sort_m28416_GenericMethod/* genericMethod */

};
extern Il2CppType Comparison_1_t4646_0_0_0;
extern Il2CppType Comparison_1_t4646_0_0_0;
static ParameterInfo List_1_t1007_List_1_Sort_m28417_ParameterInfos[] = 
{
	{"comparison", 0, 134217728, &EmptyCustomAttributesCache, &Comparison_1_t4646_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_Sort_m28417_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Sort(System.Comparison`1<T>)
MethodInfo List_1_Sort_m28417_MethodInfo = 
{
	"Sort"/* name */
	, (methodPointerType)&List_1_Sort_m14624_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, List_1_t1007_List_1_Sort_m28417_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_Sort_m28417_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody2DU5BU5D_t4640_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_ToArray_m28418_GenericMethod;
// T[] System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::ToArray()
MethodInfo List_1_ToArray_m28418_MethodInfo = 
{
	"ToArray"/* name */
	, (methodPointerType)&List_1_ToArray_m14626_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Rigidbody2DU5BU5D_t4640_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_ToArray_m28418_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_TrimExcess_m28419_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::TrimExcess()
MethodInfo List_1_TrimExcess_m28419_MethodInfo = 
{
	"TrimExcess"/* name */
	, (methodPointerType)&List_1_TrimExcess_m14628_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_TrimExcess_m28419_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_get_Capacity_m28420_GenericMethod;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::get_Capacity()
MethodInfo List_1_get_Capacity_m28420_MethodInfo = 
{
	"get_Capacity"/* name */
	, (methodPointerType)&List_1_get_Capacity_m14630_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_get_Capacity_m28420_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo List_1_t1007_List_1_set_Capacity_m28421_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_set_Capacity_m28421_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::set_Capacity(System.Int32)
MethodInfo List_1_set_Capacity_m28421_MethodInfo = 
{
	"set_Capacity"/* name */
	, (methodPointerType)&List_1_set_Capacity_m14632_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, List_1_t1007_List_1_set_Capacity_m28421_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_set_Capacity_m28421_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_get_Count_m28422_GenericMethod;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::get_Count()
MethodInfo List_1_get_Count_m28422_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&List_1_get_Count_m14634_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_get_Count_m28422_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo List_1_t1007_List_1_get_Item_m28423_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Rigidbody2D_t1008_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_get_Item_m28423_GenericMethod;
// T System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::get_Item(System.Int32)
MethodInfo List_1_get_Item_m28423_MethodInfo = 
{
	"get_Item"/* name */
	, (methodPointerType)&List_1_get_Item_m14636_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Rigidbody2D_t1008_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, List_1_t1007_List_1_get_Item_m28423_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_get_Item_m28423_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Rigidbody2D_t1008_0_0_0;
static ParameterInfo List_1_t1007_List_1_set_Item_m28424_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Rigidbody2D_t1008_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod List_1_set_Item_m28424_GenericMethod;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::set_Item(System.Int32,T)
MethodInfo List_1_set_Item_m28424_MethodInfo = 
{
	"set_Item"/* name */
	, (methodPointerType)&List_1_set_Item_m14638_gshared/* method */
	, &List_1_t1007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, List_1_t1007_List_1_set_Item_m28424_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 32/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &List_1_set_Item_m28424_GenericMethod/* genericMethod */

};
static MethodInfo* List_1_t1007_MethodInfos[] =
{
	&List_1__ctor_m6487_MethodInfo,
	&List_1__ctor_m28376_MethodInfo,
	&List_1__ctor_m28377_MethodInfo,
	&List_1__cctor_m28378_MethodInfo,
	&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m28379_MethodInfo,
	&List_1_System_Collections_ICollection_CopyTo_m28380_MethodInfo,
	&List_1_System_Collections_IEnumerable_GetEnumerator_m28381_MethodInfo,
	&List_1_System_Collections_IList_Add_m28382_MethodInfo,
	&List_1_System_Collections_IList_Contains_m28383_MethodInfo,
	&List_1_System_Collections_IList_IndexOf_m28384_MethodInfo,
	&List_1_System_Collections_IList_Insert_m28385_MethodInfo,
	&List_1_System_Collections_IList_Remove_m28386_MethodInfo,
	&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28387_MethodInfo,
	&List_1_System_Collections_ICollection_get_IsSynchronized_m28388_MethodInfo,
	&List_1_System_Collections_ICollection_get_SyncRoot_m28389_MethodInfo,
	&List_1_System_Collections_IList_get_IsFixedSize_m28390_MethodInfo,
	&List_1_System_Collections_IList_get_IsReadOnly_m28391_MethodInfo,
	&List_1_System_Collections_IList_get_Item_m28392_MethodInfo,
	&List_1_System_Collections_IList_set_Item_m28393_MethodInfo,
	&List_1_Add_m28394_MethodInfo,
	&List_1_GrowIfNeeded_m28395_MethodInfo,
	&List_1_AddCollection_m28396_MethodInfo,
	&List_1_AddEnumerable_m28397_MethodInfo,
	&List_1_AddRange_m28398_MethodInfo,
	&List_1_AsReadOnly_m28399_MethodInfo,
	&List_1_Clear_m28400_MethodInfo,
	&List_1_Contains_m28401_MethodInfo,
	&List_1_CopyTo_m28402_MethodInfo,
	&List_1_Find_m28403_MethodInfo,
	&List_1_CheckMatch_m28404_MethodInfo,
	&List_1_GetIndex_m28405_MethodInfo,
	&List_1_GetEnumerator_m28406_MethodInfo,
	&List_1_IndexOf_m28407_MethodInfo,
	&List_1_Shift_m28408_MethodInfo,
	&List_1_CheckIndex_m28409_MethodInfo,
	&List_1_Insert_m28410_MethodInfo,
	&List_1_CheckCollection_m28411_MethodInfo,
	&List_1_Remove_m28412_MethodInfo,
	&List_1_RemoveAll_m28413_MethodInfo,
	&List_1_RemoveAt_m28414_MethodInfo,
	&List_1_Reverse_m28415_MethodInfo,
	&List_1_Sort_m28416_MethodInfo,
	&List_1_Sort_m28417_MethodInfo,
	&List_1_ToArray_m28418_MethodInfo,
	&List_1_TrimExcess_m28419_MethodInfo,
	&List_1_get_Capacity_m28420_MethodInfo,
	&List_1_set_Capacity_m28421_MethodInfo,
	&List_1_get_Count_m28422_MethodInfo,
	&List_1_get_Item_m28423_MethodInfo,
	&List_1_set_Item_m28424_MethodInfo,
	NULL
};
extern MethodInfo List_1_System_Collections_IEnumerable_GetEnumerator_m28381_MethodInfo;
extern MethodInfo List_1_System_Collections_ICollection_CopyTo_m28380_MethodInfo;
extern MethodInfo List_1_System_Collections_IList_Add_m28382_MethodInfo;
extern MethodInfo List_1_Clear_m28400_MethodInfo;
extern MethodInfo List_1_System_Collections_IList_Contains_m28383_MethodInfo;
extern MethodInfo List_1_System_Collections_IList_IndexOf_m28384_MethodInfo;
extern MethodInfo List_1_System_Collections_IList_Insert_m28385_MethodInfo;
extern MethodInfo List_1_System_Collections_IList_Remove_m28386_MethodInfo;
extern MethodInfo List_1_CopyTo_m28402_MethodInfo;
extern MethodInfo List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m28379_MethodInfo;
static MethodInfo* List_1_t1007_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&List_1_System_Collections_IEnumerable_GetEnumerator_m28381_MethodInfo,
	&List_1_get_Count_m28422_MethodInfo,
	&List_1_System_Collections_ICollection_get_IsSynchronized_m28388_MethodInfo,
	&List_1_System_Collections_ICollection_get_SyncRoot_m28389_MethodInfo,
	&List_1_System_Collections_ICollection_CopyTo_m28380_MethodInfo,
	&List_1_System_Collections_IList_get_IsFixedSize_m28390_MethodInfo,
	&List_1_System_Collections_IList_get_IsReadOnly_m28391_MethodInfo,
	&List_1_System_Collections_IList_get_Item_m28392_MethodInfo,
	&List_1_System_Collections_IList_set_Item_m28393_MethodInfo,
	&List_1_System_Collections_IList_Add_m28382_MethodInfo,
	&List_1_Clear_m28400_MethodInfo,
	&List_1_System_Collections_IList_Contains_m28383_MethodInfo,
	&List_1_System_Collections_IList_IndexOf_m28384_MethodInfo,
	&List_1_System_Collections_IList_Insert_m28385_MethodInfo,
	&List_1_System_Collections_IList_Remove_m28386_MethodInfo,
	&List_1_RemoveAt_m28414_MethodInfo,
	&List_1_get_Count_m28422_MethodInfo,
	&List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m28387_MethodInfo,
	&List_1_Add_m28394_MethodInfo,
	&List_1_Clear_m28400_MethodInfo,
	&List_1_Contains_m28401_MethodInfo,
	&List_1_CopyTo_m28402_MethodInfo,
	&List_1_Remove_m28412_MethodInfo,
	&List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m28379_MethodInfo,
	&List_1_IndexOf_m28407_MethodInfo,
	&List_1_Insert_m28410_MethodInfo,
	&List_1_RemoveAt_m28414_MethodInfo,
	&List_1_get_Item_m28423_MethodInfo,
	&List_1_set_Item_m28424_MethodInfo,
};
extern TypeInfo ICollection_t1206_il2cpp_TypeInfo;
extern TypeInfo IList_t1435_il2cpp_TypeInfo;
extern TypeInfo IList_1_t4649_il2cpp_TypeInfo;
static TypeInfo* List_1_t1007_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_t1206_il2cpp_TypeInfo,
	&IList_t1435_il2cpp_TypeInfo,
	&ICollection_1_t4643_il2cpp_TypeInfo,
	&IEnumerable_1_t4641_il2cpp_TypeInfo,
	&IList_1_t4649_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair List_1_t1007_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t4643_il2cpp_TypeInfo, 20},
	{ &IEnumerable_1_t4641_il2cpp_TypeInfo, 27},
	{ &IList_1_t4649_il2cpp_TypeInfo, 28},
};
extern TypeInfo List_1_t1007_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t4643_il2cpp_TypeInfo;
extern TypeInfo Rigidbody2DU5BU5D_t4640_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t4647_il2cpp_TypeInfo;
extern TypeInfo Rigidbody2D_t1008_il2cpp_TypeInfo;
extern TypeInfo ReadOnlyCollection_1_t4644_il2cpp_TypeInfo;
static Il2CppRGCTXData List_1_t1007_RGCTXData[37] = 
{
	&List_1_t1007_il2cpp_TypeInfo/* Static Usage */,
	&List_1_CheckCollection_m28411_MethodInfo/* Method Usage */,
	&ICollection_1_t4643_il2cpp_TypeInfo/* Class Usage */,
	&List_1_AddEnumerable_m28397_MethodInfo/* Method Usage */,
	&ICollection_1_get_Count_m47395_MethodInfo/* Method Usage */,
	&Rigidbody2DU5BU5D_t4640_il2cpp_TypeInfo/* Array Usage */,
	&List_1_AddCollection_m28396_MethodInfo/* Method Usage */,
	&List_1_GetEnumerator_m28406_MethodInfo/* Method Usage */,
	&Enumerator_t4647_il2cpp_TypeInfo/* Class Usage */,
	&Rigidbody2D_t1008_il2cpp_TypeInfo/* Class Usage */,
	&List_1_Add_m28394_MethodInfo/* Method Usage */,
	&List_1_Contains_m28401_MethodInfo/* Method Usage */,
	&List_1_IndexOf_m28407_MethodInfo/* Method Usage */,
	&List_1_CheckIndex_m28409_MethodInfo/* Method Usage */,
	&List_1_Insert_m28410_MethodInfo/* Method Usage */,
	&List_1_Remove_m28412_MethodInfo/* Method Usage */,
	&List_1_get_Item_m28423_MethodInfo/* Method Usage */,
	&List_1_set_Item_m28424_MethodInfo/* Method Usage */,
	&List_1_GrowIfNeeded_m28395_MethodInfo/* Method Usage */,
	&List_1_get_Capacity_m28420_MethodInfo/* Method Usage */,
	&List_1_set_Capacity_m28421_MethodInfo/* Method Usage */,
	&ICollection_1_CopyTo_m47396_MethodInfo/* Method Usage */,
	&IEnumerable_1_GetEnumerator_m47397_MethodInfo/* Method Usage */,
	&IEnumerator_1_get_Current_m47398_MethodInfo/* Method Usage */,
	&ReadOnlyCollection_1_t4644_il2cpp_TypeInfo/* Class Usage */,
	&ReadOnlyCollection_1__ctor_m28436_MethodInfo/* Method Usage */,
	&Array_IndexOf_TisRigidbody2D_t1008_m37375_MethodInfo/* Method Usage */,
	&List_1_CheckMatch_m28404_MethodInfo/* Method Usage */,
	&List_1_GetIndex_m28405_MethodInfo/* Method Usage */,
	&Predicate_1_Invoke_m28511_MethodInfo/* Method Usage */,
	&Enumerator__ctor_m28430_MethodInfo/* Method Usage */,
	&List_1_Shift_m28408_MethodInfo/* Method Usage */,
	&List_1_RemoveAt_m28414_MethodInfo/* Method Usage */,
	&Comparer_1_get_Default_m28517_MethodInfo/* Method Usage */,
	&Array_Sort_TisRigidbody2D_t1008_m37377_MethodInfo/* Method Usage */,
	&Array_Sort_TisRigidbody2D_t1008_m37385_MethodInfo/* Method Usage */,
	&Array_Resize_TisRigidbody2D_t1008_m37373_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType List_1_t1007_0_0_0;
extern Il2CppType List_1_t1007_1_0_0;
struct List_1_t1007;
extern Il2CppGenericClass List_1_t1007_GenericClass;
extern CustomAttributesCache List_1_t1818__CustomAttributeCache;
TypeInfo List_1_t1007_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "List`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, List_1_t1007_MethodInfos/* methods */
	, List_1_t1007_PropertyInfos/* properties */
	, List_1_t1007_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &List_1_t1007_il2cpp_TypeInfo/* element_class */
	, List_1_t1007_InterfacesTypeInfos/* implemented_interfaces */
	, List_1_t1007_VTable/* vtable */
	, &List_1_t1818__CustomAttributeCache/* custom_attributes_cache */
	, &List_1_t1007_il2cpp_TypeInfo/* cast_class */
	, &List_1_t1007_0_0_0/* byval_arg */
	, &List_1_t1007_1_0_0/* this_arg */
	, List_1_t1007_InterfacesOffsets/* interface_offsets */
	, &List_1_t1007_GenericClass/* generic_class */
	, NULL/* generic_container */
	, List_1_t1007_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, List_1_t1007_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (List_1_t1007)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(List_1_t1007_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 50/* method_count */
	, 9/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 33/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>
static PropertyInfo ICollection_1_t4643____Count_PropertyInfo = 
{
	&ICollection_1_t4643_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m47395_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m47399_MethodInfo;
static PropertyInfo ICollection_1_t4643____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t4643_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m47399_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t4643_PropertyInfos[] =
{
	&ICollection_1_t4643____Count_PropertyInfo,
	&ICollection_1_t4643____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m47395_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>::get_Count()
MethodInfo ICollection_1_get_Count_m47395_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t4643_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m47395_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m47399_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m47399_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t4643_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m47399_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody2D_t1008_0_0_0;
static ParameterInfo ICollection_1_t4643_ICollection_1_Add_m47400_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody2D_t1008_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m47400_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>::Add(T)
MethodInfo ICollection_1_Add_m47400_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t4643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t4643_ICollection_1_Add_m47400_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m47400_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m47401_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>::Clear()
MethodInfo ICollection_1_Clear_m47401_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t4643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m47401_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody2D_t1008_0_0_0;
static ParameterInfo ICollection_1_t4643_ICollection_1_Contains_m47402_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody2D_t1008_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m47402_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>::Contains(T)
MethodInfo ICollection_1_Contains_m47402_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t4643_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t4643_ICollection_1_Contains_m47402_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m47402_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody2DU5BU5D_t4640_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t4643_ICollection_1_CopyTo_m47396_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody2DU5BU5D_t4640_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m47396_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m47396_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t4643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t4643_ICollection_1_CopyTo_m47396_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m47396_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody2D_t1008_0_0_0;
static ParameterInfo ICollection_1_t4643_ICollection_1_Remove_m47403_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody2D_t1008_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m47403_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Rigidbody2D>::Remove(T)
MethodInfo ICollection_1_Remove_m47403_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t4643_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t4643_ICollection_1_Remove_m47403_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m47403_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t4643_MethodInfos[] =
{
	&ICollection_1_get_Count_m47395_MethodInfo,
	&ICollection_1_get_IsReadOnly_m47399_MethodInfo,
	&ICollection_1_Add_m47400_MethodInfo,
	&ICollection_1_Clear_m47401_MethodInfo,
	&ICollection_1_Contains_m47402_MethodInfo,
	&ICollection_1_CopyTo_m47396_MethodInfo,
	&ICollection_1_Remove_m47403_MethodInfo,
	NULL
};
static TypeInfo* ICollection_1_t4643_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t4641_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t4643_1_0_0;
struct ICollection_1_t4643;
extern Il2CppGenericClass ICollection_1_t4643_GenericClass;
TypeInfo ICollection_1_t4643_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t4643_MethodInfos/* methods */
	, ICollection_1_t4643_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t4643_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t4643_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t4643_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t4643_0_0_0/* byval_arg */
	, &ICollection_1_t4643_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t4643_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Rigidbody2D>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Rigidbody2D>
extern Il2CppType IEnumerator_1_t4642_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m47397_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Rigidbody2D>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m47397_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t4641_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t4642_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m47397_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t4641_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m47397_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t4641_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t4641_1_0_0;
struct IEnumerable_1_t4641;
extern Il2CppGenericClass IEnumerable_1_t4641_GenericClass;
TypeInfo IEnumerable_1_t4641_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t4641_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t4641_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t4641_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t4641_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t4641_0_0_0/* byval_arg */
	, &IEnumerable_1_t4641_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t4641_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// T System.Collections.Generic.IEnumerator`1<UnityEngine.Rigidbody2D>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Rigidbody2D>
static PropertyInfo IEnumerator_1_t4642____Current_PropertyInfo = 
{
	&IEnumerator_1_t4642_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m47398_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t4642_PropertyInfos[] =
{
	&IEnumerator_1_t4642____Current_PropertyInfo,
	NULL
};
extern Il2CppType Rigidbody2D_t1008_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m47398_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Rigidbody2D>::get_Current()
MethodInfo IEnumerator_1_get_Current_m47398_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t4642_il2cpp_TypeInfo/* declaring_type */
	, &Rigidbody2D_t1008_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m47398_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t4642_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m47398_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t4642_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t4642_0_0_0;
extern Il2CppType IEnumerator_1_t4642_1_0_0;
struct IEnumerator_1_t4642;
extern Il2CppGenericClass IEnumerator_1_t4642_GenericClass;
TypeInfo IEnumerator_1_t4642_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t4642_MethodInfos/* methods */
	, IEnumerator_1_t4642_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t4642_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t4642_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t4642_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t4642_0_0_0/* byval_arg */
	, &IEnumerator_1_t4642_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t4642_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_420.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4648_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Rigidbody2D>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_420MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m28429_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRigidbody2D_t1008_m37362_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Rigidbody2D>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Rigidbody2D>(System.Int32)
#define Array_InternalArray__get_Item_TisRigidbody2D_t1008_m37362(__this, p0, method) (Rigidbody2D_t1008 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rigidbody2D>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Rigidbody2D>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rigidbody2D>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Rigidbody2D>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Rigidbody2D>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Rigidbody2D>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4648____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4648_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4648, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4648____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4648_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4648, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4648_FieldInfos[] =
{
	&InternalEnumerator_1_t4648____array_0_FieldInfo,
	&InternalEnumerator_1_t4648____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28426_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4648____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4648_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28426_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4648____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4648_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m28429_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4648_PropertyInfos[] =
{
	&InternalEnumerator_1_t4648____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4648____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4648_InternalEnumerator_1__ctor_m28425_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m28425_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rigidbody2D>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m28425_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4648_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4648_InternalEnumerator_1__ctor_m28425_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m28425_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28426_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Rigidbody2D>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28426_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4648_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28426_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m28427_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Rigidbody2D>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m28427_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4648_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m28427_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m28428_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Rigidbody2D>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m28428_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4648_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m28428_GenericMethod/* genericMethod */

};
extern Il2CppType Rigidbody2D_t1008_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m28429_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Rigidbody2D>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m28429_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4648_il2cpp_TypeInfo/* declaring_type */
	, &Rigidbody2D_t1008_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m28429_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4648_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m28425_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28426_MethodInfo,
	&InternalEnumerator_1_Dispose_m28427_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28428_MethodInfo,
	&InternalEnumerator_1_get_Current_m28429_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m28428_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m28427_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4648_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28426_MethodInfo,
	&InternalEnumerator_1_MoveNext_m28428_MethodInfo,
	&InternalEnumerator_1_Dispose_m28427_MethodInfo,
	&InternalEnumerator_1_get_Current_m28429_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4648_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t4642_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4648_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t4642_il2cpp_TypeInfo, 7},
};
extern TypeInfo Rigidbody2D_t1008_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4648_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m28429_MethodInfo/* Method Usage */,
	&Rigidbody2D_t1008_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisRigidbody2D_t1008_m37362_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4648_0_0_0;
extern Il2CppType InternalEnumerator_1_t4648_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4648_GenericClass;
TypeInfo InternalEnumerator_1_t4648_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4648_MethodInfos/* methods */
	, InternalEnumerator_1_t4648_PropertyInfos/* properties */
	, InternalEnumerator_1_t4648_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4648_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4648_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4648_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4648_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4648_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4648_1_0_0/* this_arg */
	, InternalEnumerator_1_t4648_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4648_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4648_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4648)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Rigidbody2D>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Rigidbody2D>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Rigidbody2D>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Rigidbody2D>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Rigidbody2D>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Rigidbody2D>
extern MethodInfo IList_1_get_Item_m47404_MethodInfo;
extern MethodInfo IList_1_set_Item_m47405_MethodInfo;
static PropertyInfo IList_1_t4649____Item_PropertyInfo = 
{
	&IList_1_t4649_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m47404_MethodInfo/* get */
	, &IList_1_set_Item_m47405_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t4649_PropertyInfos[] =
{
	&IList_1_t4649____Item_PropertyInfo,
	NULL
};
extern Il2CppType Rigidbody2D_t1008_0_0_0;
static ParameterInfo IList_1_t4649_IList_1_IndexOf_m47406_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Rigidbody2D_t1008_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m47406_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Rigidbody2D>::IndexOf(T)
MethodInfo IList_1_IndexOf_m47406_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t4649_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t4649_IList_1_IndexOf_m47406_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m47406_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Rigidbody2D_t1008_0_0_0;
static ParameterInfo IList_1_t4649_IList_1_Insert_m47407_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Rigidbody2D_t1008_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m47407_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Rigidbody2D>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m47407_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t4649_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t4649_IList_1_Insert_m47407_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m47407_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t4649_IList_1_RemoveAt_m47408_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m47408_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Rigidbody2D>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m47408_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t4649_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t4649_IList_1_RemoveAt_m47408_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m47408_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t4649_IList_1_get_Item_m47404_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Rigidbody2D_t1008_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m47404_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Rigidbody2D>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m47404_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t4649_il2cpp_TypeInfo/* declaring_type */
	, &Rigidbody2D_t1008_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t4649_IList_1_get_Item_m47404_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m47404_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Rigidbody2D_t1008_0_0_0;
static ParameterInfo IList_1_t4649_IList_1_set_Item_m47405_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Rigidbody2D_t1008_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m47405_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Rigidbody2D>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m47405_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t4649_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t4649_IList_1_set_Item_m47405_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m47405_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t4649_MethodInfos[] =
{
	&IList_1_IndexOf_m47406_MethodInfo,
	&IList_1_Insert_m47407_MethodInfo,
	&IList_1_RemoveAt_m47408_MethodInfo,
	&IList_1_get_Item_m47404_MethodInfo,
	&IList_1_set_Item_m47405_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t4649_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t4643_il2cpp_TypeInfo,
	&IEnumerable_1_t4641_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t4649_0_0_0;
extern Il2CppType IList_1_t4649_1_0_0;
struct IList_1_t4649;
extern Il2CppGenericClass IList_1_t4649_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t4649_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t4649_MethodInfos/* methods */
	, IList_1_t4649_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t4649_il2cpp_TypeInfo/* element_class */
	, IList_1_t4649_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t4649_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t4649_0_0_0/* byval_arg */
	, &IList_1_t4649_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t4649_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
