﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordResult,Vuforia.WordResult>
struct Transform_1_t3971;
// System.Object
struct Object_t;
// Vuforia.WordResult
struct WordResult_t702;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordResult,Vuforia.WordResult>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m23061 (Transform_1_t3971 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordResult,Vuforia.WordResult>::Invoke(TKey,TValue)
 WordResult_t702 * Transform_1_Invoke_m23062 (Transform_1_t3971 * __this, int32_t ___key, WordResult_t702 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordResult,Vuforia.WordResult>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m23063 (Transform_1_t3971 * __this, int32_t ___key, WordResult_t702 * ___value, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordResult,Vuforia.WordResult>::EndInvoke(System.IAsyncResult)
 WordResult_t702 * Transform_1_EndInvoke_m23064 (Transform_1_t3971 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
