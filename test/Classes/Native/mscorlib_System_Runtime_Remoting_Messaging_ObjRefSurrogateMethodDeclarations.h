﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.ObjRefSurrogate
struct ObjRefSurrogate_t2024;
// System.Object
struct Object_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1066;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t2023;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Runtime.Remoting.Messaging.ObjRefSurrogate::.ctor()
 void ObjRefSurrogate__ctor_m11598 (ObjRefSurrogate_t2024 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Messaging.ObjRefSurrogate::SetObjectData(System.Object,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext,System.Runtime.Serialization.ISurrogateSelector)
 Object_t * ObjRefSurrogate_SetObjectData_m11599 (ObjRefSurrogate_t2024 * __this, Object_t * ___obj, SerializationInfo_t1066 * ___si, StreamingContext_t1067  ___sc, Object_t * ___selector, MethodInfo* method) IL2CPP_METHOD_ATTR;
