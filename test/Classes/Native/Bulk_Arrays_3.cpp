﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include "mscorlib_ArrayTypes.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo KeyValuePair_2U5BU5D_t3831_il2cpp_TypeInfo;


// System.Array
#include "mscorlib_System_Array.h"

// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>[]
static MethodInfo* KeyValuePair_2U5BU5D_t3831_MethodInfos[] =
{
	NULL
};
extern MethodInfo Object_Equals_m304_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo Object_GetHashCode_m305_MethodInfo;
extern MethodInfo Object_ToString_m306_MethodInfo;
extern MethodInfo Array_GetEnumerator_m7880_MethodInfo;
extern MethodInfo Array_System_Collections_ICollection_get_Count_m9576_MethodInfo;
extern MethodInfo Array_get_IsSynchronized_m9592_MethodInfo;
extern MethodInfo Array_get_SyncRoot_m9593_MethodInfo;
extern MethodInfo Array_CopyTo_m7879_MethodInfo;
extern MethodInfo Array_get_IsFixedSize_m9594_MethodInfo;
extern MethodInfo Array_get_IsReadOnly_m9595_MethodInfo;
extern MethodInfo Array_System_Collections_IList_get_Item_m9567_MethodInfo;
extern MethodInfo Array_System_Collections_IList_set_Item_m9568_MethodInfo;
extern MethodInfo Array_System_Collections_IList_Add_m9569_MethodInfo;
extern MethodInfo Array_System_Collections_IList_Clear_m9570_MethodInfo;
extern MethodInfo Array_System_Collections_IList_Contains_m9571_MethodInfo;
extern MethodInfo Array_System_Collections_IList_IndexOf_m9572_MethodInfo;
extern MethodInfo Array_System_Collections_IList_Insert_m9573_MethodInfo;
extern MethodInfo Array_System_Collections_IList_Remove_m9574_MethodInfo;
extern MethodInfo Array_System_Collections_IList_RemoveAt_m9575_MethodInfo;
extern MethodInfo Array_Clone_m7689_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_get_Count_m9577_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3832_m35406_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Clear_m9579_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3832_m35407_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3832_m35408_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3832_m35409_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t3832_m35410_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t3832_m35411_MethodInfo;
extern MethodInfo Array_InternalArray__RemoveAt_m9580_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t3832_m35405_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t3832_m35413_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3832_m35414_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t3831_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3832_m35406_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3832_m35407_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3832_m35408_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3832_m35409_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3832_m35410_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t3832_m35411_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t3832_m35405_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t3832_m35413_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3832_m35414_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8010_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8011_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8012_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7243_il2cpp_TypeInfo;
extern TypeInfo IList_1_t7244_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7245_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t2848_il2cpp_TypeInfo;
extern TypeInfo IList_1_t2851_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t2847_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t3831_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8010_il2cpp_TypeInfo,
	&IList_1_t8011_il2cpp_TypeInfo,
	&IEnumerable_1_t8012_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
extern TypeInfo ICloneable_t481_il2cpp_TypeInfo;
extern TypeInfo ICollection_t1206_il2cpp_TypeInfo;
extern TypeInfo IList_t1435_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t3831_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8010_il2cpp_TypeInfo, 21},
	{ &IList_1_t8011_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8012_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t3831_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t3831_1_0_0;
extern TypeInfo Array_t_il2cpp_TypeInfo;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"
extern TypeInfo KeyValuePair_2_t3832_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t3831_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t3831_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t3832_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t3831_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t3831_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t3831_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t3831_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t3831_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t3831_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t3832 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo DataSetImplU5BU5D_t3841_il2cpp_TypeInfo;



// Metadata Definition Vuforia.DataSetImpl[]
static MethodInfo* DataSetImplU5BU5D_t3841_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisDataSetImpl_t566_m35427_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisDataSetImpl_t566_m35428_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisDataSetImpl_t566_m35429_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisDataSetImpl_t566_m35430_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisDataSetImpl_t566_m35431_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisDataSetImpl_t566_m35432_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDataSetImpl_t566_m35426_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisDataSetImpl_t566_m35434_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisDataSetImpl_t566_m35435_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisDataSet_t568_m35438_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisDataSet_t568_m35439_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisDataSet_t568_m35440_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisDataSet_t568_m35441_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisDataSet_t568_m35442_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisDataSet_t568_m35443_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDataSet_t568_m35437_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisDataSet_t568_m35445_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisDataSet_t568_m35446_MethodInfo;
static MethodInfo* DataSetImplU5BU5D_t3841_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisDataSetImpl_t566_m35427_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisDataSetImpl_t566_m35428_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisDataSetImpl_t566_m35429_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisDataSetImpl_t566_m35430_MethodInfo,
	&Array_InternalArray__IndexOf_TisDataSetImpl_t566_m35431_MethodInfo,
	&Array_InternalArray__Insert_TisDataSetImpl_t566_m35432_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisDataSetImpl_t566_m35426_MethodInfo,
	&Array_InternalArray__set_Item_TisDataSetImpl_t566_m35434_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisDataSetImpl_t566_m35435_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisDataSet_t568_m35438_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisDataSet_t568_m35439_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisDataSet_t568_m35440_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisDataSet_t568_m35441_MethodInfo,
	&Array_InternalArray__IndexOf_TisDataSet_t568_m35442_MethodInfo,
	&Array_InternalArray__Insert_TisDataSet_t568_m35443_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisDataSet_t568_m35437_MethodInfo,
	&Array_InternalArray__set_Item_TisDataSet_t568_m35445_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisDataSet_t568_m35446_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t3844_il2cpp_TypeInfo;
extern TypeInfo IList_1_t3850_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t3842_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t3857_il2cpp_TypeInfo;
extern TypeInfo IList_1_t3862_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t617_il2cpp_TypeInfo;
static TypeInfo* DataSetImplU5BU5D_t3841_InterfacesTypeInfos[] = 
{
	&ICollection_1_t3844_il2cpp_TypeInfo,
	&IList_1_t3850_il2cpp_TypeInfo,
	&IEnumerable_1_t3842_il2cpp_TypeInfo,
	&ICollection_1_t3857_il2cpp_TypeInfo,
	&IList_1_t3862_il2cpp_TypeInfo,
	&IEnumerable_1_t617_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair DataSetImplU5BU5D_t3841_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t3844_il2cpp_TypeInfo, 21},
	{ &IList_1_t3850_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t3842_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t3857_il2cpp_TypeInfo, 34},
	{ &IList_1_t3862_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t617_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType DataSetImplU5BU5D_t3841_0_0_0;
extern Il2CppType DataSetImplU5BU5D_t3841_1_0_0;
struct DataSetImpl_t566;
extern TypeInfo DataSetImpl_t566_il2cpp_TypeInfo;
extern CustomAttributesCache DataSetImpl_t566__CustomAttributeCache_DataSetImpl_Load_m2867;
TypeInfo DataSetImplU5BU5D_t3841_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "DataSetImpl[]"/* name */
	, "Vuforia"/* namespaze */
	, DataSetImplU5BU5D_t3841_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &DataSetImpl_t566_il2cpp_TypeInfo/* element_class */
	, DataSetImplU5BU5D_t3841_InterfacesTypeInfos/* implemented_interfaces */
	, DataSetImplU5BU5D_t3841_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DataSetImplU5BU5D_t3841_il2cpp_TypeInfo/* cast_class */
	, &DataSetImplU5BU5D_t3841_0_0_0/* byval_arg */
	, &DataSetImplU5BU5D_t3841_1_0_0/* this_arg */
	, DataSetImplU5BU5D_t3841_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (DataSetImpl_t566 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo DataSetU5BU5D_t3856_il2cpp_TypeInfo;



// Metadata Definition Vuforia.DataSet[]
static MethodInfo* DataSetU5BU5D_t3856_MethodInfos[] =
{
	NULL
};
static MethodInfo* DataSetU5BU5D_t3856_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisDataSet_t568_m35438_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisDataSet_t568_m35439_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisDataSet_t568_m35440_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisDataSet_t568_m35441_MethodInfo,
	&Array_InternalArray__IndexOf_TisDataSet_t568_m35442_MethodInfo,
	&Array_InternalArray__Insert_TisDataSet_t568_m35443_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisDataSet_t568_m35437_MethodInfo,
	&Array_InternalArray__set_Item_TisDataSet_t568_m35445_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisDataSet_t568_m35446_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
static TypeInfo* DataSetU5BU5D_t3856_InterfacesTypeInfos[] = 
{
	&ICollection_1_t3857_il2cpp_TypeInfo,
	&IList_1_t3862_il2cpp_TypeInfo,
	&IEnumerable_1_t617_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair DataSetU5BU5D_t3856_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t3857_il2cpp_TypeInfo, 21},
	{ &IList_1_t3862_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t617_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 34},
	{ &IList_1_t2851_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 46},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType DataSetU5BU5D_t3856_0_0_0;
extern Il2CppType DataSetU5BU5D_t3856_1_0_0;
struct DataSet_t568;
extern TypeInfo DataSet_t568_il2cpp_TypeInfo;
extern CustomAttributesCache DataSet_t568__CustomAttributeCache_DataSet_Exists_m2783;
extern CustomAttributesCache DataSet_t568__CustomAttributeCache_DataSet_Load_m4538;
TypeInfo DataSetU5BU5D_t3856_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "DataSet[]"/* name */
	, "Vuforia"/* namespaze */
	, DataSetU5BU5D_t3856_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &DataSet_t568_il2cpp_TypeInfo/* element_class */
	, DataSetU5BU5D_t3856_InterfacesTypeInfos/* implemented_interfaces */
	, DataSetU5BU5D_t3856_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DataSetU5BU5D_t3856_il2cpp_TypeInfo/* cast_class */
	, &DataSetU5BU5D_t3856_0_0_0/* byval_arg */
	, &DataSetU5BU5D_t3856_1_0_0/* this_arg */
	, DataSetU5BU5D_t3856_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (DataSet_t568 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 47/* vtable_count */
	, 6/* interfaces_count */
	, 10/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t3873_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>[]
static MethodInfo* KeyValuePair_2U5BU5D_t3873_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3874_m35481_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3874_m35482_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3874_m35483_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3874_m35484_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t3874_m35485_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t3874_m35486_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t3874_m35480_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t3874_m35488_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3874_m35489_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t3873_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3874_m35481_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3874_m35482_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3874_m35483_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3874_m35484_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3874_m35485_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t3874_m35486_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t3874_m35480_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t3874_m35488_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3874_m35489_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8013_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8014_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8015_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t3873_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8013_il2cpp_TypeInfo,
	&IList_1_t8014_il2cpp_TypeInfo,
	&IEnumerable_1_t8015_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t3873_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8013_il2cpp_TypeInfo, 21},
	{ &IList_1_t8014_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8015_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t3873_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t3873_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"
extern TypeInfo KeyValuePair_2_t3874_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t3873_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t3873_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t3874_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t3873_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t3873_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t3873_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t3873_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t3873_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t3873_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t3874 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo MarkerU5BU5D_t3870_il2cpp_TypeInfo;



// Metadata Definition Vuforia.Marker[]
static MethodInfo* MarkerU5BU5D_t3870_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisMarker_t623_m35492_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisMarker_t623_m35493_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisMarker_t623_m35494_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisMarker_t623_m35495_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisMarker_t623_m35496_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisMarker_t623_m35497_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMarker_t623_m35491_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisMarker_t623_m35499_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisMarker_t623_m35500_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisTrackable_t550_m35304_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisTrackable_t550_m35305_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisTrackable_t550_m35306_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisTrackable_t550_m35307_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisTrackable_t550_m35308_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisTrackable_t550_m35309_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTrackable_t550_m35303_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisTrackable_t550_m35311_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisTrackable_t550_m35312_MethodInfo;
static MethodInfo* MarkerU5BU5D_t3870_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisMarker_t623_m35492_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisMarker_t623_m35493_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisMarker_t623_m35494_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisMarker_t623_m35495_MethodInfo,
	&Array_InternalArray__IndexOf_TisMarker_t623_m35496_MethodInfo,
	&Array_InternalArray__Insert_TisMarker_t623_m35497_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisMarker_t623_m35491_MethodInfo,
	&Array_InternalArray__set_Item_TisMarker_t623_m35499_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisMarker_t623_m35500_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTrackable_t550_m35304_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTrackable_t550_m35305_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTrackable_t550_m35306_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTrackable_t550_m35307_MethodInfo,
	&Array_InternalArray__IndexOf_TisTrackable_t550_m35308_MethodInfo,
	&Array_InternalArray__Insert_TisTrackable_t550_m35309_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTrackable_t550_m35303_MethodInfo,
	&Array_InternalArray__set_Item_TisTrackable_t550_m35311_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTrackable_t550_m35312_MethodInfo,
};
extern TypeInfo ICollection_1_t3887_il2cpp_TypeInfo;
extern TypeInfo IList_1_t3891_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t624_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t3815_il2cpp_TypeInfo;
extern TypeInfo IList_1_t3819_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t585_il2cpp_TypeInfo;
static TypeInfo* MarkerU5BU5D_t3870_InterfacesTypeInfos[] = 
{
	&ICollection_1_t3887_il2cpp_TypeInfo,
	&IList_1_t3891_il2cpp_TypeInfo,
	&IEnumerable_1_t624_il2cpp_TypeInfo,
	&ICollection_1_t3815_il2cpp_TypeInfo,
	&IList_1_t3819_il2cpp_TypeInfo,
	&IEnumerable_1_t585_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair MarkerU5BU5D_t3870_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t3887_il2cpp_TypeInfo, 21},
	{ &IList_1_t3891_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t624_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t3815_il2cpp_TypeInfo, 34},
	{ &IList_1_t3819_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t585_il2cpp_TypeInfo, 46},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType MarkerU5BU5D_t3870_0_0_0;
extern Il2CppType MarkerU5BU5D_t3870_1_0_0;
struct Marker_t623;
extern TypeInfo Marker_t623_il2cpp_TypeInfo;
TypeInfo MarkerU5BU5D_t3870_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "Marker[]"/* name */
	, "Vuforia"/* namespaze */
	, MarkerU5BU5D_t3870_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Marker_t623_il2cpp_TypeInfo/* element_class */
	, MarkerU5BU5D_t3870_InterfacesTypeInfos/* implemented_interfaces */
	, MarkerU5BU5D_t3870_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &MarkerU5BU5D_t3870_il2cpp_TypeInfo/* cast_class */
	, &MarkerU5BU5D_t3870_0_0_0/* byval_arg */
	, &MarkerU5BU5D_t3870_1_0_0/* this_arg */
	, MarkerU5BU5D_t3870_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 47/* vtable_count */
	, 6/* interfaces_count */
	, 10/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TrackableResultDataU5BU5D_t653_il2cpp_TypeInfo;



// Metadata Definition Vuforia.QCARManagerImpl/TrackableResultData[]
static MethodInfo* TrackableResultDataU5BU5D_t653_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisTrackableResultData_t639_m35528_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisTrackableResultData_t639_m35529_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisTrackableResultData_t639_m35530_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisTrackableResultData_t639_m35531_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisTrackableResultData_t639_m35532_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisTrackableResultData_t639_m35533_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTrackableResultData_t639_m35527_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisTrackableResultData_t639_m35535_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisTrackableResultData_t639_m35536_MethodInfo;
static MethodInfo* TrackableResultDataU5BU5D_t653_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTrackableResultData_t639_m35528_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTrackableResultData_t639_m35529_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTrackableResultData_t639_m35530_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTrackableResultData_t639_m35531_MethodInfo,
	&Array_InternalArray__IndexOf_TisTrackableResultData_t639_m35532_MethodInfo,
	&Array_InternalArray__Insert_TisTrackableResultData_t639_m35533_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTrackableResultData_t639_m35527_MethodInfo,
	&Array_InternalArray__set_Item_TisTrackableResultData_t639_m35535_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTrackableResultData_t639_m35536_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8016_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8017_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8018_il2cpp_TypeInfo;
static TypeInfo* TrackableResultDataU5BU5D_t653_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8016_il2cpp_TypeInfo,
	&IList_1_t8017_il2cpp_TypeInfo,
	&IEnumerable_1_t8018_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair TrackableResultDataU5BU5D_t653_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8016_il2cpp_TypeInfo, 21},
	{ &IList_1_t8017_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8018_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType TrackableResultDataU5BU5D_t653_0_0_0;
extern Il2CppType TrackableResultDataU5BU5D_t653_1_0_0;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"
extern TypeInfo TrackableResultData_t639_il2cpp_TypeInfo;
TypeInfo TrackableResultDataU5BU5D_t653_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackableResultData[]"/* name */
	, ""/* namespaze */
	, TrackableResultDataU5BU5D_t653_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TrackableResultData_t639_il2cpp_TypeInfo/* element_class */
	, TrackableResultDataU5BU5D_t653_InterfacesTypeInfos/* implemented_interfaces */
	, TrackableResultDataU5BU5D_t653_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TrackableResultDataU5BU5D_t653_il2cpp_TypeInfo/* cast_class */
	, &TrackableResultDataU5BU5D_t653_0_0_0/* byval_arg */
	, &TrackableResultDataU5BU5D_t653_1_0_0/* this_arg */
	, TrackableResultDataU5BU5D_t653_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (TrackableResultData_t639 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WordDataU5BU5D_t654_il2cpp_TypeInfo;



// Metadata Definition Vuforia.QCARManagerImpl/WordData[]
static MethodInfo* WordDataU5BU5D_t654_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisWordData_t644_m35539_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisWordData_t644_m35540_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisWordData_t644_m35541_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisWordData_t644_m35542_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisWordData_t644_m35543_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisWordData_t644_m35544_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWordData_t644_m35538_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisWordData_t644_m35546_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisWordData_t644_m35547_MethodInfo;
static MethodInfo* WordDataU5BU5D_t654_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisWordData_t644_m35539_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisWordData_t644_m35540_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisWordData_t644_m35541_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisWordData_t644_m35542_MethodInfo,
	&Array_InternalArray__IndexOf_TisWordData_t644_m35543_MethodInfo,
	&Array_InternalArray__Insert_TisWordData_t644_m35544_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisWordData_t644_m35538_MethodInfo,
	&Array_InternalArray__set_Item_TisWordData_t644_m35546_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisWordData_t644_m35547_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8019_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8020_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t700_il2cpp_TypeInfo;
static TypeInfo* WordDataU5BU5D_t654_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8019_il2cpp_TypeInfo,
	&IList_1_t8020_il2cpp_TypeInfo,
	&IEnumerable_1_t700_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair WordDataU5BU5D_t654_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8019_il2cpp_TypeInfo, 21},
	{ &IList_1_t8020_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t700_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WordDataU5BU5D_t654_0_0_0;
extern Il2CppType WordDataU5BU5D_t654_1_0_0;
// Vuforia.QCARManagerImpl/WordData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Wor_0.h"
extern TypeInfo WordData_t644_il2cpp_TypeInfo;
TypeInfo WordDataU5BU5D_t654_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WordData[]"/* name */
	, ""/* namespaze */
	, WordDataU5BU5D_t654_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WordData_t644_il2cpp_TypeInfo/* element_class */
	, WordDataU5BU5D_t654_InterfacesTypeInfos/* implemented_interfaces */
	, WordDataU5BU5D_t654_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WordDataU5BU5D_t654_il2cpp_TypeInfo/* cast_class */
	, &WordDataU5BU5D_t654_0_0_0/* byval_arg */
	, &WordDataU5BU5D_t654_1_0_0/* this_arg */
	, WordDataU5BU5D_t654_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (WordData_t644 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, true/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WordResultDataU5BU5D_t655_il2cpp_TypeInfo;



// Metadata Definition Vuforia.QCARManagerImpl/WordResultData[]
static MethodInfo* WordResultDataU5BU5D_t655_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisWordResultData_t643_m35550_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisWordResultData_t643_m35551_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisWordResultData_t643_m35552_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisWordResultData_t643_m35553_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisWordResultData_t643_m35554_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisWordResultData_t643_m35555_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWordResultData_t643_m35549_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisWordResultData_t643_m35557_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisWordResultData_t643_m35558_MethodInfo;
static MethodInfo* WordResultDataU5BU5D_t655_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisWordResultData_t643_m35550_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisWordResultData_t643_m35551_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisWordResultData_t643_m35552_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisWordResultData_t643_m35553_MethodInfo,
	&Array_InternalArray__IndexOf_TisWordResultData_t643_m35554_MethodInfo,
	&Array_InternalArray__Insert_TisWordResultData_t643_m35555_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisWordResultData_t643_m35549_MethodInfo,
	&Array_InternalArray__set_Item_TisWordResultData_t643_m35557_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisWordResultData_t643_m35558_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8021_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8022_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t701_il2cpp_TypeInfo;
static TypeInfo* WordResultDataU5BU5D_t655_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8021_il2cpp_TypeInfo,
	&IList_1_t8022_il2cpp_TypeInfo,
	&IEnumerable_1_t701_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair WordResultDataU5BU5D_t655_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8021_il2cpp_TypeInfo, 21},
	{ &IList_1_t8022_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t701_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WordResultDataU5BU5D_t655_0_0_0;
extern Il2CppType WordResultDataU5BU5D_t655_1_0_0;
// Vuforia.QCARManagerImpl/WordResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Wor.h"
extern TypeInfo WordResultData_t643_il2cpp_TypeInfo;
TypeInfo WordResultDataU5BU5D_t655_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WordResultData[]"/* name */
	, ""/* namespaze */
	, WordResultDataU5BU5D_t655_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WordResultData_t643_il2cpp_TypeInfo/* element_class */
	, WordResultDataU5BU5D_t655_InterfacesTypeInfos/* implemented_interfaces */
	, WordResultDataU5BU5D_t655_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WordResultDataU5BU5D_t655_il2cpp_TypeInfo/* cast_class */
	, &WordResultDataU5BU5D_t655_0_0_0/* byval_arg */
	, &WordResultDataU5BU5D_t655_1_0_0/* this_arg */
	, WordResultDataU5BU5D_t655_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (WordResultData_t643 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SmartTerrainRevisionDataU5BU5D_t671_il2cpp_TypeInfo;



// Metadata Definition Vuforia.QCARManagerImpl/SmartTerrainRevisionData[]
static MethodInfo* SmartTerrainRevisionDataU5BU5D_t671_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisSmartTerrainRevisionData_t647_m35576_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisSmartTerrainRevisionData_t647_m35577_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisSmartTerrainRevisionData_t647_m35578_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisSmartTerrainRevisionData_t647_m35579_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisSmartTerrainRevisionData_t647_m35580_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisSmartTerrainRevisionData_t647_m35581_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t647_m35575_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisSmartTerrainRevisionData_t647_m35583_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisSmartTerrainRevisionData_t647_m35584_MethodInfo;
static MethodInfo* SmartTerrainRevisionDataU5BU5D_t671_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisSmartTerrainRevisionData_t647_m35576_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisSmartTerrainRevisionData_t647_m35577_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisSmartTerrainRevisionData_t647_m35578_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisSmartTerrainRevisionData_t647_m35579_MethodInfo,
	&Array_InternalArray__IndexOf_TisSmartTerrainRevisionData_t647_m35580_MethodInfo,
	&Array_InternalArray__Insert_TisSmartTerrainRevisionData_t647_m35581_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisSmartTerrainRevisionData_t647_m35575_MethodInfo,
	&Array_InternalArray__set_Item_TisSmartTerrainRevisionData_t647_m35583_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisSmartTerrainRevisionData_t647_m35584_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8023_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8024_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8025_il2cpp_TypeInfo;
static TypeInfo* SmartTerrainRevisionDataU5BU5D_t671_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8023_il2cpp_TypeInfo,
	&IList_1_t8024_il2cpp_TypeInfo,
	&IEnumerable_1_t8025_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair SmartTerrainRevisionDataU5BU5D_t671_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8023_il2cpp_TypeInfo, 21},
	{ &IList_1_t8024_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8025_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType SmartTerrainRevisionDataU5BU5D_t671_0_0_0;
extern Il2CppType SmartTerrainRevisionDataU5BU5D_t671_1_0_0;
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Sma.h"
extern TypeInfo SmartTerrainRevisionData_t647_il2cpp_TypeInfo;
TypeInfo SmartTerrainRevisionDataU5BU5D_t671_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "SmartTerrainRevisionData[]"/* name */
	, ""/* namespaze */
	, SmartTerrainRevisionDataU5BU5D_t671_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SmartTerrainRevisionData_t647_il2cpp_TypeInfo/* element_class */
	, SmartTerrainRevisionDataU5BU5D_t671_InterfacesTypeInfos/* implemented_interfaces */
	, SmartTerrainRevisionDataU5BU5D_t671_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SmartTerrainRevisionDataU5BU5D_t671_il2cpp_TypeInfo/* cast_class */
	, &SmartTerrainRevisionDataU5BU5D_t671_0_0_0/* byval_arg */
	, &SmartTerrainRevisionDataU5BU5D_t671_1_0_0/* this_arg */
	, SmartTerrainRevisionDataU5BU5D_t671_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (SmartTerrainRevisionData_t647 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, true/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SurfaceDataU5BU5D_t672_il2cpp_TypeInfo;



// Metadata Definition Vuforia.QCARManagerImpl/SurfaceData[]
static MethodInfo* SurfaceDataU5BU5D_t672_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisSurfaceData_t648_m35587_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisSurfaceData_t648_m35588_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisSurfaceData_t648_m35589_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisSurfaceData_t648_m35590_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisSurfaceData_t648_m35591_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisSurfaceData_t648_m35592_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSurfaceData_t648_m35586_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisSurfaceData_t648_m35594_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisSurfaceData_t648_m35595_MethodInfo;
static MethodInfo* SurfaceDataU5BU5D_t672_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisSurfaceData_t648_m35587_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisSurfaceData_t648_m35588_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisSurfaceData_t648_m35589_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisSurfaceData_t648_m35590_MethodInfo,
	&Array_InternalArray__IndexOf_TisSurfaceData_t648_m35591_MethodInfo,
	&Array_InternalArray__Insert_TisSurfaceData_t648_m35592_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisSurfaceData_t648_m35586_MethodInfo,
	&Array_InternalArray__set_Item_TisSurfaceData_t648_m35594_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisSurfaceData_t648_m35595_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8026_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8027_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8028_il2cpp_TypeInfo;
static TypeInfo* SurfaceDataU5BU5D_t672_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8026_il2cpp_TypeInfo,
	&IList_1_t8027_il2cpp_TypeInfo,
	&IEnumerable_1_t8028_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair SurfaceDataU5BU5D_t672_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8026_il2cpp_TypeInfo, 21},
	{ &IList_1_t8027_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8028_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType SurfaceDataU5BU5D_t672_0_0_0;
extern Il2CppType SurfaceDataU5BU5D_t672_1_0_0;
// Vuforia.QCARManagerImpl/SurfaceData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Sur.h"
extern TypeInfo SurfaceData_t648_il2cpp_TypeInfo;
TypeInfo SurfaceDataU5BU5D_t672_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "SurfaceData[]"/* name */
	, ""/* namespaze */
	, SurfaceDataU5BU5D_t672_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SurfaceData_t648_il2cpp_TypeInfo/* element_class */
	, SurfaceDataU5BU5D_t672_InterfacesTypeInfos/* implemented_interfaces */
	, SurfaceDataU5BU5D_t672_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SurfaceDataU5BU5D_t672_il2cpp_TypeInfo/* cast_class */
	, &SurfaceDataU5BU5D_t672_0_0_0/* byval_arg */
	, &SurfaceDataU5BU5D_t672_1_0_0/* this_arg */
	, SurfaceDataU5BU5D_t672_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (SurfaceData_t648 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, true/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo PropDataU5BU5D_t673_il2cpp_TypeInfo;



// Metadata Definition Vuforia.QCARManagerImpl/PropData[]
static MethodInfo* PropDataU5BU5D_t673_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisPropData_t649_m35598_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisPropData_t649_m35599_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisPropData_t649_m35600_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisPropData_t649_m35601_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisPropData_t649_m35602_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisPropData_t649_m35603_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisPropData_t649_m35597_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisPropData_t649_m35605_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisPropData_t649_m35606_MethodInfo;
static MethodInfo* PropDataU5BU5D_t673_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisPropData_t649_m35598_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisPropData_t649_m35599_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisPropData_t649_m35600_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisPropData_t649_m35601_MethodInfo,
	&Array_InternalArray__IndexOf_TisPropData_t649_m35602_MethodInfo,
	&Array_InternalArray__Insert_TisPropData_t649_m35603_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisPropData_t649_m35597_MethodInfo,
	&Array_InternalArray__set_Item_TisPropData_t649_m35605_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisPropData_t649_m35606_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8029_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8030_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8031_il2cpp_TypeInfo;
static TypeInfo* PropDataU5BU5D_t673_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8029_il2cpp_TypeInfo,
	&IList_1_t8030_il2cpp_TypeInfo,
	&IEnumerable_1_t8031_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair PropDataU5BU5D_t673_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8029_il2cpp_TypeInfo, 21},
	{ &IList_1_t8030_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8031_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType PropDataU5BU5D_t673_0_0_0;
extern Il2CppType PropDataU5BU5D_t673_1_0_0;
// Vuforia.QCARManagerImpl/PropData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Pro.h"
extern TypeInfo PropData_t649_il2cpp_TypeInfo;
TypeInfo PropDataU5BU5D_t673_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropData[]"/* name */
	, ""/* namespaze */
	, PropDataU5BU5D_t673_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &PropData_t649_il2cpp_TypeInfo/* element_class */
	, PropDataU5BU5D_t673_InterfacesTypeInfos/* implemented_interfaces */
	, PropDataU5BU5D_t673_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &PropDataU5BU5D_t673_il2cpp_TypeInfo/* cast_class */
	, &PropDataU5BU5D_t673_0_0_0/* byval_arg */
	, &PropDataU5BU5D_t673_1_0_0/* this_arg */
	, PropDataU5BU5D_t673_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (PropData_t649 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, true/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo VideoBackgroundReflectionU5BU5D_t5535_il2cpp_TypeInfo;



// Metadata Definition Vuforia.QCARRenderer/VideoBackgroundReflection[]
static MethodInfo* VideoBackgroundReflectionU5BU5D_t5535_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisVideoBackgroundReflection_t659_m35611_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisVideoBackgroundReflection_t659_m35612_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisVideoBackgroundReflection_t659_m35613_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisVideoBackgroundReflection_t659_m35614_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisVideoBackgroundReflection_t659_m35615_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisVideoBackgroundReflection_t659_m35616_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVideoBackgroundReflection_t659_m35610_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisVideoBackgroundReflection_t659_m35618_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisVideoBackgroundReflection_t659_m35619_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo;
static MethodInfo* VideoBackgroundReflectionU5BU5D_t5535_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisVideoBackgroundReflection_t659_m35611_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisVideoBackgroundReflection_t659_m35612_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisVideoBackgroundReflection_t659_m35613_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisVideoBackgroundReflection_t659_m35614_MethodInfo,
	&Array_InternalArray__IndexOf_TisVideoBackgroundReflection_t659_m35615_MethodInfo,
	&Array_InternalArray__Insert_TisVideoBackgroundReflection_t659_m35616_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisVideoBackgroundReflection_t659_m35610_MethodInfo,
	&Array_InternalArray__set_Item_TisVideoBackgroundReflection_t659_m35618_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisVideoBackgroundReflection_t659_m35619_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8032_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8033_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8034_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7231_il2cpp_TypeInfo;
extern TypeInfo IList_1_t7232_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7233_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7234_il2cpp_TypeInfo;
extern TypeInfo IList_1_t7235_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7236_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7237_il2cpp_TypeInfo;
extern TypeInfo IList_1_t7238_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7239_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7240_il2cpp_TypeInfo;
extern TypeInfo IList_1_t7241_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7242_il2cpp_TypeInfo;
static TypeInfo* VideoBackgroundReflectionU5BU5D_t5535_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8032_il2cpp_TypeInfo,
	&IList_1_t8033_il2cpp_TypeInfo,
	&IEnumerable_1_t8034_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair VideoBackgroundReflectionU5BU5D_t5535_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8032_il2cpp_TypeInfo, 21},
	{ &IList_1_t8033_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8034_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType VideoBackgroundReflectionU5BU5D_t5535_0_0_0;
extern Il2CppType VideoBackgroundReflectionU5BU5D_t5535_1_0_0;
// Vuforia.QCARRenderer/VideoBackgroundReflection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB.h"
extern TypeInfo VideoBackgroundReflection_t659_il2cpp_TypeInfo;
extern TypeInfo Int32_t93_il2cpp_TypeInfo;
TypeInfo VideoBackgroundReflectionU5BU5D_t5535_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "VideoBackgroundReflection[]"/* name */
	, ""/* namespaze */
	, VideoBackgroundReflectionU5BU5D_t5535_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &VideoBackgroundReflection_t659_il2cpp_TypeInfo/* element_class */
	, VideoBackgroundReflectionU5BU5D_t5535_InterfacesTypeInfos/* implemented_interfaces */
	, VideoBackgroundReflectionU5BU5D_t5535_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &VideoBackgroundReflectionU5BU5D_t5535_0_0_0/* byval_arg */
	, &VideoBackgroundReflectionU5BU5D_t5535_1_0_0/* this_arg */
	, VideoBackgroundReflectionU5BU5D_t5535_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo RenderEventU5BU5D_t5536_il2cpp_TypeInfo;



// Metadata Definition Vuforia.QCARRendererImpl/RenderEvent[]
static MethodInfo* RenderEventU5BU5D_t5536_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisRenderEvent_t663_m35622_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisRenderEvent_t663_m35623_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisRenderEvent_t663_m35624_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisRenderEvent_t663_m35625_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisRenderEvent_t663_m35626_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisRenderEvent_t663_m35627_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRenderEvent_t663_m35621_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisRenderEvent_t663_m35629_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisRenderEvent_t663_m35630_MethodInfo;
static MethodInfo* RenderEventU5BU5D_t5536_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisRenderEvent_t663_m35622_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisRenderEvent_t663_m35623_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisRenderEvent_t663_m35624_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisRenderEvent_t663_m35625_MethodInfo,
	&Array_InternalArray__IndexOf_TisRenderEvent_t663_m35626_MethodInfo,
	&Array_InternalArray__Insert_TisRenderEvent_t663_m35627_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisRenderEvent_t663_m35621_MethodInfo,
	&Array_InternalArray__set_Item_TisRenderEvent_t663_m35629_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisRenderEvent_t663_m35630_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8035_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8036_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8037_il2cpp_TypeInfo;
static TypeInfo* RenderEventU5BU5D_t5536_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8035_il2cpp_TypeInfo,
	&IList_1_t8036_il2cpp_TypeInfo,
	&IEnumerable_1_t8037_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair RenderEventU5BU5D_t5536_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8035_il2cpp_TypeInfo, 21},
	{ &IList_1_t8036_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8037_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType RenderEventU5BU5D_t5536_0_0_0;
extern Il2CppType RenderEventU5BU5D_t5536_1_0_0;
// Vuforia.QCARRendererImpl/RenderEvent
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl_Re.h"
extern TypeInfo RenderEvent_t663_il2cpp_TypeInfo;
TypeInfo RenderEventU5BU5D_t5536_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderEvent[]"/* name */
	, ""/* namespaze */
	, RenderEventU5BU5D_t5536_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &RenderEvent_t663_il2cpp_TypeInfo/* element_class */
	, RenderEventU5BU5D_t5536_InterfacesTypeInfos/* implemented_interfaces */
	, RenderEventU5BU5D_t5536_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &RenderEventU5BU5D_t5536_0_0_0/* byval_arg */
	, &RenderEventU5BU5D_t5536_1_0_0/* this_arg */
	, RenderEventU5BU5D_t5536_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 261/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SmartTerrainTrackableU5BU5D_t3913_il2cpp_TypeInfo;



// Metadata Definition Vuforia.SmartTerrainTrackable[]
static MethodInfo* SmartTerrainTrackableU5BU5D_t3913_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisSmartTerrainTrackable_t581_m35633_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisSmartTerrainTrackable_t581_m35634_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisSmartTerrainTrackable_t581_m35635_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisSmartTerrainTrackable_t581_m35636_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisSmartTerrainTrackable_t581_m35637_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisSmartTerrainTrackable_t581_m35638_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSmartTerrainTrackable_t581_m35632_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisSmartTerrainTrackable_t581_m35640_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisSmartTerrainTrackable_t581_m35641_MethodInfo;
static MethodInfo* SmartTerrainTrackableU5BU5D_t3913_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisSmartTerrainTrackable_t581_m35633_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisSmartTerrainTrackable_t581_m35634_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisSmartTerrainTrackable_t581_m35635_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisSmartTerrainTrackable_t581_m35636_MethodInfo,
	&Array_InternalArray__IndexOf_TisSmartTerrainTrackable_t581_m35637_MethodInfo,
	&Array_InternalArray__Insert_TisSmartTerrainTrackable_t581_m35638_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisSmartTerrainTrackable_t581_m35632_MethodInfo,
	&Array_InternalArray__set_Item_TisSmartTerrainTrackable_t581_m35640_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisSmartTerrainTrackable_t581_m35641_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTrackable_t550_m35304_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTrackable_t550_m35305_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTrackable_t550_m35306_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTrackable_t550_m35307_MethodInfo,
	&Array_InternalArray__IndexOf_TisTrackable_t550_m35308_MethodInfo,
	&Array_InternalArray__Insert_TisTrackable_t550_m35309_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTrackable_t550_m35303_MethodInfo,
	&Array_InternalArray__set_Item_TisTrackable_t550_m35311_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTrackable_t550_m35312_MethodInfo,
};
extern TypeInfo ICollection_1_t3915_il2cpp_TypeInfo;
extern TypeInfo IList_1_t3921_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t667_il2cpp_TypeInfo;
static TypeInfo* SmartTerrainTrackableU5BU5D_t3913_InterfacesTypeInfos[] = 
{
	&ICollection_1_t3915_il2cpp_TypeInfo,
	&IList_1_t3921_il2cpp_TypeInfo,
	&IEnumerable_1_t667_il2cpp_TypeInfo,
	&ICollection_1_t3815_il2cpp_TypeInfo,
	&IList_1_t3819_il2cpp_TypeInfo,
	&IEnumerable_1_t585_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair SmartTerrainTrackableU5BU5D_t3913_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t3915_il2cpp_TypeInfo, 21},
	{ &IList_1_t3921_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t667_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t3815_il2cpp_TypeInfo, 34},
	{ &IList_1_t3819_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t585_il2cpp_TypeInfo, 46},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType SmartTerrainTrackableU5BU5D_t3913_0_0_0;
extern Il2CppType SmartTerrainTrackableU5BU5D_t3913_1_0_0;
struct SmartTerrainTrackable_t581;
extern TypeInfo SmartTerrainTrackable_t581_il2cpp_TypeInfo;
TypeInfo SmartTerrainTrackableU5BU5D_t3913_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "SmartTerrainTrackable[]"/* name */
	, "Vuforia"/* namespaze */
	, SmartTerrainTrackableU5BU5D_t3913_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SmartTerrainTrackable_t581_il2cpp_TypeInfo/* element_class */
	, SmartTerrainTrackableU5BU5D_t3913_InterfacesTypeInfos/* implemented_interfaces */
	, SmartTerrainTrackableU5BU5D_t3913_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SmartTerrainTrackableU5BU5D_t3913_il2cpp_TypeInfo/* cast_class */
	, &SmartTerrainTrackableU5BU5D_t3913_0_0_0/* byval_arg */
	, &SmartTerrainTrackableU5BU5D_t3913_1_0_0/* this_arg */
	, SmartTerrainTrackableU5BU5D_t3913_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 47/* vtable_count */
	, 6/* interfaces_count */
	, 10/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UpDirectionU5BU5D_t5537_il2cpp_TypeInfo;



// Metadata Definition Vuforia.TextTrackerImpl/UpDirection[]
static MethodInfo* UpDirectionU5BU5D_t5537_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisUpDirection_t679_m35676_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisUpDirection_t679_m35677_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisUpDirection_t679_m35678_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisUpDirection_t679_m35679_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisUpDirection_t679_m35680_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisUpDirection_t679_m35681_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUpDirection_t679_m35675_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisUpDirection_t679_m35683_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisUpDirection_t679_m35684_MethodInfo;
static MethodInfo* UpDirectionU5BU5D_t5537_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisUpDirection_t679_m35676_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisUpDirection_t679_m35677_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisUpDirection_t679_m35678_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisUpDirection_t679_m35679_MethodInfo,
	&Array_InternalArray__IndexOf_TisUpDirection_t679_m35680_MethodInfo,
	&Array_InternalArray__Insert_TisUpDirection_t679_m35681_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisUpDirection_t679_m35675_MethodInfo,
	&Array_InternalArray__set_Item_TisUpDirection_t679_m35683_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisUpDirection_t679_m35684_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8038_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8039_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8040_il2cpp_TypeInfo;
static TypeInfo* UpDirectionU5BU5D_t5537_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8038_il2cpp_TypeInfo,
	&IList_1_t8039_il2cpp_TypeInfo,
	&IEnumerable_1_t8040_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair UpDirectionU5BU5D_t5537_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8038_il2cpp_TypeInfo, 21},
	{ &IList_1_t8039_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8040_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType UpDirectionU5BU5D_t5537_0_0_0;
extern Il2CppType UpDirectionU5BU5D_t5537_1_0_0;
// Vuforia.TextTrackerImpl/UpDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrackerImpl_UpD.h"
extern TypeInfo UpDirection_t679_il2cpp_TypeInfo;
TypeInfo UpDirectionU5BU5D_t5537_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "UpDirection[]"/* name */
	, ""/* namespaze */
	, UpDirectionU5BU5D_t5537_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UpDirection_t679_il2cpp_TypeInfo/* element_class */
	, UpDirectionU5BU5D_t5537_InterfacesTypeInfos/* implemented_interfaces */
	, UpDirectionU5BU5D_t5537_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &UpDirectionU5BU5D_t5537_0_0_0/* byval_arg */
	, &UpDirectionU5BU5D_t5537_1_0_0/* this_arg */
	, UpDirectionU5BU5D_t5537_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t3940_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
static MethodInfo* KeyValuePair_2U5BU5D_t3940_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3941_m35687_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3941_m35688_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3941_m35689_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3941_m35690_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t3941_m35691_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t3941_m35692_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t3941_m35686_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t3941_m35694_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3941_m35695_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t3940_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3941_m35687_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3941_m35688_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3941_m35689_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3941_m35690_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3941_m35691_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t3941_m35692_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t3941_m35686_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t3941_m35694_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3941_m35695_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8041_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8042_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8043_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t3940_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8041_il2cpp_TypeInfo,
	&IList_1_t8042_il2cpp_TypeInfo,
	&IEnumerable_1_t8043_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t3940_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8041_il2cpp_TypeInfo, 21},
	{ &IList_1_t8042_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8043_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t3940_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t3940_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13.h"
extern TypeInfo KeyValuePair_2_t3941_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t3940_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t3940_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t3941_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t3940_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t3940_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t3940_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t3940_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t3940_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t3940_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t3941 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UInt16U5BU5D_t1337_il2cpp_TypeInfo;



// Metadata Definition System.UInt16[]
static MethodInfo* UInt16U5BU5D_t1337_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisUInt16_t823_m35698_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisUInt16_t823_m35699_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisUInt16_t823_m35700_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisUInt16_t823_m35701_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisUInt16_t823_m35702_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisUInt16_t823_m35703_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUInt16_t823_m35697_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisUInt16_t823_m35705_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisUInt16_t823_m35706_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisIComparable_1_t2308_m35709_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisIComparable_1_t2308_m35710_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisIComparable_1_t2308_m35711_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisIComparable_1_t2308_m35712_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisIComparable_1_t2308_m35713_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisIComparable_1_t2308_m35714_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIComparable_1_t2308_m35708_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisIComparable_1_t2308_m35716_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_1_t2308_m35717_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisIEquatable_1_t2309_m35720_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisIEquatable_1_t2309_m35721_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisIEquatable_1_t2309_m35722_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisIEquatable_1_t2309_m35723_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisIEquatable_1_t2309_m35724_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisIEquatable_1_t2309_m35725_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEquatable_1_t2309_m35719_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisIEquatable_1_t2309_m35727_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisIEquatable_1_t2309_m35728_MethodInfo;
static MethodInfo* UInt16U5BU5D_t1337_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisUInt16_t823_m35698_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisUInt16_t823_m35699_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisUInt16_t823_m35700_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisUInt16_t823_m35701_MethodInfo,
	&Array_InternalArray__IndexOf_TisUInt16_t823_m35702_MethodInfo,
	&Array_InternalArray__Insert_TisUInt16_t823_m35703_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisUInt16_t823_m35697_MethodInfo,
	&Array_InternalArray__set_Item_TisUInt16_t823_m35705_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisUInt16_t823_m35706_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_1_t2308_m35709_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_1_t2308_m35710_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_1_t2308_m35711_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_1_t2308_m35712_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_1_t2308_m35713_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_1_t2308_m35714_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_1_t2308_m35708_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_1_t2308_m35716_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_1_t2308_m35717_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIEquatable_1_t2309_m35720_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIEquatable_1_t2309_m35721_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIEquatable_1_t2309_m35722_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIEquatable_1_t2309_m35723_MethodInfo,
	&Array_InternalArray__IndexOf_TisIEquatable_1_t2309_m35724_MethodInfo,
	&Array_InternalArray__Insert_TisIEquatable_1_t2309_m35725_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIEquatable_1_t2309_m35719_MethodInfo,
	&Array_InternalArray__set_Item_TisIEquatable_1_t2309_m35727_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIEquatable_1_t2309_m35728_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8044_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8045_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8046_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t8047_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8048_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8049_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t8050_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8051_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8052_il2cpp_TypeInfo;
static TypeInfo* UInt16U5BU5D_t1337_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8044_il2cpp_TypeInfo,
	&IList_1_t8045_il2cpp_TypeInfo,
	&IEnumerable_1_t8046_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t8047_il2cpp_TypeInfo,
	&IList_1_t8048_il2cpp_TypeInfo,
	&IEnumerable_1_t8049_il2cpp_TypeInfo,
	&ICollection_1_t8050_il2cpp_TypeInfo,
	&IList_1_t8051_il2cpp_TypeInfo,
	&IEnumerable_1_t8052_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair UInt16U5BU5D_t1337_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8044_il2cpp_TypeInfo, 21},
	{ &IList_1_t8045_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8046_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 34},
	{ &IList_1_t7235_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 47},
	{ &IList_1_t7238_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 60},
	{ &IList_1_t7241_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t8047_il2cpp_TypeInfo, 73},
	{ &IList_1_t8048_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t8049_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t8050_il2cpp_TypeInfo, 86},
	{ &IList_1_t8051_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t8052_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 99},
	{ &IList_1_t7244_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 111},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 112},
	{ &IList_1_t2851_il2cpp_TypeInfo, 119},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 124},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType UInt16U5BU5D_t1337_0_0_0;
extern Il2CppType UInt16U5BU5D_t1337_1_0_0;
// System.UInt16
#include "mscorlib_System_UInt16.h"
extern TypeInfo UInt16_t823_il2cpp_TypeInfo;
extern CustomAttributesCache UInt16_t823__CustomAttributeCache;
extern CustomAttributesCache UInt16_t823__CustomAttributeCache_UInt16_Parse_m9191;
extern CustomAttributesCache UInt16_t823__CustomAttributeCache_UInt16_Parse_m9192;
extern CustomAttributesCache UInt16_t823__CustomAttributeCache_UInt16_TryParse_m9193;
extern CustomAttributesCache UInt16_t823__CustomAttributeCache_UInt16_TryParse_m9194;
TypeInfo UInt16U5BU5D_t1337_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "UInt16[]"/* name */
	, "System"/* namespaze */
	, UInt16U5BU5D_t1337_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UInt16_t823_il2cpp_TypeInfo/* element_class */
	, UInt16U5BU5D_t1337_InterfacesTypeInfos/* implemented_interfaces */
	, UInt16U5BU5D_t1337_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UInt16U5BU5D_t1337_il2cpp_TypeInfo/* cast_class */
	, &UInt16U5BU5D_t1337_0_0_0/* byval_arg */
	, &UInt16U5BU5D_t1337_1_0_0/* this_arg */
	, UInt16U5BU5D_t1337_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (uint16_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, true/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 125/* vtable_count */
	, 24/* interfaces_count */
	, 28/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IComparable_1U5BU5D_t5231_il2cpp_TypeInfo;



// Metadata Definition System.IComparable`1<System.UInt16>[]
static MethodInfo* IComparable_1U5BU5D_t5231_MethodInfos[] =
{
	NULL
};
static MethodInfo* IComparable_1U5BU5D_t5231_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_1_t2308_m35709_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_1_t2308_m35710_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_1_t2308_m35711_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_1_t2308_m35712_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_1_t2308_m35713_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_1_t2308_m35714_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_1_t2308_m35708_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_1_t2308_m35716_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_1_t2308_m35717_MethodInfo,
};
static TypeInfo* IComparable_1U5BU5D_t5231_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8047_il2cpp_TypeInfo,
	&IList_1_t8048_il2cpp_TypeInfo,
	&IEnumerable_1_t8049_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair IComparable_1U5BU5D_t5231_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8047_il2cpp_TypeInfo, 21},
	{ &IList_1_t8048_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8049_il2cpp_TypeInfo, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1U5BU5D_t5231_0_0_0;
extern Il2CppType IComparable_1U5BU5D_t5231_1_0_0;
struct IComparable_1_t2308;
extern TypeInfo IComparable_1_t2308_il2cpp_TypeInfo;
TypeInfo IComparable_1U5BU5D_t5231_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1[]"/* name */
	, "System"/* namespaze */
	, IComparable_1U5BU5D_t5231_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t2308_il2cpp_TypeInfo/* element_class */
	, IComparable_1U5BU5D_t5231_InterfacesTypeInfos/* implemented_interfaces */
	, IComparable_1U5BU5D_t5231_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1U5BU5D_t5231_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1U5BU5D_t5231_0_0_0/* byval_arg */
	, &IComparable_1U5BU5D_t5231_1_0_0/* this_arg */
	, IComparable_1U5BU5D_t5231_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t*)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 3/* interfaces_count */
	, 7/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEquatable_1U5BU5D_t5232_il2cpp_TypeInfo;



// Metadata Definition System.IEquatable`1<System.UInt16>[]
static MethodInfo* IEquatable_1U5BU5D_t5232_MethodInfos[] =
{
	NULL
};
static MethodInfo* IEquatable_1U5BU5D_t5232_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIEquatable_1_t2309_m35720_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIEquatable_1_t2309_m35721_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIEquatable_1_t2309_m35722_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIEquatable_1_t2309_m35723_MethodInfo,
	&Array_InternalArray__IndexOf_TisIEquatable_1_t2309_m35724_MethodInfo,
	&Array_InternalArray__Insert_TisIEquatable_1_t2309_m35725_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIEquatable_1_t2309_m35719_MethodInfo,
	&Array_InternalArray__set_Item_TisIEquatable_1_t2309_m35727_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIEquatable_1_t2309_m35728_MethodInfo,
};
static TypeInfo* IEquatable_1U5BU5D_t5232_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8050_il2cpp_TypeInfo,
	&IList_1_t8051_il2cpp_TypeInfo,
	&IEnumerable_1_t8052_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair IEquatable_1U5BU5D_t5232_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8050_il2cpp_TypeInfo, 21},
	{ &IList_1_t8051_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8052_il2cpp_TypeInfo, 33},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEquatable_1U5BU5D_t5232_0_0_0;
extern Il2CppType IEquatable_1U5BU5D_t5232_1_0_0;
struct IEquatable_1_t2309;
extern TypeInfo IEquatable_1_t2309_il2cpp_TypeInfo;
TypeInfo IEquatable_1U5BU5D_t5232_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1[]"/* name */
	, "System"/* namespaze */
	, IEquatable_1U5BU5D_t5232_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEquatable_1_t2309_il2cpp_TypeInfo/* element_class */
	, IEquatable_1U5BU5D_t5232_InterfacesTypeInfos/* implemented_interfaces */
	, IEquatable_1U5BU5D_t5232_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEquatable_1U5BU5D_t5232_il2cpp_TypeInfo/* cast_class */
	, &IEquatable_1U5BU5D_t5232_0_0_0/* byval_arg */
	, &IEquatable_1U5BU5D_t5232_1_0_0/* this_arg */
	, IEquatable_1U5BU5D_t5232_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t*)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 3/* interfaces_count */
	, 7/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo RectangleDataU5BU5D_t685_il2cpp_TypeInfo;



// Metadata Definition Vuforia.RectangleData[]
static MethodInfo* RectangleDataU5BU5D_t685_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisRectangleData_t588_m35741_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisRectangleData_t588_m35742_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisRectangleData_t588_m35743_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisRectangleData_t588_m35744_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisRectangleData_t588_m35745_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisRectangleData_t588_m35746_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRectangleData_t588_m35740_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisRectangleData_t588_m35748_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisRectangleData_t588_m35749_MethodInfo;
static MethodInfo* RectangleDataU5BU5D_t685_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisRectangleData_t588_m35741_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisRectangleData_t588_m35742_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisRectangleData_t588_m35743_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisRectangleData_t588_m35744_MethodInfo,
	&Array_InternalArray__IndexOf_TisRectangleData_t588_m35745_MethodInfo,
	&Array_InternalArray__Insert_TisRectangleData_t588_m35746_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisRectangleData_t588_m35740_MethodInfo,
	&Array_InternalArray__set_Item_TisRectangleData_t588_m35748_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisRectangleData_t588_m35749_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8053_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8054_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8055_il2cpp_TypeInfo;
static TypeInfo* RectangleDataU5BU5D_t685_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8053_il2cpp_TypeInfo,
	&IList_1_t8054_il2cpp_TypeInfo,
	&IEnumerable_1_t8055_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair RectangleDataU5BU5D_t685_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8053_il2cpp_TypeInfo, 21},
	{ &IList_1_t8054_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8055_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType RectangleDataU5BU5D_t685_0_0_0;
extern Il2CppType RectangleDataU5BU5D_t685_1_0_0;
// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"
extern TypeInfo RectangleData_t588_il2cpp_TypeInfo;
TypeInfo RectangleDataU5BU5D_t685_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "RectangleData[]"/* name */
	, "Vuforia"/* namespaze */
	, RectangleDataU5BU5D_t685_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &RectangleData_t588_il2cpp_TypeInfo/* element_class */
	, RectangleDataU5BU5D_t685_InterfacesTypeInfos/* implemented_interfaces */
	, RectangleDataU5BU5D_t685_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &RectangleDataU5BU5D_t685_il2cpp_TypeInfo/* cast_class */
	, &RectangleDataU5BU5D_t685_0_0_0/* byval_arg */
	, &RectangleDataU5BU5D_t685_1_0_0/* this_arg */
	, RectangleDataU5BU5D_t685_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (RectangleData_t588 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, true/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WordPrefabCreationModeU5BU5D_t5538_il2cpp_TypeInfo;



// Metadata Definition Vuforia.WordPrefabCreationMode[]
static MethodInfo* WordPrefabCreationModeU5BU5D_t5538_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisWordPrefabCreationMode_t687_m35752_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisWordPrefabCreationMode_t687_m35753_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisWordPrefabCreationMode_t687_m35754_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisWordPrefabCreationMode_t687_m35755_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisWordPrefabCreationMode_t687_m35756_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisWordPrefabCreationMode_t687_m35757_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWordPrefabCreationMode_t687_m35751_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisWordPrefabCreationMode_t687_m35759_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisWordPrefabCreationMode_t687_m35760_MethodInfo;
static MethodInfo* WordPrefabCreationModeU5BU5D_t5538_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisWordPrefabCreationMode_t687_m35752_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisWordPrefabCreationMode_t687_m35753_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisWordPrefabCreationMode_t687_m35754_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisWordPrefabCreationMode_t687_m35755_MethodInfo,
	&Array_InternalArray__IndexOf_TisWordPrefabCreationMode_t687_m35756_MethodInfo,
	&Array_InternalArray__Insert_TisWordPrefabCreationMode_t687_m35757_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisWordPrefabCreationMode_t687_m35751_MethodInfo,
	&Array_InternalArray__set_Item_TisWordPrefabCreationMode_t687_m35759_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisWordPrefabCreationMode_t687_m35760_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8056_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8057_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8058_il2cpp_TypeInfo;
static TypeInfo* WordPrefabCreationModeU5BU5D_t5538_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8056_il2cpp_TypeInfo,
	&IList_1_t8057_il2cpp_TypeInfo,
	&IEnumerable_1_t8058_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair WordPrefabCreationModeU5BU5D_t5538_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8056_il2cpp_TypeInfo, 21},
	{ &IList_1_t8057_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8058_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WordPrefabCreationModeU5BU5D_t5538_0_0_0;
extern Il2CppType WordPrefabCreationModeU5BU5D_t5538_1_0_0;
// Vuforia.WordPrefabCreationMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordPrefabCreationM.h"
extern TypeInfo WordPrefabCreationMode_t687_il2cpp_TypeInfo;
TypeInfo WordPrefabCreationModeU5BU5D_t5538_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WordPrefabCreationMode[]"/* name */
	, "Vuforia"/* namespaze */
	, WordPrefabCreationModeU5BU5D_t5538_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WordPrefabCreationMode_t687_il2cpp_TypeInfo/* element_class */
	, WordPrefabCreationModeU5BU5D_t5538_InterfacesTypeInfos/* implemented_interfaces */
	, WordPrefabCreationModeU5BU5D_t5538_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &WordPrefabCreationModeU5BU5D_t5538_0_0_0/* byval_arg */
	, &WordPrefabCreationModeU5BU5D_t5538_1_0_0/* this_arg */
	, WordPrefabCreationModeU5BU5D_t5538_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t3962_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>[]
static MethodInfo* KeyValuePair_2U5BU5D_t3962_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3963_m35763_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3963_m35764_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3963_m35765_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3963_m35766_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t3963_m35767_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t3963_m35768_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t3963_m35762_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t3963_m35770_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3963_m35771_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t3962_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t3963_m35763_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t3963_m35764_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t3963_m35765_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t3963_m35766_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t3963_m35767_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t3963_m35768_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t3963_m35762_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t3963_m35770_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t3963_m35771_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8059_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8060_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8061_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t3962_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8059_il2cpp_TypeInfo,
	&IList_1_t8060_il2cpp_TypeInfo,
	&IEnumerable_1_t8061_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t3962_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8059_il2cpp_TypeInfo, 21},
	{ &IList_1_t8060_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8061_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t3962_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t3962_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
extern TypeInfo KeyValuePair_2_t3963_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t3962_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t3962_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t3963_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t3962_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t3962_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t3962_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t3962_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t3962_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t3962_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t3963 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WordResultU5BU5D_t3960_il2cpp_TypeInfo;



// Metadata Definition Vuforia.WordResult[]
static MethodInfo* WordResultU5BU5D_t3960_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisWordResult_t702_m35774_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisWordResult_t702_m35775_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisWordResult_t702_m35776_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisWordResult_t702_m35777_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisWordResult_t702_m35778_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisWordResult_t702_m35779_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWordResult_t702_m35773_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisWordResult_t702_m35781_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisWordResult_t702_m35782_MethodInfo;
static MethodInfo* WordResultU5BU5D_t3960_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisWordResult_t702_m35774_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisWordResult_t702_m35775_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisWordResult_t702_m35776_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisWordResult_t702_m35777_MethodInfo,
	&Array_InternalArray__IndexOf_TisWordResult_t702_m35778_MethodInfo,
	&Array_InternalArray__Insert_TisWordResult_t702_m35779_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisWordResult_t702_m35773_MethodInfo,
	&Array_InternalArray__set_Item_TisWordResult_t702_m35781_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisWordResult_t702_m35782_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t3976_il2cpp_TypeInfo;
extern TypeInfo IList_1_t3980_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t689_il2cpp_TypeInfo;
static TypeInfo* WordResultU5BU5D_t3960_InterfacesTypeInfos[] = 
{
	&ICollection_1_t3976_il2cpp_TypeInfo,
	&IList_1_t3980_il2cpp_TypeInfo,
	&IEnumerable_1_t689_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair WordResultU5BU5D_t3960_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t3976_il2cpp_TypeInfo, 21},
	{ &IList_1_t3980_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t689_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 34},
	{ &IList_1_t2851_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 46},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WordResultU5BU5D_t3960_0_0_0;
extern Il2CppType WordResultU5BU5D_t3960_1_0_0;
struct WordResult_t702;
extern TypeInfo WordResult_t702_il2cpp_TypeInfo;
TypeInfo WordResultU5BU5D_t3960_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WordResult[]"/* name */
	, "Vuforia"/* namespaze */
	, WordResultU5BU5D_t3960_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WordResult_t702_il2cpp_TypeInfo/* element_class */
	, WordResultU5BU5D_t3960_InterfacesTypeInfos/* implemented_interfaces */
	, WordResultU5BU5D_t3960_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WordResultU5BU5D_t3960_il2cpp_TypeInfo/* cast_class */
	, &WordResultU5BU5D_t3960_0_0_0/* byval_arg */
	, &WordResultU5BU5D_t3960_1_0_0/* this_arg */
	, WordResultU5BU5D_t3960_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (WordResult_t702 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 47/* vtable_count */
	, 6/* interfaces_count */
	, 10/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WordU5BU5D_t3984_il2cpp_TypeInfo;



// Metadata Definition Vuforia.Word[]
static MethodInfo* WordU5BU5D_t3984_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisWord_t691_m35810_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisWord_t691_m35811_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisWord_t691_m35812_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisWord_t691_m35813_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisWord_t691_m35814_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisWord_t691_m35815_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWord_t691_m35809_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisWord_t691_m35817_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisWord_t691_m35818_MethodInfo;
static MethodInfo* WordU5BU5D_t3984_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisWord_t691_m35810_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisWord_t691_m35811_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisWord_t691_m35812_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisWord_t691_m35813_MethodInfo,
	&Array_InternalArray__IndexOf_TisWord_t691_m35814_MethodInfo,
	&Array_InternalArray__Insert_TisWord_t691_m35815_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisWord_t691_m35809_MethodInfo,
	&Array_InternalArray__set_Item_TisWord_t691_m35817_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisWord_t691_m35818_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTrackable_t550_m35304_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTrackable_t550_m35305_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTrackable_t550_m35306_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTrackable_t550_m35307_MethodInfo,
	&Array_InternalArray__IndexOf_TisTrackable_t550_m35308_MethodInfo,
	&Array_InternalArray__Insert_TisTrackable_t550_m35309_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTrackable_t550_m35303_MethodInfo,
	&Array_InternalArray__set_Item_TisTrackable_t550_m35311_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTrackable_t550_m35312_MethodInfo,
};
extern TypeInfo ICollection_1_t3985_il2cpp_TypeInfo;
extern TypeInfo IList_1_t3990_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t690_il2cpp_TypeInfo;
static TypeInfo* WordU5BU5D_t3984_InterfacesTypeInfos[] = 
{
	&ICollection_1_t3985_il2cpp_TypeInfo,
	&IList_1_t3990_il2cpp_TypeInfo,
	&IEnumerable_1_t690_il2cpp_TypeInfo,
	&ICollection_1_t3815_il2cpp_TypeInfo,
	&IList_1_t3819_il2cpp_TypeInfo,
	&IEnumerable_1_t585_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair WordU5BU5D_t3984_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t3985_il2cpp_TypeInfo, 21},
	{ &IList_1_t3990_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t690_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t3815_il2cpp_TypeInfo, 34},
	{ &IList_1_t3819_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t585_il2cpp_TypeInfo, 46},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WordU5BU5D_t3984_0_0_0;
extern Il2CppType WordU5BU5D_t3984_1_0_0;
struct Word_t691;
extern TypeInfo Word_t691_il2cpp_TypeInfo;
TypeInfo WordU5BU5D_t3984_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "Word[]"/* name */
	, "Vuforia"/* namespaze */
	, WordU5BU5D_t3984_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Word_t691_il2cpp_TypeInfo/* element_class */
	, WordU5BU5D_t3984_InterfacesTypeInfos/* implemented_interfaces */
	, WordU5BU5D_t3984_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WordU5BU5D_t3984_il2cpp_TypeInfo/* cast_class */
	, &WordU5BU5D_t3984_0_0_0/* byval_arg */
	, &WordU5BU5D_t3984_1_0_0/* this_arg */
	, WordU5BU5D_t3984_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 47/* vtable_count */
	, 6/* interfaces_count */
	, 10/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t3998_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>[]
static MethodInfo* KeyValuePair_2U5BU5D_t3998_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t834_m35836_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t834_m35837_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t834_m35838_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t834_m35839_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t834_m35840_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t834_m35841_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t834_m35835_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t834_m35843_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t834_m35844_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t3998_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t834_m35836_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t834_m35837_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t834_m35838_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t834_m35839_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t834_m35840_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t834_m35841_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t834_m35835_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t834_m35843_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t834_m35844_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8062_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8063_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8064_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t3998_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8062_il2cpp_TypeInfo,
	&IList_1_t8063_il2cpp_TypeInfo,
	&IEnumerable_1_t8064_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t3998_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8062_il2cpp_TypeInfo, 21},
	{ &IList_1_t8063_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8064_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t3998_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t3998_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"
extern TypeInfo KeyValuePair_2_t834_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t3998_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t3998_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t834_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t3998_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t3998_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t3998_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t3998_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t3998_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t3998_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t834 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t4018_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
static MethodInfo* KeyValuePair_2U5BU5D_t4018_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4019_m35872_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4019_m35873_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4019_m35874_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4019_m35875_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t4019_m35876_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t4019_m35877_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t4019_m35871_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t4019_m35879_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4019_m35880_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t4018_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4019_m35872_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4019_m35873_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4019_m35874_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4019_m35875_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t4019_m35876_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t4019_m35877_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t4019_m35871_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t4019_m35879_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4019_m35880_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8065_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8066_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8067_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t4018_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8065_il2cpp_TypeInfo,
	&IList_1_t8066_il2cpp_TypeInfo,
	&IEnumerable_1_t8067_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t4018_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8065_il2cpp_TypeInfo, 21},
	{ &IList_1_t8066_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8067_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t4018_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t4018_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_15.h"
extern TypeInfo KeyValuePair_2_t4019_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t4018_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t4018_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t4019_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t4018_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t4018_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t4018_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t4018_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t4018_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t4018_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t4019 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo List_1U5BU5D_t4016_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
static MethodInfo* List_1U5BU5D_t4016_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisList_1_t697_m35883_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisList_1_t697_m35884_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisList_1_t697_m35885_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisList_1_t697_m35886_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisList_1_t697_m35887_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisList_1_t697_m35888_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisList_1_t697_m35882_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisList_1_t697_m35890_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisList_1_t697_m35891_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisIEnumerable_t1126_m31618_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisIEnumerable_t1126_m31619_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisIEnumerable_t1126_m31620_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisIEnumerable_t1126_m31621_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisIEnumerable_t1126_m31622_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisIEnumerable_t1126_m31623_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEnumerable_t1126_m31617_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisIEnumerable_t1126_m31625_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisIEnumerable_t1126_m31626_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisICollection_t1206_m33056_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisICollection_t1206_m33057_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisICollection_t1206_m33058_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisICollection_t1206_m33059_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisICollection_t1206_m33060_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisICollection_t1206_m33061_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisICollection_t1206_m33055_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisICollection_t1206_m33063_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisICollection_t1206_m33064_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisIList_t1435_m33067_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisIList_t1435_m33068_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisIList_t1435_m33069_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisIList_t1435_m33070_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisIList_t1435_m33071_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisIList_t1435_m33072_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIList_t1435_m33066_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisIList_t1435_m33074_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisIList_t1435_m33075_MethodInfo;
static MethodInfo* List_1U5BU5D_t4016_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisList_1_t697_m35883_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisList_1_t697_m35884_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisList_1_t697_m35885_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisList_1_t697_m35886_MethodInfo,
	&Array_InternalArray__IndexOf_TisList_1_t697_m35887_MethodInfo,
	&Array_InternalArray__Insert_TisList_1_t697_m35888_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisList_1_t697_m35882_MethodInfo,
	&Array_InternalArray__set_Item_TisList_1_t697_m35890_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisList_1_t697_m35891_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIEnumerable_t1126_m31618_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIEnumerable_t1126_m31619_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIEnumerable_t1126_m31620_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIEnumerable_t1126_m31621_MethodInfo,
	&Array_InternalArray__IndexOf_TisIEnumerable_t1126_m31622_MethodInfo,
	&Array_InternalArray__Insert_TisIEnumerable_t1126_m31623_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIEnumerable_t1126_m31617_MethodInfo,
	&Array_InternalArray__set_Item_TisIEnumerable_t1126_m31625_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIEnumerable_t1126_m31626_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisICollection_t1206_m33056_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisICollection_t1206_m33057_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisICollection_t1206_m33058_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisICollection_t1206_m33059_MethodInfo,
	&Array_InternalArray__IndexOf_TisICollection_t1206_m33060_MethodInfo,
	&Array_InternalArray__Insert_TisICollection_t1206_m33061_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisICollection_t1206_m33055_MethodInfo,
	&Array_InternalArray__set_Item_TisICollection_t1206_m33063_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisICollection_t1206_m33064_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIList_t1435_m33067_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIList_t1435_m33068_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIList_t1435_m33069_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIList_t1435_m33070_MethodInfo,
	&Array_InternalArray__IndexOf_TisIList_t1435_m33071_MethodInfo,
	&Array_InternalArray__Insert_TisIList_t1435_m33072_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIList_t1435_m33066_MethodInfo,
	&Array_InternalArray__set_Item_TisIList_t1435_m33074_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIList_t1435_m33075_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8068_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8069_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8070_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7255_il2cpp_TypeInfo;
extern TypeInfo IList_1_t7256_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7257_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7584_il2cpp_TypeInfo;
extern TypeInfo IList_1_t7585_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7586_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7587_il2cpp_TypeInfo;
extern TypeInfo IList_1_t7588_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7589_il2cpp_TypeInfo;
static TypeInfo* List_1U5BU5D_t4016_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8068_il2cpp_TypeInfo,
	&IList_1_t8069_il2cpp_TypeInfo,
	&IEnumerable_1_t8070_il2cpp_TypeInfo,
	&ICollection_1_t7255_il2cpp_TypeInfo,
	&IList_1_t7256_il2cpp_TypeInfo,
	&IEnumerable_1_t7257_il2cpp_TypeInfo,
	&ICollection_1_t7584_il2cpp_TypeInfo,
	&IList_1_t7585_il2cpp_TypeInfo,
	&IEnumerable_1_t7586_il2cpp_TypeInfo,
	&ICollection_1_t7587_il2cpp_TypeInfo,
	&IList_1_t7588_il2cpp_TypeInfo,
	&IEnumerable_1_t7589_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair List_1U5BU5D_t4016_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8068_il2cpp_TypeInfo, 21},
	{ &IList_1_t8069_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8070_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7255_il2cpp_TypeInfo, 34},
	{ &IList_1_t7256_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7257_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7584_il2cpp_TypeInfo, 47},
	{ &IList_1_t7585_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7586_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7587_il2cpp_TypeInfo, 60},
	{ &IList_1_t7588_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7589_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 73},
	{ &IList_1_t2851_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 85},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType List_1U5BU5D_t4016_0_0_0;
extern Il2CppType List_1U5BU5D_t4016_1_0_0;
struct List_1_t697;
extern TypeInfo List_1_t697_il2cpp_TypeInfo;
extern CustomAttributesCache List_1_t1818__CustomAttributeCache;
TypeInfo List_1U5BU5D_t4016_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "List`1[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, List_1U5BU5D_t4016_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &List_1_t697_il2cpp_TypeInfo/* element_class */
	, List_1U5BU5D_t4016_InterfacesTypeInfos/* implemented_interfaces */
	, List_1U5BU5D_t4016_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &List_1U5BU5D_t4016_il2cpp_TypeInfo/* cast_class */
	, &List_1U5BU5D_t4016_0_0_0/* byval_arg */
	, &List_1U5BU5D_t4016_1_0_0/* this_arg */
	, List_1U5BU5D_t4016_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (List_1_t697 *)/* element_size */
	, -1/* native_size */
	, sizeof(List_1U5BU5D_t4016_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 86/* vtable_count */
	, 15/* interfaces_count */
	, 19/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ILoadLevelEventHandlerU5BU5D_t4036_il2cpp_TypeInfo;



// Metadata Definition Vuforia.ILoadLevelEventHandler[]
static MethodInfo* ILoadLevelEventHandlerU5BU5D_t4036_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisILoadLevelEventHandler_t711_m35911_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisILoadLevelEventHandler_t711_m35912_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisILoadLevelEventHandler_t711_m35913_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisILoadLevelEventHandler_t711_m35914_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisILoadLevelEventHandler_t711_m35915_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisILoadLevelEventHandler_t711_m35916_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisILoadLevelEventHandler_t711_m35910_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisILoadLevelEventHandler_t711_m35918_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisILoadLevelEventHandler_t711_m35919_MethodInfo;
static MethodInfo* ILoadLevelEventHandlerU5BU5D_t4036_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisILoadLevelEventHandler_t711_m35911_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisILoadLevelEventHandler_t711_m35912_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisILoadLevelEventHandler_t711_m35913_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisILoadLevelEventHandler_t711_m35914_MethodInfo,
	&Array_InternalArray__IndexOf_TisILoadLevelEventHandler_t711_m35915_MethodInfo,
	&Array_InternalArray__Insert_TisILoadLevelEventHandler_t711_m35916_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisILoadLevelEventHandler_t711_m35910_MethodInfo,
	&Array_InternalArray__set_Item_TisILoadLevelEventHandler_t711_m35918_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisILoadLevelEventHandler_t711_m35919_MethodInfo,
};
extern TypeInfo ICollection_1_t4039_il2cpp_TypeInfo;
extern TypeInfo IList_1_t4044_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t4037_il2cpp_TypeInfo;
static TypeInfo* ILoadLevelEventHandlerU5BU5D_t4036_InterfacesTypeInfos[] = 
{
	&ICollection_1_t4039_il2cpp_TypeInfo,
	&IList_1_t4044_il2cpp_TypeInfo,
	&IEnumerable_1_t4037_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair ILoadLevelEventHandlerU5BU5D_t4036_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t4039_il2cpp_TypeInfo, 21},
	{ &IList_1_t4044_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t4037_il2cpp_TypeInfo, 33},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType ILoadLevelEventHandlerU5BU5D_t4036_0_0_0;
extern Il2CppType ILoadLevelEventHandlerU5BU5D_t4036_1_0_0;
struct ILoadLevelEventHandler_t711;
extern TypeInfo ILoadLevelEventHandler_t711_il2cpp_TypeInfo;
TypeInfo ILoadLevelEventHandlerU5BU5D_t4036_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILoadLevelEventHandler[]"/* name */
	, "Vuforia"/* namespaze */
	, ILoadLevelEventHandlerU5BU5D_t4036_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ILoadLevelEventHandler_t711_il2cpp_TypeInfo/* element_class */
	, ILoadLevelEventHandlerU5BU5D_t4036_InterfacesTypeInfos/* implemented_interfaces */
	, ILoadLevelEventHandlerU5BU5D_t4036_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ILoadLevelEventHandlerU5BU5D_t4036_il2cpp_TypeInfo/* cast_class */
	, &ILoadLevelEventHandlerU5BU5D_t4036_0_0_0/* byval_arg */
	, &ILoadLevelEventHandlerU5BU5D_t4036_1_0_0/* this_arg */
	, ILoadLevelEventHandlerU5BU5D_t4036_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 3/* interfaces_count */
	, 7/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ISmartTerrainEventHandlerU5BU5D_t4064_il2cpp_TypeInfo;



// Metadata Definition Vuforia.ISmartTerrainEventHandler[]
static MethodInfo* ISmartTerrainEventHandlerU5BU5D_t4064_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisISmartTerrainEventHandler_t718_m35959_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisISmartTerrainEventHandler_t718_m35960_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisISmartTerrainEventHandler_t718_m35961_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisISmartTerrainEventHandler_t718_m35962_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisISmartTerrainEventHandler_t718_m35963_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisISmartTerrainEventHandler_t718_m35964_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisISmartTerrainEventHandler_t718_m35958_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisISmartTerrainEventHandler_t718_m35966_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisISmartTerrainEventHandler_t718_m35967_MethodInfo;
static MethodInfo* ISmartTerrainEventHandlerU5BU5D_t4064_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisISmartTerrainEventHandler_t718_m35959_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisISmartTerrainEventHandler_t718_m35960_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisISmartTerrainEventHandler_t718_m35961_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisISmartTerrainEventHandler_t718_m35962_MethodInfo,
	&Array_InternalArray__IndexOf_TisISmartTerrainEventHandler_t718_m35963_MethodInfo,
	&Array_InternalArray__Insert_TisISmartTerrainEventHandler_t718_m35964_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisISmartTerrainEventHandler_t718_m35958_MethodInfo,
	&Array_InternalArray__set_Item_TisISmartTerrainEventHandler_t718_m35966_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisISmartTerrainEventHandler_t718_m35967_MethodInfo,
};
extern TypeInfo ICollection_1_t4067_il2cpp_TypeInfo;
extern TypeInfo IList_1_t4072_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t4065_il2cpp_TypeInfo;
static TypeInfo* ISmartTerrainEventHandlerU5BU5D_t4064_InterfacesTypeInfos[] = 
{
	&ICollection_1_t4067_il2cpp_TypeInfo,
	&IList_1_t4072_il2cpp_TypeInfo,
	&IEnumerable_1_t4065_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair ISmartTerrainEventHandlerU5BU5D_t4064_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t4067_il2cpp_TypeInfo, 21},
	{ &IList_1_t4072_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t4065_il2cpp_TypeInfo, 33},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType ISmartTerrainEventHandlerU5BU5D_t4064_0_0_0;
extern Il2CppType ISmartTerrainEventHandlerU5BU5D_t4064_1_0_0;
struct ISmartTerrainEventHandler_t718;
extern TypeInfo ISmartTerrainEventHandler_t718_il2cpp_TypeInfo;
extern CustomAttributesCache ISmartTerrainEventHandler_t718__CustomAttributeCache;
TypeInfo ISmartTerrainEventHandlerU5BU5D_t4064_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "ISmartTerrainEventHandler[]"/* name */
	, "Vuforia"/* namespaze */
	, ISmartTerrainEventHandlerU5BU5D_t4064_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ISmartTerrainEventHandler_t718_il2cpp_TypeInfo/* element_class */
	, ISmartTerrainEventHandlerU5BU5D_t4064_InterfacesTypeInfos/* implemented_interfaces */
	, ISmartTerrainEventHandlerU5BU5D_t4064_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ISmartTerrainEventHandlerU5BU5D_t4064_il2cpp_TypeInfo/* cast_class */
	, &ISmartTerrainEventHandlerU5BU5D_t4064_0_0_0/* byval_arg */
	, &ISmartTerrainEventHandlerU5BU5D_t4064_1_0_0/* this_arg */
	, ISmartTerrainEventHandlerU5BU5D_t4064_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 3/* interfaces_count */
	, 7/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t4080_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
static MethodInfo* KeyValuePair_2U5BU5D_t4080_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t848_m35985_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t848_m35986_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t848_m35987_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t848_m35988_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t848_m35989_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t848_m35990_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t848_m35984_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t848_m35992_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t848_m35993_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t4080_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t848_m35985_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t848_m35986_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t848_m35987_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t848_m35988_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t848_m35989_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t848_m35990_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t848_m35984_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t848_m35992_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t848_m35993_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8071_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8072_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8073_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t4080_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8071_il2cpp_TypeInfo,
	&IList_1_t8072_il2cpp_TypeInfo,
	&IEnumerable_1_t8073_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t4080_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8071_il2cpp_TypeInfo, 21},
	{ &IList_1_t8072_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8073_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t4080_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t4080_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"
extern TypeInfo KeyValuePair_2_t848_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t4080_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t4080_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t848_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t4080_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t4080_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t4080_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t4080_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t4080_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t4080_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t848 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SurfaceU5BU5D_t4078_il2cpp_TypeInfo;



// Metadata Definition Vuforia.Surface[]
static MethodInfo* SurfaceU5BU5D_t4078_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisSurface_t43_m35996_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisSurface_t43_m35997_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisSurface_t43_m35998_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisSurface_t43_m35999_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisSurface_t43_m36000_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisSurface_t43_m36001_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSurface_t43_m35995_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisSurface_t43_m36003_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisSurface_t43_m36004_MethodInfo;
static MethodInfo* SurfaceU5BU5D_t4078_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisSurface_t43_m35996_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisSurface_t43_m35997_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisSurface_t43_m35998_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisSurface_t43_m35999_MethodInfo,
	&Array_InternalArray__IndexOf_TisSurface_t43_m36000_MethodInfo,
	&Array_InternalArray__Insert_TisSurface_t43_m36001_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisSurface_t43_m35995_MethodInfo,
	&Array_InternalArray__set_Item_TisSurface_t43_m36003_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisSurface_t43_m36004_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisSmartTerrainTrackable_t581_m35633_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisSmartTerrainTrackable_t581_m35634_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisSmartTerrainTrackable_t581_m35635_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisSmartTerrainTrackable_t581_m35636_MethodInfo,
	&Array_InternalArray__IndexOf_TisSmartTerrainTrackable_t581_m35637_MethodInfo,
	&Array_InternalArray__Insert_TisSmartTerrainTrackable_t581_m35638_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisSmartTerrainTrackable_t581_m35632_MethodInfo,
	&Array_InternalArray__set_Item_TisSmartTerrainTrackable_t581_m35640_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisSmartTerrainTrackable_t581_m35641_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTrackable_t550_m35304_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTrackable_t550_m35305_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTrackable_t550_m35306_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTrackable_t550_m35307_MethodInfo,
	&Array_InternalArray__IndexOf_TisTrackable_t550_m35308_MethodInfo,
	&Array_InternalArray__Insert_TisTrackable_t550_m35309_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTrackable_t550_m35303_MethodInfo,
	&Array_InternalArray__set_Item_TisTrackable_t550_m35311_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTrackable_t550_m35312_MethodInfo,
};
extern TypeInfo ICollection_1_t4145_il2cpp_TypeInfo;
extern TypeInfo IList_1_t4149_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t720_il2cpp_TypeInfo;
static TypeInfo* SurfaceU5BU5D_t4078_InterfacesTypeInfos[] = 
{
	&ICollection_1_t4145_il2cpp_TypeInfo,
	&IList_1_t4149_il2cpp_TypeInfo,
	&IEnumerable_1_t720_il2cpp_TypeInfo,
	&ICollection_1_t3915_il2cpp_TypeInfo,
	&IList_1_t3921_il2cpp_TypeInfo,
	&IEnumerable_1_t667_il2cpp_TypeInfo,
	&ICollection_1_t3815_il2cpp_TypeInfo,
	&IList_1_t3819_il2cpp_TypeInfo,
	&IEnumerable_1_t585_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair SurfaceU5BU5D_t4078_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t4145_il2cpp_TypeInfo, 21},
	{ &IList_1_t4149_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t720_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t3915_il2cpp_TypeInfo, 34},
	{ &IList_1_t3921_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t667_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t3815_il2cpp_TypeInfo, 47},
	{ &IList_1_t3819_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t585_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType SurfaceU5BU5D_t4078_0_0_0;
extern Il2CppType SurfaceU5BU5D_t4078_1_0_0;
struct Surface_t43;
extern TypeInfo Surface_t43_il2cpp_TypeInfo;
TypeInfo SurfaceU5BU5D_t4078_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "Surface[]"/* name */
	, "Vuforia"/* namespaze */
	, SurfaceU5BU5D_t4078_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Surface_t43_il2cpp_TypeInfo/* element_class */
	, SurfaceU5BU5D_t4078_InterfacesTypeInfos/* implemented_interfaces */
	, SurfaceU5BU5D_t4078_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SurfaceU5BU5D_t4078_il2cpp_TypeInfo/* cast_class */
	, &SurfaceU5BU5D_t4078_0_0_0/* byval_arg */
	, &SurfaceU5BU5D_t4078_1_0_0/* this_arg */
	, SurfaceU5BU5D_t4078_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t4095_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>[]
static MethodInfo* KeyValuePair_2U5BU5D_t4095_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4096_m36017_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4096_m36018_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4096_m36019_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4096_m36020_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t4096_m36021_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t4096_m36022_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t4096_m36016_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t4096_m36024_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4096_m36025_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t4095_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4096_m36017_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4096_m36018_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4096_m36019_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4096_m36020_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t4096_m36021_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t4096_m36022_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t4096_m36016_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t4096_m36024_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4096_m36025_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8074_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8075_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8076_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t4095_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8074_il2cpp_TypeInfo,
	&IList_1_t8075_il2cpp_TypeInfo,
	&IEnumerable_1_t8076_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t4095_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8074_il2cpp_TypeInfo, 21},
	{ &IList_1_t8075_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8076_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t4095_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t4095_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
extern TypeInfo KeyValuePair_2_t4096_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t4095_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t4095_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t4096_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t4095_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t4095_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t4095_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t4095_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t4095_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t4095_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t4096 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t4110_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
static MethodInfo* KeyValuePair_2U5BU5D_t4110_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t846_m36038_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t846_m36039_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t846_m36040_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t846_m36041_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t846_m36042_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t846_m36043_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t846_m36037_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t846_m36045_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t846_m36046_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t4110_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t846_m36038_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t846_m36039_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t846_m36040_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t846_m36041_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t846_m36042_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t846_m36043_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t846_m36037_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t846_m36045_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t846_m36046_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8077_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8078_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8079_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t4110_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8077_il2cpp_TypeInfo,
	&IList_1_t8078_il2cpp_TypeInfo,
	&IEnumerable_1_t8079_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t4110_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8077_il2cpp_TypeInfo, 21},
	{ &IList_1_t8078_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8079_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t4110_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t4110_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_1.h"
extern TypeInfo KeyValuePair_2_t846_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t4110_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t4110_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t846_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t4110_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t4110_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t4110_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t4110_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t4110_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t4110_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t846 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo PropU5BU5D_t4108_il2cpp_TypeInfo;



// Metadata Definition Vuforia.Prop[]
static MethodInfo* PropU5BU5D_t4108_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisProp_t42_m36049_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisProp_t42_m36050_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisProp_t42_m36051_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisProp_t42_m36052_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisProp_t42_m36053_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisProp_t42_m36054_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisProp_t42_m36048_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisProp_t42_m36056_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisProp_t42_m36057_MethodInfo;
static MethodInfo* PropU5BU5D_t4108_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisProp_t42_m36049_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisProp_t42_m36050_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisProp_t42_m36051_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisProp_t42_m36052_MethodInfo,
	&Array_InternalArray__IndexOf_TisProp_t42_m36053_MethodInfo,
	&Array_InternalArray__Insert_TisProp_t42_m36054_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisProp_t42_m36048_MethodInfo,
	&Array_InternalArray__set_Item_TisProp_t42_m36056_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisProp_t42_m36057_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisSmartTerrainTrackable_t581_m35633_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisSmartTerrainTrackable_t581_m35634_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisSmartTerrainTrackable_t581_m35635_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisSmartTerrainTrackable_t581_m35636_MethodInfo,
	&Array_InternalArray__IndexOf_TisSmartTerrainTrackable_t581_m35637_MethodInfo,
	&Array_InternalArray__Insert_TisSmartTerrainTrackable_t581_m35638_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisSmartTerrainTrackable_t581_m35632_MethodInfo,
	&Array_InternalArray__set_Item_TisSmartTerrainTrackable_t581_m35640_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisSmartTerrainTrackable_t581_m35641_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTrackable_t550_m35304_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTrackable_t550_m35305_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTrackable_t550_m35306_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTrackable_t550_m35307_MethodInfo,
	&Array_InternalArray__IndexOf_TisTrackable_t550_m35308_MethodInfo,
	&Array_InternalArray__Insert_TisTrackable_t550_m35309_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTrackable_t550_m35303_MethodInfo,
	&Array_InternalArray__set_Item_TisTrackable_t550_m35311_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTrackable_t550_m35312_MethodInfo,
};
extern TypeInfo ICollection_1_t4137_il2cpp_TypeInfo;
extern TypeInfo IList_1_t4141_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t719_il2cpp_TypeInfo;
static TypeInfo* PropU5BU5D_t4108_InterfacesTypeInfos[] = 
{
	&ICollection_1_t4137_il2cpp_TypeInfo,
	&IList_1_t4141_il2cpp_TypeInfo,
	&IEnumerable_1_t719_il2cpp_TypeInfo,
	&ICollection_1_t3915_il2cpp_TypeInfo,
	&IList_1_t3921_il2cpp_TypeInfo,
	&IEnumerable_1_t667_il2cpp_TypeInfo,
	&ICollection_1_t3815_il2cpp_TypeInfo,
	&IList_1_t3819_il2cpp_TypeInfo,
	&IEnumerable_1_t585_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair PropU5BU5D_t4108_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t4137_il2cpp_TypeInfo, 21},
	{ &IList_1_t4141_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t719_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t3915_il2cpp_TypeInfo, 34},
	{ &IList_1_t3921_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t667_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t3815_il2cpp_TypeInfo, 47},
	{ &IList_1_t3819_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t585_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType PropU5BU5D_t4108_0_0_0;
extern Il2CppType PropU5BU5D_t4108_1_0_0;
struct Prop_t42;
extern TypeInfo Prop_t42_il2cpp_TypeInfo;
TypeInfo PropU5BU5D_t4108_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "Prop[]"/* name */
	, "Vuforia"/* namespaze */
	, PropU5BU5D_t4108_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Prop_t42_il2cpp_TypeInfo/* element_class */
	, PropU5BU5D_t4108_InterfacesTypeInfos/* implemented_interfaces */
	, PropU5BU5D_t4108_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &PropU5BU5D_t4108_il2cpp_TypeInfo/* cast_class */
	, &PropU5BU5D_t4108_0_0_0/* byval_arg */
	, &PropU5BU5D_t4108_1_0_0/* this_arg */
	, PropU5BU5D_t4108_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t4124_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>[]
static MethodInfo* KeyValuePair_2U5BU5D_t4124_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4125_m36070_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4125_m36071_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4125_m36072_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4125_m36073_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t4125_m36074_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t4125_m36075_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t4125_m36069_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t4125_m36077_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4125_m36078_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t4124_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4125_m36070_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4125_m36071_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4125_m36072_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4125_m36073_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t4125_m36074_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t4125_m36075_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t4125_m36069_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t4125_m36077_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4125_m36078_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8080_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8081_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8082_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t4124_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8080_il2cpp_TypeInfo,
	&IList_1_t8081_il2cpp_TypeInfo,
	&IEnumerable_1_t8082_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t4124_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8080_il2cpp_TypeInfo, 21},
	{ &IList_1_t8081_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8082_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t4124_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t4124_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_17.h"
extern TypeInfo KeyValuePair_2_t4125_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t4124_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t4124_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t4125_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t4124_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t4124_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t4124_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t4124_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t4124_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t4124_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t4125 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#include "UnityEngine_ArrayTypes.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo MeshFilterU5BU5D_t856_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.MeshFilter[]
static MethodInfo* MeshFilterU5BU5D_t856_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisMeshFilter_t169_m36121_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisMeshFilter_t169_m36122_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisMeshFilter_t169_m36123_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisMeshFilter_t169_m36124_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisMeshFilter_t169_m36125_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisMeshFilter_t169_m36126_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMeshFilter_t169_m36120_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisMeshFilter_t169_m36128_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisMeshFilter_t169_m36129_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisComponent_t100_m31481_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisComponent_t100_m31482_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisComponent_t100_m31483_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisComponent_t100_m31484_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisComponent_t100_m31485_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisComponent_t100_m31486_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisComponent_t100_m31480_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisComponent_t100_m31488_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisComponent_t100_m31489_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisObject_t117_m31492_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisObject_t117_m31493_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisObject_t117_m31494_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisObject_t117_m31495_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisObject_t117_m31496_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisObject_t117_m31497_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisObject_t117_m31499_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t117_m31500_MethodInfo;
static MethodInfo* MeshFilterU5BU5D_t856_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisMeshFilter_t169_m36121_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisMeshFilter_t169_m36122_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisMeshFilter_t169_m36123_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisMeshFilter_t169_m36124_MethodInfo,
	&Array_InternalArray__IndexOf_TisMeshFilter_t169_m36125_MethodInfo,
	&Array_InternalArray__Insert_TisMeshFilter_t169_m36126_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisMeshFilter_t169_m36120_MethodInfo,
	&Array_InternalArray__set_Item_TisMeshFilter_t169_m36128_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisMeshFilter_t169_m36129_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisComponent_t100_m31481_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisComponent_t100_m31482_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisComponent_t100_m31483_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisComponent_t100_m31484_MethodInfo,
	&Array_InternalArray__IndexOf_TisComponent_t100_m31485_MethodInfo,
	&Array_InternalArray__Insert_TisComponent_t100_m31486_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisComponent_t100_m31480_MethodInfo,
	&Array_InternalArray__set_Item_TisComponent_t100_m31488_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisComponent_t100_m31489_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t117_m31492_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t117_m31493_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t117_m31494_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t117_m31495_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t117_m31496_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t117_m31497_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t117_m31499_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t117_m31500_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8083_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8084_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8085_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t3046_il2cpp_TypeInfo;
extern TypeInfo IList_1_t3050_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t3044_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7225_il2cpp_TypeInfo;
extern TypeInfo IList_1_t7226_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7227_il2cpp_TypeInfo;
static TypeInfo* MeshFilterU5BU5D_t856_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8083_il2cpp_TypeInfo,
	&IList_1_t8084_il2cpp_TypeInfo,
	&IEnumerable_1_t8085_il2cpp_TypeInfo,
	&ICollection_1_t3046_il2cpp_TypeInfo,
	&IList_1_t3050_il2cpp_TypeInfo,
	&IEnumerable_1_t3044_il2cpp_TypeInfo,
	&ICollection_1_t7225_il2cpp_TypeInfo,
	&IList_1_t7226_il2cpp_TypeInfo,
	&IEnumerable_1_t7227_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair MeshFilterU5BU5D_t856_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8083_il2cpp_TypeInfo, 21},
	{ &IList_1_t8084_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8085_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t3046_il2cpp_TypeInfo, 34},
	{ &IList_1_t3050_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t3044_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7225_il2cpp_TypeInfo, 47},
	{ &IList_1_t7226_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7227_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 60},
	{ &IList_1_t2851_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType MeshFilterU5BU5D_t856_0_0_0;
extern Il2CppType MeshFilterU5BU5D_t856_1_0_0;
struct MeshFilter_t169;
extern TypeInfo MeshFilter_t169_il2cpp_TypeInfo;
extern CustomAttributesCache MeshFilter_t169__CustomAttributeCache_MeshFilter_get_mesh_m4286;
extern CustomAttributesCache MeshFilter_t169__CustomAttributeCache_MeshFilter_set_mesh_m5291;
extern CustomAttributesCache MeshFilter_t169__CustomAttributeCache_MeshFilter_get_sharedMesh_m644;
extern CustomAttributesCache MeshFilter_t169__CustomAttributeCache_MeshFilter_set_sharedMesh_m4512;
TypeInfo MeshFilterU5BU5D_t856_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MeshFilter[]"/* name */
	, "UnityEngine"/* namespaze */
	, MeshFilterU5BU5D_t856_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &MeshFilter_t169_il2cpp_TypeInfo/* element_class */
	, MeshFilterU5BU5D_t856_InterfacesTypeInfos/* implemented_interfaces */
	, MeshFilterU5BU5D_t856_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &MeshFilterU5BU5D_t856_il2cpp_TypeInfo/* cast_class */
	, &MeshFilterU5BU5D_t856_0_0_0/* byval_arg */
	, &MeshFilterU5BU5D_t856_1_0_0/* this_arg */
	, MeshFilterU5BU5D_t856_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (MeshFilter_t169 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 73/* vtable_count */
	, 12/* interfaces_count */
	, 16/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t4160_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>[]
static MethodInfo* KeyValuePair_2U5BU5D_t4160_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4161_m36137_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4161_m36138_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4161_m36139_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4161_m36140_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t4161_m36141_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t4161_m36142_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t4161_m36136_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t4161_m36144_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4161_m36145_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t4160_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4161_m36137_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4161_m36138_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4161_m36139_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4161_m36140_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t4161_m36141_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t4161_m36142_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t4161_m36136_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t4161_m36144_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4161_m36145_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8086_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8087_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8088_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t4160_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8086_il2cpp_TypeInfo,
	&IList_1_t8087_il2cpp_TypeInfo,
	&IEnumerable_1_t8088_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t4160_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8086_il2cpp_TypeInfo, 21},
	{ &IList_1_t8087_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8088_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t4160_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t4160_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
extern TypeInfo KeyValuePair_2_t4161_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t4160_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t4160_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t4161_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t4160_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t4160_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t4160_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t4160_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t4160_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t4160_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t4161 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t4174_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>[]
static MethodInfo* KeyValuePair_2U5BU5D_t4174_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4175_m36160_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4175_m36161_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4175_m36162_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4175_m36163_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t4175_m36164_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t4175_m36165_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t4175_m36159_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t4175_m36167_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4175_m36168_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t4174_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4175_m36160_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4175_m36161_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4175_m36162_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4175_m36163_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t4175_m36164_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t4175_m36165_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t4175_m36159_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t4175_m36167_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4175_m36168_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8089_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8090_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8091_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t4174_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8089_il2cpp_TypeInfo,
	&IList_1_t8090_il2cpp_TypeInfo,
	&IEnumerable_1_t8091_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t4174_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8089_il2cpp_TypeInfo, 21},
	{ &IList_1_t8090_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8091_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t4174_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t4174_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_19.h"
extern TypeInfo KeyValuePair_2_t4175_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t4174_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t4174_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t4175_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t4174_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t4174_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t4174_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t4174_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t4174_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t4174_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t4175 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t4192_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>[]
static MethodInfo* KeyValuePair_2U5BU5D_t4192_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4193_m36183_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4193_m36184_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4193_m36185_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4193_m36186_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t4193_m36187_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t4193_m36188_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t4193_m36182_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t4193_m36190_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4193_m36191_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t4192_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4193_m36183_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4193_m36184_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4193_m36185_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4193_m36186_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t4193_m36187_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t4193_m36188_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t4193_m36182_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t4193_m36190_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4193_m36191_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8092_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8093_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8094_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t4192_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8092_il2cpp_TypeInfo,
	&IList_1_t8093_il2cpp_TypeInfo,
	&IEnumerable_1_t8094_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t4192_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8092_il2cpp_TypeInfo, 21},
	{ &IList_1_t8093_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8094_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t4192_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t4192_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_20.h"
extern TypeInfo KeyValuePair_2_t4193_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t4192_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t4192_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t4193_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t4192_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t4192_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t4192_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t4192_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t4192_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t4192_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t4193 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, true/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo VirtualButtonDataU5BU5D_t4188_il2cpp_TypeInfo;



// Metadata Definition Vuforia.QCARManagerImpl/VirtualButtonData[]
static MethodInfo* VirtualButtonDataU5BU5D_t4188_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisVirtualButtonData_t640_m36194_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisVirtualButtonData_t640_m36195_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisVirtualButtonData_t640_m36196_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisVirtualButtonData_t640_m36197_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisVirtualButtonData_t640_m36198_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisVirtualButtonData_t640_m36199_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVirtualButtonData_t640_m36193_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisVirtualButtonData_t640_m36201_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisVirtualButtonData_t640_m36202_MethodInfo;
static MethodInfo* VirtualButtonDataU5BU5D_t4188_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisVirtualButtonData_t640_m36194_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisVirtualButtonData_t640_m36195_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisVirtualButtonData_t640_m36196_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisVirtualButtonData_t640_m36197_MethodInfo,
	&Array_InternalArray__IndexOf_TisVirtualButtonData_t640_m36198_MethodInfo,
	&Array_InternalArray__Insert_TisVirtualButtonData_t640_m36199_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisVirtualButtonData_t640_m36193_MethodInfo,
	&Array_InternalArray__set_Item_TisVirtualButtonData_t640_m36201_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisVirtualButtonData_t640_m36202_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8095_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8096_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8097_il2cpp_TypeInfo;
static TypeInfo* VirtualButtonDataU5BU5D_t4188_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8095_il2cpp_TypeInfo,
	&IList_1_t8096_il2cpp_TypeInfo,
	&IEnumerable_1_t8097_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair VirtualButtonDataU5BU5D_t4188_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8095_il2cpp_TypeInfo, 21},
	{ &IList_1_t8096_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8097_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType VirtualButtonDataU5BU5D_t4188_0_0_0;
extern Il2CppType VirtualButtonDataU5BU5D_t4188_1_0_0;
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"
extern TypeInfo VirtualButtonData_t640_il2cpp_TypeInfo;
TypeInfo VirtualButtonDataU5BU5D_t4188_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "VirtualButtonData[]"/* name */
	, ""/* namespaze */
	, VirtualButtonDataU5BU5D_t4188_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &VirtualButtonData_t640_il2cpp_TypeInfo/* element_class */
	, VirtualButtonDataU5BU5D_t4188_InterfacesTypeInfos/* implemented_interfaces */
	, VirtualButtonDataU5BU5D_t4188_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &VirtualButtonDataU5BU5D_t4188_il2cpp_TypeInfo/* cast_class */
	, &VirtualButtonDataU5BU5D_t4188_0_0_0/* byval_arg */
	, &VirtualButtonDataU5BU5D_t4188_1_0_0/* this_arg */
	, VirtualButtonDataU5BU5D_t4188_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (VirtualButtonData_t640 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, true/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InitStateU5BU5D_t5539_il2cpp_TypeInfo;



// Metadata Definition Vuforia.TargetFinder/InitState[]
static MethodInfo* InitStateU5BU5D_t5539_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisInitState_t730_m36230_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisInitState_t730_m36231_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisInitState_t730_m36232_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisInitState_t730_m36233_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisInitState_t730_m36234_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisInitState_t730_m36235_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisInitState_t730_m36229_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisInitState_t730_m36237_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisInitState_t730_m36238_MethodInfo;
static MethodInfo* InitStateU5BU5D_t5539_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisInitState_t730_m36230_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisInitState_t730_m36231_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisInitState_t730_m36232_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisInitState_t730_m36233_MethodInfo,
	&Array_InternalArray__IndexOf_TisInitState_t730_m36234_MethodInfo,
	&Array_InternalArray__Insert_TisInitState_t730_m36235_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisInitState_t730_m36229_MethodInfo,
	&Array_InternalArray__set_Item_TisInitState_t730_m36237_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisInitState_t730_m36238_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8098_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8099_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8100_il2cpp_TypeInfo;
static TypeInfo* InitStateU5BU5D_t5539_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8098_il2cpp_TypeInfo,
	&IList_1_t8099_il2cpp_TypeInfo,
	&IEnumerable_1_t8100_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InitStateU5BU5D_t5539_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8098_il2cpp_TypeInfo, 21},
	{ &IList_1_t8099_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8100_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType InitStateU5BU5D_t5539_0_0_0;
extern Il2CppType InitStateU5BU5D_t5539_1_0_0;
// Vuforia.TargetFinder/InitState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_InitSt.h"
extern TypeInfo InitState_t730_il2cpp_TypeInfo;
TypeInfo InitStateU5BU5D_t5539_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "InitState[]"/* name */
	, ""/* namespaze */
	, InitStateU5BU5D_t5539_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InitState_t730_il2cpp_TypeInfo/* element_class */
	, InitStateU5BU5D_t5539_InterfacesTypeInfos/* implemented_interfaces */
	, InitStateU5BU5D_t5539_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &InitStateU5BU5D_t5539_0_0_0/* byval_arg */
	, &InitStateU5BU5D_t5539_1_0_0/* this_arg */
	, InitStateU5BU5D_t5539_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UpdateStateU5BU5D_t5540_il2cpp_TypeInfo;



// Metadata Definition Vuforia.TargetFinder/UpdateState[]
static MethodInfo* UpdateStateU5BU5D_t5540_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisUpdateState_t731_m36241_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisUpdateState_t731_m36242_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisUpdateState_t731_m36243_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisUpdateState_t731_m36244_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisUpdateState_t731_m36245_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisUpdateState_t731_m36246_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUpdateState_t731_m36240_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisUpdateState_t731_m36248_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisUpdateState_t731_m36249_MethodInfo;
static MethodInfo* UpdateStateU5BU5D_t5540_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisUpdateState_t731_m36241_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisUpdateState_t731_m36242_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisUpdateState_t731_m36243_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisUpdateState_t731_m36244_MethodInfo,
	&Array_InternalArray__IndexOf_TisUpdateState_t731_m36245_MethodInfo,
	&Array_InternalArray__Insert_TisUpdateState_t731_m36246_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisUpdateState_t731_m36240_MethodInfo,
	&Array_InternalArray__set_Item_TisUpdateState_t731_m36248_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisUpdateState_t731_m36249_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8101_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8102_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8103_il2cpp_TypeInfo;
static TypeInfo* UpdateStateU5BU5D_t5540_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8101_il2cpp_TypeInfo,
	&IList_1_t8102_il2cpp_TypeInfo,
	&IEnumerable_1_t8103_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair UpdateStateU5BU5D_t5540_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8101_il2cpp_TypeInfo, 21},
	{ &IList_1_t8102_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8103_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType UpdateStateU5BU5D_t5540_0_0_0;
extern Il2CppType UpdateStateU5BU5D_t5540_1_0_0;
// Vuforia.TargetFinder/UpdateState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Update.h"
extern TypeInfo UpdateState_t731_il2cpp_TypeInfo;
TypeInfo UpdateStateU5BU5D_t5540_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "UpdateState[]"/* name */
	, ""/* namespaze */
	, UpdateStateU5BU5D_t5540_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UpdateState_t731_il2cpp_TypeInfo/* element_class */
	, UpdateStateU5BU5D_t5540_InterfacesTypeInfos/* implemented_interfaces */
	, UpdateStateU5BU5D_t5540_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &UpdateStateU5BU5D_t5540_0_0_0/* byval_arg */
	, &UpdateStateU5BU5D_t5540_1_0_0/* this_arg */
	, UpdateStateU5BU5D_t5540_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TargetSearchResultU5BU5D_t4219_il2cpp_TypeInfo;



// Metadata Definition Vuforia.TargetFinder/TargetSearchResult[]
static MethodInfo* TargetSearchResultU5BU5D_t4219_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisTargetSearchResult_t732_m36252_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisTargetSearchResult_t732_m36253_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisTargetSearchResult_t732_m36254_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisTargetSearchResult_t732_m36255_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisTargetSearchResult_t732_m36256_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisTargetSearchResult_t732_m36257_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTargetSearchResult_t732_m36251_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisTargetSearchResult_t732_m36259_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisTargetSearchResult_t732_m36260_MethodInfo;
static MethodInfo* TargetSearchResultU5BU5D_t4219_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTargetSearchResult_t732_m36252_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTargetSearchResult_t732_m36253_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTargetSearchResult_t732_m36254_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTargetSearchResult_t732_m36255_MethodInfo,
	&Array_InternalArray__IndexOf_TisTargetSearchResult_t732_m36256_MethodInfo,
	&Array_InternalArray__Insert_TisTargetSearchResult_t732_m36257_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTargetSearchResult_t732_m36251_MethodInfo,
	&Array_InternalArray__set_Item_TisTargetSearchResult_t732_m36259_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTargetSearchResult_t732_m36260_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t4220_il2cpp_TypeInfo;
extern TypeInfo IList_1_t4226_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t733_il2cpp_TypeInfo;
static TypeInfo* TargetSearchResultU5BU5D_t4219_InterfacesTypeInfos[] = 
{
	&ICollection_1_t4220_il2cpp_TypeInfo,
	&IList_1_t4226_il2cpp_TypeInfo,
	&IEnumerable_1_t733_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair TargetSearchResultU5BU5D_t4219_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t4220_il2cpp_TypeInfo, 21},
	{ &IList_1_t4226_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t733_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType TargetSearchResultU5BU5D_t4219_0_0_0;
extern Il2CppType TargetSearchResultU5BU5D_t4219_1_0_0;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"
extern TypeInfo TargetSearchResult_t732_il2cpp_TypeInfo;
TypeInfo TargetSearchResultU5BU5D_t4219_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "TargetSearchResult[]"/* name */
	, ""/* namespaze */
	, TargetSearchResultU5BU5D_t4219_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TargetSearchResult_t732_il2cpp_TypeInfo/* element_class */
	, TargetSearchResultU5BU5D_t4219_InterfacesTypeInfos/* implemented_interfaces */
	, TargetSearchResultU5BU5D_t4219_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TargetSearchResultU5BU5D_t4219_il2cpp_TypeInfo/* cast_class */
	, &TargetSearchResultU5BU5D_t4219_0_0_0/* byval_arg */
	, &TargetSearchResultU5BU5D_t4219_1_0_0/* this_arg */
	, TargetSearchResultU5BU5D_t4219_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (TargetSearchResult_t732 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048842/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t4234_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>[]
static MethodInfo* KeyValuePair_2U5BU5D_t4234_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4235_m36278_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4235_m36279_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4235_m36280_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4235_m36281_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t4235_m36282_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t4235_m36283_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t4235_m36277_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t4235_m36285_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4235_m36286_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t4234_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4235_m36278_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4235_m36279_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4235_m36280_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4235_m36281_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t4235_m36282_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t4235_m36283_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t4235_m36277_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t4235_m36285_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4235_m36286_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8104_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8105_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8106_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t4234_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8104_il2cpp_TypeInfo,
	&IList_1_t8105_il2cpp_TypeInfo,
	&IEnumerable_1_t8106_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t4234_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8104_il2cpp_TypeInfo, 21},
	{ &IList_1_t8105_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8106_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t4234_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t4234_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"
extern TypeInfo KeyValuePair_2_t4235_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t4234_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t4234_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t4235_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t4234_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t4234_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t4234_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t4234_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t4234_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t4234_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t4235 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ImageTargetU5BU5D_t876_il2cpp_TypeInfo;



// Metadata Definition Vuforia.ImageTarget[]
static MethodInfo* ImageTargetU5BU5D_t876_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisImageTarget_t572_m36289_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisImageTarget_t572_m36290_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisImageTarget_t572_m36291_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisImageTarget_t572_m36292_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisImageTarget_t572_m36293_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisImageTarget_t572_m36294_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisImageTarget_t572_m36288_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisImageTarget_t572_m36296_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisImageTarget_t572_m36297_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisObjectTarget_t554_m36300_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisObjectTarget_t554_m36301_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisObjectTarget_t554_m36302_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisObjectTarget_t554_m36303_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisObjectTarget_t554_m36304_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisObjectTarget_t554_m36305_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisObjectTarget_t554_m36299_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisObjectTarget_t554_m36307_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisObjectTarget_t554_m36308_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisExtendedTrackable_t780_m36311_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisExtendedTrackable_t780_m36312_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisExtendedTrackable_t780_m36313_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisExtendedTrackable_t780_m36314_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisExtendedTrackable_t780_m36315_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisExtendedTrackable_t780_m36316_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisExtendedTrackable_t780_m36310_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisExtendedTrackable_t780_m36318_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisExtendedTrackable_t780_m36319_MethodInfo;
static MethodInfo* ImageTargetU5BU5D_t876_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisImageTarget_t572_m36289_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisImageTarget_t572_m36290_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisImageTarget_t572_m36291_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisImageTarget_t572_m36292_MethodInfo,
	&Array_InternalArray__IndexOf_TisImageTarget_t572_m36293_MethodInfo,
	&Array_InternalArray__Insert_TisImageTarget_t572_m36294_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisImageTarget_t572_m36288_MethodInfo,
	&Array_InternalArray__set_Item_TisImageTarget_t572_m36296_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisImageTarget_t572_m36297_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObjectTarget_t554_m36300_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObjectTarget_t554_m36301_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObjectTarget_t554_m36302_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObjectTarget_t554_m36303_MethodInfo,
	&Array_InternalArray__IndexOf_TisObjectTarget_t554_m36304_MethodInfo,
	&Array_InternalArray__Insert_TisObjectTarget_t554_m36305_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObjectTarget_t554_m36299_MethodInfo,
	&Array_InternalArray__set_Item_TisObjectTarget_t554_m36307_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObjectTarget_t554_m36308_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisExtendedTrackable_t780_m36311_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisExtendedTrackable_t780_m36312_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisExtendedTrackable_t780_m36313_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisExtendedTrackable_t780_m36314_MethodInfo,
	&Array_InternalArray__IndexOf_TisExtendedTrackable_t780_m36315_MethodInfo,
	&Array_InternalArray__Insert_TisExtendedTrackable_t780_m36316_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisExtendedTrackable_t780_m36310_MethodInfo,
	&Array_InternalArray__set_Item_TisExtendedTrackable_t780_m36318_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisExtendedTrackable_t780_m36319_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTrackable_t550_m35304_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTrackable_t550_m35305_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTrackable_t550_m35306_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTrackable_t550_m35307_MethodInfo,
	&Array_InternalArray__IndexOf_TisTrackable_t550_m35308_MethodInfo,
	&Array_InternalArray__Insert_TisTrackable_t550_m35309_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTrackable_t550_m35303_MethodInfo,
	&Array_InternalArray__set_Item_TisTrackable_t550_m35311_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTrackable_t550_m35312_MethodInfo,
};
extern TypeInfo ICollection_1_t4251_il2cpp_TypeInfo;
extern TypeInfo IList_1_t4256_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t734_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t8107_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8108_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8109_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t8110_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8111_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8112_il2cpp_TypeInfo;
static TypeInfo* ImageTargetU5BU5D_t876_InterfacesTypeInfos[] = 
{
	&ICollection_1_t4251_il2cpp_TypeInfo,
	&IList_1_t4256_il2cpp_TypeInfo,
	&IEnumerable_1_t734_il2cpp_TypeInfo,
	&ICollection_1_t8107_il2cpp_TypeInfo,
	&IList_1_t8108_il2cpp_TypeInfo,
	&IEnumerable_1_t8109_il2cpp_TypeInfo,
	&ICollection_1_t8110_il2cpp_TypeInfo,
	&IList_1_t8111_il2cpp_TypeInfo,
	&IEnumerable_1_t8112_il2cpp_TypeInfo,
	&ICollection_1_t3815_il2cpp_TypeInfo,
	&IList_1_t3819_il2cpp_TypeInfo,
	&IEnumerable_1_t585_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair ImageTargetU5BU5D_t876_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t4251_il2cpp_TypeInfo, 21},
	{ &IList_1_t4256_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t734_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t8107_il2cpp_TypeInfo, 34},
	{ &IList_1_t8108_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t8109_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t8110_il2cpp_TypeInfo, 47},
	{ &IList_1_t8111_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t8112_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t3815_il2cpp_TypeInfo, 60},
	{ &IList_1_t3819_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t585_il2cpp_TypeInfo, 72},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType ImageTargetU5BU5D_t876_0_0_0;
extern Il2CppType ImageTargetU5BU5D_t876_1_0_0;
struct ImageTarget_t572;
extern TypeInfo ImageTarget_t572_il2cpp_TypeInfo;
TypeInfo ImageTargetU5BU5D_t876_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "ImageTarget[]"/* name */
	, "Vuforia"/* namespaze */
	, ImageTargetU5BU5D_t876_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ImageTarget_t572_il2cpp_TypeInfo/* element_class */
	, ImageTargetU5BU5D_t876_InterfacesTypeInfos/* implemented_interfaces */
	, ImageTargetU5BU5D_t876_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ImageTargetU5BU5D_t876_il2cpp_TypeInfo/* cast_class */
	, &ImageTargetU5BU5D_t876_0_0_0/* byval_arg */
	, &ImageTargetU5BU5D_t876_1_0_0/* this_arg */
	, ImageTargetU5BU5D_t876_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 73/* vtable_count */
	, 12/* interfaces_count */
	, 16/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ObjectTargetU5BU5D_t5541_il2cpp_TypeInfo;



// Metadata Definition Vuforia.ObjectTarget[]
static MethodInfo* ObjectTargetU5BU5D_t5541_MethodInfos[] =
{
	NULL
};
static MethodInfo* ObjectTargetU5BU5D_t5541_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObjectTarget_t554_m36300_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObjectTarget_t554_m36301_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObjectTarget_t554_m36302_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObjectTarget_t554_m36303_MethodInfo,
	&Array_InternalArray__IndexOf_TisObjectTarget_t554_m36304_MethodInfo,
	&Array_InternalArray__Insert_TisObjectTarget_t554_m36305_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObjectTarget_t554_m36299_MethodInfo,
	&Array_InternalArray__set_Item_TisObjectTarget_t554_m36307_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObjectTarget_t554_m36308_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisExtendedTrackable_t780_m36311_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisExtendedTrackable_t780_m36312_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisExtendedTrackable_t780_m36313_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisExtendedTrackable_t780_m36314_MethodInfo,
	&Array_InternalArray__IndexOf_TisExtendedTrackable_t780_m36315_MethodInfo,
	&Array_InternalArray__Insert_TisExtendedTrackable_t780_m36316_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisExtendedTrackable_t780_m36310_MethodInfo,
	&Array_InternalArray__set_Item_TisExtendedTrackable_t780_m36318_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisExtendedTrackable_t780_m36319_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTrackable_t550_m35304_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTrackable_t550_m35305_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTrackable_t550_m35306_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTrackable_t550_m35307_MethodInfo,
	&Array_InternalArray__IndexOf_TisTrackable_t550_m35308_MethodInfo,
	&Array_InternalArray__Insert_TisTrackable_t550_m35309_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTrackable_t550_m35303_MethodInfo,
	&Array_InternalArray__set_Item_TisTrackable_t550_m35311_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTrackable_t550_m35312_MethodInfo,
};
static TypeInfo* ObjectTargetU5BU5D_t5541_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8107_il2cpp_TypeInfo,
	&IList_1_t8108_il2cpp_TypeInfo,
	&IEnumerable_1_t8109_il2cpp_TypeInfo,
	&ICollection_1_t8110_il2cpp_TypeInfo,
	&IList_1_t8111_il2cpp_TypeInfo,
	&IEnumerable_1_t8112_il2cpp_TypeInfo,
	&ICollection_1_t3815_il2cpp_TypeInfo,
	&IList_1_t3819_il2cpp_TypeInfo,
	&IEnumerable_1_t585_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair ObjectTargetU5BU5D_t5541_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8107_il2cpp_TypeInfo, 21},
	{ &IList_1_t8108_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8109_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t8110_il2cpp_TypeInfo, 34},
	{ &IList_1_t8111_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t8112_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t3815_il2cpp_TypeInfo, 47},
	{ &IList_1_t3819_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t585_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType ObjectTargetU5BU5D_t5541_0_0_0;
extern Il2CppType ObjectTargetU5BU5D_t5541_1_0_0;
struct ObjectTarget_t554;
extern TypeInfo ObjectTarget_t554_il2cpp_TypeInfo;
TypeInfo ObjectTargetU5BU5D_t5541_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectTarget[]"/* name */
	, "Vuforia"/* namespaze */
	, ObjectTargetU5BU5D_t5541_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ObjectTarget_t554_il2cpp_TypeInfo/* element_class */
	, ObjectTargetU5BU5D_t5541_InterfacesTypeInfos/* implemented_interfaces */
	, ObjectTargetU5BU5D_t5541_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ObjectTargetU5BU5D_t5541_il2cpp_TypeInfo/* cast_class */
	, &ObjectTargetU5BU5D_t5541_0_0_0/* byval_arg */
	, &ObjectTargetU5BU5D_t5541_1_0_0/* this_arg */
	, ObjectTargetU5BU5D_t5541_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ExtendedTrackableU5BU5D_t5542_il2cpp_TypeInfo;



// Metadata Definition Vuforia.ExtendedTrackable[]
static MethodInfo* ExtendedTrackableU5BU5D_t5542_MethodInfos[] =
{
	NULL
};
static MethodInfo* ExtendedTrackableU5BU5D_t5542_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisExtendedTrackable_t780_m36311_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisExtendedTrackable_t780_m36312_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisExtendedTrackable_t780_m36313_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisExtendedTrackable_t780_m36314_MethodInfo,
	&Array_InternalArray__IndexOf_TisExtendedTrackable_t780_m36315_MethodInfo,
	&Array_InternalArray__Insert_TisExtendedTrackable_t780_m36316_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisExtendedTrackable_t780_m36310_MethodInfo,
	&Array_InternalArray__set_Item_TisExtendedTrackable_t780_m36318_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisExtendedTrackable_t780_m36319_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTrackable_t550_m35304_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTrackable_t550_m35305_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTrackable_t550_m35306_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTrackable_t550_m35307_MethodInfo,
	&Array_InternalArray__IndexOf_TisTrackable_t550_m35308_MethodInfo,
	&Array_InternalArray__Insert_TisTrackable_t550_m35309_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTrackable_t550_m35303_MethodInfo,
	&Array_InternalArray__set_Item_TisTrackable_t550_m35311_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTrackable_t550_m35312_MethodInfo,
};
static TypeInfo* ExtendedTrackableU5BU5D_t5542_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8110_il2cpp_TypeInfo,
	&IList_1_t8111_il2cpp_TypeInfo,
	&IEnumerable_1_t8112_il2cpp_TypeInfo,
	&ICollection_1_t3815_il2cpp_TypeInfo,
	&IList_1_t3819_il2cpp_TypeInfo,
	&IEnumerable_1_t585_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair ExtendedTrackableU5BU5D_t5542_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8110_il2cpp_TypeInfo, 21},
	{ &IList_1_t8111_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8112_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t3815_il2cpp_TypeInfo, 34},
	{ &IList_1_t3819_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t585_il2cpp_TypeInfo, 46},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType ExtendedTrackableU5BU5D_t5542_0_0_0;
extern Il2CppType ExtendedTrackableU5BU5D_t5542_1_0_0;
struct ExtendedTrackable_t780;
extern TypeInfo ExtendedTrackable_t780_il2cpp_TypeInfo;
TypeInfo ExtendedTrackableU5BU5D_t5542_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExtendedTrackable[]"/* name */
	, "Vuforia"/* namespaze */
	, ExtendedTrackableU5BU5D_t5542_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ExtendedTrackable_t780_il2cpp_TypeInfo/* element_class */
	, ExtendedTrackableU5BU5D_t5542_InterfacesTypeInfos/* implemented_interfaces */
	, ExtendedTrackableU5BU5D_t5542_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ExtendedTrackableU5BU5D_t5542_il2cpp_TypeInfo/* cast_class */
	, &ExtendedTrackableU5BU5D_t5542_0_0_0/* byval_arg */
	, &ExtendedTrackableU5BU5D_t5542_1_0_0/* this_arg */
	, ExtendedTrackableU5BU5D_t5542_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 47/* vtable_count */
	, 6/* interfaces_count */
	, 10/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo MeshRendererU5BU5D_t4386_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.MeshRenderer[]
static MethodInfo* MeshRendererU5BU5D_t4386_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisMeshRenderer_t168_m36349_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisMeshRenderer_t168_m36350_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisMeshRenderer_t168_m36351_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisMeshRenderer_t168_m36352_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisMeshRenderer_t168_m36353_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisMeshRenderer_t168_m36354_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMeshRenderer_t168_m36348_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisMeshRenderer_t168_m36356_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisMeshRenderer_t168_m36357_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisRenderer_t7_m31775_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisRenderer_t7_m31776_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisRenderer_t7_m31777_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisRenderer_t7_m31778_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisRenderer_t7_m31779_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisRenderer_t7_m31780_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRenderer_t7_m31774_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisRenderer_t7_m31782_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisRenderer_t7_m31783_MethodInfo;
static MethodInfo* MeshRendererU5BU5D_t4386_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisMeshRenderer_t168_m36349_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisMeshRenderer_t168_m36350_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisMeshRenderer_t168_m36351_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisMeshRenderer_t168_m36352_MethodInfo,
	&Array_InternalArray__IndexOf_TisMeshRenderer_t168_m36353_MethodInfo,
	&Array_InternalArray__Insert_TisMeshRenderer_t168_m36354_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisMeshRenderer_t168_m36348_MethodInfo,
	&Array_InternalArray__set_Item_TisMeshRenderer_t168_m36356_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisMeshRenderer_t168_m36357_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisRenderer_t7_m31775_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisRenderer_t7_m31776_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisRenderer_t7_m31777_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisRenderer_t7_m31778_MethodInfo,
	&Array_InternalArray__IndexOf_TisRenderer_t7_m31779_MethodInfo,
	&Array_InternalArray__Insert_TisRenderer_t7_m31780_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisRenderer_t7_m31774_MethodInfo,
	&Array_InternalArray__set_Item_TisRenderer_t7_m31782_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisRenderer_t7_m31783_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisComponent_t100_m31481_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisComponent_t100_m31482_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisComponent_t100_m31483_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisComponent_t100_m31484_MethodInfo,
	&Array_InternalArray__IndexOf_TisComponent_t100_m31485_MethodInfo,
	&Array_InternalArray__Insert_TisComponent_t100_m31486_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisComponent_t100_m31480_MethodInfo,
	&Array_InternalArray__set_Item_TisComponent_t100_m31488_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisComponent_t100_m31489_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t117_m31492_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t117_m31493_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t117_m31494_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t117_m31495_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t117_m31496_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t117_m31497_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t117_m31499_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t117_m31500_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8113_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8114_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8115_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7293_il2cpp_TypeInfo;
extern TypeInfo IList_1_t7294_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7295_il2cpp_TypeInfo;
static TypeInfo* MeshRendererU5BU5D_t4386_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8113_il2cpp_TypeInfo,
	&IList_1_t8114_il2cpp_TypeInfo,
	&IEnumerable_1_t8115_il2cpp_TypeInfo,
	&ICollection_1_t7293_il2cpp_TypeInfo,
	&IList_1_t7294_il2cpp_TypeInfo,
	&IEnumerable_1_t7295_il2cpp_TypeInfo,
	&ICollection_1_t3046_il2cpp_TypeInfo,
	&IList_1_t3050_il2cpp_TypeInfo,
	&IEnumerable_1_t3044_il2cpp_TypeInfo,
	&ICollection_1_t7225_il2cpp_TypeInfo,
	&IList_1_t7226_il2cpp_TypeInfo,
	&IEnumerable_1_t7227_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair MeshRendererU5BU5D_t4386_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8113_il2cpp_TypeInfo, 21},
	{ &IList_1_t8114_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8115_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7293_il2cpp_TypeInfo, 34},
	{ &IList_1_t7294_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7295_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t3046_il2cpp_TypeInfo, 47},
	{ &IList_1_t3050_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t3044_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7225_il2cpp_TypeInfo, 60},
	{ &IList_1_t7226_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7227_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 73},
	{ &IList_1_t2851_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType MeshRendererU5BU5D_t4386_0_0_0;
extern Il2CppType MeshRendererU5BU5D_t4386_1_0_0;
struct MeshRenderer_t168;
extern TypeInfo MeshRenderer_t168_il2cpp_TypeInfo;
TypeInfo MeshRendererU5BU5D_t4386_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "MeshRenderer[]"/* name */
	, "UnityEngine"/* namespaze */
	, MeshRendererU5BU5D_t4386_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &MeshRenderer_t168_il2cpp_TypeInfo/* element_class */
	, MeshRendererU5BU5D_t4386_InterfacesTypeInfos/* implemented_interfaces */
	, MeshRendererU5BU5D_t4386_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &MeshRendererU5BU5D_t4386_il2cpp_TypeInfo/* cast_class */
	, &MeshRendererU5BU5D_t4386_0_0_0/* byval_arg */
	, &MeshRendererU5BU5D_t4386_1_0_0/* this_arg */
	, MeshRendererU5BU5D_t4386_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (MeshRenderer_t168 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 86/* vtable_count */
	, 15/* interfaces_count */
	, 19/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SensitivityU5BU5D_t5543_il2cpp_TypeInfo;



// Metadata Definition Vuforia.VirtualButton/Sensitivity[]
static MethodInfo* SensitivityU5BU5D_t5543_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisSensitivity_t745_m36360_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisSensitivity_t745_m36361_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisSensitivity_t745_m36362_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisSensitivity_t745_m36363_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisSensitivity_t745_m36364_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisSensitivity_t745_m36365_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSensitivity_t745_m36359_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisSensitivity_t745_m36367_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisSensitivity_t745_m36368_MethodInfo;
static MethodInfo* SensitivityU5BU5D_t5543_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisSensitivity_t745_m36360_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisSensitivity_t745_m36361_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisSensitivity_t745_m36362_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisSensitivity_t745_m36363_MethodInfo,
	&Array_InternalArray__IndexOf_TisSensitivity_t745_m36364_MethodInfo,
	&Array_InternalArray__Insert_TisSensitivity_t745_m36365_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisSensitivity_t745_m36359_MethodInfo,
	&Array_InternalArray__set_Item_TisSensitivity_t745_m36367_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisSensitivity_t745_m36368_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8116_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8117_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8118_il2cpp_TypeInfo;
static TypeInfo* SensitivityU5BU5D_t5543_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8116_il2cpp_TypeInfo,
	&IList_1_t8117_il2cpp_TypeInfo,
	&IEnumerable_1_t8118_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair SensitivityU5BU5D_t5543_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8116_il2cpp_TypeInfo, 21},
	{ &IList_1_t8117_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8118_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType SensitivityU5BU5D_t5543_0_0_0;
extern Il2CppType SensitivityU5BU5D_t5543_1_0_0;
// Vuforia.VirtualButton/Sensitivity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton_Sensi.h"
extern TypeInfo Sensitivity_t745_il2cpp_TypeInfo;
TypeInfo SensitivityU5BU5D_t5543_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "Sensitivity[]"/* name */
	, ""/* namespaze */
	, SensitivityU5BU5D_t5543_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Sensitivity_t745_il2cpp_TypeInfo/* element_class */
	, SensitivityU5BU5D_t5543_InterfacesTypeInfos/* implemented_interfaces */
	, SensitivityU5BU5D_t5543_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &SensitivityU5BU5D_t5543_0_0_0/* byval_arg */
	, &SensitivityU5BU5D_t5543_1_0_0/* this_arg */
	, SensitivityU5BU5D_t5543_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WebCamDeviceU5BU5D_t886_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.WebCamDevice[]
static MethodInfo* WebCamDeviceU5BU5D_t886_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisWebCamDevice_t885_m36371_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisWebCamDevice_t885_m36372_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisWebCamDevice_t885_m36373_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisWebCamDevice_t885_m36374_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisWebCamDevice_t885_m36375_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisWebCamDevice_t885_m36376_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWebCamDevice_t885_m36370_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisWebCamDevice_t885_m36378_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisWebCamDevice_t885_m36379_MethodInfo;
static MethodInfo* WebCamDeviceU5BU5D_t886_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisWebCamDevice_t885_m36371_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisWebCamDevice_t885_m36372_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisWebCamDevice_t885_m36373_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisWebCamDevice_t885_m36374_MethodInfo,
	&Array_InternalArray__IndexOf_TisWebCamDevice_t885_m36375_MethodInfo,
	&Array_InternalArray__Insert_TisWebCamDevice_t885_m36376_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisWebCamDevice_t885_m36370_MethodInfo,
	&Array_InternalArray__set_Item_TisWebCamDevice_t885_m36378_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisWebCamDevice_t885_m36379_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8119_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8120_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8121_il2cpp_TypeInfo;
static TypeInfo* WebCamDeviceU5BU5D_t886_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8119_il2cpp_TypeInfo,
	&IList_1_t8120_il2cpp_TypeInfo,
	&IEnumerable_1_t8121_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair WebCamDeviceU5BU5D_t886_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8119_il2cpp_TypeInfo, 21},
	{ &IList_1_t8120_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8121_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType WebCamDeviceU5BU5D_t886_0_0_0;
extern Il2CppType WebCamDeviceU5BU5D_t886_1_0_0;
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDevice.h"
extern TypeInfo WebCamDevice_t885_il2cpp_TypeInfo;
TypeInfo WebCamDeviceU5BU5D_t886_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebCamDevice[]"/* name */
	, "UnityEngine"/* namespaze */
	, WebCamDeviceU5BU5D_t886_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WebCamDevice_t885_il2cpp_TypeInfo/* element_class */
	, WebCamDeviceU5BU5D_t886_InterfacesTypeInfos/* implemented_interfaces */
	, WebCamDeviceU5BU5D_t886_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WebCamDeviceU5BU5D_t886_il2cpp_TypeInfo/* cast_class */
	, &WebCamDeviceU5BU5D_t886_0_0_0/* byval_arg */
	, &WebCamDeviceU5BU5D_t886_1_0_0/* this_arg */
	, WebCamDeviceU5BU5D_t886_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (WebCamDevice_t885 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t4267_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
static MethodInfo* KeyValuePair_2U5BU5D_t4267_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4268_m36382_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4268_m36383_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4268_m36384_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4268_m36385_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t4268_m36386_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t4268_m36387_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t4268_m36381_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t4268_m36389_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4268_m36390_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t4267_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4268_m36382_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4268_m36383_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4268_m36384_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4268_m36385_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t4268_m36386_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t4268_m36387_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t4268_m36381_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t4268_m36389_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4268_m36390_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8122_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8123_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8124_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t4267_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8122_il2cpp_TypeInfo,
	&IList_1_t8123_il2cpp_TypeInfo,
	&IEnumerable_1_t8124_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t4267_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8122_il2cpp_TypeInfo, 21},
	{ &IList_1_t8123_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8124_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t4267_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t4267_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"
extern TypeInfo KeyValuePair_2_t4268_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t4267_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t4267_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t4268_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t4267_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t4267_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t4267_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t4267_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t4267_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t4267_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t4268 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ProfileDataU5BU5D_t4263_il2cpp_TypeInfo;



// Metadata Definition Vuforia.WebCamProfile/ProfileData[]
static MethodInfo* ProfileDataU5BU5D_t4263_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisProfileData_t747_m36393_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisProfileData_t747_m36394_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisProfileData_t747_m36395_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisProfileData_t747_m36396_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisProfileData_t747_m36397_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisProfileData_t747_m36398_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisProfileData_t747_m36392_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisProfileData_t747_m36400_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisProfileData_t747_m36401_MethodInfo;
static MethodInfo* ProfileDataU5BU5D_t4263_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisProfileData_t747_m36393_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisProfileData_t747_m36394_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisProfileData_t747_m36395_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisProfileData_t747_m36396_MethodInfo,
	&Array_InternalArray__IndexOf_TisProfileData_t747_m36397_MethodInfo,
	&Array_InternalArray__Insert_TisProfileData_t747_m36398_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisProfileData_t747_m36392_MethodInfo,
	&Array_InternalArray__set_Item_TisProfileData_t747_m36400_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisProfileData_t747_m36401_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8125_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8126_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8127_il2cpp_TypeInfo;
static TypeInfo* ProfileDataU5BU5D_t4263_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8125_il2cpp_TypeInfo,
	&IList_1_t8126_il2cpp_TypeInfo,
	&IEnumerable_1_t8127_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair ProfileDataU5BU5D_t4263_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8125_il2cpp_TypeInfo, 21},
	{ &IList_1_t8126_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8127_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType ProfileDataU5BU5D_t4263_0_0_0;
extern Il2CppType ProfileDataU5BU5D_t4263_1_0_0;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi_0.h"
extern TypeInfo ProfileData_t747_il2cpp_TypeInfo;
TypeInfo ProfileDataU5BU5D_t4263_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "ProfileData[]"/* name */
	, ""/* namespaze */
	, ProfileDataU5BU5D_t4263_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ProfileData_t747_il2cpp_TypeInfo/* element_class */
	, ProfileDataU5BU5D_t4263_InterfacesTypeInfos/* implemented_interfaces */
	, ProfileDataU5BU5D_t4263_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ProfileDataU5BU5D_t4263_il2cpp_TypeInfo/* cast_class */
	, &ProfileDataU5BU5D_t4263_0_0_0/* byval_arg */
	, &ProfileDataU5BU5D_t4263_1_0_0/* this_arg */
	, ProfileDataU5BU5D_t4263_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (ProfileData_t747 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048842/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, true/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t4287_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>[]
static MethodInfo* KeyValuePair_2U5BU5D_t4287_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4288_m36415_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4288_m36416_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4288_m36417_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4288_m36418_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t4288_m36419_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t4288_m36420_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t4288_m36414_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t4288_m36422_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4288_m36423_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t4287_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4288_m36415_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4288_m36416_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4288_m36417_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4288_m36418_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t4288_m36419_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t4288_m36420_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t4288_m36414_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t4288_m36422_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4288_m36423_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8128_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8129_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8130_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t4287_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8128_il2cpp_TypeInfo,
	&IList_1_t8129_il2cpp_TypeInfo,
	&IEnumerable_1_t8130_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t4287_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8128_il2cpp_TypeInfo, 21},
	{ &IList_1_t8129_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8130_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t4287_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t4287_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_23.h"
extern TypeInfo KeyValuePair_2_t4288_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t4287_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t4287_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t4288_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t4287_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t4287_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t4287_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t4287_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t4287_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t4287_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t4288 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InitErrorU5BU5D_t5544_il2cpp_TypeInfo;



// Metadata Definition Vuforia.QCARUnity/InitError[]
static MethodInfo* InitErrorU5BU5D_t5544_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisInitError_t128_m36439_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisInitError_t128_m36440_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisInitError_t128_m36441_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisInitError_t128_m36442_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisInitError_t128_m36443_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisInitError_t128_m36444_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisInitError_t128_m36438_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisInitError_t128_m36446_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisInitError_t128_m36447_MethodInfo;
static MethodInfo* InitErrorU5BU5D_t5544_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisInitError_t128_m36439_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisInitError_t128_m36440_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisInitError_t128_m36441_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisInitError_t128_m36442_MethodInfo,
	&Array_InternalArray__IndexOf_TisInitError_t128_m36443_MethodInfo,
	&Array_InternalArray__Insert_TisInitError_t128_m36444_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisInitError_t128_m36438_MethodInfo,
	&Array_InternalArray__set_Item_TisInitError_t128_m36446_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisInitError_t128_m36447_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8131_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8132_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8133_il2cpp_TypeInfo;
static TypeInfo* InitErrorU5BU5D_t5544_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8131_il2cpp_TypeInfo,
	&IList_1_t8132_il2cpp_TypeInfo,
	&IEnumerable_1_t8133_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InitErrorU5BU5D_t5544_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8131_il2cpp_TypeInfo, 21},
	{ &IList_1_t8132_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8133_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType InitErrorU5BU5D_t5544_0_0_0;
extern Il2CppType InitErrorU5BU5D_t5544_1_0_0;
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"
extern TypeInfo InitError_t128_il2cpp_TypeInfo;
TypeInfo InitErrorU5BU5D_t5544_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "InitError[]"/* name */
	, ""/* namespaze */
	, InitErrorU5BU5D_t5544_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InitError_t128_il2cpp_TypeInfo/* element_class */
	, InitErrorU5BU5D_t5544_InterfacesTypeInfos/* implemented_interfaces */
	, InitErrorU5BU5D_t5544_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &InitErrorU5BU5D_t5544_0_0_0/* byval_arg */
	, &InitErrorU5BU5D_t5544_1_0_0/* this_arg */
	, InitErrorU5BU5D_t5544_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo QCARHintU5BU5D_t5545_il2cpp_TypeInfo;



// Metadata Definition Vuforia.QCARUnity/QCARHint[]
static MethodInfo* QCARHintU5BU5D_t5545_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisQCARHint_t752_m36450_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisQCARHint_t752_m36451_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisQCARHint_t752_m36452_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisQCARHint_t752_m36453_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisQCARHint_t752_m36454_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisQCARHint_t752_m36455_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisQCARHint_t752_m36449_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisQCARHint_t752_m36457_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisQCARHint_t752_m36458_MethodInfo;
static MethodInfo* QCARHintU5BU5D_t5545_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisQCARHint_t752_m36450_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisQCARHint_t752_m36451_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisQCARHint_t752_m36452_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisQCARHint_t752_m36453_MethodInfo,
	&Array_InternalArray__IndexOf_TisQCARHint_t752_m36454_MethodInfo,
	&Array_InternalArray__Insert_TisQCARHint_t752_m36455_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisQCARHint_t752_m36449_MethodInfo,
	&Array_InternalArray__set_Item_TisQCARHint_t752_m36457_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisQCARHint_t752_m36458_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8134_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8135_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8136_il2cpp_TypeInfo;
static TypeInfo* QCARHintU5BU5D_t5545_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8134_il2cpp_TypeInfo,
	&IList_1_t8135_il2cpp_TypeInfo,
	&IEnumerable_1_t8136_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair QCARHintU5BU5D_t5545_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8134_il2cpp_TypeInfo, 21},
	{ &IList_1_t8135_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8136_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType QCARHintU5BU5D_t5545_0_0_0;
extern Il2CppType QCARHintU5BU5D_t5545_1_0_0;
// Vuforia.QCARUnity/QCARHint
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_QCARHint.h"
extern TypeInfo QCARHint_t752_il2cpp_TypeInfo;
TypeInfo QCARHintU5BU5D_t5545_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "QCARHint[]"/* name */
	, ""/* namespaze */
	, QCARHintU5BU5D_t5545_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &QCARHint_t752_il2cpp_TypeInfo/* element_class */
	, QCARHintU5BU5D_t5545_InterfacesTypeInfos/* implemented_interfaces */
	, QCARHintU5BU5D_t5545_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &QCARHintU5BU5D_t5545_0_0_0/* byval_arg */
	, &QCARHintU5BU5D_t5545_1_0_0/* this_arg */
	, QCARHintU5BU5D_t5545_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo StorageTypeU5BU5D_t5546_il2cpp_TypeInfo;



// Metadata Definition Vuforia.QCARUnity/StorageType[]
static MethodInfo* StorageTypeU5BU5D_t5546_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisStorageType_t753_m36461_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisStorageType_t753_m36462_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisStorageType_t753_m36463_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisStorageType_t753_m36464_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisStorageType_t753_m36465_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisStorageType_t753_m36466_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisStorageType_t753_m36460_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisStorageType_t753_m36468_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisStorageType_t753_m36469_MethodInfo;
static MethodInfo* StorageTypeU5BU5D_t5546_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisStorageType_t753_m36461_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisStorageType_t753_m36462_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisStorageType_t753_m36463_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisStorageType_t753_m36464_MethodInfo,
	&Array_InternalArray__IndexOf_TisStorageType_t753_m36465_MethodInfo,
	&Array_InternalArray__Insert_TisStorageType_t753_m36466_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisStorageType_t753_m36460_MethodInfo,
	&Array_InternalArray__set_Item_TisStorageType_t753_m36468_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisStorageType_t753_m36469_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8137_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8138_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8139_il2cpp_TypeInfo;
static TypeInfo* StorageTypeU5BU5D_t5546_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8137_il2cpp_TypeInfo,
	&IList_1_t8138_il2cpp_TypeInfo,
	&IEnumerable_1_t8139_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair StorageTypeU5BU5D_t5546_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8137_il2cpp_TypeInfo, 21},
	{ &IList_1_t8138_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8139_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType StorageTypeU5BU5D_t5546_0_0_0;
extern Il2CppType StorageTypeU5BU5D_t5546_1_0_0;
// Vuforia.QCARUnity/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_StorageTy.h"
extern TypeInfo StorageType_t753_il2cpp_TypeInfo;
TypeInfo StorageTypeU5BU5D_t5546_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "StorageType[]"/* name */
	, ""/* namespaze */
	, StorageTypeU5BU5D_t5546_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &StorageType_t753_il2cpp_TypeInfo/* element_class */
	, StorageTypeU5BU5D_t5546_InterfacesTypeInfos/* implemented_interfaces */
	, StorageTypeU5BU5D_t5546_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &StorageTypeU5BU5D_t5546_0_0_0/* byval_arg */
	, &StorageTypeU5BU5D_t5546_1_0_0/* this_arg */
	, StorageTypeU5BU5D_t5546_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ITrackerEventHandlerU5BU5D_t4313_il2cpp_TypeInfo;



// Metadata Definition Vuforia.ITrackerEventHandler[]
static MethodInfo* ITrackerEventHandlerU5BU5D_t4313_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisITrackerEventHandler_t761_m36473_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisITrackerEventHandler_t761_m36474_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisITrackerEventHandler_t761_m36475_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisITrackerEventHandler_t761_m36476_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisITrackerEventHandler_t761_m36477_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisITrackerEventHandler_t761_m36478_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisITrackerEventHandler_t761_m36472_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisITrackerEventHandler_t761_m36480_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisITrackerEventHandler_t761_m36481_MethodInfo;
static MethodInfo* ITrackerEventHandlerU5BU5D_t4313_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisITrackerEventHandler_t761_m36473_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisITrackerEventHandler_t761_m36474_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisITrackerEventHandler_t761_m36475_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisITrackerEventHandler_t761_m36476_MethodInfo,
	&Array_InternalArray__IndexOf_TisITrackerEventHandler_t761_m36477_MethodInfo,
	&Array_InternalArray__Insert_TisITrackerEventHandler_t761_m36478_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisITrackerEventHandler_t761_m36472_MethodInfo,
	&Array_InternalArray__set_Item_TisITrackerEventHandler_t761_m36480_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisITrackerEventHandler_t761_m36481_MethodInfo,
};
extern TypeInfo ICollection_1_t4316_il2cpp_TypeInfo;
extern TypeInfo IList_1_t4321_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t4314_il2cpp_TypeInfo;
static TypeInfo* ITrackerEventHandlerU5BU5D_t4313_InterfacesTypeInfos[] = 
{
	&ICollection_1_t4316_il2cpp_TypeInfo,
	&IList_1_t4321_il2cpp_TypeInfo,
	&IEnumerable_1_t4314_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair ITrackerEventHandlerU5BU5D_t4313_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t4316_il2cpp_TypeInfo, 21},
	{ &IList_1_t4321_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t4314_il2cpp_TypeInfo, 33},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType ITrackerEventHandlerU5BU5D_t4313_0_0_0;
extern Il2CppType ITrackerEventHandlerU5BU5D_t4313_1_0_0;
struct ITrackerEventHandler_t761;
extern TypeInfo ITrackerEventHandler_t761_il2cpp_TypeInfo;
extern CustomAttributesCache ITrackerEventHandler_t761__CustomAttributeCache;
TypeInfo ITrackerEventHandlerU5BU5D_t4313_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "ITrackerEventHandler[]"/* name */
	, "Vuforia"/* namespaze */
	, ITrackerEventHandlerU5BU5D_t4313_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ITrackerEventHandler_t761_il2cpp_TypeInfo/* element_class */
	, ITrackerEventHandlerU5BU5D_t4313_InterfacesTypeInfos/* implemented_interfaces */
	, ITrackerEventHandlerU5BU5D_t4313_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ITrackerEventHandlerU5BU5D_t4313_il2cpp_TypeInfo/* cast_class */
	, &ITrackerEventHandlerU5BU5D_t4313_0_0_0/* byval_arg */
	, &ITrackerEventHandlerU5BU5D_t4313_1_0_0/* this_arg */
	, ITrackerEventHandlerU5BU5D_t4313_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 3/* interfaces_count */
	, 7/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WorldCenterModeU5BU5D_t5547_il2cpp_TypeInfo;



// Metadata Definition Vuforia.QCARAbstractBehaviour/WorldCenterMode[]
static MethodInfo* WorldCenterModeU5BU5D_t5547_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisWorldCenterMode_t755_m36516_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisWorldCenterMode_t755_m36517_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisWorldCenterMode_t755_m36518_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisWorldCenterMode_t755_m36519_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisWorldCenterMode_t755_m36520_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisWorldCenterMode_t755_m36521_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWorldCenterMode_t755_m36515_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisWorldCenterMode_t755_m36523_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisWorldCenterMode_t755_m36524_MethodInfo;
static MethodInfo* WorldCenterModeU5BU5D_t5547_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisWorldCenterMode_t755_m36516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisWorldCenterMode_t755_m36517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisWorldCenterMode_t755_m36518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisWorldCenterMode_t755_m36519_MethodInfo,
	&Array_InternalArray__IndexOf_TisWorldCenterMode_t755_m36520_MethodInfo,
	&Array_InternalArray__Insert_TisWorldCenterMode_t755_m36521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisWorldCenterMode_t755_m36515_MethodInfo,
	&Array_InternalArray__set_Item_TisWorldCenterMode_t755_m36523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisWorldCenterMode_t755_m36524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8140_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8141_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8142_il2cpp_TypeInfo;
static TypeInfo* WorldCenterModeU5BU5D_t5547_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8140_il2cpp_TypeInfo,
	&IList_1_t8141_il2cpp_TypeInfo,
	&IEnumerable_1_t8142_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair WorldCenterModeU5BU5D_t5547_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8140_il2cpp_TypeInfo, 21},
	{ &IList_1_t8141_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8142_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WorldCenterModeU5BU5D_t5547_0_0_0;
extern Il2CppType WorldCenterModeU5BU5D_t5547_1_0_0;
// Vuforia.QCARAbstractBehaviour/WorldCenterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio_0.h"
extern TypeInfo WorldCenterMode_t755_il2cpp_TypeInfo;
TypeInfo WorldCenterModeU5BU5D_t5547_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WorldCenterMode[]"/* name */
	, ""/* namespaze */
	, WorldCenterModeU5BU5D_t5547_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WorldCenterMode_t755_il2cpp_TypeInfo/* element_class */
	, WorldCenterModeU5BU5D_t5547_InterfacesTypeInfos/* implemented_interfaces */
	, WorldCenterModeU5BU5D_t5547_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &WorldCenterModeU5BU5D_t5547_0_0_0/* byval_arg */
	, &WorldCenterModeU5BU5D_t5547_1_0_0/* this_arg */
	, WorldCenterModeU5BU5D_t5547_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WebCamUsedU5BU5D_t5548_il2cpp_TypeInfo;



// Metadata Definition Vuforia.QCARRuntimeUtilities/WebCamUsed[]
static MethodInfo* WebCamUsedU5BU5D_t5548_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisWebCamUsed_t763_m36527_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisWebCamUsed_t763_m36528_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisWebCamUsed_t763_m36529_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisWebCamUsed_t763_m36530_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisWebCamUsed_t763_m36531_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisWebCamUsed_t763_m36532_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWebCamUsed_t763_m36526_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisWebCamUsed_t763_m36534_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisWebCamUsed_t763_m36535_MethodInfo;
static MethodInfo* WebCamUsedU5BU5D_t5548_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisWebCamUsed_t763_m36527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisWebCamUsed_t763_m36528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisWebCamUsed_t763_m36529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisWebCamUsed_t763_m36530_MethodInfo,
	&Array_InternalArray__IndexOf_TisWebCamUsed_t763_m36531_MethodInfo,
	&Array_InternalArray__Insert_TisWebCamUsed_t763_m36532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisWebCamUsed_t763_m36526_MethodInfo,
	&Array_InternalArray__set_Item_TisWebCamUsed_t763_m36534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisWebCamUsed_t763_m36535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8143_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8144_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8145_il2cpp_TypeInfo;
static TypeInfo* WebCamUsedU5BU5D_t5548_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8143_il2cpp_TypeInfo,
	&IList_1_t8144_il2cpp_TypeInfo,
	&IEnumerable_1_t8145_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair WebCamUsedU5BU5D_t5548_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8143_il2cpp_TypeInfo, 21},
	{ &IList_1_t8144_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8145_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WebCamUsedU5BU5D_t5548_0_0_0;
extern Il2CppType WebCamUsedU5BU5D_t5548_1_0_0;
// Vuforia.QCARRuntimeUtilities/WebCamUsed
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitie_0.h"
extern TypeInfo WebCamUsed_t763_il2cpp_TypeInfo;
TypeInfo WebCamUsedU5BU5D_t5548_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebCamUsed[]"/* name */
	, ""/* namespaze */
	, WebCamUsedU5BU5D_t5548_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WebCamUsed_t763_il2cpp_TypeInfo/* element_class */
	, WebCamUsedU5BU5D_t5548_InterfacesTypeInfos/* implemented_interfaces */
	, WebCamUsedU5BU5D_t5548_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &WebCamUsedU5BU5D_t5548_0_0_0/* byval_arg */
	, &WebCamUsedU5BU5D_t5548_1_0_0/* this_arg */
	, WebCamUsedU5BU5D_t5548_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ITextRecoEventHandlerU5BU5D_t4347_il2cpp_TypeInfo;



// Metadata Definition Vuforia.ITextRecoEventHandler[]
static MethodInfo* ITextRecoEventHandlerU5BU5D_t4347_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisITextRecoEventHandler_t765_m36539_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisITextRecoEventHandler_t765_m36540_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisITextRecoEventHandler_t765_m36541_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisITextRecoEventHandler_t765_m36542_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisITextRecoEventHandler_t765_m36543_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisITextRecoEventHandler_t765_m36544_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisITextRecoEventHandler_t765_m36538_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisITextRecoEventHandler_t765_m36546_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisITextRecoEventHandler_t765_m36547_MethodInfo;
static MethodInfo* ITextRecoEventHandlerU5BU5D_t4347_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisITextRecoEventHandler_t765_m36539_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisITextRecoEventHandler_t765_m36540_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisITextRecoEventHandler_t765_m36541_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisITextRecoEventHandler_t765_m36542_MethodInfo,
	&Array_InternalArray__IndexOf_TisITextRecoEventHandler_t765_m36543_MethodInfo,
	&Array_InternalArray__Insert_TisITextRecoEventHandler_t765_m36544_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisITextRecoEventHandler_t765_m36538_MethodInfo,
	&Array_InternalArray__set_Item_TisITextRecoEventHandler_t765_m36546_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisITextRecoEventHandler_t765_m36547_MethodInfo,
};
extern TypeInfo ICollection_1_t4350_il2cpp_TypeInfo;
extern TypeInfo IList_1_t4355_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t4348_il2cpp_TypeInfo;
static TypeInfo* ITextRecoEventHandlerU5BU5D_t4347_InterfacesTypeInfos[] = 
{
	&ICollection_1_t4350_il2cpp_TypeInfo,
	&IList_1_t4355_il2cpp_TypeInfo,
	&IEnumerable_1_t4348_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair ITextRecoEventHandlerU5BU5D_t4347_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t4350_il2cpp_TypeInfo, 21},
	{ &IList_1_t4355_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t4348_il2cpp_TypeInfo, 33},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType ITextRecoEventHandlerU5BU5D_t4347_0_0_0;
extern Il2CppType ITextRecoEventHandlerU5BU5D_t4347_1_0_0;
struct ITextRecoEventHandler_t765;
extern TypeInfo ITextRecoEventHandler_t765_il2cpp_TypeInfo;
TypeInfo ITextRecoEventHandlerU5BU5D_t4347_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "ITextRecoEventHandler[]"/* name */
	, "Vuforia"/* namespaze */
	, ITextRecoEventHandlerU5BU5D_t4347_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ITextRecoEventHandler_t765_il2cpp_TypeInfo/* element_class */
	, ITextRecoEventHandlerU5BU5D_t4347_InterfacesTypeInfos/* implemented_interfaces */
	, ITextRecoEventHandlerU5BU5D_t4347_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ITextRecoEventHandlerU5BU5D_t4347_il2cpp_TypeInfo/* cast_class */
	, &ITextRecoEventHandlerU5BU5D_t4347_0_0_0/* byval_arg */
	, &ITextRecoEventHandlerU5BU5D_t4347_1_0_0/* this_arg */
	, ITextRecoEventHandlerU5BU5D_t4347_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 3/* interfaces_count */
	, 7/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IUserDefinedTargetEventHandlerU5BU5D_t4367_il2cpp_TypeInfo;



// Metadata Definition Vuforia.IUserDefinedTargetEventHandler[]
static MethodInfo* IUserDefinedTargetEventHandlerU5BU5D_t4367_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisIUserDefinedTargetEventHandler_t768_m36567_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisIUserDefinedTargetEventHandler_t768_m36568_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisIUserDefinedTargetEventHandler_t768_m36569_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisIUserDefinedTargetEventHandler_t768_m36570_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisIUserDefinedTargetEventHandler_t768_m36571_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisIUserDefinedTargetEventHandler_t768_m36572_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIUserDefinedTargetEventHandler_t768_m36566_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisIUserDefinedTargetEventHandler_t768_m36574_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisIUserDefinedTargetEventHandler_t768_m36575_MethodInfo;
static MethodInfo* IUserDefinedTargetEventHandlerU5BU5D_t4367_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIUserDefinedTargetEventHandler_t768_m36567_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIUserDefinedTargetEventHandler_t768_m36568_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIUserDefinedTargetEventHandler_t768_m36569_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIUserDefinedTargetEventHandler_t768_m36570_MethodInfo,
	&Array_InternalArray__IndexOf_TisIUserDefinedTargetEventHandler_t768_m36571_MethodInfo,
	&Array_InternalArray__Insert_TisIUserDefinedTargetEventHandler_t768_m36572_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIUserDefinedTargetEventHandler_t768_m36566_MethodInfo,
	&Array_InternalArray__set_Item_TisIUserDefinedTargetEventHandler_t768_m36574_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIUserDefinedTargetEventHandler_t768_m36575_MethodInfo,
};
extern TypeInfo ICollection_1_t4370_il2cpp_TypeInfo;
extern TypeInfo IList_1_t4375_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t4368_il2cpp_TypeInfo;
static TypeInfo* IUserDefinedTargetEventHandlerU5BU5D_t4367_InterfacesTypeInfos[] = 
{
	&ICollection_1_t4370_il2cpp_TypeInfo,
	&IList_1_t4375_il2cpp_TypeInfo,
	&IEnumerable_1_t4368_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair IUserDefinedTargetEventHandlerU5BU5D_t4367_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t4370_il2cpp_TypeInfo, 21},
	{ &IList_1_t4375_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t4368_il2cpp_TypeInfo, 33},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType IUserDefinedTargetEventHandlerU5BU5D_t4367_0_0_0;
extern Il2CppType IUserDefinedTargetEventHandlerU5BU5D_t4367_1_0_0;
struct IUserDefinedTargetEventHandler_t768;
extern TypeInfo IUserDefinedTargetEventHandler_t768_il2cpp_TypeInfo;
TypeInfo IUserDefinedTargetEventHandlerU5BU5D_t4367_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "IUserDefinedTargetEventHandler[]"/* name */
	, "Vuforia"/* namespaze */
	, IUserDefinedTargetEventHandlerU5BU5D_t4367_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IUserDefinedTargetEventHandler_t768_il2cpp_TypeInfo/* element_class */
	, IUserDefinedTargetEventHandlerU5BU5D_t4367_InterfacesTypeInfos/* implemented_interfaces */
	, IUserDefinedTargetEventHandlerU5BU5D_t4367_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IUserDefinedTargetEventHandlerU5BU5D_t4367_il2cpp_TypeInfo/* cast_class */
	, &IUserDefinedTargetEventHandlerU5BU5D_t4367_0_0_0/* byval_arg */
	, &IUserDefinedTargetEventHandlerU5BU5D_t4367_1_0_0/* this_arg */
	, IUserDefinedTargetEventHandlerU5BU5D_t4367_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 3/* interfaces_count */
	, 7/* interface_offsets_count */

};
#include "System.Core_ArrayTypes.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo LinkU5BU5D_t4390_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.HashSet`1/Link<System.Object>[]
static MethodInfo* LinkU5BU5D_t4390_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisLink_t4391_m36594_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisLink_t4391_m36595_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisLink_t4391_m36596_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisLink_t4391_m36597_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisLink_t4391_m36598_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisLink_t4391_m36599_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisLink_t4391_m36593_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisLink_t4391_m36601_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t4391_m36602_MethodInfo;
static MethodInfo* LinkU5BU5D_t4390_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisLink_t4391_m36594_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisLink_t4391_m36595_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisLink_t4391_m36596_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisLink_t4391_m36597_MethodInfo,
	&Array_InternalArray__IndexOf_TisLink_t4391_m36598_MethodInfo,
	&Array_InternalArray__Insert_TisLink_t4391_m36599_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisLink_t4391_m36593_MethodInfo,
	&Array_InternalArray__set_Item_TisLink_t4391_m36601_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t4391_m36602_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8146_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8147_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8148_il2cpp_TypeInfo;
static TypeInfo* LinkU5BU5D_t4390_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8146_il2cpp_TypeInfo,
	&IList_1_t8147_il2cpp_TypeInfo,
	&IEnumerable_1_t8148_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair LinkU5BU5D_t4390_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8146_il2cpp_TypeInfo, 21},
	{ &IList_1_t8147_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8148_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType LinkU5BU5D_t4390_0_0_0;
extern Il2CppType LinkU5BU5D_t4390_1_0_0;
// System.Collections.Generic.HashSet`1/Link<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen.h"
extern TypeInfo Link_t4391_il2cpp_TypeInfo;
TypeInfo LinkU5BU5D_t4390_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Link[]"/* name */
	, ""/* namespaze */
	, LinkU5BU5D_t4390_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Link_t4391_il2cpp_TypeInfo/* element_class */
	, LinkU5BU5D_t4390_InterfacesTypeInfos/* implemented_interfaces */
	, LinkU5BU5D_t4390_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &LinkU5BU5D_t4390_il2cpp_TypeInfo/* cast_class */
	, &LinkU5BU5D_t4390_0_0_0/* byval_arg */
	, &LinkU5BU5D_t4390_1_0_0/* this_arg */
	, LinkU5BU5D_t4390_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Link_t4391 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, true/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo LinkU5BU5D_t4384_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>[]
static MethodInfo* LinkU5BU5D_t4384_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisLink_t4385_m36605_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisLink_t4385_m36606_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisLink_t4385_m36607_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisLink_t4385_m36608_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisLink_t4385_m36609_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisLink_t4385_m36610_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisLink_t4385_m36604_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisLink_t4385_m36612_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t4385_m36613_MethodInfo;
static MethodInfo* LinkU5BU5D_t4384_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisLink_t4385_m36605_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisLink_t4385_m36606_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisLink_t4385_m36607_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisLink_t4385_m36608_MethodInfo,
	&Array_InternalArray__IndexOf_TisLink_t4385_m36609_MethodInfo,
	&Array_InternalArray__Insert_TisLink_t4385_m36610_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisLink_t4385_m36604_MethodInfo,
	&Array_InternalArray__set_Item_TisLink_t4385_m36612_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisLink_t4385_m36613_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8149_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8150_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8151_il2cpp_TypeInfo;
static TypeInfo* LinkU5BU5D_t4384_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8149_il2cpp_TypeInfo,
	&IList_1_t8150_il2cpp_TypeInfo,
	&IEnumerable_1_t8151_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair LinkU5BU5D_t4384_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8149_il2cpp_TypeInfo, 21},
	{ &IList_1_t8150_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8151_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType LinkU5BU5D_t4384_0_0_0;
extern Il2CppType LinkU5BU5D_t4384_1_0_0;
// System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen_0.h"
extern TypeInfo Link_t4385_il2cpp_TypeInfo;
TypeInfo LinkU5BU5D_t4384_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Link[]"/* name */
	, ""/* namespaze */
	, LinkU5BU5D_t4384_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Link_t4385_il2cpp_TypeInfo/* element_class */
	, LinkU5BU5D_t4384_InterfacesTypeInfos/* implemented_interfaces */
	, LinkU5BU5D_t4384_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &LinkU5BU5D_t4384_il2cpp_TypeInfo/* cast_class */
	, &LinkU5BU5D_t4384_0_0_0/* byval_arg */
	, &LinkU5BU5D_t4384_1_0_0/* this_arg */
	, LinkU5BU5D_t4384_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Link_t4385 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048843/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, true/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IVirtualButtonEventHandlerU5BU5D_t4406_il2cpp_TypeInfo;



// Metadata Definition Vuforia.IVirtualButtonEventHandler[]
static MethodInfo* IVirtualButtonEventHandlerU5BU5D_t4406_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisIVirtualButtonEventHandler_t771_m36618_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisIVirtualButtonEventHandler_t771_m36619_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisIVirtualButtonEventHandler_t771_m36620_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisIVirtualButtonEventHandler_t771_m36621_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisIVirtualButtonEventHandler_t771_m36622_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisIVirtualButtonEventHandler_t771_m36623_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIVirtualButtonEventHandler_t771_m36617_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisIVirtualButtonEventHandler_t771_m36625_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisIVirtualButtonEventHandler_t771_m36626_MethodInfo;
static MethodInfo* IVirtualButtonEventHandlerU5BU5D_t4406_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIVirtualButtonEventHandler_t771_m36618_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIVirtualButtonEventHandler_t771_m36619_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIVirtualButtonEventHandler_t771_m36620_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIVirtualButtonEventHandler_t771_m36621_MethodInfo,
	&Array_InternalArray__IndexOf_TisIVirtualButtonEventHandler_t771_m36622_MethodInfo,
	&Array_InternalArray__Insert_TisIVirtualButtonEventHandler_t771_m36623_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIVirtualButtonEventHandler_t771_m36617_MethodInfo,
	&Array_InternalArray__set_Item_TisIVirtualButtonEventHandler_t771_m36625_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIVirtualButtonEventHandler_t771_m36626_MethodInfo,
};
extern TypeInfo ICollection_1_t4409_il2cpp_TypeInfo;
extern TypeInfo IList_1_t4414_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t4407_il2cpp_TypeInfo;
static TypeInfo* IVirtualButtonEventHandlerU5BU5D_t4406_InterfacesTypeInfos[] = 
{
	&ICollection_1_t4409_il2cpp_TypeInfo,
	&IList_1_t4414_il2cpp_TypeInfo,
	&IEnumerable_1_t4407_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair IVirtualButtonEventHandlerU5BU5D_t4406_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t4409_il2cpp_TypeInfo, 21},
	{ &IList_1_t4414_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t4407_il2cpp_TypeInfo, 33},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType IVirtualButtonEventHandlerU5BU5D_t4406_0_0_0;
extern Il2CppType IVirtualButtonEventHandlerU5BU5D_t4406_1_0_0;
struct IVirtualButtonEventHandler_t771;
extern TypeInfo IVirtualButtonEventHandler_t771_il2cpp_TypeInfo;
TypeInfo IVirtualButtonEventHandlerU5BU5D_t4406_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "IVirtualButtonEventHandler[]"/* name */
	, "Vuforia"/* namespaze */
	, IVirtualButtonEventHandlerU5BU5D_t4406_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IVirtualButtonEventHandler_t771_il2cpp_TypeInfo/* element_class */
	, IVirtualButtonEventHandlerU5BU5D_t4406_InterfacesTypeInfos/* implemented_interfaces */
	, IVirtualButtonEventHandlerU5BU5D_t4406_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IVirtualButtonEventHandlerU5BU5D_t4406_il2cpp_TypeInfo/* cast_class */
	, &IVirtualButtonEventHandlerU5BU5D_t4406_0_0_0/* byval_arg */
	, &IVirtualButtonEventHandlerU5BU5D_t4406_1_0_0/* this_arg */
	, IVirtualButtonEventHandlerU5BU5D_t4406_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 3/* interfaces_count */
	, 7/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WordFilterModeU5BU5D_t5549_il2cpp_TypeInfo;



// Metadata Definition Vuforia.WordFilterMode[]
static MethodInfo* WordFilterModeU5BU5D_t5549_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisWordFilterMode_t772_m36647_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisWordFilterMode_t772_m36648_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisWordFilterMode_t772_m36649_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisWordFilterMode_t772_m36650_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisWordFilterMode_t772_m36651_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisWordFilterMode_t772_m36652_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWordFilterMode_t772_m36646_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisWordFilterMode_t772_m36654_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisWordFilterMode_t772_m36655_MethodInfo;
static MethodInfo* WordFilterModeU5BU5D_t5549_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisWordFilterMode_t772_m36647_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisWordFilterMode_t772_m36648_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisWordFilterMode_t772_m36649_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisWordFilterMode_t772_m36650_MethodInfo,
	&Array_InternalArray__IndexOf_TisWordFilterMode_t772_m36651_MethodInfo,
	&Array_InternalArray__Insert_TisWordFilterMode_t772_m36652_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisWordFilterMode_t772_m36646_MethodInfo,
	&Array_InternalArray__set_Item_TisWordFilterMode_t772_m36654_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisWordFilterMode_t772_m36655_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8152_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8153_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8154_il2cpp_TypeInfo;
static TypeInfo* WordFilterModeU5BU5D_t5549_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8152_il2cpp_TypeInfo,
	&IList_1_t8153_il2cpp_TypeInfo,
	&IEnumerable_1_t8154_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair WordFilterModeU5BU5D_t5549_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8152_il2cpp_TypeInfo, 21},
	{ &IList_1_t8153_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8154_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WordFilterModeU5BU5D_t5549_0_0_0;
extern Il2CppType WordFilterModeU5BU5D_t5549_1_0_0;
// Vuforia.WordFilterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilterMode.h"
extern TypeInfo WordFilterMode_t772_il2cpp_TypeInfo;
TypeInfo WordFilterModeU5BU5D_t5549_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WordFilterMode[]"/* name */
	, "Vuforia"/* namespaze */
	, WordFilterModeU5BU5D_t5549_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WordFilterMode_t772_il2cpp_TypeInfo/* element_class */
	, WordFilterModeU5BU5D_t5549_InterfacesTypeInfos/* implemented_interfaces */
	, WordFilterModeU5BU5D_t5549_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &WordFilterModeU5BU5D_t5549_0_0_0/* byval_arg */
	, &WordFilterModeU5BU5D_t5549_1_0_0/* this_arg */
	, WordFilterModeU5BU5D_t5549_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo AssetBundleU5BU5D_t5413_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.AssetBundle[]
static MethodInfo* AssetBundleU5BU5D_t5413_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisAssetBundle_t910_m36658_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisAssetBundle_t910_m36659_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisAssetBundle_t910_m36660_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisAssetBundle_t910_m36661_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisAssetBundle_t910_m36662_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisAssetBundle_t910_m36663_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssetBundle_t910_m36657_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisAssetBundle_t910_m36665_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisAssetBundle_t910_m36666_MethodInfo;
static MethodInfo* AssetBundleU5BU5D_t5413_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisAssetBundle_t910_m36658_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisAssetBundle_t910_m36659_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisAssetBundle_t910_m36660_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisAssetBundle_t910_m36661_MethodInfo,
	&Array_InternalArray__IndexOf_TisAssetBundle_t910_m36662_MethodInfo,
	&Array_InternalArray__Insert_TisAssetBundle_t910_m36663_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisAssetBundle_t910_m36657_MethodInfo,
	&Array_InternalArray__set_Item_TisAssetBundle_t910_m36665_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisAssetBundle_t910_m36666_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t117_m31492_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t117_m31493_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t117_m31494_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t117_m31495_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t117_m31496_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t117_m31497_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t117_m31499_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t117_m31500_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8155_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8156_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8157_il2cpp_TypeInfo;
static TypeInfo* AssetBundleU5BU5D_t5413_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8155_il2cpp_TypeInfo,
	&IList_1_t8156_il2cpp_TypeInfo,
	&IEnumerable_1_t8157_il2cpp_TypeInfo,
	&ICollection_1_t7225_il2cpp_TypeInfo,
	&IList_1_t7226_il2cpp_TypeInfo,
	&IEnumerable_1_t7227_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair AssetBundleU5BU5D_t5413_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8155_il2cpp_TypeInfo, 21},
	{ &IList_1_t8156_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8157_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7225_il2cpp_TypeInfo, 34},
	{ &IList_1_t7226_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7227_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType AssetBundleU5BU5D_t5413_0_0_0;
extern Il2CppType AssetBundleU5BU5D_t5413_1_0_0;
struct AssetBundle_t910;
extern TypeInfo AssetBundle_t910_il2cpp_TypeInfo;
extern CustomAttributesCache AssetBundle_t910__CustomAttributeCache_AssetBundle_LoadAsset_m5427;
extern CustomAttributesCache AssetBundle_t910__CustomAttributeCache_AssetBundle_LoadAsset_Internal_m5428;
extern CustomAttributesCache AssetBundle_t910__CustomAttributeCache_AssetBundle_LoadAssetWithSubAssets_Internal_m5429;
TypeInfo AssetBundleU5BU5D_t5413_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AssetBundle[]"/* name */
	, "UnityEngine"/* namespaze */
	, AssetBundleU5BU5D_t5413_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &AssetBundle_t910_il2cpp_TypeInfo/* element_class */
	, AssetBundleU5BU5D_t5413_InterfacesTypeInfos/* implemented_interfaces */
	, AssetBundleU5BU5D_t5413_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &AssetBundleU5BU5D_t5413_il2cpp_TypeInfo/* cast_class */
	, &AssetBundleU5BU5D_t5413_0_0_0/* byval_arg */
	, &AssetBundleU5BU5D_t5413_1_0_0/* this_arg */
	, AssetBundleU5BU5D_t5413_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (AssetBundle_t910 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SendMessageOptionsU5BU5D_t5414_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SendMessageOptions[]
static MethodInfo* SendMessageOptionsU5BU5D_t5414_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisSendMessageOptions_t912_m36670_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisSendMessageOptions_t912_m36671_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisSendMessageOptions_t912_m36672_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisSendMessageOptions_t912_m36673_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisSendMessageOptions_t912_m36674_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisSendMessageOptions_t912_m36675_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSendMessageOptions_t912_m36669_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisSendMessageOptions_t912_m36677_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisSendMessageOptions_t912_m36678_MethodInfo;
static MethodInfo* SendMessageOptionsU5BU5D_t5414_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisSendMessageOptions_t912_m36670_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisSendMessageOptions_t912_m36671_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisSendMessageOptions_t912_m36672_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisSendMessageOptions_t912_m36673_MethodInfo,
	&Array_InternalArray__IndexOf_TisSendMessageOptions_t912_m36674_MethodInfo,
	&Array_InternalArray__Insert_TisSendMessageOptions_t912_m36675_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisSendMessageOptions_t912_m36669_MethodInfo,
	&Array_InternalArray__set_Item_TisSendMessageOptions_t912_m36677_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisSendMessageOptions_t912_m36678_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8158_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8159_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8160_il2cpp_TypeInfo;
static TypeInfo* SendMessageOptionsU5BU5D_t5414_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8158_il2cpp_TypeInfo,
	&IList_1_t8159_il2cpp_TypeInfo,
	&IEnumerable_1_t8160_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair SendMessageOptionsU5BU5D_t5414_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8158_il2cpp_TypeInfo, 21},
	{ &IList_1_t8159_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8160_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SendMessageOptionsU5BU5D_t5414_0_0_0;
extern Il2CppType SendMessageOptionsU5BU5D_t5414_1_0_0;
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
extern TypeInfo SendMessageOptions_t912_il2cpp_TypeInfo;
TypeInfo SendMessageOptionsU5BU5D_t5414_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SendMessageOptions[]"/* name */
	, "UnityEngine"/* namespaze */
	, SendMessageOptionsU5BU5D_t5414_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SendMessageOptions_t912_il2cpp_TypeInfo/* element_class */
	, SendMessageOptionsU5BU5D_t5414_InterfacesTypeInfos/* implemented_interfaces */
	, SendMessageOptionsU5BU5D_t5414_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &SendMessageOptionsU5BU5D_t5414_0_0_0/* byval_arg */
	, &SendMessageOptionsU5BU5D_t5414_1_0_0/* this_arg */
	, SendMessageOptionsU5BU5D_t5414_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo PrimitiveTypeU5BU5D_t5415_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.PrimitiveType[]
static MethodInfo* PrimitiveTypeU5BU5D_t5415_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisPrimitiveType_t913_m36681_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisPrimitiveType_t913_m36682_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisPrimitiveType_t913_m36683_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisPrimitiveType_t913_m36684_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisPrimitiveType_t913_m36685_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisPrimitiveType_t913_m36686_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisPrimitiveType_t913_m36680_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisPrimitiveType_t913_m36688_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisPrimitiveType_t913_m36689_MethodInfo;
static MethodInfo* PrimitiveTypeU5BU5D_t5415_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisPrimitiveType_t913_m36681_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisPrimitiveType_t913_m36682_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisPrimitiveType_t913_m36683_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisPrimitiveType_t913_m36684_MethodInfo,
	&Array_InternalArray__IndexOf_TisPrimitiveType_t913_m36685_MethodInfo,
	&Array_InternalArray__Insert_TisPrimitiveType_t913_m36686_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisPrimitiveType_t913_m36680_MethodInfo,
	&Array_InternalArray__set_Item_TisPrimitiveType_t913_m36688_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisPrimitiveType_t913_m36689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8161_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8162_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8163_il2cpp_TypeInfo;
static TypeInfo* PrimitiveTypeU5BU5D_t5415_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8161_il2cpp_TypeInfo,
	&IList_1_t8162_il2cpp_TypeInfo,
	&IEnumerable_1_t8163_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair PrimitiveTypeU5BU5D_t5415_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8161_il2cpp_TypeInfo, 21},
	{ &IList_1_t8162_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8163_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType PrimitiveTypeU5BU5D_t5415_0_0_0;
extern Il2CppType PrimitiveTypeU5BU5D_t5415_1_0_0;
// UnityEngine.PrimitiveType
#include "UnityEngine_UnityEngine_PrimitiveType.h"
extern TypeInfo PrimitiveType_t913_il2cpp_TypeInfo;
TypeInfo PrimitiveTypeU5BU5D_t5415_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PrimitiveType[]"/* name */
	, "UnityEngine"/* namespaze */
	, PrimitiveTypeU5BU5D_t5415_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &PrimitiveType_t913_il2cpp_TypeInfo/* element_class */
	, PrimitiveTypeU5BU5D_t5415_InterfacesTypeInfos/* implemented_interfaces */
	, PrimitiveTypeU5BU5D_t5415_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &PrimitiveTypeU5BU5D_t5415_0_0_0/* byval_arg */
	, &PrimitiveTypeU5BU5D_t5415_1_0_0/* this_arg */
	, PrimitiveTypeU5BU5D_t5415_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SpaceU5BU5D_t5416_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.Space[]
static MethodInfo* SpaceU5BU5D_t5416_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisSpace_t914_m36692_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisSpace_t914_m36693_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisSpace_t914_m36694_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisSpace_t914_m36695_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisSpace_t914_m36696_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisSpace_t914_m36697_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSpace_t914_m36691_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisSpace_t914_m36699_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisSpace_t914_m36700_MethodInfo;
static MethodInfo* SpaceU5BU5D_t5416_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisSpace_t914_m36692_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisSpace_t914_m36693_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisSpace_t914_m36694_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisSpace_t914_m36695_MethodInfo,
	&Array_InternalArray__IndexOf_TisSpace_t914_m36696_MethodInfo,
	&Array_InternalArray__Insert_TisSpace_t914_m36697_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisSpace_t914_m36691_MethodInfo,
	&Array_InternalArray__set_Item_TisSpace_t914_m36699_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisSpace_t914_m36700_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8164_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8165_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8166_il2cpp_TypeInfo;
static TypeInfo* SpaceU5BU5D_t5416_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8164_il2cpp_TypeInfo,
	&IList_1_t8165_il2cpp_TypeInfo,
	&IEnumerable_1_t8166_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair SpaceU5BU5D_t5416_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8164_il2cpp_TypeInfo, 21},
	{ &IList_1_t8165_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8166_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SpaceU5BU5D_t5416_0_0_0;
extern Il2CppType SpaceU5BU5D_t5416_1_0_0;
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"
extern TypeInfo Space_t914_il2cpp_TypeInfo;
TypeInfo SpaceU5BU5D_t5416_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Space[]"/* name */
	, "UnityEngine"/* namespaze */
	, SpaceU5BU5D_t5416_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Space_t914_il2cpp_TypeInfo/* element_class */
	, SpaceU5BU5D_t5416_InterfacesTypeInfos/* implemented_interfaces */
	, SpaceU5BU5D_t5416_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &SpaceU5BU5D_t5416_0_0_0/* byval_arg */
	, &SpaceU5BU5D_t5416_1_0_0/* this_arg */
	, SpaceU5BU5D_t5416_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo RuntimePlatformU5BU5D_t5417_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.RuntimePlatform[]
static MethodInfo* RuntimePlatformU5BU5D_t5417_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisRuntimePlatform_t915_m36703_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisRuntimePlatform_t915_m36704_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisRuntimePlatform_t915_m36705_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisRuntimePlatform_t915_m36706_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisRuntimePlatform_t915_m36707_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisRuntimePlatform_t915_m36708_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRuntimePlatform_t915_m36702_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisRuntimePlatform_t915_m36710_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisRuntimePlatform_t915_m36711_MethodInfo;
static MethodInfo* RuntimePlatformU5BU5D_t5417_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisRuntimePlatform_t915_m36703_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisRuntimePlatform_t915_m36704_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisRuntimePlatform_t915_m36705_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisRuntimePlatform_t915_m36706_MethodInfo,
	&Array_InternalArray__IndexOf_TisRuntimePlatform_t915_m36707_MethodInfo,
	&Array_InternalArray__Insert_TisRuntimePlatform_t915_m36708_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisRuntimePlatform_t915_m36702_MethodInfo,
	&Array_InternalArray__set_Item_TisRuntimePlatform_t915_m36710_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisRuntimePlatform_t915_m36711_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8167_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8168_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8169_il2cpp_TypeInfo;
static TypeInfo* RuntimePlatformU5BU5D_t5417_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8167_il2cpp_TypeInfo,
	&IList_1_t8168_il2cpp_TypeInfo,
	&IEnumerable_1_t8169_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair RuntimePlatformU5BU5D_t5417_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8167_il2cpp_TypeInfo, 21},
	{ &IList_1_t8168_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8169_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType RuntimePlatformU5BU5D_t5417_0_0_0;
extern Il2CppType RuntimePlatformU5BU5D_t5417_1_0_0;
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
extern TypeInfo RuntimePlatform_t915_il2cpp_TypeInfo;
extern CustomAttributesCache RuntimePlatform_t915__CustomAttributeCache_NaCl;
extern CustomAttributesCache RuntimePlatform_t915__CustomAttributeCache_FlashPlayer;
extern CustomAttributesCache RuntimePlatform_t915__CustomAttributeCache_MetroPlayerX86;
extern CustomAttributesCache RuntimePlatform_t915__CustomAttributeCache_MetroPlayerX64;
extern CustomAttributesCache RuntimePlatform_t915__CustomAttributeCache_MetroPlayerARM;
TypeInfo RuntimePlatformU5BU5D_t5417_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RuntimePlatform[]"/* name */
	, "UnityEngine"/* namespaze */
	, RuntimePlatformU5BU5D_t5417_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &RuntimePlatform_t915_il2cpp_TypeInfo/* element_class */
	, RuntimePlatformU5BU5D_t5417_InterfacesTypeInfos/* implemented_interfaces */
	, RuntimePlatformU5BU5D_t5417_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &RuntimePlatformU5BU5D_t5417_0_0_0/* byval_arg */
	, &RuntimePlatformU5BU5D_t5417_1_0_0/* this_arg */
	, RuntimePlatformU5BU5D_t5417_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo LogTypeU5BU5D_t5418_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.LogType[]
static MethodInfo* LogTypeU5BU5D_t5418_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisLogType_t916_m36714_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisLogType_t916_m36715_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisLogType_t916_m36716_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisLogType_t916_m36717_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisLogType_t916_m36718_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisLogType_t916_m36719_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisLogType_t916_m36713_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisLogType_t916_m36721_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisLogType_t916_m36722_MethodInfo;
static MethodInfo* LogTypeU5BU5D_t5418_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisLogType_t916_m36714_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisLogType_t916_m36715_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisLogType_t916_m36716_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisLogType_t916_m36717_MethodInfo,
	&Array_InternalArray__IndexOf_TisLogType_t916_m36718_MethodInfo,
	&Array_InternalArray__Insert_TisLogType_t916_m36719_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisLogType_t916_m36713_MethodInfo,
	&Array_InternalArray__set_Item_TisLogType_t916_m36721_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisLogType_t916_m36722_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8170_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8171_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8172_il2cpp_TypeInfo;
static TypeInfo* LogTypeU5BU5D_t5418_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8170_il2cpp_TypeInfo,
	&IList_1_t8171_il2cpp_TypeInfo,
	&IEnumerable_1_t8172_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair LogTypeU5BU5D_t5418_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8170_il2cpp_TypeInfo, 21},
	{ &IList_1_t8171_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8172_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType LogTypeU5BU5D_t5418_0_0_0;
extern Il2CppType LogTypeU5BU5D_t5418_1_0_0;
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogType.h"
extern TypeInfo LogType_t916_il2cpp_TypeInfo;
TypeInfo LogTypeU5BU5D_t5418_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LogType[]"/* name */
	, "UnityEngine"/* namespaze */
	, LogTypeU5BU5D_t5418_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &LogType_t916_il2cpp_TypeInfo/* element_class */
	, LogTypeU5BU5D_t5418_InterfacesTypeInfos/* implemented_interfaces */
	, LogTypeU5BU5D_t5418_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &LogTypeU5BU5D_t5418_0_0_0/* byval_arg */
	, &LogTypeU5BU5D_t5418_1_0_0/* this_arg */
	, LogTypeU5BU5D_t5418_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ScriptableObjectU5BU5D_t5419_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.ScriptableObject[]
static MethodInfo* ScriptableObjectU5BU5D_t5419_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisScriptableObject_t919_m36725_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisScriptableObject_t919_m36726_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisScriptableObject_t919_m36727_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisScriptableObject_t919_m36728_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisScriptableObject_t919_m36729_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisScriptableObject_t919_m36730_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisScriptableObject_t919_m36724_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisScriptableObject_t919_m36732_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisScriptableObject_t919_m36733_MethodInfo;
static MethodInfo* ScriptableObjectU5BU5D_t5419_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisScriptableObject_t919_m36725_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisScriptableObject_t919_m36726_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisScriptableObject_t919_m36727_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisScriptableObject_t919_m36728_MethodInfo,
	&Array_InternalArray__IndexOf_TisScriptableObject_t919_m36729_MethodInfo,
	&Array_InternalArray__Insert_TisScriptableObject_t919_m36730_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisScriptableObject_t919_m36724_MethodInfo,
	&Array_InternalArray__set_Item_TisScriptableObject_t919_m36732_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisScriptableObject_t919_m36733_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t117_m31492_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t117_m31493_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t117_m31494_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t117_m31495_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t117_m31496_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t117_m31497_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t117_m31499_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t117_m31500_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8173_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8174_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8175_il2cpp_TypeInfo;
static TypeInfo* ScriptableObjectU5BU5D_t5419_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8173_il2cpp_TypeInfo,
	&IList_1_t8174_il2cpp_TypeInfo,
	&IEnumerable_1_t8175_il2cpp_TypeInfo,
	&ICollection_1_t7225_il2cpp_TypeInfo,
	&IList_1_t7226_il2cpp_TypeInfo,
	&IEnumerable_1_t7227_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair ScriptableObjectU5BU5D_t5419_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8173_il2cpp_TypeInfo, 21},
	{ &IList_1_t8174_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8175_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7225_il2cpp_TypeInfo, 34},
	{ &IList_1_t7226_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7227_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ScriptableObjectU5BU5D_t5419_0_0_0;
extern Il2CppType ScriptableObjectU5BU5D_t5419_1_0_0;
struct ScriptableObject_t919;
extern TypeInfo ScriptableObject_t919_il2cpp_TypeInfo;
extern CustomAttributesCache ScriptableObject_t919__CustomAttributeCache_ScriptableObject_Internal_CreateScriptableObject_m5440;
extern CustomAttributesCache ScriptableObject_t919__CustomAttributeCache_ScriptableObject_CreateInstance_m5441;
extern CustomAttributesCache ScriptableObject_t919__CustomAttributeCache_ScriptableObject_CreateInstanceFromType_m5443;
TypeInfo ScriptableObjectU5BU5D_t5419_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ScriptableObject[]"/* name */
	, "UnityEngine"/* namespaze */
	, ScriptableObjectU5BU5D_t5419_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ScriptableObject_t919_il2cpp_TypeInfo/* element_class */
	, ScriptableObjectU5BU5D_t5419_InterfacesTypeInfos/* implemented_interfaces */
	, ScriptableObjectU5BU5D_t5419_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ScriptableObjectU5BU5D_t5419_il2cpp_TypeInfo/* cast_class */
	, &ScriptableObjectU5BU5D_t5419_0_0_0/* byval_arg */
	, &ScriptableObjectU5BU5D_t5419_1_0_0/* this_arg */
	, ScriptableObjectU5BU5D_t5419_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (ScriptableObject_t919 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IAchievementDescriptionU5BU5D_t1104_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.IAchievementDescription[]
static MethodInfo* IAchievementDescriptionU5BU5D_t1104_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisIAchievementDescription_t1105_m36737_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisIAchievementDescription_t1105_m36738_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisIAchievementDescription_t1105_m36739_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisIAchievementDescription_t1105_m36740_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisIAchievementDescription_t1105_m36741_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisIAchievementDescription_t1105_m36742_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIAchievementDescription_t1105_m36736_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisIAchievementDescription_t1105_m36744_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisIAchievementDescription_t1105_m36745_MethodInfo;
static MethodInfo* IAchievementDescriptionU5BU5D_t1104_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIAchievementDescription_t1105_m36737_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIAchievementDescription_t1105_m36738_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIAchievementDescription_t1105_m36739_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIAchievementDescription_t1105_m36740_MethodInfo,
	&Array_InternalArray__IndexOf_TisIAchievementDescription_t1105_m36741_MethodInfo,
	&Array_InternalArray__Insert_TisIAchievementDescription_t1105_m36742_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIAchievementDescription_t1105_m36736_MethodInfo,
	&Array_InternalArray__set_Item_TisIAchievementDescription_t1105_m36744_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIAchievementDescription_t1105_m36745_MethodInfo,
};
extern TypeInfo ICollection_1_t8176_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8177_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8178_il2cpp_TypeInfo;
static TypeInfo* IAchievementDescriptionU5BU5D_t1104_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8176_il2cpp_TypeInfo,
	&IList_1_t8177_il2cpp_TypeInfo,
	&IEnumerable_1_t8178_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair IAchievementDescriptionU5BU5D_t1104_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8176_il2cpp_TypeInfo, 21},
	{ &IList_1_t8177_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8178_il2cpp_TypeInfo, 33},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IAchievementDescriptionU5BU5D_t1104_0_0_0;
extern Il2CppType IAchievementDescriptionU5BU5D_t1104_1_0_0;
struct IAchievementDescription_t1105;
extern TypeInfo IAchievementDescription_t1105_il2cpp_TypeInfo;
TypeInfo IAchievementDescriptionU5BU5D_t1104_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievementDescription[]"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievementDescriptionU5BU5D_t1104_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IAchievementDescription_t1105_il2cpp_TypeInfo/* element_class */
	, IAchievementDescriptionU5BU5D_t1104_InterfacesTypeInfos/* implemented_interfaces */
	, IAchievementDescriptionU5BU5D_t1104_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IAchievementDescriptionU5BU5D_t1104_il2cpp_TypeInfo/* cast_class */
	, &IAchievementDescriptionU5BU5D_t1104_0_0_0/* byval_arg */
	, &IAchievementDescriptionU5BU5D_t1104_1_0_0/* this_arg */
	, IAchievementDescriptionU5BU5D_t1104_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 3/* interfaces_count */
	, 7/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IAchievementU5BU5D_t1107_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.IAchievement[]
static MethodInfo* IAchievementU5BU5D_t1107_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisIAchievement_t935_m36748_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisIAchievement_t935_m36749_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisIAchievement_t935_m36750_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisIAchievement_t935_m36751_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisIAchievement_t935_m36752_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisIAchievement_t935_m36753_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIAchievement_t935_m36747_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisIAchievement_t935_m36755_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisIAchievement_t935_m36756_MethodInfo;
static MethodInfo* IAchievementU5BU5D_t1107_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIAchievement_t935_m36748_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIAchievement_t935_m36749_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIAchievement_t935_m36750_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIAchievement_t935_m36751_MethodInfo,
	&Array_InternalArray__IndexOf_TisIAchievement_t935_m36752_MethodInfo,
	&Array_InternalArray__Insert_TisIAchievement_t935_m36753_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIAchievement_t935_m36747_MethodInfo,
	&Array_InternalArray__set_Item_TisIAchievement_t935_m36755_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIAchievement_t935_m36756_MethodInfo,
};
extern TypeInfo ICollection_1_t8179_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8180_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8181_il2cpp_TypeInfo;
static TypeInfo* IAchievementU5BU5D_t1107_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8179_il2cpp_TypeInfo,
	&IList_1_t8180_il2cpp_TypeInfo,
	&IEnumerable_1_t8181_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair IAchievementU5BU5D_t1107_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8179_il2cpp_TypeInfo, 21},
	{ &IList_1_t8180_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8181_il2cpp_TypeInfo, 33},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IAchievementU5BU5D_t1107_0_0_0;
extern Il2CppType IAchievementU5BU5D_t1107_1_0_0;
struct IAchievement_t935;
extern TypeInfo IAchievement_t935_il2cpp_TypeInfo;
TypeInfo IAchievementU5BU5D_t1107_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievement[]"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievementU5BU5D_t1107_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IAchievement_t935_il2cpp_TypeInfo/* element_class */
	, IAchievementU5BU5D_t1107_InterfacesTypeInfos/* implemented_interfaces */
	, IAchievementU5BU5D_t1107_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IAchievementU5BU5D_t1107_il2cpp_TypeInfo/* cast_class */
	, &IAchievementU5BU5D_t1107_0_0_0/* byval_arg */
	, &IAchievementU5BU5D_t1107_1_0_0/* this_arg */
	, IAchievementU5BU5D_t1107_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 3/* interfaces_count */
	, 7/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IScoreU5BU5D_t1054_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.IScore[]
static MethodInfo* IScoreU5BU5D_t1054_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisIScore_t1053_m36759_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisIScore_t1053_m36760_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisIScore_t1053_m36761_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisIScore_t1053_m36762_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisIScore_t1053_m36763_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisIScore_t1053_m36764_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIScore_t1053_m36758_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisIScore_t1053_m36766_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisIScore_t1053_m36767_MethodInfo;
static MethodInfo* IScoreU5BU5D_t1054_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIScore_t1053_m36759_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIScore_t1053_m36760_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIScore_t1053_m36761_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIScore_t1053_m36762_MethodInfo,
	&Array_InternalArray__IndexOf_TisIScore_t1053_m36763_MethodInfo,
	&Array_InternalArray__Insert_TisIScore_t1053_m36764_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIScore_t1053_m36758_MethodInfo,
	&Array_InternalArray__set_Item_TisIScore_t1053_m36766_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIScore_t1053_m36767_MethodInfo,
};
extern TypeInfo ICollection_1_t8182_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8183_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8184_il2cpp_TypeInfo;
static TypeInfo* IScoreU5BU5D_t1054_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8182_il2cpp_TypeInfo,
	&IList_1_t8183_il2cpp_TypeInfo,
	&IEnumerable_1_t8184_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair IScoreU5BU5D_t1054_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8182_il2cpp_TypeInfo, 21},
	{ &IList_1_t8183_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8184_il2cpp_TypeInfo, 33},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IScoreU5BU5D_t1054_0_0_0;
extern Il2CppType IScoreU5BU5D_t1054_1_0_0;
struct IScore_t1053;
extern TypeInfo IScore_t1053_il2cpp_TypeInfo;
TypeInfo IScoreU5BU5D_t1054_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IScore[]"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IScoreU5BU5D_t1054_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IScore_t1053_il2cpp_TypeInfo/* element_class */
	, IScoreU5BU5D_t1054_InterfacesTypeInfos/* implemented_interfaces */
	, IScoreU5BU5D_t1054_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IScoreU5BU5D_t1054_il2cpp_TypeInfo/* cast_class */
	, &IScoreU5BU5D_t1054_0_0_0/* byval_arg */
	, &IScoreU5BU5D_t1054_1_0_0/* this_arg */
	, IScoreU5BU5D_t1054_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 3/* interfaces_count */
	, 7/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IUserProfileU5BU5D_t1052_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.IUserProfile[]
static MethodInfo* IUserProfileU5BU5D_t1052_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisIUserProfile_t1110_m36770_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisIUserProfile_t1110_m36771_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisIUserProfile_t1110_m36772_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisIUserProfile_t1110_m36773_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisIUserProfile_t1110_m36774_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisIUserProfile_t1110_m36775_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIUserProfile_t1110_m36769_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisIUserProfile_t1110_m36777_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisIUserProfile_t1110_m36778_MethodInfo;
static MethodInfo* IUserProfileU5BU5D_t1052_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIUserProfile_t1110_m36770_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIUserProfile_t1110_m36771_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIUserProfile_t1110_m36772_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIUserProfile_t1110_m36773_MethodInfo,
	&Array_InternalArray__IndexOf_TisIUserProfile_t1110_m36774_MethodInfo,
	&Array_InternalArray__Insert_TisIUserProfile_t1110_m36775_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIUserProfile_t1110_m36769_MethodInfo,
	&Array_InternalArray__set_Item_TisIUserProfile_t1110_m36777_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIUserProfile_t1110_m36778_MethodInfo,
};
extern TypeInfo ICollection_1_t8185_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8186_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8187_il2cpp_TypeInfo;
static TypeInfo* IUserProfileU5BU5D_t1052_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8185_il2cpp_TypeInfo,
	&IList_1_t8186_il2cpp_TypeInfo,
	&IEnumerable_1_t8187_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair IUserProfileU5BU5D_t1052_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8185_il2cpp_TypeInfo, 21},
	{ &IList_1_t8186_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8187_il2cpp_TypeInfo, 33},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IUserProfileU5BU5D_t1052_0_0_0;
extern Il2CppType IUserProfileU5BU5D_t1052_1_0_0;
struct IUserProfile_t1110;
extern TypeInfo IUserProfile_t1110_il2cpp_TypeInfo;
TypeInfo IUserProfileU5BU5D_t1052_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IUserProfile[]"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IUserProfileU5BU5D_t1052_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IUserProfile_t1110_il2cpp_TypeInfo/* element_class */
	, IUserProfileU5BU5D_t1052_InterfacesTypeInfos/* implemented_interfaces */
	, IUserProfileU5BU5D_t1052_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IUserProfileU5BU5D_t1052_il2cpp_TypeInfo/* cast_class */
	, &IUserProfileU5BU5D_t1052_0_0_0/* byval_arg */
	, &IUserProfileU5BU5D_t1052_1_0_0/* this_arg */
	, IUserProfileU5BU5D_t1052_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Object_t *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 3/* interfaces_count */
	, 7/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo AchievementDescriptionU5BU5D_t924_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
static MethodInfo* AchievementDescriptionU5BU5D_t924_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisAchievementDescription_t1043_m36781_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisAchievementDescription_t1043_m36782_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisAchievementDescription_t1043_m36783_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisAchievementDescription_t1043_m36784_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisAchievementDescription_t1043_m36785_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisAchievementDescription_t1043_m36786_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAchievementDescription_t1043_m36780_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisAchievementDescription_t1043_m36788_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisAchievementDescription_t1043_m36789_MethodInfo;
static MethodInfo* AchievementDescriptionU5BU5D_t924_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisAchievementDescription_t1043_m36781_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisAchievementDescription_t1043_m36782_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisAchievementDescription_t1043_m36783_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisAchievementDescription_t1043_m36784_MethodInfo,
	&Array_InternalArray__IndexOf_TisAchievementDescription_t1043_m36785_MethodInfo,
	&Array_InternalArray__Insert_TisAchievementDescription_t1043_m36786_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisAchievementDescription_t1043_m36780_MethodInfo,
	&Array_InternalArray__set_Item_TisAchievementDescription_t1043_m36788_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisAchievementDescription_t1043_m36789_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIAchievementDescription_t1105_m36737_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIAchievementDescription_t1105_m36738_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIAchievementDescription_t1105_m36739_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIAchievementDescription_t1105_m36740_MethodInfo,
	&Array_InternalArray__IndexOf_TisIAchievementDescription_t1105_m36741_MethodInfo,
	&Array_InternalArray__Insert_TisIAchievementDescription_t1105_m36742_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIAchievementDescription_t1105_m36736_MethodInfo,
	&Array_InternalArray__set_Item_TisIAchievementDescription_t1105_m36744_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIAchievementDescription_t1105_m36745_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8188_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8189_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8190_il2cpp_TypeInfo;
static TypeInfo* AchievementDescriptionU5BU5D_t924_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8188_il2cpp_TypeInfo,
	&IList_1_t8189_il2cpp_TypeInfo,
	&IEnumerable_1_t8190_il2cpp_TypeInfo,
	&ICollection_1_t8176_il2cpp_TypeInfo,
	&IList_1_t8177_il2cpp_TypeInfo,
	&IEnumerable_1_t8178_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair AchievementDescriptionU5BU5D_t924_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8188_il2cpp_TypeInfo, 21},
	{ &IList_1_t8189_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8190_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t8176_il2cpp_TypeInfo, 34},
	{ &IList_1_t8177_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t8178_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType AchievementDescriptionU5BU5D_t924_0_0_0;
extern Il2CppType AchievementDescriptionU5BU5D_t924_1_0_0;
struct AchievementDescription_t1043;
extern TypeInfo AchievementDescription_t1043_il2cpp_TypeInfo;
extern CustomAttributesCache AchievementDescription_t1043__CustomAttributeCache_U3CidU3Ek__BackingField;
extern CustomAttributesCache AchievementDescription_t1043__CustomAttributeCache_AchievementDescription_get_id_m6278;
extern CustomAttributesCache AchievementDescription_t1043__CustomAttributeCache_AchievementDescription_set_id_m6279;
TypeInfo AchievementDescriptionU5BU5D_t924_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "AchievementDescription[]"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, AchievementDescriptionU5BU5D_t924_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &AchievementDescription_t1043_il2cpp_TypeInfo/* element_class */
	, AchievementDescriptionU5BU5D_t924_InterfacesTypeInfos/* implemented_interfaces */
	, AchievementDescriptionU5BU5D_t924_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &AchievementDescriptionU5BU5D_t924_il2cpp_TypeInfo/* cast_class */
	, &AchievementDescriptionU5BU5D_t924_0_0_0/* byval_arg */
	, &AchievementDescriptionU5BU5D_t924_1_0_0/* this_arg */
	, AchievementDescriptionU5BU5D_t924_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (AchievementDescription_t1043 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UserProfileU5BU5D_t925_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.Impl.UserProfile[]
static MethodInfo* UserProfileU5BU5D_t925_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisUserProfile_t1042_m36792_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisUserProfile_t1042_m36793_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisUserProfile_t1042_m36794_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisUserProfile_t1042_m36795_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisUserProfile_t1042_m36796_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisUserProfile_t1042_m36797_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUserProfile_t1042_m36791_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisUserProfile_t1042_m36799_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisUserProfile_t1042_m36800_MethodInfo;
static MethodInfo* UserProfileU5BU5D_t925_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisUserProfile_t1042_m36792_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisUserProfile_t1042_m36793_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisUserProfile_t1042_m36794_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisUserProfile_t1042_m36795_MethodInfo,
	&Array_InternalArray__IndexOf_TisUserProfile_t1042_m36796_MethodInfo,
	&Array_InternalArray__Insert_TisUserProfile_t1042_m36797_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisUserProfile_t1042_m36791_MethodInfo,
	&Array_InternalArray__set_Item_TisUserProfile_t1042_m36799_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisUserProfile_t1042_m36800_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIUserProfile_t1110_m36770_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIUserProfile_t1110_m36771_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIUserProfile_t1110_m36772_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIUserProfile_t1110_m36773_MethodInfo,
	&Array_InternalArray__IndexOf_TisIUserProfile_t1110_m36774_MethodInfo,
	&Array_InternalArray__Insert_TisIUserProfile_t1110_m36775_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIUserProfile_t1110_m36769_MethodInfo,
	&Array_InternalArray__set_Item_TisIUserProfile_t1110_m36777_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIUserProfile_t1110_m36778_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8191_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8192_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8193_il2cpp_TypeInfo;
static TypeInfo* UserProfileU5BU5D_t925_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8191_il2cpp_TypeInfo,
	&IList_1_t8192_il2cpp_TypeInfo,
	&IEnumerable_1_t8193_il2cpp_TypeInfo,
	&ICollection_1_t8185_il2cpp_TypeInfo,
	&IList_1_t8186_il2cpp_TypeInfo,
	&IEnumerable_1_t8187_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair UserProfileU5BU5D_t925_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8191_il2cpp_TypeInfo, 21},
	{ &IList_1_t8192_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8193_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t8185_il2cpp_TypeInfo, 34},
	{ &IList_1_t8186_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t8187_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UserProfileU5BU5D_t925_0_0_0;
extern Il2CppType UserProfileU5BU5D_t925_1_0_0;
struct UserProfile_t1042;
extern TypeInfo UserProfile_t1042_il2cpp_TypeInfo;
TypeInfo UserProfileU5BU5D_t925_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserProfile[]"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, UserProfileU5BU5D_t925_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UserProfile_t1042_il2cpp_TypeInfo/* element_class */
	, UserProfileU5BU5D_t925_InterfacesTypeInfos/* implemented_interfaces */
	, UserProfileU5BU5D_t925_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UserProfileU5BU5D_t925_il2cpp_TypeInfo/* cast_class */
	, &UserProfileU5BU5D_t925_0_0_0/* byval_arg */
	, &UserProfileU5BU5D_t925_1_0_0/* this_arg */
	, UserProfileU5BU5D_t925_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (UserProfile_t1042 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GcLeaderboardU5BU5D_t4447_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
static MethodInfo* GcLeaderboardU5BU5D_t4447_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisGcLeaderboard_t939_m36803_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisGcLeaderboard_t939_m36804_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisGcLeaderboard_t939_m36805_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisGcLeaderboard_t939_m36806_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisGcLeaderboard_t939_m36807_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisGcLeaderboard_t939_m36808_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGcLeaderboard_t939_m36802_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisGcLeaderboard_t939_m36810_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisGcLeaderboard_t939_m36811_MethodInfo;
static MethodInfo* GcLeaderboardU5BU5D_t4447_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisGcLeaderboard_t939_m36803_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisGcLeaderboard_t939_m36804_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisGcLeaderboard_t939_m36805_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisGcLeaderboard_t939_m36806_MethodInfo,
	&Array_InternalArray__IndexOf_TisGcLeaderboard_t939_m36807_MethodInfo,
	&Array_InternalArray__Insert_TisGcLeaderboard_t939_m36808_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisGcLeaderboard_t939_m36802_MethodInfo,
	&Array_InternalArray__set_Item_TisGcLeaderboard_t939_m36810_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisGcLeaderboard_t939_m36811_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t4450_il2cpp_TypeInfo;
extern TypeInfo IList_1_t4455_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t4448_il2cpp_TypeInfo;
static TypeInfo* GcLeaderboardU5BU5D_t4447_InterfacesTypeInfos[] = 
{
	&ICollection_1_t4450_il2cpp_TypeInfo,
	&IList_1_t4455_il2cpp_TypeInfo,
	&IEnumerable_1_t4448_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair GcLeaderboardU5BU5D_t4447_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t4450_il2cpp_TypeInfo, 21},
	{ &IList_1_t4455_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t4448_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 34},
	{ &IList_1_t2851_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 46},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GcLeaderboardU5BU5D_t4447_0_0_0;
extern Il2CppType GcLeaderboardU5BU5D_t4447_1_0_0;
struct GcLeaderboard_t939;
extern TypeInfo GcLeaderboard_t939_il2cpp_TypeInfo;
extern CustomAttributesCache GcLeaderboard_t939__CustomAttributeCache_GcLeaderboard_Internal_LoadScores_m5512;
extern CustomAttributesCache GcLeaderboard_t939__CustomAttributeCache_GcLeaderboard_Internal_LoadScoresWithUsers_m5513;
extern CustomAttributesCache GcLeaderboard_t939__CustomAttributeCache_GcLeaderboard_Loading_m5514;
extern CustomAttributesCache GcLeaderboard_t939__CustomAttributeCache_GcLeaderboard_Dispose_m5515;
TypeInfo GcLeaderboardU5BU5D_t4447_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcLeaderboard[]"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcLeaderboardU5BU5D_t4447_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GcLeaderboard_t939_il2cpp_TypeInfo/* element_class */
	, GcLeaderboardU5BU5D_t4447_InterfacesTypeInfos/* implemented_interfaces */
	, GcLeaderboardU5BU5D_t4447_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GcLeaderboardU5BU5D_t4447_il2cpp_TypeInfo/* cast_class */
	, &GcLeaderboardU5BU5D_t4447_0_0_0/* byval_arg */
	, &GcLeaderboardU5BU5D_t4447_1_0_0/* this_arg */
	, GcLeaderboardU5BU5D_t4447_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (GcLeaderboard_t939 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 47/* vtable_count */
	, 6/* interfaces_count */
	, 10/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GcAchievementDataU5BU5D_t930_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
static MethodInfo* GcAchievementDataU5BU5D_t930_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisGcAchievementData_t931_m36829_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisGcAchievementData_t931_m36830_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisGcAchievementData_t931_m36831_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisGcAchievementData_t931_m36832_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisGcAchievementData_t931_m36833_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisGcAchievementData_t931_m36834_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGcAchievementData_t931_m36828_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisGcAchievementData_t931_m36836_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisGcAchievementData_t931_m36837_MethodInfo;
static MethodInfo* GcAchievementDataU5BU5D_t930_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisGcAchievementData_t931_m36829_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisGcAchievementData_t931_m36830_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisGcAchievementData_t931_m36831_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisGcAchievementData_t931_m36832_MethodInfo,
	&Array_InternalArray__IndexOf_TisGcAchievementData_t931_m36833_MethodInfo,
	&Array_InternalArray__Insert_TisGcAchievementData_t931_m36834_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisGcAchievementData_t931_m36828_MethodInfo,
	&Array_InternalArray__set_Item_TisGcAchievementData_t931_m36836_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisGcAchievementData_t931_m36837_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8194_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8195_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8196_il2cpp_TypeInfo;
static TypeInfo* GcAchievementDataU5BU5D_t930_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8194_il2cpp_TypeInfo,
	&IList_1_t8195_il2cpp_TypeInfo,
	&IEnumerable_1_t8196_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair GcAchievementDataU5BU5D_t930_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8194_il2cpp_TypeInfo, 21},
	{ &IList_1_t8195_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8196_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GcAchievementDataU5BU5D_t930_0_0_0;
extern Il2CppType GcAchievementDataU5BU5D_t930_1_0_0;
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
extern TypeInfo GcAchievementData_t931_il2cpp_TypeInfo;
TypeInfo GcAchievementDataU5BU5D_t930_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcAchievementData[]"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcAchievementDataU5BU5D_t930_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GcAchievementData_t931_il2cpp_TypeInfo/* element_class */
	, GcAchievementDataU5BU5D_t930_InterfacesTypeInfos/* implemented_interfaces */
	, GcAchievementDataU5BU5D_t930_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GcAchievementDataU5BU5D_t930_il2cpp_TypeInfo/* cast_class */
	, &GcAchievementDataU5BU5D_t930_0_0_0/* byval_arg */
	, &GcAchievementDataU5BU5D_t930_1_0_0/* this_arg */
	, GcAchievementDataU5BU5D_t930_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (GcAchievementData_t931 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo AchievementU5BU5D_t1106_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.Impl.Achievement[]
static MethodInfo* AchievementU5BU5D_t1106_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisAchievement_t1044_m36840_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisAchievement_t1044_m36841_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisAchievement_t1044_m36842_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisAchievement_t1044_m36843_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisAchievement_t1044_m36844_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisAchievement_t1044_m36845_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAchievement_t1044_m36839_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisAchievement_t1044_m36847_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisAchievement_t1044_m36848_MethodInfo;
static MethodInfo* AchievementU5BU5D_t1106_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisAchievement_t1044_m36840_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisAchievement_t1044_m36841_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisAchievement_t1044_m36842_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisAchievement_t1044_m36843_MethodInfo,
	&Array_InternalArray__IndexOf_TisAchievement_t1044_m36844_MethodInfo,
	&Array_InternalArray__Insert_TisAchievement_t1044_m36845_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisAchievement_t1044_m36839_MethodInfo,
	&Array_InternalArray__set_Item_TisAchievement_t1044_m36847_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisAchievement_t1044_m36848_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIAchievement_t935_m36748_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIAchievement_t935_m36749_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIAchievement_t935_m36750_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIAchievement_t935_m36751_MethodInfo,
	&Array_InternalArray__IndexOf_TisIAchievement_t935_m36752_MethodInfo,
	&Array_InternalArray__Insert_TisIAchievement_t935_m36753_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIAchievement_t935_m36747_MethodInfo,
	&Array_InternalArray__set_Item_TisIAchievement_t935_m36755_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIAchievement_t935_m36756_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8197_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8198_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8199_il2cpp_TypeInfo;
static TypeInfo* AchievementU5BU5D_t1106_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8197_il2cpp_TypeInfo,
	&IList_1_t8198_il2cpp_TypeInfo,
	&IEnumerable_1_t8199_il2cpp_TypeInfo,
	&ICollection_1_t8179_il2cpp_TypeInfo,
	&IList_1_t8180_il2cpp_TypeInfo,
	&IEnumerable_1_t8181_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair AchievementU5BU5D_t1106_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8197_il2cpp_TypeInfo, 21},
	{ &IList_1_t8198_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8199_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t8179_il2cpp_TypeInfo, 34},
	{ &IList_1_t8180_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t8181_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType AchievementU5BU5D_t1106_0_0_0;
extern Il2CppType AchievementU5BU5D_t1106_1_0_0;
struct Achievement_t1044;
extern TypeInfo Achievement_t1044_il2cpp_TypeInfo;
extern CustomAttributesCache Achievement_t1044__CustomAttributeCache_U3CidU3Ek__BackingField;
extern CustomAttributesCache Achievement_t1044__CustomAttributeCache_U3CpercentCompletedU3Ek__BackingField;
extern CustomAttributesCache Achievement_t1044__CustomAttributeCache_Achievement_get_id_m6268;
extern CustomAttributesCache Achievement_t1044__CustomAttributeCache_Achievement_set_id_m6269;
extern CustomAttributesCache Achievement_t1044__CustomAttributeCache_Achievement_get_percentCompleted_m6270;
extern CustomAttributesCache Achievement_t1044__CustomAttributeCache_Achievement_set_percentCompleted_m6271;
TypeInfo AchievementU5BU5D_t1106_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Achievement[]"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, AchievementU5BU5D_t1106_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Achievement_t1044_il2cpp_TypeInfo/* element_class */
	, AchievementU5BU5D_t1106_InterfacesTypeInfos/* implemented_interfaces */
	, AchievementU5BU5D_t1106_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &AchievementU5BU5D_t1106_il2cpp_TypeInfo/* cast_class */
	, &AchievementU5BU5D_t1106_0_0_0/* byval_arg */
	, &AchievementU5BU5D_t1106_1_0_0/* this_arg */
	, AchievementU5BU5D_t1106_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Achievement_t1044 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GcScoreDataU5BU5D_t932_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
static MethodInfo* GcScoreDataU5BU5D_t932_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisGcScoreData_t933_m36851_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisGcScoreData_t933_m36852_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisGcScoreData_t933_m36853_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisGcScoreData_t933_m36854_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisGcScoreData_t933_m36855_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisGcScoreData_t933_m36856_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGcScoreData_t933_m36850_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisGcScoreData_t933_m36858_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisGcScoreData_t933_m36859_MethodInfo;
static MethodInfo* GcScoreDataU5BU5D_t932_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisGcScoreData_t933_m36851_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisGcScoreData_t933_m36852_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisGcScoreData_t933_m36853_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisGcScoreData_t933_m36854_MethodInfo,
	&Array_InternalArray__IndexOf_TisGcScoreData_t933_m36855_MethodInfo,
	&Array_InternalArray__Insert_TisGcScoreData_t933_m36856_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisGcScoreData_t933_m36850_MethodInfo,
	&Array_InternalArray__set_Item_TisGcScoreData_t933_m36858_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisGcScoreData_t933_m36859_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8200_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8201_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8202_il2cpp_TypeInfo;
static TypeInfo* GcScoreDataU5BU5D_t932_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8200_il2cpp_TypeInfo,
	&IList_1_t8201_il2cpp_TypeInfo,
	&IEnumerable_1_t8202_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair GcScoreDataU5BU5D_t932_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8200_il2cpp_TypeInfo, 21},
	{ &IList_1_t8201_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8202_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GcScoreDataU5BU5D_t932_0_0_0;
extern Il2CppType GcScoreDataU5BU5D_t932_1_0_0;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
extern TypeInfo GcScoreData_t933_il2cpp_TypeInfo;
TypeInfo GcScoreDataU5BU5D_t932_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GcScoreData[]"/* name */
	, "UnityEngine.SocialPlatforms.GameCenter"/* namespaze */
	, GcScoreDataU5BU5D_t932_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GcScoreData_t933_il2cpp_TypeInfo/* element_class */
	, GcScoreDataU5BU5D_t932_InterfacesTypeInfos/* implemented_interfaces */
	, GcScoreDataU5BU5D_t932_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GcScoreDataU5BU5D_t932_il2cpp_TypeInfo/* cast_class */
	, &GcScoreDataU5BU5D_t932_0_0_0/* byval_arg */
	, &GcScoreDataU5BU5D_t932_1_0_0/* this_arg */
	, GcScoreDataU5BU5D_t932_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (GcScoreData_t933 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ScoreU5BU5D_t1108_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.Impl.Score[]
static MethodInfo* ScoreU5BU5D_t1108_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisScore_t1045_m36862_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisScore_t1045_m36863_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisScore_t1045_m36864_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisScore_t1045_m36865_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisScore_t1045_m36866_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisScore_t1045_m36867_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisScore_t1045_m36861_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisScore_t1045_m36869_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisScore_t1045_m36870_MethodInfo;
static MethodInfo* ScoreU5BU5D_t1108_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisScore_t1045_m36862_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisScore_t1045_m36863_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisScore_t1045_m36864_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisScore_t1045_m36865_MethodInfo,
	&Array_InternalArray__IndexOf_TisScore_t1045_m36866_MethodInfo,
	&Array_InternalArray__Insert_TisScore_t1045_m36867_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisScore_t1045_m36861_MethodInfo,
	&Array_InternalArray__set_Item_TisScore_t1045_m36869_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisScore_t1045_m36870_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIScore_t1053_m36759_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIScore_t1053_m36760_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIScore_t1053_m36761_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIScore_t1053_m36762_MethodInfo,
	&Array_InternalArray__IndexOf_TisIScore_t1053_m36763_MethodInfo,
	&Array_InternalArray__Insert_TisIScore_t1053_m36764_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIScore_t1053_m36758_MethodInfo,
	&Array_InternalArray__set_Item_TisIScore_t1053_m36766_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIScore_t1053_m36767_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8203_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8204_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8205_il2cpp_TypeInfo;
static TypeInfo* ScoreU5BU5D_t1108_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8203_il2cpp_TypeInfo,
	&IList_1_t8204_il2cpp_TypeInfo,
	&IEnumerable_1_t8205_il2cpp_TypeInfo,
	&ICollection_1_t8182_il2cpp_TypeInfo,
	&IList_1_t8183_il2cpp_TypeInfo,
	&IEnumerable_1_t8184_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair ScoreU5BU5D_t1108_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8203_il2cpp_TypeInfo, 21},
	{ &IList_1_t8204_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8205_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t8182_il2cpp_TypeInfo, 34},
	{ &IList_1_t8183_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t8184_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ScoreU5BU5D_t1108_0_0_0;
extern Il2CppType ScoreU5BU5D_t1108_1_0_0;
struct Score_t1045;
extern TypeInfo Score_t1045_il2cpp_TypeInfo;
extern CustomAttributesCache Score_t1045__CustomAttributeCache_U3CleaderboardIDU3Ek__BackingField;
extern CustomAttributesCache Score_t1045__CustomAttributeCache_U3CvalueU3Ek__BackingField;
extern CustomAttributesCache Score_t1045__CustomAttributeCache_Score_get_leaderboardID_m6288;
extern CustomAttributesCache Score_t1045__CustomAttributeCache_Score_set_leaderboardID_m6289;
extern CustomAttributesCache Score_t1045__CustomAttributeCache_Score_get_value_m6290;
extern CustomAttributesCache Score_t1045__CustomAttributeCache_Score_set_value_m6291;
TypeInfo ScoreU5BU5D_t1108_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Score[]"/* name */
	, "UnityEngine.SocialPlatforms.Impl"/* namespaze */
	, ScoreU5BU5D_t1108_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Score_t1045_il2cpp_TypeInfo/* element_class */
	, ScoreU5BU5D_t1108_InterfacesTypeInfos/* implemented_interfaces */
	, ScoreU5BU5D_t1108_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ScoreU5BU5D_t1108_il2cpp_TypeInfo/* cast_class */
	, &ScoreU5BU5D_t1108_0_0_0/* byval_arg */
	, &ScoreU5BU5D_t1108_1_0_0/* this_arg */
	, ScoreU5BU5D_t1108_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Score_t1045 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo MeshU5BU5D_t5420_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.Mesh[]
static MethodInfo* MeshU5BU5D_t5420_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisMesh_t174_m36874_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisMesh_t174_m36875_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisMesh_t174_m36876_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisMesh_t174_m36877_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisMesh_t174_m36878_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisMesh_t174_m36879_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMesh_t174_m36873_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisMesh_t174_m36881_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisMesh_t174_m36882_MethodInfo;
static MethodInfo* MeshU5BU5D_t5420_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisMesh_t174_m36874_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisMesh_t174_m36875_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisMesh_t174_m36876_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisMesh_t174_m36877_MethodInfo,
	&Array_InternalArray__IndexOf_TisMesh_t174_m36878_MethodInfo,
	&Array_InternalArray__Insert_TisMesh_t174_m36879_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisMesh_t174_m36873_MethodInfo,
	&Array_InternalArray__set_Item_TisMesh_t174_m36881_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisMesh_t174_m36882_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t117_m31492_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t117_m31493_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t117_m31494_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t117_m31495_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t117_m31496_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t117_m31497_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t117_m31499_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t117_m31500_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8206_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8207_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8208_il2cpp_TypeInfo;
static TypeInfo* MeshU5BU5D_t5420_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8206_il2cpp_TypeInfo,
	&IList_1_t8207_il2cpp_TypeInfo,
	&IEnumerable_1_t8208_il2cpp_TypeInfo,
	&ICollection_1_t7225_il2cpp_TypeInfo,
	&IList_1_t7226_il2cpp_TypeInfo,
	&IEnumerable_1_t7227_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair MeshU5BU5D_t5420_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8206_il2cpp_TypeInfo, 21},
	{ &IList_1_t8207_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8208_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7225_il2cpp_TypeInfo, 34},
	{ &IList_1_t7226_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7227_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType MeshU5BU5D_t5420_0_0_0;
extern Il2CppType MeshU5BU5D_t5420_1_0_0;
struct Mesh_t174;
extern TypeInfo Mesh_t174_il2cpp_TypeInfo;
extern CustomAttributesCache Mesh_t174__CustomAttributeCache_Mesh_Internal_Create_m5516;
extern CustomAttributesCache Mesh_t174__CustomAttributeCache_Mesh_Clear_m5517;
extern CustomAttributesCache Mesh_t174__CustomAttributeCache_Mesh_Clear_m4287;
extern CustomAttributesCache Mesh_t174__CustomAttributeCache_Mesh_get_vertices_m645;
extern CustomAttributesCache Mesh_t174__CustomAttributeCache_Mesh_set_vertices_m4288;
extern CustomAttributesCache Mesh_t174__CustomAttributeCache_Mesh_set_normals_m4292;
extern CustomAttributesCache Mesh_t174__CustomAttributeCache_Mesh_get_uv_m4291;
extern CustomAttributesCache Mesh_t174__CustomAttributeCache_Mesh_set_uv_m4290;
extern CustomAttributesCache Mesh_t174__CustomAttributeCache_Mesh_INTERNAL_get_bounds_m5518;
extern CustomAttributesCache Mesh_t174__CustomAttributeCache_Mesh_RecalculateNormals_m4293;
extern CustomAttributesCache Mesh_t174__CustomAttributeCache_Mesh_get_triangles_m646;
extern CustomAttributesCache Mesh_t174__CustomAttributeCache_Mesh_set_triangles_m4289;
TypeInfo MeshU5BU5D_t5420_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mesh[]"/* name */
	, "UnityEngine"/* namespaze */
	, MeshU5BU5D_t5420_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Mesh_t174_il2cpp_TypeInfo/* element_class */
	, MeshU5BU5D_t5420_InterfacesTypeInfos/* implemented_interfaces */
	, MeshU5BU5D_t5420_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &MeshU5BU5D_t5420_il2cpp_TypeInfo/* cast_class */
	, &MeshU5BU5D_t5420_0_0_0/* byval_arg */
	, &MeshU5BU5D_t5420_1_0_0/* this_arg */
	, MeshU5BU5D_t5420_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Mesh_t174 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TextureU5BU5D_t5421_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.Texture[]
static MethodInfo* TextureU5BU5D_t5421_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisTexture_t294_m36888_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisTexture_t294_m36889_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisTexture_t294_m36890_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisTexture_t294_m36891_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisTexture_t294_m36892_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisTexture_t294_m36893_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTexture_t294_m36887_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisTexture_t294_m36895_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisTexture_t294_m36896_MethodInfo;
static MethodInfo* TextureU5BU5D_t5421_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTexture_t294_m36888_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTexture_t294_m36889_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTexture_t294_m36890_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTexture_t294_m36891_MethodInfo,
	&Array_InternalArray__IndexOf_TisTexture_t294_m36892_MethodInfo,
	&Array_InternalArray__Insert_TisTexture_t294_m36893_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTexture_t294_m36887_MethodInfo,
	&Array_InternalArray__set_Item_TisTexture_t294_m36895_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTexture_t294_m36896_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t117_m31492_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t117_m31493_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t117_m31494_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t117_m31495_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t117_m31496_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t117_m31497_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t117_m31499_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t117_m31500_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8209_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8210_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8211_il2cpp_TypeInfo;
static TypeInfo* TextureU5BU5D_t5421_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8209_il2cpp_TypeInfo,
	&IList_1_t8210_il2cpp_TypeInfo,
	&IEnumerable_1_t8211_il2cpp_TypeInfo,
	&ICollection_1_t7225_il2cpp_TypeInfo,
	&IList_1_t7226_il2cpp_TypeInfo,
	&IEnumerable_1_t7227_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair TextureU5BU5D_t5421_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8209_il2cpp_TypeInfo, 21},
	{ &IList_1_t8210_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8211_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7225_il2cpp_TypeInfo, 34},
	{ &IList_1_t7226_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7227_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextureU5BU5D_t5421_0_0_0;
extern Il2CppType TextureU5BU5D_t5421_1_0_0;
struct Texture_t294;
extern TypeInfo Texture_t294_il2cpp_TypeInfo;
extern CustomAttributesCache Texture_t294__CustomAttributeCache_Texture_Internal_GetWidth_m5551;
extern CustomAttributesCache Texture_t294__CustomAttributeCache_Texture_Internal_GetHeight_m5552;
extern CustomAttributesCache Texture_t294__CustomAttributeCache_Texture_set_filterMode_m5404;
extern CustomAttributesCache Texture_t294__CustomAttributeCache_Texture_set_wrapMode_m5405;
extern CustomAttributesCache Texture_t294__CustomAttributeCache_Texture_GetNativeTextureID_m5406;
TypeInfo TextureU5BU5D_t5421_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Texture[]"/* name */
	, "UnityEngine"/* namespaze */
	, TextureU5BU5D_t5421_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Texture_t294_il2cpp_TypeInfo/* element_class */
	, TextureU5BU5D_t5421_InterfacesTypeInfos/* implemented_interfaces */
	, TextureU5BU5D_t5421_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TextureU5BU5D_t5421_il2cpp_TypeInfo/* cast_class */
	, &TextureU5BU5D_t5421_0_0_0/* byval_arg */
	, &TextureU5BU5D_t5421_1_0_0/* this_arg */
	, TextureU5BU5D_t5421_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Texture_t294 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Texture2DU5BU5D_t5422_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.Texture2D[]
static MethodInfo* Texture2DU5BU5D_t5422_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisTexture2D_t285_m36900_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisTexture2D_t285_m36901_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisTexture2D_t285_m36902_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisTexture2D_t285_m36903_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisTexture2D_t285_m36904_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisTexture2D_t285_m36905_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTexture2D_t285_m36899_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisTexture2D_t285_m36907_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisTexture2D_t285_m36908_MethodInfo;
static MethodInfo* Texture2DU5BU5D_t5422_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTexture2D_t285_m36900_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTexture2D_t285_m36901_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTexture2D_t285_m36902_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTexture2D_t285_m36903_MethodInfo,
	&Array_InternalArray__IndexOf_TisTexture2D_t285_m36904_MethodInfo,
	&Array_InternalArray__Insert_TisTexture2D_t285_m36905_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTexture2D_t285_m36899_MethodInfo,
	&Array_InternalArray__set_Item_TisTexture2D_t285_m36907_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTexture2D_t285_m36908_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTexture_t294_m36888_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTexture_t294_m36889_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTexture_t294_m36890_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTexture_t294_m36891_MethodInfo,
	&Array_InternalArray__IndexOf_TisTexture_t294_m36892_MethodInfo,
	&Array_InternalArray__Insert_TisTexture_t294_m36893_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTexture_t294_m36887_MethodInfo,
	&Array_InternalArray__set_Item_TisTexture_t294_m36895_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTexture_t294_m36896_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t117_m31492_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t117_m31493_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t117_m31494_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t117_m31495_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t117_m31496_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t117_m31497_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t117_m31499_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t117_m31500_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8212_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8213_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8214_il2cpp_TypeInfo;
static TypeInfo* Texture2DU5BU5D_t5422_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8212_il2cpp_TypeInfo,
	&IList_1_t8213_il2cpp_TypeInfo,
	&IEnumerable_1_t8214_il2cpp_TypeInfo,
	&ICollection_1_t8209_il2cpp_TypeInfo,
	&IList_1_t8210_il2cpp_TypeInfo,
	&IEnumerable_1_t8211_il2cpp_TypeInfo,
	&ICollection_1_t7225_il2cpp_TypeInfo,
	&IList_1_t7226_il2cpp_TypeInfo,
	&IEnumerable_1_t7227_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Texture2DU5BU5D_t5422_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8212_il2cpp_TypeInfo, 21},
	{ &IList_1_t8213_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8214_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t8209_il2cpp_TypeInfo, 34},
	{ &IList_1_t8210_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t8211_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7225_il2cpp_TypeInfo, 47},
	{ &IList_1_t7226_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7227_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 60},
	{ &IList_1_t2851_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType Texture2DU5BU5D_t5422_0_0_0;
extern Il2CppType Texture2DU5BU5D_t5422_1_0_0;
struct Texture2D_t285;
extern TypeInfo Texture2D_t285_il2cpp_TypeInfo;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_Internal_Create_m5553;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_get_format_m4724;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_get_whiteTexture_m2120;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_GetPixelBilinear_m2226;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_SetPixels_m4729;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_SetPixels_m5555;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_SetAllPixels32_m5556;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_SetPixels32_m5314;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_GetPixels_m4726;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_GetPixels_m5559;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_GetPixels32_m5560;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_GetPixels32_m5312;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_Apply_m5561;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_Apply_m5315;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_Resize_m4725;
extern CustomAttributesCache Texture2D_t285__CustomAttributeCache_Texture2D_INTERNAL_CALL_ReadPixels_m5562;
TypeInfo Texture2DU5BU5D_t5422_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Texture2D[]"/* name */
	, "UnityEngine"/* namespaze */
	, Texture2DU5BU5D_t5422_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Texture2D_t285_il2cpp_TypeInfo/* element_class */
	, Texture2DU5BU5D_t5422_InterfacesTypeInfos/* implemented_interfaces */
	, Texture2DU5BU5D_t5422_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Texture2DU5BU5D_t5422_il2cpp_TypeInfo/* cast_class */
	, &Texture2DU5BU5D_t5422_0_0_0/* byval_arg */
	, &Texture2DU5BU5D_t5422_1_0_0/* this_arg */
	, Texture2DU5BU5D_t5422_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (Texture2D_t285 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 73/* vtable_count */
	, 12/* interfaces_count */
	, 16/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo RenderTextureU5BU5D_t5423_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.RenderTexture[]
static MethodInfo* RenderTextureU5BU5D_t5423_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisRenderTexture_t742_m36912_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisRenderTexture_t742_m36913_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisRenderTexture_t742_m36914_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisRenderTexture_t742_m36915_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisRenderTexture_t742_m36916_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisRenderTexture_t742_m36917_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRenderTexture_t742_m36911_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisRenderTexture_t742_m36919_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisRenderTexture_t742_m36920_MethodInfo;
static MethodInfo* RenderTextureU5BU5D_t5423_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisRenderTexture_t742_m36912_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisRenderTexture_t742_m36913_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisRenderTexture_t742_m36914_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisRenderTexture_t742_m36915_MethodInfo,
	&Array_InternalArray__IndexOf_TisRenderTexture_t742_m36916_MethodInfo,
	&Array_InternalArray__Insert_TisRenderTexture_t742_m36917_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisRenderTexture_t742_m36911_MethodInfo,
	&Array_InternalArray__set_Item_TisRenderTexture_t742_m36919_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisRenderTexture_t742_m36920_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTexture_t294_m36888_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTexture_t294_m36889_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTexture_t294_m36890_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTexture_t294_m36891_MethodInfo,
	&Array_InternalArray__IndexOf_TisTexture_t294_m36892_MethodInfo,
	&Array_InternalArray__Insert_TisTexture_t294_m36893_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTexture_t294_m36887_MethodInfo,
	&Array_InternalArray__set_Item_TisTexture_t294_m36895_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTexture_t294_m36896_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t117_m31492_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t117_m31493_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t117_m31494_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t117_m31495_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t117_m31496_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t117_m31497_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t117_m31499_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t117_m31500_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8215_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8216_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8217_il2cpp_TypeInfo;
static TypeInfo* RenderTextureU5BU5D_t5423_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8215_il2cpp_TypeInfo,
	&IList_1_t8216_il2cpp_TypeInfo,
	&IEnumerable_1_t8217_il2cpp_TypeInfo,
	&ICollection_1_t8209_il2cpp_TypeInfo,
	&IList_1_t8210_il2cpp_TypeInfo,
	&IEnumerable_1_t8211_il2cpp_TypeInfo,
	&ICollection_1_t7225_il2cpp_TypeInfo,
	&IList_1_t7226_il2cpp_TypeInfo,
	&IEnumerable_1_t7227_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair RenderTextureU5BU5D_t5423_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8215_il2cpp_TypeInfo, 21},
	{ &IList_1_t8216_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8217_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t8209_il2cpp_TypeInfo, 34},
	{ &IList_1_t8210_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t8211_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7225_il2cpp_TypeInfo, 47},
	{ &IList_1_t7226_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7227_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 60},
	{ &IList_1_t2851_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType RenderTextureU5BU5D_t5423_0_0_0;
extern Il2CppType RenderTextureU5BU5D_t5423_1_0_0;
struct RenderTexture_t742;
extern TypeInfo RenderTexture_t742_il2cpp_TypeInfo;
extern CustomAttributesCache RenderTexture_t742__CustomAttributeCache_RenderTexture_GetTemporary_m5563;
extern CustomAttributesCache RenderTexture_t742__CustomAttributeCache_RenderTexture_GetTemporary_m5292;
extern CustomAttributesCache RenderTexture_t742__CustomAttributeCache_RenderTexture_ReleaseTemporary_m5313;
extern CustomAttributesCache RenderTexture_t742__CustomAttributeCache_RenderTexture_Internal_GetWidth_m5564;
extern CustomAttributesCache RenderTexture_t742__CustomAttributeCache_RenderTexture_Internal_GetHeight_m5565;
extern CustomAttributesCache RenderTexture_t742__CustomAttributeCache_RenderTexture_set_depth_m5295;
extern CustomAttributesCache RenderTexture_t742__CustomAttributeCache_RenderTexture_set_active_m5310;
TypeInfo RenderTextureU5BU5D_t5423_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RenderTexture[]"/* name */
	, "UnityEngine"/* namespaze */
	, RenderTextureU5BU5D_t5423_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &RenderTexture_t742_il2cpp_TypeInfo/* element_class */
	, RenderTextureU5BU5D_t5423_InterfacesTypeInfos/* implemented_interfaces */
	, RenderTextureU5BU5D_t5423_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &RenderTextureU5BU5D_t5423_il2cpp_TypeInfo/* cast_class */
	, &RenderTextureU5BU5D_t5423_0_0_0/* byval_arg */
	, &RenderTextureU5BU5D_t5423_1_0_0/* this_arg */
	, RenderTextureU5BU5D_t5423_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (RenderTexture_t742 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 73/* vtable_count */
	, 12/* interfaces_count */
	, 16/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ReflectionProbeU5BU5D_t5424_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.ReflectionProbe[]
static MethodInfo* ReflectionProbeU5BU5D_t5424_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisReflectionProbe_t944_m36924_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisReflectionProbe_t944_m36925_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisReflectionProbe_t944_m36926_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisReflectionProbe_t944_m36927_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisReflectionProbe_t944_m36928_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisReflectionProbe_t944_m36929_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisReflectionProbe_t944_m36923_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisReflectionProbe_t944_m36931_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisReflectionProbe_t944_m36932_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Add_TisBehaviour_t515_m31470_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisBehaviour_t515_m31471_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisBehaviour_t515_m31472_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisBehaviour_t515_m31473_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisBehaviour_t515_m31474_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisBehaviour_t515_m31475_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisBehaviour_t515_m31469_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisBehaviour_t515_m31477_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisBehaviour_t515_m31478_MethodInfo;
static MethodInfo* ReflectionProbeU5BU5D_t5424_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisReflectionProbe_t944_m36924_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisReflectionProbe_t944_m36925_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisReflectionProbe_t944_m36926_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisReflectionProbe_t944_m36927_MethodInfo,
	&Array_InternalArray__IndexOf_TisReflectionProbe_t944_m36928_MethodInfo,
	&Array_InternalArray__Insert_TisReflectionProbe_t944_m36929_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisReflectionProbe_t944_m36923_MethodInfo,
	&Array_InternalArray__set_Item_TisReflectionProbe_t944_m36931_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisReflectionProbe_t944_m36932_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisBehaviour_t515_m31470_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisBehaviour_t515_m31471_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisBehaviour_t515_m31472_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisBehaviour_t515_m31473_MethodInfo,
	&Array_InternalArray__IndexOf_TisBehaviour_t515_m31474_MethodInfo,
	&Array_InternalArray__Insert_TisBehaviour_t515_m31475_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisBehaviour_t515_m31469_MethodInfo,
	&Array_InternalArray__set_Item_TisBehaviour_t515_m31477_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisBehaviour_t515_m31478_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisComponent_t100_m31481_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisComponent_t100_m31482_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisComponent_t100_m31483_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisComponent_t100_m31484_MethodInfo,
	&Array_InternalArray__IndexOf_TisComponent_t100_m31485_MethodInfo,
	&Array_InternalArray__Insert_TisComponent_t100_m31486_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisComponent_t100_m31480_MethodInfo,
	&Array_InternalArray__set_Item_TisComponent_t100_m31488_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisComponent_t100_m31489_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t117_m31492_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t117_m31493_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t117_m31494_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t117_m31495_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t117_m31496_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t117_m31497_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t117_m31499_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t117_m31500_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8218_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8219_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8220_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7222_il2cpp_TypeInfo;
extern TypeInfo IList_1_t7223_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7224_il2cpp_TypeInfo;
static TypeInfo* ReflectionProbeU5BU5D_t5424_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8218_il2cpp_TypeInfo,
	&IList_1_t8219_il2cpp_TypeInfo,
	&IEnumerable_1_t8220_il2cpp_TypeInfo,
	&ICollection_1_t7222_il2cpp_TypeInfo,
	&IList_1_t7223_il2cpp_TypeInfo,
	&IEnumerable_1_t7224_il2cpp_TypeInfo,
	&ICollection_1_t3046_il2cpp_TypeInfo,
	&IList_1_t3050_il2cpp_TypeInfo,
	&IEnumerable_1_t3044_il2cpp_TypeInfo,
	&ICollection_1_t7225_il2cpp_TypeInfo,
	&IList_1_t7226_il2cpp_TypeInfo,
	&IEnumerable_1_t7227_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair ReflectionProbeU5BU5D_t5424_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8218_il2cpp_TypeInfo, 21},
	{ &IList_1_t8219_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8220_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7222_il2cpp_TypeInfo, 34},
	{ &IList_1_t7223_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7224_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t3046_il2cpp_TypeInfo, 47},
	{ &IList_1_t3050_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t3044_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7225_il2cpp_TypeInfo, 60},
	{ &IList_1_t7226_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7227_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 73},
	{ &IList_1_t2851_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ReflectionProbeU5BU5D_t5424_0_0_0;
extern Il2CppType ReflectionProbeU5BU5D_t5424_1_0_0;
struct ReflectionProbe_t944;
extern TypeInfo ReflectionProbe_t944_il2cpp_TypeInfo;
TypeInfo ReflectionProbeU5BU5D_t5424_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReflectionProbe[]"/* name */
	, "UnityEngine"/* namespaze */
	, ReflectionProbeU5BU5D_t5424_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ReflectionProbe_t944_il2cpp_TypeInfo/* element_class */
	, ReflectionProbeU5BU5D_t5424_InterfacesTypeInfos/* implemented_interfaces */
	, ReflectionProbeU5BU5D_t5424_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ReflectionProbeU5BU5D_t5424_il2cpp_TypeInfo/* cast_class */
	, &ReflectionProbeU5BU5D_t5424_0_0_0/* byval_arg */
	, &ReflectionProbeU5BU5D_t5424_1_0_0/* this_arg */
	, ReflectionProbeU5BU5D_t5424_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (ReflectionProbe_t944 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 86/* vtable_count */
	, 15/* interfaces_count */
	, 19/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GUIElementU5BU5D_t5425_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.GUIElement[]
static MethodInfo* GUIElementU5BU5D_t5425_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisGUIElement_t945_m36936_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisGUIElement_t945_m36937_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisGUIElement_t945_m36938_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisGUIElement_t945_m36939_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisGUIElement_t945_m36940_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisGUIElement_t945_m36941_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGUIElement_t945_m36935_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisGUIElement_t945_m36943_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisGUIElement_t945_m36944_MethodInfo;
static MethodInfo* GUIElementU5BU5D_t5425_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisGUIElement_t945_m36936_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisGUIElement_t945_m36937_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisGUIElement_t945_m36938_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisGUIElement_t945_m36939_MethodInfo,
	&Array_InternalArray__IndexOf_TisGUIElement_t945_m36940_MethodInfo,
	&Array_InternalArray__Insert_TisGUIElement_t945_m36941_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisGUIElement_t945_m36935_MethodInfo,
	&Array_InternalArray__set_Item_TisGUIElement_t945_m36943_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisGUIElement_t945_m36944_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisBehaviour_t515_m31470_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisBehaviour_t515_m31471_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisBehaviour_t515_m31472_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisBehaviour_t515_m31473_MethodInfo,
	&Array_InternalArray__IndexOf_TisBehaviour_t515_m31474_MethodInfo,
	&Array_InternalArray__Insert_TisBehaviour_t515_m31475_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisBehaviour_t515_m31469_MethodInfo,
	&Array_InternalArray__set_Item_TisBehaviour_t515_m31477_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisBehaviour_t515_m31478_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisComponent_t100_m31481_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisComponent_t100_m31482_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisComponent_t100_m31483_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisComponent_t100_m31484_MethodInfo,
	&Array_InternalArray__IndexOf_TisComponent_t100_m31485_MethodInfo,
	&Array_InternalArray__Insert_TisComponent_t100_m31486_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisComponent_t100_m31480_MethodInfo,
	&Array_InternalArray__set_Item_TisComponent_t100_m31488_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisComponent_t100_m31489_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t117_m31492_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t117_m31493_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t117_m31494_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t117_m31495_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t117_m31496_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t117_m31497_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t117_m31499_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t117_m31500_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8221_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8222_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8223_il2cpp_TypeInfo;
static TypeInfo* GUIElementU5BU5D_t5425_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8221_il2cpp_TypeInfo,
	&IList_1_t8222_il2cpp_TypeInfo,
	&IEnumerable_1_t8223_il2cpp_TypeInfo,
	&ICollection_1_t7222_il2cpp_TypeInfo,
	&IList_1_t7223_il2cpp_TypeInfo,
	&IEnumerable_1_t7224_il2cpp_TypeInfo,
	&ICollection_1_t3046_il2cpp_TypeInfo,
	&IList_1_t3050_il2cpp_TypeInfo,
	&IEnumerable_1_t3044_il2cpp_TypeInfo,
	&ICollection_1_t7225_il2cpp_TypeInfo,
	&IList_1_t7226_il2cpp_TypeInfo,
	&IEnumerable_1_t7227_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair GUIElementU5BU5D_t5425_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8221_il2cpp_TypeInfo, 21},
	{ &IList_1_t8222_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8223_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7222_il2cpp_TypeInfo, 34},
	{ &IList_1_t7223_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7224_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t3046_il2cpp_TypeInfo, 47},
	{ &IList_1_t3050_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t3044_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7225_il2cpp_TypeInfo, 60},
	{ &IList_1_t7226_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7227_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 73},
	{ &IList_1_t2851_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GUIElementU5BU5D_t5425_0_0_0;
extern Il2CppType GUIElementU5BU5D_t5425_1_0_0;
struct GUIElement_t945;
extern TypeInfo GUIElement_t945_il2cpp_TypeInfo;
TypeInfo GUIElementU5BU5D_t5425_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIElement[]"/* name */
	, "UnityEngine"/* namespaze */
	, GUIElementU5BU5D_t5425_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GUIElement_t945_il2cpp_TypeInfo/* element_class */
	, GUIElementU5BU5D_t5425_InterfacesTypeInfos/* implemented_interfaces */
	, GUIElementU5BU5D_t5425_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GUIElementU5BU5D_t5425_il2cpp_TypeInfo/* cast_class */
	, &GUIElementU5BU5D_t5425_0_0_0/* byval_arg */
	, &GUIElementU5BU5D_t5425_1_0_0/* this_arg */
	, GUIElementU5BU5D_t5425_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (GUIElement_t945 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 86/* vtable_count */
	, 15/* interfaces_count */
	, 19/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GUILayerU5BU5D_t5426_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.GUILayer[]
static MethodInfo* GUILayerU5BU5D_t5426_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisGUILayer_t946_m36948_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisGUILayer_t946_m36949_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisGUILayer_t946_m36950_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisGUILayer_t946_m36951_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisGUILayer_t946_m36952_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisGUILayer_t946_m36953_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGUILayer_t946_m36947_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisGUILayer_t946_m36955_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisGUILayer_t946_m36956_MethodInfo;
static MethodInfo* GUILayerU5BU5D_t5426_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisGUILayer_t946_m36948_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisGUILayer_t946_m36949_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisGUILayer_t946_m36950_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisGUILayer_t946_m36951_MethodInfo,
	&Array_InternalArray__IndexOf_TisGUILayer_t946_m36952_MethodInfo,
	&Array_InternalArray__Insert_TisGUILayer_t946_m36953_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisGUILayer_t946_m36947_MethodInfo,
	&Array_InternalArray__set_Item_TisGUILayer_t946_m36955_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisGUILayer_t946_m36956_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisBehaviour_t515_m31470_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisBehaviour_t515_m31471_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisBehaviour_t515_m31472_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisBehaviour_t515_m31473_MethodInfo,
	&Array_InternalArray__IndexOf_TisBehaviour_t515_m31474_MethodInfo,
	&Array_InternalArray__Insert_TisBehaviour_t515_m31475_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisBehaviour_t515_m31469_MethodInfo,
	&Array_InternalArray__set_Item_TisBehaviour_t515_m31477_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisBehaviour_t515_m31478_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisComponent_t100_m31481_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisComponent_t100_m31482_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisComponent_t100_m31483_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisComponent_t100_m31484_MethodInfo,
	&Array_InternalArray__IndexOf_TisComponent_t100_m31485_MethodInfo,
	&Array_InternalArray__Insert_TisComponent_t100_m31486_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisComponent_t100_m31480_MethodInfo,
	&Array_InternalArray__set_Item_TisComponent_t100_m31488_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisComponent_t100_m31489_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t117_m31492_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t117_m31493_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t117_m31494_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t117_m31495_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t117_m31496_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t117_m31497_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t117_m31499_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t117_m31500_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8224_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8225_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8226_il2cpp_TypeInfo;
static TypeInfo* GUILayerU5BU5D_t5426_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8224_il2cpp_TypeInfo,
	&IList_1_t8225_il2cpp_TypeInfo,
	&IEnumerable_1_t8226_il2cpp_TypeInfo,
	&ICollection_1_t7222_il2cpp_TypeInfo,
	&IList_1_t7223_il2cpp_TypeInfo,
	&IEnumerable_1_t7224_il2cpp_TypeInfo,
	&ICollection_1_t3046_il2cpp_TypeInfo,
	&IList_1_t3050_il2cpp_TypeInfo,
	&IEnumerable_1_t3044_il2cpp_TypeInfo,
	&ICollection_1_t7225_il2cpp_TypeInfo,
	&IList_1_t7226_il2cpp_TypeInfo,
	&IEnumerable_1_t7227_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair GUILayerU5BU5D_t5426_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8224_il2cpp_TypeInfo, 21},
	{ &IList_1_t8225_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8226_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7222_il2cpp_TypeInfo, 34},
	{ &IList_1_t7223_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7224_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t3046_il2cpp_TypeInfo, 47},
	{ &IList_1_t3050_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t3044_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7225_il2cpp_TypeInfo, 60},
	{ &IList_1_t7226_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7227_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 73},
	{ &IList_1_t2851_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 85},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GUILayerU5BU5D_t5426_0_0_0;
extern Il2CppType GUILayerU5BU5D_t5426_1_0_0;
struct GUILayer_t946;
extern TypeInfo GUILayer_t946_il2cpp_TypeInfo;
extern CustomAttributesCache GUILayer_t946__CustomAttributeCache_GUILayer_INTERNAL_CALL_HitTest_m5569;
TypeInfo GUILayerU5BU5D_t5426_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayer[]"/* name */
	, "UnityEngine"/* namespaze */
	, GUILayerU5BU5D_t5426_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GUILayer_t946_il2cpp_TypeInfo/* element_class */
	, GUILayerU5BU5D_t5426_InterfacesTypeInfos/* implemented_interfaces */
	, GUILayerU5BU5D_t5426_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GUILayerU5BU5D_t5426_il2cpp_TypeInfo/* cast_class */
	, &GUILayerU5BU5D_t5426_0_0_0/* byval_arg */
	, &GUILayerU5BU5D_t5426_1_0_0/* this_arg */
	, GUILayerU5BU5D_t5426_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (GUILayer_t946 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 86/* vtable_count */
	, 15/* interfaces_count */
	, 19/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GUILayoutOptionU5BU5D_t960_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.GUILayoutOption[]
static MethodInfo* GUILayoutOptionU5BU5D_t960_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisGUILayoutOption_t955_m36960_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisGUILayoutOption_t955_m36961_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisGUILayoutOption_t955_m36962_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisGUILayoutOption_t955_m36963_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisGUILayoutOption_t955_m36964_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisGUILayoutOption_t955_m36965_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGUILayoutOption_t955_m36959_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisGUILayoutOption_t955_m36967_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisGUILayoutOption_t955_m36968_MethodInfo;
static MethodInfo* GUILayoutOptionU5BU5D_t960_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisGUILayoutOption_t955_m36960_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisGUILayoutOption_t955_m36961_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisGUILayoutOption_t955_m36962_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisGUILayoutOption_t955_m36963_MethodInfo,
	&Array_InternalArray__IndexOf_TisGUILayoutOption_t955_m36964_MethodInfo,
	&Array_InternalArray__Insert_TisGUILayoutOption_t955_m36965_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisGUILayoutOption_t955_m36959_MethodInfo,
	&Array_InternalArray__set_Item_TisGUILayoutOption_t955_m36967_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisGUILayoutOption_t955_m36968_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8227_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8228_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8229_il2cpp_TypeInfo;
static TypeInfo* GUILayoutOptionU5BU5D_t960_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8227_il2cpp_TypeInfo,
	&IList_1_t8228_il2cpp_TypeInfo,
	&IEnumerable_1_t8229_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair GUILayoutOptionU5BU5D_t960_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8227_il2cpp_TypeInfo, 21},
	{ &IList_1_t8228_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8229_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 34},
	{ &IList_1_t2851_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 46},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GUILayoutOptionU5BU5D_t960_0_0_0;
extern Il2CppType GUILayoutOptionU5BU5D_t960_1_0_0;
struct GUILayoutOption_t955;
extern TypeInfo GUILayoutOption_t955_il2cpp_TypeInfo;
TypeInfo GUILayoutOptionU5BU5D_t960_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayoutOption[]"/* name */
	, "UnityEngine"/* namespaze */
	, GUILayoutOptionU5BU5D_t960_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GUILayoutOption_t955_il2cpp_TypeInfo/* element_class */
	, GUILayoutOptionU5BU5D_t960_InterfacesTypeInfos/* implemented_interfaces */
	, GUILayoutOptionU5BU5D_t960_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GUILayoutOptionU5BU5D_t960_il2cpp_TypeInfo/* cast_class */
	, &GUILayoutOptionU5BU5D_t960_0_0_0/* byval_arg */
	, &GUILayoutOptionU5BU5D_t960_1_0_0/* this_arg */
	, GUILayoutOptionU5BU5D_t960_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (GUILayoutOption_t955 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 47/* vtable_count */
	, 6/* interfaces_count */
	, 10/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t4507_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
static MethodInfo* KeyValuePair_2U5BU5D_t4507_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4508_m36971_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4508_m36972_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4508_m36973_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4508_m36974_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t4508_m36975_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t4508_m36976_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t4508_m36970_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t4508_m36978_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4508_m36979_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t4507_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4508_m36971_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4508_m36972_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4508_m36973_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4508_m36974_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t4508_m36975_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t4508_m36976_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t4508_m36970_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t4508_m36978_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4508_m36979_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8230_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8231_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8232_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t4507_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8230_il2cpp_TypeInfo,
	&IList_1_t8231_il2cpp_TypeInfo,
	&IEnumerable_1_t8232_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t4507_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8230_il2cpp_TypeInfo, 21},
	{ &IList_1_t8231_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8232_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t4507_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t4507_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_24.h"
extern TypeInfo KeyValuePair_2_t4508_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t4507_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t4507_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t4508_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t4507_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t4507_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t4507_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t4507_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t4507_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t4507_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t4508 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo LayoutCacheU5BU5D_t4503_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.GUILayoutUtility/LayoutCache[]
static MethodInfo* LayoutCacheU5BU5D_t4503_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisLayoutCache_t957_m36982_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisLayoutCache_t957_m36983_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisLayoutCache_t957_m36984_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisLayoutCache_t957_m36985_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisLayoutCache_t957_m36986_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisLayoutCache_t957_m36987_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisLayoutCache_t957_m36981_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisLayoutCache_t957_m36989_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisLayoutCache_t957_m36990_MethodInfo;
static MethodInfo* LayoutCacheU5BU5D_t4503_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisLayoutCache_t957_m36982_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisLayoutCache_t957_m36983_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisLayoutCache_t957_m36984_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisLayoutCache_t957_m36985_MethodInfo,
	&Array_InternalArray__IndexOf_TisLayoutCache_t957_m36986_MethodInfo,
	&Array_InternalArray__Insert_TisLayoutCache_t957_m36987_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisLayoutCache_t957_m36981_MethodInfo,
	&Array_InternalArray__set_Item_TisLayoutCache_t957_m36989_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisLayoutCache_t957_m36990_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8233_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8234_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8235_il2cpp_TypeInfo;
static TypeInfo* LayoutCacheU5BU5D_t4503_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8233_il2cpp_TypeInfo,
	&IList_1_t8234_il2cpp_TypeInfo,
	&IEnumerable_1_t8235_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair LayoutCacheU5BU5D_t4503_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8233_il2cpp_TypeInfo, 21},
	{ &IList_1_t8234_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8235_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 34},
	{ &IList_1_t2851_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 46},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType LayoutCacheU5BU5D_t4503_0_0_0;
extern Il2CppType LayoutCacheU5BU5D_t4503_1_0_0;
struct LayoutCache_t957;
extern TypeInfo LayoutCache_t957_il2cpp_TypeInfo;
TypeInfo LayoutCacheU5BU5D_t4503_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "LayoutCache[]"/* name */
	, ""/* namespaze */
	, LayoutCacheU5BU5D_t4503_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &LayoutCache_t957_il2cpp_TypeInfo/* element_class */
	, LayoutCacheU5BU5D_t4503_InterfacesTypeInfos/* implemented_interfaces */
	, LayoutCacheU5BU5D_t4503_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &LayoutCacheU5BU5D_t4503_il2cpp_TypeInfo/* cast_class */
	, &LayoutCacheU5BU5D_t4503_0_0_0/* byval_arg */
	, &LayoutCacheU5BU5D_t4503_1_0_0/* this_arg */
	, LayoutCacheU5BU5D_t4503_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (LayoutCache_t957 *)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048837/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 47/* vtable_count */
	, 6/* interfaces_count */
	, 10/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GUILayoutEntryU5BU5D_t4522_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.GUILayoutEntry[]
static MethodInfo* GUILayoutEntryU5BU5D_t4522_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisGUILayoutEntry_t961_m37003_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisGUILayoutEntry_t961_m37004_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisGUILayoutEntry_t961_m37005_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisGUILayoutEntry_t961_m37006_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisGUILayoutEntry_t961_m37007_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisGUILayoutEntry_t961_m37008_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGUILayoutEntry_t961_m37002_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisGUILayoutEntry_t961_m37010_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisGUILayoutEntry_t961_m37011_MethodInfo;
static MethodInfo* GUILayoutEntryU5BU5D_t4522_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisGUILayoutEntry_t961_m37003_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisGUILayoutEntry_t961_m37004_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisGUILayoutEntry_t961_m37005_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisGUILayoutEntry_t961_m37006_MethodInfo,
	&Array_InternalArray__IndexOf_TisGUILayoutEntry_t961_m37007_MethodInfo,
	&Array_InternalArray__Insert_TisGUILayoutEntry_t961_m37008_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisGUILayoutEntry_t961_m37002_MethodInfo,
	&Array_InternalArray__set_Item_TisGUILayoutEntry_t961_m37010_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisGUILayoutEntry_t961_m37011_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t4525_il2cpp_TypeInfo;
extern TypeInfo IList_1_t4530_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t4523_il2cpp_TypeInfo;
static TypeInfo* GUILayoutEntryU5BU5D_t4522_InterfacesTypeInfos[] = 
{
	&ICollection_1_t4525_il2cpp_TypeInfo,
	&IList_1_t4530_il2cpp_TypeInfo,
	&IEnumerable_1_t4523_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair GUILayoutEntryU5BU5D_t4522_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t4525_il2cpp_TypeInfo, 21},
	{ &IList_1_t4530_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t4523_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 34},
	{ &IList_1_t2851_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 46},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GUILayoutEntryU5BU5D_t4522_0_0_0;
extern Il2CppType GUILayoutEntryU5BU5D_t4522_1_0_0;
struct GUILayoutEntry_t961;
extern TypeInfo GUILayoutEntry_t961_il2cpp_TypeInfo;
TypeInfo GUILayoutEntryU5BU5D_t4522_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUILayoutEntry[]"/* name */
	, "UnityEngine"/* namespaze */
	, GUILayoutEntryU5BU5D_t4522_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GUILayoutEntry_t961_il2cpp_TypeInfo/* element_class */
	, GUILayoutEntryU5BU5D_t4522_InterfacesTypeInfos/* implemented_interfaces */
	, GUILayoutEntryU5BU5D_t4522_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GUILayoutEntryU5BU5D_t4522_il2cpp_TypeInfo/* cast_class */
	, &GUILayoutEntryU5BU5D_t4522_0_0_0/* byval_arg */
	, &GUILayoutEntryU5BU5D_t4522_1_0_0/* this_arg */
	, GUILayoutEntryU5BU5D_t4522_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (GUILayoutEntry_t961 *)/* element_size */
	, -1/* native_size */
	, sizeof(GUILayoutEntryU5BU5D_t4522_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 47/* vtable_count */
	, 6/* interfaces_count */
	, 10/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TypeU5BU5D_t5427_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.GUILayoutOption/Type[]
static MethodInfo* TypeU5BU5D_t5427_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisType_t964_m37029_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisType_t964_m37030_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisType_t964_m37031_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisType_t964_m37032_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisType_t964_m37033_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisType_t964_m37034_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisType_t964_m37028_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisType_t964_m37036_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisType_t964_m37037_MethodInfo;
static MethodInfo* TypeU5BU5D_t5427_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisType_t964_m37029_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisType_t964_m37030_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisType_t964_m37031_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisType_t964_m37032_MethodInfo,
	&Array_InternalArray__IndexOf_TisType_t964_m37033_MethodInfo,
	&Array_InternalArray__Insert_TisType_t964_m37034_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisType_t964_m37028_MethodInfo,
	&Array_InternalArray__set_Item_TisType_t964_m37036_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisType_t964_m37037_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8236_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8237_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8238_il2cpp_TypeInfo;
static TypeInfo* TypeU5BU5D_t5427_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8236_il2cpp_TypeInfo,
	&IList_1_t8237_il2cpp_TypeInfo,
	&IEnumerable_1_t8238_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair TypeU5BU5D_t5427_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8236_il2cpp_TypeInfo, 21},
	{ &IList_1_t8237_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8238_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TypeU5BU5D_t5427_0_0_0;
extern Il2CppType TypeU5BU5D_t5427_1_0_0;
// UnityEngine.GUILayoutOption/Type
#include "UnityEngine_UnityEngine_GUILayoutOption_Type.h"
extern TypeInfo Type_t964_il2cpp_TypeInfo;
TypeInfo TypeU5BU5D_t5427_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Type[]"/* name */
	, ""/* namespaze */
	, TypeU5BU5D_t5427_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Type_t964_il2cpp_TypeInfo/* element_class */
	, TypeU5BU5D_t5427_InterfacesTypeInfos/* implemented_interfaces */
	, TypeU5BU5D_t5427_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &TypeU5BU5D_t5427_0_0_0/* byval_arg */
	, &TypeU5BU5D_t5427_1_0_0/* this_arg */
	, TypeU5BU5D_t5427_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 261/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GUISkinU5BU5D_t5428_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.GUISkin[]
static MethodInfo* GUISkinU5BU5D_t5428_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisGUISkin_t951_m37040_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisGUISkin_t951_m37041_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisGUISkin_t951_m37042_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisGUISkin_t951_m37043_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisGUISkin_t951_m37044_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisGUISkin_t951_m37045_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGUISkin_t951_m37039_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisGUISkin_t951_m37047_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisGUISkin_t951_m37048_MethodInfo;
static MethodInfo* GUISkinU5BU5D_t5428_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisGUISkin_t951_m37040_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisGUISkin_t951_m37041_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisGUISkin_t951_m37042_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisGUISkin_t951_m37043_MethodInfo,
	&Array_InternalArray__IndexOf_TisGUISkin_t951_m37044_MethodInfo,
	&Array_InternalArray__Insert_TisGUISkin_t951_m37045_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisGUISkin_t951_m37039_MethodInfo,
	&Array_InternalArray__set_Item_TisGUISkin_t951_m37047_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisGUISkin_t951_m37048_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisScriptableObject_t919_m36725_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisScriptableObject_t919_m36726_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisScriptableObject_t919_m36727_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisScriptableObject_t919_m36728_MethodInfo,
	&Array_InternalArray__IndexOf_TisScriptableObject_t919_m36729_MethodInfo,
	&Array_InternalArray__Insert_TisScriptableObject_t919_m36730_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisScriptableObject_t919_m36724_MethodInfo,
	&Array_InternalArray__set_Item_TisScriptableObject_t919_m36732_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisScriptableObject_t919_m36733_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t117_m31492_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t117_m31493_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t117_m31494_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t117_m31495_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t117_m31496_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t117_m31497_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t117_m31499_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t117_m31500_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8239_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8240_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8241_il2cpp_TypeInfo;
static TypeInfo* GUISkinU5BU5D_t5428_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8239_il2cpp_TypeInfo,
	&IList_1_t8240_il2cpp_TypeInfo,
	&IEnumerable_1_t8241_il2cpp_TypeInfo,
	&ICollection_1_t8173_il2cpp_TypeInfo,
	&IList_1_t8174_il2cpp_TypeInfo,
	&IEnumerable_1_t8175_il2cpp_TypeInfo,
	&ICollection_1_t7225_il2cpp_TypeInfo,
	&IList_1_t7226_il2cpp_TypeInfo,
	&IEnumerable_1_t7227_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair GUISkinU5BU5D_t5428_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8239_il2cpp_TypeInfo, 21},
	{ &IList_1_t8240_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8241_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t8173_il2cpp_TypeInfo, 34},
	{ &IList_1_t8174_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t8175_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7225_il2cpp_TypeInfo, 47},
	{ &IList_1_t7226_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7227_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 60},
	{ &IList_1_t2851_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 72},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GUISkinU5BU5D_t5428_0_0_0;
extern Il2CppType GUISkinU5BU5D_t5428_1_0_0;
struct GUISkin_t951;
extern TypeInfo GUISkin_t951_il2cpp_TypeInfo;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_Font;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_box;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_button;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_toggle;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_label;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_textField;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_textArea;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_window;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_horizontalSlider;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_horizontalSliderThumb;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_verticalSlider;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_verticalSliderThumb;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_horizontalScrollbar;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_horizontalScrollbarThumb;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_horizontalScrollbarLeftButton;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_horizontalScrollbarRightButton;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_verticalScrollbar;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_verticalScrollbarThumb;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_verticalScrollbarUpButton;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_verticalScrollbarDownButton;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_ScrollView;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_CustomStyles;
extern CustomAttributesCache GUISkin_t951__CustomAttributeCache_m_Settings;
TypeInfo GUISkinU5BU5D_t5428_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUISkin[]"/* name */
	, "UnityEngine"/* namespaze */
	, GUISkinU5BU5D_t5428_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GUISkin_t951_il2cpp_TypeInfo/* element_class */
	, GUISkinU5BU5D_t5428_InterfacesTypeInfos/* implemented_interfaces */
	, GUISkinU5BU5D_t5428_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GUISkinU5BU5D_t5428_il2cpp_TypeInfo/* cast_class */
	, &GUISkinU5BU5D_t5428_0_0_0/* byval_arg */
	, &GUISkinU5BU5D_t5428_1_0_0/* this_arg */
	, GUISkinU5BU5D_t5428_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (GUISkin_t951 *)/* element_size */
	, -1/* native_size */
	, sizeof(GUISkinU5BU5D_t5428_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057025/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 73/* vtable_count */
	, 12/* interfaces_count */
	, 16/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GUIStyleU5BU5D_t969_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.GUIStyle[]
static MethodInfo* GUIStyleU5BU5D_t969_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisGUIStyle_t953_m37052_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisGUIStyle_t953_m37053_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisGUIStyle_t953_m37054_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisGUIStyle_t953_m37055_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisGUIStyle_t953_m37056_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisGUIStyle_t953_m37057_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGUIStyle_t953_m37051_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisGUIStyle_t953_m37059_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisGUIStyle_t953_m37060_MethodInfo;
static MethodInfo* GUIStyleU5BU5D_t969_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisGUIStyle_t953_m37052_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisGUIStyle_t953_m37053_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisGUIStyle_t953_m37054_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisGUIStyle_t953_m37055_MethodInfo,
	&Array_InternalArray__IndexOf_TisGUIStyle_t953_m37056_MethodInfo,
	&Array_InternalArray__Insert_TisGUIStyle_t953_m37057_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisGUIStyle_t953_m37051_MethodInfo,
	&Array_InternalArray__set_Item_TisGUIStyle_t953_m37059_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisGUIStyle_t953_m37060_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8242_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8243_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8244_il2cpp_TypeInfo;
static TypeInfo* GUIStyleU5BU5D_t969_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8242_il2cpp_TypeInfo,
	&IList_1_t8243_il2cpp_TypeInfo,
	&IEnumerable_1_t8244_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair GUIStyleU5BU5D_t969_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8242_il2cpp_TypeInfo, 21},
	{ &IList_1_t8243_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8244_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 34},
	{ &IList_1_t2851_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 46},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GUIStyleU5BU5D_t969_0_0_0;
extern Il2CppType GUIStyleU5BU5D_t969_1_0_0;
struct GUIStyle_t953;
extern TypeInfo GUIStyle_t953_il2cpp_TypeInfo;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_Init_m5736;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_Cleanup_m5737;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_get_name_m5738;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_set_name_m5739;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_GetStyleStatePtr_m5741;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_GetRectOffsetPtr_m5744;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_get_fixedWidth_m5745;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_get_fixedHeight_m5746;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_get_stretchWidth_m5747;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_set_stretchWidth_m5748;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_get_stretchHeight_m5749;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_set_stretchHeight_m5750;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_Internal_GetLineHeight_m5751;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_SetDefaultFont_m5753;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m5757;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_Internal_CalcSize_m5759;
extern CustomAttributesCache GUIStyle_t953__CustomAttributeCache_GUIStyle_Internal_CalcHeight_m5761;
TypeInfo GUIStyleU5BU5D_t969_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GUIStyle[]"/* name */
	, "UnityEngine"/* namespaze */
	, GUIStyleU5BU5D_t969_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GUIStyle_t953_il2cpp_TypeInfo/* element_class */
	, GUIStyleU5BU5D_t969_InterfacesTypeInfos/* implemented_interfaces */
	, GUIStyleU5BU5D_t969_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GUIStyleU5BU5D_t969_il2cpp_TypeInfo/* cast_class */
	, &GUIStyleU5BU5D_t969_0_0_0/* byval_arg */
	, &GUIStyleU5BU5D_t969_1_0_0/* this_arg */
	, GUIStyleU5BU5D_t969_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (GUIStyle_t953 *)/* element_size */
	, -1/* native_size */
	, sizeof(GUIStyleU5BU5D_t969_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, true/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 47/* vtable_count */
	, 6/* interfaces_count */
	, 10/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeyValuePair_2U5BU5D_t4544_il2cpp_TypeInfo;



// Metadata Definition System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
static MethodInfo* KeyValuePair_2U5BU5D_t4544_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4545_m37063_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4545_m37064_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4545_m37065_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4545_m37066_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisKeyValuePair_2_t4545_m37067_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisKeyValuePair_2_t4545_m37068_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyValuePair_2_t4545_m37062_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisKeyValuePair_2_t4545_m37070_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4545_m37071_MethodInfo;
static MethodInfo* KeyValuePair_2U5BU5D_t4544_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisKeyValuePair_2_t4545_m37063_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisKeyValuePair_2_t4545_m37064_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisKeyValuePair_2_t4545_m37065_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisKeyValuePair_2_t4545_m37066_MethodInfo,
	&Array_InternalArray__IndexOf_TisKeyValuePair_2_t4545_m37067_MethodInfo,
	&Array_InternalArray__Insert_TisKeyValuePair_2_t4545_m37068_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisKeyValuePair_2_t4545_m37062_MethodInfo,
	&Array_InternalArray__set_Item_TisKeyValuePair_2_t4545_m37070_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisKeyValuePair_2_t4545_m37071_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8245_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8246_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8247_il2cpp_TypeInfo;
static TypeInfo* KeyValuePair_2U5BU5D_t4544_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8245_il2cpp_TypeInfo,
	&IList_1_t8246_il2cpp_TypeInfo,
	&IEnumerable_1_t8247_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair KeyValuePair_2U5BU5D_t4544_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8245_il2cpp_TypeInfo, 21},
	{ &IList_1_t8246_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8247_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 34},
	{ &IList_1_t7244_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 47},
	{ &IList_1_t2851_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 59},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType KeyValuePair_2U5BU5D_t4544_0_0_0;
extern Il2CppType KeyValuePair_2U5BU5D_t4544_1_0_0;
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25.h"
extern TypeInfo KeyValuePair_2_t4545_il2cpp_TypeInfo;
extern CustomAttributesCache KeyValuePair_2_t1816__CustomAttributeCache;
TypeInfo KeyValuePair_2U5BU5D_t4544_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeyValuePair`2[]"/* name */
	, "System.Collections.Generic"/* namespaze */
	, KeyValuePair_2U5BU5D_t4544_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeyValuePair_2_t4545_il2cpp_TypeInfo/* element_class */
	, KeyValuePair_2U5BU5D_t4544_InterfacesTypeInfos/* implemented_interfaces */
	, KeyValuePair_2U5BU5D_t4544_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &KeyValuePair_2U5BU5D_t4544_il2cpp_TypeInfo/* cast_class */
	, &KeyValuePair_2U5BU5D_t4544_0_0_0/* byval_arg */
	, &KeyValuePair_2U5BU5D_t4544_1_0_0/* this_arg */
	, KeyValuePair_2U5BU5D_t4544_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (KeyValuePair_2_t4545 )/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057033/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 60/* vtable_count */
	, 9/* interfaces_count */
	, 13/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo FontStyleU5BU5D_t5429_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.FontStyle[]
static MethodInfo* FontStyleU5BU5D_t5429_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisFontStyle_t458_m37084_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisFontStyle_t458_m37085_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisFontStyle_t458_m37086_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisFontStyle_t458_m37087_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisFontStyle_t458_m37088_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisFontStyle_t458_m37089_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFontStyle_t458_m37083_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisFontStyle_t458_m37091_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisFontStyle_t458_m37092_MethodInfo;
static MethodInfo* FontStyleU5BU5D_t5429_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisFontStyle_t458_m37084_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisFontStyle_t458_m37085_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisFontStyle_t458_m37086_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisFontStyle_t458_m37087_MethodInfo,
	&Array_InternalArray__IndexOf_TisFontStyle_t458_m37088_MethodInfo,
	&Array_InternalArray__Insert_TisFontStyle_t458_m37089_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisFontStyle_t458_m37083_MethodInfo,
	&Array_InternalArray__set_Item_TisFontStyle_t458_m37091_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisFontStyle_t458_m37092_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8248_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8249_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8250_il2cpp_TypeInfo;
static TypeInfo* FontStyleU5BU5D_t5429_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8248_il2cpp_TypeInfo,
	&IList_1_t8249_il2cpp_TypeInfo,
	&IEnumerable_1_t8250_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair FontStyleU5BU5D_t5429_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8248_il2cpp_TypeInfo, 21},
	{ &IList_1_t8249_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8250_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType FontStyleU5BU5D_t5429_0_0_0;
extern Il2CppType FontStyleU5BU5D_t5429_1_0_0;
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
extern TypeInfo FontStyle_t458_il2cpp_TypeInfo;
TypeInfo FontStyleU5BU5D_t5429_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FontStyle[]"/* name */
	, "UnityEngine"/* namespaze */
	, FontStyleU5BU5D_t5429_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &FontStyle_t458_il2cpp_TypeInfo/* element_class */
	, FontStyleU5BU5D_t5429_InterfacesTypeInfos/* implemented_interfaces */
	, FontStyleU5BU5D_t5429_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &FontStyleU5BU5D_t5429_0_0_0/* byval_arg */
	, &FontStyleU5BU5D_t5429_1_0_0/* this_arg */
	, FontStyleU5BU5D_t5429_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TouchScreenKeyboardTypeU5BU5D_t5430_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.TouchScreenKeyboardType[]
static MethodInfo* TouchScreenKeyboardTypeU5BU5D_t5430_MethodInfos[] =
{
	NULL
};
extern MethodInfo Array_InternalArray__ICollection_Add_TisTouchScreenKeyboardType_t491_m37095_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Contains_TisTouchScreenKeyboardType_t491_m37096_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_CopyTo_TisTouchScreenKeyboardType_t491_m37097_MethodInfo;
extern MethodInfo Array_InternalArray__ICollection_Remove_TisTouchScreenKeyboardType_t491_m37098_MethodInfo;
extern MethodInfo Array_InternalArray__IndexOf_TisTouchScreenKeyboardType_t491_m37099_MethodInfo;
extern MethodInfo Array_InternalArray__Insert_TisTouchScreenKeyboardType_t491_m37100_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTouchScreenKeyboardType_t491_m37094_MethodInfo;
extern MethodInfo Array_InternalArray__set_Item_TisTouchScreenKeyboardType_t491_m37102_MethodInfo;
extern MethodInfo Array_InternalArray__IEnumerable_GetEnumerator_TisTouchScreenKeyboardType_t491_m37103_MethodInfo;
static MethodInfo* TouchScreenKeyboardTypeU5BU5D_t5430_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Array_GetEnumerator_m7880_MethodInfo,
	&Array_System_Collections_ICollection_get_Count_m9576_MethodInfo,
	&Array_get_IsSynchronized_m9592_MethodInfo,
	&Array_get_SyncRoot_m9593_MethodInfo,
	&Array_CopyTo_m7879_MethodInfo,
	&Array_get_IsFixedSize_m9594_MethodInfo,
	&Array_get_IsReadOnly_m9595_MethodInfo,
	&Array_System_Collections_IList_get_Item_m9567_MethodInfo,
	&Array_System_Collections_IList_set_Item_m9568_MethodInfo,
	&Array_System_Collections_IList_Add_m9569_MethodInfo,
	&Array_System_Collections_IList_Clear_m9570_MethodInfo,
	&Array_System_Collections_IList_Contains_m9571_MethodInfo,
	&Array_System_Collections_IList_IndexOf_m9572_MethodInfo,
	&Array_System_Collections_IList_Insert_m9573_MethodInfo,
	&Array_System_Collections_IList_Remove_m9574_MethodInfo,
	&Array_System_Collections_IList_RemoveAt_m9575_MethodInfo,
	&Array_Clone_m7689_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisTouchScreenKeyboardType_t491_m37095_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisTouchScreenKeyboardType_t491_m37096_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisTouchScreenKeyboardType_t491_m37097_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisTouchScreenKeyboardType_t491_m37098_MethodInfo,
	&Array_InternalArray__IndexOf_TisTouchScreenKeyboardType_t491_m37099_MethodInfo,
	&Array_InternalArray__Insert_TisTouchScreenKeyboardType_t491_m37100_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisTouchScreenKeyboardType_t491_m37094_MethodInfo,
	&Array_InternalArray__set_Item_TisTouchScreenKeyboardType_t491_m37102_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisTouchScreenKeyboardType_t491_m37103_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisEnum_t97_m31516_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisEnum_t97_m31517_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisEnum_t97_m31518_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisEnum_t97_m31519_MethodInfo,
	&Array_InternalArray__IndexOf_TisEnum_t97_m31520_MethodInfo,
	&Array_InternalArray__Insert_TisEnum_t97_m31521_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo,
	&Array_InternalArray__set_Item_TisEnum_t97_m31523_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisEnum_t97_m31524_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIFormattable_t94_m31527_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIFormattable_t94_m31528_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIFormattable_t94_m31529_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIFormattable_t94_m31530_MethodInfo,
	&Array_InternalArray__IndexOf_TisIFormattable_t94_m31531_MethodInfo,
	&Array_InternalArray__Insert_TisIFormattable_t94_m31532_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo,
	&Array_InternalArray__set_Item_TisIFormattable_t94_m31534_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIFormattable_t94_m31535_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIConvertible_t95_m31538_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIConvertible_t95_m31539_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIConvertible_t95_m31540_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIConvertible_t95_m31541_MethodInfo,
	&Array_InternalArray__IndexOf_TisIConvertible_t95_m31542_MethodInfo,
	&Array_InternalArray__Insert_TisIConvertible_t95_m31543_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo,
	&Array_InternalArray__set_Item_TisIConvertible_t95_m31545_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIConvertible_t95_m31546_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisIComparable_t96_m31549_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisIComparable_t96_m31550_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisIComparable_t96_m31551_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisIComparable_t96_m31552_MethodInfo,
	&Array_InternalArray__IndexOf_TisIComparable_t96_m31553_MethodInfo,
	&Array_InternalArray__Insert_TisIComparable_t96_m31554_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo,
	&Array_InternalArray__set_Item_TisIComparable_t96_m31556_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisIComparable_t96_m31557_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisValueType_t436_m31560_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisValueType_t436_m31561_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisValueType_t436_m31562_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisValueType_t436_m31563_MethodInfo,
	&Array_InternalArray__IndexOf_TisValueType_t436_m31564_MethodInfo,
	&Array_InternalArray__Insert_TisValueType_t436_m31565_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo,
	&Array_InternalArray__set_Item_TisValueType_t436_m31567_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisValueType_t436_m31568_MethodInfo,
	&Array_InternalArray__ICollection_get_Count_m9577_MethodInfo,
	&Array_InternalArray__ICollection_get_IsReadOnly_m9578_MethodInfo,
	&Array_InternalArray__ICollection_Add_TisObject_t_m31440_MethodInfo,
	&Array_InternalArray__ICollection_Clear_m9579_MethodInfo,
	&Array_InternalArray__ICollection_Contains_TisObject_t_m31442_MethodInfo,
	&Array_InternalArray__ICollection_CopyTo_TisObject_t_m31444_MethodInfo,
	&Array_InternalArray__ICollection_Remove_TisObject_t_m31445_MethodInfo,
	&Array_InternalArray__IndexOf_TisObject_t_m31446_MethodInfo,
	&Array_InternalArray__Insert_TisObject_t_m31447_MethodInfo,
	&Array_InternalArray__RemoveAt_m9580_MethodInfo,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo,
	&Array_InternalArray__set_Item_TisObject_t_m31449_MethodInfo,
	&Array_InternalArray__IEnumerable_GetEnumerator_TisObject_t_m31450_MethodInfo,
};
extern TypeInfo ICollection_1_t8251_il2cpp_TypeInfo;
extern TypeInfo IList_1_t8252_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8253_il2cpp_TypeInfo;
static TypeInfo* TouchScreenKeyboardTypeU5BU5D_t5430_InterfacesTypeInfos[] = 
{
	&ICollection_1_t8251_il2cpp_TypeInfo,
	&IList_1_t8252_il2cpp_TypeInfo,
	&IEnumerable_1_t8253_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IList_1_t7232_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IList_1_t7235_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IList_1_t7238_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IList_1_t7241_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IList_1_t7244_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IList_1_t2851_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair TouchScreenKeyboardTypeU5BU5D_t5430_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IList_t1435_il2cpp_TypeInfo, 9},
	{ &ICollection_1_t8251_il2cpp_TypeInfo, 21},
	{ &IList_1_t8252_il2cpp_TypeInfo, 28},
	{ &IEnumerable_1_t8253_il2cpp_TypeInfo, 33},
	{ &ICollection_1_t7231_il2cpp_TypeInfo, 34},
	{ &IList_1_t7232_il2cpp_TypeInfo, 41},
	{ &IEnumerable_1_t7233_il2cpp_TypeInfo, 46},
	{ &ICollection_1_t7234_il2cpp_TypeInfo, 47},
	{ &IList_1_t7235_il2cpp_TypeInfo, 54},
	{ &IEnumerable_1_t7236_il2cpp_TypeInfo, 59},
	{ &ICollection_1_t7237_il2cpp_TypeInfo, 60},
	{ &IList_1_t7238_il2cpp_TypeInfo, 67},
	{ &IEnumerable_1_t7239_il2cpp_TypeInfo, 72},
	{ &ICollection_1_t7240_il2cpp_TypeInfo, 73},
	{ &IList_1_t7241_il2cpp_TypeInfo, 80},
	{ &IEnumerable_1_t7242_il2cpp_TypeInfo, 85},
	{ &ICollection_1_t7243_il2cpp_TypeInfo, 86},
	{ &IList_1_t7244_il2cpp_TypeInfo, 93},
	{ &IEnumerable_1_t7245_il2cpp_TypeInfo, 98},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 99},
	{ &IList_1_t2851_il2cpp_TypeInfo, 106},
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 111},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TouchScreenKeyboardTypeU5BU5D_t5430_0_0_0;
extern Il2CppType TouchScreenKeyboardTypeU5BU5D_t5430_1_0_0;
// UnityEngine.TouchScreenKeyboardType
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
extern TypeInfo TouchScreenKeyboardType_t491_il2cpp_TypeInfo;
TypeInfo TouchScreenKeyboardTypeU5BU5D_t5430_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TouchScreenKeyboardType[]"/* name */
	, "UnityEngine"/* namespaze */
	, TouchScreenKeyboardTypeU5BU5D_t5430_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Array_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TouchScreenKeyboardType_t491_il2cpp_TypeInfo/* element_class */
	, TouchScreenKeyboardTypeU5BU5D_t5430_InterfacesTypeInfos/* implemented_interfaces */
	, TouchScreenKeyboardTypeU5BU5D_t5430_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &TouchScreenKeyboardTypeU5BU5D_t5430_0_0_0/* byval_arg */
	, &TouchScreenKeyboardTypeU5BU5D_t5430_1_0_0/* this_arg */
	, TouchScreenKeyboardTypeU5BU5D_t5430_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Array_t)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 1/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, true/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 112/* vtable_count */
	, 21/* interfaces_count */
	, 25/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
