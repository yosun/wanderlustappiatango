﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.PropAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_102.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropAbstractBehaviour>
struct CachedInvokableCall_1_t4156  : public InvokableCall_1_t4157
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
