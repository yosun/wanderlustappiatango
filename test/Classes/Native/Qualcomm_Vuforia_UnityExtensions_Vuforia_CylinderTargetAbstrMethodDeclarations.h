﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t34;
// Vuforia.CylinderTarget
struct CylinderTarget_t571;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t553;
// UnityEngine.Transform
struct Transform_t10;
// UnityEngine.GameObject
struct GameObject_t2;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// Vuforia.CylinderTarget Vuforia.CylinderTargetAbstractBehaviour::get_CylinderTarget()
 Object_t * CylinderTargetAbstractBehaviour_get_CylinderTarget_m2772 (CylinderTargetAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.CylinderTargetAbstractBehaviour::get_SideLength()
 float CylinderTargetAbstractBehaviour_get_SideLength_m2773 (CylinderTargetAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.CylinderTargetAbstractBehaviour::get_TopDiameter()
 float CylinderTargetAbstractBehaviour_get_TopDiameter_m2774 (CylinderTargetAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.CylinderTargetAbstractBehaviour::get_BottomDiameter()
 float CylinderTargetAbstractBehaviour_get_BottomDiameter_m2775 (CylinderTargetAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetSideLength(System.Single)
 bool CylinderTargetAbstractBehaviour_SetSideLength_m2776 (CylinderTargetAbstractBehaviour_t34 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetTopDiameter(System.Single)
 bool CylinderTargetAbstractBehaviour_SetTopDiameter_m2777 (CylinderTargetAbstractBehaviour_t34 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetBottomDiameter(System.Single)
 bool CylinderTargetAbstractBehaviour_SetBottomDiameter_m2778 (CylinderTargetAbstractBehaviour_t34 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::OnFrameIndexUpdate(System.Int32)
 void CylinderTargetAbstractBehaviour_OnFrameIndexUpdate_m362 (CylinderTargetAbstractBehaviour_t34 * __this, int32_t ___newFrameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::CorrectScaleImpl()
 bool CylinderTargetAbstractBehaviour_CorrectScaleImpl_m364 (CylinderTargetAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::InternalUnregisterTrackable()
 void CylinderTargetAbstractBehaviour_InternalUnregisterTrackable_m363 (CylinderTargetAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
 void CylinderTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m390 (CylinderTargetAbstractBehaviour_t34 * __this, Vector3_t13 * ___boundsMin, Vector3_t13 * ___boundsMax, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget)
 void CylinderTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m391 (CylinderTargetAbstractBehaviour_t34 * __this, Object_t * ___reconstructionFromTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.CylinderTargetAbstractBehaviour::GetScale()
 float CylinderTargetAbstractBehaviour_GetScale_m2779 (CylinderTargetAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::SetScale(System.Single)
 bool CylinderTargetAbstractBehaviour_SetScale_m2780 (CylinderTargetAbstractBehaviour_t34 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::ApplyScale(System.Single)
 void CylinderTargetAbstractBehaviour_ApplyScale_m2781 (CylinderTargetAbstractBehaviour_t34 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorCylinderTargetBehaviour.InitializeCylinderTarget(Vuforia.CylinderTarget)
 void CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_InitializeCylinderTarget_m392 (CylinderTargetAbstractBehaviour_t34 * __this, Object_t * ___cylinderTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorCylinderTargetBehaviour.SetAspectRatio(System.Single,System.Single)
 void CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_SetAspectRatio_m393 (CylinderTargetAbstractBehaviour_t34 * __this, float ___topRatio, float ___bottomRatio, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::.ctor()
 void CylinderTargetAbstractBehaviour__ctor_m344 (CylinderTargetAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m356 (CylinderTargetAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m357 (CylinderTargetAbstractBehaviour_t34 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t10 * CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m358 (CylinderTargetAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.CylinderTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t2 * CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m359 (CylinderTargetAbstractBehaviour_t34 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
