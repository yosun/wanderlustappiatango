﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t911;
// UnityEngine.Object
struct Object_t117;
struct Object_t117_marshaled;
// UnityEngine.Object[]
struct ObjectU5BU5D_t816;

// System.Void UnityEngine.AssetBundleRequest::.ctor()
 void AssetBundleRequest__ctor_m5424 (AssetBundleRequest_t911 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
 Object_t117 * AssetBundleRequest_get_asset_m5425 (AssetBundleRequest_t911 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
 ObjectU5BU5D_t816* AssetBundleRequest_get_allAssets_m5426 (AssetBundleRequest_t911 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
