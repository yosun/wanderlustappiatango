﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// System.Object
struct Object_t;
// System.EventArgs
struct EventArgs_t1567;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
struct KeyGeneratedEventHandler_t1568  : public MulticastDelegate_t325
{
};
