﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// UnityEngine.EventSystems.IPointerUpHandler
struct IPointerUpHandler_t225;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t184;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_t204  : public MulticastDelegate_t325
{
};
