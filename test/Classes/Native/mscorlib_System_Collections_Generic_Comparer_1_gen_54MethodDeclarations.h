﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.Events.BaseInvokableCall>
struct Comparer_1_t4821;
// System.Object
struct Object_t;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t1075;

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Events.BaseInvokableCall>::.ctor()
// System.Collections.Generic.Comparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_genMethodDeclarations.h"
#define Comparer_1__ctor_m29634(__this, method) (void)Comparer_1__ctor_m14754_gshared((Comparer_1_t2861 *)__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.Events.BaseInvokableCall>::.cctor()
#define Comparer_1__cctor_m29635(__this/* static, unused */, method) (void)Comparer_1__cctor_m14755_gshared((Object_t *)__this/* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IComparer.Compare(System.Object,System.Object)
#define Comparer_1_System_Collections_IComparer_Compare_m29636(__this, ___x, ___y, method) (int32_t)Comparer_1_System_Collections_IComparer_Compare_m14756_gshared((Comparer_1_t2861 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.Events.BaseInvokableCall>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Events.BaseInvokableCall>::get_Default()
#define Comparer_1_get_Default_m29637(__this/* static, unused */, method) (Comparer_1_t4821 *)Comparer_1_get_Default_m14757_gshared((Object_t *)__this/* static, unused */, method)
