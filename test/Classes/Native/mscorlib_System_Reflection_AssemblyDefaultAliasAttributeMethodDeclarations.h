﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyDefaultAliasAttribute
struct AssemblyDefaultAliasAttribute_t1282;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyDefaultAliasAttribute::.ctor(System.String)
 void AssemblyDefaultAliasAttribute__ctor_m6712 (AssemblyDefaultAliasAttribute_t1282 * __this, String_t* ___defaultAlias, MethodInfo* method) IL2CPP_METHOD_ATTR;
