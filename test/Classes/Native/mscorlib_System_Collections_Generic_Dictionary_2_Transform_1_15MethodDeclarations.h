﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>,UnityEngine.Font>
struct Transform_1_t3314;
// System.Object
struct Object_t;
// UnityEngine.Font
struct Font_t280;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t462;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>,UnityEngine.Font>::.ctor(System.Object,System.IntPtr)
// System.Collections.Generic.Dictionary`2/Transform`1<System.Object,System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_12MethodDeclarations.h"
#define Transform_1__ctor_m17936(__this, ___object, ___method, method) (void)Transform_1__ctor_m17683_gshared((Transform_1_t3286 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>,UnityEngine.Font>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m17937(__this, ___key, ___value, method) (Font_t280 *)Transform_1_Invoke_m17684_gshared((Transform_1_t3286 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>,UnityEngine.Font>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m17938(__this, ___key, ___value, ___callback, ___object, method) (Object_t *)Transform_1_BeginInvoke_m17685_gshared((Transform_1_t3286 *)__this, (Object_t *)___key, (Object_t *)___value, (AsyncCallback_t200 *)___callback, (Object_t *)___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>,UnityEngine.Font>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m17939(__this, ___result, method) (Font_t280 *)Transform_1_EndInvoke_m17686_gshared((Transform_1_t3286 *)__this, (Object_t *)___result, method)
