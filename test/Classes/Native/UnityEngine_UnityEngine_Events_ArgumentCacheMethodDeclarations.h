﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.ArgumentCache
struct ArgumentCache_t1074;
// UnityEngine.Object
struct Object_t117;
struct Object_t117_marshaled;
// System.String
struct String_t;

// System.Void UnityEngine.Events.ArgumentCache::.ctor()
 void ArgumentCache__ctor_m6350 (ArgumentCache_t1074 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
 Object_t117 * ArgumentCache_get_unityObjectArgument_m6351 (ArgumentCache_t1074 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
 String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352 (ArgumentCache_t1074 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
 int32_t ArgumentCache_get_intArgument_m6353 (ArgumentCache_t1074 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
 float ArgumentCache_get_floatArgument_m6354 (ArgumentCache_t1074 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
 String_t* ArgumentCache_get_stringArgument_m6355 (ArgumentCache_t1074 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
 bool ArgumentCache_get_boolArgument_m6356 (ArgumentCache_t1074 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
