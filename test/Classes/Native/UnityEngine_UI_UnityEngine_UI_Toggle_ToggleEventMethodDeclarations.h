﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Toggle/ToggleEvent
struct ToggleEvent_t367;

// System.Void UnityEngine.UI.Toggle/ToggleEvent::.ctor()
 void ToggleEvent__ctor_m1599 (ToggleEvent_t367 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
