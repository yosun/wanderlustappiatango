﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForSeconds
struct WaitForSeconds_t483;
struct WaitForSeconds_t483_marshaled;

// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
 void WaitForSeconds__ctor_m2250 (WaitForSeconds_t483 * __this, float ___seconds, MethodInfo* method) IL2CPP_METHOD_ATTR;
void WaitForSeconds_t483_marshal(const WaitForSeconds_t483& unmarshaled, WaitForSeconds_t483_marshaled& marshaled);
void WaitForSeconds_t483_marshal_back(const WaitForSeconds_t483_marshaled& marshaled, WaitForSeconds_t483& unmarshaled);
void WaitForSeconds_t483_marshal_cleanup(WaitForSeconds_t483_marshaled& marshaled);
