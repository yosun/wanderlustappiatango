﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t295;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct UnityAction_1_t292  : public MulticastDelegate_t325
{
};
