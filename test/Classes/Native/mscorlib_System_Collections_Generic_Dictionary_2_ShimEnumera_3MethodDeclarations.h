﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t3289;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3275;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m17714_gshared (ShimEnumerator_t3289 * __this, Dictionary_2_t3275 * ___host, MethodInfo* method);
#define ShimEnumerator__ctor_m17714(__this, ___host, method) (void)ShimEnumerator__ctor_m17714_gshared((ShimEnumerator_t3289 *)__this, (Dictionary_2_t3275 *)___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
 bool ShimEnumerator_MoveNext_m17715_gshared (ShimEnumerator_t3289 * __this, MethodInfo* method);
#define ShimEnumerator_MoveNext_m17715(__this, method) (bool)ShimEnumerator_MoveNext_m17715_gshared((ShimEnumerator_t3289 *)__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
 DictionaryEntry_t1302  ShimEnumerator_get_Entry_m17716_gshared (ShimEnumerator_t3289 * __this, MethodInfo* method);
#define ShimEnumerator_get_Entry_m17716(__this, method) (DictionaryEntry_t1302 )ShimEnumerator_get_Entry_m17716_gshared((ShimEnumerator_t3289 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
 Object_t * ShimEnumerator_get_Key_m17717_gshared (ShimEnumerator_t3289 * __this, MethodInfo* method);
#define ShimEnumerator_get_Key_m17717(__this, method) (Object_t *)ShimEnumerator_get_Key_m17717_gshared((ShimEnumerator_t3289 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
 Object_t * ShimEnumerator_get_Value_m17718_gshared (ShimEnumerator_t3289 * __this, MethodInfo* method);
#define ShimEnumerator_get_Value_m17718(__this, method) (Object_t *)ShimEnumerator_get_Value_m17718_gshared((ShimEnumerator_t3289 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
 Object_t * ShimEnumerator_get_Current_m17719_gshared (ShimEnumerator_t3289 * __this, MethodInfo* method);
#define ShimEnumerator_get_Current_m17719(__this, method) (Object_t *)ShimEnumerator_get_Current_m17719_gshared((ShimEnumerator_t3289 *)__this, method)
