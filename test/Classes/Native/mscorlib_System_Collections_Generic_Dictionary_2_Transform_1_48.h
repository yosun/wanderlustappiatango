﻿#pragma once
#include <stdint.h>
// Vuforia.WordResult
struct WordResult_t702;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo Int32_t93_il2cpp_TypeInfo;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordResult,Vuforia.WordResult>
struct Transform_1_t3971  : public MulticastDelegate_t325
{
};
