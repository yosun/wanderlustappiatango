﻿#pragma once
#include <stdint.h>
// Vuforia.Prop
struct Prop_t42;
// UnityEngine.BoxCollider
struct BoxCollider_t723;
// Vuforia.SmartTerrainTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab.h"
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t69  : public SmartTerrainTrackableBehaviour_t583
{
	// Vuforia.Prop Vuforia.PropAbstractBehaviour::mProp
	Object_t * ___mProp_13;
	// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::mBoxColliderToUpdate
	BoxCollider_t723 * ___mBoxColliderToUpdate_14;
};
