﻿#pragma once
#include <stdint.h>
// System.Reflection.MethodInfo
struct MethodInfo_t142;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.Reflection.MethodInfo>
struct Predicate_1_t2843  : public MulticastDelegate_t325
{
};
