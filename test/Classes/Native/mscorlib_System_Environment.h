﻿#pragma once
#include <stdint.h>
// System.OperatingSystem
struct OperatingSystem_t2215;
// System.Object
#include "mscorlib_System_Object.h"
// System.Environment
struct Environment_t2216  : public Object_t
{
};
struct Environment_t2216_StaticFields{
	// System.OperatingSystem System.Environment::os
	OperatingSystem_t2215 * ___os_0;
};
