﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CloudRecoAbstractBehaviour
struct CloudRecoAbstractBehaviour_t32;
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t563;

// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoEnabled()
 bool CloudRecoAbstractBehaviour_get_CloudRecoEnabled_m2652 (CloudRecoAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::set_CloudRecoEnabled(System.Boolean)
 void CloudRecoAbstractBehaviour_set_CloudRecoEnabled_m2653 (CloudRecoAbstractBehaviour_t32 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::get_CloudRecoInitialized()
 bool CloudRecoAbstractBehaviour_get_CloudRecoInitialized_m2654 (CloudRecoAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Initialize()
 void CloudRecoAbstractBehaviour_Initialize_m2655 (CloudRecoAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Deinitialize()
 void CloudRecoAbstractBehaviour_Deinitialize_m2656 (CloudRecoAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::CheckInitialization()
 void CloudRecoAbstractBehaviour_CheckInitialization_m2657 (CloudRecoAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::StartCloudReco()
 void CloudRecoAbstractBehaviour_StartCloudReco_m2658 (CloudRecoAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::StopCloudReco()
 void CloudRecoAbstractBehaviour_StopCloudReco_m2659 (CloudRecoAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::RegisterEventHandler(Vuforia.ICloudRecoEventHandler)
 void CloudRecoAbstractBehaviour_RegisterEventHandler_m2660 (CloudRecoAbstractBehaviour_t32 * __this, Object_t * ___eventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoAbstractBehaviour::UnregisterEventHandler(Vuforia.ICloudRecoEventHandler)
 bool CloudRecoAbstractBehaviour_UnregisterEventHandler_m2661 (CloudRecoAbstractBehaviour_t32 * __this, Object_t * ___eventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnEnable()
 void CloudRecoAbstractBehaviour_OnEnable_m2662 (CloudRecoAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDisable()
 void CloudRecoAbstractBehaviour_OnDisable_m2663 (CloudRecoAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Start()
 void CloudRecoAbstractBehaviour_Start_m2664 (CloudRecoAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::Update()
 void CloudRecoAbstractBehaviour_Update_m2665 (CloudRecoAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnDestroy()
 void CloudRecoAbstractBehaviour_OnDestroy_m2666 (CloudRecoAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::OnQCARStarted()
 void CloudRecoAbstractBehaviour_OnQCARStarted_m2667 (CloudRecoAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoAbstractBehaviour::.ctor()
 void CloudRecoAbstractBehaviour__ctor_m343 (CloudRecoAbstractBehaviour_t32 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
