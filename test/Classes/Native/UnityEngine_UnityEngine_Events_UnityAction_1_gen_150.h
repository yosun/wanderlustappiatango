﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// UnityEngine.Collider2D
struct Collider2D_t448;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Collider2D>
struct UnityAction_1_t4661  : public MulticastDelegate_t325
{
};
