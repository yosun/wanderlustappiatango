﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordResult,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>>
struct Transform_1_t3972;
// System.Object
struct Object_t;
// Vuforia.WordResult
struct WordResult_t702;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordResult,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m23069 (Transform_1_t3972 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordResult,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>>::Invoke(TKey,TValue)
 KeyValuePair_2_t3963  Transform_1_Invoke_m23070 (Transform_1_t3972 * __this, int32_t ___key, WordResult_t702 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordResult,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m23071 (Transform_1_t3972 * __this, int32_t ___key, WordResult_t702 * ___value, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordResult,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>>::EndInvoke(System.IAsyncResult)
 KeyValuePair_2_t3963  Transform_1_EndInvoke_m23072 (Transform_1_t3972 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
