﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.BoxCollider>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_143.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider>
struct CachedInvokableCall_1_t4633  : public InvokableCall_1_t4634
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.BoxCollider>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
