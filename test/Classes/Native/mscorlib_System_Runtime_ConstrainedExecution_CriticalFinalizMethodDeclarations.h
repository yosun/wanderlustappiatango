﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct CriticalFinalizerObject_t1963;

// System.Void System.Runtime.ConstrainedExecution.CriticalFinalizerObject::.ctor()
 void CriticalFinalizerObject__ctor_m11403 (CriticalFinalizerObject_t1963 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.ConstrainedExecution.CriticalFinalizerObject::Finalize()
 void CriticalFinalizerObject_Finalize_m11404 (CriticalFinalizerObject_t1963 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
