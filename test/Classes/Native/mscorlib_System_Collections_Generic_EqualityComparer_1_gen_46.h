﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.SurfaceAbstractBehaviour>
struct EqualityComparer_1_t4106;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.SurfaceAbstractBehaviour>
struct EqualityComparer_1_t4106  : public Object_t
{
};
struct EqualityComparer_1_t4106_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.SurfaceAbstractBehaviour>::_default
	EqualityComparer_1_t4106 * ____default_0;
};
