﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo;
// <Module>
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// Metadata Definition <Module>
static MethodInfo* U3CModuleU3E_t0_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType U3CModuleU3E_t0_0_0_0;
extern Il2CppType U3CModuleU3E_t0_1_0_0;
struct U3CModuleU3E_t0;
TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "<Module>"/* name */
	, ""/* namespaze */
	, U3CModuleU3E_t0_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &U3CModuleU3E_t0_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &U3CModuleU3E_t0_il2cpp_TypeInfo/* cast_class */
	, &U3CModuleU3E_t0_0_0_0/* byval_arg */
	, &U3CModuleU3E_t0_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CModuleU3E_t0)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentMode.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TheCurrentMode_t1_il2cpp_TypeInfo;
// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentModeMethodDeclarations.h"



// Metadata Definition ARVRModes/TheCurrentMode
extern Il2CppType Int32_t93_0_0_1542;
FieldInfo TheCurrentMode_t1____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t93_0_0_1542/* type */
	, &TheCurrentMode_t1_il2cpp_TypeInfo/* parent */
	, offsetof(TheCurrentMode_t1, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TheCurrentMode_t1_0_0_32854;
FieldInfo TheCurrentMode_t1____AR_2_FieldInfo = 
{
	"AR"/* name */
	, &TheCurrentMode_t1_0_0_32854/* type */
	, &TheCurrentMode_t1_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TheCurrentMode_t1_0_0_32854;
FieldInfo TheCurrentMode_t1____VR_3_FieldInfo = 
{
	"VR"/* name */
	, &TheCurrentMode_t1_0_0_32854/* type */
	, &TheCurrentMode_t1_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TheCurrentMode_t1_FieldInfos[] =
{
	&TheCurrentMode_t1____value___1_FieldInfo,
	&TheCurrentMode_t1____AR_2_FieldInfo,
	&TheCurrentMode_t1____VR_3_FieldInfo,
	NULL
};
static const int32_t TheCurrentMode_t1____AR_2_DefaultValueData = 0;
extern Il2CppType Int32_t93_0_0_0;
static Il2CppFieldDefaultValueEntry TheCurrentMode_t1____AR_2_DefaultValue = 
{
	&TheCurrentMode_t1____AR_2_FieldInfo/* field */
	, { (char*)&TheCurrentMode_t1____AR_2_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TheCurrentMode_t1____VR_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TheCurrentMode_t1____VR_3_DefaultValue = 
{
	&TheCurrentMode_t1____VR_3_FieldInfo/* field */
	, { (char*)&TheCurrentMode_t1____VR_3_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TheCurrentMode_t1_FieldDefaultValues[] = 
{
	&TheCurrentMode_t1____AR_2_DefaultValue,
	&TheCurrentMode_t1____VR_3_DefaultValue,
	NULL
};
static MethodInfo* TheCurrentMode_t1_MethodInfos[] =
{
	NULL
};
extern MethodInfo Enum_Equals_m191_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo Enum_GetHashCode_m193_MethodInfo;
extern MethodInfo Enum_ToString_m194_MethodInfo;
extern MethodInfo Enum_ToString_m195_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToBoolean_m196_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToByte_m197_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToChar_m198_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDateTime_m199_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDecimal_m200_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDouble_m201_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt16_m202_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt32_m203_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt64_m204_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSByte_m205_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSingle_m206_MethodInfo;
extern MethodInfo Enum_ToString_m207_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToType_m208_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt16_m209_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt32_m210_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt64_m211_MethodInfo;
extern MethodInfo Enum_CompareTo_m212_MethodInfo;
extern MethodInfo Enum_GetTypeCode_m213_MethodInfo;
static MethodInfo* TheCurrentMode_t1_VTable[] =
{
	&Enum_Equals_m191_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Enum_GetHashCode_m193_MethodInfo,
	&Enum_ToString_m194_MethodInfo,
	&Enum_ToString_m195_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m196_MethodInfo,
	&Enum_System_IConvertible_ToByte_m197_MethodInfo,
	&Enum_System_IConvertible_ToChar_m198_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m199_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m200_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m201_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m202_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m203_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m204_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m205_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m206_MethodInfo,
	&Enum_ToString_m207_MethodInfo,
	&Enum_System_IConvertible_ToType_m208_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m209_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m210_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m211_MethodInfo,
	&Enum_CompareTo_m212_MethodInfo,
	&Enum_GetTypeCode_m213_MethodInfo,
};
extern TypeInfo IFormattable_t94_il2cpp_TypeInfo;
extern TypeInfo IConvertible_t95_il2cpp_TypeInfo;
extern TypeInfo IComparable_t96_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair TheCurrentMode_t1_InterfacesOffsets[] = 
{
	{ &IFormattable_t94_il2cpp_TypeInfo, 4},
	{ &IConvertible_t95_il2cpp_TypeInfo, 5},
	{ &IComparable_t96_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType TheCurrentMode_t1_0_0_0;
extern Il2CppType TheCurrentMode_t1_1_0_0;
extern TypeInfo Enum_t97_il2cpp_TypeInfo;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t93_il2cpp_TypeInfo;
extern TypeInfo ARVRModes_t5_il2cpp_TypeInfo;
TypeInfo TheCurrentMode_t1_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TheCurrentMode"/* name */
	, ""/* namespaze */
	, TheCurrentMode_t1_MethodInfos/* methods */
	, NULL/* properties */
	, TheCurrentMode_t1_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t97_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &ARVRModes_t5_il2cpp_TypeInfo/* nested_in */
	, &Int32_t93_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TheCurrentMode_t1_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &TheCurrentMode_t1_0_0_0/* byval_arg */
	, &TheCurrentMode_t1_1_0_0/* this_arg */
	, TheCurrentMode_t1_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TheCurrentMode_t1_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TheCurrentMode_t1)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// ARVRModes
#include "AssemblyU2DCSharp_ARVRModes.h"
#ifndef _MSC_VER
#else
#endif
// ARVRModes
#include "AssemblyU2DCSharp_ARVRModesMethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// System.Single
#include "mscorlib_System_Single.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// System.String
#include "mscorlib_System_String.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// Mathf2
#include "AssemblyU2DCSharp_Mathf2.h"
// System.Object
#include "mscorlib_System_Object.h"
extern TypeInfo Color_t19_il2cpp_TypeInfo;
extern TypeInfo Input2_t14_il2cpp_TypeInfo;
extern TypeInfo Input_t98_il2cpp_TypeInfo;
extern TypeInfo Mathf2_t15_il2cpp_TypeInfo;
extern TypeInfo String_t_il2cpp_TypeInfo;
extern TypeInfo Vector2_t9_il2cpp_TypeInfo;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// Input2
#include "AssemblyU2DCSharp_Input2MethodDeclarations.h"
// UnityEngine.Input
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// Mathf2
#include "AssemblyU2DCSharp_Mathf2MethodDeclarations.h"
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
extern MethodInfo MonoBehaviour__ctor_m214_MethodInfo;
extern MethodInfo GameObject_SetActive_m215_MethodInfo;
extern MethodInfo Behaviour_set_enabled_m216_MethodInfo;
extern MethodInfo Color__ctor_m217_MethodInfo;
extern MethodInfo Material_set_color_m218_MethodInfo;
extern MethodInfo Component_get_transform_m219_MethodInfo;
extern MethodInfo Transform_get_parent_m220_MethodInfo;
extern MethodInfo ARVRModes_SwitchARToVR_m1_MethodInfo;
extern MethodInfo ARVRModes_SwitchVRToAR_m2_MethodInfo;
extern MethodInfo Input2_HasPointStarted_m19_MethodInfo;
extern MethodInfo Input_get_mousePosition_m221_MethodInfo;
extern MethodInfo Vector2_op_Implicit_m222_MethodInfo;
extern MethodInfo Mathf2_WhatDidWeHit_m31_MethodInfo;
extern MethodInfo RaycastHit_get_point_m223_MethodInfo;
extern MethodInfo Vector3_op_Inequality_m224_MethodInfo;
extern MethodInfo RaycastHit_get_transform_m225_MethodInfo;
extern MethodInfo Object_get_name_m226_MethodInfo;
extern MethodInfo String_op_Equality_m227_MethodInfo;
extern MethodInfo ARVRModes_SwitchModes_m3_MethodInfo;
extern MethodInfo Input2_Swipe_m25_MethodInfo;
extern MethodInfo MonoBehaviour_print_m228_MethodInfo;


// System.Void ARVRModes::.ctor()
extern MethodInfo ARVRModes__ctor_m0_MethodInfo;
 void ARVRModes__ctor_m0 (ARVRModes_t5 * __this, MethodInfo* method){
	{
		__this->___tcm_8 = 1;
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void ARVRModes::SwitchARToVR()
 void ARVRModes_SwitchARToVR_m1 (ARVRModes_t5 * __this, MethodInfo* method){
	Transform_t10 * V_0 = {0};
	{
		GameObject_t2 * L_0 = (__this->___goMaskThese_2);
		NullCheck(L_0);
		GameObject_SetActive_m215(L_0, 1, /*hidden argument*/&GameObject_SetActive_m215_MethodInfo);
		GameObject_t2 * L_1 = (__this->___goARModeOnlyStuff_3);
		NullCheck(L_1);
		GameObject_SetActive_m215(L_1, 0, /*hidden argument*/&GameObject_SetActive_m215_MethodInfo);
		GameObject_t2 * L_2 = (__this->___goVRSet_6);
		NullCheck(L_2);
		GameObject_SetActive_m215(L_2, 1, /*hidden argument*/&GameObject_SetActive_m215_MethodInfo);
		Camera_t3 * L_3 = (__this->___camVR_5);
		NullCheck(L_3);
		Behaviour_set_enabled_m216(L_3, 1, /*hidden argument*/&Behaviour_set_enabled_m216_MethodInfo);
		Camera_t3 * L_4 = (__this->___camAR_4);
		NullCheck(L_4);
		Behaviour_set_enabled_m216(L_4, 0, /*hidden argument*/&Behaviour_set_enabled_m216_MethodInfo);
		Material_t4 * L_5 = (__this->___matTouchMeLipstick_7);
		Color_t19  L_6 = {0};
		Color__ctor_m217(&L_6, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/&Color__ctor_m217_MethodInfo);
		NullCheck(L_5);
		Material_set_color_m218(L_5, L_6, /*hidden argument*/&Material_set_color_m218_MethodInfo);
		Camera_t3 * L_7 = (__this->___camVR_5);
		NullCheck(L_7);
		Transform_t10 * L_8 = Component_get_transform_m219(L_7, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_8);
		Transform_t10 * L_9 = Transform_get_parent_m220(L_8, /*hidden argument*/&Transform_get_parent_m220_MethodInfo);
		V_0 = L_9;
		return;
	}
}
// System.Void ARVRModes::SwitchVRToAR()
 void ARVRModes_SwitchVRToAR_m2 (ARVRModes_t5 * __this, MethodInfo* method){
	{
		GameObject_t2 * L_0 = (__this->___goMaskThese_2);
		NullCheck(L_0);
		GameObject_SetActive_m215(L_0, 0, /*hidden argument*/&GameObject_SetActive_m215_MethodInfo);
		GameObject_t2 * L_1 = (__this->___goARModeOnlyStuff_3);
		NullCheck(L_1);
		GameObject_SetActive_m215(L_1, 1, /*hidden argument*/&GameObject_SetActive_m215_MethodInfo);
		GameObject_t2 * L_2 = (__this->___goVRSet_6);
		NullCheck(L_2);
		GameObject_SetActive_m215(L_2, 0, /*hidden argument*/&GameObject_SetActive_m215_MethodInfo);
		Camera_t3 * L_3 = (__this->___camVR_5);
		NullCheck(L_3);
		Behaviour_set_enabled_m216(L_3, 0, /*hidden argument*/&Behaviour_set_enabled_m216_MethodInfo);
		Camera_t3 * L_4 = (__this->___camAR_4);
		NullCheck(L_4);
		Behaviour_set_enabled_m216(L_4, 1, /*hidden argument*/&Behaviour_set_enabled_m216_MethodInfo);
		Material_t4 * L_5 = (__this->___matTouchMeLipstick_7);
		Color_t19  L_6 = {0};
		Color__ctor_m217(&L_6, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/&Color__ctor_m217_MethodInfo);
		NullCheck(L_5);
		Material_set_color_m218(L_5, L_6, /*hidden argument*/&Material_set_color_m218_MethodInfo);
		return;
	}
}
// System.Void ARVRModes::SwitchModes()
 void ARVRModes_SwitchModes_m3 (ARVRModes_t5 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___tcm_8);
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		__this->___tcm_8 = 1;
		ARVRModes_SwitchARToVR_m1(__this, /*hidden argument*/&ARVRModes_SwitchARToVR_m1_MethodInfo);
		goto IL_0036;
	}

IL_001d:
	{
		int32_t L_1 = (__this->___tcm_8);
		if ((((uint32_t)L_1) != ((uint32_t)1)))
		{
			goto IL_0036;
		}
	}
	{
		__this->___tcm_8 = 0;
		ARVRModes_SwitchVRToAR_m2(__this, /*hidden argument*/&ARVRModes_SwitchVRToAR_m2_MethodInfo);
	}

IL_0036:
	{
		return;
	}
}
// System.Void ARVRModes::Update()
extern MethodInfo ARVRModes_Update_m4_MethodInfo;
 void ARVRModes_Update_m4 (ARVRModes_t5 * __this, MethodInfo* method){
	RaycastHit_t16  V_0 = {0};
	String_t* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo));
		bool L_0 = Input2_HasPointStarted_m19(NULL /*static, unused*/, 1, /*hidden argument*/&Input2_HasPointStarted_m19_MethodInfo);
		if (!L_0)
		{
			goto IL_0054;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Vector3_t13  L_1 = Input_get_mousePosition_m221(NULL /*static, unused*/, /*hidden argument*/&Input_get_mousePosition_m221_MethodInfo);
		Vector2_t9  L_2 = Vector2_op_Implicit_m222(NULL /*static, unused*/, L_1, /*hidden argument*/&Vector2_op_Implicit_m222_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo));
		RaycastHit_t16  L_3 = Mathf2_WhatDidWeHit_m31(NULL /*static, unused*/, L_2, /*hidden argument*/&Mathf2_WhatDidWeHit_m31_MethodInfo);
		V_0 = L_3;
		Vector3_t13  L_4 = RaycastHit_get_point_m223((&V_0), /*hidden argument*/&RaycastHit_get_point_m223_MethodInfo);
		bool L_5 = Vector3_op_Inequality_m224(NULL /*static, unused*/, L_4, (((Mathf2_t15_StaticFields*)InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo)->static_fields)->___FarFarAway_0), /*hidden argument*/&Vector3_op_Inequality_m224_MethodInfo);
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		Transform_t10 * L_6 = RaycastHit_get_transform_m225((&V_0), /*hidden argument*/&RaycastHit_get_transform_m225_MethodInfo);
		NullCheck(L_6);
		String_t* L_7 = Object_get_name_m226(L_6, /*hidden argument*/&Object_get_name_m226_MethodInfo);
		V_1 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_8 = String_op_Equality_m227(NULL /*static, unused*/, V_1, (String_t*) &_stringLiteral1, /*hidden argument*/&String_op_Equality_m227_MethodInfo);
		if (!L_8)
		{
			goto IL_0054;
		}
	}
	{
		ARVRModes_SwitchModes_m3(__this, /*hidden argument*/&ARVRModes_SwitchModes_m3_MethodInfo);
	}

IL_0054:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo));
		Vector2_t9  L_9 = Input2_Swipe_m25(NULL /*static, unused*/, /*hidden argument*/&Input2_Swipe_m25_MethodInfo);
		Vector2_t9  L_10 = L_9;
		Object_t * L_11 = Box(InitializedTypeInfo(&Vector2_t9_il2cpp_TypeInfo), &L_10);
		MonoBehaviour_print_m228(NULL /*static, unused*/, L_11, /*hidden argument*/&MonoBehaviour_print_m228_MethodInfo);
		return;
	}
}
// Metadata Definition ARVRModes
extern Il2CppType GameObject_t2_0_0_6;
FieldInfo ARVRModes_t5____goMaskThese_2_FieldInfo = 
{
	"goMaskThese"/* name */
	, &GameObject_t2_0_0_6/* type */
	, &ARVRModes_t5_il2cpp_TypeInfo/* parent */
	, offsetof(ARVRModes_t5, ___goMaskThese_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GameObject_t2_0_0_6;
FieldInfo ARVRModes_t5____goARModeOnlyStuff_3_FieldInfo = 
{
	"goARModeOnlyStuff"/* name */
	, &GameObject_t2_0_0_6/* type */
	, &ARVRModes_t5_il2cpp_TypeInfo/* parent */
	, offsetof(ARVRModes_t5, ___goARModeOnlyStuff_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Camera_t3_0_0_6;
FieldInfo ARVRModes_t5____camAR_4_FieldInfo = 
{
	"camAR"/* name */
	, &Camera_t3_0_0_6/* type */
	, &ARVRModes_t5_il2cpp_TypeInfo/* parent */
	, offsetof(ARVRModes_t5, ___camAR_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Camera_t3_0_0_6;
FieldInfo ARVRModes_t5____camVR_5_FieldInfo = 
{
	"camVR"/* name */
	, &Camera_t3_0_0_6/* type */
	, &ARVRModes_t5_il2cpp_TypeInfo/* parent */
	, offsetof(ARVRModes_t5, ___camVR_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GameObject_t2_0_0_6;
FieldInfo ARVRModes_t5____goVRSet_6_FieldInfo = 
{
	"goVRSet"/* name */
	, &GameObject_t2_0_0_6/* type */
	, &ARVRModes_t5_il2cpp_TypeInfo/* parent */
	, offsetof(ARVRModes_t5, ___goVRSet_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Material_t4_0_0_6;
FieldInfo ARVRModes_t5____matTouchMeLipstick_7_FieldInfo = 
{
	"matTouchMeLipstick"/* name */
	, &Material_t4_0_0_6/* type */
	, &ARVRModes_t5_il2cpp_TypeInfo/* parent */
	, offsetof(ARVRModes_t5, ___matTouchMeLipstick_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TheCurrentMode_t1_0_0_1;
FieldInfo ARVRModes_t5____tcm_8_FieldInfo = 
{
	"tcm"/* name */
	, &TheCurrentMode_t1_0_0_1/* type */
	, &ARVRModes_t5_il2cpp_TypeInfo/* parent */
	, offsetof(ARVRModes_t5, ___tcm_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* ARVRModes_t5_FieldInfos[] =
{
	&ARVRModes_t5____goMaskThese_2_FieldInfo,
	&ARVRModes_t5____goARModeOnlyStuff_3_FieldInfo,
	&ARVRModes_t5____camAR_4_FieldInfo,
	&ARVRModes_t5____camVR_5_FieldInfo,
	&ARVRModes_t5____goVRSet_6_FieldInfo,
	&ARVRModes_t5____matTouchMeLipstick_7_FieldInfo,
	&ARVRModes_t5____tcm_8_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::.ctor()
MethodInfo ARVRModes__ctor_m0_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ARVRModes__ctor_m0/* method */
	, &ARVRModes_t5_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::SwitchARToVR()
MethodInfo ARVRModes_SwitchARToVR_m1_MethodInfo = 
{
	"SwitchARToVR"/* name */
	, (methodPointerType)&ARVRModes_SwitchARToVR_m1/* method */
	, &ARVRModes_t5_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::SwitchVRToAR()
MethodInfo ARVRModes_SwitchVRToAR_m2_MethodInfo = 
{
	"SwitchVRToAR"/* name */
	, (methodPointerType)&ARVRModes_SwitchVRToAR_m2/* method */
	, &ARVRModes_t5_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 3/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::SwitchModes()
MethodInfo ARVRModes_SwitchModes_m3_MethodInfo = 
{
	"SwitchModes"/* name */
	, (methodPointerType)&ARVRModes_SwitchModes_m3/* method */
	, &ARVRModes_t5_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 4/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void ARVRModes::Update()
MethodInfo ARVRModes_Update_m4_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&ARVRModes_Update_m4/* method */
	, &ARVRModes_t5_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 5/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ARVRModes_t5_MethodInfos[] =
{
	&ARVRModes__ctor_m0_MethodInfo,
	&ARVRModes_SwitchARToVR_m1_MethodInfo,
	&ARVRModes_SwitchVRToAR_m2_MethodInfo,
	&ARVRModes_SwitchModes_m3_MethodInfo,
	&ARVRModes_Update_m4_MethodInfo,
	NULL
};
extern TypeInfo TheCurrentMode_t1_il2cpp_TypeInfo;
static TypeInfo* ARVRModes_t5_il2cpp_TypeInfo__nestedTypes[2] =
{
	&TheCurrentMode_t1_il2cpp_TypeInfo,
	NULL
};
extern MethodInfo Object_Equals_m229_MethodInfo;
extern MethodInfo Object_GetHashCode_m230_MethodInfo;
extern MethodInfo Object_ToString_m231_MethodInfo;
static MethodInfo* ARVRModes_t5_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType ARVRModes_t5_0_0_0;
extern Il2CppType ARVRModes_t5_1_0_0;
extern TypeInfo MonoBehaviour_t6_il2cpp_TypeInfo;
struct ARVRModes_t5;
TypeInfo ARVRModes_t5_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ARVRModes"/* name */
	, ""/* namespaze */
	, ARVRModes_t5_MethodInfos/* methods */
	, NULL/* properties */
	, ARVRModes_t5_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, ARVRModes_t5_il2cpp_TypeInfo__nestedTypes/* nested_types */
	, NULL/* nested_in */
	, &ARVRModes_t5_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ARVRModes_t5_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ARVRModes_t5_il2cpp_TypeInfo/* cast_class */
	, &ARVRModes_t5_0_0_0/* byval_arg */
	, &ARVRModes_t5_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ARVRModes_t5)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 0/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// AnimateTexture
#include "AssemblyU2DCSharp_AnimateTexture.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo AnimateTexture_t8_il2cpp_TypeInfo;
// AnimateTexture
#include "AssemblyU2DCSharp_AnimateTextureMethodDeclarations.h"

// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// UnityEngine.Time
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
extern MethodInfo Vector2_get_zero_m232_MethodInfo;
extern MethodInfo Vector2__ctor_m233_MethodInfo;
extern MethodInfo Component_GetComponent_TisRenderer_t7_m234_MethodInfo;
extern MethodInfo Time_get_deltaTime_m235_MethodInfo;
extern MethodInfo Vector2_op_Multiply_m236_MethodInfo;
extern MethodInfo Vector2_op_Addition_m237_MethodInfo;
extern MethodInfo Renderer_get_enabled_m238_MethodInfo;
extern MethodInfo Renderer_get_material_m239_MethodInfo;
extern MethodInfo Material_SetTextureOffset_m240_MethodInfo;
struct Component_t100;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
// UnityEngine.CastHelper`1<UnityEngine.Renderer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
struct Component_t100;
// UnityEngine.CastHelper`1<System.Object>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_0.h"
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
 Object_t * Component_GetComponent_TisObject_t_m241_gshared (Component_t100 * __this, MethodInfo* method);
#define Component_GetComponent_TisObject_t_m241(__this, method) (Object_t *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t7_m234(__this, method) (Renderer_t7 *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)


// System.Void AnimateTexture::.ctor()
extern MethodInfo AnimateTexture__ctor_m5_MethodInfo;
 void AnimateTexture__ctor_m5 (AnimateTexture_t8 * __this, MethodInfo* method){
	{
		Vector2_t9  L_0 = Vector2_get_zero_m232(NULL /*static, unused*/, /*hidden argument*/&Vector2_get_zero_m232_MethodInfo);
		__this->___uvOffset_2 = L_0;
		Vector2_t9  L_1 = {0};
		Vector2__ctor_m233(&L_1, (0.1f), (0.1f), /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		__this->___uvAnimationRate_3 = L_1;
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void AnimateTexture::Awake()
extern MethodInfo AnimateTexture_Awake_m6_MethodInfo;
 void AnimateTexture_Awake_m6 (AnimateTexture_t8 * __this, MethodInfo* method){
	{
		Renderer_t7 * L_0 = Component_GetComponent_TisRenderer_t7_m234(__this, /*hidden argument*/&Component_GetComponent_TisRenderer_t7_m234_MethodInfo);
		__this->___renderer_4 = L_0;
		return;
	}
}
// System.Void AnimateTexture::Update()
extern MethodInfo AnimateTexture_Update_m7_MethodInfo;
 void AnimateTexture_Update_m7 (AnimateTexture_t8 * __this, MethodInfo* method){
	{
		Vector2_t9  L_0 = (__this->___uvOffset_2);
		Vector2_t9  L_1 = (__this->___uvAnimationRate_3);
		float L_2 = Time_get_deltaTime_m235(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m235_MethodInfo);
		Vector2_t9  L_3 = Vector2_op_Multiply_m236(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/&Vector2_op_Multiply_m236_MethodInfo);
		Vector2_t9  L_4 = Vector2_op_Addition_m237(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/&Vector2_op_Addition_m237_MethodInfo);
		__this->___uvOffset_2 = L_4;
		Renderer_t7 * L_5 = (__this->___renderer_4);
		NullCheck(L_5);
		bool L_6 = Renderer_get_enabled_m238(L_5, /*hidden argument*/&Renderer_get_enabled_m238_MethodInfo);
		if (!L_6)
		{
			goto IL_004c;
		}
	}
	{
		Renderer_t7 * L_7 = (__this->___renderer_4);
		NullCheck(L_7);
		Material_t4 * L_8 = Renderer_get_material_m239(L_7, /*hidden argument*/&Renderer_get_material_m239_MethodInfo);
		Vector2_t9  L_9 = (__this->___uvOffset_2);
		NullCheck(L_8);
		Material_SetTextureOffset_m240(L_8, (String_t*) &_stringLiteral2, L_9, /*hidden argument*/&Material_SetTextureOffset_m240_MethodInfo);
	}

IL_004c:
	{
		return;
	}
}
// Metadata Definition AnimateTexture
extern Il2CppType Vector2_t9_0_0_6;
FieldInfo AnimateTexture_t8____uvOffset_2_FieldInfo = 
{
	"uvOffset"/* name */
	, &Vector2_t9_0_0_6/* type */
	, &AnimateTexture_t8_il2cpp_TypeInfo/* parent */
	, offsetof(AnimateTexture_t8, ___uvOffset_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t9_0_0_6;
FieldInfo AnimateTexture_t8____uvAnimationRate_3_FieldInfo = 
{
	"uvAnimationRate"/* name */
	, &Vector2_t9_0_0_6/* type */
	, &AnimateTexture_t8_il2cpp_TypeInfo/* parent */
	, offsetof(AnimateTexture_t8, ___uvAnimationRate_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Renderer_t7_0_0_1;
FieldInfo AnimateTexture_t8____renderer_4_FieldInfo = 
{
	"renderer"/* name */
	, &Renderer_t7_0_0_1/* type */
	, &AnimateTexture_t8_il2cpp_TypeInfo/* parent */
	, offsetof(AnimateTexture_t8, ___renderer_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* AnimateTexture_t8_FieldInfos[] =
{
	&AnimateTexture_t8____uvOffset_2_FieldInfo,
	&AnimateTexture_t8____uvAnimationRate_3_FieldInfo,
	&AnimateTexture_t8____renderer_4_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void AnimateTexture::.ctor()
MethodInfo AnimateTexture__ctor_m5_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AnimateTexture__ctor_m5/* method */
	, &AnimateTexture_t8_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 6/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void AnimateTexture::Awake()
MethodInfo AnimateTexture_Awake_m6_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&AnimateTexture_Awake_m6/* method */
	, &AnimateTexture_t8_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 7/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void AnimateTexture::Update()
MethodInfo AnimateTexture_Update_m7_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&AnimateTexture_Update_m7/* method */
	, &AnimateTexture_t8_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 8/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* AnimateTexture_t8_MethodInfos[] =
{
	&AnimateTexture__ctor_m5_MethodInfo,
	&AnimateTexture_Awake_m6_MethodInfo,
	&AnimateTexture_Update_m7_MethodInfo,
	NULL
};
static MethodInfo* AnimateTexture_t8_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType AnimateTexture_t8_0_0_0;
extern Il2CppType AnimateTexture_t8_1_0_0;
struct AnimateTexture_t8;
TypeInfo AnimateTexture_t8_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "AnimateTexture"/* name */
	, ""/* namespaze */
	, AnimateTexture_t8_MethodInfos/* methods */
	, NULL/* properties */
	, AnimateTexture_t8_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &AnimateTexture_t8_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, AnimateTexture_t8_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &AnimateTexture_t8_il2cpp_TypeInfo/* cast_class */
	, &AnimateTexture_t8_0_0_0/* byval_arg */
	, &AnimateTexture_t8_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AnimateTexture_t8)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// GyroCameraYo
#include "AssemblyU2DCSharp_GyroCameraYo.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GyroCameraYo_t11_il2cpp_TypeInfo;
// GyroCameraYo
#include "AssemblyU2DCSharp_GyroCameraYoMethodDeclarations.h"

// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Gyroscope
#include "UnityEngine_UnityEngine_Gyroscope.h"
extern TypeInfo Quaternion_t12_il2cpp_TypeInfo;
extern TypeInfo Vector3_t13_il2cpp_TypeInfo;
extern TypeInfo Mathf_t101_il2cpp_TypeInfo;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
// UnityEngine.Gyroscope
#include "UnityEngine_UnityEngine_GyroscopeMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
extern MethodInfo Quaternion__ctor_m242_MethodInfo;
extern MethodInfo Component_GetComponent_TisTransform_t10_m243_MethodInfo;
extern MethodInfo Screen_set_sleepTimeout_m244_MethodInfo;
extern MethodInfo Input_get_gyro_m245_MethodInfo;
extern MethodInfo Gyroscope_set_enabled_m246_MethodInfo;
extern MethodInfo Gyroscope_set_updateInterval_m247_MethodInfo;
extern MethodInfo Vector3__ctor_m248_MethodInfo;
extern MethodInfo Transform_set_eulerAngles_m249_MethodInfo;
extern MethodInfo GyroCameraYo_GyroCam_m12_MethodInfo;
extern MethodInfo Gyroscope_get_attitude_m250_MethodInfo;
extern MethodInfo Quaternion_get_eulerAngles_m251_MethodInfo;
extern MethodInfo Quaternion_Euler_m252_MethodInfo;
extern MethodInfo GyroCameraYo_ConvertRotation_m11_MethodInfo;
extern MethodInfo Gyroscope_get_rotationRate_m253_MethodInfo;
extern MethodInfo Transform_Rotate_m254_MethodInfo;
extern MethodInfo Transform_get_localRotation_m255_MethodInfo;
extern MethodInfo Mathf_Clamp01_m256_MethodInfo;
extern MethodInfo Quaternion_Slerp_m257_MethodInfo;
extern MethodInfo Transform_set_localRotation_m258_MethodInfo;
struct Component_t100;
// UnityEngine.CastHelper`1<UnityEngine.Transform>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_1.h"
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
#define Component_GetComponent_TisTransform_t10_m243(__this, method) (Transform_t10 *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)


// System.Void GyroCameraYo::.ctor()
extern MethodInfo GyroCameraYo__ctor_m8_MethodInfo;
 void GyroCameraYo__ctor_m8 (GyroCameraYo_t11 * __this, MethodInfo* method){
	{
		Quaternion_t12  L_0 = {0};
		Quaternion__ctor_m242(&L_0, (0.0f), (0.0f), (1.0f), (0.0f), /*hidden argument*/&Quaternion__ctor_m242_MethodInfo);
		__this->___rotFix_4 = L_0;
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void GyroCameraYo::Start()
extern MethodInfo GyroCameraYo_Start_m9_MethodInfo;
 void GyroCameraYo_Start_m9 (GyroCameraYo_t11 * __this, MethodInfo* method){
	{
		Transform_t10 * L_0 = Component_GetComponent_TisTransform_t10_m243(__this, /*hidden argument*/&Component_GetComponent_TisTransform_t10_m243_MethodInfo);
		__this->___transform_3 = L_0;
		Screen_set_sleepTimeout_m244(NULL /*static, unused*/, (-1), /*hidden argument*/&Screen_set_sleepTimeout_m244_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Gyroscope_t102 * L_1 = Input_get_gyro_m245(NULL /*static, unused*/, /*hidden argument*/&Input_get_gyro_m245_MethodInfo);
		NullCheck(L_1);
		Gyroscope_set_enabled_m246(L_1, 1, /*hidden argument*/&Gyroscope_set_enabled_m246_MethodInfo);
		Gyroscope_t102 * L_2 = Input_get_gyro_m245(NULL /*static, unused*/, /*hidden argument*/&Input_get_gyro_m245_MethodInfo);
		NullCheck(L_2);
		Gyroscope_set_updateInterval_m247(L_2, (0.01f), /*hidden argument*/&Gyroscope_set_updateInterval_m247_MethodInfo);
		Transform_t10 * L_3 = (__this->___parentTransform_2);
		NullCheck(L_3);
		Transform_t10 * L_4 = Component_get_transform_m219(L_3, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		Vector3_t13  L_5 = {0};
		Vector3__ctor_m248(&L_5, (90.0f), (200.0f), (0.0f), /*hidden argument*/&Vector3__ctor_m248_MethodInfo);
		NullCheck(L_4);
		Transform_set_eulerAngles_m249(L_4, L_5, /*hidden argument*/&Transform_set_eulerAngles_m249_MethodInfo);
		return;
	}
}
// System.Void GyroCameraYo::Update()
extern MethodInfo GyroCameraYo_Update_m10_MethodInfo;
 void GyroCameraYo_Update_m10 (GyroCameraYo_t11 * __this, MethodInfo* method){
	{
		GyroCameraYo_GyroCam_m12(__this, /*hidden argument*/&GyroCameraYo_GyroCam_m12_MethodInfo);
		return;
	}
}
// UnityEngine.Quaternion GyroCameraYo::ConvertRotation(UnityEngine.Quaternion)
 Quaternion_t12  GyroCameraYo_ConvertRotation_m11 (Object_t * __this/* static, unused */, Quaternion_t12  ___q, MethodInfo* method){
	{
		NullCheck((&___q));
		float L_0 = ((&___q)->___x_1);
		NullCheck((&___q));
		float L_1 = ((&___q)->___y_2);
		NullCheck((&___q));
		float L_2 = ((&___q)->___z_3);
		NullCheck((&___q));
		float L_3 = ((&___q)->___w_4);
		Quaternion_t12  L_4 = {0};
		Quaternion__ctor_m242(&L_4, L_0, L_1, ((-L_2)), ((-L_3)), /*hidden argument*/&Quaternion__ctor_m242_MethodInfo);
		return L_4;
	}
}
// System.Void GyroCameraYo::GyroCam()
 void GyroCameraYo_GyroCam_m12 (GyroCameraYo_t11 * __this, MethodInfo* method){
	Quaternion_t12  V_0 = {0};
	Vector3_t13  V_1 = {0};
	Quaternion_t12  V_2 = {0};
	Quaternion_t12  V_3 = {0};
	Quaternion_t12  V_4 = {0};
	Quaternion_t12  V_5 = {0};
	Vector3_t13  V_6 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Gyroscope_t102 * L_0 = Input_get_gyro_m245(NULL /*static, unused*/, /*hidden argument*/&Input_get_gyro_m245_MethodInfo);
		NullCheck(L_0);
		Quaternion_t12  L_1 = Gyroscope_get_attitude_m250(L_0, /*hidden argument*/&Gyroscope_get_attitude_m250_MethodInfo);
		V_5 = L_1;
		Vector3_t13  L_2 = Quaternion_get_eulerAngles_m251((&V_5), /*hidden argument*/&Quaternion_get_eulerAngles_m251_MethodInfo);
		Quaternion_t12  L_3 = Quaternion_Euler_m252(NULL /*static, unused*/, L_2, /*hidden argument*/&Quaternion_Euler_m252_MethodInfo);
		Quaternion_t12  L_4 = GyroCameraYo_ConvertRotation_m11(NULL /*static, unused*/, L_3, /*hidden argument*/&GyroCameraYo_ConvertRotation_m11_MethodInfo);
		V_0 = L_4;
		Vector3_t13  L_5 = Quaternion_get_eulerAngles_m251((&V_0), /*hidden argument*/&Quaternion_get_eulerAngles_m251_MethodInfo);
		V_1 = L_5;
		Transform_t10 * L_6 = (__this->___transform_3);
		Gyroscope_t102 * L_7 = Input_get_gyro_m245(NULL /*static, unused*/, /*hidden argument*/&Input_get_gyro_m245_MethodInfo);
		NullCheck(L_7);
		Vector3_t13  L_8 = Gyroscope_get_rotationRate_m253(L_7, /*hidden argument*/&Gyroscope_get_rotationRate_m253_MethodInfo);
		V_6 = L_8;
		NullCheck((&V_6));
		float L_9 = ((&V_6)->___y_2);
		NullCheck(L_6);
		Transform_Rotate_m254(L_6, (0.0f), ((-L_9)), (0.0f), /*hidden argument*/&Transform_Rotate_m254_MethodInfo);
		Transform_t10 * L_10 = (__this->___transform_3);
		NullCheck(L_10);
		Quaternion_t12  L_11 = Transform_get_localRotation_m255(L_10, /*hidden argument*/&Transform_get_localRotation_m255_MethodInfo);
		V_2 = L_11;
		float L_12 = Time_get_deltaTime_m235(NULL /*static, unused*/, /*hidden argument*/&Time_get_deltaTime_m235_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_13 = Mathf_Clamp01_m256(NULL /*static, unused*/, ((float)((float)(5.0f)*(float)L_12)), /*hidden argument*/&Mathf_Clamp01_m256_MethodInfo);
		Quaternion_t12  L_14 = Quaternion_Slerp_m257(NULL /*static, unused*/, V_2, V_0, L_13, /*hidden argument*/&Quaternion_Slerp_m257_MethodInfo);
		V_3 = L_14;
		V_4 = V_3;
		Transform_t10 * L_15 = (__this->___transform_3);
		NullCheck(L_15);
		Transform_set_localRotation_m258(L_15, V_4, /*hidden argument*/&Transform_set_localRotation_m258_MethodInfo);
		return;
	}
}
// Metadata Definition GyroCameraYo
extern Il2CppType Transform_t10_0_0_1;
extern CustomAttributesCache GyroCameraYo_t11__CustomAttributeCache_parentTransform;
FieldInfo GyroCameraYo_t11____parentTransform_2_FieldInfo = 
{
	"parentTransform"/* name */
	, &Transform_t10_0_0_1/* type */
	, &GyroCameraYo_t11_il2cpp_TypeInfo/* parent */
	, offsetof(GyroCameraYo_t11, ___parentTransform_2)/* data */
	, &GyroCameraYo_t11__CustomAttributeCache_parentTransform/* custom_attributes_cache */

};
extern Il2CppType Transform_t10_0_0_1;
FieldInfo GyroCameraYo_t11____transform_3_FieldInfo = 
{
	"transform"/* name */
	, &Transform_t10_0_0_1/* type */
	, &GyroCameraYo_t11_il2cpp_TypeInfo/* parent */
	, offsetof(GyroCameraYo_t11, ___transform_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Quaternion_t12_0_0_1;
FieldInfo GyroCameraYo_t11____rotFix_4_FieldInfo = 
{
	"rotFix"/* name */
	, &Quaternion_t12_0_0_1/* type */
	, &GyroCameraYo_t11_il2cpp_TypeInfo/* parent */
	, offsetof(GyroCameraYo_t11, ___rotFix_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector3_t13_0_0_1;
FieldInfo GyroCameraYo_t11____rotateAround_5_FieldInfo = 
{
	"rotateAround"/* name */
	, &Vector3_t13_0_0_1/* type */
	, &GyroCameraYo_t11_il2cpp_TypeInfo/* parent */
	, offsetof(GyroCameraYo_t11, ___rotateAround_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* GyroCameraYo_t11_FieldInfos[] =
{
	&GyroCameraYo_t11____parentTransform_2_FieldInfo,
	&GyroCameraYo_t11____transform_3_FieldInfo,
	&GyroCameraYo_t11____rotFix_4_FieldInfo,
	&GyroCameraYo_t11____rotateAround_5_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void GyroCameraYo::.ctor()
MethodInfo GyroCameraYo__ctor_m8_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GyroCameraYo__ctor_m8/* method */
	, &GyroCameraYo_t11_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 9/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void GyroCameraYo::Start()
MethodInfo GyroCameraYo_Start_m9_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&GyroCameraYo_Start_m9/* method */
	, &GyroCameraYo_t11_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 10/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void GyroCameraYo::Update()
MethodInfo GyroCameraYo_Update_m10_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&GyroCameraYo_Update_m10/* method */
	, &GyroCameraYo_t11_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 11/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Quaternion_t12_0_0_0;
extern Il2CppType Quaternion_t12_0_0_0;
static ParameterInfo GyroCameraYo_t11_GyroCameraYo_ConvertRotation_m11_ParameterInfos[] = 
{
	{"q", 0, 134217729, &EmptyCustomAttributesCache, &Quaternion_t12_0_0_0},
};
extern Il2CppType Quaternion_t12_0_0_0;
extern void* RuntimeInvoker_Quaternion_t12_Quaternion_t12 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion GyroCameraYo::ConvertRotation(UnityEngine.Quaternion)
MethodInfo GyroCameraYo_ConvertRotation_m11_MethodInfo = 
{
	"ConvertRotation"/* name */
	, (methodPointerType)&GyroCameraYo_ConvertRotation_m11/* method */
	, &GyroCameraYo_t11_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t12_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t12_Quaternion_t12/* invoker_method */
	, GyroCameraYo_t11_GyroCameraYo_ConvertRotation_m11_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 12/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void GyroCameraYo::GyroCam()
MethodInfo GyroCameraYo_GyroCam_m12_MethodInfo = 
{
	"GyroCam"/* name */
	, (methodPointerType)&GyroCameraYo_GyroCam_m12/* method */
	, &GyroCameraYo_t11_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 13/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* GyroCameraYo_t11_MethodInfos[] =
{
	&GyroCameraYo__ctor_m8_MethodInfo,
	&GyroCameraYo_Start_m9_MethodInfo,
	&GyroCameraYo_Update_m10_MethodInfo,
	&GyroCameraYo_ConvertRotation_m11_MethodInfo,
	&GyroCameraYo_GyroCam_m12_MethodInfo,
	NULL
};
static MethodInfo* GyroCameraYo_t11_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern TypeInfo SerializeField_t103_il2cpp_TypeInfo;
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern MethodInfo SerializeField__ctor_m259_MethodInfo;
void GyroCameraYo_t11_CustomAttributesCacheGenerator_parentTransform(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache GyroCameraYo_t11__CustomAttributeCache_parentTransform = {
1,
NULL,
&GyroCameraYo_t11_CustomAttributesCacheGenerator_parentTransform
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType GyroCameraYo_t11_0_0_0;
extern Il2CppType GyroCameraYo_t11_1_0_0;
struct GyroCameraYo_t11;
extern CustomAttributesCache GyroCameraYo_t11__CustomAttributeCache_parentTransform;
TypeInfo GyroCameraYo_t11_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "GyroCameraYo"/* name */
	, ""/* namespaze */
	, GyroCameraYo_t11_MethodInfos/* methods */
	, NULL/* properties */
	, GyroCameraYo_t11_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GyroCameraYo_t11_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, GyroCameraYo_t11_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GyroCameraYo_t11_il2cpp_TypeInfo/* cast_class */
	, &GyroCameraYo_t11_0_0_0/* byval_arg */
	, &GyroCameraYo_t11_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GyroCameraYo_t11)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Input2
#include "AssemblyU2DCSharp_Input2.h"
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Touch
#include "UnityEngine_UnityEngine_Touch.h"
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"
// UnityEngine.Touch
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"
extern MethodInfo Input_get_touchCount_m260_MethodInfo;
extern MethodInfo Input_GetTouch_m261_MethodInfo;
extern MethodInfo Touch_get_phase_m262_MethodInfo;
extern MethodInfo Touch_get_position_m263_MethodInfo;
extern MethodInfo Input2_GetPointerCount_m18_MethodInfo;
extern MethodInfo Touch_get_deltaPosition_m264_MethodInfo;
extern MethodInfo Vector2_op_Subtraction_m265_MethodInfo;
extern MethodInfo Vector2_get_magnitude_m266_MethodInfo;
extern MethodInfo Mathf_Atan2_m267_MethodInfo;
extern MethodInfo Mathf_DeltaAngle_m268_MethodInfo;
extern MethodInfo Mathf_Sign_m269_MethodInfo;
extern MethodInfo Input2_GetFirstPoint_m16_MethodInfo;
extern MethodInfo Input2_HasPointEnded_m20_MethodInfo;
extern MethodInfo Mathf_Abs_m270_MethodInfo;


// System.Void Input2::.ctor()
extern MethodInfo Input2__ctor_m13_MethodInfo;
 void Input2__ctor_m13 (Input2_t14 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void Input2::.cctor()
extern MethodInfo Input2__cctor_m14_MethodInfo;
 void Input2__cctor_m14 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		Vector2_t9  L_0 = {0};
		Vector2__ctor_m233(&L_0, (-999.0f), (-999.0f), /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		((Input2_t14_StaticFields*)InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo)->static_fields)->___offScreen_2 = L_0;
		((Input2_t14_StaticFields*)InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo)->static_fields)->___oldAngle_3 = (0.0f);
		Vector2_t9  L_1 = Vector2_get_zero_m232(NULL /*static, unused*/, /*hidden argument*/&Vector2_get_zero_m232_MethodInfo);
		((Input2_t14_StaticFields*)InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo)->static_fields)->___lastPos_4 = L_1;
		return;
	}
}
// System.Boolean Input2::TouchBegin()
extern MethodInfo Input2_TouchBegin_m15_MethodInfo;
 bool Input2_TouchBegin_m15 (Object_t * __this/* static, unused */, MethodInfo* method){
	Touch_t104  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		int32_t L_0 = Input_get_touchCount_m260(NULL /*static, unused*/, /*hidden argument*/&Input_get_touchCount_m260_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_1 = Input_GetTouch_m261(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_0 = L_1;
		int32_t L_2 = Touch_get_phase_m262((&V_0), /*hidden argument*/&Touch_get_phase_m262_MethodInfo);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		return 1;
	}

IL_0020:
	{
		return 0;
	}
}
// UnityEngine.Vector2 Input2::GetFirstPoint()
 Vector2_t9  Input2_GetFirstPoint_m16 (Object_t * __this/* static, unused */, MethodInfo* method){
	Touch_t104  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_0 = Input_GetTouch_m261(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_0 = L_0;
		Vector2_t9  L_1 = Touch_get_position_m263((&V_0), /*hidden argument*/&Touch_get_position_m263_MethodInfo);
		return L_1;
	}
}
// UnityEngine.Vector2 Input2::GetSecondPoint()
extern MethodInfo Input2_GetSecondPoint_m17_MethodInfo;
 Vector2_t9  Input2_GetSecondPoint_m17 (Object_t * __this/* static, unused */, MethodInfo* method){
	Touch_t104  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_0 = Input_GetTouch_m261(NULL /*static, unused*/, 1, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_0 = L_0;
		Vector2_t9  L_1 = Touch_get_position_m263((&V_0), /*hidden argument*/&Touch_get_position_m263_MethodInfo);
		return L_1;
	}
}
// System.Int32 Input2::GetPointerCount()
 int32_t Input2_GetPointerCount_m18 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		int32_t L_0 = Input_get_touchCount_m260(NULL /*static, unused*/, /*hidden argument*/&Input_get_touchCount_m260_MethodInfo);
		return L_0;
	}
}
// System.Boolean Input2::HasPointStarted(System.Int32)
 bool Input2_HasPointStarted_m19 (Object_t * __this/* static, unused */, int32_t ___num, MethodInfo* method){
	int32_t V_0 = 0;
	Touch_t104  V_1 = {0};
	Touch_t104  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo));
		int32_t L_0 = Input2_GetPointerCount_m18(NULL /*static, unused*/, /*hidden argument*/&Input2_GetPointerCount_m18_MethodInfo);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		if ((((uint32_t)___num) != ((uint32_t)V_0)))
		{
			goto IL_0047;
		}
	}
	{
		if ((((uint32_t)V_0) != ((uint32_t)1)))
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_1 = Input_GetTouch_m261(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_1 = L_1;
		int32_t L_2 = Touch_get_phase_m262((&V_1), /*hidden argument*/&Touch_get_phase_m262_MethodInfo);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}

IL_002e:
	{
		if ((((uint32_t)V_0) != ((uint32_t)2)))
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_3 = Input_GetTouch_m261(NULL /*static, unused*/, 1, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_2 = L_3;
		int32_t L_4 = Touch_get_phase_m262((&V_2), /*hidden argument*/&Touch_get_phase_m262_MethodInfo);
		return ((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}

IL_0047:
	{
		return 0;
	}
}
// System.Boolean Input2::HasPointEnded(System.Int32)
 bool Input2_HasPointEnded_m20 (Object_t * __this/* static, unused */, int32_t ___num, MethodInfo* method){
	int32_t V_0 = 0;
	Touch_t104  V_1 = {0};
	Touch_t104  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo));
		int32_t L_0 = Input2_GetPointerCount_m18(NULL /*static, unused*/, /*hidden argument*/&Input2_GetPointerCount_m18_MethodInfo);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		if ((((uint32_t)___num) != ((uint32_t)V_0)))
		{
			goto IL_0047;
		}
	}
	{
		if ((((uint32_t)V_0) != ((uint32_t)1)))
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_1 = Input_GetTouch_m261(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_1 = L_1;
		int32_t L_2 = Touch_get_phase_m262((&V_1), /*hidden argument*/&Touch_get_phase_m262_MethodInfo);
		return ((((int32_t)L_2) == ((int32_t)3))? 1 : 0);
	}

IL_002e:
	{
		if ((((uint32_t)V_0) != ((uint32_t)2)))
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_3 = Input_GetTouch_m261(NULL /*static, unused*/, 1, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_2 = L_3;
		int32_t L_4 = Touch_get_phase_m262((&V_2), /*hidden argument*/&Touch_get_phase_m262_MethodInfo);
		return ((((int32_t)L_4) == ((int32_t)3))? 1 : 0);
	}

IL_0047:
	{
		return 0;
	}
}
// System.Boolean Input2::HasPointMoved(System.Int32)
extern MethodInfo Input2_HasPointMoved_m21_MethodInfo;
 bool Input2_HasPointMoved_m21 (Object_t * __this/* static, unused */, int32_t ___num, MethodInfo* method){
	int32_t V_0 = 0;
	Touch_t104  V_1 = {0};
	Touch_t104  V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo));
		int32_t L_0 = Input2_GetPointerCount_m18(NULL /*static, unused*/, /*hidden argument*/&Input2_GetPointerCount_m18_MethodInfo);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_000e;
		}
	}
	{
		return 0;
	}

IL_000e:
	{
		if ((((uint32_t)___num) != ((uint32_t)V_0)))
		{
			goto IL_0047;
		}
	}
	{
		if ((((uint32_t)V_0) != ((uint32_t)1)))
		{
			goto IL_002e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_1 = Input_GetTouch_m261(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_1 = L_1;
		int32_t L_2 = Touch_get_phase_m262((&V_1), /*hidden argument*/&Touch_get_phase_m262_MethodInfo);
		return ((((int32_t)L_2) == ((int32_t)1))? 1 : 0);
	}

IL_002e:
	{
		if ((((uint32_t)V_0) != ((uint32_t)2)))
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_3 = Input_GetTouch_m261(NULL /*static, unused*/, 1, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_2 = L_3;
		int32_t L_4 = Touch_get_phase_m262((&V_2), /*hidden argument*/&Touch_get_phase_m262_MethodInfo);
		return ((((int32_t)L_4) == ((int32_t)1))? 1 : 0);
	}

IL_0047:
	{
		return 0;
	}
}
// System.Single Input2::Pinch()
extern MethodInfo Input2_Pinch_m22_MethodInfo;
 float Input2_Pinch_m22 (Object_t * __this/* static, unused */, MethodInfo* method){
	Touch_t104  V_0 = {0};
	Touch_t104  V_1 = {0};
	Vector2_t9  V_2 = {0};
	Vector2_t9  V_3 = {0};
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	Vector2_t9  V_7 = {0};
	Vector2_t9  V_8 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo));
		int32_t L_0 = Input2_GetPointerCount_m18(NULL /*static, unused*/, /*hidden argument*/&Input2_GetPointerCount_m18_MethodInfo);
		if ((((uint32_t)L_0) != ((uint32_t)2)))
		{
			goto IL_007b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_1 = Input_GetTouch_m261(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_0 = L_1;
		Touch_t104  L_2 = Input_GetTouch_m261(NULL /*static, unused*/, 1, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_1 = L_2;
		Vector2_t9  L_3 = Touch_get_position_m263((&V_0), /*hidden argument*/&Touch_get_position_m263_MethodInfo);
		Vector2_t9  L_4 = Touch_get_deltaPosition_m264((&V_0), /*hidden argument*/&Touch_get_deltaPosition_m264_MethodInfo);
		Vector2_t9  L_5 = Vector2_op_Subtraction_m265(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/&Vector2_op_Subtraction_m265_MethodInfo);
		V_2 = L_5;
		Vector2_t9  L_6 = Touch_get_position_m263((&V_1), /*hidden argument*/&Touch_get_position_m263_MethodInfo);
		Vector2_t9  L_7 = Touch_get_deltaPosition_m264((&V_1), /*hidden argument*/&Touch_get_deltaPosition_m264_MethodInfo);
		Vector2_t9  L_8 = Vector2_op_Subtraction_m265(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/&Vector2_op_Subtraction_m265_MethodInfo);
		V_3 = L_8;
		Vector2_t9  L_9 = Vector2_op_Subtraction_m265(NULL /*static, unused*/, V_2, V_3, /*hidden argument*/&Vector2_op_Subtraction_m265_MethodInfo);
		V_7 = L_9;
		float L_10 = Vector2_get_magnitude_m266((&V_7), /*hidden argument*/&Vector2_get_magnitude_m266_MethodInfo);
		V_4 = L_10;
		Vector2_t9  L_11 = Touch_get_position_m263((&V_0), /*hidden argument*/&Touch_get_position_m263_MethodInfo);
		Vector2_t9  L_12 = Touch_get_position_m263((&V_1), /*hidden argument*/&Touch_get_position_m263_MethodInfo);
		Vector2_t9  L_13 = Vector2_op_Subtraction_m265(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/&Vector2_op_Subtraction_m265_MethodInfo);
		V_8 = L_13;
		float L_14 = Vector2_get_magnitude_m266((&V_8), /*hidden argument*/&Vector2_get_magnitude_m266_MethodInfo);
		V_5 = L_14;
		V_6 = ((float)(V_4-V_5));
		return V_6;
	}

IL_007b:
	{
		return (0.0f);
	}
}
// System.Single Input2::Twist()
extern MethodInfo Input2_Twist_m23_MethodInfo;
 float Input2_Twist_m23 (Object_t * __this/* static, unused */, MethodInfo* method){
	Touch_t104  V_0 = {0};
	Touch_t104  V_1 = {0};
	Vector2_t9  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_0 = Input_GetTouch_m261(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_0 = L_0;
		Touch_t104  L_1 = Input_GetTouch_m261(NULL /*static, unused*/, 1, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_1 = L_1;
		Vector2_t9  L_2 = Touch_get_position_m263((&V_0), /*hidden argument*/&Touch_get_position_m263_MethodInfo);
		Vector2_t9  L_3 = Touch_get_position_m263((&V_1), /*hidden argument*/&Touch_get_position_m263_MethodInfo);
		Vector2_t9  L_4 = Vector2_op_Subtraction_m265(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/&Vector2_op_Subtraction_m265_MethodInfo);
		V_2 = L_4;
		NullCheck((&V_2));
		float L_5 = ((&V_2)->___y_2);
		NullCheck((&V_2));
		float L_6 = ((&V_2)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_7 = atan2f(L_5, L_6);
		V_3 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo));
		float L_8 = Mathf_DeltaAngle_m268(NULL /*static, unused*/, V_3, (((Input2_t14_StaticFields*)InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo)->static_fields)->___oldAngle_3), /*hidden argument*/&Mathf_DeltaAngle_m268_MethodInfo);
		V_4 = L_8;
		((Input2_t14_StaticFields*)InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo)->static_fields)->___oldAngle_3 = V_3;
		return V_3;
	}
}
// System.Int32 Input2::TwistInt()
extern MethodInfo Input2_TwistInt_m24_MethodInfo;
 int32_t Input2_TwistInt_m24 (Object_t * __this/* static, unused */, MethodInfo* method){
	Touch_t104  V_0 = {0};
	Touch_t104  V_1 = {0};
	Vector2_t9  V_2 = {0};
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t V_5 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_0 = Input_GetTouch_m261(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_0 = L_0;
		Touch_t104  L_1 = Input_GetTouch_m261(NULL /*static, unused*/, 1, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_1 = L_1;
		Vector2_t9  L_2 = Touch_get_position_m263((&V_0), /*hidden argument*/&Touch_get_position_m263_MethodInfo);
		Vector2_t9  L_3 = Touch_get_position_m263((&V_1), /*hidden argument*/&Touch_get_position_m263_MethodInfo);
		Vector2_t9  L_4 = Vector2_op_Subtraction_m265(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/&Vector2_op_Subtraction_m265_MethodInfo);
		V_2 = L_4;
		NullCheck((&V_2));
		float L_5 = ((&V_2)->___y_2);
		NullCheck((&V_2));
		float L_6 = ((&V_2)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_7 = atan2f(L_5, L_6);
		V_3 = L_7;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo));
		float L_8 = Mathf_DeltaAngle_m268(NULL /*static, unused*/, V_3, (((Input2_t14_StaticFields*)InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo)->static_fields)->___oldAngle_3), /*hidden argument*/&Mathf_DeltaAngle_m268_MethodInfo);
		V_4 = L_8;
		float L_9 = Mathf_Sign_m269(NULL /*static, unused*/, ((float)(V_3-(((Input2_t14_StaticFields*)InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo)->static_fields)->___oldAngle_3))), /*hidden argument*/&Mathf_Sign_m269_MethodInfo);
		V_5 = (((int32_t)L_9));
		((Input2_t14_StaticFields*)InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo)->static_fields)->___oldAngle_3 = V_3;
		return ((-V_5));
	}
}
// UnityEngine.Vector2 Input2::Swipe()
 Vector2_t9  Input2_Swipe_m25 (Object_t * __this/* static, unused */, MethodInfo* method){
	Vector2_t9  V_0 = {0};
	Vector2_t9  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo));
		int32_t L_0 = Input2_GetPointerCount_m18(NULL /*static, unused*/, /*hidden argument*/&Input2_GetPointerCount_m18_MethodInfo);
		if ((((uint32_t)L_0) != ((uint32_t)1)))
		{
			goto IL_009e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo));
		Vector2_t9  L_1 = Input2_GetFirstPoint_m16(NULL /*static, unused*/, /*hidden argument*/&Input2_GetFirstPoint_m16_MethodInfo);
		V_0 = L_1;
		bool L_2 = Input2_HasPointEnded_m20(NULL /*static, unused*/, 1, /*hidden argument*/&Input2_HasPointEnded_m20_MethodInfo);
		if (!L_2)
		{
			goto IL_0098;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo));
		Vector2_t9  L_3 = Vector2_op_Subtraction_m265(NULL /*static, unused*/, V_0, (((Input2_t14_StaticFields*)InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo)->static_fields)->___lastPos_4), /*hidden argument*/&Vector2_op_Subtraction_m265_MethodInfo);
		V_1 = L_3;
		float L_4 = Vector2_get_magnitude_m266((&V_1), /*hidden argument*/&Vector2_get_magnitude_m266_MethodInfo);
		if ((((float)L_4) <= ((float)(10.0f))))
		{
			goto IL_0098;
		}
	}
	{
		NullCheck((&V_1));
		float L_5 = ((&V_1)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_6 = fabsf(L_5);
		NullCheck((&V_1));
		float L_7 = ((&V_1)->___y_2);
		float L_8 = fabsf(L_7);
		if ((((float)L_6) <= ((float)L_8)))
		{
			goto IL_0077;
		}
	}
	{
		Vector2_t9  L_9 = {0};
		Vector2__ctor_m233(&L_9, (1.0f), (0.0f), /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		NullCheck((&V_1));
		float L_10 = ((&V_1)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_11 = Mathf_Sign_m269(NULL /*static, unused*/, L_10, /*hidden argument*/&Mathf_Sign_m269_MethodInfo);
		Vector2_t9  L_12 = Vector2_op_Multiply_m236(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/&Vector2_op_Multiply_m236_MethodInfo);
		return L_12;
	}

IL_0077:
	{
		Vector2_t9  L_13 = {0};
		Vector2__ctor_m233(&L_13, (0.0f), (1.0f), /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		NullCheck((&V_1));
		float L_14 = ((&V_1)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_15 = Mathf_Sign_m269(NULL /*static, unused*/, L_14, /*hidden argument*/&Mathf_Sign_m269_MethodInfo);
		Vector2_t9  L_16 = Vector2_op_Multiply_m236(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/&Vector2_op_Multiply_m236_MethodInfo);
		return L_16;
	}

IL_0098:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo));
		((Input2_t14_StaticFields*)InitializedTypeInfo(&Input2_t14_il2cpp_TypeInfo)->static_fields)->___lastPos_4 = V_0;
	}

IL_009e:
	{
		Vector2_t9  L_17 = Vector2_get_zero_m232(NULL /*static, unused*/, /*hidden argument*/&Vector2_get_zero_m232_MethodInfo);
		return L_17;
	}
}
// Metadata Definition Input2
extern Il2CppType Vector2_t9_0_0_22;
FieldInfo Input2_t14____offScreen_2_FieldInfo = 
{
	"offScreen"/* name */
	, &Vector2_t9_0_0_22/* type */
	, &Input2_t14_il2cpp_TypeInfo/* parent */
	, offsetof(Input2_t14_StaticFields, ___offScreen_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_17;
FieldInfo Input2_t14____oldAngle_3_FieldInfo = 
{
	"oldAngle"/* name */
	, &Single_t105_0_0_17/* type */
	, &Input2_t14_il2cpp_TypeInfo/* parent */
	, offsetof(Input2_t14_StaticFields, ___oldAngle_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t9_0_0_17;
FieldInfo Input2_t14____lastPos_4_FieldInfo = 
{
	"lastPos"/* name */
	, &Vector2_t9_0_0_17/* type */
	, &Input2_t14_il2cpp_TypeInfo/* parent */
	, offsetof(Input2_t14_StaticFields, ___lastPos_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Input2_t14_FieldInfos[] =
{
	&Input2_t14____offScreen_2_FieldInfo,
	&Input2_t14____oldAngle_3_FieldInfo,
	&Input2_t14____lastPos_4_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Input2::.ctor()
MethodInfo Input2__ctor_m13_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Input2__ctor_m13/* method */
	, &Input2_t14_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 14/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Input2::.cctor()
MethodInfo Input2__cctor_m14_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Input2__cctor_m14/* method */
	, &Input2_t14_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 15/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::TouchBegin()
MethodInfo Input2_TouchBegin_m15_MethodInfo = 
{
	"TouchBegin"/* name */
	, (methodPointerType)&Input2_TouchBegin_m15/* method */
	, &Input2_t14_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 16/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t9_0_0_0;
extern void* RuntimeInvoker_Vector2_t9 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Input2::GetFirstPoint()
MethodInfo Input2_GetFirstPoint_m16_MethodInfo = 
{
	"GetFirstPoint"/* name */
	, (methodPointerType)&Input2_GetFirstPoint_m16/* method */
	, &Input2_t14_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t9_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t9/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 17/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t9_0_0_0;
extern void* RuntimeInvoker_Vector2_t9 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Input2::GetSecondPoint()
MethodInfo Input2_GetSecondPoint_m17_MethodInfo = 
{
	"GetSecondPoint"/* name */
	, (methodPointerType)&Input2_GetSecondPoint_m17/* method */
	, &Input2_t14_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t9_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t9/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 18/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Int32 Input2::GetPointerCount()
MethodInfo Input2_GetPointerCount_m18_MethodInfo = 
{
	"GetPointerCount"/* name */
	, (methodPointerType)&Input2_GetPointerCount_m18/* method */
	, &Input2_t14_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 19/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo Input2_t14_Input2_HasPointStarted_m19_ParameterInfos[] = 
{
	{"num", 0, 134217730, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::HasPointStarted(System.Int32)
MethodInfo Input2_HasPointStarted_m19_MethodInfo = 
{
	"HasPointStarted"/* name */
	, (methodPointerType)&Input2_HasPointStarted_m19/* method */
	, &Input2_t14_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, Input2_t14_Input2_HasPointStarted_m19_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 20/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo Input2_t14_Input2_HasPointEnded_m20_ParameterInfos[] = 
{
	{"num", 0, 134217731, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::HasPointEnded(System.Int32)
MethodInfo Input2_HasPointEnded_m20_MethodInfo = 
{
	"HasPointEnded"/* name */
	, (methodPointerType)&Input2_HasPointEnded_m20/* method */
	, &Input2_t14_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, Input2_t14_Input2_HasPointEnded_m20_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 21/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo Input2_t14_Input2_HasPointMoved_m21_ParameterInfos[] = 
{
	{"num", 0, 134217732, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Boolean Input2::HasPointMoved(System.Int32)
MethodInfo Input2_HasPointMoved_m21_MethodInfo = 
{
	"HasPointMoved"/* name */
	, (methodPointerType)&Input2_HasPointMoved_m21/* method */
	, &Input2_t14_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, Input2_t14_Input2_HasPointMoved_m21_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 22/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t105_0_0_0;
extern void* RuntimeInvoker_Single_t105 (MethodInfo* method, void* obj, void** args);
// System.Single Input2::Pinch()
MethodInfo Input2_Pinch_m22_MethodInfo = 
{
	"Pinch"/* name */
	, (methodPointerType)&Input2_Pinch_m22/* method */
	, &Input2_t14_il2cpp_TypeInfo/* declaring_type */
	, &Single_t105_0_0_0/* return_type */
	, RuntimeInvoker_Single_t105/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 23/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t105_0_0_0;
extern void* RuntimeInvoker_Single_t105 (MethodInfo* method, void* obj, void** args);
// System.Single Input2::Twist()
MethodInfo Input2_Twist_m23_MethodInfo = 
{
	"Twist"/* name */
	, (methodPointerType)&Input2_Twist_m23/* method */
	, &Input2_t14_il2cpp_TypeInfo/* declaring_type */
	, &Single_t105_0_0_0/* return_type */
	, RuntimeInvoker_Single_t105/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 24/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Int32 Input2::TwistInt()
MethodInfo Input2_TwistInt_m24_MethodInfo = 
{
	"TwistInt"/* name */
	, (methodPointerType)&Input2_TwistInt_m24/* method */
	, &Input2_t14_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 25/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t9_0_0_0;
extern void* RuntimeInvoker_Vector2_t9 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Input2::Swipe()
MethodInfo Input2_Swipe_m25_MethodInfo = 
{
	"Swipe"/* name */
	, (methodPointerType)&Input2_Swipe_m25/* method */
	, &Input2_t14_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t9_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t9/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 26/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Input2_t14_MethodInfos[] =
{
	&Input2__ctor_m13_MethodInfo,
	&Input2__cctor_m14_MethodInfo,
	&Input2_TouchBegin_m15_MethodInfo,
	&Input2_GetFirstPoint_m16_MethodInfo,
	&Input2_GetSecondPoint_m17_MethodInfo,
	&Input2_GetPointerCount_m18_MethodInfo,
	&Input2_HasPointStarted_m19_MethodInfo,
	&Input2_HasPointEnded_m20_MethodInfo,
	&Input2_HasPointMoved_m21_MethodInfo,
	&Input2_Pinch_m22_MethodInfo,
	&Input2_Twist_m23_MethodInfo,
	&Input2_TwistInt_m24_MethodInfo,
	&Input2_Swipe_m25_MethodInfo,
	NULL
};
static MethodInfo* Input2_t14_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType Input2_t14_0_0_0;
extern Il2CppType Input2_t14_1_0_0;
struct Input2_t14;
TypeInfo Input2_t14_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Input2"/* name */
	, ""/* namespaze */
	, Input2_t14_MethodInfos/* methods */
	, NULL/* properties */
	, Input2_t14_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Input2_t14_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Input2_t14_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Input2_t14_il2cpp_TypeInfo/* cast_class */
	, &Input2_t14_0_0_0/* byval_arg */
	, &Input2_t14_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Input2_t14)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Input2_t14_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 13/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// System.Guid
#include "mscorlib_System_Guid.h"
#include "mscorlib_ArrayTypes.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.Double
#include "mscorlib_System_Double.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"
// System.DateTimeKind
#include "mscorlib_System_DateTimeKind.h"
extern TypeInfo RaycastHit_t16_il2cpp_TypeInfo;
extern TypeInfo Guid_t107_il2cpp_TypeInfo;
extern TypeInfo CharU5BU5D_t108_il2cpp_TypeInfo;
extern TypeInfo Char_t109_il2cpp_TypeInfo;
extern TypeInfo DateTime_t110_il2cpp_TypeInfo;
extern TypeInfo Double_t111_il2cpp_TypeInfo;
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// UnityEngine.Physics
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
// System.Guid
#include "mscorlib_System_GuidMethodDeclarations.h"
// UnityEngine.Random
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
// System.Single
#include "mscorlib_System_SingleMethodDeclarations.h"
// System.DateTime
#include "mscorlib_System_DateTimeMethodDeclarations.h"
// System.TimeSpan
#include "mscorlib_System_TimeSpanMethodDeclarations.h"
// System.Double
#include "mscorlib_System_DoubleMethodDeclarations.h"
extern MethodInfo Object__ctor_m271_MethodInfo;
extern MethodInfo LayerMask_op_Implicit_m272_MethodInfo;
extern MethodInfo Mathf2_WhatDidWeHitCenterScreen_m29_MethodInfo;
extern MethodInfo Screen_get_width_m273_MethodInfo;
extern MethodInfo Screen_get_height_m274_MethodInfo;
extern MethodInfo Mathf2_WhatDidWeHit_m32_MethodInfo;
extern MethodInfo Camera_get_main_m275_MethodInfo;
extern MethodInfo Vector2_op_Implicit_m276_MethodInfo;
extern MethodInfo Camera_ScreenPointToRay_m277_MethodInfo;
extern MethodInfo Mathf2_WhatDidWeHit_m33_MethodInfo;
extern MethodInfo RaycastHit_set_point_m278_MethodInfo;
extern MethodInfo LayerMask_op_Implicit_m279_MethodInfo;
extern MethodInfo Physics_Raycast_m280_MethodInfo;
extern MethodInfo Int32_ToString_m281_MethodInfo;
extern MethodInfo String_Concat_m282_MethodInfo;
extern MethodInfo Mathf_Round_m283_MethodInfo;
extern MethodInfo Guid_NewGuid_m284_MethodInfo;
extern MethodInfo Guid_ToString_m285_MethodInfo;
extern MethodInfo Mathf2_round_m39_MethodInfo;
extern MethodInfo Random_Range_m286_MethodInfo;
extern MethodInfo Quaternion_Euler_m287_MethodInfo;
extern MethodInfo Single_Parse_m288_MethodInfo;
extern MethodInfo Int32_TryParse_m289_MethodInfo;
extern MethodInfo Mathf2_String2Float_m40_MethodInfo;
extern MethodInfo Mathf_Floor_m290_MethodInfo;
extern MethodInfo String_Replace_m291_MethodInfo;
extern MethodInfo String_get_Chars_m292_MethodInfo;
extern MethodInfo String_Split_m293_MethodInfo;
extern MethodInfo DateTime__ctor_m294_MethodInfo;
extern MethodInfo DateTime_get_UtcNow_m295_MethodInfo;
extern MethodInfo DateTime_op_Subtraction_m296_MethodInfo;
extern MethodInfo TimeSpan_get_TotalSeconds_m297_MethodInfo;
extern MethodInfo Double_ToString_m298_MethodInfo;
extern MethodInfo Transform_get_position_m299_MethodInfo;
extern MethodInfo Vector3_ToString_m300_MethodInfo;
extern MethodInfo Transform_get_rotation_m301_MethodInfo;
extern MethodInfo Quaternion_ToString_m302_MethodInfo;
extern MethodInfo Transform_get_localScale_m303_MethodInfo;


// System.Void Mathf2::.ctor()
extern MethodInfo Mathf2__ctor_m26_MethodInfo;
 void Mathf2__ctor_m26 (Mathf2_t15 * __this, MethodInfo* method){
	{
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		return;
	}
}
// System.Void Mathf2::.cctor()
extern MethodInfo Mathf2__cctor_m27_MethodInfo;
 void Mathf2__cctor_m27 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		Vector3_t13  L_0 = {0};
		Vector3__ctor_m248(&L_0, (-9999.0f), (-9999.0f), (-9999.0f), /*hidden argument*/&Vector3__ctor_m248_MethodInfo);
		((Mathf2_t15_StaticFields*)InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo)->static_fields)->___FarFarAway_0 = L_0;
		return;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreenIgnore0()
extern MethodInfo Mathf2_WhatDidWeHitCenterScreenIgnore0_m28_MethodInfo;
 RaycastHit_t16  Mathf2_WhatDidWeHitCenterScreenIgnore0_m28 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		LayerMask_t17  L_0 = LayerMask_op_Implicit_m272(NULL /*static, unused*/, ((int32_t)-2), /*hidden argument*/&LayerMask_op_Implicit_m272_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo));
		RaycastHit_t16  L_1 = Mathf2_WhatDidWeHitCenterScreen_m29(NULL /*static, unused*/, L_0, /*hidden argument*/&Mathf2_WhatDidWeHitCenterScreen_m29_MethodInfo);
		return L_1;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen(UnityEngine.LayerMask)
 RaycastHit_t16  Mathf2_WhatDidWeHitCenterScreen_m29 (Object_t * __this/* static, unused */, LayerMask_t17  ___lm, MethodInfo* method){
	{
		int32_t L_0 = Screen_get_width_m273(NULL /*static, unused*/, /*hidden argument*/&Screen_get_width_m273_MethodInfo);
		int32_t L_1 = Screen_get_height_m274(NULL /*static, unused*/, /*hidden argument*/&Screen_get_height_m274_MethodInfo);
		Vector2_t9  L_2 = {0};
		Vector2__ctor_m233(&L_2, ((float)((float)(((float)L_0))*(float)(0.5f))), ((float)((float)(((float)L_1))*(float)(0.5f))), /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo));
		RaycastHit_t16  L_3 = Mathf2_WhatDidWeHit_m32(NULL /*static, unused*/, L_2, ___lm, /*hidden argument*/&Mathf2_WhatDidWeHit_m32_MethodInfo);
		return L_3;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen()
extern MethodInfo Mathf2_WhatDidWeHitCenterScreen_m30_MethodInfo;
 RaycastHit_t16  Mathf2_WhatDidWeHitCenterScreen_m30 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		LayerMask_t17  L_0 = LayerMask_op_Implicit_m272(NULL /*static, unused*/, (-1), /*hidden argument*/&LayerMask_op_Implicit_m272_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo));
		RaycastHit_t16  L_1 = Mathf2_WhatDidWeHitCenterScreen_m29(NULL /*static, unused*/, L_0, /*hidden argument*/&Mathf2_WhatDidWeHitCenterScreen_m29_MethodInfo);
		return L_1;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2)
 RaycastHit_t16  Mathf2_WhatDidWeHit_m31 (Object_t * __this/* static, unused */, Vector2_t9  ___v, MethodInfo* method){
	{
		LayerMask_t17  L_0 = LayerMask_op_Implicit_m272(NULL /*static, unused*/, (-1), /*hidden argument*/&LayerMask_op_Implicit_m272_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo));
		RaycastHit_t16  L_1 = Mathf2_WhatDidWeHit_m32(NULL /*static, unused*/, ___v, L_0, /*hidden argument*/&Mathf2_WhatDidWeHit_m32_MethodInfo);
		return L_1;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2,UnityEngine.LayerMask)
 RaycastHit_t16  Mathf2_WhatDidWeHit_m32 (Object_t * __this/* static, unused */, Vector2_t9  ___v, LayerMask_t17  ___lm, MethodInfo* method){
	Ray_t18  V_0 = {0};
	{
		Camera_t3 * L_0 = Camera_get_main_m275(NULL /*static, unused*/, /*hidden argument*/&Camera_get_main_m275_MethodInfo);
		Vector3_t13  L_1 = Vector2_op_Implicit_m276(NULL /*static, unused*/, ___v, /*hidden argument*/&Vector2_op_Implicit_m276_MethodInfo);
		NullCheck(L_0);
		Ray_t18  L_2 = Camera_ScreenPointToRay_m277(L_0, L_1, /*hidden argument*/&Camera_ScreenPointToRay_m277_MethodInfo);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo));
		RaycastHit_t16  L_3 = Mathf2_WhatDidWeHit_m33(NULL /*static, unused*/, V_0, ___lm, /*hidden argument*/&Mathf2_WhatDidWeHit_m33_MethodInfo);
		return L_3;
	}
}
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Ray,UnityEngine.LayerMask)
 RaycastHit_t16  Mathf2_WhatDidWeHit_m33 (Object_t * __this/* static, unused */, Ray_t18  ___ray, LayerMask_t17  ___lm, MethodInfo* method){
	RaycastHit_t16  V_0 = {0};
	{
		Initobj (&RaycastHit_t16_il2cpp_TypeInfo, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo));
		RaycastHit_set_point_m278((&V_0), (((Mathf2_t15_StaticFields*)InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo)->static_fields)->___FarFarAway_0), /*hidden argument*/&RaycastHit_set_point_m278_MethodInfo);
		int32_t L_0 = LayerMask_op_Implicit_m279(NULL /*static, unused*/, ___lm, /*hidden argument*/&LayerMask_op_Implicit_m279_MethodInfo);
		bool L_1 = Physics_Raycast_m280(NULL /*static, unused*/, ___ray, (&V_0), (3000.0f), L_0, /*hidden argument*/&Physics_Raycast_m280_MethodInfo);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		return V_0;
	}

IL_002e:
	{
		return V_0;
	}
}
// System.String Mathf2::Pad0s(System.Int32)
extern MethodInfo Mathf2_Pad0s_m34_MethodInfo;
 String_t* Mathf2_Pad0s_m34 (Object_t * __this/* static, unused */, int32_t ___n, MethodInfo* method){
	{
		if ((((int32_t)___n) >= ((int32_t)((int32_t)10))))
		{
			goto IL_001a;
		}
	}
	{
		String_t* L_0 = Int32_ToString_m281((&___n), /*hidden argument*/&Int32_ToString_m281_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_1 = String_Concat_m282(NULL /*static, unused*/, (String_t*) &_stringLiteral3, L_0, /*hidden argument*/&String_Concat_m282_MethodInfo);
		return L_1;
	}

IL_001a:
	{
		if ((((int32_t)___n) >= ((int32_t)((int32_t)100))))
		{
			goto IL_0034;
		}
	}
	{
		String_t* L_2 = Int32_ToString_m281((&___n), /*hidden argument*/&Int32_ToString_m281_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_3 = String_Concat_m282(NULL /*static, unused*/, (String_t*) &_stringLiteral4, L_2, /*hidden argument*/&String_Concat_m282_MethodInfo);
		return L_3;
	}

IL_0034:
	{
		String_t* L_4 = Int32_ToString_m281((&___n), /*hidden argument*/&Int32_ToString_m281_MethodInfo);
		return L_4;
	}
}
// System.Single Mathf2::Round10th(System.Single)
extern MethodInfo Mathf2_Round10th_m35_MethodInfo;
 float Mathf2_Round10th_m35 (Object_t * __this/* static, unused */, float ___n, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_0 = roundf(((float)((float)___n*(float)(10.0f))));
		return ((float)((float)L_0/(float)(10.0f)));
	}
}
// System.String Mathf2::GenUUID()
extern MethodInfo Mathf2_GenUUID_m36_MethodInfo;
 String_t* Mathf2_GenUUID_m36 (Object_t * __this/* static, unused */, MethodInfo* method){
	Guid_t107  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Guid_t107_il2cpp_TypeInfo));
		Guid_t107  L_0 = Guid_NewGuid_m284(NULL /*static, unused*/, /*hidden argument*/&Guid_NewGuid_m284_MethodInfo);
		V_0 = L_0;
		String_t* L_1 = Guid_ToString_m285((&V_0), (String_t*) &_stringLiteral5, /*hidden argument*/&Guid_ToString_m285_MethodInfo);
		return L_1;
	}
}
// UnityEngine.Vector3 Mathf2::RoundVector3(UnityEngine.Vector3)
extern MethodInfo Mathf2_RoundVector3_m37_MethodInfo;
 Vector3_t13  Mathf2_RoundVector3_m37 (Object_t * __this/* static, unused */, Vector3_t13  ___v, MethodInfo* method){
	{
		NullCheck((&___v));
		float L_0 = ((&___v)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo));
		float L_1 = Mathf2_round_m39(NULL /*static, unused*/, L_0, /*hidden argument*/&Mathf2_round_m39_MethodInfo);
		NullCheck((&___v));
		float L_2 = ((&___v)->___y_2);
		float L_3 = Mathf2_round_m39(NULL /*static, unused*/, L_2, /*hidden argument*/&Mathf2_round_m39_MethodInfo);
		NullCheck((&___v));
		float L_4 = ((&___v)->___z_3);
		float L_5 = Mathf2_round_m39(NULL /*static, unused*/, L_4, /*hidden argument*/&Mathf2_round_m39_MethodInfo);
		Vector3_t13  L_6 = {0};
		Vector3__ctor_m248(&L_6, L_1, L_3, L_5, /*hidden argument*/&Vector3__ctor_m248_MethodInfo);
		return L_6;
	}
}
// UnityEngine.Quaternion Mathf2::RandRot()
extern MethodInfo Mathf2_RandRot_m38_MethodInfo;
 Quaternion_t12  Mathf2_RandRot_m38 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		int32_t L_0 = Random_Range_m286(NULL /*static, unused*/, 0, ((int32_t)360), /*hidden argument*/&Random_Range_m286_MethodInfo);
		int32_t L_1 = Random_Range_m286(NULL /*static, unused*/, 0, ((int32_t)360), /*hidden argument*/&Random_Range_m286_MethodInfo);
		int32_t L_2 = Random_Range_m286(NULL /*static, unused*/, 0, ((int32_t)360), /*hidden argument*/&Random_Range_m286_MethodInfo);
		Quaternion_t12  L_3 = Quaternion_Euler_m287(NULL /*static, unused*/, (((float)L_0)), (((float)L_1)), (((float)L_2)), /*hidden argument*/&Quaternion_Euler_m287_MethodInfo);
		return L_3;
	}
}
// System.Single Mathf2::round(System.Single)
 float Mathf2_round_m39 (Object_t * __this/* static, unused */, float ___f, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_0 = roundf(___f);
		return L_0;
	}
}
// System.Single Mathf2::String2Float(System.String)
 float Mathf2_String2Float_m40 (Object_t * __this/* static, unused */, String_t* ___str, MethodInfo* method){
	{
		float L_0 = Single_Parse_m288(NULL /*static, unused*/, ___str, /*hidden argument*/&Single_Parse_m288_MethodInfo);
		return L_0;
	}
}
// System.Boolean Mathf2::isNumeric(System.String)
extern MethodInfo Mathf2_isNumeric_m41_MethodInfo;
 bool Mathf2_isNumeric_m41 (Object_t * __this/* static, unused */, String_t* ___str, MethodInfo* method){
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		bool L_0 = Int32_TryParse_m289(NULL /*static, unused*/, ___str, (&V_0), /*hidden argument*/&Int32_TryParse_m289_MethodInfo);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// System.Int32 Mathf2::String2Int(System.String)
extern MethodInfo Mathf2_String2Int_m42_MethodInfo;
 int32_t Mathf2_String2Int_m42 (Object_t * __this/* static, unused */, String_t* ___str, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo));
		float L_0 = Mathf2_String2Float_m40(NULL /*static, unused*/, ___str, /*hidden argument*/&Mathf2_String2Float_m40_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_1 = floorf(L_0);
		return (((int32_t)L_1));
	}
}
// UnityEngine.Color Mathf2::String2Color(System.String)
extern MethodInfo Mathf2_String2Color_m43_MethodInfo;
 Color_t19  Mathf2_String2Color_m43 (Object_t * __this/* static, unused */, String_t* ___str, MethodInfo* method){
	StringU5BU5D_t112* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		NullCheck(___str);
		String_t* L_0 = String_Replace_m291(___str, (String_t*) &_stringLiteral6, (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), /*hidden argument*/&String_Replace_m291_MethodInfo);
		___str = L_0;
		NullCheck(___str);
		String_t* L_1 = String_Replace_m291(___str, (String_t*) &_stringLiteral7, (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), /*hidden argument*/&String_Replace_m291_MethodInfo);
		___str = L_1;
		CharU5BU5D_t108* L_2 = ((CharU5BU5D_t108*)SZArrayNew(InitializedTypeInfo(&CharU5BU5D_t108_il2cpp_TypeInfo), 1));
		NullCheck((String_t*) &_stringLiteral8);
		uint16_t L_3 = String_get_Chars_m292((String_t*) &_stringLiteral8, 0, /*hidden argument*/&String_get_Chars_m292_MethodInfo);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_2, 0)) = (uint16_t)L_3;
		NullCheck(___str);
		StringU5BU5D_t112* L_4 = String_Split_m293(___str, L_2, /*hidden argument*/&String_Split_m293_MethodInfo);
		V_0 = L_4;
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, 0);
		int32_t L_5 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo));
		float L_6 = Mathf2_String2Float_m40(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_5)), /*hidden argument*/&Mathf2_String2Float_m40_MethodInfo);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, 1);
		int32_t L_7 = 1;
		float L_8 = Mathf2_String2Float_m40(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_7)), /*hidden argument*/&Mathf2_String2Float_m40_MethodInfo);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, 2);
		int32_t L_9 = 2;
		float L_10 = Mathf2_String2Float_m40(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_9)), /*hidden argument*/&Mathf2_String2Float_m40_MethodInfo);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, 3);
		int32_t L_11 = 3;
		float L_12 = Mathf2_String2Float_m40(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_11)), /*hidden argument*/&Mathf2_String2Float_m40_MethodInfo);
		Color_t19  L_13 = {0};
		Color__ctor_m217(&L_13, L_6, L_8, L_10, L_12, /*hidden argument*/&Color__ctor_m217_MethodInfo);
		return L_13;
	}
}
// UnityEngine.Vector3 Mathf2::String2Vector3(System.String)
extern MethodInfo Mathf2_String2Vector3_m44_MethodInfo;
 Vector3_t13  Mathf2_String2Vector3_m44 (Object_t * __this/* static, unused */, String_t* ___str, MethodInfo* method){
	StringU5BU5D_t112* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		NullCheck(___str);
		String_t* L_0 = String_Replace_m291(___str, (String_t*) &_stringLiteral9, (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), /*hidden argument*/&String_Replace_m291_MethodInfo);
		___str = L_0;
		NullCheck(___str);
		String_t* L_1 = String_Replace_m291(___str, (String_t*) &_stringLiteral7, (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), /*hidden argument*/&String_Replace_m291_MethodInfo);
		___str = L_1;
		CharU5BU5D_t108* L_2 = ((CharU5BU5D_t108*)SZArrayNew(InitializedTypeInfo(&CharU5BU5D_t108_il2cpp_TypeInfo), 1));
		NullCheck((String_t*) &_stringLiteral8);
		uint16_t L_3 = String_get_Chars_m292((String_t*) &_stringLiteral8, 0, /*hidden argument*/&String_get_Chars_m292_MethodInfo);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_2, 0)) = (uint16_t)L_3;
		NullCheck(___str);
		StringU5BU5D_t112* L_4 = String_Split_m293(___str, L_2, /*hidden argument*/&String_Split_m293_MethodInfo);
		V_0 = L_4;
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, 0);
		int32_t L_5 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo));
		float L_6 = Mathf2_String2Float_m40(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_5)), /*hidden argument*/&Mathf2_String2Float_m40_MethodInfo);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, 1);
		int32_t L_7 = 1;
		float L_8 = Mathf2_String2Float_m40(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_7)), /*hidden argument*/&Mathf2_String2Float_m40_MethodInfo);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, 2);
		int32_t L_9 = 2;
		float L_10 = Mathf2_String2Float_m40(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_9)), /*hidden argument*/&Mathf2_String2Float_m40_MethodInfo);
		Vector3_t13  L_11 = {0};
		Vector3__ctor_m248(&L_11, L_6, L_8, L_10, /*hidden argument*/&Vector3__ctor_m248_MethodInfo);
		return L_11;
	}
}
// UnityEngine.Vector2 Mathf2::String2Vector2(System.String)
extern MethodInfo Mathf2_String2Vector2_m45_MethodInfo;
 Vector2_t9  Mathf2_String2Vector2_m45 (Object_t * __this/* static, unused */, String_t* ___str, MethodInfo* method){
	StringU5BU5D_t112* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		NullCheck(___str);
		String_t* L_0 = String_Replace_m291(___str, (String_t*) &_stringLiteral9, (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), /*hidden argument*/&String_Replace_m291_MethodInfo);
		___str = L_0;
		NullCheck(___str);
		String_t* L_1 = String_Replace_m291(___str, (String_t*) &_stringLiteral7, (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), /*hidden argument*/&String_Replace_m291_MethodInfo);
		___str = L_1;
		CharU5BU5D_t108* L_2 = ((CharU5BU5D_t108*)SZArrayNew(InitializedTypeInfo(&CharU5BU5D_t108_il2cpp_TypeInfo), 1));
		NullCheck((String_t*) &_stringLiteral8);
		uint16_t L_3 = String_get_Chars_m292((String_t*) &_stringLiteral8, 0, /*hidden argument*/&String_get_Chars_m292_MethodInfo);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_2, 0)) = (uint16_t)L_3;
		NullCheck(___str);
		StringU5BU5D_t112* L_4 = String_Split_m293(___str, L_2, /*hidden argument*/&String_Split_m293_MethodInfo);
		V_0 = L_4;
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, 0);
		int32_t L_5 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo));
		float L_6 = Mathf2_String2Float_m40(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_5)), /*hidden argument*/&Mathf2_String2Float_m40_MethodInfo);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, 1);
		int32_t L_7 = 1;
		float L_8 = Mathf2_String2Float_m40(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_7)), /*hidden argument*/&Mathf2_String2Float_m40_MethodInfo);
		Vector2_t9  L_9 = {0};
		Vector2__ctor_m233(&L_9, L_6, L_8, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		return L_9;
	}
}
// System.Single Mathf2::RoundFraction(System.Single,System.Single)
extern MethodInfo Mathf2_RoundFraction_m46_MethodInfo;
 float Mathf2_RoundFraction_m46 (Object_t * __this/* static, unused */, float ___val, float ___denominator, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_0 = floorf(((float)((float)___val*(float)___denominator)));
		return ((float)((float)L_0/(float)___denominator));
	}
}
// UnityEngine.Quaternion Mathf2::String2Quat(System.String)
extern MethodInfo Mathf2_String2Quat_m47_MethodInfo;
 Quaternion_t12  Mathf2_String2Quat_m47 (Object_t * __this/* static, unused */, String_t* ___str, MethodInfo* method){
	StringU5BU5D_t112* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		NullCheck(___str);
		String_t* L_0 = String_Replace_m291(___str, (String_t*) &_stringLiteral9, (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), /*hidden argument*/&String_Replace_m291_MethodInfo);
		___str = L_0;
		NullCheck(___str);
		String_t* L_1 = String_Replace_m291(___str, (String_t*) &_stringLiteral7, (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), /*hidden argument*/&String_Replace_m291_MethodInfo);
		___str = L_1;
		CharU5BU5D_t108* L_2 = ((CharU5BU5D_t108*)SZArrayNew(InitializedTypeInfo(&CharU5BU5D_t108_il2cpp_TypeInfo), 1));
		NullCheck((String_t*) &_stringLiteral8);
		uint16_t L_3 = String_get_Chars_m292((String_t*) &_stringLiteral8, 0, /*hidden argument*/&String_get_Chars_m292_MethodInfo);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_2, 0)) = (uint16_t)L_3;
		NullCheck(___str);
		StringU5BU5D_t112* L_4 = String_Split_m293(___str, L_2, /*hidden argument*/&String_Split_m293_MethodInfo);
		V_0 = L_4;
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, 0);
		int32_t L_5 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf2_t15_il2cpp_TypeInfo));
		float L_6 = Mathf2_String2Float_m40(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_5)), /*hidden argument*/&Mathf2_String2Float_m40_MethodInfo);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, 1);
		int32_t L_7 = 1;
		float L_8 = Mathf2_String2Float_m40(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_7)), /*hidden argument*/&Mathf2_String2Float_m40_MethodInfo);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, 2);
		int32_t L_9 = 2;
		float L_10 = Mathf2_String2Float_m40(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_9)), /*hidden argument*/&Mathf2_String2Float_m40_MethodInfo);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, 3);
		int32_t L_11 = 3;
		float L_12 = Mathf2_String2Float_m40(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_11)), /*hidden argument*/&Mathf2_String2Float_m40_MethodInfo);
		Quaternion_t12  L_13 = {0};
		Quaternion__ctor_m242(&L_13, L_6, L_8, L_10, L_12, /*hidden argument*/&Quaternion__ctor_m242_MethodInfo);
		return L_13;
	}
}
// UnityEngine.Vector3 Mathf2::UnsignedVector3(UnityEngine.Vector3)
extern MethodInfo Mathf2_UnsignedVector3_m48_MethodInfo;
 Vector3_t13  Mathf2_UnsignedVector3_m48 (Object_t * __this/* static, unused */, Vector3_t13  ___v, MethodInfo* method){
	{
		NullCheck((&___v));
		float L_0 = ((&___v)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_1 = fabsf(L_0);
		NullCheck((&___v));
		float L_2 = ((&___v)->___y_2);
		float L_3 = fabsf(L_2);
		NullCheck((&___v));
		float L_4 = ((&___v)->___z_3);
		float L_5 = fabsf(L_4);
		Vector3_t13  L_6 = {0};
		Vector3__ctor_m248(&L_6, L_1, L_3, L_5, /*hidden argument*/&Vector3__ctor_m248_MethodInfo);
		return L_6;
	}
}
// System.String Mathf2::GetUnixTime()
extern MethodInfo Mathf2_GetUnixTime_m49_MethodInfo;
 String_t* Mathf2_GetUnixTime_m49 (Object_t * __this/* static, unused */, MethodInfo* method){
	DateTime_t110  V_0 = {0};
	double V_1 = 0.0;
	TimeSpan_t113  V_2 = {0};
	{
		DateTime__ctor_m294((&V_0), ((int32_t)1970), 1, 1, 8, 0, 0, 1, /*hidden argument*/&DateTime__ctor_m294_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&DateTime_t110_il2cpp_TypeInfo));
		DateTime_t110  L_0 = DateTime_get_UtcNow_m295(NULL /*static, unused*/, /*hidden argument*/&DateTime_get_UtcNow_m295_MethodInfo);
		TimeSpan_t113  L_1 = DateTime_op_Subtraction_m296(NULL /*static, unused*/, L_0, V_0, /*hidden argument*/&DateTime_op_Subtraction_m296_MethodInfo);
		V_2 = L_1;
		double L_2 = TimeSpan_get_TotalSeconds_m297((&V_2), /*hidden argument*/&TimeSpan_get_TotalSeconds_m297_MethodInfo);
		V_1 = L_2;
		String_t* L_3 = Double_ToString_m298((&V_1), /*hidden argument*/&Double_ToString_m298_MethodInfo);
		return L_3;
	}
}
// System.String Mathf2::GetPositionString(UnityEngine.Transform)
extern MethodInfo Mathf2_GetPositionString_m50_MethodInfo;
 String_t* Mathf2_GetPositionString_m50 (Object_t * __this/* static, unused */, Transform_t10 * ___t, MethodInfo* method){
	Vector3_t13  V_0 = {0};
	{
		NullCheck(___t);
		Vector3_t13  L_0 = Transform_get_position_m299(___t, /*hidden argument*/&Transform_get_position_m299_MethodInfo);
		V_0 = L_0;
		String_t* L_1 = Vector3_ToString_m300((&V_0), (String_t*) &_stringLiteral10, /*hidden argument*/&Vector3_ToString_m300_MethodInfo);
		return L_1;
	}
}
// System.String Mathf2::GetRotationString(UnityEngine.Transform)
extern MethodInfo Mathf2_GetRotationString_m51_MethodInfo;
 String_t* Mathf2_GetRotationString_m51 (Object_t * __this/* static, unused */, Transform_t10 * ___t, MethodInfo* method){
	Quaternion_t12  V_0 = {0};
	{
		NullCheck(___t);
		Quaternion_t12  L_0 = Transform_get_rotation_m301(___t, /*hidden argument*/&Transform_get_rotation_m301_MethodInfo);
		V_0 = L_0;
		String_t* L_1 = Quaternion_ToString_m302((&V_0), (String_t*) &_stringLiteral11, /*hidden argument*/&Quaternion_ToString_m302_MethodInfo);
		return L_1;
	}
}
// System.String Mathf2::GetScaleString(UnityEngine.Transform)
extern MethodInfo Mathf2_GetScaleString_m52_MethodInfo;
 String_t* Mathf2_GetScaleString_m52 (Object_t * __this/* static, unused */, Transform_t10 * ___t, MethodInfo* method){
	Vector3_t13  V_0 = {0};
	{
		NullCheck(___t);
		Vector3_t13  L_0 = Transform_get_localScale_m303(___t, /*hidden argument*/&Transform_get_localScale_m303_MethodInfo);
		V_0 = L_0;
		String_t* L_1 = Vector3_ToString_m300((&V_0), (String_t*) &_stringLiteral12, /*hidden argument*/&Vector3_ToString_m300_MethodInfo);
		return L_1;
	}
}
// Metadata Definition Mathf2
extern Il2CppType Vector3_t13_0_0_22;
FieldInfo Mathf2_t15____FarFarAway_0_FieldInfo = 
{
	"FarFarAway"/* name */
	, &Vector3_t13_0_0_22/* type */
	, &Mathf2_t15_il2cpp_TypeInfo/* parent */
	, offsetof(Mathf2_t15_StaticFields, ___FarFarAway_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Mathf2_t15_FieldInfos[] =
{
	&Mathf2_t15____FarFarAway_0_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Mathf2::.ctor()
MethodInfo Mathf2__ctor_m26_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Mathf2__ctor_m26/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 27/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Mathf2::.cctor()
MethodInfo Mathf2__cctor_m27_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&Mathf2__cctor_m27/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 28/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RaycastHit_t16_0_0_0;
extern void* RuntimeInvoker_RaycastHit_t16 (MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreenIgnore0()
MethodInfo Mathf2_WhatDidWeHitCenterScreenIgnore0_m28_MethodInfo = 
{
	"WhatDidWeHitCenterScreenIgnore0"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHitCenterScreenIgnore0_m28/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t16_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t16/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 29/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType LayerMask_t17_0_0_0;
extern Il2CppType LayerMask_t17_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_WhatDidWeHitCenterScreen_m29_ParameterInfos[] = 
{
	{"lm", 0, 134217733, &EmptyCustomAttributesCache, &LayerMask_t17_0_0_0},
};
extern Il2CppType RaycastHit_t16_0_0_0;
extern void* RuntimeInvoker_RaycastHit_t16_LayerMask_t17 (MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen(UnityEngine.LayerMask)
MethodInfo Mathf2_WhatDidWeHitCenterScreen_m29_MethodInfo = 
{
	"WhatDidWeHitCenterScreen"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHitCenterScreen_m29/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t16_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t16_LayerMask_t17/* invoker_method */
	, Mathf2_t15_Mathf2_WhatDidWeHitCenterScreen_m29_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 30/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType RaycastHit_t16_0_0_0;
extern void* RuntimeInvoker_RaycastHit_t16 (MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen()
MethodInfo Mathf2_WhatDidWeHitCenterScreen_m30_MethodInfo = 
{
	"WhatDidWeHitCenterScreen"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHitCenterScreen_m30/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t16_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t16/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 31/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t9_0_0_0;
extern Il2CppType Vector2_t9_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_WhatDidWeHit_m31_ParameterInfos[] = 
{
	{"v", 0, 134217734, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
};
extern Il2CppType RaycastHit_t16_0_0_0;
extern void* RuntimeInvoker_RaycastHit_t16_Vector2_t9 (MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2)
MethodInfo Mathf2_WhatDidWeHit_m31_MethodInfo = 
{
	"WhatDidWeHit"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHit_m31/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t16_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t16_Vector2_t9/* invoker_method */
	, Mathf2_t15_Mathf2_WhatDidWeHit_m31_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 32/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t9_0_0_0;
extern Il2CppType LayerMask_t17_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_WhatDidWeHit_m32_ParameterInfos[] = 
{
	{"v", 0, 134217735, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
	{"lm", 1, 134217736, &EmptyCustomAttributesCache, &LayerMask_t17_0_0_0},
};
extern Il2CppType RaycastHit_t16_0_0_0;
extern void* RuntimeInvoker_RaycastHit_t16_Vector2_t9_LayerMask_t17 (MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2,UnityEngine.LayerMask)
MethodInfo Mathf2_WhatDidWeHit_m32_MethodInfo = 
{
	"WhatDidWeHit"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHit_m32/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t16_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t16_Vector2_t9_LayerMask_t17/* invoker_method */
	, Mathf2_t15_Mathf2_WhatDidWeHit_m32_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 33/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Ray_t18_0_0_0;
extern Il2CppType Ray_t18_0_0_0;
extern Il2CppType LayerMask_t17_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_WhatDidWeHit_m33_ParameterInfos[] = 
{
	{"ray", 0, 134217737, &EmptyCustomAttributesCache, &Ray_t18_0_0_0},
	{"lm", 1, 134217738, &EmptyCustomAttributesCache, &LayerMask_t17_0_0_0},
};
extern Il2CppType RaycastHit_t16_0_0_0;
extern void* RuntimeInvoker_RaycastHit_t16_Ray_t18_LayerMask_t17 (MethodInfo* method, void* obj, void** args);
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Ray,UnityEngine.LayerMask)
MethodInfo Mathf2_WhatDidWeHit_m33_MethodInfo = 
{
	"WhatDidWeHit"/* name */
	, (methodPointerType)&Mathf2_WhatDidWeHit_m33/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &RaycastHit_t16_0_0_0/* return_type */
	, RuntimeInvoker_RaycastHit_t16_Ray_t18_LayerMask_t17/* invoker_method */
	, Mathf2_t15_Mathf2_WhatDidWeHit_m33_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 34/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_Pad0s_m34_ParameterInfos[] = 
{
	{"n", 0, 134217739, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.String Mathf2::Pad0s(System.Int32)
MethodInfo Mathf2_Pad0s_m34_MethodInfo = 
{
	"Pad0s"/* name */
	, (methodPointerType)&Mathf2_Pad0s_m34/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, Mathf2_t15_Mathf2_Pad0s_m34_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 35/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t105_0_0_0;
extern Il2CppType Single_t105_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_Round10th_m35_ParameterInfos[] = 
{
	{"n", 0, 134217740, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
};
extern Il2CppType Single_t105_0_0_0;
extern void* RuntimeInvoker_Single_t105_Single_t105 (MethodInfo* method, void* obj, void** args);
// System.Single Mathf2::Round10th(System.Single)
MethodInfo Mathf2_Round10th_m35_MethodInfo = 
{
	"Round10th"/* name */
	, (methodPointerType)&Mathf2_Round10th_m35/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Single_t105_0_0_0/* return_type */
	, RuntimeInvoker_Single_t105_Single_t105/* invoker_method */
	, Mathf2_t15_Mathf2_Round10th_m35_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 36/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GenUUID()
MethodInfo Mathf2_GenUUID_m36_MethodInfo = 
{
	"GenUUID"/* name */
	, (methodPointerType)&Mathf2_GenUUID_m36/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 37/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector3_t13_0_0_0;
extern Il2CppType Vector3_t13_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_RoundVector3_m37_ParameterInfos[] = 
{
	{"v", 0, 134217741, &EmptyCustomAttributesCache, &Vector3_t13_0_0_0},
};
extern Il2CppType Vector3_t13_0_0_0;
extern void* RuntimeInvoker_Vector3_t13_Vector3_t13 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 Mathf2::RoundVector3(UnityEngine.Vector3)
MethodInfo Mathf2_RoundVector3_m37_MethodInfo = 
{
	"RoundVector3"/* name */
	, (methodPointerType)&Mathf2_RoundVector3_m37/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t13_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t13_Vector3_t13/* invoker_method */
	, Mathf2_t15_Mathf2_RoundVector3_m37_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 38/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Quaternion_t12_0_0_0;
extern void* RuntimeInvoker_Quaternion_t12 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion Mathf2::RandRot()
MethodInfo Mathf2_RandRot_m38_MethodInfo = 
{
	"RandRot"/* name */
	, (methodPointerType)&Mathf2_RandRot_m38/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t12_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t12/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 39/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t105_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_round_m39_ParameterInfos[] = 
{
	{"f", 0, 134217742, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
};
extern Il2CppType Single_t105_0_0_0;
extern void* RuntimeInvoker_Single_t105_Single_t105 (MethodInfo* method, void* obj, void** args);
// System.Single Mathf2::round(System.Single)
MethodInfo Mathf2_round_m39_MethodInfo = 
{
	"round"/* name */
	, (methodPointerType)&Mathf2_round_m39/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Single_t105_0_0_0/* return_type */
	, RuntimeInvoker_Single_t105_Single_t105/* invoker_method */
	, Mathf2_t15_Mathf2_round_m39_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 40/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_String2Float_m40_ParameterInfos[] = 
{
	{"str", 0, 134217743, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Single_t105_0_0_0;
extern void* RuntimeInvoker_Single_t105_Object_t (MethodInfo* method, void* obj, void** args);
// System.Single Mathf2::String2Float(System.String)
MethodInfo Mathf2_String2Float_m40_MethodInfo = 
{
	"String2Float"/* name */
	, (methodPointerType)&Mathf2_String2Float_m40/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Single_t105_0_0_0/* return_type */
	, RuntimeInvoker_Single_t105_Object_t/* invoker_method */
	, Mathf2_t15_Mathf2_String2Float_m40_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 41/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_isNumeric_m41_ParameterInfos[] = 
{
	{"str", 0, 134217744, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Mathf2::isNumeric(System.String)
MethodInfo Mathf2_isNumeric_m41_MethodInfo = 
{
	"isNumeric"/* name */
	, (methodPointerType)&Mathf2_isNumeric_m41/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, Mathf2_t15_Mathf2_isNumeric_m41_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 42/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_String2Int_m42_ParameterInfos[] = 
{
	{"str", 0, 134217745, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
// System.Int32 Mathf2::String2Int(System.String)
MethodInfo Mathf2_String2Int_m42_MethodInfo = 
{
	"String2Int"/* name */
	, (methodPointerType)&Mathf2_String2Int_m42/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, Mathf2_t15_Mathf2_String2Int_m42_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 43/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_String2Color_m43_ParameterInfos[] = 
{
	{"str", 0, 134217746, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Color_t19_0_0_0;
extern void* RuntimeInvoker_Color_t19_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Color Mathf2::String2Color(System.String)
MethodInfo Mathf2_String2Color_m43_MethodInfo = 
{
	"String2Color"/* name */
	, (methodPointerType)&Mathf2_String2Color_m43/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Color_t19_0_0_0/* return_type */
	, RuntimeInvoker_Color_t19_Object_t/* invoker_method */
	, Mathf2_t15_Mathf2_String2Color_m43_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 44/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_String2Vector3_m44_ParameterInfos[] = 
{
	{"str", 0, 134217747, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Vector3_t13_0_0_0;
extern void* RuntimeInvoker_Vector3_t13_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 Mathf2::String2Vector3(System.String)
MethodInfo Mathf2_String2Vector3_m44_MethodInfo = 
{
	"String2Vector3"/* name */
	, (methodPointerType)&Mathf2_String2Vector3_m44/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t13_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t13_Object_t/* invoker_method */
	, Mathf2_t15_Mathf2_String2Vector3_m44_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 45/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_String2Vector2_m45_ParameterInfos[] = 
{
	{"str", 0, 134217748, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Vector2_t9_0_0_0;
extern void* RuntimeInvoker_Vector2_t9_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Mathf2::String2Vector2(System.String)
MethodInfo Mathf2_String2Vector2_m45_MethodInfo = 
{
	"String2Vector2"/* name */
	, (methodPointerType)&Mathf2_String2Vector2_m45/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t9_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t9_Object_t/* invoker_method */
	, Mathf2_t15_Mathf2_String2Vector2_m45_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 46/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t105_0_0_0;
extern Il2CppType Single_t105_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_RoundFraction_m46_ParameterInfos[] = 
{
	{"val", 0, 134217749, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
	{"denominator", 1, 134217750, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
};
extern Il2CppType Single_t105_0_0_0;
extern void* RuntimeInvoker_Single_t105_Single_t105_Single_t105 (MethodInfo* method, void* obj, void** args);
// System.Single Mathf2::RoundFraction(System.Single,System.Single)
MethodInfo Mathf2_RoundFraction_m46_MethodInfo = 
{
	"RoundFraction"/* name */
	, (methodPointerType)&Mathf2_RoundFraction_m46/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Single_t105_0_0_0/* return_type */
	, RuntimeInvoker_Single_t105_Single_t105_Single_t105/* invoker_method */
	, Mathf2_t15_Mathf2_RoundFraction_m46_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 47/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_String2Quat_m47_ParameterInfos[] = 
{
	{"str", 0, 134217751, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Quaternion_t12_0_0_0;
extern void* RuntimeInvoker_Quaternion_t12_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Quaternion Mathf2::String2Quat(System.String)
MethodInfo Mathf2_String2Quat_m47_MethodInfo = 
{
	"String2Quat"/* name */
	, (methodPointerType)&Mathf2_String2Quat_m47/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Quaternion_t12_0_0_0/* return_type */
	, RuntimeInvoker_Quaternion_t12_Object_t/* invoker_method */
	, Mathf2_t15_Mathf2_String2Quat_m47_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 48/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector3_t13_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_UnsignedVector3_m48_ParameterInfos[] = 
{
	{"v", 0, 134217752, &EmptyCustomAttributesCache, &Vector3_t13_0_0_0},
};
extern Il2CppType Vector3_t13_0_0_0;
extern void* RuntimeInvoker_Vector3_t13_Vector3_t13 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 Mathf2::UnsignedVector3(UnityEngine.Vector3)
MethodInfo Mathf2_UnsignedVector3_m48_MethodInfo = 
{
	"UnsignedVector3"/* name */
	, (methodPointerType)&Mathf2_UnsignedVector3_m48/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t13_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t13_Vector3_t13/* invoker_method */
	, Mathf2_t15_Mathf2_UnsignedVector3_m48_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 49/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GetUnixTime()
MethodInfo Mathf2_GetUnixTime_m49_MethodInfo = 
{
	"GetUnixTime"/* name */
	, (methodPointerType)&Mathf2_GetUnixTime_m49/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 50/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Transform_t10_0_0_0;
extern Il2CppType Transform_t10_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_GetPositionString_m50_ParameterInfos[] = 
{
	{"t", 0, 134217753, &EmptyCustomAttributesCache, &Transform_t10_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GetPositionString(UnityEngine.Transform)
MethodInfo Mathf2_GetPositionString_m50_MethodInfo = 
{
	"GetPositionString"/* name */
	, (methodPointerType)&Mathf2_GetPositionString_m50/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Mathf2_t15_Mathf2_GetPositionString_m50_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 51/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Transform_t10_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_GetRotationString_m51_ParameterInfos[] = 
{
	{"t", 0, 134217754, &EmptyCustomAttributesCache, &Transform_t10_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GetRotationString(UnityEngine.Transform)
MethodInfo Mathf2_GetRotationString_m51_MethodInfo = 
{
	"GetRotationString"/* name */
	, (methodPointerType)&Mathf2_GetRotationString_m51/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Mathf2_t15_Mathf2_GetRotationString_m51_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 52/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Transform_t10_0_0_0;
static ParameterInfo Mathf2_t15_Mathf2_GetScaleString_m52_ParameterInfos[] = 
{
	{"t", 0, 134217755, &EmptyCustomAttributesCache, &Transform_t10_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Mathf2::GetScaleString(UnityEngine.Transform)
MethodInfo Mathf2_GetScaleString_m52_MethodInfo = 
{
	"GetScaleString"/* name */
	, (methodPointerType)&Mathf2_GetScaleString_m52/* method */
	, &Mathf2_t15_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Mathf2_t15_Mathf2_GetScaleString_m52_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 53/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Mathf2_t15_MethodInfos[] =
{
	&Mathf2__ctor_m26_MethodInfo,
	&Mathf2__cctor_m27_MethodInfo,
	&Mathf2_WhatDidWeHitCenterScreenIgnore0_m28_MethodInfo,
	&Mathf2_WhatDidWeHitCenterScreen_m29_MethodInfo,
	&Mathf2_WhatDidWeHitCenterScreen_m30_MethodInfo,
	&Mathf2_WhatDidWeHit_m31_MethodInfo,
	&Mathf2_WhatDidWeHit_m32_MethodInfo,
	&Mathf2_WhatDidWeHit_m33_MethodInfo,
	&Mathf2_Pad0s_m34_MethodInfo,
	&Mathf2_Round10th_m35_MethodInfo,
	&Mathf2_GenUUID_m36_MethodInfo,
	&Mathf2_RoundVector3_m37_MethodInfo,
	&Mathf2_RandRot_m38_MethodInfo,
	&Mathf2_round_m39_MethodInfo,
	&Mathf2_String2Float_m40_MethodInfo,
	&Mathf2_isNumeric_m41_MethodInfo,
	&Mathf2_String2Int_m42_MethodInfo,
	&Mathf2_String2Color_m43_MethodInfo,
	&Mathf2_String2Vector3_m44_MethodInfo,
	&Mathf2_String2Vector2_m45_MethodInfo,
	&Mathf2_RoundFraction_m46_MethodInfo,
	&Mathf2_String2Quat_m47_MethodInfo,
	&Mathf2_UnsignedVector3_m48_MethodInfo,
	&Mathf2_GetUnixTime_m49_MethodInfo,
	&Mathf2_GetPositionString_m50_MethodInfo,
	&Mathf2_GetRotationString_m51_MethodInfo,
	&Mathf2_GetScaleString_m52_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m304_MethodInfo;
extern MethodInfo Object_GetHashCode_m305_MethodInfo;
extern MethodInfo Object_ToString_m306_MethodInfo;
static MethodInfo* Mathf2_t15_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType Mathf2_t15_0_0_0;
extern Il2CppType Mathf2_t15_1_0_0;
extern TypeInfo Object_t_il2cpp_TypeInfo;
struct Mathf2_t15;
TypeInfo Mathf2_t15_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Mathf2"/* name */
	, ""/* namespaze */
	, Mathf2_t15_MethodInfos/* methods */
	, NULL/* properties */
	, Mathf2_t15_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Mathf2_t15_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Mathf2_t15_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Mathf2_t15_il2cpp_TypeInfo/* cast_class */
	, &Mathf2_t15_0_0_0/* byval_arg */
	, &Mathf2_t15_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Mathf2_t15)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(Mathf2_t15_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 27/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Raycaster
#include "AssemblyU2DCSharp_Raycaster.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Raycaster_t20_il2cpp_TypeInfo;
// Raycaster
#include "AssemblyU2DCSharp_RaycasterMethodDeclarations.h"

extern MethodInfo Physics_Raycast_m307_MethodInfo;


// System.Void Raycaster::.ctor()
extern MethodInfo Raycaster__ctor_m53_MethodInfo;
 void Raycaster__ctor_m53 (Raycaster_t20 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void Raycaster::RaycastIt(UnityEngine.Camera,UnityEngine.Vector2,System.Single)
extern MethodInfo Raycaster_RaycastIt_m54_MethodInfo;
 void Raycaster_RaycastIt_m54 (Raycaster_t20 * __this, Camera_t3 * ___cam, Vector2_t9  ___pos, float ___distance, MethodInfo* method){
	Ray_t18  V_0 = {0};
	RaycastHit_t16  V_1 = {0};
	String_t* V_2 = {0};
	{
		Vector3_t13  L_0 = Vector2_op_Implicit_m276(NULL /*static, unused*/, ___pos, /*hidden argument*/&Vector2_op_Implicit_m276_MethodInfo);
		NullCheck(___cam);
		Ray_t18  L_1 = Camera_ScreenPointToRay_m277(___cam, L_0, /*hidden argument*/&Camera_ScreenPointToRay_m277_MethodInfo);
		V_0 = L_1;
		bool L_2 = Physics_Raycast_m307(NULL /*static, unused*/, V_0, (&V_1), ___distance, /*hidden argument*/&Physics_Raycast_m307_MethodInfo);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		Transform_t10 * L_3 = RaycastHit_get_transform_m225((&V_1), /*hidden argument*/&RaycastHit_get_transform_m225_MethodInfo);
		NullCheck(L_3);
		String_t* L_4 = Object_get_name_m226(L_3, /*hidden argument*/&Object_get_name_m226_MethodInfo);
		V_2 = L_4;
	}

IL_0028:
	{
		return;
	}
}
// Metadata Definition Raycaster
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Raycaster::.ctor()
MethodInfo Raycaster__ctor_m53_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Raycaster__ctor_m53/* method */
	, &Raycaster_t20_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 54/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Camera_t3_0_0_0;
extern Il2CppType Camera_t3_0_0_0;
extern Il2CppType Vector2_t9_0_0_0;
extern Il2CppType Single_t105_0_0_0;
static ParameterInfo Raycaster_t20_Raycaster_RaycastIt_m54_ParameterInfos[] = 
{
	{"cam", 0, 134217756, &EmptyCustomAttributesCache, &Camera_t3_0_0_0},
	{"pos", 1, 134217757, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
	{"distance", 2, 134217758, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Vector2_t9_Single_t105 (MethodInfo* method, void* obj, void** args);
// System.Void Raycaster::RaycastIt(UnityEngine.Camera,UnityEngine.Vector2,System.Single)
MethodInfo Raycaster_RaycastIt_m54_MethodInfo = 
{
	"RaycastIt"/* name */
	, (methodPointerType)&Raycaster_RaycastIt_m54/* method */
	, &Raycaster_t20_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Vector2_t9_Single_t105/* invoker_method */
	, Raycaster_t20_Raycaster_RaycastIt_m54_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 55/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Raycaster_t20_MethodInfos[] =
{
	&Raycaster__ctor_m53_MethodInfo,
	&Raycaster_RaycastIt_m54_MethodInfo,
	NULL
};
static MethodInfo* Raycaster_t20_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType Raycaster_t20_0_0_0;
extern Il2CppType Raycaster_t20_1_0_0;
struct Raycaster_t20;
TypeInfo Raycaster_t20_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "Raycaster"/* name */
	, ""/* namespaze */
	, Raycaster_t20_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Raycaster_t20_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Raycaster_t20_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Raycaster_t20_il2cpp_TypeInfo/* cast_class */
	, &Raycaster_t20_0_0_0/* byval_arg */
	, &Raycaster_t20_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Raycaster_t20)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SetRenderQueue
#include "AssemblyU2DCSharp_SetRenderQueue.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SetRenderQueue_t22_il2cpp_TypeInfo;
// SetRenderQueue
#include "AssemblyU2DCSharp_SetRenderQueueMethodDeclarations.h"

#include "UnityEngine_ArrayTypes.h"
extern TypeInfo Int32U5BU5D_t21_il2cpp_TypeInfo;
extern MethodInfo Renderer_get_materials_m308_MethodInfo;
extern MethodInfo Material_set_renderQueue_m309_MethodInfo;


// System.Void SetRenderQueue::.ctor()
extern MethodInfo SetRenderQueue__ctor_m55_MethodInfo;
 void SetRenderQueue__ctor_m55 (SetRenderQueue_t22 * __this, MethodInfo* method){
	{
		Int32U5BU5D_t21* L_0 = ((Int32U5BU5D_t21*)SZArrayNew(InitializedTypeInfo(&Int32U5BU5D_t21_il2cpp_TypeInfo), 1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_0, 0)) = (int32_t)((int32_t)3000);
		__this->___m_queues_2 = L_0;
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void SetRenderQueue::Awake()
extern MethodInfo SetRenderQueue_Awake_m56_MethodInfo;
 void SetRenderQueue_Awake_m56 (SetRenderQueue_t22 * __this, MethodInfo* method){
	MaterialU5BU5D_t114* V_0 = {0};
	int32_t V_1 = 0;
	{
		Renderer_t7 * L_0 = Component_GetComponent_TisRenderer_t7_m234(__this, /*hidden argument*/&Component_GetComponent_TisRenderer_t7_m234_MethodInfo);
		NullCheck(L_0);
		MaterialU5BU5D_t114* L_1 = Renderer_get_materials_m308(L_0, /*hidden argument*/&Renderer_get_materials_m308_MethodInfo);
		V_0 = L_1;
		V_1 = 0;
		goto IL_0027;
	}

IL_0013:
	{
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, V_1);
		int32_t L_2 = V_1;
		Int32U5BU5D_t21* L_3 = (__this->___m_queues_2);
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, V_1);
		int32_t L_4 = V_1;
		NullCheck((*(Material_t4 **)(Material_t4 **)SZArrayLdElema(V_0, L_2)));
		Material_set_renderQueue_m309((*(Material_t4 **)(Material_t4 **)SZArrayLdElema(V_0, L_2)), (*(int32_t*)(int32_t*)SZArrayLdElema(L_3, L_4)), /*hidden argument*/&Material_set_renderQueue_m309_MethodInfo);
		V_1 = ((int32_t)(V_1+1));
	}

IL_0027:
	{
		NullCheck(V_0);
		if ((((int32_t)V_1) >= ((int32_t)(((int32_t)(((Array_t *)V_0)->max_length))))))
		{
			goto IL_003e;
		}
	}
	{
		Int32U5BU5D_t21* L_5 = (__this->___m_queues_2);
		NullCheck(L_5);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((Array_t *)L_5)->max_length))))))
		{
			goto IL_0013;
		}
	}

IL_003e:
	{
		return;
	}
}
// Metadata Definition SetRenderQueue
extern Il2CppType Int32U5BU5D_t21_0_0_4;
extern CustomAttributesCache SetRenderQueue_t22__CustomAttributeCache_m_queues;
FieldInfo SetRenderQueue_t22____m_queues_2_FieldInfo = 
{
	"m_queues"/* name */
	, &Int32U5BU5D_t21_0_0_4/* type */
	, &SetRenderQueue_t22_il2cpp_TypeInfo/* parent */
	, offsetof(SetRenderQueue_t22, ___m_queues_2)/* data */
	, &SetRenderQueue_t22__CustomAttributeCache_m_queues/* custom_attributes_cache */

};
static FieldInfo* SetRenderQueue_t22_FieldInfos[] =
{
	&SetRenderQueue_t22____m_queues_2_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueue::.ctor()
MethodInfo SetRenderQueue__ctor_m55_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetRenderQueue__ctor_m55/* method */
	, &SetRenderQueue_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 56/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueue::Awake()
MethodInfo SetRenderQueue_Awake_m56_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SetRenderQueue_Awake_m56/* method */
	, &SetRenderQueue_t22_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 57/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SetRenderQueue_t22_MethodInfos[] =
{
	&SetRenderQueue__ctor_m55_MethodInfo,
	&SetRenderQueue_Awake_m56_MethodInfo,
	NULL
};
static MethodInfo* SetRenderQueue_t22_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
void SetRenderQueue_t22_CustomAttributesCacheGenerator_m_queues(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache SetRenderQueue_t22__CustomAttributeCache_m_queues = {
1,
NULL,
&SetRenderQueue_t22_CustomAttributesCacheGenerator_m_queues
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType SetRenderQueue_t22_0_0_0;
extern Il2CppType SetRenderQueue_t22_1_0_0;
struct SetRenderQueue_t22;
extern CustomAttributesCache SetRenderQueue_t22__CustomAttributeCache_m_queues;
TypeInfo SetRenderQueue_t22_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetRenderQueue"/* name */
	, ""/* namespaze */
	, SetRenderQueue_t22_MethodInfos/* methods */
	, NULL/* properties */
	, SetRenderQueue_t22_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SetRenderQueue_t22_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SetRenderQueue_t22_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SetRenderQueue_t22_il2cpp_TypeInfo/* cast_class */
	, &SetRenderQueue_t22_0_0_0/* byval_arg */
	, &SetRenderQueue_t22_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetRenderQueue_t22)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// SetRenderQueueChildren
#include "AssemblyU2DCSharp_SetRenderQueueChildren.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SetRenderQueueChildren_t23_il2cpp_TypeInfo;
// SetRenderQueueChildren
#include "AssemblyU2DCSharp_SetRenderQueueChildrenMethodDeclarations.h"

extern MethodInfo Component_GetComponentsInChildren_TisRenderer_t7_m310_MethodInfo;
struct Component_t100;
struct Component_t100;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
 ObjectU5BU5D_t115* Component_GetComponentsInChildren_TisObject_t_m311_gshared (Component_t100 * __this, bool p0, MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m311(__this, p0, method) (ObjectU5BU5D_t115*)Component_GetComponentsInChildren_TisObject_t_m311_gshared((Component_t100 *)__this, (bool)p0, method)
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Renderer>(System.Boolean)
#define Component_GetComponentsInChildren_TisRenderer_t7_m310(__this, p0, method) (RendererU5BU5D_t116*)Component_GetComponentsInChildren_TisObject_t_m311_gshared((Component_t100 *)__this, (bool)p0, method)


// System.Void SetRenderQueueChildren::.ctor()
extern MethodInfo SetRenderQueueChildren__ctor_m57_MethodInfo;
 void SetRenderQueueChildren__ctor_m57 (SetRenderQueueChildren_t23 * __this, MethodInfo* method){
	{
		Int32U5BU5D_t21* L_0 = ((Int32U5BU5D_t21*)SZArrayNew(InitializedTypeInfo(&Int32U5BU5D_t21_il2cpp_TypeInfo), 1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((int32_t*)(int32_t*)SZArrayLdElema(L_0, 0)) = (int32_t)((int32_t)3000);
		__this->___m_queues_2 = L_0;
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void SetRenderQueueChildren::Start()
extern MethodInfo SetRenderQueueChildren_Start_m58_MethodInfo;
 void SetRenderQueueChildren_Start_m58 (SetRenderQueueChildren_t23 * __this, MethodInfo* method){
	{
		Transform_t10 * L_0 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m226(L_0, /*hidden argument*/&Object_get_name_m226_MethodInfo);
		MonoBehaviour_print_m228(NULL /*static, unused*/, L_1, /*hidden argument*/&MonoBehaviour_print_m228_MethodInfo);
		return;
	}
}
// System.Void SetRenderQueueChildren::Awake()
extern MethodInfo SetRenderQueueChildren_Awake_m59_MethodInfo;
 void SetRenderQueueChildren_Awake_m59 (SetRenderQueueChildren_t23 * __this, MethodInfo* method){
	RendererU5BU5D_t116* V_0 = {0};
	int32_t V_1 = 0;
	MaterialU5BU5D_t114* V_2 = {0};
	int32_t V_3 = 0;
	{
		RendererU5BU5D_t116* L_0 = Component_GetComponentsInChildren_TisRenderer_t7_m310(__this, 1, /*hidden argument*/&Component_GetComponentsInChildren_TisRenderer_t7_m310_MethodInfo);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003d;
	}

IL_000f:
	{
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, V_1);
		int32_t L_1 = V_1;
		NullCheck((*(Renderer_t7 **)(Renderer_t7 **)SZArrayLdElema(V_0, L_1)));
		MaterialU5BU5D_t114* L_2 = Renderer_get_materials_m308((*(Renderer_t7 **)(Renderer_t7 **)SZArrayLdElema(V_0, L_1)), /*hidden argument*/&Renderer_get_materials_m308_MethodInfo);
		V_2 = L_2;
		V_3 = 0;
		goto IL_0030;
	}

IL_001f:
	{
		NullCheck(V_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_2, V_3);
		int32_t L_3 = V_3;
		NullCheck((*(Material_t4 **)(Material_t4 **)SZArrayLdElema(V_2, L_3)));
		Material_set_renderQueue_m309((*(Material_t4 **)(Material_t4 **)SZArrayLdElema(V_2, L_3)), ((int32_t)3020), /*hidden argument*/&Material_set_renderQueue_m309_MethodInfo);
		V_3 = ((int32_t)(V_3+1));
	}

IL_0030:
	{
		NullCheck(V_2);
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((Array_t *)V_2)->max_length))))))
		{
			goto IL_001f;
		}
	}
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_003d:
	{
		NullCheck(V_0);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((Array_t *)V_0)->max_length))))))
		{
			goto IL_000f;
		}
	}
	{
		return;
	}
}
// Metadata Definition SetRenderQueueChildren
extern Il2CppType Int32U5BU5D_t21_0_0_4;
extern CustomAttributesCache SetRenderQueueChildren_t23__CustomAttributeCache_m_queues;
FieldInfo SetRenderQueueChildren_t23____m_queues_2_FieldInfo = 
{
	"m_queues"/* name */
	, &Int32U5BU5D_t21_0_0_4/* type */
	, &SetRenderQueueChildren_t23_il2cpp_TypeInfo/* parent */
	, offsetof(SetRenderQueueChildren_t23, ___m_queues_2)/* data */
	, &SetRenderQueueChildren_t23__CustomAttributeCache_m_queues/* custom_attributes_cache */

};
static FieldInfo* SetRenderQueueChildren_t23_FieldInfos[] =
{
	&SetRenderQueueChildren_t23____m_queues_2_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueueChildren::.ctor()
MethodInfo SetRenderQueueChildren__ctor_m57_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SetRenderQueueChildren__ctor_m57/* method */
	, &SetRenderQueueChildren_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 58/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueueChildren::Start()
MethodInfo SetRenderQueueChildren_Start_m58_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&SetRenderQueueChildren_Start_m58/* method */
	, &SetRenderQueueChildren_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 59/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void SetRenderQueueChildren::Awake()
MethodInfo SetRenderQueueChildren_Awake_m59_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&SetRenderQueueChildren_Awake_m59/* method */
	, &SetRenderQueueChildren_t23_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 60/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SetRenderQueueChildren_t23_MethodInfos[] =
{
	&SetRenderQueueChildren__ctor_m57_MethodInfo,
	&SetRenderQueueChildren_Start_m58_MethodInfo,
	&SetRenderQueueChildren_Awake_m59_MethodInfo,
	NULL
};
static MethodInfo* SetRenderQueueChildren_t23_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
void SetRenderQueueChildren_t23_CustomAttributesCacheGenerator_m_queues(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache SetRenderQueueChildren_t23__CustomAttributeCache_m_queues = {
1,
NULL,
&SetRenderQueueChildren_t23_CustomAttributesCacheGenerator_m_queues
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType SetRenderQueueChildren_t23_0_0_0;
extern Il2CppType SetRenderQueueChildren_t23_1_0_0;
struct SetRenderQueueChildren_t23;
extern CustomAttributesCache SetRenderQueueChildren_t23__CustomAttributeCache_m_queues;
TypeInfo SetRenderQueueChildren_t23_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SetRenderQueueChildren"/* name */
	, ""/* namespaze */
	, SetRenderQueueChildren_t23_MethodInfos/* methods */
	, NULL/* properties */
	, SetRenderQueueChildren_t23_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SetRenderQueueChildren_t23_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SetRenderQueueChildren_t23_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SetRenderQueueChildren_t23_il2cpp_TypeInfo/* cast_class */
	, &SetRenderQueueChildren_t23_0_0_0/* byval_arg */
	, &SetRenderQueueChildren_t23_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SetRenderQueueChildren_t23)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// WorldLoop
#include "AssemblyU2DCSharp_WorldLoop.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WorldLoop_t24_il2cpp_TypeInfo;
// WorldLoop
#include "AssemblyU2DCSharp_WorldLoopMethodDeclarations.h"

extern TypeInfo WorldNav_t25_il2cpp_TypeInfo;
// WorldNav
#include "AssemblyU2DCSharp_WorldNavMethodDeclarations.h"
extern MethodInfo Component_GetComponent_TisRaycaster_t20_m312_MethodInfo;
extern MethodInfo WorldNav_Walk_m67_MethodInfo;
struct Component_t100;
// UnityEngine.CastHelper`1<Raycaster>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_2.h"
// Declaration !!0 UnityEngine.Component::GetComponent<Raycaster>()
// !!0 UnityEngine.Component::GetComponent<Raycaster>()
#define Component_GetComponent_TisRaycaster_t20_m312(__this, method) (Raycaster_t20 *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)


// System.Void WorldLoop::.ctor()
extern MethodInfo WorldLoop__ctor_m60_MethodInfo;
 void WorldLoop__ctor_m60 (WorldLoop_t24 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void WorldLoop::Awake()
extern MethodInfo WorldLoop_Awake_m61_MethodInfo;
 void WorldLoop_Awake_m61 (WorldLoop_t24 * __this, MethodInfo* method){
	{
		Raycaster_t20 * L_0 = Component_GetComponent_TisRaycaster_t20_m312(__this, /*hidden argument*/&Component_GetComponent_TisRaycaster_t20_m312_MethodInfo);
		__this->___raycaster_3 = L_0;
		return;
	}
}
// System.Void WorldLoop::Start()
extern MethodInfo WorldLoop_Start_m62_MethodInfo;
 void WorldLoop_Start_m62 (WorldLoop_t24 * __this, MethodInfo* method){
	{
		return;
	}
}
// System.Void WorldLoop::Update()
extern MethodInfo WorldLoop_Update_m63_MethodInfo;
 void WorldLoop_Update_m63 (WorldLoop_t24 * __this, MethodInfo* method){
	Touch_t104  V_0 = {0};
	Vector2_t9  V_1 = {0};
	Touch_t104  V_2 = {0};
	Vector2_t9  V_3 = {0};
	Touch_t104  V_4 = {0};
	Touch_t104  V_5 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		int32_t L_0 = Input_get_touchCount_m260(NULL /*static, unused*/, /*hidden argument*/&Input_get_touchCount_m260_MethodInfo);
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_00a3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_1 = Input_GetTouch_m261(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_0 = L_1;
		Vector2_t9  L_2 = Touch_get_position_m263((&V_0), /*hidden argument*/&Touch_get_position_m263_MethodInfo);
		V_1 = L_2;
		NullCheck((&V_1));
		float L_3 = ((&V_1)->___y_2);
		int32_t L_4 = Screen_get_height_m274(NULL /*static, unused*/, /*hidden argument*/&Screen_get_height_m274_MethodInfo);
		if ((((float)L_3) <= ((float)((float)((float)(((float)L_4))*(float)(0.5f))))))
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo));
		WorldNav_Walk_m67(NULL /*static, unused*/, 1, /*hidden argument*/&WorldNav_Walk_m67_MethodInfo);
		goto IL_006a;
	}

IL_003d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_5 = Input_GetTouch_m261(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_2 = L_5;
		Vector2_t9  L_6 = Touch_get_position_m263((&V_2), /*hidden argument*/&Touch_get_position_m263_MethodInfo);
		V_3 = L_6;
		NullCheck((&V_3));
		float L_7 = ((&V_3)->___y_2);
		int32_t L_8 = Screen_get_height_m274(NULL /*static, unused*/, /*hidden argument*/&Screen_get_height_m274_MethodInfo);
		if ((((float)L_7) >= ((float)((float)((float)(((float)L_8))*(float)(0.5f))))))
		{
			goto IL_006a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo));
		WorldNav_Walk_m67(NULL /*static, unused*/, (-1), /*hidden argument*/&WorldNav_Walk_m67_MethodInfo);
	}

IL_006a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_9 = Input_GetTouch_m261(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_4 = L_9;
		int32_t L_10 = Touch_get_phase_m262((&V_4), /*hidden argument*/&Touch_get_phase_m262_MethodInfo);
		if (L_10)
		{
			goto IL_00a3;
		}
	}
	{
		Raycaster_t20 * L_11 = (__this->___raycaster_3);
		Camera_t3 * L_12 = (__this->___cam_2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		Touch_t104  L_13 = Input_GetTouch_m261(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetTouch_m261_MethodInfo);
		V_5 = L_13;
		Vector2_t9  L_14 = Touch_get_position_m263((&V_5), /*hidden argument*/&Touch_get_position_m263_MethodInfo);
		NullCheck(L_11);
		Raycaster_RaycastIt_m54(L_11, L_12, L_14, (100.0f), /*hidden argument*/&Raycaster_RaycastIt_m54_MethodInfo);
	}

IL_00a3:
	{
		return;
	}
}
// Metadata Definition WorldLoop
extern Il2CppType Camera_t3_0_0_6;
FieldInfo WorldLoop_t24____cam_2_FieldInfo = 
{
	"cam"/* name */
	, &Camera_t3_0_0_6/* type */
	, &WorldLoop_t24_il2cpp_TypeInfo/* parent */
	, offsetof(WorldLoop_t24, ___cam_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Raycaster_t20_0_0_1;
FieldInfo WorldLoop_t24____raycaster_3_FieldInfo = 
{
	"raycaster"/* name */
	, &Raycaster_t20_0_0_1/* type */
	, &WorldLoop_t24_il2cpp_TypeInfo/* parent */
	, offsetof(WorldLoop_t24, ___raycaster_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* WorldLoop_t24_FieldInfos[] =
{
	&WorldLoop_t24____cam_2_FieldInfo,
	&WorldLoop_t24____raycaster_3_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void WorldLoop::.ctor()
MethodInfo WorldLoop__ctor_m60_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WorldLoop__ctor_m60/* method */
	, &WorldLoop_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 61/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void WorldLoop::Awake()
MethodInfo WorldLoop_Awake_m61_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&WorldLoop_Awake_m61/* method */
	, &WorldLoop_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 62/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void WorldLoop::Start()
MethodInfo WorldLoop_Start_m62_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&WorldLoop_Start_m62/* method */
	, &WorldLoop_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 63/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void WorldLoop::Update()
MethodInfo WorldLoop_Update_m63_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&WorldLoop_Update_m63/* method */
	, &WorldLoop_t24_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 64/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* WorldLoop_t24_MethodInfos[] =
{
	&WorldLoop__ctor_m60_MethodInfo,
	&WorldLoop_Awake_m61_MethodInfo,
	&WorldLoop_Start_m62_MethodInfo,
	&WorldLoop_Update_m63_MethodInfo,
	NULL
};
static MethodInfo* WorldLoop_t24_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType WorldLoop_t24_0_0_0;
extern Il2CppType WorldLoop_t24_1_0_0;
struct WorldLoop_t24;
TypeInfo WorldLoop_t24_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WorldLoop"/* name */
	, ""/* namespaze */
	, WorldLoop_t24_MethodInfos/* methods */
	, NULL/* properties */
	, WorldLoop_t24_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WorldLoop_t24_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, WorldLoop_t24_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WorldLoop_t24_il2cpp_TypeInfo/* cast_class */
	, &WorldLoop_t24_0_0_0/* byval_arg */
	, &WorldLoop_t24_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WorldLoop_t24)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// WorldNav
#include "AssemblyU2DCSharp_WorldNav.h"
#ifndef _MSC_VER
#else
#endif

extern MethodInfo GameObject_get_transform_m313_MethodInfo;
extern MethodInfo Transform_Find_m314_MethodInfo;
extern MethodInfo Component_GetComponent_TisCamera_t3_m315_MethodInfo;
extern MethodInfo Transform_get_forward_m316_MethodInfo;
extern MethodInfo Vector3_op_Multiply_m317_MethodInfo;
extern MethodInfo Vector3_op_Addition_m318_MethodInfo;
extern MethodInfo Transform_set_position_m319_MethodInfo;
extern MethodInfo Vector3_op_Subtraction_m320_MethodInfo;
struct Component_t100;
// UnityEngine.CastHelper`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_3.h"
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t3_m315(__this, method) (Camera_t3 *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)


// System.Void WorldNav::.ctor()
extern MethodInfo WorldNav__ctor_m64_MethodInfo;
 void WorldNav__ctor_m64 (WorldNav_t25 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void WorldNav::.cctor()
extern MethodInfo WorldNav__cctor_m65_MethodInfo;
 void WorldNav__cctor_m65 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		((WorldNav_t25_StaticFields*)InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo)->static_fields)->___coeff_2 = (0.1f);
		return;
	}
}
// System.Void WorldNav::Awake()
extern MethodInfo WorldNav_Awake_m66_MethodInfo;
 void WorldNav_Awake_m66 (WorldNav_t25 * __this, MethodInfo* method){
	{
		GameObject_t2 * L_0 = (__this->___mainCharacter_3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo));
		((WorldNav_t25_StaticFields*)InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo)->static_fields)->___goMainCharacter_4 = L_0;
		NullCheck((((WorldNav_t25_StaticFields*)InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo)->static_fields)->___goMainCharacter_4));
		Transform_t10 * L_1 = GameObject_get_transform_m313((((WorldNav_t25_StaticFields*)InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo)->static_fields)->___goMainCharacter_4), /*hidden argument*/&GameObject_get_transform_m313_MethodInfo);
		NullCheck(L_1);
		Transform_t10 * L_2 = Transform_Find_m314(L_1, (String_t*) &_stringLiteral13, /*hidden argument*/&Transform_Find_m314_MethodInfo);
		NullCheck(L_2);
		Camera_t3 * L_3 = Component_GetComponent_TisCamera_t3_m315(L_2, /*hidden argument*/&Component_GetComponent_TisCamera_t3_m315_MethodInfo);
		((WorldNav_t25_StaticFields*)InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo)->static_fields)->___camCharacter_5 = L_3;
		return;
	}
}
// System.Void WorldNav::Walk(System.Int32)
 void WorldNav_Walk_m67 (Object_t * __this/* static, unused */, int32_t ___dir, MethodInfo* method){
	Vector3_t13  V_0 = {0};
	Vector3_t13  V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo));
		NullCheck((((WorldNav_t25_StaticFields*)InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo)->static_fields)->___camCharacter_5));
		Transform_t10 * L_0 = Component_get_transform_m219((((WorldNav_t25_StaticFields*)InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo)->static_fields)->___camCharacter_5), /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_0);
		Vector3_t13  L_1 = Transform_get_forward_m316(L_0, /*hidden argument*/&Transform_get_forward_m316_MethodInfo);
		V_0 = L_1;
		NullCheck((&V_0));
		float L_2 = ((&V_0)->___x_1);
		NullCheck((&V_0));
		float L_3 = ((&V_0)->___z_3);
		Vector3_t13  L_4 = {0};
		Vector3__ctor_m248(&L_4, L_2, (0.0f), L_3, /*hidden argument*/&Vector3__ctor_m248_MethodInfo);
		Vector3_t13  L_5 = Vector3_op_Multiply_m317(NULL /*static, unused*/, (((WorldNav_t25_StaticFields*)InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo)->static_fields)->___coeff_2), L_4, /*hidden argument*/&Vector3_op_Multiply_m317_MethodInfo);
		V_1 = L_5;
		if ((((int32_t)___dir) <= ((int32_t)0)))
		{
			goto IL_005a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo));
		NullCheck((((WorldNav_t25_StaticFields*)InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo)->static_fields)->___goMainCharacter_4));
		Transform_t10 * L_6 = GameObject_get_transform_m313((((WorldNav_t25_StaticFields*)InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo)->static_fields)->___goMainCharacter_4), /*hidden argument*/&GameObject_get_transform_m313_MethodInfo);
		Transform_t10 * L_7 = L_6;
		NullCheck(L_7);
		Vector3_t13  L_8 = Transform_get_position_m299(L_7, /*hidden argument*/&Transform_get_position_m299_MethodInfo);
		Vector3_t13  L_9 = Vector3_op_Addition_m318(NULL /*static, unused*/, L_8, V_1, /*hidden argument*/&Vector3_op_Addition_m318_MethodInfo);
		NullCheck(L_7);
		Transform_set_position_m319(L_7, L_9, /*hidden argument*/&Transform_set_position_m319_MethodInfo);
		goto IL_0075;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo));
		NullCheck((((WorldNav_t25_StaticFields*)InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo)->static_fields)->___goMainCharacter_4));
		Transform_t10 * L_10 = GameObject_get_transform_m313((((WorldNav_t25_StaticFields*)InitializedTypeInfo(&WorldNav_t25_il2cpp_TypeInfo)->static_fields)->___goMainCharacter_4), /*hidden argument*/&GameObject_get_transform_m313_MethodInfo);
		Transform_t10 * L_11 = L_10;
		NullCheck(L_11);
		Vector3_t13  L_12 = Transform_get_position_m299(L_11, /*hidden argument*/&Transform_get_position_m299_MethodInfo);
		Vector3_t13  L_13 = Vector3_op_Subtraction_m320(NULL /*static, unused*/, L_12, V_1, /*hidden argument*/&Vector3_op_Subtraction_m320_MethodInfo);
		NullCheck(L_11);
		Transform_set_position_m319(L_11, L_13, /*hidden argument*/&Transform_set_position_m319_MethodInfo);
	}

IL_0075:
	{
		return;
	}
}
// Metadata Definition WorldNav
extern Il2CppType Single_t105_0_0_17;
FieldInfo WorldNav_t25____coeff_2_FieldInfo = 
{
	"coeff"/* name */
	, &Single_t105_0_0_17/* type */
	, &WorldNav_t25_il2cpp_TypeInfo/* parent */
	, offsetof(WorldNav_t25_StaticFields, ___coeff_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GameObject_t2_0_0_6;
FieldInfo WorldNav_t25____mainCharacter_3_FieldInfo = 
{
	"mainCharacter"/* name */
	, &GameObject_t2_0_0_6/* type */
	, &WorldNav_t25_il2cpp_TypeInfo/* parent */
	, offsetof(WorldNav_t25, ___mainCharacter_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GameObject_t2_0_0_22;
FieldInfo WorldNav_t25____goMainCharacter_4_FieldInfo = 
{
	"goMainCharacter"/* name */
	, &GameObject_t2_0_0_22/* type */
	, &WorldNav_t25_il2cpp_TypeInfo/* parent */
	, offsetof(WorldNav_t25_StaticFields, ___goMainCharacter_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Camera_t3_0_0_17;
FieldInfo WorldNav_t25____camCharacter_5_FieldInfo = 
{
	"camCharacter"/* name */
	, &Camera_t3_0_0_17/* type */
	, &WorldNav_t25_il2cpp_TypeInfo/* parent */
	, offsetof(WorldNav_t25_StaticFields, ___camCharacter_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* WorldNav_t25_FieldInfos[] =
{
	&WorldNav_t25____coeff_2_FieldInfo,
	&WorldNav_t25____mainCharacter_3_FieldInfo,
	&WorldNav_t25____goMainCharacter_4_FieldInfo,
	&WorldNav_t25____camCharacter_5_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::.ctor()
MethodInfo WorldNav__ctor_m64_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WorldNav__ctor_m64/* method */
	, &WorldNav_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 65/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::.cctor()
MethodInfo WorldNav__cctor_m65_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&WorldNav__cctor_m65/* method */
	, &WorldNav_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 66/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::Awake()
MethodInfo WorldNav_Awake_m66_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&WorldNav_Awake_m66/* method */
	, &WorldNav_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 67/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo WorldNav_t25_WorldNav_Walk_m67_ParameterInfos[] = 
{
	{"dir", 0, 134217759, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void WorldNav::Walk(System.Int32)
MethodInfo WorldNav_Walk_m67_MethodInfo = 
{
	"Walk"/* name */
	, (methodPointerType)&WorldNav_Walk_m67/* method */
	, &WorldNav_t25_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, WorldNav_t25_WorldNav_Walk_m67_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 68/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* WorldNav_t25_MethodInfos[] =
{
	&WorldNav__ctor_m64_MethodInfo,
	&WorldNav__cctor_m65_MethodInfo,
	&WorldNav_Awake_m66_MethodInfo,
	&WorldNav_Walk_m67_MethodInfo,
	NULL
};
static MethodInfo* WorldNav_t25_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType WorldNav_t25_0_0_0;
extern Il2CppType WorldNav_t25_1_0_0;
struct WorldNav_t25;
TypeInfo WorldNav_t25_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WorldNav"/* name */
	, ""/* namespaze */
	, WorldNav_t25_MethodInfos/* methods */
	, NULL/* properties */
	, WorldNav_t25_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WorldNav_t25_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, WorldNav_t25_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WorldNav_t25_il2cpp_TypeInfo/* cast_class */
	, &WorldNav_t25_0_0_0/* byval_arg */
	, &WorldNav_t25_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WorldNav_t25)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(WorldNav_t25_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// OrbitCamera
#include "AssemblyU2DCSharp_OrbitCamera.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo OrbitCamera_t26_il2cpp_TypeInfo;
// OrbitCamera
#include "AssemblyU2DCSharp_OrbitCameraMethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
extern MethodInfo Vector3_get_zero_m321_MethodInfo;
extern MethodInfo Component_get_gameObject_m322_MethodInfo;
extern MethodInfo Vector3_Distance_m323_MethodInfo;
extern MethodInfo OrbitCamera_Reset_m76_MethodInfo;
extern MethodInfo Object_op_Equality_m324_MethodInfo;
extern MethodInfo OrbitCamera_HandlePlayerInput_m72_MethodInfo;
extern MethodInfo OrbitCamera_CalculateDesiredPosition_m73_MethodInfo;
extern MethodInfo OrbitCamera_UpdatePosition_m75_MethodInfo;
extern MethodInfo Input_GetMouseButton_m325_MethodInfo;
extern MethodInfo Input_GetAxis_m326_MethodInfo;
extern MethodInfo OrbitCamera_ClampAngle_m77_MethodInfo;
extern MethodInfo Mathf_Clamp_m327_MethodInfo;
extern MethodInfo Mathf_SmoothDamp_m328_MethodInfo;
extern MethodInfo OrbitCamera_CalculatePosition_m74_MethodInfo;
extern MethodInfo Quaternion_op_Multiply_m329_MethodInfo;
extern MethodInfo Transform_LookAt_m330_MethodInfo;


// System.Void OrbitCamera::.ctor()
extern MethodInfo OrbitCamera__ctor_m68_MethodInfo;
 void OrbitCamera__ctor_m68 (OrbitCamera_t26 * __this, MethodInfo* method){
	{
		__this->___Distance_3 = (10.0f);
		__this->___DistanceMin_4 = (5.0f);
		__this->___DistanceMax_5 = (15.0f);
		__this->___X_MouseSensitivity_10 = (5.0f);
		__this->___Y_MouseSensitivity_11 = (5.0f);
		__this->___MouseWheelSensitivity_12 = (5.0f);
		__this->___Y_MinLimit_13 = (15.0f);
		__this->___Y_MaxLimit_14 = (70.0f);
		__this->___DistanceSmooth_15 = (0.025f);
		Vector3_t13  L_0 = Vector3_get_zero_m321(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_zero_m321_MethodInfo);
		__this->___desiredPosition_17 = L_0;
		__this->___X_Smooth_18 = (0.05f);
		__this->___Y_Smooth_19 = (0.1f);
		Vector3_t13  L_1 = Vector3_get_zero_m321(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_zero_m321_MethodInfo);
		__this->___position_23 = L_1;
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void OrbitCamera::Start()
extern MethodInfo OrbitCamera_Start_m69_MethodInfo;
 void OrbitCamera_Start_m69 (OrbitCamera_t26 * __this, MethodInfo* method){
	{
		Transform_t10 * L_0 = (__this->___TargetLookAt_2);
		NullCheck(L_0);
		Transform_t10 * L_1 = Component_get_transform_m219(L_0, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_1);
		Vector3_t13  L_2 = Transform_get_position_m299(L_1, /*hidden argument*/&Transform_get_position_m299_MethodInfo);
		GameObject_t2 * L_3 = Component_get_gameObject_m322(__this, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		NullCheck(L_3);
		Transform_t10 * L_4 = GameObject_get_transform_m313(L_3, /*hidden argument*/&GameObject_get_transform_m313_MethodInfo);
		NullCheck(L_4);
		Vector3_t13  L_5 = Transform_get_position_m299(L_4, /*hidden argument*/&Transform_get_position_m299_MethodInfo);
		float L_6 = Vector3_Distance_m323(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/&Vector3_Distance_m323_MethodInfo);
		__this->___Distance_3 = L_6;
		float L_7 = (__this->___Distance_3);
		float L_8 = (__this->___DistanceMax_5);
		if ((((float)L_7) <= ((float)L_8)))
		{
			goto IL_0048;
		}
	}
	{
		float L_9 = (__this->___Distance_3);
		__this->___DistanceMax_5 = L_9;
	}

IL_0048:
	{
		float L_10 = (__this->___Distance_3);
		__this->___startingDistance_6 = L_10;
		OrbitCamera_Reset_m76(__this, /*hidden argument*/&OrbitCamera_Reset_m76_MethodInfo);
		return;
	}
}
// System.Void OrbitCamera::Update()
extern MethodInfo OrbitCamera_Update_m70_MethodInfo;
 void OrbitCamera_Update_m70 (OrbitCamera_t26 * __this, MethodInfo* method){
	{
		return;
	}
}
// System.Void OrbitCamera::LateUpdate()
extern MethodInfo OrbitCamera_LateUpdate_m71_MethodInfo;
 void OrbitCamera_LateUpdate_m71 (OrbitCamera_t26 * __this, MethodInfo* method){
	{
		Transform_t10 * L_0 = (__this->___TargetLookAt_2);
		bool L_1 = Object_op_Equality_m324(NULL /*static, unused*/, L_0, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Equality_m324_MethodInfo);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		OrbitCamera_HandlePlayerInput_m72(__this, /*hidden argument*/&OrbitCamera_HandlePlayerInput_m72_MethodInfo);
		OrbitCamera_CalculateDesiredPosition_m73(__this, /*hidden argument*/&OrbitCamera_CalculateDesiredPosition_m73_MethodInfo);
		OrbitCamera_UpdatePosition_m75(__this, /*hidden argument*/&OrbitCamera_UpdatePosition_m75_MethodInfo);
		return;
	}
}
// System.Void OrbitCamera::HandlePlayerInput()
 void OrbitCamera_HandlePlayerInput_m72 (OrbitCamera_t26 * __this, MethodInfo* method){
	float V_0 = 0.0f;
	{
		V_0 = (0.01f);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		bool L_0 = Input_GetMouseButton_m325(NULL /*static, unused*/, 0, /*hidden argument*/&Input_GetMouseButton_m325_MethodInfo);
		if (!L_0)
		{
			goto IL_004d;
		}
	}
	{
		float L_1 = (__this->___mouseX_8);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		float L_2 = Input_GetAxis_m326(NULL /*static, unused*/, (String_t*) &_stringLiteral14, /*hidden argument*/&Input_GetAxis_m326_MethodInfo);
		float L_3 = (__this->___X_MouseSensitivity_10);
		__this->___mouseX_8 = ((float)(L_1+((float)((float)L_2*(float)L_3))));
		float L_4 = (__this->___mouseY_9);
		float L_5 = Input_GetAxis_m326(NULL /*static, unused*/, (String_t*) &_stringLiteral15, /*hidden argument*/&Input_GetAxis_m326_MethodInfo);
		float L_6 = (__this->___Y_MouseSensitivity_11);
		__this->___mouseY_9 = ((float)(L_4-((float)((float)L_5*(float)L_6))));
	}

IL_004d:
	{
		float L_7 = (__this->___mouseY_9);
		float L_8 = (__this->___Y_MinLimit_13);
		float L_9 = (__this->___Y_MaxLimit_14);
		float L_10 = OrbitCamera_ClampAngle_m77(__this, L_7, L_8, L_9, /*hidden argument*/&OrbitCamera_ClampAngle_m77_MethodInfo);
		__this->___mouseY_9 = L_10;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		float L_11 = Input_GetAxis_m326(NULL /*static, unused*/, (String_t*) &_stringLiteral16, /*hidden argument*/&Input_GetAxis_m326_MethodInfo);
		if ((((float)L_11) < ((float)((-V_0)))))
		{
			goto IL_008c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		float L_12 = Input_GetAxis_m326(NULL /*static, unused*/, (String_t*) &_stringLiteral16, /*hidden argument*/&Input_GetAxis_m326_MethodInfo);
		if ((((float)L_12) <= ((float)V_0)))
		{
			goto IL_00bb;
		}
	}

IL_008c:
	{
		float L_13 = (__this->___Distance_3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		float L_14 = Input_GetAxis_m326(NULL /*static, unused*/, (String_t*) &_stringLiteral16, /*hidden argument*/&Input_GetAxis_m326_MethodInfo);
		float L_15 = (__this->___MouseWheelSensitivity_12);
		float L_16 = (__this->___DistanceMin_4);
		float L_17 = (__this->___DistanceMax_5);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_18 = Mathf_Clamp_m327(NULL /*static, unused*/, ((float)(L_13-((float)((float)L_14*(float)L_15)))), L_16, L_17, /*hidden argument*/&Mathf_Clamp_m327_MethodInfo);
		__this->___desiredDistance_7 = L_18;
	}

IL_00bb:
	{
		return;
	}
}
// System.Void OrbitCamera::CalculateDesiredPosition()
 void OrbitCamera_CalculateDesiredPosition_m73 (OrbitCamera_t26 * __this, MethodInfo* method){
	{
		float L_0 = (__this->___Distance_3);
		float L_1 = (__this->___desiredDistance_7);
		float* L_2 = &(__this->___velocityDistance_16);
		float L_3 = (__this->___DistanceSmooth_15);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_4 = Mathf_SmoothDamp_m328(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/&Mathf_SmoothDamp_m328_MethodInfo);
		__this->___Distance_3 = L_4;
		float L_5 = (__this->___mouseY_9);
		float L_6 = (__this->___mouseX_8);
		float L_7 = (__this->___Distance_3);
		Vector3_t13  L_8 = OrbitCamera_CalculatePosition_m74(__this, L_5, L_6, L_7, /*hidden argument*/&OrbitCamera_CalculatePosition_m74_MethodInfo);
		__this->___desiredPosition_17 = L_8;
		return;
	}
}
// UnityEngine.Vector3 OrbitCamera::CalculatePosition(System.Single,System.Single,System.Single)
 Vector3_t13  OrbitCamera_CalculatePosition_m74 (OrbitCamera_t26 * __this, float ___rotationX, float ___rotationY, float ___distance, MethodInfo* method){
	Vector3_t13  V_0 = {0};
	Quaternion_t12  V_1 = {0};
	{
		Vector3__ctor_m248((&V_0), (0.0f), (0.0f), ((-___distance)), /*hidden argument*/&Vector3__ctor_m248_MethodInfo);
		Quaternion_t12  L_0 = Quaternion_Euler_m287(NULL /*static, unused*/, ___rotationX, ___rotationY, (0.0f), /*hidden argument*/&Quaternion_Euler_m287_MethodInfo);
		V_1 = L_0;
		Transform_t10 * L_1 = (__this->___TargetLookAt_2);
		NullCheck(L_1);
		Vector3_t13  L_2 = Transform_get_position_m299(L_1, /*hidden argument*/&Transform_get_position_m299_MethodInfo);
		Vector3_t13  L_3 = Quaternion_op_Multiply_m329(NULL /*static, unused*/, V_1, V_0, /*hidden argument*/&Quaternion_op_Multiply_m329_MethodInfo);
		Vector3_t13  L_4 = Vector3_op_Addition_m318(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/&Vector3_op_Addition_m318_MethodInfo);
		return L_4;
	}
}
// System.Void OrbitCamera::UpdatePosition()
 void OrbitCamera_UpdatePosition_m75 (OrbitCamera_t26 * __this, MethodInfo* method){
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		Vector3_t13 * L_0 = &(__this->___position_23);
		NullCheck(L_0);
		float L_1 = (L_0->___x_1);
		Vector3_t13 * L_2 = &(__this->___desiredPosition_17);
		NullCheck(L_2);
		float L_3 = (L_2->___x_1);
		float* L_4 = &(__this->___velX_20);
		float L_5 = (__this->___X_Smooth_18);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_6 = Mathf_SmoothDamp_m328(NULL /*static, unused*/, L_1, L_3, L_4, L_5, /*hidden argument*/&Mathf_SmoothDamp_m328_MethodInfo);
		V_0 = L_6;
		Vector3_t13 * L_7 = &(__this->___position_23);
		NullCheck(L_7);
		float L_8 = (L_7->___y_2);
		Vector3_t13 * L_9 = &(__this->___desiredPosition_17);
		NullCheck(L_9);
		float L_10 = (L_9->___y_2);
		float* L_11 = &(__this->___velY_21);
		float L_12 = (__this->___Y_Smooth_19);
		float L_13 = Mathf_SmoothDamp_m328(NULL /*static, unused*/, L_8, L_10, L_11, L_12, /*hidden argument*/&Mathf_SmoothDamp_m328_MethodInfo);
		V_1 = L_13;
		Vector3_t13 * L_14 = &(__this->___position_23);
		NullCheck(L_14);
		float L_15 = (L_14->___z_3);
		Vector3_t13 * L_16 = &(__this->___desiredPosition_17);
		NullCheck(L_16);
		float L_17 = (L_16->___z_3);
		float* L_18 = &(__this->___velZ_22);
		float L_19 = (__this->___X_Smooth_18);
		float L_20 = Mathf_SmoothDamp_m328(NULL /*static, unused*/, L_15, L_17, L_18, L_19, /*hidden argument*/&Mathf_SmoothDamp_m328_MethodInfo);
		V_2 = L_20;
		Vector3_t13  L_21 = {0};
		Vector3__ctor_m248(&L_21, V_0, V_1, V_2, /*hidden argument*/&Vector3__ctor_m248_MethodInfo);
		__this->___position_23 = L_21;
		Transform_t10 * L_22 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		Vector3_t13  L_23 = (__this->___position_23);
		NullCheck(L_22);
		Transform_set_position_m319(L_22, L_23, /*hidden argument*/&Transform_set_position_m319_MethodInfo);
		Transform_t10 * L_24 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		Transform_t10 * L_25 = (__this->___TargetLookAt_2);
		NullCheck(L_24);
		Transform_LookAt_m330(L_24, L_25, /*hidden argument*/&Transform_LookAt_m330_MethodInfo);
		return;
	}
}
// System.Void OrbitCamera::Reset()
 void OrbitCamera_Reset_m76 (OrbitCamera_t26 * __this, MethodInfo* method){
	{
		__this->___mouseX_8 = (0.0f);
		__this->___mouseY_9 = (0.0f);
		float L_0 = (__this->___startingDistance_6);
		__this->___Distance_3 = L_0;
		float L_1 = (__this->___Distance_3);
		__this->___desiredDistance_7 = L_1;
		return;
	}
}
// System.Single OrbitCamera::ClampAngle(System.Single,System.Single,System.Single)
 float OrbitCamera_ClampAngle_m77 (OrbitCamera_t26 * __this, float ___angle, float ___min, float ___max, MethodInfo* method){
	{
		goto IL_002d;
	}

IL_0005:
	{
		if ((((float)___angle) >= ((float)(-360.0f))))
		{
			goto IL_0019;
		}
	}
	{
		___angle = ((float)(___angle+(360.0f)));
	}

IL_0019:
	{
		if ((((float)___angle) <= ((float)(360.0f))))
		{
			goto IL_002d;
		}
	}
	{
		___angle = ((float)(___angle-(360.0f)));
	}

IL_002d:
	{
		if ((((float)___angle) < ((float)(-360.0f))))
		{
			goto IL_0005;
		}
	}
	{
		if ((((float)___angle) > ((float)(360.0f))))
		{
			goto IL_0005;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_0 = Mathf_Clamp_m327(NULL /*static, unused*/, ___angle, ___min, ___max, /*hidden argument*/&Mathf_Clamp_m327_MethodInfo);
		return L_0;
	}
}
// Metadata Definition OrbitCamera
extern Il2CppType Transform_t10_0_0_6;
FieldInfo OrbitCamera_t26____TargetLookAt_2_FieldInfo = 
{
	"TargetLookAt"/* name */
	, &Transform_t10_0_0_6/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___TargetLookAt_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_6;
FieldInfo OrbitCamera_t26____Distance_3_FieldInfo = 
{
	"Distance"/* name */
	, &Single_t105_0_0_6/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___Distance_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_6;
FieldInfo OrbitCamera_t26____DistanceMin_4_FieldInfo = 
{
	"DistanceMin"/* name */
	, &Single_t105_0_0_6/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___DistanceMin_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_6;
FieldInfo OrbitCamera_t26____DistanceMax_5_FieldInfo = 
{
	"DistanceMax"/* name */
	, &Single_t105_0_0_6/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___DistanceMax_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_1;
FieldInfo OrbitCamera_t26____startingDistance_6_FieldInfo = 
{
	"startingDistance"/* name */
	, &Single_t105_0_0_1/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___startingDistance_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_1;
FieldInfo OrbitCamera_t26____desiredDistance_7_FieldInfo = 
{
	"desiredDistance"/* name */
	, &Single_t105_0_0_1/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___desiredDistance_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_1;
FieldInfo OrbitCamera_t26____mouseX_8_FieldInfo = 
{
	"mouseX"/* name */
	, &Single_t105_0_0_1/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___mouseX_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_1;
FieldInfo OrbitCamera_t26____mouseY_9_FieldInfo = 
{
	"mouseY"/* name */
	, &Single_t105_0_0_1/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___mouseY_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_6;
FieldInfo OrbitCamera_t26____X_MouseSensitivity_10_FieldInfo = 
{
	"X_MouseSensitivity"/* name */
	, &Single_t105_0_0_6/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___X_MouseSensitivity_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_6;
FieldInfo OrbitCamera_t26____Y_MouseSensitivity_11_FieldInfo = 
{
	"Y_MouseSensitivity"/* name */
	, &Single_t105_0_0_6/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___Y_MouseSensitivity_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_6;
FieldInfo OrbitCamera_t26____MouseWheelSensitivity_12_FieldInfo = 
{
	"MouseWheelSensitivity"/* name */
	, &Single_t105_0_0_6/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___MouseWheelSensitivity_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_6;
FieldInfo OrbitCamera_t26____Y_MinLimit_13_FieldInfo = 
{
	"Y_MinLimit"/* name */
	, &Single_t105_0_0_6/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___Y_MinLimit_13)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_6;
FieldInfo OrbitCamera_t26____Y_MaxLimit_14_FieldInfo = 
{
	"Y_MaxLimit"/* name */
	, &Single_t105_0_0_6/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___Y_MaxLimit_14)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_6;
FieldInfo OrbitCamera_t26____DistanceSmooth_15_FieldInfo = 
{
	"DistanceSmooth"/* name */
	, &Single_t105_0_0_6/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___DistanceSmooth_15)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_1;
FieldInfo OrbitCamera_t26____velocityDistance_16_FieldInfo = 
{
	"velocityDistance"/* name */
	, &Single_t105_0_0_1/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___velocityDistance_16)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector3_t13_0_0_1;
FieldInfo OrbitCamera_t26____desiredPosition_17_FieldInfo = 
{
	"desiredPosition"/* name */
	, &Vector3_t13_0_0_1/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___desiredPosition_17)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_6;
FieldInfo OrbitCamera_t26____X_Smooth_18_FieldInfo = 
{
	"X_Smooth"/* name */
	, &Single_t105_0_0_6/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___X_Smooth_18)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_6;
FieldInfo OrbitCamera_t26____Y_Smooth_19_FieldInfo = 
{
	"Y_Smooth"/* name */
	, &Single_t105_0_0_6/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___Y_Smooth_19)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_1;
FieldInfo OrbitCamera_t26____velX_20_FieldInfo = 
{
	"velX"/* name */
	, &Single_t105_0_0_1/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___velX_20)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_1;
FieldInfo OrbitCamera_t26____velY_21_FieldInfo = 
{
	"velY"/* name */
	, &Single_t105_0_0_1/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___velY_21)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_1;
FieldInfo OrbitCamera_t26____velZ_22_FieldInfo = 
{
	"velZ"/* name */
	, &Single_t105_0_0_1/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___velZ_22)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector3_t13_0_0_1;
FieldInfo OrbitCamera_t26____position_23_FieldInfo = 
{
	"position"/* name */
	, &Vector3_t13_0_0_1/* type */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* parent */
	, offsetof(OrbitCamera_t26, ___position_23)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* OrbitCamera_t26_FieldInfos[] =
{
	&OrbitCamera_t26____TargetLookAt_2_FieldInfo,
	&OrbitCamera_t26____Distance_3_FieldInfo,
	&OrbitCamera_t26____DistanceMin_4_FieldInfo,
	&OrbitCamera_t26____DistanceMax_5_FieldInfo,
	&OrbitCamera_t26____startingDistance_6_FieldInfo,
	&OrbitCamera_t26____desiredDistance_7_FieldInfo,
	&OrbitCamera_t26____mouseX_8_FieldInfo,
	&OrbitCamera_t26____mouseY_9_FieldInfo,
	&OrbitCamera_t26____X_MouseSensitivity_10_FieldInfo,
	&OrbitCamera_t26____Y_MouseSensitivity_11_FieldInfo,
	&OrbitCamera_t26____MouseWheelSensitivity_12_FieldInfo,
	&OrbitCamera_t26____Y_MinLimit_13_FieldInfo,
	&OrbitCamera_t26____Y_MaxLimit_14_FieldInfo,
	&OrbitCamera_t26____DistanceSmooth_15_FieldInfo,
	&OrbitCamera_t26____velocityDistance_16_FieldInfo,
	&OrbitCamera_t26____desiredPosition_17_FieldInfo,
	&OrbitCamera_t26____X_Smooth_18_FieldInfo,
	&OrbitCamera_t26____Y_Smooth_19_FieldInfo,
	&OrbitCamera_t26____velX_20_FieldInfo,
	&OrbitCamera_t26____velY_21_FieldInfo,
	&OrbitCamera_t26____velZ_22_FieldInfo,
	&OrbitCamera_t26____position_23_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::.ctor()
MethodInfo OrbitCamera__ctor_m68_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&OrbitCamera__ctor_m68/* method */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 69/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::Start()
MethodInfo OrbitCamera_Start_m69_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&OrbitCamera_Start_m69/* method */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 70/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::Update()
MethodInfo OrbitCamera_Update_m70_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&OrbitCamera_Update_m70/* method */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 71/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::LateUpdate()
MethodInfo OrbitCamera_LateUpdate_m71_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&OrbitCamera_LateUpdate_m71/* method */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 72/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::HandlePlayerInput()
MethodInfo OrbitCamera_HandlePlayerInput_m72_MethodInfo = 
{
	"HandlePlayerInput"/* name */
	, (methodPointerType)&OrbitCamera_HandlePlayerInput_m72/* method */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 73/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::CalculateDesiredPosition()
MethodInfo OrbitCamera_CalculateDesiredPosition_m73_MethodInfo = 
{
	"CalculateDesiredPosition"/* name */
	, (methodPointerType)&OrbitCamera_CalculateDesiredPosition_m73/* method */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 74/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t105_0_0_0;
extern Il2CppType Single_t105_0_0_0;
extern Il2CppType Single_t105_0_0_0;
static ParameterInfo OrbitCamera_t26_OrbitCamera_CalculatePosition_m74_ParameterInfos[] = 
{
	{"rotationX", 0, 134217760, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
	{"rotationY", 1, 134217761, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
	{"distance", 2, 134217762, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
};
extern Il2CppType Vector3_t13_0_0_0;
extern void* RuntimeInvoker_Vector3_t13_Single_t105_Single_t105_Single_t105 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector3 OrbitCamera::CalculatePosition(System.Single,System.Single,System.Single)
MethodInfo OrbitCamera_CalculatePosition_m74_MethodInfo = 
{
	"CalculatePosition"/* name */
	, (methodPointerType)&OrbitCamera_CalculatePosition_m74/* method */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* declaring_type */
	, &Vector3_t13_0_0_0/* return_type */
	, RuntimeInvoker_Vector3_t13_Single_t105_Single_t105_Single_t105/* invoker_method */
	, OrbitCamera_t26_OrbitCamera_CalculatePosition_m74_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 75/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::UpdatePosition()
MethodInfo OrbitCamera_UpdatePosition_m75_MethodInfo = 
{
	"UpdatePosition"/* name */
	, (methodPointerType)&OrbitCamera_UpdatePosition_m75/* method */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 76/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void OrbitCamera::Reset()
MethodInfo OrbitCamera_Reset_m76_MethodInfo = 
{
	"Reset"/* name */
	, (methodPointerType)&OrbitCamera_Reset_m76/* method */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 77/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t105_0_0_0;
extern Il2CppType Single_t105_0_0_0;
extern Il2CppType Single_t105_0_0_0;
static ParameterInfo OrbitCamera_t26_OrbitCamera_ClampAngle_m77_ParameterInfos[] = 
{
	{"angle", 0, 134217763, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
	{"min", 1, 134217764, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
	{"max", 2, 134217765, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
};
extern Il2CppType Single_t105_0_0_0;
extern void* RuntimeInvoker_Single_t105_Single_t105_Single_t105_Single_t105 (MethodInfo* method, void* obj, void** args);
// System.Single OrbitCamera::ClampAngle(System.Single,System.Single,System.Single)
MethodInfo OrbitCamera_ClampAngle_m77_MethodInfo = 
{
	"ClampAngle"/* name */
	, (methodPointerType)&OrbitCamera_ClampAngle_m77/* method */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* declaring_type */
	, &Single_t105_0_0_0/* return_type */
	, RuntimeInvoker_Single_t105_Single_t105_Single_t105_Single_t105/* invoker_method */
	, OrbitCamera_t26_OrbitCamera_ClampAngle_m77_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 78/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* OrbitCamera_t26_MethodInfos[] =
{
	&OrbitCamera__ctor_m68_MethodInfo,
	&OrbitCamera_Start_m69_MethodInfo,
	&OrbitCamera_Update_m70_MethodInfo,
	&OrbitCamera_LateUpdate_m71_MethodInfo,
	&OrbitCamera_HandlePlayerInput_m72_MethodInfo,
	&OrbitCamera_CalculateDesiredPosition_m73_MethodInfo,
	&OrbitCamera_CalculatePosition_m74_MethodInfo,
	&OrbitCamera_UpdatePosition_m75_MethodInfo,
	&OrbitCamera_Reset_m76_MethodInfo,
	&OrbitCamera_ClampAngle_m77_MethodInfo,
	NULL
};
static MethodInfo* OrbitCamera_t26_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType OrbitCamera_t26_0_0_0;
extern Il2CppType OrbitCamera_t26_1_0_0;
struct OrbitCamera_t26;
TypeInfo OrbitCamera_t26_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "OrbitCamera"/* name */
	, ""/* namespaze */
	, OrbitCamera_t26_MethodInfos/* methods */
	, NULL/* properties */
	, OrbitCamera_t26_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, OrbitCamera_t26_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &OrbitCamera_t26_il2cpp_TypeInfo/* cast_class */
	, &OrbitCamera_t26_0_0_0/* byval_arg */
	, &OrbitCamera_t26_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (OrbitCamera_t26)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 10/* method_count */
	, 0/* property_count */
	, 22/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// TestParticles
#include "AssemblyU2DCSharp_TestParticles.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TestParticles_t28_il2cpp_TypeInfo;
// TestParticles
#include "AssemblyU2DCSharp_TestParticlesMethodDeclarations.h"

// UnityEngine.KeyCode
#include "UnityEngine_UnityEngine_KeyCode.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunction.h"
extern TypeInfo Rect_t118_il2cpp_TypeInfo;
extern TypeInfo WindowFunction_t119_il2cpp_TypeInfo;
extern TypeInfo GUI_t120_il2cpp_TypeInfo;
extern TypeInfo ObjectU5BU5D_t115_il2cpp_TypeInfo;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.GUI/WindowFunction
#include "UnityEngine_UnityEngine_GUI_WindowFunctionMethodDeclarations.h"
// UnityEngine.GUI
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
extern MethodInfo TestParticles_ShowParticle_m82_MethodInfo;
extern MethodInfo Input_GetKeyUp_m331_MethodInfo;
extern MethodInfo Rect__ctor_m332_MethodInfo;
extern MethodInfo TestParticles_InfoWindow_m84_MethodInfo;
extern MethodInfo WindowFunction__ctor_m333_MethodInfo;
extern MethodInfo GUI_Window_m334_MethodInfo;
extern MethodInfo TestParticles_ParticleInformationWindow_m83_MethodInfo;
extern MethodInfo Object_op_Inequality_m335_MethodInfo;
extern MethodInfo Object_DestroyObject_m336_MethodInfo;
extern MethodInfo Object_Instantiate_TisGameObject_t2_m337_MethodInfo;
extern MethodInfo String_Concat_m338_MethodInfo;
extern MethodInfo GUI_Label_m339_MethodInfo;
extern MethodInfo String_ToUpper_m340_MethodInfo;
struct Object_t117;
// Declaration !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
 GameObject_t2 * Object_Instantiate_TisGameObject_t2_m337 (Object_t * __this/* static, unused */, GameObject_t2 * p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void TestParticles::.ctor()
extern MethodInfo TestParticles__ctor_m78_MethodInfo;
 void TestParticles__ctor_m78 (TestParticles_t28 * __this, MethodInfo* method){
	{
		__this->___m_CurrentElementIndex_10 = (-1);
		__this->___m_CurrentParticleIndex_11 = (-1);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		__this->___m_ElementName_12 = (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		__this->___m_ParticleName_13 = (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void TestParticles::Start()
extern MethodInfo TestParticles_Start_m79_MethodInfo;
 void TestParticles_Start_m79 (TestParticles_t28 * __this, MethodInfo* method){
	{
		GameObjectU5BU5D_t27* L_0 = (__this->___m_PrefabListFire_2);
		NullCheck(L_0);
		if ((((int32_t)(((int32_t)(((Array_t *)L_0)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t27* L_1 = (__this->___m_PrefabListWind_3);
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)(((Array_t *)L_1)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t27* L_2 = (__this->___m_PrefabListWater_4);
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t27* L_3 = (__this->___m_PrefabListEarth_5);
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)(((Array_t *)L_3)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t27* L_4 = (__this->___m_PrefabListIce_6);
		NullCheck(L_4);
		if ((((int32_t)(((int32_t)(((Array_t *)L_4)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t27* L_5 = (__this->___m_PrefabListThunder_7);
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)(((Array_t *)L_5)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t27* L_6 = (__this->___m_PrefabListLight_8);
		NullCheck(L_6);
		if ((((int32_t)(((int32_t)(((Array_t *)L_6)->max_length)))) > ((int32_t)0)))
		{
			goto IL_0070;
		}
	}
	{
		GameObjectU5BU5D_t27* L_7 = (__this->___m_PrefabListDarkness_9);
		NullCheck(L_7);
		if ((((int32_t)(((int32_t)(((Array_t *)L_7)->max_length)))) <= ((int32_t)0)))
		{
			goto IL_0084;
		}
	}

IL_0070:
	{
		__this->___m_CurrentElementIndex_10 = 0;
		__this->___m_CurrentParticleIndex_11 = 0;
		TestParticles_ShowParticle_m82(__this, /*hidden argument*/&TestParticles_ShowParticle_m82_MethodInfo);
	}

IL_0084:
	{
		return;
	}
}
// System.Void TestParticles::Update()
extern MethodInfo TestParticles_Update_m80_MethodInfo;
 void TestParticles_Update_m80 (TestParticles_t28 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___m_CurrentElementIndex_10);
		if ((((int32_t)L_0) == ((int32_t)(-1))))
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_1 = (__this->___m_CurrentParticleIndex_11);
		if ((((int32_t)L_1) == ((int32_t)(-1))))
		{
			goto IL_00c1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		bool L_2 = Input_GetKeyUp_m331(NULL /*static, unused*/, ((int32_t)273), /*hidden argument*/&Input_GetKeyUp_m331_MethodInfo);
		if (!L_2)
		{
			goto IL_0047;
		}
	}
	{
		int32_t L_3 = (__this->___m_CurrentElementIndex_10);
		__this->___m_CurrentElementIndex_10 = ((int32_t)(L_3+1));
		__this->___m_CurrentParticleIndex_11 = 0;
		TestParticles_ShowParticle_m82(__this, /*hidden argument*/&TestParticles_ShowParticle_m82_MethodInfo);
		goto IL_00c1;
	}

IL_0047:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		bool L_4 = Input_GetKeyUp_m331(NULL /*static, unused*/, ((int32_t)274), /*hidden argument*/&Input_GetKeyUp_m331_MethodInfo);
		if (!L_4)
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_5 = (__this->___m_CurrentElementIndex_10);
		__this->___m_CurrentElementIndex_10 = ((int32_t)(L_5-1));
		__this->___m_CurrentParticleIndex_11 = 0;
		TestParticles_ShowParticle_m82(__this, /*hidden argument*/&TestParticles_ShowParticle_m82_MethodInfo);
		goto IL_00c1;
	}

IL_0076:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		bool L_6 = Input_GetKeyUp_m331(NULL /*static, unused*/, ((int32_t)276), /*hidden argument*/&Input_GetKeyUp_m331_MethodInfo);
		if (!L_6)
		{
			goto IL_009e;
		}
	}
	{
		int32_t L_7 = (__this->___m_CurrentParticleIndex_11);
		__this->___m_CurrentParticleIndex_11 = ((int32_t)(L_7-1));
		TestParticles_ShowParticle_m82(__this, /*hidden argument*/&TestParticles_ShowParticle_m82_MethodInfo);
		goto IL_00c1;
	}

IL_009e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Input_t98_il2cpp_TypeInfo));
		bool L_8 = Input_GetKeyUp_m331(NULL /*static, unused*/, ((int32_t)275), /*hidden argument*/&Input_GetKeyUp_m331_MethodInfo);
		if (!L_8)
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_9 = (__this->___m_CurrentParticleIndex_11);
		__this->___m_CurrentParticleIndex_11 = ((int32_t)(L_9+1));
		TestParticles_ShowParticle_m82(__this, /*hidden argument*/&TestParticles_ShowParticle_m82_MethodInfo);
	}

IL_00c1:
	{
		return;
	}
}
// System.Void TestParticles::OnGUI()
extern MethodInfo TestParticles_OnGUI_m81_MethodInfo;
 void TestParticles_OnGUI_m81 (TestParticles_t28 * __this, MethodInfo* method){
	{
		int32_t L_0 = Screen_get_width_m273(NULL /*static, unused*/, /*hidden argument*/&Screen_get_width_m273_MethodInfo);
		Rect_t118  L_1 = {0};
		Rect__ctor_m332(&L_1, (((float)((int32_t)(L_0-((int32_t)260))))), (5.0f), (250.0f), (80.0f), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		IntPtr_t121 L_2 = { &TestParticles_InfoWindow_m84_MethodInfo };
		WindowFunction_t119 * L_3 = (WindowFunction_t119 *)il2cpp_codegen_object_new (InitializedTypeInfo(&WindowFunction_t119_il2cpp_TypeInfo));
		WindowFunction__ctor_m333(L_3, __this, L_2, /*hidden argument*/&WindowFunction__ctor_m333_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUI_t120_il2cpp_TypeInfo));
		GUI_Window_m334(NULL /*static, unused*/, 1, L_1, L_3, (String_t*) &_stringLiteral17, /*hidden argument*/&GUI_Window_m334_MethodInfo);
		int32_t L_4 = Screen_get_width_m273(NULL /*static, unused*/, /*hidden argument*/&Screen_get_width_m273_MethodInfo);
		int32_t L_5 = Screen_get_height_m274(NULL /*static, unused*/, /*hidden argument*/&Screen_get_height_m274_MethodInfo);
		Rect_t118  L_6 = {0};
		Rect__ctor_m332(&L_6, (((float)((int32_t)(L_4-((int32_t)260))))), (((float)((int32_t)(L_5-((int32_t)85))))), (250.0f), (80.0f), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		IntPtr_t121 L_7 = { &TestParticles_ParticleInformationWindow_m83_MethodInfo };
		WindowFunction_t119 * L_8 = (WindowFunction_t119 *)il2cpp_codegen_object_new (InitializedTypeInfo(&WindowFunction_t119_il2cpp_TypeInfo));
		WindowFunction__ctor_m333(L_8, __this, L_7, /*hidden argument*/&WindowFunction__ctor_m333_MethodInfo);
		GUI_Window_m334(NULL /*static, unused*/, 2, L_6, L_8, (String_t*) &_stringLiteral18, /*hidden argument*/&GUI_Window_m334_MethodInfo);
		return;
	}
}
// System.Void TestParticles::ShowParticle()
 void TestParticles_ShowParticle_m82 (TestParticles_t28 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___m_CurrentElementIndex_10);
		if ((((int32_t)L_0) <= ((int32_t)7)))
		{
			goto IL_0018;
		}
	}
	{
		__this->___m_CurrentElementIndex_10 = 0;
		goto IL_002b;
	}

IL_0018:
	{
		int32_t L_1 = (__this->___m_CurrentElementIndex_10);
		if ((((int32_t)L_1) >= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		__this->___m_CurrentElementIndex_10 = 7;
	}

IL_002b:
	{
		int32_t L_2 = (__this->___m_CurrentElementIndex_10);
		if (L_2)
		{
			goto IL_0052;
		}
	}
	{
		GameObjectU5BU5D_t27* L_3 = (__this->___m_PrefabListFire_2);
		__this->___m_CurrentElementList_14 = L_3;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral19;
		goto IL_0165;
	}

IL_0052:
	{
		int32_t L_4 = (__this->___m_CurrentElementIndex_10);
		if ((((uint32_t)L_4) != ((uint32_t)1)))
		{
			goto IL_007a;
		}
	}
	{
		GameObjectU5BU5D_t27* L_5 = (__this->___m_PrefabListWater_4);
		__this->___m_CurrentElementList_14 = L_5;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral20;
		goto IL_0165;
	}

IL_007a:
	{
		int32_t L_6 = (__this->___m_CurrentElementIndex_10);
		if ((((uint32_t)L_6) != ((uint32_t)2)))
		{
			goto IL_00a2;
		}
	}
	{
		GameObjectU5BU5D_t27* L_7 = (__this->___m_PrefabListWind_3);
		__this->___m_CurrentElementList_14 = L_7;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral21;
		goto IL_0165;
	}

IL_00a2:
	{
		int32_t L_8 = (__this->___m_CurrentElementIndex_10);
		if ((((uint32_t)L_8) != ((uint32_t)3)))
		{
			goto IL_00ca;
		}
	}
	{
		GameObjectU5BU5D_t27* L_9 = (__this->___m_PrefabListEarth_5);
		__this->___m_CurrentElementList_14 = L_9;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral22;
		goto IL_0165;
	}

IL_00ca:
	{
		int32_t L_10 = (__this->___m_CurrentElementIndex_10);
		if ((((uint32_t)L_10) != ((uint32_t)4)))
		{
			goto IL_00f2;
		}
	}
	{
		GameObjectU5BU5D_t27* L_11 = (__this->___m_PrefabListThunder_7);
		__this->___m_CurrentElementList_14 = L_11;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral23;
		goto IL_0165;
	}

IL_00f2:
	{
		int32_t L_12 = (__this->___m_CurrentElementIndex_10);
		if ((((uint32_t)L_12) != ((uint32_t)5)))
		{
			goto IL_011a;
		}
	}
	{
		GameObjectU5BU5D_t27* L_13 = (__this->___m_PrefabListIce_6);
		__this->___m_CurrentElementList_14 = L_13;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral24;
		goto IL_0165;
	}

IL_011a:
	{
		int32_t L_14 = (__this->___m_CurrentElementIndex_10);
		if ((((uint32_t)L_14) != ((uint32_t)6)))
		{
			goto IL_0142;
		}
	}
	{
		GameObjectU5BU5D_t27* L_15 = (__this->___m_PrefabListLight_8);
		__this->___m_CurrentElementList_14 = L_15;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral25;
		goto IL_0165;
	}

IL_0142:
	{
		int32_t L_16 = (__this->___m_CurrentElementIndex_10);
		if ((((uint32_t)L_16) != ((uint32_t)7)))
		{
			goto IL_0165;
		}
	}
	{
		GameObjectU5BU5D_t27* L_17 = (__this->___m_PrefabListDarkness_9);
		__this->___m_CurrentElementList_14 = L_17;
		__this->___m_ElementName_12 = (String_t*) &_stringLiteral26;
	}

IL_0165:
	{
		int32_t L_18 = (__this->___m_CurrentParticleIndex_11);
		GameObjectU5BU5D_t27* L_19 = (__this->___m_CurrentElementList_14);
		NullCheck(L_19);
		if ((((int32_t)L_18) < ((int32_t)(((int32_t)(((Array_t *)L_19)->max_length))))))
		{
			goto IL_0184;
		}
	}
	{
		__this->___m_CurrentParticleIndex_11 = 0;
		goto IL_01a0;
	}

IL_0184:
	{
		int32_t L_20 = (__this->___m_CurrentParticleIndex_11);
		if ((((int32_t)L_20) >= ((int32_t)0)))
		{
			goto IL_01a0;
		}
	}
	{
		GameObjectU5BU5D_t27* L_21 = (__this->___m_CurrentElementList_14);
		NullCheck(L_21);
		__this->___m_CurrentParticleIndex_11 = ((int32_t)((((int32_t)(((Array_t *)L_21)->max_length)))-1));
	}

IL_01a0:
	{
		GameObjectU5BU5D_t27* L_22 = (__this->___m_CurrentElementList_14);
		int32_t L_23 = (__this->___m_CurrentParticleIndex_11);
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		NullCheck((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_22, L_24)));
		String_t* L_25 = Object_get_name_m226((*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_22, L_24)), /*hidden argument*/&Object_get_name_m226_MethodInfo);
		__this->___m_ParticleName_13 = L_25;
		GameObject_t2 * L_26 = (__this->___m_CurrentParticle_15);
		bool L_27 = Object_op_Inequality_m335(NULL /*static, unused*/, L_26, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_27)
		{
			goto IL_01d4;
		}
	}
	{
		GameObject_t2 * L_28 = (__this->___m_CurrentParticle_15);
		Object_DestroyObject_m336(NULL /*static, unused*/, L_28, /*hidden argument*/&Object_DestroyObject_m336_MethodInfo);
	}

IL_01d4:
	{
		GameObjectU5BU5D_t27* L_29 = (__this->___m_CurrentElementList_14);
		int32_t L_30 = (__this->___m_CurrentParticleIndex_11);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_30);
		int32_t L_31 = L_30;
		GameObject_t2 * L_32 = Object_Instantiate_TisGameObject_t2_m337(NULL /*static, unused*/, (*(GameObject_t2 **)(GameObject_t2 **)SZArrayLdElema(L_29, L_31)), /*hidden argument*/&Object_Instantiate_TisGameObject_t2_m337_MethodInfo);
		__this->___m_CurrentParticle_15 = L_32;
		return;
	}
}
// System.Void TestParticles::ParticleInformationWindow(System.Int32)
 void TestParticles_ParticleInformationWindow_m83 (TestParticles_t28 * __this, int32_t ___id, MethodInfo* method){
	{
		Rect_t118  L_0 = {0};
		Rect__ctor_m332(&L_0, (12.0f), (25.0f), (280.0f), (20.0f), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		ObjectU5BU5D_t115* L_1 = ((ObjectU5BU5D_t115*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t115_il2cpp_TypeInfo), 7));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, (String_t*) &_stringLiteral27);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0)) = (Object_t *)(String_t*) &_stringLiteral27;
		ObjectU5BU5D_t115* L_2 = L_1;
		String_t* L_3 = (__this->___m_ElementName_12);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1)) = (Object_t *)L_3;
		ObjectU5BU5D_t115* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, (String_t*) &_stringLiteral28);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2)) = (Object_t *)(String_t*) &_stringLiteral28;
		ObjectU5BU5D_t115* L_5 = L_4;
		int32_t L_6 = (__this->___m_CurrentParticleIndex_11);
		int32_t L_7 = ((int32_t)(L_6+1));
		Object_t * L_8 = Box(InitializedTypeInfo(&Int32_t93_il2cpp_TypeInfo), &L_7);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 3);
		ArrayElementTypeCheck (L_5, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_5, 3)) = (Object_t *)L_8;
		ObjectU5BU5D_t115* L_9 = L_5;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 4);
		ArrayElementTypeCheck (L_9, (String_t*) &_stringLiteral29);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 4)) = (Object_t *)(String_t*) &_stringLiteral29;
		ObjectU5BU5D_t115* L_10 = L_9;
		GameObjectU5BU5D_t27* L_11 = (__this->___m_CurrentElementList_14);
		NullCheck(L_11);
		int32_t L_12 = (((int32_t)(((Array_t *)L_11)->max_length)));
		Object_t * L_13 = Box(InitializedTypeInfo(&Int32_t93_il2cpp_TypeInfo), &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 5);
		ArrayElementTypeCheck (L_10, L_13);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 5)) = (Object_t *)L_13;
		ObjectU5BU5D_t115* L_14 = L_10;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 6);
		ArrayElementTypeCheck (L_14, (String_t*) &_stringLiteral7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 6)) = (Object_t *)(String_t*) &_stringLiteral7;
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_15 = String_Concat_m338(NULL /*static, unused*/, L_14, /*hidden argument*/&String_Concat_m338_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUI_t120_il2cpp_TypeInfo));
		GUI_Label_m339(NULL /*static, unused*/, L_0, L_15, /*hidden argument*/&GUI_Label_m339_MethodInfo);
		Rect_t118  L_16 = {0};
		Rect__ctor_m332(&L_16, (12.0f), (50.0f), (280.0f), (20.0f), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		String_t* L_17 = (__this->___m_ParticleName_13);
		NullCheck(L_17);
		String_t* L_18 = String_ToUpper_m340(L_17, /*hidden argument*/&String_ToUpper_m340_MethodInfo);
		String_t* L_19 = String_Concat_m282(NULL /*static, unused*/, (String_t*) &_stringLiteral30, L_18, /*hidden argument*/&String_Concat_m282_MethodInfo);
		GUI_Label_m339(NULL /*static, unused*/, L_16, L_19, /*hidden argument*/&GUI_Label_m339_MethodInfo);
		return;
	}
}
// System.Void TestParticles::InfoWindow(System.Int32)
 void TestParticles_InfoWindow_m84 (TestParticles_t28 * __this, int32_t ___id, MethodInfo* method){
	{
		Rect_t118  L_0 = {0};
		Rect__ctor_m332(&L_0, (15.0f), (25.0f), (240.0f), (20.0f), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUI_t120_il2cpp_TypeInfo));
		GUI_Label_m339(NULL /*static, unused*/, L_0, (String_t*) &_stringLiteral31, /*hidden argument*/&GUI_Label_m339_MethodInfo);
		Rect_t118  L_1 = {0};
		Rect__ctor_m332(&L_1, (15.0f), (50.0f), (240.0f), (20.0f), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		GUI_Label_m339(NULL /*static, unused*/, L_1, (String_t*) &_stringLiteral32, /*hidden argument*/&GUI_Label_m339_MethodInfo);
		return;
	}
}
// Metadata Definition TestParticles
extern Il2CppType GameObjectU5BU5D_t27_0_0_6;
FieldInfo TestParticles_t28____m_PrefabListFire_2_FieldInfo = 
{
	"m_PrefabListFire"/* name */
	, &GameObjectU5BU5D_t27_0_0_6/* type */
	, &TestParticles_t28_il2cpp_TypeInfo/* parent */
	, offsetof(TestParticles_t28, ___m_PrefabListFire_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GameObjectU5BU5D_t27_0_0_6;
FieldInfo TestParticles_t28____m_PrefabListWind_3_FieldInfo = 
{
	"m_PrefabListWind"/* name */
	, &GameObjectU5BU5D_t27_0_0_6/* type */
	, &TestParticles_t28_il2cpp_TypeInfo/* parent */
	, offsetof(TestParticles_t28, ___m_PrefabListWind_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GameObjectU5BU5D_t27_0_0_6;
FieldInfo TestParticles_t28____m_PrefabListWater_4_FieldInfo = 
{
	"m_PrefabListWater"/* name */
	, &GameObjectU5BU5D_t27_0_0_6/* type */
	, &TestParticles_t28_il2cpp_TypeInfo/* parent */
	, offsetof(TestParticles_t28, ___m_PrefabListWater_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GameObjectU5BU5D_t27_0_0_6;
FieldInfo TestParticles_t28____m_PrefabListEarth_5_FieldInfo = 
{
	"m_PrefabListEarth"/* name */
	, &GameObjectU5BU5D_t27_0_0_6/* type */
	, &TestParticles_t28_il2cpp_TypeInfo/* parent */
	, offsetof(TestParticles_t28, ___m_PrefabListEarth_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GameObjectU5BU5D_t27_0_0_6;
FieldInfo TestParticles_t28____m_PrefabListIce_6_FieldInfo = 
{
	"m_PrefabListIce"/* name */
	, &GameObjectU5BU5D_t27_0_0_6/* type */
	, &TestParticles_t28_il2cpp_TypeInfo/* parent */
	, offsetof(TestParticles_t28, ___m_PrefabListIce_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GameObjectU5BU5D_t27_0_0_6;
FieldInfo TestParticles_t28____m_PrefabListThunder_7_FieldInfo = 
{
	"m_PrefabListThunder"/* name */
	, &GameObjectU5BU5D_t27_0_0_6/* type */
	, &TestParticles_t28_il2cpp_TypeInfo/* parent */
	, offsetof(TestParticles_t28, ___m_PrefabListThunder_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GameObjectU5BU5D_t27_0_0_6;
FieldInfo TestParticles_t28____m_PrefabListLight_8_FieldInfo = 
{
	"m_PrefabListLight"/* name */
	, &GameObjectU5BU5D_t27_0_0_6/* type */
	, &TestParticles_t28_il2cpp_TypeInfo/* parent */
	, offsetof(TestParticles_t28, ___m_PrefabListLight_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GameObjectU5BU5D_t27_0_0_6;
FieldInfo TestParticles_t28____m_PrefabListDarkness_9_FieldInfo = 
{
	"m_PrefabListDarkness"/* name */
	, &GameObjectU5BU5D_t27_0_0_6/* type */
	, &TestParticles_t28_il2cpp_TypeInfo/* parent */
	, offsetof(TestParticles_t28, ___m_PrefabListDarkness_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo TestParticles_t28____m_CurrentElementIndex_10_FieldInfo = 
{
	"m_CurrentElementIndex"/* name */
	, &Int32_t93_0_0_1/* type */
	, &TestParticles_t28_il2cpp_TypeInfo/* parent */
	, offsetof(TestParticles_t28, ___m_CurrentElementIndex_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo TestParticles_t28____m_CurrentParticleIndex_11_FieldInfo = 
{
	"m_CurrentParticleIndex"/* name */
	, &Int32_t93_0_0_1/* type */
	, &TestParticles_t28_il2cpp_TypeInfo/* parent */
	, offsetof(TestParticles_t28, ___m_CurrentParticleIndex_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo TestParticles_t28____m_ElementName_12_FieldInfo = 
{
	"m_ElementName"/* name */
	, &String_t_0_0_1/* type */
	, &TestParticles_t28_il2cpp_TypeInfo/* parent */
	, offsetof(TestParticles_t28, ___m_ElementName_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo TestParticles_t28____m_ParticleName_13_FieldInfo = 
{
	"m_ParticleName"/* name */
	, &String_t_0_0_1/* type */
	, &TestParticles_t28_il2cpp_TypeInfo/* parent */
	, offsetof(TestParticles_t28, ___m_ParticleName_13)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GameObjectU5BU5D_t27_0_0_1;
FieldInfo TestParticles_t28____m_CurrentElementList_14_FieldInfo = 
{
	"m_CurrentElementList"/* name */
	, &GameObjectU5BU5D_t27_0_0_1/* type */
	, &TestParticles_t28_il2cpp_TypeInfo/* parent */
	, offsetof(TestParticles_t28, ___m_CurrentElementList_14)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GameObject_t2_0_0_1;
FieldInfo TestParticles_t28____m_CurrentParticle_15_FieldInfo = 
{
	"m_CurrentParticle"/* name */
	, &GameObject_t2_0_0_1/* type */
	, &TestParticles_t28_il2cpp_TypeInfo/* parent */
	, offsetof(TestParticles_t28, ___m_CurrentParticle_15)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TestParticles_t28_FieldInfos[] =
{
	&TestParticles_t28____m_PrefabListFire_2_FieldInfo,
	&TestParticles_t28____m_PrefabListWind_3_FieldInfo,
	&TestParticles_t28____m_PrefabListWater_4_FieldInfo,
	&TestParticles_t28____m_PrefabListEarth_5_FieldInfo,
	&TestParticles_t28____m_PrefabListIce_6_FieldInfo,
	&TestParticles_t28____m_PrefabListThunder_7_FieldInfo,
	&TestParticles_t28____m_PrefabListLight_8_FieldInfo,
	&TestParticles_t28____m_PrefabListDarkness_9_FieldInfo,
	&TestParticles_t28____m_CurrentElementIndex_10_FieldInfo,
	&TestParticles_t28____m_CurrentParticleIndex_11_FieldInfo,
	&TestParticles_t28____m_ElementName_12_FieldInfo,
	&TestParticles_t28____m_ParticleName_13_FieldInfo,
	&TestParticles_t28____m_CurrentElementList_14_FieldInfo,
	&TestParticles_t28____m_CurrentParticle_15_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::.ctor()
MethodInfo TestParticles__ctor_m78_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TestParticles__ctor_m78/* method */
	, &TestParticles_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 79/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::Start()
MethodInfo TestParticles_Start_m79_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&TestParticles_Start_m79/* method */
	, &TestParticles_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 80/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::Update()
MethodInfo TestParticles_Update_m80_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&TestParticles_Update_m80/* method */
	, &TestParticles_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 81/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::OnGUI()
MethodInfo TestParticles_OnGUI_m81_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&TestParticles_OnGUI_m81/* method */
	, &TestParticles_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 82/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::ShowParticle()
MethodInfo TestParticles_ShowParticle_m82_MethodInfo = 
{
	"ShowParticle"/* name */
	, (methodPointerType)&TestParticles_ShowParticle_m82/* method */
	, &TestParticles_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 83/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo TestParticles_t28_TestParticles_ParticleInformationWindow_m83_ParameterInfos[] = 
{
	{"id", 0, 134217766, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::ParticleInformationWindow(System.Int32)
MethodInfo TestParticles_ParticleInformationWindow_m83_MethodInfo = 
{
	"ParticleInformationWindow"/* name */
	, (methodPointerType)&TestParticles_ParticleInformationWindow_m83/* method */
	, &TestParticles_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, TestParticles_t28_TestParticles_ParticleInformationWindow_m83_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 84/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo TestParticles_t28_TestParticles_InfoWindow_m84_ParameterInfos[] = 
{
	{"id", 0, 134217767, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void TestParticles::InfoWindow(System.Int32)
MethodInfo TestParticles_InfoWindow_m84_MethodInfo = 
{
	"InfoWindow"/* name */
	, (methodPointerType)&TestParticles_InfoWindow_m84/* method */
	, &TestParticles_t28_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, TestParticles_t28_TestParticles_InfoWindow_m84_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 85/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TestParticles_t28_MethodInfos[] =
{
	&TestParticles__ctor_m78_MethodInfo,
	&TestParticles_Start_m79_MethodInfo,
	&TestParticles_Update_m80_MethodInfo,
	&TestParticles_OnGUI_m81_MethodInfo,
	&TestParticles_ShowParticle_m82_MethodInfo,
	&TestParticles_ParticleInformationWindow_m83_MethodInfo,
	&TestParticles_InfoWindow_m84_MethodInfo,
	NULL
};
static MethodInfo* TestParticles_t28_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType TestParticles_t28_0_0_0;
extern Il2CppType TestParticles_t28_1_0_0;
struct TestParticles_t28;
TypeInfo TestParticles_t28_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TestParticles"/* name */
	, ""/* namespaze */
	, TestParticles_t28_MethodInfos/* methods */
	, NULL/* properties */
	, TestParticles_t28_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TestParticles_t28_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TestParticles_t28_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TestParticles_t28_il2cpp_TypeInfo/* cast_class */
	, &TestParticles_t28_0_0_0/* byval_arg */
	, &TestParticles_t28_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TestParticles_t28)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 0/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo;
// Vuforia.BackgroundPlaneBehaviour
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviourMethodDeclarations.h"

// Vuforia.BackgroundPlaneAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbstMethodDeclarations.h"
extern MethodInfo BackgroundPlaneAbstractBehaviour__ctor_m341_MethodInfo;


// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
extern MethodInfo BackgroundPlaneBehaviour__ctor_m85_MethodInfo;
 void BackgroundPlaneBehaviour__ctor_m85 (BackgroundPlaneBehaviour_t29 * __this, MethodInfo* method){
	{
		BackgroundPlaneAbstractBehaviour__ctor_m341(__this, /*hidden argument*/&BackgroundPlaneAbstractBehaviour__ctor_m341_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.BackgroundPlaneBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
MethodInfo BackgroundPlaneBehaviour__ctor_m85_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BackgroundPlaneBehaviour__ctor_m85/* method */
	, &BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 86/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* BackgroundPlaneBehaviour_t29_MethodInfos[] =
{
	&BackgroundPlaneBehaviour__ctor_m85_MethodInfo,
	NULL
};
extern MethodInfo BackgroundPlaneAbstractBehaviour_OnVideoBackgroundConfigChanged_m342_MethodInfo;
static MethodInfo* BackgroundPlaneBehaviour_t29_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&BackgroundPlaneAbstractBehaviour_OnVideoBackgroundConfigChanged_m342_MethodInfo,
};
extern TypeInfo IVideoBackgroundEventHandler_t122_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair BackgroundPlaneBehaviour_t29_InterfacesOffsets[] = 
{
	{ &IVideoBackgroundEventHandler_t122_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType BackgroundPlaneBehaviour_t29_0_0_0;
extern Il2CppType BackgroundPlaneBehaviour_t29_1_0_0;
extern TypeInfo BackgroundPlaneAbstractBehaviour_t30_il2cpp_TypeInfo;
struct BackgroundPlaneBehaviour_t29;
TypeInfo BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "BackgroundPlaneBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, BackgroundPlaneBehaviour_t29_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &BackgroundPlaneAbstractBehaviour_t30_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, BackgroundPlaneBehaviour_t29_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo/* cast_class */
	, &BackgroundPlaneBehaviour_t29_0_0_0/* byval_arg */
	, &BackgroundPlaneBehaviour_t29_1_0_0/* this_arg */
	, BackgroundPlaneBehaviour_t29_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BackgroundPlaneBehaviour_t29)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CloudRecoBehaviour_t31_il2cpp_TypeInfo;
// Vuforia.CloudRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviourMethodDeclarations.h"

// Vuforia.CloudRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CloudRecoAbstractBeMethodDeclarations.h"
extern MethodInfo CloudRecoAbstractBehaviour__ctor_m343_MethodInfo;


// System.Void Vuforia.CloudRecoBehaviour::.ctor()
extern MethodInfo CloudRecoBehaviour__ctor_m86_MethodInfo;
 void CloudRecoBehaviour__ctor_m86 (CloudRecoBehaviour_t31 * __this, MethodInfo* method){
	{
		CloudRecoAbstractBehaviour__ctor_m343(__this, /*hidden argument*/&CloudRecoAbstractBehaviour__ctor_m343_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.CloudRecoBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.CloudRecoBehaviour::.ctor()
MethodInfo CloudRecoBehaviour__ctor_m86_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CloudRecoBehaviour__ctor_m86/* method */
	, &CloudRecoBehaviour_t31_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 87/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CloudRecoBehaviour_t31_MethodInfos[] =
{
	&CloudRecoBehaviour__ctor_m86_MethodInfo,
	NULL
};
static MethodInfo* CloudRecoBehaviour_t31_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType CloudRecoBehaviour_t31_0_0_0;
extern Il2CppType CloudRecoBehaviour_t31_1_0_0;
extern TypeInfo CloudRecoAbstractBehaviour_t32_il2cpp_TypeInfo;
struct CloudRecoBehaviour_t31;
TypeInfo CloudRecoBehaviour_t31_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CloudRecoBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, CloudRecoBehaviour_t31_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &CloudRecoAbstractBehaviour_t32_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CloudRecoBehaviour_t31_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CloudRecoBehaviour_t31_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CloudRecoBehaviour_t31_il2cpp_TypeInfo/* cast_class */
	, &CloudRecoBehaviour_t31_0_0_0/* byval_arg */
	, &CloudRecoBehaviour_t31_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CloudRecoBehaviour_t31)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CylinderTargetBehaviour_t33_il2cpp_TypeInfo;
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviourMethodDeclarations.h"

// Vuforia.CylinderTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstrMethodDeclarations.h"
extern MethodInfo CylinderTargetAbstractBehaviour__ctor_m344_MethodInfo;


// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
extern MethodInfo CylinderTargetBehaviour__ctor_m87_MethodInfo;
 void CylinderTargetBehaviour__ctor_m87 (CylinderTargetBehaviour_t33 * __this, MethodInfo* method){
	{
		CylinderTargetAbstractBehaviour__ctor_m344(__this, /*hidden argument*/&CylinderTargetAbstractBehaviour__ctor_m344_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.CylinderTargetBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
MethodInfo CylinderTargetBehaviour__ctor_m87_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CylinderTargetBehaviour__ctor_m87/* method */
	, &CylinderTargetBehaviour_t33_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 88/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CylinderTargetBehaviour_t33_MethodInfos[] =
{
	&CylinderTargetBehaviour__ctor_m87_MethodInfo,
	NULL
};
extern MethodInfo TrackableBehaviour_get_TrackableName_m345_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m346_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m347_MethodInfo;
extern MethodInfo TrackableBehaviour_get_Trackable_m348_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m349_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m350_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m351_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m352_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m353_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m354_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m355_MethodInfo;
extern MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m356_MethodInfo;
extern MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m357_MethodInfo;
extern MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m358_MethodInfo;
extern MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m359_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m360_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_OnTrackerUpdate_m361_MethodInfo;
extern MethodInfo CylinderTargetAbstractBehaviour_OnFrameIndexUpdate_m362_MethodInfo;
extern MethodInfo CylinderTargetAbstractBehaviour_InternalUnregisterTrackable_m363_MethodInfo;
extern MethodInfo CylinderTargetAbstractBehaviour_CorrectScaleImpl_m364_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m365_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m366_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m367_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m368_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m369_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m370_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m371_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m372_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m373_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m374_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m375_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m376_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m377_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m378_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m379_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m380_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m381_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m382_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m383_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m384_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m385_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m386_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m387_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m388_MethodInfo;
extern MethodInfo DataSetTrackableBehaviour_OnDrawGizmos_m389_MethodInfo;
extern MethodInfo CylinderTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m390_MethodInfo;
extern MethodInfo CylinderTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m391_MethodInfo;
extern MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_InitializeCylinderTarget_m392_MethodInfo;
extern MethodInfo CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_SetAspectRatio_m393_MethodInfo;
static MethodInfo* CylinderTargetBehaviour_t33_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m345_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m346_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m347_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m349_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m350_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m351_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m352_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m353_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m354_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m355_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m356_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m357_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m358_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m359_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m360_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&DataSetTrackableBehaviour_OnTrackerUpdate_m361_MethodInfo,
	&CylinderTargetAbstractBehaviour_OnFrameIndexUpdate_m362_MethodInfo,
	&CylinderTargetAbstractBehaviour_InternalUnregisterTrackable_m363_MethodInfo,
	&CylinderTargetAbstractBehaviour_CorrectScaleImpl_m364_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m365_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m366_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m367_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m368_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m369_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m370_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m371_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m372_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m373_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m374_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m375_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m376_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m377_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m378_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m379_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m380_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m381_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m382_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m383_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m384_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m385_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m386_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m387_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m388_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&DataSetTrackableBehaviour_OnDrawGizmos_m389_MethodInfo,
	&CylinderTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m390_MethodInfo,
	&CylinderTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m391_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_InitializeCylinderTarget_m392_MethodInfo,
	&CylinderTargetAbstractBehaviour_Vuforia_IEditorCylinderTargetBehaviour_SetAspectRatio_m393_MethodInfo,
};
extern TypeInfo IEditorCylinderTargetBehaviour_t123_il2cpp_TypeInfo;
extern TypeInfo IEditorDataSetTrackableBehaviour_t124_il2cpp_TypeInfo;
extern TypeInfo IEditorTrackableBehaviour_t125_il2cpp_TypeInfo;
extern TypeInfo WorldCenterTrackableBehaviour_t126_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair CylinderTargetBehaviour_t33_InterfacesOffsets[] = 
{
	{ &IEditorCylinderTargetBehaviour_t123_il2cpp_TypeInfo, 53},
	{ &IEditorDataSetTrackableBehaviour_t124_il2cpp_TypeInfo, 25},
	{ &IEditorTrackableBehaviour_t125_il2cpp_TypeInfo, 4},
	{ &WorldCenterTrackableBehaviour_t126_il2cpp_TypeInfo, 49},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
extern Il2CppType CylinderTargetBehaviour_t33_1_0_0;
extern TypeInfo CylinderTargetAbstractBehaviour_t34_il2cpp_TypeInfo;
struct CylinderTargetBehaviour_t33;
TypeInfo CylinderTargetBehaviour_t33_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "CylinderTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, CylinderTargetBehaviour_t33_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &CylinderTargetAbstractBehaviour_t34_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CylinderTargetBehaviour_t33_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CylinderTargetBehaviour_t33_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CylinderTargetBehaviour_t33_il2cpp_TypeInfo/* cast_class */
	, &CylinderTargetBehaviour_t33_0_0_0/* byval_arg */
	, &CylinderTargetBehaviour_t33_1_0_0/* this_arg */
	, CylinderTargetBehaviour_t33_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CylinderTargetBehaviour_t33)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 55/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Vuforia.DataSetLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DataSetLoadBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo DataSetLoadBehaviour_t35_il2cpp_TypeInfo;
// Vuforia.DataSetLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DataSetLoadBehaviourMethodDeclarations.h"

// Vuforia.DataSetLoadAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetLoadAbstractMethodDeclarations.h"
extern MethodInfo DataSetLoadAbstractBehaviour__ctor_m394_MethodInfo;


// System.Void Vuforia.DataSetLoadBehaviour::.ctor()
extern MethodInfo DataSetLoadBehaviour__ctor_m88_MethodInfo;
 void DataSetLoadBehaviour__ctor_m88 (DataSetLoadBehaviour_t35 * __this, MethodInfo* method){
	{
		DataSetLoadAbstractBehaviour__ctor_m394(__this, /*hidden argument*/&DataSetLoadAbstractBehaviour__ctor_m394_MethodInfo);
		return;
	}
}
// System.Void Vuforia.DataSetLoadBehaviour::AddOSSpecificExternalDatasetSearchDirs()
extern MethodInfo DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m89_MethodInfo;
 void DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m89 (DataSetLoadBehaviour_t35 * __this, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition Vuforia.DataSetLoadBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DataSetLoadBehaviour::.ctor()
MethodInfo DataSetLoadBehaviour__ctor_m88_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DataSetLoadBehaviour__ctor_m88/* method */
	, &DataSetLoadBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 89/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DataSetLoadBehaviour::AddOSSpecificExternalDatasetSearchDirs()
MethodInfo DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m89_MethodInfo = 
{
	"AddOSSpecificExternalDatasetSearchDirs"/* name */
	, (methodPointerType)&DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m89/* method */
	, &DataSetLoadBehaviour_t35_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 90/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* DataSetLoadBehaviour_t35_MethodInfos[] =
{
	&DataSetLoadBehaviour__ctor_m88_MethodInfo,
	&DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m89_MethodInfo,
	NULL
};
static MethodInfo* DataSetLoadBehaviour_t35_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&DataSetLoadBehaviour_AddOSSpecificExternalDatasetSearchDirs_m89_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType DataSetLoadBehaviour_t35_0_0_0;
extern Il2CppType DataSetLoadBehaviour_t35_1_0_0;
extern TypeInfo DataSetLoadAbstractBehaviour_t36_il2cpp_TypeInfo;
struct DataSetLoadBehaviour_t35;
TypeInfo DataSetLoadBehaviour_t35_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DataSetLoadBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, DataSetLoadBehaviour_t35_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &DataSetLoadAbstractBehaviour_t36_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &DataSetLoadBehaviour_t35_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DataSetLoadBehaviour_t35_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DataSetLoadBehaviour_t35_il2cpp_TypeInfo/* cast_class */
	, &DataSetLoadBehaviour_t35_0_0_0/* byval_arg */
	, &DataSetLoadBehaviour_t35_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DataSetLoadBehaviour_t35)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandler.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo;
// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandlerMethodDeclarations.h"

// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio.h"
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"
// System.Action`1<Vuforia.QCARUnity/InitError>
#include "mscorlib_System_Action_1_gen.h"
extern TypeInfo QCARAbstractBehaviour_t71_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo Action_1_t127_il2cpp_TypeInfo;
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Action`1<Vuforia.QCARUnity/InitError>
#include "mscorlib_System_Action_1_genMethodDeclarations.h"
// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavioMethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern Il2CppType QCARAbstractBehaviour_t71_0_0_0;
extern MethodInfo Type_GetTypeFromHandle_m395_MethodInfo;
extern MethodInfo Object_FindObjectOfType_m396_MethodInfo;
extern MethodInfo Object_op_Implicit_m397_MethodInfo;
extern MethodInfo DefaultInitializationErrorHandler_OnQCARInitializationError_m97_MethodInfo;
extern MethodInfo Action_1__ctor_m398_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_RegisterQCARInitErrorCallback_m399_MethodInfo;
extern MethodInfo DefaultInitializationErrorHandler_DrawWindowContent_m94_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_UnregisterQCARInitErrorCallback_m400_MethodInfo;
extern MethodInfo GUI_Button_m401_MethodInfo;
extern MethodInfo Application_Quit_m402_MethodInfo;
extern MethodInfo Debug_LogError_m403_MethodInfo;
extern MethodInfo DefaultInitializationErrorHandler_SetErrorCode_m95_MethodInfo;
extern MethodInfo DefaultInitializationErrorHandler_SetErrorOccurred_m96_MethodInfo;


// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
extern MethodInfo DefaultInitializationErrorHandler__ctor_m90_MethodInfo;
 void DefaultInitializationErrorHandler__ctor_m90 (DefaultInitializationErrorHandler_t37 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		__this->___mErrorText_3 = (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
extern MethodInfo DefaultInitializationErrorHandler_Awake_m91_MethodInfo;
 void DefaultInitializationErrorHandler_Awake_m91 (DefaultInitializationErrorHandler_t37 * __this, MethodInfo* method){
	QCARAbstractBehaviour_t71 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&QCARAbstractBehaviour_t71_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		Object_t117 * L_1 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_FindObjectOfType_m396_MethodInfo);
		V_0 = ((QCARAbstractBehaviour_t71 *)Castclass(L_1, InitializedTypeInfo(&QCARAbstractBehaviour_t71_il2cpp_TypeInfo)));
		bool L_2 = Object_op_Implicit_m397(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		IntPtr_t121 L_3 = { &DefaultInitializationErrorHandler_OnQCARInitializationError_m97_MethodInfo };
		Action_1_t127 * L_4 = (Action_1_t127 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_1_t127_il2cpp_TypeInfo));
		Action_1__ctor_m398(L_4, __this, L_3, /*hidden argument*/&Action_1__ctor_m398_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterQCARInitErrorCallback_m399(V_0, L_4, /*hidden argument*/&QCARAbstractBehaviour_RegisterQCARInitErrorCallback_m399_MethodInfo);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
extern MethodInfo DefaultInitializationErrorHandler_OnGUI_m92_MethodInfo;
 void DefaultInitializationErrorHandler_OnGUI_m92 (DefaultInitializationErrorHandler_t37 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mErrorOccurred_4);
		if (!L_0)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m273(NULL /*static, unused*/, /*hidden argument*/&Screen_get_width_m273_MethodInfo);
		int32_t L_2 = Screen_get_height_m274(NULL /*static, unused*/, /*hidden argument*/&Screen_get_height_m274_MethodInfo);
		Rect_t118  L_3 = {0};
		Rect__ctor_m332(&L_3, (0.0f), (0.0f), (((float)L_1)), (((float)L_2)), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		IntPtr_t121 L_4 = { &DefaultInitializationErrorHandler_DrawWindowContent_m94_MethodInfo };
		WindowFunction_t119 * L_5 = (WindowFunction_t119 *)il2cpp_codegen_object_new (InitializedTypeInfo(&WindowFunction_t119_il2cpp_TypeInfo));
		WindowFunction__ctor_m333(L_5, __this, L_4, /*hidden argument*/&WindowFunction__ctor_m333_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUI_t120_il2cpp_TypeInfo));
		GUI_Window_m334(NULL /*static, unused*/, 0, L_3, L_5, (String_t*) &_stringLiteral33, /*hidden argument*/&GUI_Window_m334_MethodInfo);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
extern MethodInfo DefaultInitializationErrorHandler_OnDestroy_m93_MethodInfo;
 void DefaultInitializationErrorHandler_OnDestroy_m93 (DefaultInitializationErrorHandler_t37 * __this, MethodInfo* method){
	QCARAbstractBehaviour_t71 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&QCARAbstractBehaviour_t71_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		Object_t117 * L_1 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_FindObjectOfType_m396_MethodInfo);
		V_0 = ((QCARAbstractBehaviour_t71 *)Castclass(L_1, InitializedTypeInfo(&QCARAbstractBehaviour_t71_il2cpp_TypeInfo)));
		bool L_2 = Object_op_Implicit_m397(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		IntPtr_t121 L_3 = { &DefaultInitializationErrorHandler_OnQCARInitializationError_m97_MethodInfo };
		Action_1_t127 * L_4 = (Action_1_t127 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_1_t127_il2cpp_TypeInfo));
		Action_1__ctor_m398(L_4, __this, L_3, /*hidden argument*/&Action_1__ctor_m398_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_UnregisterQCARInitErrorCallback_m400(V_0, L_4, /*hidden argument*/&QCARAbstractBehaviour_UnregisterQCARInitErrorCallback_m400_MethodInfo);
	}

IL_0032:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
 void DefaultInitializationErrorHandler_DrawWindowContent_m94 (DefaultInitializationErrorHandler_t37 * __this, int32_t ___id, MethodInfo* method){
	{
		int32_t L_0 = Screen_get_width_m273(NULL /*static, unused*/, /*hidden argument*/&Screen_get_width_m273_MethodInfo);
		int32_t L_1 = Screen_get_height_m274(NULL /*static, unused*/, /*hidden argument*/&Screen_get_height_m274_MethodInfo);
		Rect_t118  L_2 = {0};
		Rect__ctor_m332(&L_2, (10.0f), (25.0f), (((float)((int32_t)(L_0-((int32_t)20))))), (((float)((int32_t)(L_1-((int32_t)95))))), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		String_t* L_3 = (__this->___mErrorText_3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUI_t120_il2cpp_TypeInfo));
		GUI_Label_m339(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/&GUI_Label_m339_MethodInfo);
		int32_t L_4 = Screen_get_width_m273(NULL /*static, unused*/, /*hidden argument*/&Screen_get_width_m273_MethodInfo);
		int32_t L_5 = Screen_get_height_m274(NULL /*static, unused*/, /*hidden argument*/&Screen_get_height_m274_MethodInfo);
		Rect_t118  L_6 = {0};
		Rect__ctor_m332(&L_6, (((float)((int32_t)(((int32_t)((int32_t)L_4/(int32_t)2))-((int32_t)75))))), (((float)((int32_t)(L_5-((int32_t)60))))), (150.0f), (50.0f), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		bool L_7 = GUI_Button_m401(NULL /*static, unused*/, L_6, (String_t*) &_stringLiteral34, /*hidden argument*/&GUI_Button_m401_MethodInfo);
		if (!L_7)
		{
			goto IL_0063;
		}
	}
	{
		Application_Quit_m402(NULL /*static, unused*/, /*hidden argument*/&Application_Quit_m402_MethodInfo);
	}

IL_0063:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.QCARUnity/InitError)
 void DefaultInitializationErrorHandler_SetErrorCode_m95 (DefaultInitializationErrorHandler_t37 * __this, int32_t ___errorCode, MethodInfo* method){
	int32_t V_0 = {0};
	{
		String_t* L_0 = (__this->___mErrorText_3);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_1 = String_Concat_m282(NULL /*static, unused*/, (String_t*) &_stringLiteral35, L_0, /*hidden argument*/&String_Concat_m282_MethodInfo);
		Debug_LogError_m403(NULL /*static, unused*/, L_1, /*hidden argument*/&Debug_LogError_m403_MethodInfo);
		V_0 = ___errorCode;
		if (((int32_t)(V_0+((int32_t)10))) == 0)
		{
			goto IL_004d;
		}
		if (((int32_t)(V_0+((int32_t)10))) == 1)
		{
			goto IL_00ad;
		}
		if (((int32_t)(V_0+((int32_t)10))) == 2)
		{
			goto IL_009d;
		}
		if (((int32_t)(V_0+((int32_t)10))) == 3)
		{
			goto IL_007d;
		}
		if (((int32_t)(V_0+((int32_t)10))) == 4)
		{
			goto IL_008d;
		}
		if (((int32_t)(V_0+((int32_t)10))) == 5)
		{
			goto IL_006d;
		}
		if (((int32_t)(V_0+((int32_t)10))) == 6)
		{
			goto IL_005d;
		}
		if (((int32_t)(V_0+((int32_t)10))) == 7)
		{
			goto IL_00bd;
		}
		if (((int32_t)(V_0+((int32_t)10))) == 8)
		{
			goto IL_00cd;
		}
		if (((int32_t)(V_0+((int32_t)10))) == 9)
		{
			goto IL_00dd;
		}
	}
	{
		goto IL_00ed;
	}

IL_004d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral36;
		goto IL_00ed;
	}

IL_005d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral37;
		goto IL_00ed;
	}

IL_006d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral38;
		goto IL_00ed;
	}

IL_007d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral39;
		goto IL_00ed;
	}

IL_008d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral40;
		goto IL_00ed;
	}

IL_009d:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral41;
		goto IL_00ed;
	}

IL_00ad:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral42;
		goto IL_00ed;
	}

IL_00bd:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral43;
		goto IL_00ed;
	}

IL_00cd:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral44;
		goto IL_00ed;
	}

IL_00dd:
	{
		__this->___mErrorText_3 = (String_t*) &_stringLiteral45;
		goto IL_00ed;
	}

IL_00ed:
	{
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
 void DefaultInitializationErrorHandler_SetErrorOccurred_m96 (DefaultInitializationErrorHandler_t37 * __this, bool ___errorOccurred, MethodInfo* method){
	{
		__this->___mErrorOccurred_4 = ___errorOccurred;
		return;
	}
}
// System.Void Vuforia.DefaultInitializationErrorHandler::OnQCARInitializationError(Vuforia.QCARUnity/InitError)
 void DefaultInitializationErrorHandler_OnQCARInitializationError_m97 (DefaultInitializationErrorHandler_t37 * __this, int32_t ___initError, MethodInfo* method){
	{
		if (!___initError)
		{
			goto IL_0014;
		}
	}
	{
		DefaultInitializationErrorHandler_SetErrorCode_m95(__this, ___initError, /*hidden argument*/&DefaultInitializationErrorHandler_SetErrorCode_m95_MethodInfo);
		DefaultInitializationErrorHandler_SetErrorOccurred_m96(__this, 1, /*hidden argument*/&DefaultInitializationErrorHandler_SetErrorOccurred_m96_MethodInfo);
	}

IL_0014:
	{
		return;
	}
}
// Metadata Definition Vuforia.DefaultInitializationErrorHandler
extern Il2CppType String_t_0_0_32849;
FieldInfo DefaultInitializationErrorHandler_t37____WINDOW_TITLE_2_FieldInfo = 
{
	"WINDOW_TITLE"/* name */
	, &String_t_0_0_32849/* type */
	, &DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo DefaultInitializationErrorHandler_t37____mErrorText_3_FieldInfo = 
{
	"mErrorText"/* name */
	, &String_t_0_0_1/* type */
	, &DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* parent */
	, offsetof(DefaultInitializationErrorHandler_t37, ___mErrorText_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo DefaultInitializationErrorHandler_t37____mErrorOccurred_4_FieldInfo = 
{
	"mErrorOccurred"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* parent */
	, offsetof(DefaultInitializationErrorHandler_t37, ___mErrorOccurred_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* DefaultInitializationErrorHandler_t37_FieldInfos[] =
{
	&DefaultInitializationErrorHandler_t37____WINDOW_TITLE_2_FieldInfo,
	&DefaultInitializationErrorHandler_t37____mErrorText_3_FieldInfo,
	&DefaultInitializationErrorHandler_t37____mErrorOccurred_4_FieldInfo,
	NULL
};
static const uint16_t DefaultInitializationErrorHandler_t37____WINDOW_TITLE_2_DefaultValueData[] = { 0x51, 0x43, 0x41, 0x52, 0x20, 0x49, 0x6E, 0x69, 0x74, 0x69, 0x61, 0x6C, 0x69, 0x7A, 0x61, 0x74, 0x69, 0x6F, 0x6E, 0x20, 0x45, 0x72, 0x72, 0x6F, 0x72, 0x00 };
static Il2CppFieldDefaultValueEntry DefaultInitializationErrorHandler_t37____WINDOW_TITLE_2_DefaultValue = 
{
	&DefaultInitializationErrorHandler_t37____WINDOW_TITLE_2_FieldInfo/* field */
	, { (char*)&DefaultInitializationErrorHandler_t37____WINDOW_TITLE_2_DefaultValueData, &String_t_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* DefaultInitializationErrorHandler_t37_FieldDefaultValues[] = 
{
	&DefaultInitializationErrorHandler_t37____WINDOW_TITLE_2_DefaultValue,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
MethodInfo DefaultInitializationErrorHandler__ctor_m90_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler__ctor_m90/* method */
	, &DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 91/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
MethodInfo DefaultInitializationErrorHandler_Awake_m91_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_Awake_m91/* method */
	, &DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 92/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
MethodInfo DefaultInitializationErrorHandler_OnGUI_m92_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_OnGUI_m92/* method */
	, &DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 93/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
MethodInfo DefaultInitializationErrorHandler_OnDestroy_m93_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_OnDestroy_m93/* method */
	, &DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 94/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo DefaultInitializationErrorHandler_t37_DefaultInitializationErrorHandler_DrawWindowContent_m94_ParameterInfos[] = 
{
	{"id", 0, 134217768, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
MethodInfo DefaultInitializationErrorHandler_DrawWindowContent_m94_MethodInfo = 
{
	"DrawWindowContent"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_DrawWindowContent_m94/* method */
	, &DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, DefaultInitializationErrorHandler_t37_DefaultInitializationErrorHandler_DrawWindowContent_m94_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 95/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType InitError_t128_0_0_0;
extern Il2CppType InitError_t128_0_0_0;
static ParameterInfo DefaultInitializationErrorHandler_t37_DefaultInitializationErrorHandler_SetErrorCode_m95_ParameterInfos[] = 
{
	{"errorCode", 0, 134217769, &EmptyCustomAttributesCache, &InitError_t128_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.QCARUnity/InitError)
MethodInfo DefaultInitializationErrorHandler_SetErrorCode_m95_MethodInfo = 
{
	"SetErrorCode"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_SetErrorCode_m95/* method */
	, &DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, DefaultInitializationErrorHandler_t37_DefaultInitializationErrorHandler_SetErrorCode_m95_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 96/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo DefaultInitializationErrorHandler_t37_DefaultInitializationErrorHandler_SetErrorOccurred_m96_ParameterInfos[] = 
{
	{"errorOccurred", 0, 134217770, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_SByte_t129 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
MethodInfo DefaultInitializationErrorHandler_SetErrorOccurred_m96_MethodInfo = 
{
	"SetErrorOccurred"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_SetErrorOccurred_m96/* method */
	, &DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_SByte_t129/* invoker_method */
	, DefaultInitializationErrorHandler_t37_DefaultInitializationErrorHandler_SetErrorOccurred_m96_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 97/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType InitError_t128_0_0_0;
static ParameterInfo DefaultInitializationErrorHandler_t37_DefaultInitializationErrorHandler_OnQCARInitializationError_m97_ParameterInfos[] = 
{
	{"initError", 0, 134217771, &EmptyCustomAttributesCache, &InitError_t128_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultInitializationErrorHandler::OnQCARInitializationError(Vuforia.QCARUnity/InitError)
MethodInfo DefaultInitializationErrorHandler_OnQCARInitializationError_m97_MethodInfo = 
{
	"OnQCARInitializationError"/* name */
	, (methodPointerType)&DefaultInitializationErrorHandler_OnQCARInitializationError_m97/* method */
	, &DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, DefaultInitializationErrorHandler_t37_DefaultInitializationErrorHandler_OnQCARInitializationError_m97_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 98/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* DefaultInitializationErrorHandler_t37_MethodInfos[] =
{
	&DefaultInitializationErrorHandler__ctor_m90_MethodInfo,
	&DefaultInitializationErrorHandler_Awake_m91_MethodInfo,
	&DefaultInitializationErrorHandler_OnGUI_m92_MethodInfo,
	&DefaultInitializationErrorHandler_OnDestroy_m93_MethodInfo,
	&DefaultInitializationErrorHandler_DrawWindowContent_m94_MethodInfo,
	&DefaultInitializationErrorHandler_SetErrorCode_m95_MethodInfo,
	&DefaultInitializationErrorHandler_SetErrorOccurred_m96_MethodInfo,
	&DefaultInitializationErrorHandler_OnQCARInitializationError_m97_MethodInfo,
	NULL
};
static MethodInfo* DefaultInitializationErrorHandler_t37_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType DefaultInitializationErrorHandler_t37_0_0_0;
extern Il2CppType DefaultInitializationErrorHandler_t37_1_0_0;
struct DefaultInitializationErrorHandler_t37;
TypeInfo DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultInitializationErrorHandler"/* name */
	, "Vuforia"/* namespaze */
	, DefaultInitializationErrorHandler_t37_MethodInfos/* methods */
	, NULL/* properties */
	, DefaultInitializationErrorHandler_t37_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DefaultInitializationErrorHandler_t37_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* cast_class */
	, &DefaultInitializationErrorHandler_t37_0_0_0/* byval_arg */
	, &DefaultInitializationErrorHandler_t37_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, DefaultInitializationErrorHandler_t37_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultInitializationErrorHandler_t37)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 8/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandler.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo;
// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandlerMethodDeclarations.h"

// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour.h"
// System.Action`1<Vuforia.Prop>
#include "mscorlib_System_Action_1_gen_0.h"
// System.Action`1<Vuforia.Surface>
#include "mscorlib_System_Action_1_gen_1.h"
// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour.h"
// Vuforia.PropAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavio.h"
// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour.h"
// Vuforia.SurfaceAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeha.h"
extern TypeInfo Action_1_t130_il2cpp_TypeInfo;
extern TypeInfo Action_1_t131_il2cpp_TypeInfo;
// System.Action`1<Vuforia.Prop>
#include "mscorlib_System_Action_1_gen_0MethodDeclarations.h"
// Vuforia.ReconstructionAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstrMethodDeclarations.h"
// System.Action`1<Vuforia.Surface>
#include "mscorlib_System_Action_1_gen_1MethodDeclarations.h"
extern MethodInfo Component_GetComponent_TisReconstructionBehaviour_t38_m404_MethodInfo;
extern MethodInfo DefaultSmartTerrainEventHandler_OnPropCreated_m101_MethodInfo;
extern MethodInfo Action_1__ctor_m405_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_RegisterPropCreatedCallback_m406_MethodInfo;
extern MethodInfo DefaultSmartTerrainEventHandler_OnSurfaceCreated_m102_MethodInfo;
extern MethodInfo Action_1__ctor_m407_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_RegisterSurfaceCreatedCallback_m408_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_UnregisterPropCreatedCallback_m409_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_UnregisterSurfaceCreatedCallback_m410_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_AssociateProp_m411_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_AssociateSurface_m412_MethodInfo;
struct Component_t100;
// UnityEngine.CastHelper`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_4.h"
// Declaration !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionBehaviour>()
// !!0 UnityEngine.Component::GetComponent<Vuforia.ReconstructionBehaviour>()
#define Component_GetComponent_TisReconstructionBehaviour_t38_m404(__this, method) (ReconstructionBehaviour_t38 *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)


// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
extern MethodInfo DefaultSmartTerrainEventHandler__ctor_m98_MethodInfo;
 void DefaultSmartTerrainEventHandler__ctor_m98 (DefaultSmartTerrainEventHandler_t41 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
extern MethodInfo DefaultSmartTerrainEventHandler_Start_m99_MethodInfo;
 void DefaultSmartTerrainEventHandler_Start_m99 (DefaultSmartTerrainEventHandler_t41 * __this, MethodInfo* method){
	{
		ReconstructionBehaviour_t38 * L_0 = Component_GetComponent_TisReconstructionBehaviour_t38_m404(__this, /*hidden argument*/&Component_GetComponent_TisReconstructionBehaviour_t38_m404_MethodInfo);
		__this->___mReconstructionBehaviour_2 = L_0;
		ReconstructionBehaviour_t38 * L_1 = (__this->___mReconstructionBehaviour_2);
		bool L_2 = Object_op_Implicit_m397(NULL /*static, unused*/, L_1, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (!L_2)
		{
			goto IL_004a;
		}
	}
	{
		ReconstructionBehaviour_t38 * L_3 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t121 L_4 = { &DefaultSmartTerrainEventHandler_OnPropCreated_m101_MethodInfo };
		Action_1_t130 * L_5 = (Action_1_t130 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_1_t130_il2cpp_TypeInfo));
		Action_1__ctor_m405(L_5, __this, L_4, /*hidden argument*/&Action_1__ctor_m405_MethodInfo);
		NullCheck(L_3);
		ReconstructionAbstractBehaviour_RegisterPropCreatedCallback_m406(L_3, L_5, /*hidden argument*/&ReconstructionAbstractBehaviour_RegisterPropCreatedCallback_m406_MethodInfo);
		ReconstructionBehaviour_t38 * L_6 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t121 L_7 = { &DefaultSmartTerrainEventHandler_OnSurfaceCreated_m102_MethodInfo };
		Action_1_t131 * L_8 = (Action_1_t131 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_1_t131_il2cpp_TypeInfo));
		Action_1__ctor_m407(L_8, __this, L_7, /*hidden argument*/&Action_1__ctor_m407_MethodInfo);
		NullCheck(L_6);
		ReconstructionAbstractBehaviour_RegisterSurfaceCreatedCallback_m408(L_6, L_8, /*hidden argument*/&ReconstructionAbstractBehaviour_RegisterSurfaceCreatedCallback_m408_MethodInfo);
	}

IL_004a:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
extern MethodInfo DefaultSmartTerrainEventHandler_OnDestroy_m100_MethodInfo;
 void DefaultSmartTerrainEventHandler_OnDestroy_m100 (DefaultSmartTerrainEventHandler_t41 * __this, MethodInfo* method){
	{
		ReconstructionBehaviour_t38 * L_0 = (__this->___mReconstructionBehaviour_2);
		bool L_1 = Object_op_Implicit_m397(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		ReconstructionBehaviour_t38 * L_2 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t121 L_3 = { &DefaultSmartTerrainEventHandler_OnPropCreated_m101_MethodInfo };
		Action_1_t130 * L_4 = (Action_1_t130 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_1_t130_il2cpp_TypeInfo));
		Action_1__ctor_m405(L_4, __this, L_3, /*hidden argument*/&Action_1__ctor_m405_MethodInfo);
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_UnregisterPropCreatedCallback_m409(L_2, L_4, /*hidden argument*/&ReconstructionAbstractBehaviour_UnregisterPropCreatedCallback_m409_MethodInfo);
		ReconstructionBehaviour_t38 * L_5 = (__this->___mReconstructionBehaviour_2);
		IntPtr_t121 L_6 = { &DefaultSmartTerrainEventHandler_OnSurfaceCreated_m102_MethodInfo };
		Action_1_t131 * L_7 = (Action_1_t131 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_1_t131_il2cpp_TypeInfo));
		Action_1__ctor_m407(L_7, __this, L_6, /*hidden argument*/&Action_1__ctor_m407_MethodInfo);
		NullCheck(L_5);
		ReconstructionAbstractBehaviour_UnregisterSurfaceCreatedCallback_m410(L_5, L_7, /*hidden argument*/&ReconstructionAbstractBehaviour_UnregisterSurfaceCreatedCallback_m410_MethodInfo);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
 void DefaultSmartTerrainEventHandler_OnPropCreated_m101 (DefaultSmartTerrainEventHandler_t41 * __this, Object_t * ___prop, MethodInfo* method){
	{
		ReconstructionBehaviour_t38 * L_0 = (__this->___mReconstructionBehaviour_2);
		bool L_1 = Object_op_Implicit_m397(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t38 * L_2 = (__this->___mReconstructionBehaviour_2);
		PropBehaviour_t39 * L_3 = (__this->___PropTemplate_3);
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateProp_m411(L_2, L_3, ___prop, /*hidden argument*/&ReconstructionAbstractBehaviour_AssociateProp_m411_MethodInfo);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
 void DefaultSmartTerrainEventHandler_OnSurfaceCreated_m102 (DefaultSmartTerrainEventHandler_t41 * __this, Object_t * ___surface, MethodInfo* method){
	{
		ReconstructionBehaviour_t38 * L_0 = (__this->___mReconstructionBehaviour_2);
		bool L_1 = Object_op_Implicit_m397(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		ReconstructionBehaviour_t38 * L_2 = (__this->___mReconstructionBehaviour_2);
		SurfaceBehaviour_t40 * L_3 = (__this->___SurfaceTemplate_4);
		NullCheck(L_2);
		ReconstructionAbstractBehaviour_AssociateSurface_m412(L_2, L_3, ___surface, /*hidden argument*/&ReconstructionAbstractBehaviour_AssociateSurface_m412_MethodInfo);
	}

IL_0023:
	{
		return;
	}
}
// Metadata Definition Vuforia.DefaultSmartTerrainEventHandler
extern Il2CppType ReconstructionBehaviour_t38_0_0_1;
FieldInfo DefaultSmartTerrainEventHandler_t41____mReconstructionBehaviour_2_FieldInfo = 
{
	"mReconstructionBehaviour"/* name */
	, &ReconstructionBehaviour_t38_0_0_1/* type */
	, &DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo/* parent */
	, offsetof(DefaultSmartTerrainEventHandler_t41, ___mReconstructionBehaviour_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PropBehaviour_t39_0_0_6;
FieldInfo DefaultSmartTerrainEventHandler_t41____PropTemplate_3_FieldInfo = 
{
	"PropTemplate"/* name */
	, &PropBehaviour_t39_0_0_6/* type */
	, &DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo/* parent */
	, offsetof(DefaultSmartTerrainEventHandler_t41, ___PropTemplate_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType SurfaceBehaviour_t40_0_0_6;
FieldInfo DefaultSmartTerrainEventHandler_t41____SurfaceTemplate_4_FieldInfo = 
{
	"SurfaceTemplate"/* name */
	, &SurfaceBehaviour_t40_0_0_6/* type */
	, &DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo/* parent */
	, offsetof(DefaultSmartTerrainEventHandler_t41, ___SurfaceTemplate_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* DefaultSmartTerrainEventHandler_t41_FieldInfos[] =
{
	&DefaultSmartTerrainEventHandler_t41____mReconstructionBehaviour_2_FieldInfo,
	&DefaultSmartTerrainEventHandler_t41____PropTemplate_3_FieldInfo,
	&DefaultSmartTerrainEventHandler_t41____SurfaceTemplate_4_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
MethodInfo DefaultSmartTerrainEventHandler__ctor_m98_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler__ctor_m98/* method */
	, &DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 99/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
MethodInfo DefaultSmartTerrainEventHandler_Start_m99_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler_Start_m99/* method */
	, &DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 100/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
MethodInfo DefaultSmartTerrainEventHandler_OnDestroy_m100_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler_OnDestroy_m100/* method */
	, &DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 101/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Prop_t42_0_0_0;
extern Il2CppType Prop_t42_0_0_0;
static ParameterInfo DefaultSmartTerrainEventHandler_t41_DefaultSmartTerrainEventHandler_OnPropCreated_m101_ParameterInfos[] = 
{
	{"prop", 0, 134217772, &EmptyCustomAttributesCache, &Prop_t42_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
MethodInfo DefaultSmartTerrainEventHandler_OnPropCreated_m101_MethodInfo = 
{
	"OnPropCreated"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler_OnPropCreated_m101/* method */
	, &DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, DefaultSmartTerrainEventHandler_t41_DefaultSmartTerrainEventHandler_OnPropCreated_m101_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 102/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Surface_t43_0_0_0;
extern Il2CppType Surface_t43_0_0_0;
static ParameterInfo DefaultSmartTerrainEventHandler_t41_DefaultSmartTerrainEventHandler_OnSurfaceCreated_m102_ParameterInfos[] = 
{
	{"surface", 0, 134217773, &EmptyCustomAttributesCache, &Surface_t43_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
MethodInfo DefaultSmartTerrainEventHandler_OnSurfaceCreated_m102_MethodInfo = 
{
	"OnSurfaceCreated"/* name */
	, (methodPointerType)&DefaultSmartTerrainEventHandler_OnSurfaceCreated_m102/* method */
	, &DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, DefaultSmartTerrainEventHandler_t41_DefaultSmartTerrainEventHandler_OnSurfaceCreated_m102_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 103/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* DefaultSmartTerrainEventHandler_t41_MethodInfos[] =
{
	&DefaultSmartTerrainEventHandler__ctor_m98_MethodInfo,
	&DefaultSmartTerrainEventHandler_Start_m99_MethodInfo,
	&DefaultSmartTerrainEventHandler_OnDestroy_m100_MethodInfo,
	&DefaultSmartTerrainEventHandler_OnPropCreated_m101_MethodInfo,
	&DefaultSmartTerrainEventHandler_OnSurfaceCreated_m102_MethodInfo,
	NULL
};
static MethodInfo* DefaultSmartTerrainEventHandler_t41_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType DefaultSmartTerrainEventHandler_t41_0_0_0;
extern Il2CppType DefaultSmartTerrainEventHandler_t41_1_0_0;
struct DefaultSmartTerrainEventHandler_t41;
TypeInfo DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultSmartTerrainEventHandler"/* name */
	, "Vuforia"/* namespaze */
	, DefaultSmartTerrainEventHandler_t41_MethodInfos/* methods */
	, NULL/* properties */
	, DefaultSmartTerrainEventHandler_t41_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DefaultSmartTerrainEventHandler_t41_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo/* cast_class */
	, &DefaultSmartTerrainEventHandler_t41_0_0_0/* byval_arg */
	, &DefaultSmartTerrainEventHandler_t41_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultSmartTerrainEventHandler_t41)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandler.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo DefaultTrackableEventHandler_t45_il2cpp_TypeInfo;
// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandlerMethodDeclarations.h"

// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"
extern TypeInfo TrackableBehaviour_t44_il2cpp_TypeInfo;
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviourMethodDeclarations.h"
// UnityEngine.Collider
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
extern MethodInfo Component_GetComponent_TisTrackableBehaviour_t44_m413_MethodInfo;
extern MethodInfo TrackableBehaviour_RegisterTrackableEventHandler_m414_MethodInfo;
extern MethodInfo DefaultTrackableEventHandler_OnTrackingFound_m106_MethodInfo;
extern MethodInfo DefaultTrackableEventHandler_OnTrackingLost_m107_MethodInfo;
extern MethodInfo Component_GetComponentsInChildren_TisCollider_t132_m415_MethodInfo;
extern MethodInfo Renderer_set_enabled_m416_MethodInfo;
extern MethodInfo Collider_set_enabled_m417_MethodInfo;
extern MethodInfo String_Concat_m418_MethodInfo;
extern MethodInfo Debug_Log_m419_MethodInfo;
struct Component_t100;
// UnityEngine.CastHelper`1<Vuforia.TrackableBehaviour>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_5.h"
// Declaration !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
// !!0 UnityEngine.Component::GetComponent<Vuforia.TrackableBehaviour>()
#define Component_GetComponent_TisTrackableBehaviour_t44_m413(__this, method) (TrackableBehaviour_t44 *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)
struct Component_t100;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Collider>(System.Boolean)
#define Component_GetComponentsInChildren_TisCollider_t132_m415(__this, p0, method) (ColliderU5BU5D_t133*)Component_GetComponentsInChildren_TisObject_t_m311_gshared((Component_t100 *)__this, (bool)p0, method)


// System.Void Vuforia.DefaultTrackableEventHandler::.ctor()
extern MethodInfo DefaultTrackableEventHandler__ctor_m103_MethodInfo;
 void DefaultTrackableEventHandler__ctor_m103 (DefaultTrackableEventHandler_t45 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::Start()
extern MethodInfo DefaultTrackableEventHandler_Start_m104_MethodInfo;
 void DefaultTrackableEventHandler_Start_m104 (DefaultTrackableEventHandler_t45 * __this, MethodInfo* method){
	{
		TrackableBehaviour_t44 * L_0 = Component_GetComponent_TisTrackableBehaviour_t44_m413(__this, /*hidden argument*/&Component_GetComponent_TisTrackableBehaviour_t44_m413_MethodInfo);
		__this->___mTrackableBehaviour_2 = L_0;
		TrackableBehaviour_t44 * L_1 = (__this->___mTrackableBehaviour_2);
		bool L_2 = Object_op_Implicit_m397(NULL /*static, unused*/, L_1, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t44 * L_3 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m414(L_3, __this, /*hidden argument*/&TrackableBehaviour_RegisterTrackableEventHandler_m414_MethodInfo);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern MethodInfo DefaultTrackableEventHandler_OnTrackableStateChanged_m105_MethodInfo;
 void DefaultTrackableEventHandler_OnTrackableStateChanged_m105 (DefaultTrackableEventHandler_t45 * __this, int32_t ___previousStatus, int32_t ___newStatus, MethodInfo* method){
	{
		if ((((int32_t)___newStatus) == ((int32_t)2)))
		{
			goto IL_0015;
		}
	}
	{
		if ((((int32_t)___newStatus) == ((int32_t)3)))
		{
			goto IL_0015;
		}
	}
	{
		if ((((uint32_t)___newStatus) != ((uint32_t)4)))
		{
			goto IL_0020;
		}
	}

IL_0015:
	{
		DefaultTrackableEventHandler_OnTrackingFound_m106(__this, /*hidden argument*/&DefaultTrackableEventHandler_OnTrackingFound_m106_MethodInfo);
		goto IL_0026;
	}

IL_0020:
	{
		DefaultTrackableEventHandler_OnTrackingLost_m107(__this, /*hidden argument*/&DefaultTrackableEventHandler_OnTrackingLost_m107_MethodInfo);
	}

IL_0026:
	{
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingFound()
 void DefaultTrackableEventHandler_OnTrackingFound_m106 (DefaultTrackableEventHandler_t45 * __this, MethodInfo* method){
	RendererU5BU5D_t116* V_0 = {0};
	ColliderU5BU5D_t133* V_1 = {0};
	Renderer_t7 * V_2 = {0};
	RendererU5BU5D_t116* V_3 = {0};
	int32_t V_4 = 0;
	Collider_t132 * V_5 = {0};
	ColliderU5BU5D_t133* V_6 = {0};
	int32_t V_7 = 0;
	{
		RendererU5BU5D_t116* L_0 = Component_GetComponentsInChildren_TisRenderer_t7_m310(__this, 1, /*hidden argument*/&Component_GetComponentsInChildren_TisRenderer_t7_m310_MethodInfo);
		V_0 = L_0;
		ColliderU5BU5D_t133* L_1 = Component_GetComponentsInChildren_TisCollider_t132_m415(__this, 1, /*hidden argument*/&Component_GetComponentsInChildren_TisCollider_t132_m415_MethodInfo);
		V_1 = L_1;
		V_3 = V_0;
		V_4 = 0;
		goto IL_002c;
	}

IL_001a:
	{
		NullCheck(V_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_3, V_4);
		int32_t L_2 = V_4;
		V_2 = (*(Renderer_t7 **)(Renderer_t7 **)SZArrayLdElema(V_3, L_2));
		NullCheck(V_2);
		Renderer_set_enabled_m416(V_2, 1, /*hidden argument*/&Renderer_set_enabled_m416_MethodInfo);
		V_4 = ((int32_t)(V_4+1));
	}

IL_002c:
	{
		NullCheck(V_3);
		if ((((int32_t)V_4) < ((int32_t)(((int32_t)(((Array_t *)V_3)->max_length))))))
		{
			goto IL_001a;
		}
	}
	{
		V_6 = V_1;
		V_7 = 0;
		goto IL_0056;
	}

IL_0041:
	{
		NullCheck(V_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_6, V_7);
		int32_t L_3 = V_7;
		V_5 = (*(Collider_t132 **)(Collider_t132 **)SZArrayLdElema(V_6, L_3));
		NullCheck(V_5);
		Collider_set_enabled_m417(V_5, 1, /*hidden argument*/&Collider_set_enabled_m417_MethodInfo);
		V_7 = ((int32_t)(V_7+1));
	}

IL_0056:
	{
		NullCheck(V_6);
		if ((((int32_t)V_7) < ((int32_t)(((int32_t)(((Array_t *)V_6)->max_length))))))
		{
			goto IL_0041;
		}
	}
	{
		TrackableBehaviour_t44 * L_4 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_4);
		String_t* L_5 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&TrackableBehaviour_get_TrackableName_m345_MethodInfo, L_4);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_6 = String_Concat_m418(NULL /*static, unused*/, (String_t*) &_stringLiteral46, L_5, (String_t*) &_stringLiteral47, /*hidden argument*/&String_Concat_m418_MethodInfo);
		Debug_Log_m419(NULL /*static, unused*/, L_6, /*hidden argument*/&Debug_Log_m419_MethodInfo);
		return;
	}
}
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingLost()
 void DefaultTrackableEventHandler_OnTrackingLost_m107 (DefaultTrackableEventHandler_t45 * __this, MethodInfo* method){
	RendererU5BU5D_t116* V_0 = {0};
	ColliderU5BU5D_t133* V_1 = {0};
	Renderer_t7 * V_2 = {0};
	RendererU5BU5D_t116* V_3 = {0};
	int32_t V_4 = 0;
	Collider_t132 * V_5 = {0};
	ColliderU5BU5D_t133* V_6 = {0};
	int32_t V_7 = 0;
	{
		RendererU5BU5D_t116* L_0 = Component_GetComponentsInChildren_TisRenderer_t7_m310(__this, 1, /*hidden argument*/&Component_GetComponentsInChildren_TisRenderer_t7_m310_MethodInfo);
		V_0 = L_0;
		ColliderU5BU5D_t133* L_1 = Component_GetComponentsInChildren_TisCollider_t132_m415(__this, 1, /*hidden argument*/&Component_GetComponentsInChildren_TisCollider_t132_m415_MethodInfo);
		V_1 = L_1;
		V_3 = V_0;
		V_4 = 0;
		goto IL_002c;
	}

IL_001a:
	{
		NullCheck(V_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_3, V_4);
		int32_t L_2 = V_4;
		V_2 = (*(Renderer_t7 **)(Renderer_t7 **)SZArrayLdElema(V_3, L_2));
		NullCheck(V_2);
		Renderer_set_enabled_m416(V_2, 0, /*hidden argument*/&Renderer_set_enabled_m416_MethodInfo);
		V_4 = ((int32_t)(V_4+1));
	}

IL_002c:
	{
		NullCheck(V_3);
		if ((((int32_t)V_4) < ((int32_t)(((int32_t)(((Array_t *)V_3)->max_length))))))
		{
			goto IL_001a;
		}
	}
	{
		V_6 = V_1;
		V_7 = 0;
		goto IL_0056;
	}

IL_0041:
	{
		NullCheck(V_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_6, V_7);
		int32_t L_3 = V_7;
		V_5 = (*(Collider_t132 **)(Collider_t132 **)SZArrayLdElema(V_6, L_3));
		NullCheck(V_5);
		Collider_set_enabled_m417(V_5, 0, /*hidden argument*/&Collider_set_enabled_m417_MethodInfo);
		V_7 = ((int32_t)(V_7+1));
	}

IL_0056:
	{
		NullCheck(V_6);
		if ((((int32_t)V_7) < ((int32_t)(((int32_t)(((Array_t *)V_6)->max_length))))))
		{
			goto IL_0041;
		}
	}
	{
		TrackableBehaviour_t44 * L_4 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_4);
		String_t* L_5 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&TrackableBehaviour_get_TrackableName_m345_MethodInfo, L_4);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_6 = String_Concat_m418(NULL /*static, unused*/, (String_t*) &_stringLiteral46, L_5, (String_t*) &_stringLiteral48, /*hidden argument*/&String_Concat_m418_MethodInfo);
		Debug_Log_m419(NULL /*static, unused*/, L_6, /*hidden argument*/&Debug_Log_m419_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.DefaultTrackableEventHandler
extern Il2CppType TrackableBehaviour_t44_0_0_1;
FieldInfo DefaultTrackableEventHandler_t45____mTrackableBehaviour_2_FieldInfo = 
{
	"mTrackableBehaviour"/* name */
	, &TrackableBehaviour_t44_0_0_1/* type */
	, &DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* parent */
	, offsetof(DefaultTrackableEventHandler_t45, ___mTrackableBehaviour_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* DefaultTrackableEventHandler_t45_FieldInfos[] =
{
	&DefaultTrackableEventHandler_t45____mTrackableBehaviour_2_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::.ctor()
MethodInfo DefaultTrackableEventHandler__ctor_m103_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler__ctor_m103/* method */
	, &DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 104/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::Start()
MethodInfo DefaultTrackableEventHandler_Start_m104_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler_Start_m104/* method */
	, &DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 105/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Status_t134_0_0_0;
extern Il2CppType Status_t134_0_0_0;
extern Il2CppType Status_t134_0_0_0;
static ParameterInfo DefaultTrackableEventHandler_t45_DefaultTrackableEventHandler_OnTrackableStateChanged_m105_ParameterInfos[] = 
{
	{"previousStatus", 0, 134217774, &EmptyCustomAttributesCache, &Status_t134_0_0_0},
	{"newStatus", 1, 134217775, &EmptyCustomAttributesCache, &Status_t134_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
MethodInfo DefaultTrackableEventHandler_OnTrackableStateChanged_m105_MethodInfo = 
{
	"OnTrackableStateChanged"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler_OnTrackableStateChanged_m105/* method */
	, &DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, DefaultTrackableEventHandler_t45_DefaultTrackableEventHandler_OnTrackableStateChanged_m105_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 106/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingFound()
MethodInfo DefaultTrackableEventHandler_OnTrackingFound_m106_MethodInfo = 
{
	"OnTrackingFound"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler_OnTrackingFound_m106/* method */
	, &DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 107/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingLost()
MethodInfo DefaultTrackableEventHandler_OnTrackingLost_m107_MethodInfo = 
{
	"OnTrackingLost"/* name */
	, (methodPointerType)&DefaultTrackableEventHandler_OnTrackingLost_m107/* method */
	, &DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 108/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* DefaultTrackableEventHandler_t45_MethodInfos[] =
{
	&DefaultTrackableEventHandler__ctor_m103_MethodInfo,
	&DefaultTrackableEventHandler_Start_m104_MethodInfo,
	&DefaultTrackableEventHandler_OnTrackableStateChanged_m105_MethodInfo,
	&DefaultTrackableEventHandler_OnTrackingFound_m106_MethodInfo,
	&DefaultTrackableEventHandler_OnTrackingLost_m107_MethodInfo,
	NULL
};
static MethodInfo* DefaultTrackableEventHandler_t45_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&DefaultTrackableEventHandler_OnTrackableStateChanged_m105_MethodInfo,
};
extern TypeInfo ITrackableEventHandler_t135_il2cpp_TypeInfo;
static TypeInfo* DefaultTrackableEventHandler_t45_InterfacesTypeInfos[] = 
{
	&ITrackableEventHandler_t135_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair DefaultTrackableEventHandler_t45_InterfacesOffsets[] = 
{
	{ &ITrackableEventHandler_t135_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
extern Il2CppType DefaultTrackableEventHandler_t45_1_0_0;
struct DefaultTrackableEventHandler_t45;
TypeInfo DefaultTrackableEventHandler_t45_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultTrackableEventHandler"/* name */
	, "Vuforia"/* namespaze */
	, DefaultTrackableEventHandler_t45_MethodInfos/* methods */
	, NULL/* properties */
	, DefaultTrackableEventHandler_t45_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* element_class */
	, DefaultTrackableEventHandler_t45_InterfacesTypeInfos/* implemented_interfaces */
	, DefaultTrackableEventHandler_t45_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* cast_class */
	, &DefaultTrackableEventHandler_t45_0_0_0/* byval_arg */
	, &DefaultTrackableEventHandler_t45_1_0_0/* this_arg */
	, DefaultTrackableEventHandler_t45_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultTrackableEventHandler_t45)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GLErrorHandler_t46_il2cpp_TypeInfo;
// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandlerMethodDeclarations.h"

extern MethodInfo GLErrorHandler_DrawWindowContent_m112_MethodInfo;


// System.Void Vuforia.GLErrorHandler::.ctor()
extern MethodInfo GLErrorHandler__ctor_m108_MethodInfo;
 void GLErrorHandler__ctor_m108 (GLErrorHandler_t46 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::.cctor()
extern MethodInfo GLErrorHandler__cctor_m109_MethodInfo;
 void GLErrorHandler__cctor_m109 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		((GLErrorHandler_t46_StaticFields*)InitializedTypeInfo(&GLErrorHandler_t46_il2cpp_TypeInfo)->static_fields)->___mErrorText_3 = (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::SetError(System.String)
extern MethodInfo GLErrorHandler_SetError_m110_MethodInfo;
 void GLErrorHandler_SetError_m110 (Object_t * __this/* static, unused */, String_t* ___errorText, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GLErrorHandler_t46_il2cpp_TypeInfo));
		((GLErrorHandler_t46_StaticFields*)InitializedTypeInfo(&GLErrorHandler_t46_il2cpp_TypeInfo)->static_fields)->___mErrorText_3 = ___errorText;
		((GLErrorHandler_t46_StaticFields*)InitializedTypeInfo(&GLErrorHandler_t46_il2cpp_TypeInfo)->static_fields)->___mErrorOccurred_4 = 1;
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::OnGUI()
extern MethodInfo GLErrorHandler_OnGUI_m111_MethodInfo;
 void GLErrorHandler_OnGUI_m111 (GLErrorHandler_t46 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GLErrorHandler_t46_il2cpp_TypeInfo));
		if (!(((GLErrorHandler_t46_StaticFields*)InitializedTypeInfo(&GLErrorHandler_t46_il2cpp_TypeInfo)->static_fields)->___mErrorOccurred_4))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_0 = Screen_get_width_m273(NULL /*static, unused*/, /*hidden argument*/&Screen_get_width_m273_MethodInfo);
		int32_t L_1 = Screen_get_height_m274(NULL /*static, unused*/, /*hidden argument*/&Screen_get_height_m274_MethodInfo);
		Rect_t118  L_2 = {0};
		Rect__ctor_m332(&L_2, (0.0f), (0.0f), (((float)L_0)), (((float)L_1)), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		IntPtr_t121 L_3 = { &GLErrorHandler_DrawWindowContent_m112_MethodInfo };
		WindowFunction_t119 * L_4 = (WindowFunction_t119 *)il2cpp_codegen_object_new (InitializedTypeInfo(&WindowFunction_t119_il2cpp_TypeInfo));
		WindowFunction__ctor_m333(L_4, __this, L_3, /*hidden argument*/&WindowFunction__ctor_m333_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUI_t120_il2cpp_TypeInfo));
		GUI_Window_m334(NULL /*static, unused*/, 0, L_2, L_4, (String_t*) &_stringLiteral49, /*hidden argument*/&GUI_Window_m334_MethodInfo);
	}

IL_003d:
	{
		return;
	}
}
// System.Void Vuforia.GLErrorHandler::DrawWindowContent(System.Int32)
 void GLErrorHandler_DrawWindowContent_m112 (GLErrorHandler_t46 * __this, int32_t ___id, MethodInfo* method){
	{
		int32_t L_0 = Screen_get_width_m273(NULL /*static, unused*/, /*hidden argument*/&Screen_get_width_m273_MethodInfo);
		int32_t L_1 = Screen_get_height_m274(NULL /*static, unused*/, /*hidden argument*/&Screen_get_height_m274_MethodInfo);
		Rect_t118  L_2 = {0};
		Rect__ctor_m332(&L_2, (10.0f), (25.0f), (((float)((int32_t)(L_0-((int32_t)20))))), (((float)((int32_t)(L_1-((int32_t)95))))), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GLErrorHandler_t46_il2cpp_TypeInfo));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUI_t120_il2cpp_TypeInfo));
		GUI_Label_m339(NULL /*static, unused*/, L_2, (((GLErrorHandler_t46_StaticFields*)InitializedTypeInfo(&GLErrorHandler_t46_il2cpp_TypeInfo)->static_fields)->___mErrorText_3), /*hidden argument*/&GUI_Label_m339_MethodInfo);
		int32_t L_3 = Screen_get_width_m273(NULL /*static, unused*/, /*hidden argument*/&Screen_get_width_m273_MethodInfo);
		int32_t L_4 = Screen_get_height_m274(NULL /*static, unused*/, /*hidden argument*/&Screen_get_height_m274_MethodInfo);
		Rect_t118  L_5 = {0};
		Rect__ctor_m332(&L_5, (((float)((int32_t)(((int32_t)((int32_t)L_3/(int32_t)2))-((int32_t)75))))), (((float)((int32_t)(L_4-((int32_t)60))))), (150.0f), (50.0f), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		bool L_6 = GUI_Button_m401(NULL /*static, unused*/, L_5, (String_t*) &_stringLiteral34, /*hidden argument*/&GUI_Button_m401_MethodInfo);
		if (!L_6)
		{
			goto IL_0062;
		}
	}
	{
		Application_Quit_m402(NULL /*static, unused*/, /*hidden argument*/&Application_Quit_m402_MethodInfo);
	}

IL_0062:
	{
		return;
	}
}
// Metadata Definition Vuforia.GLErrorHandler
extern Il2CppType String_t_0_0_32849;
FieldInfo GLErrorHandler_t46____WINDOW_TITLE_2_FieldInfo = 
{
	"WINDOW_TITLE"/* name */
	, &String_t_0_0_32849/* type */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_17;
FieldInfo GLErrorHandler_t46____mErrorText_3_FieldInfo = 
{
	"mErrorText"/* name */
	, &String_t_0_0_17/* type */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* parent */
	, offsetof(GLErrorHandler_t46_StaticFields, ___mErrorText_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_17;
FieldInfo GLErrorHandler_t46____mErrorOccurred_4_FieldInfo = 
{
	"mErrorOccurred"/* name */
	, &Boolean_t106_0_0_17/* type */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* parent */
	, offsetof(GLErrorHandler_t46_StaticFields, ___mErrorOccurred_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* GLErrorHandler_t46_FieldInfos[] =
{
	&GLErrorHandler_t46____WINDOW_TITLE_2_FieldInfo,
	&GLErrorHandler_t46____mErrorText_3_FieldInfo,
	&GLErrorHandler_t46____mErrorOccurred_4_FieldInfo,
	NULL
};
static const uint16_t GLErrorHandler_t46____WINDOW_TITLE_2_DefaultValueData[] = { 0x53, 0x61, 0x6D, 0x70, 0x6C, 0x65, 0x20, 0x45, 0x72, 0x72, 0x6F, 0x72, 0x00 };
static Il2CppFieldDefaultValueEntry GLErrorHandler_t46____WINDOW_TITLE_2_DefaultValue = 
{
	&GLErrorHandler_t46____WINDOW_TITLE_2_FieldInfo/* field */
	, { (char*)&GLErrorHandler_t46____WINDOW_TITLE_2_DefaultValueData, &String_t_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* GLErrorHandler_t46_FieldDefaultValues[] = 
{
	&GLErrorHandler_t46____WINDOW_TITLE_2_DefaultValue,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::.ctor()
MethodInfo GLErrorHandler__ctor_m108_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GLErrorHandler__ctor_m108/* method */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 109/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::.cctor()
MethodInfo GLErrorHandler__cctor_m109_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&GLErrorHandler__cctor_m109/* method */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 110/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo GLErrorHandler_t46_GLErrorHandler_SetError_m110_ParameterInfos[] = 
{
	{"errorText", 0, 134217776, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::SetError(System.String)
MethodInfo GLErrorHandler_SetError_m110_MethodInfo = 
{
	"SetError"/* name */
	, (methodPointerType)&GLErrorHandler_SetError_m110/* method */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, GLErrorHandler_t46_GLErrorHandler_SetError_m110_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 111/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::OnGUI()
MethodInfo GLErrorHandler_OnGUI_m111_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&GLErrorHandler_OnGUI_m111/* method */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 112/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo GLErrorHandler_t46_GLErrorHandler_DrawWindowContent_m112_ParameterInfos[] = 
{
	{"id", 0, 134217777, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.GLErrorHandler::DrawWindowContent(System.Int32)
MethodInfo GLErrorHandler_DrawWindowContent_m112_MethodInfo = 
{
	"DrawWindowContent"/* name */
	, (methodPointerType)&GLErrorHandler_DrawWindowContent_m112/* method */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, GLErrorHandler_t46_GLErrorHandler_DrawWindowContent_m112_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 113/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* GLErrorHandler_t46_MethodInfos[] =
{
	&GLErrorHandler__ctor_m108_MethodInfo,
	&GLErrorHandler__cctor_m109_MethodInfo,
	&GLErrorHandler_SetError_m110_MethodInfo,
	&GLErrorHandler_OnGUI_m111_MethodInfo,
	&GLErrorHandler_DrawWindowContent_m112_MethodInfo,
	NULL
};
static MethodInfo* GLErrorHandler_t46_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType GLErrorHandler_t46_0_0_0;
extern Il2CppType GLErrorHandler_t46_1_0_0;
struct GLErrorHandler_t46;
TypeInfo GLErrorHandler_t46_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "GLErrorHandler"/* name */
	, "Vuforia"/* namespaze */
	, GLErrorHandler_t46_MethodInfos/* methods */
	, NULL/* properties */
	, GLErrorHandler_t46_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, GLErrorHandler_t46_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GLErrorHandler_t46_il2cpp_TypeInfo/* cast_class */
	, &GLErrorHandler_t46_0_0_0/* byval_arg */
	, &GLErrorHandler_t46_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, GLErrorHandler_t46_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GLErrorHandler_t46)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(GLErrorHandler_t46_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo HideExcessAreaBehaviour_t47_il2cpp_TypeInfo;
// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviourMethodDeclarations.h"

// Vuforia.HideExcessAreaAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstrMethodDeclarations.h"
extern MethodInfo HideExcessAreaAbstractBehaviour__ctor_m420_MethodInfo;


// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
extern MethodInfo HideExcessAreaBehaviour__ctor_m113_MethodInfo;
 void HideExcessAreaBehaviour__ctor_m113 (HideExcessAreaBehaviour_t47 * __this, MethodInfo* method){
	{
		HideExcessAreaAbstractBehaviour__ctor_m420(__this, /*hidden argument*/&HideExcessAreaAbstractBehaviour__ctor_m420_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.HideExcessAreaBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.HideExcessAreaBehaviour::.ctor()
MethodInfo HideExcessAreaBehaviour__ctor_m113_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&HideExcessAreaBehaviour__ctor_m113/* method */
	, &HideExcessAreaBehaviour_t47_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 114/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* HideExcessAreaBehaviour_t47_MethodInfos[] =
{
	&HideExcessAreaBehaviour__ctor_m113_MethodInfo,
	NULL
};
static MethodInfo* HideExcessAreaBehaviour_t47_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
extern Il2CppType HideExcessAreaBehaviour_t47_1_0_0;
extern TypeInfo HideExcessAreaAbstractBehaviour_t48_il2cpp_TypeInfo;
struct HideExcessAreaBehaviour_t47;
TypeInfo HideExcessAreaBehaviour_t47_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "HideExcessAreaBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, HideExcessAreaBehaviour_t47_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &HideExcessAreaAbstractBehaviour_t48_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &HideExcessAreaBehaviour_t47_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, HideExcessAreaBehaviour_t47_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &HideExcessAreaBehaviour_t47_il2cpp_TypeInfo/* cast_class */
	, &HideExcessAreaBehaviour_t47_0_0_0/* byval_arg */
	, &HideExcessAreaBehaviour_t47_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (HideExcessAreaBehaviour_t47)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ImageTargetBehaviour_t49_il2cpp_TypeInfo;
// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviourMethodDeclarations.h"

// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstractMethodDeclarations.h"
extern MethodInfo ImageTargetAbstractBehaviour__ctor_m421_MethodInfo;


// System.Void Vuforia.ImageTargetBehaviour::.ctor()
extern MethodInfo ImageTargetBehaviour__ctor_m114_MethodInfo;
 void ImageTargetBehaviour__ctor_m114 (ImageTargetBehaviour_t49 * __this, MethodInfo* method){
	{
		ImageTargetAbstractBehaviour__ctor_m421(__this, /*hidden argument*/&ImageTargetAbstractBehaviour__ctor_m421_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.ImageTargetBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ImageTargetBehaviour::.ctor()
MethodInfo ImageTargetBehaviour__ctor_m114_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ImageTargetBehaviour__ctor_m114/* method */
	, &ImageTargetBehaviour_t49_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 115/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ImageTargetBehaviour_t49_MethodInfos[] =
{
	&ImageTargetBehaviour__ctor_m114_MethodInfo,
	NULL
};
extern MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m422_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m423_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m424_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m425_MethodInfo;
extern MethodInfo TrackableBehaviour_OnFrameIndexUpdate_m426_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_InternalUnregisterTrackable_m427_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_CorrectScaleImpl_m428_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m429_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m430_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_AspectRatio_m431_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_ImageTargetType_m432_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetAspectRatio_m433_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetImageTargetType_m434_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_GetSize_m435_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_SetWidth_m436_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_SetHeight_m437_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_InitializeImageTarget_m438_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_CreateMissingVirtualButtonBehaviours_m439_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_TryGetVirtualButtonBehaviourByID_m440_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_AssociateExistingVirtualButtonBehaviour_m441_MethodInfo;
static MethodInfo* ImageTargetBehaviour_t49_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m345_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m346_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m347_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m349_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m350_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m351_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m352_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m353_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m354_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m355_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m422_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m423_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m424_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m425_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m360_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&DataSetTrackableBehaviour_OnTrackerUpdate_m361_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m426_MethodInfo,
	&ImageTargetAbstractBehaviour_InternalUnregisterTrackable_m427_MethodInfo,
	&ImageTargetAbstractBehaviour_CorrectScaleImpl_m428_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m365_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m366_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m367_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m368_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m369_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m370_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m371_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m372_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m373_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m374_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m375_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m376_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m377_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m378_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m379_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m380_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m381_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m382_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m383_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m384_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m385_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m386_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m387_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m388_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&DataSetTrackableBehaviour_OnDrawGizmos_m389_MethodInfo,
	&ImageTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m429_MethodInfo,
	&ImageTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m430_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_AspectRatio_m431_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_ImageTargetType_m432_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetAspectRatio_m433_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetImageTargetType_m434_MethodInfo,
	&ImageTargetAbstractBehaviour_GetSize_m435_MethodInfo,
	&ImageTargetAbstractBehaviour_SetWidth_m436_MethodInfo,
	&ImageTargetAbstractBehaviour_SetHeight_m437_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_InitializeImageTarget_m438_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_CreateMissingVirtualButtonBehaviours_m439_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_TryGetVirtualButtonBehaviourByID_m440_MethodInfo,
	&ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_AssociateExistingVirtualButtonBehaviour_m441_MethodInfo,
};
extern TypeInfo IEditorImageTargetBehaviour_t136_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair ImageTargetBehaviour_t49_InterfacesOffsets[] = 
{
	{ &IEditorImageTargetBehaviour_t136_il2cpp_TypeInfo, 53},
	{ &IEditorDataSetTrackableBehaviour_t124_il2cpp_TypeInfo, 25},
	{ &IEditorTrackableBehaviour_t125_il2cpp_TypeInfo, 4},
	{ &WorldCenterTrackableBehaviour_t126_il2cpp_TypeInfo, 49},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType ImageTargetBehaviour_t49_0_0_0;
extern Il2CppType ImageTargetBehaviour_t49_1_0_0;
extern TypeInfo ImageTargetAbstractBehaviour_t50_il2cpp_TypeInfo;
struct ImageTargetBehaviour_t49;
TypeInfo ImageTargetBehaviour_t49_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ImageTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ImageTargetBehaviour_t49_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ImageTargetAbstractBehaviour_t50_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ImageTargetBehaviour_t49_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ImageTargetBehaviour_t49_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ImageTargetBehaviour_t49_il2cpp_TypeInfo/* cast_class */
	, &ImageTargetBehaviour_t49_0_0_0/* byval_arg */
	, &ImageTargetBehaviour_t49_1_0_0/* this_arg */
	, ImageTargetBehaviour_t49_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ImageTargetBehaviour_t49)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 64/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
// Vuforia.AndroidUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo AndroidUnityPlayer_t51_il2cpp_TypeInfo;
// Vuforia.AndroidUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayerMethodDeclarations.h"

// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
extern TypeInfo SurfaceUtilities_t137_il2cpp_TypeInfo;
// Vuforia.SurfaceUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUtilitiesMethodDeclarations.h"
// Vuforia.QCARUnity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnityMethodDeclarations.h"
extern MethodInfo AndroidUnityPlayer_LoadNativeLibrariesFromJava_m124_MethodInfo;
extern MethodInfo AndroidUnityPlayer_InitAndroidPlatform_m125_MethodInfo;
extern MethodInfo AndroidUnityPlayer_InitQCAR_m126_MethodInfo;
extern MethodInfo AndroidUnityPlayer_InitializeSurface_m127_MethodInfo;
extern MethodInfo SurfaceUtilities_HasSurfaceBeenRecreated_m442_MethodInfo;
extern MethodInfo Screen_get_orientation_m443_MethodInfo;
extern MethodInfo AndroidUnityPlayer_ResetUnityScreenOrientation_m128_MethodInfo;
extern MethodInfo AndroidUnityPlayer_CheckOrientation_m129_MethodInfo;
extern MethodInfo QCARUnity_OnPause_m444_MethodInfo;
extern MethodInfo QCARUnity_OnResume_m445_MethodInfo;
extern MethodInfo QCARUnity_Deinit_m446_MethodInfo;
extern MethodInfo SurfaceUtilities_OnSurfaceCreated_m447_MethodInfo;
extern MethodInfo SurfaceUtilities_SetSurfaceOrientation_m448_MethodInfo;


// System.Void Vuforia.AndroidUnityPlayer::.ctor()
extern MethodInfo AndroidUnityPlayer__ctor_m115_MethodInfo;
 void AndroidUnityPlayer__ctor_m115 (AndroidUnityPlayer_t51 * __this, MethodInfo* method){
	{
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibraries()
extern MethodInfo AndroidUnityPlayer_LoadNativeLibraries_m116_MethodInfo;
 void AndroidUnityPlayer_LoadNativeLibraries_m116 (AndroidUnityPlayer_t51 * __this, MethodInfo* method){
	{
		AndroidUnityPlayer_LoadNativeLibrariesFromJava_m124(__this, /*hidden argument*/&AndroidUnityPlayer_LoadNativeLibrariesFromJava_m124_MethodInfo);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializePlatform()
extern MethodInfo AndroidUnityPlayer_InitializePlatform_m117_MethodInfo;
 void AndroidUnityPlayer_InitializePlatform_m117 (AndroidUnityPlayer_t51 * __this, MethodInfo* method){
	{
		AndroidUnityPlayer_InitAndroidPlatform_m125(__this, /*hidden argument*/&AndroidUnityPlayer_InitAndroidPlatform_m125_MethodInfo);
		return;
	}
}
// Vuforia.QCARUnity/InitError Vuforia.AndroidUnityPlayer::Start(System.String)
extern MethodInfo AndroidUnityPlayer_Start_m118_MethodInfo;
 int32_t AndroidUnityPlayer_Start_m118 (AndroidUnityPlayer_t51 * __this, String_t* ___licenseKey, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = AndroidUnityPlayer_InitQCAR_m126(__this, ___licenseKey, /*hidden argument*/&AndroidUnityPlayer_InitQCAR_m126_MethodInfo);
		V_0 = L_0;
		if ((((int32_t)V_0) < ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m127(__this, /*hidden argument*/&AndroidUnityPlayer_InitializeSurface_m127_MethodInfo);
	}

IL_0015:
	{
		return (int32_t)(V_0);
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Update()
extern MethodInfo AndroidUnityPlayer_Update_m119_MethodInfo;
 void AndroidUnityPlayer_Update_m119 (AndroidUnityPlayer_t51 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SurfaceUtilities_t137_il2cpp_TypeInfo));
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m442(NULL /*static, unused*/, /*hidden argument*/&SurfaceUtilities_HasSurfaceBeenRecreated_m442_MethodInfo);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AndroidUnityPlayer_InitializeSurface_m127(__this, /*hidden argument*/&AndroidUnityPlayer_InitializeSurface_m127_MethodInfo);
		goto IL_0031;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m443(NULL /*static, unused*/, /*hidden argument*/&Screen_get_orientation_m443_MethodInfo);
		int32_t L_2 = (__this->___mScreenOrientation_2);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		AndroidUnityPlayer_ResetUnityScreenOrientation_m128(__this, /*hidden argument*/&AndroidUnityPlayer_ResetUnityScreenOrientation_m128_MethodInfo);
	}

IL_002b:
	{
		AndroidUnityPlayer_CheckOrientation_m129(__this, /*hidden argument*/&AndroidUnityPlayer_CheckOrientation_m129_MethodInfo);
	}

IL_0031:
	{
		int32_t L_3 = (__this->___mFramesSinceLastOrientationReset_4);
		__this->___mFramesSinceLastOrientationReset_4 = ((int32_t)(L_3+1));
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnPause()
extern MethodInfo AndroidUnityPlayer_OnPause_m120_MethodInfo;
 void AndroidUnityPlayer_OnPause_m120 (AndroidUnityPlayer_t51 * __this, MethodInfo* method){
	{
		QCARUnity_OnPause_m444(NULL /*static, unused*/, /*hidden argument*/&QCARUnity_OnPause_m444_MethodInfo);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnResume()
extern MethodInfo AndroidUnityPlayer_OnResume_m121_MethodInfo;
 void AndroidUnityPlayer_OnResume_m121 (AndroidUnityPlayer_t51 * __this, MethodInfo* method){
	{
		QCARUnity_OnResume_m445(NULL /*static, unused*/, /*hidden argument*/&QCARUnity_OnResume_m445_MethodInfo);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::OnDestroy()
extern MethodInfo AndroidUnityPlayer_OnDestroy_m122_MethodInfo;
 void AndroidUnityPlayer_OnDestroy_m122 (AndroidUnityPlayer_t51 * __this, MethodInfo* method){
	{
		QCARUnity_Deinit_m446(NULL /*static, unused*/, /*hidden argument*/&QCARUnity_Deinit_m446_MethodInfo);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::Dispose()
extern MethodInfo AndroidUnityPlayer_Dispose_m123_MethodInfo;
 void AndroidUnityPlayer_Dispose_m123 (AndroidUnityPlayer_t51 * __this, MethodInfo* method){
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibrariesFromJava()
 void AndroidUnityPlayer_LoadNativeLibrariesFromJava_m124 (AndroidUnityPlayer_t51 * __this, MethodInfo* method){
	{
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitAndroidPlatform()
 void AndroidUnityPlayer_InitAndroidPlatform_m125 (AndroidUnityPlayer_t51 * __this, MethodInfo* method){
	{
		return;
	}
}
// System.Int32 Vuforia.AndroidUnityPlayer::InitQCAR(System.String)
 int32_t AndroidUnityPlayer_InitQCAR_m126 (AndroidUnityPlayer_t51 * __this, String_t* ___licenseKey, MethodInfo* method){
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		return V_0;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::InitializeSurface()
 void AndroidUnityPlayer_InitializeSurface_m127 (AndroidUnityPlayer_t51 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SurfaceUtilities_t137_il2cpp_TypeInfo));
		SurfaceUtilities_OnSurfaceCreated_m447(NULL /*static, unused*/, /*hidden argument*/&SurfaceUtilities_OnSurfaceCreated_m447_MethodInfo);
		AndroidUnityPlayer_ResetUnityScreenOrientation_m128(__this, /*hidden argument*/&AndroidUnityPlayer_ResetUnityScreenOrientation_m128_MethodInfo);
		AndroidUnityPlayer_CheckOrientation_m129(__this, /*hidden argument*/&AndroidUnityPlayer_CheckOrientation_m129_MethodInfo);
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::ResetUnityScreenOrientation()
 void AndroidUnityPlayer_ResetUnityScreenOrientation_m128 (AndroidUnityPlayer_t51 * __this, MethodInfo* method){
	{
		int32_t L_0 = Screen_get_orientation_m443(NULL /*static, unused*/, /*hidden argument*/&Screen_get_orientation_m443_MethodInfo);
		__this->___mScreenOrientation_2 = L_0;
		__this->___mFramesSinceLastOrientationReset_4 = 0;
		return;
	}
}
// System.Void Vuforia.AndroidUnityPlayer::CheckOrientation()
 void AndroidUnityPlayer_CheckOrientation_m129 (AndroidUnityPlayer_t51 * __this, MethodInfo* method){
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = (__this->___mFramesSinceLastOrientationReset_4);
		V_0 = ((((int32_t)L_0) < ((int32_t)((int32_t)25)))? 1 : 0);
		if (V_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = (__this->___mFramesSinceLastJavaOrientationCheck_5);
		V_0 = ((((int32_t)L_1) > ((int32_t)((int32_t)60)))? 1 : 0);
	}

IL_001c:
	{
		if (!V_0)
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_2 = (__this->___mScreenOrientation_2);
		V_1 = L_2;
		V_2 = V_1;
		int32_t L_3 = (__this->___mJavaScreenOrientation_3);
		if ((((int32_t)V_2) == ((int32_t)L_3)))
		{
			goto IL_0049;
		}
	}
	{
		__this->___mJavaScreenOrientation_3 = V_2;
		int32_t L_4 = (__this->___mJavaScreenOrientation_3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SurfaceUtilities_t137_il2cpp_TypeInfo));
		SurfaceUtilities_SetSurfaceOrientation_m448(NULL /*static, unused*/, L_4, /*hidden argument*/&SurfaceUtilities_SetSurfaceOrientation_m448_MethodInfo);
	}

IL_0049:
	{
		__this->___mFramesSinceLastJavaOrientationCheck_5 = 0;
		goto IL_0063;
	}

IL_0055:
	{
		int32_t L_5 = (__this->___mFramesSinceLastJavaOrientationCheck_5);
		__this->___mFramesSinceLastJavaOrientationCheck_5 = ((int32_t)(L_5+1));
	}

IL_0063:
	{
		return;
	}
}
// Metadata Definition Vuforia.AndroidUnityPlayer
extern Il2CppType Int32_t93_0_0_32849;
FieldInfo AndroidUnityPlayer_t51____NUM_FRAMES_TO_QUERY_ORIENTATION_0_FieldInfo = 
{
	"NUM_FRAMES_TO_QUERY_ORIENTATION"/* name */
	, &Int32_t93_0_0_32849/* type */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_32849;
FieldInfo AndroidUnityPlayer_t51____JAVA_ORIENTATION_CHECK_FRM_INTERVAL_1_FieldInfo = 
{
	"JAVA_ORIENTATION_CHECK_FRM_INTERVAL"/* name */
	, &Int32_t93_0_0_32849/* type */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t138_0_0_1;
FieldInfo AndroidUnityPlayer_t51____mScreenOrientation_2_FieldInfo = 
{
	"mScreenOrientation"/* name */
	, &ScreenOrientation_t138_0_0_1/* type */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* parent */
	, offsetof(AndroidUnityPlayer_t51, ___mScreenOrientation_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType ScreenOrientation_t138_0_0_1;
FieldInfo AndroidUnityPlayer_t51____mJavaScreenOrientation_3_FieldInfo = 
{
	"mJavaScreenOrientation"/* name */
	, &ScreenOrientation_t138_0_0_1/* type */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* parent */
	, offsetof(AndroidUnityPlayer_t51, ___mJavaScreenOrientation_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo AndroidUnityPlayer_t51____mFramesSinceLastOrientationReset_4_FieldInfo = 
{
	"mFramesSinceLastOrientationReset"/* name */
	, &Int32_t93_0_0_1/* type */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* parent */
	, offsetof(AndroidUnityPlayer_t51, ___mFramesSinceLastOrientationReset_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo AndroidUnityPlayer_t51____mFramesSinceLastJavaOrientationCheck_5_FieldInfo = 
{
	"mFramesSinceLastJavaOrientationCheck"/* name */
	, &Int32_t93_0_0_1/* type */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* parent */
	, offsetof(AndroidUnityPlayer_t51, ___mFramesSinceLastJavaOrientationCheck_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* AndroidUnityPlayer_t51_FieldInfos[] =
{
	&AndroidUnityPlayer_t51____NUM_FRAMES_TO_QUERY_ORIENTATION_0_FieldInfo,
	&AndroidUnityPlayer_t51____JAVA_ORIENTATION_CHECK_FRM_INTERVAL_1_FieldInfo,
	&AndroidUnityPlayer_t51____mScreenOrientation_2_FieldInfo,
	&AndroidUnityPlayer_t51____mJavaScreenOrientation_3_FieldInfo,
	&AndroidUnityPlayer_t51____mFramesSinceLastOrientationReset_4_FieldInfo,
	&AndroidUnityPlayer_t51____mFramesSinceLastJavaOrientationCheck_5_FieldInfo,
	NULL
};
static const int32_t AndroidUnityPlayer_t51____NUM_FRAMES_TO_QUERY_ORIENTATION_0_DefaultValueData = 25;
static Il2CppFieldDefaultValueEntry AndroidUnityPlayer_t51____NUM_FRAMES_TO_QUERY_ORIENTATION_0_DefaultValue = 
{
	&AndroidUnityPlayer_t51____NUM_FRAMES_TO_QUERY_ORIENTATION_0_FieldInfo/* field */
	, { (char*)&AndroidUnityPlayer_t51____NUM_FRAMES_TO_QUERY_ORIENTATION_0_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t AndroidUnityPlayer_t51____JAVA_ORIENTATION_CHECK_FRM_INTERVAL_1_DefaultValueData = 60;
static Il2CppFieldDefaultValueEntry AndroidUnityPlayer_t51____JAVA_ORIENTATION_CHECK_FRM_INTERVAL_1_DefaultValue = 
{
	&AndroidUnityPlayer_t51____JAVA_ORIENTATION_CHECK_FRM_INTERVAL_1_FieldInfo/* field */
	, { (char*)&AndroidUnityPlayer_t51____JAVA_ORIENTATION_CHECK_FRM_INTERVAL_1_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* AndroidUnityPlayer_t51_FieldDefaultValues[] = 
{
	&AndroidUnityPlayer_t51____NUM_FRAMES_TO_QUERY_ORIENTATION_0_DefaultValue,
	&AndroidUnityPlayer_t51____JAVA_ORIENTATION_CHECK_FRM_INTERVAL_1_DefaultValue,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::.ctor()
MethodInfo AndroidUnityPlayer__ctor_m115_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&AndroidUnityPlayer__ctor_m115/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 116/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibraries()
MethodInfo AndroidUnityPlayer_LoadNativeLibraries_m116_MethodInfo = 
{
	"LoadNativeLibraries"/* name */
	, (methodPointerType)&AndroidUnityPlayer_LoadNativeLibraries_m116/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 117/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::InitializePlatform()
MethodInfo AndroidUnityPlayer_InitializePlatform_m117_MethodInfo = 
{
	"InitializePlatform"/* name */
	, (methodPointerType)&AndroidUnityPlayer_InitializePlatform_m117/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 118/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo AndroidUnityPlayer_t51_AndroidUnityPlayer_Start_m118_ParameterInfos[] = 
{
	{"licenseKey", 0, 134217778, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType InitError_t128_0_0_0;
extern void* RuntimeInvoker_InitError_t128_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.QCARUnity/InitError Vuforia.AndroidUnityPlayer::Start(System.String)
MethodInfo AndroidUnityPlayer_Start_m118_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&AndroidUnityPlayer_Start_m118/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &InitError_t128_0_0_0/* return_type */
	, RuntimeInvoker_InitError_t128_Object_t/* invoker_method */
	, AndroidUnityPlayer_t51_AndroidUnityPlayer_Start_m118_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 119/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::Update()
MethodInfo AndroidUnityPlayer_Update_m119_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&AndroidUnityPlayer_Update_m119/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 120/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::OnPause()
MethodInfo AndroidUnityPlayer_OnPause_m120_MethodInfo = 
{
	"OnPause"/* name */
	, (methodPointerType)&AndroidUnityPlayer_OnPause_m120/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 121/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::OnResume()
MethodInfo AndroidUnityPlayer_OnResume_m121_MethodInfo = 
{
	"OnResume"/* name */
	, (methodPointerType)&AndroidUnityPlayer_OnResume_m121/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 122/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::OnDestroy()
MethodInfo AndroidUnityPlayer_OnDestroy_m122_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&AndroidUnityPlayer_OnDestroy_m122/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 123/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::Dispose()
MethodInfo AndroidUnityPlayer_Dispose_m123_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&AndroidUnityPlayer_Dispose_m123/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 124/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::LoadNativeLibrariesFromJava()
MethodInfo AndroidUnityPlayer_LoadNativeLibrariesFromJava_m124_MethodInfo = 
{
	"LoadNativeLibrariesFromJava"/* name */
	, (methodPointerType)&AndroidUnityPlayer_LoadNativeLibrariesFromJava_m124/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 125/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::InitAndroidPlatform()
MethodInfo AndroidUnityPlayer_InitAndroidPlatform_m125_MethodInfo = 
{
	"InitAndroidPlatform"/* name */
	, (methodPointerType)&AndroidUnityPlayer_InitAndroidPlatform_m125/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 126/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo AndroidUnityPlayer_t51_AndroidUnityPlayer_InitQCAR_m126_ParameterInfos[] = 
{
	{"licenseKey", 0, 134217779, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
// System.Int32 Vuforia.AndroidUnityPlayer::InitQCAR(System.String)
MethodInfo AndroidUnityPlayer_InitQCAR_m126_MethodInfo = 
{
	"InitQCAR"/* name */
	, (methodPointerType)&AndroidUnityPlayer_InitQCAR_m126/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, AndroidUnityPlayer_t51_AndroidUnityPlayer_InitQCAR_m126_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 127/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::InitializeSurface()
MethodInfo AndroidUnityPlayer_InitializeSurface_m127_MethodInfo = 
{
	"InitializeSurface"/* name */
	, (methodPointerType)&AndroidUnityPlayer_InitializeSurface_m127/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 128/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::ResetUnityScreenOrientation()
MethodInfo AndroidUnityPlayer_ResetUnityScreenOrientation_m128_MethodInfo = 
{
	"ResetUnityScreenOrientation"/* name */
	, (methodPointerType)&AndroidUnityPlayer_ResetUnityScreenOrientation_m128/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 129/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.AndroidUnityPlayer::CheckOrientation()
MethodInfo AndroidUnityPlayer_CheckOrientation_m129_MethodInfo = 
{
	"CheckOrientation"/* name */
	, (methodPointerType)&AndroidUnityPlayer_CheckOrientation_m129/* method */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 130/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* AndroidUnityPlayer_t51_MethodInfos[] =
{
	&AndroidUnityPlayer__ctor_m115_MethodInfo,
	&AndroidUnityPlayer_LoadNativeLibraries_m116_MethodInfo,
	&AndroidUnityPlayer_InitializePlatform_m117_MethodInfo,
	&AndroidUnityPlayer_Start_m118_MethodInfo,
	&AndroidUnityPlayer_Update_m119_MethodInfo,
	&AndroidUnityPlayer_OnPause_m120_MethodInfo,
	&AndroidUnityPlayer_OnResume_m121_MethodInfo,
	&AndroidUnityPlayer_OnDestroy_m122_MethodInfo,
	&AndroidUnityPlayer_Dispose_m123_MethodInfo,
	&AndroidUnityPlayer_LoadNativeLibrariesFromJava_m124_MethodInfo,
	&AndroidUnityPlayer_InitAndroidPlatform_m125_MethodInfo,
	&AndroidUnityPlayer_InitQCAR_m126_MethodInfo,
	&AndroidUnityPlayer_InitializeSurface_m127_MethodInfo,
	&AndroidUnityPlayer_ResetUnityScreenOrientation_m128_MethodInfo,
	&AndroidUnityPlayer_CheckOrientation_m129_MethodInfo,
	NULL
};
static MethodInfo* AndroidUnityPlayer_t51_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&AndroidUnityPlayer_Dispose_m123_MethodInfo,
	&AndroidUnityPlayer_LoadNativeLibraries_m116_MethodInfo,
	&AndroidUnityPlayer_InitializePlatform_m117_MethodInfo,
	&AndroidUnityPlayer_Start_m118_MethodInfo,
	&AndroidUnityPlayer_Update_m119_MethodInfo,
	&AndroidUnityPlayer_OnPause_m120_MethodInfo,
	&AndroidUnityPlayer_OnResume_m121_MethodInfo,
	&AndroidUnityPlayer_OnDestroy_m122_MethodInfo,
};
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
extern TypeInfo IUnityPlayer_t140_il2cpp_TypeInfo;
static TypeInfo* AndroidUnityPlayer_t51_InterfacesTypeInfos[] = 
{
	&IDisposable_t139_il2cpp_TypeInfo,
	&IUnityPlayer_t140_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair AndroidUnityPlayer_t51_InterfacesOffsets[] = 
{
	{ &IDisposable_t139_il2cpp_TypeInfo, 4},
	{ &IUnityPlayer_t140_il2cpp_TypeInfo, 5},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType AndroidUnityPlayer_t51_0_0_0;
extern Il2CppType AndroidUnityPlayer_t51_1_0_0;
struct AndroidUnityPlayer_t51;
TypeInfo AndroidUnityPlayer_t51_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "AndroidUnityPlayer"/* name */
	, "Vuforia"/* namespaze */
	, AndroidUnityPlayer_t51_MethodInfos/* methods */
	, NULL/* properties */
	, AndroidUnityPlayer_t51_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* element_class */
	, AndroidUnityPlayer_t51_InterfacesTypeInfos/* implemented_interfaces */
	, AndroidUnityPlayer_t51_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &AndroidUnityPlayer_t51_il2cpp_TypeInfo/* cast_class */
	, &AndroidUnityPlayer_t51_0_0_0/* byval_arg */
	, &AndroidUnityPlayer_t51_1_0_0/* this_arg */
	, AndroidUnityPlayer_t51_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, AndroidUnityPlayer_t51_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (AndroidUnityPlayer_t51)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 15/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
// Vuforia.ComponentFactoryStarterBehaviour
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo;
// Vuforia.ComponentFactoryStarterBehaviour
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterBehaviourMethodDeclarations.h"

// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen.h"
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Action
#include "System_Core_System_Action.h"
// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
// Vuforia.FactorySetter
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySetter.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// Vuforia.VuforiaBehaviourComponentFactory
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactory.h"
extern TypeInfo MethodInfoU5BU5D_t141_il2cpp_TypeInfo;
extern TypeInfo MethodInfo_t142_il2cpp_TypeInfo;
extern TypeInfo BindingFlags_t143_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t144_il2cpp_TypeInfo;
extern TypeInfo MemberInfo_t145_il2cpp_TypeInfo;
extern TypeInfo Boolean_t106_il2cpp_TypeInfo;
extern TypeInfo Attribute_t146_il2cpp_TypeInfo;
extern TypeInfo FactorySetter_t147_il2cpp_TypeInfo;
extern TypeInfo Action_t148_il2cpp_TypeInfo;
extern TypeInfo Void_t99_il2cpp_TypeInfo;
extern TypeInfo VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo;
// System.Linq.Enumerable
#include "System_Core_System_Linq_EnumerableMethodDeclarations.h"
// System.Collections.Generic.List`1<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<System.Reflection.MethodInfo>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_genMethodDeclarations.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
// Vuforia.VuforiaBehaviourComponentFactory
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponentFactoryMethodDeclarations.h"
// Vuforia.BehaviourComponentFactory
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BehaviourComponentFMethodDeclarations.h"
extern Il2CppType Action_t148_0_0_0;
extern MethodInfo Object_GetType_m449_MethodInfo;
extern MethodInfo Type_GetMethods_m450_MethodInfo;
extern MethodInfo Enumerable_ToList_TisMethodInfo_t142_m451_MethodInfo;
extern MethodInfo List_1_AddRange_m452_MethodInfo;
extern MethodInfo List_1_GetEnumerator_m453_MethodInfo;
extern MethodInfo Enumerator_get_Current_m454_MethodInfo;
extern MethodInfo MemberInfo_GetCustomAttributes_m455_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m456_MethodInfo;
extern MethodInfo Action_Invoke_m457_MethodInfo;
extern MethodInfo Enumerator_MoveNext_m458_MethodInfo;
extern MethodInfo IDisposable_Dispose_m459_MethodInfo;
extern MethodInfo VuforiaBehaviourComponentFactory__ctor_m147_MethodInfo;
extern MethodInfo BehaviourComponentFactory_set_Instance_m460_MethodInfo;
struct Enumerable_t149;
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
struct Enumerable_t149;
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_0.h"
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
 List_1_t150 * Enumerable_ToList_TisObject_t_m461_gshared (Object_t * __this/* static, unused */, Object_t* p0, MethodInfo* method);
#define Enumerable_ToList_TisObject_t_m461(__this/* static, unused */, p0, method) (List_1_t150 *)Enumerable_ToList_TisObject_t_m461_gshared((Object_t *)__this/* static, unused */, (Object_t*)p0, method)
// Declaration System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Reflection.MethodInfo>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisMethodInfo_t142_m451(__this/* static, unused */, p0, method) (List_1_t151 *)Enumerable_ToList_TisObject_t_m461_gshared((Object_t *)__this/* static, unused */, (Object_t*)p0, method)


// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
extern MethodInfo ComponentFactoryStarterBehaviour__ctor_m130_MethodInfo;
 void ComponentFactoryStarterBehaviour__ctor_m130 (ComponentFactoryStarterBehaviour_t52 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
extern MethodInfo ComponentFactoryStarterBehaviour_Awake_m131_MethodInfo;
 void ComponentFactoryStarterBehaviour_Awake_m131 (ComponentFactoryStarterBehaviour_t52 * __this, MethodInfo* method){
	List_1_t151 * V_0 = {0};
	MethodInfo_t142 * V_1 = {0};
	Enumerator_t144  V_2 = {0};
	Attribute_t146 * V_3 = {0};
	ObjectU5BU5D_t115* V_4 = {0};
	int32_t V_5 = 0;
	Action_t148 * V_6 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t152 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t152 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		Type_t * L_0 = Object_GetType_m449(__this, /*hidden argument*/&Object_GetType_m449_MethodInfo);
		NullCheck(L_0);
		MethodInfoU5BU5D_t141* L_1 = (MethodInfoU5BU5D_t141*)VirtFuncInvoker1< MethodInfoU5BU5D_t141*, int32_t >::Invoke(&Type_GetMethods_m450_MethodInfo, L_0, ((int32_t)38));
		List_1_t151 * L_2 = Enumerable_ToList_TisMethodInfo_t142_m451(NULL /*static, unused*/, (Object_t*)(Object_t*)L_1, /*hidden argument*/&Enumerable_ToList_TisMethodInfo_t142_m451_MethodInfo);
		V_0 = L_2;
		Type_t * L_3 = Object_GetType_m449(__this, /*hidden argument*/&Object_GetType_m449_MethodInfo);
		NullCheck(L_3);
		MethodInfoU5BU5D_t141* L_4 = (MethodInfoU5BU5D_t141*)VirtFuncInvoker1< MethodInfoU5BU5D_t141*, int32_t >::Invoke(&Type_GetMethods_m450_MethodInfo, L_3, ((int32_t)22));
		NullCheck(V_0);
		List_1_AddRange_m452(V_0, (Object_t*)(Object_t*)L_4, /*hidden argument*/&List_1_AddRange_m452_MethodInfo);
		NullCheck(V_0);
		Enumerator_t144  L_5 = List_1_GetEnumerator_m453(V_0, /*hidden argument*/&List_1_GetEnumerator_m453_MethodInfo);
		V_2 = L_5;
	}

IL_002d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0098;
		}

IL_0032:
		{
			MethodInfo_t142 * L_6 = Enumerator_get_Current_m454((&V_2), /*hidden argument*/&Enumerator_get_Current_m454_MethodInfo);
			V_1 = L_6;
			NullCheck(V_1);
			ObjectU5BU5D_t115* L_7 = (ObjectU5BU5D_t115*)VirtFuncInvoker1< ObjectU5BU5D_t115*, bool >::Invoke(&MemberInfo_GetCustomAttributes_m455_MethodInfo, V_1, 1);
			V_4 = L_7;
			V_5 = 0;
			goto IL_008d;
		}

IL_004b:
		{
			NullCheck(V_4);
			IL2CPP_ARRAY_BOUNDS_CHECK(V_4, V_5);
			int32_t L_8 = V_5;
			V_3 = ((Attribute_t146 *)Castclass((*(Object_t **)(Object_t **)SZArrayLdElema(V_4, L_8)), InitializedTypeInfo(&Attribute_t146_il2cpp_TypeInfo)));
			if (!((FactorySetter_t147 *)IsInst(V_3, InitializedTypeInfo(&FactorySetter_t147_il2cpp_TypeInfo))))
			{
				goto IL_0087;
			}
		}

IL_0061:
		{
			IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
			Type_t * L_9 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&Action_t148_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
			Delegate_t153 * L_10 = Delegate_CreateDelegate_m456(NULL /*static, unused*/, L_9, __this, V_1, /*hidden argument*/&Delegate_CreateDelegate_m456_MethodInfo);
			V_6 = ((Action_t148 *)IsInst(L_10, InitializedTypeInfo(&Action_t148_il2cpp_TypeInfo)));
			if (!V_6)
			{
				goto IL_0087;
			}
		}

IL_0080:
		{
			NullCheck(V_6);
			VirtActionInvoker0::Invoke(&Action_Invoke_m457_MethodInfo, V_6);
		}

IL_0087:
		{
			V_5 = ((int32_t)(V_5+1));
		}

IL_008d:
		{
			NullCheck(V_4);
			if ((((int32_t)V_5) < ((int32_t)(((int32_t)(((Array_t *)V_4)->max_length))))))
			{
				goto IL_004b;
			}
		}

IL_0098:
		{
			bool L_11 = Enumerator_MoveNext_m458((&V_2), /*hidden argument*/&Enumerator_MoveNext_m458_MethodInfo);
			if (L_11)
			{
				goto IL_0032;
			}
		}

IL_00a4:
		{
			// IL_00a4: leave IL_00b5
			leaveInstructions[0] = 0xB5; // 1
			THROW_SENTINEL(IL_00b5);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_00a9;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t152 *)e.ex;
		goto IL_00a9;
	}

IL_00a9:
	{ // begin finally (depth: 1)
		Enumerator_t144  L_12 = V_2;
		Object_t * L_13 = Box(InitializedTypeInfo(&Enumerator_t144_il2cpp_TypeInfo), &L_12);
		NullCheck(L_13);
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m459_MethodInfo, L_13);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0xB5:
				goto IL_00b5;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t152 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_00b5:
	{
		return;
	}
}
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
extern MethodInfo ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m132_MethodInfo;
 void ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m132 (ComponentFactoryStarterBehaviour_t52 * __this, MethodInfo* method){
	{
		Debug_Log_m419(NULL /*static, unused*/, (String_t*) &_stringLiteral50, /*hidden argument*/&Debug_Log_m419_MethodInfo);
		VuforiaBehaviourComponentFactory_t54 * L_0 = (VuforiaBehaviourComponentFactory_t54 *)il2cpp_codegen_object_new (InitializedTypeInfo(&VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo));
		VuforiaBehaviourComponentFactory__ctor_m147(L_0, /*hidden argument*/&VuforiaBehaviourComponentFactory__ctor_m147_MethodInfo);
		BehaviourComponentFactory_set_Instance_m460(NULL /*static, unused*/, L_0, /*hidden argument*/&BehaviourComponentFactory_set_Instance_m460_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.ComponentFactoryStarterBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
MethodInfo ComponentFactoryStarterBehaviour__ctor_m130_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ComponentFactoryStarterBehaviour__ctor_m130/* method */
	, &ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 131/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
MethodInfo ComponentFactoryStarterBehaviour_Awake_m131_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&ComponentFactoryStarterBehaviour_Awake_m131/* method */
	, &ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 132/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache ComponentFactoryStarterBehaviour_t52__CustomAttributeCache_ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m132;
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
MethodInfo ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m132_MethodInfo = 
{
	"SetBehaviourComponentFactory"/* name */
	, (methodPointerType)&ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m132/* method */
	, &ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &ComponentFactoryStarterBehaviour_t52__CustomAttributeCache_ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m132/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 133/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ComponentFactoryStarterBehaviour_t52_MethodInfos[] =
{
	&ComponentFactoryStarterBehaviour__ctor_m130_MethodInfo,
	&ComponentFactoryStarterBehaviour_Awake_m131_MethodInfo,
	&ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m132_MethodInfo,
	NULL
};
static MethodInfo* ComponentFactoryStarterBehaviour_t52_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
// Vuforia.FactorySetter
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySetterMethodDeclarations.h"
extern MethodInfo FactorySetter__ctor_m462_MethodInfo;
void ComponentFactoryStarterBehaviour_t52_CustomAttributesCacheGenerator_ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m132(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FactorySetter_t147 * tmp;
		tmp = (FactorySetter_t147 *)il2cpp_codegen_object_new (&FactorySetter_t147_il2cpp_TypeInfo);
		FactorySetter__ctor_m462(tmp, &FactorySetter__ctor_m462_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache ComponentFactoryStarterBehaviour_t52__CustomAttributeCache_ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m132 = {
1,
NULL,
&ComponentFactoryStarterBehaviour_t52_CustomAttributesCacheGenerator_ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m132
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType ComponentFactoryStarterBehaviour_t52_0_0_0;
extern Il2CppType ComponentFactoryStarterBehaviour_t52_1_0_0;
struct ComponentFactoryStarterBehaviour_t52;
extern CustomAttributesCache ComponentFactoryStarterBehaviour_t52__CustomAttributeCache_ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m132;
TypeInfo ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ComponentFactoryStarterBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ComponentFactoryStarterBehaviour_t52_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ComponentFactoryStarterBehaviour_t52_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo/* cast_class */
	, &ComponentFactoryStarterBehaviour_t52_0_0_0/* byval_arg */
	, &ComponentFactoryStarterBehaviour_t52_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ComponentFactoryStarterBehaviour_t52)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.IOSUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IOSUnityPlayer_t53_il2cpp_TypeInfo;
// Vuforia.IOSUnityPlayer
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayerMethodDeclarations.h"

extern MethodInfo IOSUnityPlayer_setPlatFormNative_m144_MethodInfo;
extern MethodInfo IOSUnityPlayer_initQCARiOS_m145_MethodInfo;
extern MethodInfo IOSUnityPlayer_InitializeSurface_m142_MethodInfo;
extern MethodInfo IOSUnityPlayer_SetUnityScreenOrientation_m143_MethodInfo;
extern MethodInfo IOSUnityPlayer_setSurfaceOrientationiOS_m146_MethodInfo;


// System.Void Vuforia.IOSUnityPlayer::.ctor()
extern MethodInfo IOSUnityPlayer__ctor_m133_MethodInfo;
 void IOSUnityPlayer__ctor_m133 (IOSUnityPlayer_t53 * __this, MethodInfo* method){
	{
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::LoadNativeLibraries()
extern MethodInfo IOSUnityPlayer_LoadNativeLibraries_m134_MethodInfo;
 void IOSUnityPlayer_LoadNativeLibraries_m134 (IOSUnityPlayer_t53 * __this, MethodInfo* method){
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializePlatform()
extern MethodInfo IOSUnityPlayer_InitializePlatform_m135_MethodInfo;
 void IOSUnityPlayer_InitializePlatform_m135 (IOSUnityPlayer_t53 * __this, MethodInfo* method){
	{
		IOSUnityPlayer_setPlatFormNative_m144(NULL /*static, unused*/, /*hidden argument*/&IOSUnityPlayer_setPlatFormNative_m144_MethodInfo);
		return;
	}
}
// Vuforia.QCARUnity/InitError Vuforia.IOSUnityPlayer::Start(System.String)
extern MethodInfo IOSUnityPlayer_Start_m136_MethodInfo;
 int32_t IOSUnityPlayer_Start_m136 (IOSUnityPlayer_t53 * __this, String_t* ___licenseKey, MethodInfo* method){
	int32_t V_0 = 0;
	{
		int32_t L_0 = Screen_get_orientation_m443(NULL /*static, unused*/, /*hidden argument*/&Screen_get_orientation_m443_MethodInfo);
		int32_t L_1 = IOSUnityPlayer_initQCARiOS_m145(NULL /*static, unused*/, L_0, ___licenseKey, /*hidden argument*/&IOSUnityPlayer_initQCARiOS_m145_MethodInfo);
		V_0 = L_1;
		if ((((int32_t)V_0) < ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m142(__this, /*hidden argument*/&IOSUnityPlayer_InitializeSurface_m142_MethodInfo);
	}

IL_0019:
	{
		return (int32_t)(V_0);
	}
}
// System.Void Vuforia.IOSUnityPlayer::Update()
extern MethodInfo IOSUnityPlayer_Update_m137_MethodInfo;
 void IOSUnityPlayer_Update_m137 (IOSUnityPlayer_t53 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SurfaceUtilities_t137_il2cpp_TypeInfo));
		bool L_0 = SurfaceUtilities_HasSurfaceBeenRecreated_m442(NULL /*static, unused*/, /*hidden argument*/&SurfaceUtilities_HasSurfaceBeenRecreated_m442_MethodInfo);
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IOSUnityPlayer_InitializeSurface_m142(__this, /*hidden argument*/&IOSUnityPlayer_InitializeSurface_m142_MethodInfo);
		goto IL_002b;
	}

IL_0015:
	{
		int32_t L_1 = Screen_get_orientation_m443(NULL /*static, unused*/, /*hidden argument*/&Screen_get_orientation_m443_MethodInfo);
		int32_t L_2 = (__this->___mScreenOrientation_0);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_002b;
		}
	}
	{
		IOSUnityPlayer_SetUnityScreenOrientation_m143(__this, /*hidden argument*/&IOSUnityPlayer_SetUnityScreenOrientation_m143_MethodInfo);
	}

IL_002b:
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::Dispose()
extern MethodInfo IOSUnityPlayer_Dispose_m138_MethodInfo;
 void IOSUnityPlayer_Dispose_m138 (IOSUnityPlayer_t53 * __this, MethodInfo* method){
	{
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnPause()
extern MethodInfo IOSUnityPlayer_OnPause_m139_MethodInfo;
 void IOSUnityPlayer_OnPause_m139 (IOSUnityPlayer_t53 * __this, MethodInfo* method){
	{
		QCARUnity_OnPause_m444(NULL /*static, unused*/, /*hidden argument*/&QCARUnity_OnPause_m444_MethodInfo);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnResume()
extern MethodInfo IOSUnityPlayer_OnResume_m140_MethodInfo;
 void IOSUnityPlayer_OnResume_m140 (IOSUnityPlayer_t53 * __this, MethodInfo* method){
	{
		QCARUnity_OnResume_m445(NULL /*static, unused*/, /*hidden argument*/&QCARUnity_OnResume_m445_MethodInfo);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::OnDestroy()
extern MethodInfo IOSUnityPlayer_OnDestroy_m141_MethodInfo;
 void IOSUnityPlayer_OnDestroy_m141 (IOSUnityPlayer_t53 * __this, MethodInfo* method){
	{
		QCARUnity_Deinit_m446(NULL /*static, unused*/, /*hidden argument*/&QCARUnity_Deinit_m446_MethodInfo);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::InitializeSurface()
 void IOSUnityPlayer_InitializeSurface_m142 (IOSUnityPlayer_t53 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SurfaceUtilities_t137_il2cpp_TypeInfo));
		SurfaceUtilities_OnSurfaceCreated_m447(NULL /*static, unused*/, /*hidden argument*/&SurfaceUtilities_OnSurfaceCreated_m447_MethodInfo);
		IOSUnityPlayer_SetUnityScreenOrientation_m143(__this, /*hidden argument*/&IOSUnityPlayer_SetUnityScreenOrientation_m143_MethodInfo);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::SetUnityScreenOrientation()
 void IOSUnityPlayer_SetUnityScreenOrientation_m143 (IOSUnityPlayer_t53 * __this, MethodInfo* method){
	{
		int32_t L_0 = Screen_get_orientation_m443(NULL /*static, unused*/, /*hidden argument*/&Screen_get_orientation_m443_MethodInfo);
		__this->___mScreenOrientation_0 = L_0;
		int32_t L_1 = (__this->___mScreenOrientation_0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SurfaceUtilities_t137_il2cpp_TypeInfo));
		SurfaceUtilities_SetSurfaceOrientation_m448(NULL /*static, unused*/, L_1, /*hidden argument*/&SurfaceUtilities_SetSurfaceOrientation_m448_MethodInfo);
		int32_t L_2 = (__this->___mScreenOrientation_0);
		IOSUnityPlayer_setSurfaceOrientationiOS_m146(NULL /*static, unused*/, L_2, /*hidden argument*/&IOSUnityPlayer_setSurfaceOrientationiOS_m146_MethodInfo);
		return;
	}
}
// System.Void Vuforia.IOSUnityPlayer::setPlatFormNative()
extern "C" {void DEFAULT_CALL setPlatFormNative();}
 void IOSUnityPlayer_setPlatFormNative_m144 (Object_t * __this/* static, unused */, MethodInfo* method){
	typedef void (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setPlatFormNative;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setPlatFormNative'"));
		}
	}

	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.Int32 Vuforia.IOSUnityPlayer::initQCARiOS(System.Int32,System.String)
extern "C" {int32_t DEFAULT_CALL initQCARiOS(int32_t, char*);}
 int32_t IOSUnityPlayer_initQCARiOS_m145 (Object_t * __this/* static, unused */, int32_t ___screenOrientation, String_t* ___licenseKey, MethodInfo* method){
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, char*);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)initQCARiOS;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'initQCARiOS'"));
		}
	}

	// Marshaling of parameter '___screenOrientation' to native representation

	// Marshaling of parameter '___licenseKey' to native representation
	char* ____licenseKey_marshaled = { 0 };
	____licenseKey_marshaled = il2cpp_codegen_marshal_string(___licenseKey);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(___screenOrientation, ____licenseKey_marshaled);

	// Marshaling cleanup of parameter '___screenOrientation' native representation

	// Marshaling cleanup of parameter '___licenseKey' native representation
	il2cpp_codegen_marshal_free(____licenseKey_marshaled);
	____licenseKey_marshaled = NULL;

	return _return_value;
}
// System.Void Vuforia.IOSUnityPlayer::setSurfaceOrientationiOS(System.Int32)
extern "C" {void DEFAULT_CALL setSurfaceOrientationiOS(int32_t);}
 void IOSUnityPlayer_setSurfaceOrientationiOS_m146 (Object_t * __this/* static, unused */, int32_t ___screenOrientation, MethodInfo* method){
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		_il2cpp_pinvoke_func = (PInvokeFunc)setSurfaceOrientationiOS;

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'setSurfaceOrientationiOS'"));
		}
	}

	// Marshaling of parameter '___screenOrientation' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___screenOrientation);

	// Marshaling cleanup of parameter '___screenOrientation' native representation

}
// Metadata Definition Vuforia.IOSUnityPlayer
extern Il2CppType ScreenOrientation_t138_0_0_1;
FieldInfo IOSUnityPlayer_t53____mScreenOrientation_0_FieldInfo = 
{
	"mScreenOrientation"/* name */
	, &ScreenOrientation_t138_0_0_1/* type */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* parent */
	, offsetof(IOSUnityPlayer_t53, ___mScreenOrientation_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* IOSUnityPlayer_t53_FieldInfos[] =
{
	&IOSUnityPlayer_t53____mScreenOrientation_0_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::.ctor()
MethodInfo IOSUnityPlayer__ctor_m133_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&IOSUnityPlayer__ctor_m133/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 134/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::LoadNativeLibraries()
MethodInfo IOSUnityPlayer_LoadNativeLibraries_m134_MethodInfo = 
{
	"LoadNativeLibraries"/* name */
	, (methodPointerType)&IOSUnityPlayer_LoadNativeLibraries_m134/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 135/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::InitializePlatform()
MethodInfo IOSUnityPlayer_InitializePlatform_m135_MethodInfo = 
{
	"InitializePlatform"/* name */
	, (methodPointerType)&IOSUnityPlayer_InitializePlatform_m135/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 136/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo IOSUnityPlayer_t53_IOSUnityPlayer_Start_m136_ParameterInfos[] = 
{
	{"licenseKey", 0, 134217780, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType InitError_t128_0_0_0;
extern void* RuntimeInvoker_InitError_t128_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.QCARUnity/InitError Vuforia.IOSUnityPlayer::Start(System.String)
MethodInfo IOSUnityPlayer_Start_m136_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&IOSUnityPlayer_Start_m136/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &InitError_t128_0_0_0/* return_type */
	, RuntimeInvoker_InitError_t128_Object_t/* invoker_method */
	, IOSUnityPlayer_t53_IOSUnityPlayer_Start_m136_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 137/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::Update()
MethodInfo IOSUnityPlayer_Update_m137_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&IOSUnityPlayer_Update_m137/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 138/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::Dispose()
MethodInfo IOSUnityPlayer_Dispose_m138_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&IOSUnityPlayer_Dispose_m138/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 139/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::OnPause()
MethodInfo IOSUnityPlayer_OnPause_m139_MethodInfo = 
{
	"OnPause"/* name */
	, (methodPointerType)&IOSUnityPlayer_OnPause_m139/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 140/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::OnResume()
MethodInfo IOSUnityPlayer_OnResume_m140_MethodInfo = 
{
	"OnResume"/* name */
	, (methodPointerType)&IOSUnityPlayer_OnResume_m140/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 141/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::OnDestroy()
MethodInfo IOSUnityPlayer_OnDestroy_m141_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&IOSUnityPlayer_OnDestroy_m141/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 142/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::InitializeSurface()
MethodInfo IOSUnityPlayer_InitializeSurface_m142_MethodInfo = 
{
	"InitializeSurface"/* name */
	, (methodPointerType)&IOSUnityPlayer_InitializeSurface_m142/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 143/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::SetUnityScreenOrientation()
MethodInfo IOSUnityPlayer_SetUnityScreenOrientation_m143_MethodInfo = 
{
	"SetUnityScreenOrientation"/* name */
	, (methodPointerType)&IOSUnityPlayer_SetUnityScreenOrientation_m143/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 144/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::setPlatFormNative()
MethodInfo IOSUnityPlayer_setPlatFormNative_m144_MethodInfo = 
{
	"setPlatFormNative"/* name */
	, (methodPointerType)&IOSUnityPlayer_setPlatFormNative_m144/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 8337/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 145/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo IOSUnityPlayer_t53_IOSUnityPlayer_initQCARiOS_m145_ParameterInfos[] = 
{
	{"screenOrientation", 0, 134217781, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"licenseKey", 1, 134217782, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
// System.Int32 Vuforia.IOSUnityPlayer::initQCARiOS(System.Int32,System.String)
MethodInfo IOSUnityPlayer_initQCARiOS_m145_MethodInfo = 
{
	"initQCARiOS"/* name */
	, (methodPointerType)&IOSUnityPlayer_initQCARiOS_m145/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93_Object_t/* invoker_method */
	, IOSUnityPlayer_t53_IOSUnityPlayer_initQCARiOS_m145_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 8337/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 146/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IOSUnityPlayer_t53_IOSUnityPlayer_setSurfaceOrientationiOS_m146_ParameterInfos[] = 
{
	{"screenOrientation", 0, 134217783, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.IOSUnityPlayer::setSurfaceOrientationiOS(System.Int32)
MethodInfo IOSUnityPlayer_setSurfaceOrientationiOS_m146_MethodInfo = 
{
	"setSurfaceOrientationiOS"/* name */
	, (methodPointerType)&IOSUnityPlayer_setSurfaceOrientationiOS_m146/* method */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IOSUnityPlayer_t53_IOSUnityPlayer_setSurfaceOrientationiOS_m146_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 8337/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 147/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* IOSUnityPlayer_t53_MethodInfos[] =
{
	&IOSUnityPlayer__ctor_m133_MethodInfo,
	&IOSUnityPlayer_LoadNativeLibraries_m134_MethodInfo,
	&IOSUnityPlayer_InitializePlatform_m135_MethodInfo,
	&IOSUnityPlayer_Start_m136_MethodInfo,
	&IOSUnityPlayer_Update_m137_MethodInfo,
	&IOSUnityPlayer_Dispose_m138_MethodInfo,
	&IOSUnityPlayer_OnPause_m139_MethodInfo,
	&IOSUnityPlayer_OnResume_m140_MethodInfo,
	&IOSUnityPlayer_OnDestroy_m141_MethodInfo,
	&IOSUnityPlayer_InitializeSurface_m142_MethodInfo,
	&IOSUnityPlayer_SetUnityScreenOrientation_m143_MethodInfo,
	&IOSUnityPlayer_setPlatFormNative_m144_MethodInfo,
	&IOSUnityPlayer_initQCARiOS_m145_MethodInfo,
	&IOSUnityPlayer_setSurfaceOrientationiOS_m146_MethodInfo,
	NULL
};
static MethodInfo* IOSUnityPlayer_t53_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&IOSUnityPlayer_Dispose_m138_MethodInfo,
	&IOSUnityPlayer_LoadNativeLibraries_m134_MethodInfo,
	&IOSUnityPlayer_InitializePlatform_m135_MethodInfo,
	&IOSUnityPlayer_Start_m136_MethodInfo,
	&IOSUnityPlayer_Update_m137_MethodInfo,
	&IOSUnityPlayer_OnPause_m139_MethodInfo,
	&IOSUnityPlayer_OnResume_m140_MethodInfo,
	&IOSUnityPlayer_OnDestroy_m141_MethodInfo,
};
static TypeInfo* IOSUnityPlayer_t53_InterfacesTypeInfos[] = 
{
	&IDisposable_t139_il2cpp_TypeInfo,
	&IUnityPlayer_t140_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair IOSUnityPlayer_t53_InterfacesOffsets[] = 
{
	{ &IDisposable_t139_il2cpp_TypeInfo, 4},
	{ &IUnityPlayer_t140_il2cpp_TypeInfo, 5},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType IOSUnityPlayer_t53_0_0_0;
extern Il2CppType IOSUnityPlayer_t53_1_0_0;
struct IOSUnityPlayer_t53;
TypeInfo IOSUnityPlayer_t53_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "IOSUnityPlayer"/* name */
	, "Vuforia"/* namespaze */
	, IOSUnityPlayer_t53_MethodInfos/* methods */
	, NULL/* properties */
	, IOSUnityPlayer_t53_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* element_class */
	, IOSUnityPlayer_t53_InterfacesTypeInfos/* implemented_interfaces */
	, IOSUnityPlayer_t53_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IOSUnityPlayer_t53_il2cpp_TypeInfo/* cast_class */
	, &IOSUnityPlayer_t53_0_0_0/* byval_arg */
	, &IOSUnityPlayer_t53_1_0_0/* this_arg */
	, IOSUnityPlayer_t53_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (IOSUnityPlayer_t53)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 14/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 12/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// Vuforia.MaskOutAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeha.h"
// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviour.h"
// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"
// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviour.h"
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeha.h"
// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviour.h"
// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstract.h"
// Vuforia.MarkerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbstractBehav.h"
// Vuforia.MarkerBehaviour
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviour.h"
// Vuforia.MultiTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetAbstract.h"
// Vuforia.MultiTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviour.h"
// Vuforia.CylinderTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CylinderTargetAbstr.h"
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavio.h"
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviour.h"
// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBeh.h"
// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour.h"
// Vuforia.ObjectTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstrac.h"
// Vuforia.ObjectTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviour.h"
extern MethodInfo GameObject_AddComponent_TisMaskOutBehaviour_t66_m463_MethodInfo;
extern MethodInfo GameObject_AddComponent_TisVirtualButtonBehaviour_t87_m464_MethodInfo;
extern MethodInfo GameObject_AddComponent_TisTurnOffBehaviour_t79_m465_MethodInfo;
extern MethodInfo GameObject_AddComponent_TisImageTargetBehaviour_t49_m466_MethodInfo;
extern MethodInfo GameObject_AddComponent_TisMarkerBehaviour_t65_m467_MethodInfo;
extern MethodInfo GameObject_AddComponent_TisMultiTargetBehaviour_t67_m468_MethodInfo;
extern MethodInfo GameObject_AddComponent_TisCylinderTargetBehaviour_t33_m469_MethodInfo;
extern MethodInfo GameObject_AddComponent_TisWordBehaviour_t92_m470_MethodInfo;
extern MethodInfo GameObject_AddComponent_TisTextRecoBehaviour_t78_m471_MethodInfo;
extern MethodInfo GameObject_AddComponent_TisObjectTargetBehaviour_t68_m472_MethodInfo;
struct GameObject_t2;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.MaskOutBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MaskOutBehaviour>()
 MaskOutBehaviour_t66 * GameObject_AddComponent_TisMaskOutBehaviour_t66_m463 (GameObject_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct GameObject_t2;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.VirtualButtonBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.VirtualButtonBehaviour>()
 VirtualButtonBehaviour_t87 * GameObject_AddComponent_TisVirtualButtonBehaviour_t87_m464 (GameObject_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct GameObject_t2;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.TurnOffBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TurnOffBehaviour>()
 TurnOffBehaviour_t79 * GameObject_AddComponent_TisTurnOffBehaviour_t79_m465 (GameObject_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct GameObject_t2;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.ImageTargetBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ImageTargetBehaviour>()
 ImageTargetBehaviour_t49 * GameObject_AddComponent_TisImageTargetBehaviour_t49_m466 (GameObject_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct GameObject_t2;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.MarkerBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MarkerBehaviour>()
 MarkerBehaviour_t65 * GameObject_AddComponent_TisMarkerBehaviour_t65_m467 (GameObject_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct GameObject_t2;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.MultiTargetBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.MultiTargetBehaviour>()
 MultiTargetBehaviour_t67 * GameObject_AddComponent_TisMultiTargetBehaviour_t67_m468 (GameObject_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct GameObject_t2;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.CylinderTargetBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.CylinderTargetBehaviour>()
 CylinderTargetBehaviour_t33 * GameObject_AddComponent_TisCylinderTargetBehaviour_t33_m469 (GameObject_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct GameObject_t2;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.WordBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.WordBehaviour>()
 WordBehaviour_t92 * GameObject_AddComponent_TisWordBehaviour_t92_m470 (GameObject_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct GameObject_t2;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.TextRecoBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.TextRecoBehaviour>()
 TextRecoBehaviour_t78 * GameObject_AddComponent_TisTextRecoBehaviour_t78_m471 (GameObject_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
struct GameObject_t2;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.ObjectTargetBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ObjectTargetBehaviour>()
 ObjectTargetBehaviour_t68 * GameObject_AddComponent_TisObjectTargetBehaviour_t68_m472 (GameObject_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
 void VuforiaBehaviourComponentFactory__ctor_m147 (VuforiaBehaviourComponentFactory_t54 * __this, MethodInfo* method){
	{
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		return;
	}
}
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
extern MethodInfo VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m148_MethodInfo;
 MaskOutAbstractBehaviour_t55 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m148 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method){
	{
		NullCheck(___gameObject);
		MaskOutBehaviour_t66 * L_0 = GameObject_AddComponent_TisMaskOutBehaviour_t66_m463(___gameObject, /*hidden argument*/&GameObject_AddComponent_TisMaskOutBehaviour_t66_m463_MethodInfo);
		return L_0;
	}
}
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
extern MethodInfo VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m149_MethodInfo;
 VirtualButtonAbstractBehaviour_t56 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m149 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method){
	{
		NullCheck(___gameObject);
		VirtualButtonBehaviour_t87 * L_0 = GameObject_AddComponent_TisVirtualButtonBehaviour_t87_m464(___gameObject, /*hidden argument*/&GameObject_AddComponent_TisVirtualButtonBehaviour_t87_m464_MethodInfo);
		return L_0;
	}
}
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
extern MethodInfo VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m150_MethodInfo;
 TurnOffAbstractBehaviour_t57 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m150 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method){
	{
		NullCheck(___gameObject);
		TurnOffBehaviour_t79 * L_0 = GameObject_AddComponent_TisTurnOffBehaviour_t79_m465(___gameObject, /*hidden argument*/&GameObject_AddComponent_TisTurnOffBehaviour_t79_m465_MethodInfo);
		return L_0;
	}
}
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
extern MethodInfo VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m151_MethodInfo;
 ImageTargetAbstractBehaviour_t50 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m151 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method){
	{
		NullCheck(___gameObject);
		ImageTargetBehaviour_t49 * L_0 = GameObject_AddComponent_TisImageTargetBehaviour_t49_m466(___gameObject, /*hidden argument*/&GameObject_AddComponent_TisImageTargetBehaviour_t49_m466_MethodInfo);
		return L_0;
	}
}
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
extern MethodInfo VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m152_MethodInfo;
 MarkerAbstractBehaviour_t58 * VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m152 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method){
	{
		NullCheck(___gameObject);
		MarkerBehaviour_t65 * L_0 = GameObject_AddComponent_TisMarkerBehaviour_t65_m467(___gameObject, /*hidden argument*/&GameObject_AddComponent_TisMarkerBehaviour_t65_m467_MethodInfo);
		return L_0;
	}
}
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
extern MethodInfo VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m153_MethodInfo;
 MultiTargetAbstractBehaviour_t59 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m153 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method){
	{
		NullCheck(___gameObject);
		MultiTargetBehaviour_t67 * L_0 = GameObject_AddComponent_TisMultiTargetBehaviour_t67_m468(___gameObject, /*hidden argument*/&GameObject_AddComponent_TisMultiTargetBehaviour_t67_m468_MethodInfo);
		return L_0;
	}
}
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
extern MethodInfo VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m154_MethodInfo;
 CylinderTargetAbstractBehaviour_t34 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m154 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method){
	{
		NullCheck(___gameObject);
		CylinderTargetBehaviour_t33 * L_0 = GameObject_AddComponent_TisCylinderTargetBehaviour_t33_m469(___gameObject, /*hidden argument*/&GameObject_AddComponent_TisCylinderTargetBehaviour_t33_m469_MethodInfo);
		return L_0;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
extern MethodInfo VuforiaBehaviourComponentFactory_AddWordBehaviour_m155_MethodInfo;
 WordAbstractBehaviour_t60 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m155 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method){
	{
		NullCheck(___gameObject);
		WordBehaviour_t92 * L_0 = GameObject_AddComponent_TisWordBehaviour_t92_m470(___gameObject, /*hidden argument*/&GameObject_AddComponent_TisWordBehaviour_t92_m470_MethodInfo);
		return L_0;
	}
}
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
extern MethodInfo VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m156_MethodInfo;
 TextRecoAbstractBehaviour_t61 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m156 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method){
	{
		NullCheck(___gameObject);
		TextRecoBehaviour_t78 * L_0 = GameObject_AddComponent_TisTextRecoBehaviour_t78_m471(___gameObject, /*hidden argument*/&GameObject_AddComponent_TisTextRecoBehaviour_t78_m471_MethodInfo);
		return L_0;
	}
}
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
extern MethodInfo VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m157_MethodInfo;
 ObjectTargetAbstractBehaviour_t62 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m157 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method){
	{
		NullCheck(___gameObject);
		ObjectTargetBehaviour_t68 * L_0 = GameObject_AddComponent_TisObjectTargetBehaviour_t68_m472(___gameObject, /*hidden argument*/&GameObject_AddComponent_TisObjectTargetBehaviour_t68_m472_MethodInfo);
		return L_0;
	}
}
// Metadata Definition Vuforia.VuforiaBehaviourComponentFactory
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
MethodInfo VuforiaBehaviourComponentFactory__ctor_m147_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory__ctor_m147/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 148/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t2_0_0_0;
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m148_ParameterInfos[] = 
{
	{"gameObject", 0, 134217784, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType MaskOutAbstractBehaviour_t55_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
MethodInfo VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m148_MethodInfo = 
{
	"AddMaskOutBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m148/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &MaskOutAbstractBehaviour_t55_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m148_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 149/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m149_ParameterInfos[] = 
{
	{"gameObject", 0, 134217785, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType VirtualButtonAbstractBehaviour_t56_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
MethodInfo VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m149_MethodInfo = 
{
	"AddVirtualButtonBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m149/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButtonAbstractBehaviour_t56_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m149_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 150/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m150_ParameterInfos[] = 
{
	{"gameObject", 0, 134217786, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType TurnOffAbstractBehaviour_t57_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
MethodInfo VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m150_MethodInfo = 
{
	"AddTurnOffBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m150/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &TurnOffAbstractBehaviour_t57_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m150_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 151/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m151_ParameterInfos[] = 
{
	{"gameObject", 0, 134217787, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType ImageTargetAbstractBehaviour_t50_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
MethodInfo VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m151_MethodInfo = 
{
	"AddImageTargetBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m151/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetAbstractBehaviour_t50_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m151_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 152/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m152_ParameterInfos[] = 
{
	{"gameObject", 0, 134217788, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType MarkerAbstractBehaviour_t58_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
MethodInfo VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m152_MethodInfo = 
{
	"AddMarkerBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m152/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &MarkerAbstractBehaviour_t58_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m152_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 153/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m153_ParameterInfos[] = 
{
	{"gameObject", 0, 134217789, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType MultiTargetAbstractBehaviour_t59_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
MethodInfo VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m153_MethodInfo = 
{
	"AddMultiTargetBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m153/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &MultiTargetAbstractBehaviour_t59_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m153_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 154/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m154_ParameterInfos[] = 
{
	{"gameObject", 0, 134217790, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType CylinderTargetAbstractBehaviour_t34_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
MethodInfo VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m154_MethodInfo = 
{
	"AddCylinderTargetBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m154/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &CylinderTargetAbstractBehaviour_t34_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m154_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 155/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddWordBehaviour_m155_ParameterInfos[] = 
{
	{"gameObject", 0, 134217791, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType WordAbstractBehaviour_t60_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
MethodInfo VuforiaBehaviourComponentFactory_AddWordBehaviour_m155_MethodInfo = 
{
	"AddWordBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddWordBehaviour_m155/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &WordAbstractBehaviour_t60_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddWordBehaviour_m155_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 156/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m156_ParameterInfos[] = 
{
	{"gameObject", 0, 134217792, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType TextRecoAbstractBehaviour_t61_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
MethodInfo VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m156_MethodInfo = 
{
	"AddTextRecoBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m156/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoAbstractBehaviour_t61_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m156_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 157/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m157_ParameterInfos[] = 
{
	{"gameObject", 0, 134217793, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType ObjectTargetAbstractBehaviour_t62_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
MethodInfo VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m157_MethodInfo = 
{
	"AddObjectTargetBehaviour"/* name */
	, (methodPointerType)&VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m157/* method */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* declaring_type */
	, &ObjectTargetAbstractBehaviour_t62_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, VuforiaBehaviourComponentFactory_t54_VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m157_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 158/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* VuforiaBehaviourComponentFactory_t54_MethodInfos[] =
{
	&VuforiaBehaviourComponentFactory__ctor_m147_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m148_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m149_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m150_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m151_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m152_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m153_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m154_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddWordBehaviour_m155_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m156_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m157_MethodInfo,
	NULL
};
static MethodInfo* VuforiaBehaviourComponentFactory_t54_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m148_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m149_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m150_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m151_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m152_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m153_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m154_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddWordBehaviour_m155_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m156_MethodInfo,
	&VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m157_MethodInfo,
};
extern TypeInfo IBehaviourComponentFactory_t154_il2cpp_TypeInfo;
static TypeInfo* VuforiaBehaviourComponentFactory_t54_InterfacesTypeInfos[] = 
{
	&IBehaviourComponentFactory_t154_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair VuforiaBehaviourComponentFactory_t54_InterfacesOffsets[] = 
{
	{ &IBehaviourComponentFactory_t154_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType VuforiaBehaviourComponentFactory_t54_0_0_0;
extern Il2CppType VuforiaBehaviourComponentFactory_t54_1_0_0;
struct VuforiaBehaviourComponentFactory_t54;
TypeInfo VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "VuforiaBehaviourComponentFactory"/* name */
	, "Vuforia"/* namespaze */
	, VuforiaBehaviourComponentFactory_t54_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* element_class */
	, VuforiaBehaviourComponentFactory_t54_InterfacesTypeInfos/* implemented_interfaces */
	, VuforiaBehaviourComponentFactory_t54_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo/* cast_class */
	, &VuforiaBehaviourComponentFactory_t54_0_0_0/* byval_arg */
	, &VuforiaBehaviourComponentFactory_t54_1_0_0/* this_arg */
	, VuforiaBehaviourComponentFactory_t54_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VuforiaBehaviourComponentFactory_t54)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 11/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 14/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.KeepAliveBehaviour
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo KeepAliveBehaviour_t63_il2cpp_TypeInfo;
// Vuforia.KeepAliveBehaviour
#include "AssemblyU2DCSharp_Vuforia_KeepAliveBehaviourMethodDeclarations.h"

// Vuforia.KeepAliveAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBeMethodDeclarations.h"
extern MethodInfo KeepAliveAbstractBehaviour__ctor_m473_MethodInfo;


// System.Void Vuforia.KeepAliveBehaviour::.ctor()
extern MethodInfo KeepAliveBehaviour__ctor_m158_MethodInfo;
 void KeepAliveBehaviour__ctor_m158 (KeepAliveBehaviour_t63 * __this, MethodInfo* method){
	{
		KeepAliveAbstractBehaviour__ctor_m473(__this, /*hidden argument*/&KeepAliveAbstractBehaviour__ctor_m473_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.KeepAliveBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.KeepAliveBehaviour::.ctor()
MethodInfo KeepAliveBehaviour__ctor_m158_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&KeepAliveBehaviour__ctor_m158/* method */
	, &KeepAliveBehaviour_t63_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 159/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* KeepAliveBehaviour_t63_MethodInfos[] =
{
	&KeepAliveBehaviour__ctor_m158_MethodInfo,
	NULL
};
static MethodInfo* KeepAliveBehaviour_t63_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern TypeInfo RequireComponent_t155_il2cpp_TypeInfo;
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
extern MethodInfo RequireComponent__ctor_m474_MethodInfo;
extern TypeInfo QCARBehaviour_t70_il2cpp_TypeInfo;
// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviour.h"
void KeepAliveBehaviour_t63_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t155 * tmp;
		tmp = (RequireComponent_t155 *)il2cpp_codegen_object_new (&RequireComponent_t155_il2cpp_TypeInfo);
		RequireComponent__ctor_m474(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&QCARBehaviour_t70_il2cpp_TypeInfo)), &RequireComponent__ctor_m474_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache KeepAliveBehaviour_t63__CustomAttributeCache = {
1,
NULL,
&KeepAliveBehaviour_t63_CustomAttributesCacheGenerator
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType KeepAliveBehaviour_t63_0_0_0;
extern Il2CppType KeepAliveBehaviour_t63_1_0_0;
extern TypeInfo KeepAliveAbstractBehaviour_t64_il2cpp_TypeInfo;
struct KeepAliveBehaviour_t63;
extern CustomAttributesCache KeepAliveBehaviour_t63__CustomAttributeCache;
TypeInfo KeepAliveBehaviour_t63_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "KeepAliveBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, KeepAliveBehaviour_t63_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &KeepAliveAbstractBehaviour_t64_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &KeepAliveBehaviour_t63_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, KeepAliveBehaviour_t63_VTable/* vtable */
	, &KeepAliveBehaviour_t63__CustomAttributeCache/* custom_attributes_cache */
	, &KeepAliveBehaviour_t63_il2cpp_TypeInfo/* cast_class */
	, &KeepAliveBehaviour_t63_0_0_0/* byval_arg */
	, &KeepAliveBehaviour_t63_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (KeepAliveBehaviour_t63)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo MarkerBehaviour_t65_il2cpp_TypeInfo;
// Vuforia.MarkerBehaviour
#include "AssemblyU2DCSharp_Vuforia_MarkerBehaviourMethodDeclarations.h"

// Vuforia.MarkerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerAbstractBehavMethodDeclarations.h"
extern MethodInfo MarkerAbstractBehaviour__ctor_m475_MethodInfo;


// System.Void Vuforia.MarkerBehaviour::.ctor()
extern MethodInfo MarkerBehaviour__ctor_m159_MethodInfo;
 void MarkerBehaviour__ctor_m159 (MarkerBehaviour_t65 * __this, MethodInfo* method){
	{
		MarkerAbstractBehaviour__ctor_m475(__this, /*hidden argument*/&MarkerAbstractBehaviour__ctor_m475_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.MarkerBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.MarkerBehaviour::.ctor()
MethodInfo MarkerBehaviour__ctor_m159_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MarkerBehaviour__ctor_m159/* method */
	, &MarkerBehaviour_t65_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 160/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MarkerBehaviour_t65_MethodInfos[] =
{
	&MarkerBehaviour__ctor_m159_MethodInfo,
	NULL
};
extern MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m476_MethodInfo;
extern MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m477_MethodInfo;
extern MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m478_MethodInfo;
extern MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m479_MethodInfo;
extern MethodInfo TrackableBehaviour_OnTrackerUpdate_m480_MethodInfo;
extern MethodInfo MarkerAbstractBehaviour_InternalUnregisterTrackable_m481_MethodInfo;
extern MethodInfo MarkerAbstractBehaviour_CorrectScaleImpl_m482_MethodInfo;
extern MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_get_MarkerID_m483_MethodInfo;
extern MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_SetMarkerID_m484_MethodInfo;
extern MethodInfo MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_InitializeMarker_m485_MethodInfo;
static MethodInfo* MarkerBehaviour_t65_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m345_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m346_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m347_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m349_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m350_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m351_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m352_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m353_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m354_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m355_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m476_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m477_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m478_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m479_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m360_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m480_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m426_MethodInfo,
	&MarkerAbstractBehaviour_InternalUnregisterTrackable_m481_MethodInfo,
	&MarkerAbstractBehaviour_CorrectScaleImpl_m482_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_get_MarkerID_m483_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_SetMarkerID_m484_MethodInfo,
	&MarkerAbstractBehaviour_Vuforia_IEditorMarkerBehaviour_InitializeMarker_m485_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
};
extern TypeInfo IEditorMarkerBehaviour_t156_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair MarkerBehaviour_t65_InterfacesOffsets[] = 
{
	{ &IEditorMarkerBehaviour_t156_il2cpp_TypeInfo, 25},
	{ &IEditorTrackableBehaviour_t125_il2cpp_TypeInfo, 4},
	{ &WorldCenterTrackableBehaviour_t126_il2cpp_TypeInfo, 28},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType MarkerBehaviour_t65_0_0_0;
extern Il2CppType MarkerBehaviour_t65_1_0_0;
extern TypeInfo MarkerAbstractBehaviour_t58_il2cpp_TypeInfo;
struct MarkerBehaviour_t65;
TypeInfo MarkerBehaviour_t65_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MarkerBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, MarkerBehaviour_t65_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MarkerAbstractBehaviour_t58_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &MarkerBehaviour_t65_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, MarkerBehaviour_t65_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &MarkerBehaviour_t65_il2cpp_TypeInfo/* cast_class */
	, &MarkerBehaviour_t65_0_0_0/* byval_arg */
	, &MarkerBehaviour_t65_1_0_0/* this_arg */
	, MarkerBehaviour_t65_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MarkerBehaviour_t65)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 29/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo MaskOutBehaviour_t66_il2cpp_TypeInfo;
// Vuforia.MaskOutBehaviour
#include "AssemblyU2DCSharp_Vuforia_MaskOutBehaviourMethodDeclarations.h"

extern TypeInfo QCARRuntimeUtilities_t157_il2cpp_TypeInfo;
extern TypeInfo MaskOutAbstractBehaviour_t55_il2cpp_TypeInfo;
extern TypeInfo MaterialU5BU5D_t114_il2cpp_TypeInfo;
extern TypeInfo Material_t4_il2cpp_TypeInfo;
// Vuforia.MaskOutAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBehaMethodDeclarations.h"
// Vuforia.QCARRuntimeUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitieMethodDeclarations.h"
extern MethodInfo MaskOutAbstractBehaviour__ctor_m486_MethodInfo;
extern MethodInfo QCARRuntimeUtilities_IsQCAREnabled_m487_MethodInfo;
extern MethodInfo Renderer_set_sharedMaterial_m488_MethodInfo;
extern MethodInfo Renderer_set_sharedMaterials_m489_MethodInfo;


// System.Void Vuforia.MaskOutBehaviour::.ctor()
extern MethodInfo MaskOutBehaviour__ctor_m160_MethodInfo;
 void MaskOutBehaviour__ctor_m160 (MaskOutBehaviour_t66 * __this, MethodInfo* method){
	{
		MaskOutAbstractBehaviour__ctor_m486(__this, /*hidden argument*/&MaskOutAbstractBehaviour__ctor_m486_MethodInfo);
		return;
	}
}
// System.Void Vuforia.MaskOutBehaviour::Start()
extern MethodInfo MaskOutBehaviour_Start_m161_MethodInfo;
 void MaskOutBehaviour_Start_m161 (MaskOutBehaviour_t66 * __this, MethodInfo* method){
	Renderer_t7 * V_0 = {0};
	int32_t V_1 = 0;
	MaterialU5BU5D_t114* V_2 = {0};
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m487(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsQCAREnabled_m487_MethodInfo);
		if (!L_0)
		{
			goto IL_005b;
		}
	}
	{
		Renderer_t7 * L_1 = Component_GetComponent_TisRenderer_t7_m234(__this, /*hidden argument*/&Component_GetComponent_TisRenderer_t7_m234_MethodInfo);
		V_0 = L_1;
		NullCheck(V_0);
		MaterialU5BU5D_t114* L_2 = Renderer_get_materials_m308(V_0, /*hidden argument*/&Renderer_get_materials_m308_MethodInfo);
		NullCheck(L_2);
		V_1 = (((int32_t)(((Array_t *)L_2)->max_length)));
		if ((((uint32_t)V_1) != ((uint32_t)1)))
		{
			goto IL_0032;
		}
	}
	{
		Material_t4 * L_3 = (__this->___maskMaterial_2);
		NullCheck(V_0);
		Renderer_set_sharedMaterial_m488(V_0, L_3, /*hidden argument*/&Renderer_set_sharedMaterial_m488_MethodInfo);
		goto IL_005b;
	}

IL_0032:
	{
		V_2 = ((MaterialU5BU5D_t114*)SZArrayNew(InitializedTypeInfo(&MaterialU5BU5D_t114_il2cpp_TypeInfo), V_1));
		V_3 = 0;
		goto IL_004d;
	}

IL_0040:
	{
		Material_t4 * L_4 = (__this->___maskMaterial_2);
		NullCheck(V_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_2, V_3);
		ArrayElementTypeCheck (V_2, L_4);
		*((Material_t4 **)(Material_t4 **)SZArrayLdElema(V_2, V_3)) = (Material_t4 *)L_4;
		V_3 = ((int32_t)(V_3+1));
	}

IL_004d:
	{
		if ((((int32_t)V_3) < ((int32_t)V_1)))
		{
			goto IL_0040;
		}
	}
	{
		NullCheck(V_0);
		Renderer_set_sharedMaterials_m489(V_0, V_2, /*hidden argument*/&Renderer_set_sharedMaterials_m489_MethodInfo);
	}

IL_005b:
	{
		return;
	}
}
// Metadata Definition Vuforia.MaskOutBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.MaskOutBehaviour::.ctor()
MethodInfo MaskOutBehaviour__ctor_m160_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MaskOutBehaviour__ctor_m160/* method */
	, &MaskOutBehaviour_t66_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 161/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.MaskOutBehaviour::Start()
MethodInfo MaskOutBehaviour_Start_m161_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&MaskOutBehaviour_Start_m161/* method */
	, &MaskOutBehaviour_t66_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 162/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MaskOutBehaviour_t66_MethodInfos[] =
{
	&MaskOutBehaviour__ctor_m160_MethodInfo,
	&MaskOutBehaviour_Start_m161_MethodInfo,
	NULL
};
static MethodInfo* MaskOutBehaviour_t66_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType MaskOutBehaviour_t66_0_0_0;
extern Il2CppType MaskOutBehaviour_t66_1_0_0;
struct MaskOutBehaviour_t66;
TypeInfo MaskOutBehaviour_t66_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MaskOutBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, MaskOutBehaviour_t66_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MaskOutAbstractBehaviour_t55_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &MaskOutBehaviour_t66_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, MaskOutBehaviour_t66_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &MaskOutBehaviour_t66_il2cpp_TypeInfo/* cast_class */
	, &MaskOutBehaviour_t66_0_0_0/* byval_arg */
	, &MaskOutBehaviour_t66_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MaskOutBehaviour_t66)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo MultiTargetBehaviour_t67_il2cpp_TypeInfo;
// Vuforia.MultiTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_MultiTargetBehaviourMethodDeclarations.h"

// Vuforia.MultiTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MultiTargetAbstractMethodDeclarations.h"
extern MethodInfo MultiTargetAbstractBehaviour__ctor_m490_MethodInfo;


// System.Void Vuforia.MultiTargetBehaviour::.ctor()
extern MethodInfo MultiTargetBehaviour__ctor_m162_MethodInfo;
 void MultiTargetBehaviour__ctor_m162 (MultiTargetBehaviour_t67 * __this, MethodInfo* method){
	{
		MultiTargetAbstractBehaviour__ctor_m490(__this, /*hidden argument*/&MultiTargetAbstractBehaviour__ctor_m490_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.MultiTargetBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.MultiTargetBehaviour::.ctor()
MethodInfo MultiTargetBehaviour__ctor_m162_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&MultiTargetBehaviour__ctor_m162/* method */
	, &MultiTargetBehaviour_t67_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 163/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* MultiTargetBehaviour_t67_MethodInfos[] =
{
	&MultiTargetBehaviour__ctor_m162_MethodInfo,
	NULL
};
extern MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m491_MethodInfo;
extern MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m492_MethodInfo;
extern MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m493_MethodInfo;
extern MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m494_MethodInfo;
extern MethodInfo MultiTargetAbstractBehaviour_InternalUnregisterTrackable_m495_MethodInfo;
extern MethodInfo TrackableBehaviour_CorrectScaleImpl_m496_MethodInfo;
extern MethodInfo MultiTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m497_MethodInfo;
extern MethodInfo MultiTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m498_MethodInfo;
extern MethodInfo MultiTargetAbstractBehaviour_Vuforia_IEditorMultiTargetBehaviour_InitializeMultiTarget_m499_MethodInfo;
static MethodInfo* MultiTargetBehaviour_t67_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m345_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m346_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m347_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m349_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m350_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m351_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m352_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m353_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m354_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m355_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m491_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m492_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m493_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m494_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m360_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&DataSetTrackableBehaviour_OnTrackerUpdate_m361_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m426_MethodInfo,
	&MultiTargetAbstractBehaviour_InternalUnregisterTrackable_m495_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m496_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m365_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m366_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m367_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m368_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m369_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m370_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m371_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m372_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m373_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m374_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m375_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m376_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m377_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m378_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m379_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m380_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m381_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m382_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m383_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m384_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m385_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m386_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m387_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m388_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&DataSetTrackableBehaviour_OnDrawGizmos_m389_MethodInfo,
	&MultiTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m497_MethodInfo,
	&MultiTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m498_MethodInfo,
	&MultiTargetAbstractBehaviour_Vuforia_IEditorMultiTargetBehaviour_InitializeMultiTarget_m499_MethodInfo,
};
extern TypeInfo IEditorMultiTargetBehaviour_t158_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair MultiTargetBehaviour_t67_InterfacesOffsets[] = 
{
	{ &IEditorMultiTargetBehaviour_t158_il2cpp_TypeInfo, 53},
	{ &IEditorDataSetTrackableBehaviour_t124_il2cpp_TypeInfo, 25},
	{ &IEditorTrackableBehaviour_t125_il2cpp_TypeInfo, 4},
	{ &WorldCenterTrackableBehaviour_t126_il2cpp_TypeInfo, 49},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType MultiTargetBehaviour_t67_0_0_0;
extern Il2CppType MultiTargetBehaviour_t67_1_0_0;
extern TypeInfo MultiTargetAbstractBehaviour_t59_il2cpp_TypeInfo;
struct MultiTargetBehaviour_t67;
TypeInfo MultiTargetBehaviour_t67_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "MultiTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, MultiTargetBehaviour_t67_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MultiTargetAbstractBehaviour_t59_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &MultiTargetBehaviour_t67_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, MultiTargetBehaviour_t67_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &MultiTargetBehaviour_t67_il2cpp_TypeInfo/* cast_class */
	, &MultiTargetBehaviour_t67_0_0_0/* byval_arg */
	, &MultiTargetBehaviour_t67_1_0_0/* this_arg */
	, MultiTargetBehaviour_t67_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (MultiTargetBehaviour_t67)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 54/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ObjectTargetBehaviour_t68_il2cpp_TypeInfo;
// Vuforia.ObjectTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ObjectTargetBehaviourMethodDeclarations.h"

// Vuforia.ObjectTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTargetAbstracMethodDeclarations.h"
extern MethodInfo ObjectTargetAbstractBehaviour__ctor_m500_MethodInfo;


// System.Void Vuforia.ObjectTargetBehaviour::.ctor()
extern MethodInfo ObjectTargetBehaviour__ctor_m163_MethodInfo;
 void ObjectTargetBehaviour__ctor_m163 (ObjectTargetBehaviour_t68 * __this, MethodInfo* method){
	{
		ObjectTargetAbstractBehaviour__ctor_m500(__this, /*hidden argument*/&ObjectTargetAbstractBehaviour__ctor_m500_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.ObjectTargetBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ObjectTargetBehaviour::.ctor()
MethodInfo ObjectTargetBehaviour__ctor_m163_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectTargetBehaviour__ctor_m163/* method */
	, &ObjectTargetBehaviour_t68_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 164/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ObjectTargetBehaviour_t68_MethodInfos[] =
{
	&ObjectTargetBehaviour__ctor_m163_MethodInfo,
	NULL
};
extern MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m501_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m502_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m503_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m504_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_InternalUnregisterTrackable_m505_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_CorrectScaleImpl_m506_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m507_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m508_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_PreviewImage_m509_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetPreviewImage_m510_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXY_m511_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXZ_m512_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetAspectRatio_m513_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_GetSize_m514_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_SetLength_m515_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_SetWidth_m516_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_SetHeight_m517_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetShowBoundingBox_m518_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_ShowBoundingBox_m519_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_SetBoundingBox_m520_MethodInfo;
extern MethodInfo ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_InitializeObjectTarget_m521_MethodInfo;
static MethodInfo* ObjectTargetBehaviour_t68_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m345_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m346_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m347_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m349_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m350_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m351_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m352_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m353_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m354_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m355_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m501_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m502_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m503_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m504_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m360_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&DataSetTrackableBehaviour_OnTrackerUpdate_m361_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m426_MethodInfo,
	&ObjectTargetAbstractBehaviour_InternalUnregisterTrackable_m505_MethodInfo,
	&ObjectTargetAbstractBehaviour_CorrectScaleImpl_m506_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetName_m365_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_DataSetPath_m366_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetDataSetPath_m367_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ExtendedTracking_m368_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetExtendedTracking_m369_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_InitializeSmartTerrain_m370_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetInitializeSmartTerrain_m371_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_ReconstructionToInitialize_m372_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetReconstructionToInitialize_m373_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMin_m374_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMin_m375_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderBoundsMax_m376_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderBoundsMax_m377_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_IsSmartTerrainOccluderOffset_m378_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetIsSmartTerrainOccluderOffset_m379_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderOffset_m380_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderOffset_m381_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderRotation_m382_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetSmartTerrainOccluderRotation_m383_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_SmartTerrainOccluderLockedInPlace_m384_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetLockSmartTerrainOccluderInPlace_m385_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_GetDefaultOccluderBounds_m386_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_get_AutoSetOccluderFromTargetSize_m387_MethodInfo,
	&DataSetTrackableBehaviour_Vuforia_IEditorDataSetTrackableBehaviour_SetAutoSetOccluderFromTargetSize_m388_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&DataSetTrackableBehaviour_OnDrawGizmos_m389_MethodInfo,
	&ObjectTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m507_MethodInfo,
	&ObjectTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m508_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_PreviewImage_m509_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetPreviewImage_m510_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXY_m511_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_AspectRatioXZ_m512_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetAspectRatio_m513_MethodInfo,
	&ObjectTargetAbstractBehaviour_GetSize_m514_MethodInfo,
	&ObjectTargetAbstractBehaviour_SetLength_m515_MethodInfo,
	&ObjectTargetAbstractBehaviour_SetWidth_m516_MethodInfo,
	&ObjectTargetAbstractBehaviour_SetHeight_m517_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_SetShowBoundingBox_m518_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_get_ShowBoundingBox_m519_MethodInfo,
	&ObjectTargetAbstractBehaviour_SetBoundingBox_m520_MethodInfo,
	&ObjectTargetAbstractBehaviour_Vuforia_IEditorObjectTargetBehaviour_InitializeObjectTarget_m521_MethodInfo,
};
extern TypeInfo IEditorObjectTargetBehaviour_t159_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair ObjectTargetBehaviour_t68_InterfacesOffsets[] = 
{
	{ &IEditorObjectTargetBehaviour_t159_il2cpp_TypeInfo, 53},
	{ &IEditorDataSetTrackableBehaviour_t124_il2cpp_TypeInfo, 25},
	{ &IEditorTrackableBehaviour_t125_il2cpp_TypeInfo, 4},
	{ &WorldCenterTrackableBehaviour_t126_il2cpp_TypeInfo, 49},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType ObjectTargetBehaviour_t68_0_0_0;
extern Il2CppType ObjectTargetBehaviour_t68_1_0_0;
extern TypeInfo ObjectTargetAbstractBehaviour_t62_il2cpp_TypeInfo;
struct ObjectTargetBehaviour_t68;
TypeInfo ObjectTargetBehaviour_t68_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ObjectTargetBehaviour_t68_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ObjectTargetAbstractBehaviour_t62_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ObjectTargetBehaviour_t68_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ObjectTargetBehaviour_t68_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ObjectTargetBehaviour_t68_il2cpp_TypeInfo/* cast_class */
	, &ObjectTargetBehaviour_t68_0_0_0/* byval_arg */
	, &ObjectTargetBehaviour_t68_1_0_0/* this_arg */
	, ObjectTargetBehaviour_t68_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectTargetBehaviour_t68)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 66/* vtable_count */
	, 0/* interfaces_count */
	, 4/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo PropBehaviour_t39_il2cpp_TypeInfo;
// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviourMethodDeclarations.h"

// Vuforia.PropAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PropAbstractBehavioMethodDeclarations.h"
extern MethodInfo PropAbstractBehaviour__ctor_m522_MethodInfo;


// System.Void Vuforia.PropBehaviour::.ctor()
extern MethodInfo PropBehaviour__ctor_m164_MethodInfo;
 void PropBehaviour__ctor_m164 (PropBehaviour_t39 * __this, MethodInfo* method){
	{
		PropAbstractBehaviour__ctor_m522(__this, /*hidden argument*/&PropAbstractBehaviour__ctor_m522_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.PropBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.PropBehaviour::.ctor()
MethodInfo PropBehaviour__ctor_m164_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PropBehaviour__ctor_m164/* method */
	, &PropBehaviour_t39_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 165/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PropBehaviour_t39_MethodInfos[] =
{
	&PropBehaviour__ctor_m164_MethodInfo,
	NULL
};
extern MethodInfo PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m523_MethodInfo;
extern MethodInfo PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m524_MethodInfo;
extern MethodInfo PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m525_MethodInfo;
extern MethodInfo PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m526_MethodInfo;
extern MethodInfo PropAbstractBehaviour_InternalUnregisterTrackable_m527_MethodInfo;
extern MethodInfo PropAbstractBehaviour_UpdateMeshAndColliders_m528_MethodInfo;
extern MethodInfo PropAbstractBehaviour_Start_m529_MethodInfo;
extern MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_InitializeProp_m530_MethodInfo;
extern MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshFilterToUpdate_m531_MethodInfo;
extern MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshFilterToUpdate_m532_MethodInfo;
extern MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshColliderToUpdate_m533_MethodInfo;
extern MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshColliderToUpdate_m534_MethodInfo;
extern MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetBoxColliderToUpdate_m535_MethodInfo;
extern MethodInfo PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_BoxColliderToUpdate_m536_MethodInfo;
static MethodInfo* PropBehaviour_t39_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m345_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m346_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m347_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m349_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m350_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m351_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m352_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m353_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m354_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m355_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m523_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m524_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m525_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m526_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m360_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m480_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m426_MethodInfo,
	&PropAbstractBehaviour_InternalUnregisterTrackable_m527_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m496_MethodInfo,
	&PropAbstractBehaviour_UpdateMeshAndColliders_m528_MethodInfo,
	&PropAbstractBehaviour_Start_m529_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_InitializeProp_m530_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshFilterToUpdate_m531_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshFilterToUpdate_m532_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshColliderToUpdate_m533_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshColliderToUpdate_m534_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetBoxColliderToUpdate_m535_MethodInfo,
	&PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_BoxColliderToUpdate_m536_MethodInfo,
};
extern TypeInfo IEditorPropBehaviour_t160_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair PropBehaviour_t39_InterfacesOffsets[] = 
{
	{ &IEditorPropBehaviour_t160_il2cpp_TypeInfo, 27},
	{ &IEditorTrackableBehaviour_t125_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType PropBehaviour_t39_0_0_0;
extern Il2CppType PropBehaviour_t39_1_0_0;
extern TypeInfo PropAbstractBehaviour_t69_il2cpp_TypeInfo;
struct PropBehaviour_t39;
TypeInfo PropBehaviour_t39_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, PropBehaviour_t39_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &PropAbstractBehaviour_t69_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &PropBehaviour_t39_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, PropBehaviour_t39_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &PropBehaviour_t39_il2cpp_TypeInfo/* cast_class */
	, &PropBehaviour_t39_0_0_0/* byval_arg */
	, &PropBehaviour_t39_1_0_0/* this_arg */
	, PropBehaviour_t39_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropBehaviour_t39)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 34/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviourMethodDeclarations.h"

// Vuforia.NullUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayer.h"
// UnityEngine.RuntimePlatform
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
// Vuforia.PlayModeUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayer.h"
extern TypeInfo NullUnityPlayer_t161_il2cpp_TypeInfo;
extern TypeInfo PlayModeUnityPlayer_t162_il2cpp_TypeInfo;
// Vuforia.NullUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_NullUnityPlayerMethodDeclarations.h"
// Vuforia.PlayModeUnityPlayer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_PlayModeUnityPlayerMethodDeclarations.h"
extern MethodInfo QCARAbstractBehaviour__ctor_m537_MethodInfo;
extern MethodInfo NullUnityPlayer__ctor_m538_MethodInfo;
extern MethodInfo Application_get_platform_m539_MethodInfo;
extern MethodInfo QCARRuntimeUtilities_IsPlayMode_m540_MethodInfo;
extern MethodInfo PlayModeUnityPlayer__ctor_m541_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_SetUnityPlayerImplementation_m542_MethodInfo;
extern MethodInfo GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t52_m543_MethodInfo;
struct GameObject_t2;
// Declaration !!0 UnityEngine.GameObject::AddComponent<Vuforia.ComponentFactoryStarterBehaviour>()
// !!0 UnityEngine.GameObject::AddComponent<Vuforia.ComponentFactoryStarterBehaviour>()
 ComponentFactoryStarterBehaviour_t52 * GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t52_m543 (GameObject_t2 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void Vuforia.QCARBehaviour::.ctor()
extern MethodInfo QCARBehaviour__ctor_m165_MethodInfo;
 void QCARBehaviour__ctor_m165 (QCARBehaviour_t70 * __this, MethodInfo* method){
	{
		QCARAbstractBehaviour__ctor_m537(__this, /*hidden argument*/&QCARAbstractBehaviour__ctor_m537_MethodInfo);
		return;
	}
}
// System.Void Vuforia.QCARBehaviour::Awake()
extern MethodInfo QCARBehaviour_Awake_m166_MethodInfo;
 void QCARBehaviour_Awake_m166 (QCARBehaviour_t70 * __this, MethodInfo* method){
	Object_t * V_0 = {0};
	{
		NullUnityPlayer_t161 * L_0 = (NullUnityPlayer_t161 *)il2cpp_codegen_object_new (InitializedTypeInfo(&NullUnityPlayer_t161_il2cpp_TypeInfo));
		NullUnityPlayer__ctor_m538(L_0, /*hidden argument*/&NullUnityPlayer__ctor_m538_MethodInfo);
		V_0 = L_0;
		int32_t L_1 = Application_get_platform_m539(NULL /*static, unused*/, /*hidden argument*/&Application_get_platform_m539_MethodInfo);
		if ((((uint32_t)L_1) != ((uint32_t)((int32_t)11))))
		{
			goto IL_001d;
		}
	}
	{
		AndroidUnityPlayer_t51 * L_2 = (AndroidUnityPlayer_t51 *)il2cpp_codegen_object_new (InitializedTypeInfo(&AndroidUnityPlayer_t51_il2cpp_TypeInfo));
		AndroidUnityPlayer__ctor_m115(L_2, /*hidden argument*/&AndroidUnityPlayer__ctor_m115_MethodInfo);
		V_0 = L_2;
		goto IL_0043;
	}

IL_001d:
	{
		int32_t L_3 = Application_get_platform_m539(NULL /*static, unused*/, /*hidden argument*/&Application_get_platform_m539_MethodInfo);
		if ((((uint32_t)L_3) != ((uint32_t)8)))
		{
			goto IL_0033;
		}
	}
	{
		IOSUnityPlayer_t53 * L_4 = (IOSUnityPlayer_t53 *)il2cpp_codegen_object_new (InitializedTypeInfo(&IOSUnityPlayer_t53_il2cpp_TypeInfo));
		IOSUnityPlayer__ctor_m133(L_4, /*hidden argument*/&IOSUnityPlayer__ctor_m133_MethodInfo);
		V_0 = L_4;
		goto IL_0043;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		bool L_5 = QCARRuntimeUtilities_IsPlayMode_m540(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsPlayMode_m540_MethodInfo);
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		PlayModeUnityPlayer_t162 * L_6 = (PlayModeUnityPlayer_t162 *)il2cpp_codegen_object_new (InitializedTypeInfo(&PlayModeUnityPlayer_t162_il2cpp_TypeInfo));
		PlayModeUnityPlayer__ctor_m541(L_6, /*hidden argument*/&PlayModeUnityPlayer__ctor_m541_MethodInfo);
		V_0 = L_6;
	}

IL_0043:
	{
		QCARAbstractBehaviour_SetUnityPlayerImplementation_m542(__this, V_0, /*hidden argument*/&QCARAbstractBehaviour_SetUnityPlayerImplementation_m542_MethodInfo);
		GameObject_t2 * L_7 = Component_get_gameObject_m322(__this, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		NullCheck(L_7);
		GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t52_m543(L_7, /*hidden argument*/&GameObject_AddComponent_TisComponentFactoryStarterBehaviour_t52_m543_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.QCARBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARBehaviour::.ctor()
MethodInfo QCARBehaviour__ctor_m165_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&QCARBehaviour__ctor_m165/* method */
	, &QCARBehaviour_t70_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 166/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARBehaviour::Awake()
MethodInfo QCARBehaviour_Awake_m166_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&QCARBehaviour_Awake_m166/* method */
	, &QCARBehaviour_t70_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 167/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* QCARBehaviour_t70_MethodInfos[] =
{
	&QCARBehaviour__ctor_m165_MethodInfo,
	&QCARBehaviour_Awake_m166_MethodInfo,
	NULL
};
extern MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDeviceMode_m544_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousImageTargets_m545_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousImageTargets_m546_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousObjectTargets_m547_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousObjectTargets_m548_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetDelayedLoadingObjectTargets_m549_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetUseDelayedLoadingObjectTargets_m550_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetCameraDirection_m551_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDirection_m552_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMirrorVideoBackground_m553_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMirrorVideoBackground_m554_MethodInfo;
static MethodInfo* QCARBehaviour_t70_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDeviceMode_m544_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousImageTargets_m545_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousImageTargets_m546_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousObjectTargets_m547_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousObjectTargets_m548_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetDelayedLoadingObjectTargets_m549_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetUseDelayedLoadingObjectTargets_m550_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetCameraDirection_m551_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDirection_m552_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMirrorVideoBackground_m553_MethodInfo,
	&QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMirrorVideoBackground_m554_MethodInfo,
};
extern TypeInfo IEditorQCARBehaviour_t163_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair QCARBehaviour_t70_InterfacesOffsets[] = 
{
	{ &IEditorQCARBehaviour_t163_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType QCARBehaviour_t70_0_0_0;
extern Il2CppType QCARBehaviour_t70_1_0_0;
struct QCARBehaviour_t70;
TypeInfo QCARBehaviour_t70_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "QCARBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, QCARBehaviour_t70_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &QCARAbstractBehaviour_t71_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &QCARBehaviour_t70_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, QCARBehaviour_t70_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &QCARBehaviour_t70_il2cpp_TypeInfo/* cast_class */
	, &QCARBehaviour_t70_0_0_0/* byval_arg */
	, &QCARBehaviour_t70_1_0_0/* this_arg */
	, QCARBehaviour_t70_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (QCARBehaviour_t70)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 15/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ReconstructionBehaviour_t38_il2cpp_TypeInfo;
// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviourMethodDeclarations.h"

extern MethodInfo ReconstructionAbstractBehaviour__ctor_m555_MethodInfo;


// System.Void Vuforia.ReconstructionBehaviour::.ctor()
extern MethodInfo ReconstructionBehaviour__ctor_m167_MethodInfo;
 void ReconstructionBehaviour__ctor_m167 (ReconstructionBehaviour_t38 * __this, MethodInfo* method){
	{
		ReconstructionAbstractBehaviour__ctor_m555(__this, /*hidden argument*/&ReconstructionAbstractBehaviour__ctor_m555_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.ReconstructionBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ReconstructionBehaviour::.ctor()
MethodInfo ReconstructionBehaviour__ctor_m167_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReconstructionBehaviour__ctor_m167/* method */
	, &ReconstructionBehaviour_t38_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 168/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ReconstructionBehaviour_t38_MethodInfos[] =
{
	&ReconstructionBehaviour__ctor_m167_MethodInfo,
	NULL
};
extern MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_InitializedInEditor_m556_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetInitializedInEditor_m557_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtentEnabled_m558_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtentEnabled_m559_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtent_m560_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtent_m561_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetAutomaticStart_m562_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_AutomaticStart_m563_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshUpdates_m564_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshUpdates_m565_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshPadding_m566_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshPadding_m567_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorMeshesByFactor_m568_MethodInfo;
extern MethodInfo ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorPropPositionsByFactor_m569_MethodInfo;
static MethodInfo* ReconstructionBehaviour_t38_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_InitializedInEditor_m556_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetInitializedInEditor_m557_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtentEnabled_m558_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtentEnabled_m559_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtent_m560_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtent_m561_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetAutomaticStart_m562_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_AutomaticStart_m563_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshUpdates_m564_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshUpdates_m565_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshPadding_m566_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshPadding_m567_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorMeshesByFactor_m568_MethodInfo,
	&ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorPropPositionsByFactor_m569_MethodInfo,
};
extern TypeInfo IEditorReconstructionBehaviour_t164_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair ReconstructionBehaviour_t38_InterfacesOffsets[] = 
{
	{ &IEditorReconstructionBehaviour_t164_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType ReconstructionBehaviour_t38_0_0_0;
extern Il2CppType ReconstructionBehaviour_t38_1_0_0;
extern TypeInfo ReconstructionAbstractBehaviour_t72_il2cpp_TypeInfo;
struct ReconstructionBehaviour_t38;
TypeInfo ReconstructionBehaviour_t38_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReconstructionBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ReconstructionBehaviour_t38_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ReconstructionAbstractBehaviour_t72_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ReconstructionBehaviour_t38_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ReconstructionBehaviour_t38_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ReconstructionBehaviour_t38_il2cpp_TypeInfo/* cast_class */
	, &ReconstructionBehaviour_t38_0_0_0/* byval_arg */
	, &ReconstructionBehaviour_t38_1_0_0/* this_arg */
	, ReconstructionBehaviour_t38_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReconstructionBehaviour_t38)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo;
// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviourMethodDeclarations.h"

// Vuforia.ReconstructionFromTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromTMethodDeclarations.h"
extern MethodInfo ReconstructionFromTargetAbstractBehaviour__ctor_m570_MethodInfo;


// System.Void Vuforia.ReconstructionFromTargetBehaviour::.ctor()
extern MethodInfo ReconstructionFromTargetBehaviour__ctor_m168_MethodInfo;
 void ReconstructionFromTargetBehaviour__ctor_m168 (ReconstructionFromTargetBehaviour_t73 * __this, MethodInfo* method){
	{
		ReconstructionFromTargetAbstractBehaviour__ctor_m570(__this, /*hidden argument*/&ReconstructionFromTargetAbstractBehaviour__ctor_m570_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.ReconstructionFromTargetBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.ReconstructionFromTargetBehaviour::.ctor()
MethodInfo ReconstructionFromTargetBehaviour__ctor_m168_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ReconstructionFromTargetBehaviour__ctor_m168/* method */
	, &ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 169/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ReconstructionFromTargetBehaviour_t73_MethodInfos[] =
{
	&ReconstructionFromTargetBehaviour__ctor_m168_MethodInfo,
	NULL
};
static MethodInfo* ReconstructionFromTargetBehaviour_t73_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType ReconstructionFromTargetBehaviour_t73_0_0_0;
extern Il2CppType ReconstructionFromTargetBehaviour_t73_1_0_0;
extern TypeInfo ReconstructionFromTargetAbstractBehaviour_t74_il2cpp_TypeInfo;
struct ReconstructionFromTargetBehaviour_t73;
TypeInfo ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "ReconstructionFromTargetBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, ReconstructionFromTargetBehaviour_t73_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ReconstructionFromTargetAbstractBehaviour_t74_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ReconstructionFromTargetBehaviour_t73_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo/* cast_class */
	, &ReconstructionFromTargetBehaviour_t73_0_0_0/* byval_arg */
	, &ReconstructionFromTargetBehaviour_t73_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ReconstructionFromTargetBehaviour_t73)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo;
// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviourMethodDeclarations.h"

// Vuforia.SmartTerrainTrackerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackerMethodDeclarations.h"
extern MethodInfo SmartTerrainTrackerAbstractBehaviour__ctor_m571_MethodInfo;


// System.Void Vuforia.SmartTerrainTrackerBehaviour::.ctor()
extern MethodInfo SmartTerrainTrackerBehaviour__ctor_m169_MethodInfo;
 void SmartTerrainTrackerBehaviour__ctor_m169 (SmartTerrainTrackerBehaviour_t75 * __this, MethodInfo* method){
	{
		SmartTerrainTrackerAbstractBehaviour__ctor_m571(__this, /*hidden argument*/&SmartTerrainTrackerAbstractBehaviour__ctor_m571_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.SmartTerrainTrackerBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SmartTerrainTrackerBehaviour::.ctor()
MethodInfo SmartTerrainTrackerBehaviour__ctor_m169_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SmartTerrainTrackerBehaviour__ctor_m169/* method */
	, &SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 170/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SmartTerrainTrackerBehaviour_t75_MethodInfos[] =
{
	&SmartTerrainTrackerBehaviour__ctor_m169_MethodInfo,
	NULL
};
extern MethodInfo SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetAutomaticStart_m572_MethodInfo;
extern MethodInfo SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_AutomaticStart_m573_MethodInfo;
extern MethodInfo SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetSmartTerrainScaleToMM_m574_MethodInfo;
extern MethodInfo SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_ScaleToMM_m575_MethodInfo;
static MethodInfo* SmartTerrainTrackerBehaviour_t75_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetAutomaticStart_m572_MethodInfo,
	&SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_AutomaticStart_m573_MethodInfo,
	&SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetSmartTerrainScaleToMM_m574_MethodInfo,
	&SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_ScaleToMM_m575_MethodInfo,
};
extern TypeInfo IEditorSmartTerrainTrackerBehaviour_t165_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair SmartTerrainTrackerBehaviour_t75_InterfacesOffsets[] = 
{
	{ &IEditorSmartTerrainTrackerBehaviour_t165_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType SmartTerrainTrackerBehaviour_t75_0_0_0;
extern Il2CppType SmartTerrainTrackerBehaviour_t75_1_0_0;
extern TypeInfo SmartTerrainTrackerAbstractBehaviour_t76_il2cpp_TypeInfo;
struct SmartTerrainTrackerBehaviour_t75;
TypeInfo SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SmartTerrainTrackerBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, SmartTerrainTrackerBehaviour_t75_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &SmartTerrainTrackerAbstractBehaviour_t76_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SmartTerrainTrackerBehaviour_t75_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo/* cast_class */
	, &SmartTerrainTrackerBehaviour_t75_0_0_0/* byval_arg */
	, &SmartTerrainTrackerBehaviour_t75_1_0_0/* this_arg */
	, SmartTerrainTrackerBehaviour_t75_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SmartTerrainTrackerBehaviour_t75)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SurfaceBehaviour_t40_il2cpp_TypeInfo;
// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviourMethodDeclarations.h"

// Vuforia.SurfaceAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBehaMethodDeclarations.h"
extern MethodInfo SurfaceAbstractBehaviour__ctor_m576_MethodInfo;


// System.Void Vuforia.SurfaceBehaviour::.ctor()
extern MethodInfo SurfaceBehaviour__ctor_m170_MethodInfo;
 void SurfaceBehaviour__ctor_m170 (SurfaceBehaviour_t40 * __this, MethodInfo* method){
	{
		SurfaceAbstractBehaviour__ctor_m576(__this, /*hidden argument*/&SurfaceAbstractBehaviour__ctor_m576_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.SurfaceBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceBehaviour::.ctor()
MethodInfo SurfaceBehaviour__ctor_m170_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SurfaceBehaviour__ctor_m170/* method */
	, &SurfaceBehaviour_t40_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 171/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SurfaceBehaviour_t40_MethodInfos[] =
{
	&SurfaceBehaviour__ctor_m170_MethodInfo,
	NULL
};
extern MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m577_MethodInfo;
extern MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m578_MethodInfo;
extern MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m579_MethodInfo;
extern MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m580_MethodInfo;
extern MethodInfo SurfaceAbstractBehaviour_InternalUnregisterTrackable_m581_MethodInfo;
extern MethodInfo SmartTerrainTrackableBehaviour_UpdateMeshAndColliders_m582_MethodInfo;
extern MethodInfo SmartTerrainTrackableBehaviour_Start_m583_MethodInfo;
extern MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_InitializeSurface_m584_MethodInfo;
extern MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshFilterToUpdate_m585_MethodInfo;
extern MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshFilterToUpdate_m586_MethodInfo;
extern MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshColliderToUpdate_m587_MethodInfo;
extern MethodInfo SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshColliderToUpdate_m588_MethodInfo;
static MethodInfo* SurfaceBehaviour_t40_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m345_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m346_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m347_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m349_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m350_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m351_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m352_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m353_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m354_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m355_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m577_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m578_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m579_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m580_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m360_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m480_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m426_MethodInfo,
	&SurfaceAbstractBehaviour_InternalUnregisterTrackable_m581_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m496_MethodInfo,
	&SmartTerrainTrackableBehaviour_UpdateMeshAndColliders_m582_MethodInfo,
	&SmartTerrainTrackableBehaviour_Start_m583_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_InitializeSurface_m584_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshFilterToUpdate_m585_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshFilterToUpdate_m586_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshColliderToUpdate_m587_MethodInfo,
	&SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshColliderToUpdate_m588_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
};
extern TypeInfo IEditorSurfaceBehaviour_t166_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair SurfaceBehaviour_t40_InterfacesOffsets[] = 
{
	{ &IEditorSurfaceBehaviour_t166_il2cpp_TypeInfo, 27},
	{ &IEditorTrackableBehaviour_t125_il2cpp_TypeInfo, 4},
	{ &WorldCenterTrackableBehaviour_t126_il2cpp_TypeInfo, 32},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType SurfaceBehaviour_t40_0_0_0;
extern Il2CppType SurfaceBehaviour_t40_1_0_0;
extern TypeInfo SurfaceAbstractBehaviour_t77_il2cpp_TypeInfo;
struct SurfaceBehaviour_t40;
TypeInfo SurfaceBehaviour_t40_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "SurfaceBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, SurfaceBehaviour_t40_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &SurfaceAbstractBehaviour_t77_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SurfaceBehaviour_t40_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SurfaceBehaviour_t40_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SurfaceBehaviour_t40_il2cpp_TypeInfo/* cast_class */
	, &SurfaceBehaviour_t40_0_0_0/* byval_arg */
	, &SurfaceBehaviour_t40_1_0_0/* this_arg */
	, SurfaceBehaviour_t40_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SurfaceBehaviour_t40)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 33/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TextRecoBehaviour_t78_il2cpp_TypeInfo;
// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviourMethodDeclarations.h"

// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBehMethodDeclarations.h"
extern MethodInfo TextRecoAbstractBehaviour__ctor_m589_MethodInfo;


// System.Void Vuforia.TextRecoBehaviour::.ctor()
extern MethodInfo TextRecoBehaviour__ctor_m171_MethodInfo;
 void TextRecoBehaviour__ctor_m171 (TextRecoBehaviour_t78 * __this, MethodInfo* method){
	{
		TextRecoAbstractBehaviour__ctor_m589(__this, /*hidden argument*/&TextRecoAbstractBehaviour__ctor_m589_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.TextRecoBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoBehaviour::.ctor()
MethodInfo TextRecoBehaviour__ctor_m171_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextRecoBehaviour__ctor_m171/* method */
	, &TextRecoBehaviour_t78_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 172/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TextRecoBehaviour_t78_MethodInfos[] =
{
	&TextRecoBehaviour__ctor_m171_MethodInfo,
	NULL
};
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m590_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m591_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m592_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m593_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m594_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m595_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m596_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m597_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m598_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m599_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m600_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m601_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m602_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m603_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m604_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m605_MethodInfo;
static MethodInfo* TextRecoBehaviour_t78_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m590_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m591_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m592_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m593_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m594_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m595_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m596_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m597_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m598_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m599_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m600_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m601_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m602_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m603_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m604_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m605_MethodInfo,
};
extern TypeInfo IEditorTextRecoBehaviour_t167_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair TextRecoBehaviour_t78_InterfacesOffsets[] = 
{
	{ &IEditorTextRecoBehaviour_t167_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
extern Il2CppType TextRecoBehaviour_t78_1_0_0;
extern TypeInfo TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo;
struct TextRecoBehaviour_t78;
TypeInfo TextRecoBehaviour_t78_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextRecoBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TextRecoBehaviour_t78_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TextRecoBehaviour_t78_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TextRecoBehaviour_t78_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TextRecoBehaviour_t78_il2cpp_TypeInfo/* cast_class */
	, &TextRecoBehaviour_t78_0_0_0/* byval_arg */
	, &TextRecoBehaviour_t78_1_0_0/* this_arg */
	, TextRecoBehaviour_t78_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextRecoBehaviour_t78)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 20/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TurnOffBehaviour_t79_il2cpp_TypeInfo;
// Vuforia.TurnOffBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffBehaviourMethodDeclarations.h"

// UnityEngine.MeshRenderer
#include "UnityEngine_UnityEngine_MeshRenderer.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilter.h"
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBehaMethodDeclarations.h"
extern MethodInfo TurnOffAbstractBehaviour__ctor_m606_MethodInfo;
extern MethodInfo Component_GetComponent_TisMeshRenderer_t168_m607_MethodInfo;
extern MethodInfo Object_Destroy_m608_MethodInfo;
extern MethodInfo Component_GetComponent_TisMeshFilter_t169_m609_MethodInfo;
struct Component_t100;
// UnityEngine.CastHelper`1<UnityEngine.MeshRenderer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_6.h"
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t168_m607(__this, method) (MeshRenderer_t168 *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)
struct Component_t100;
// UnityEngine.CastHelper`1<UnityEngine.MeshFilter>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_7.h"
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t169_m609(__this, method) (MeshFilter_t169 *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)


// System.Void Vuforia.TurnOffBehaviour::.ctor()
extern MethodInfo TurnOffBehaviour__ctor_m172_MethodInfo;
 void TurnOffBehaviour__ctor_m172 (TurnOffBehaviour_t79 * __this, MethodInfo* method){
	{
		TurnOffAbstractBehaviour__ctor_m606(__this, /*hidden argument*/&TurnOffAbstractBehaviour__ctor_m606_MethodInfo);
		return;
	}
}
// System.Void Vuforia.TurnOffBehaviour::Awake()
extern MethodInfo TurnOffBehaviour_Awake_m173_MethodInfo;
 void TurnOffBehaviour_Awake_m173 (TurnOffBehaviour_t79 * __this, MethodInfo* method){
	MeshRenderer_t168 * V_0 = {0};
	MeshFilter_t169 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m487(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsQCAREnabled_m487_MethodInfo);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		MeshRenderer_t168 * L_1 = Component_GetComponent_TisMeshRenderer_t168_m607(__this, /*hidden argument*/&Component_GetComponent_TisMeshRenderer_t168_m607_MethodInfo);
		V_0 = L_1;
		Object_Destroy_m608(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_Destroy_m608_MethodInfo);
		MeshFilter_t169 * L_2 = Component_GetComponent_TisMeshFilter_t169_m609(__this, /*hidden argument*/&Component_GetComponent_TisMeshFilter_t169_m609_MethodInfo);
		V_1 = L_2;
		Object_Destroy_m608(NULL /*static, unused*/, V_1, /*hidden argument*/&Object_Destroy_m608_MethodInfo);
	}

IL_0024:
	{
		return;
	}
}
// Metadata Definition Vuforia.TurnOffBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffBehaviour::.ctor()
MethodInfo TurnOffBehaviour__ctor_m172_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TurnOffBehaviour__ctor_m172/* method */
	, &TurnOffBehaviour_t79_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 173/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffBehaviour::Awake()
MethodInfo TurnOffBehaviour_Awake_m173_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&TurnOffBehaviour_Awake_m173/* method */
	, &TurnOffBehaviour_t79_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 174/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TurnOffBehaviour_t79_MethodInfos[] =
{
	&TurnOffBehaviour__ctor_m172_MethodInfo,
	&TurnOffBehaviour_Awake_m173_MethodInfo,
	NULL
};
static MethodInfo* TurnOffBehaviour_t79_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType TurnOffBehaviour_t79_0_0_0;
extern Il2CppType TurnOffBehaviour_t79_1_0_0;
extern TypeInfo TurnOffAbstractBehaviour_t57_il2cpp_TypeInfo;
struct TurnOffBehaviour_t79;
TypeInfo TurnOffBehaviour_t79_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TurnOffBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TurnOffBehaviour_t79_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &TurnOffAbstractBehaviour_t57_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TurnOffBehaviour_t79_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TurnOffBehaviour_t79_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TurnOffBehaviour_t79_il2cpp_TypeInfo/* cast_class */
	, &TurnOffBehaviour_t79_0_0_0/* byval_arg */
	, &TurnOffBehaviour_t79_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TurnOffBehaviour_t79)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TurnOffWordBehaviour_t80_il2cpp_TypeInfo;
// Vuforia.TurnOffWordBehaviour
#include "AssemblyU2DCSharp_Vuforia_TurnOffWordBehaviourMethodDeclarations.h"

extern MethodInfo Transform_FindChild_m610_MethodInfo;


// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
extern MethodInfo TurnOffWordBehaviour__ctor_m174_MethodInfo;
 void TurnOffWordBehaviour__ctor_m174 (TurnOffWordBehaviour_t80 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
extern MethodInfo TurnOffWordBehaviour_Awake_m175_MethodInfo;
 void TurnOffWordBehaviour_Awake_m175 (TurnOffWordBehaviour_t80 * __this, MethodInfo* method){
	MeshRenderer_t168 * V_0 = {0};
	Transform_t10 * V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m487(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsQCAREnabled_m487_MethodInfo);
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		MeshRenderer_t168 * L_1 = Component_GetComponent_TisMeshRenderer_t168_m607(__this, /*hidden argument*/&Component_GetComponent_TisMeshRenderer_t168_m607_MethodInfo);
		V_0 = L_1;
		Object_Destroy_m608(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_Destroy_m608_MethodInfo);
		Transform_t10 * L_2 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_2);
		Transform_t10 * L_3 = Transform_FindChild_m610(L_2, (String_t*) &_stringLiteral51, /*hidden argument*/&Transform_FindChild_m610_MethodInfo);
		V_1 = L_3;
		bool L_4 = Object_op_Inequality_m335(NULL /*static, unused*/, V_1, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		NullCheck(V_1);
		GameObject_t2 * L_5 = Component_get_gameObject_m322(V_1, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		Object_Destroy_m608(NULL /*static, unused*/, L_5, /*hidden argument*/&Object_Destroy_m608_MethodInfo);
	}

IL_003f:
	{
		return;
	}
}
// Metadata Definition Vuforia.TurnOffWordBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffWordBehaviour::.ctor()
MethodInfo TurnOffWordBehaviour__ctor_m174_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TurnOffWordBehaviour__ctor_m174/* method */
	, &TurnOffWordBehaviour_t80_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 175/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffWordBehaviour::Awake()
MethodInfo TurnOffWordBehaviour_Awake_m175_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&TurnOffWordBehaviour_Awake_m175/* method */
	, &TurnOffWordBehaviour_t80_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 176/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TurnOffWordBehaviour_t80_MethodInfos[] =
{
	&TurnOffWordBehaviour__ctor_m174_MethodInfo,
	&TurnOffWordBehaviour_Awake_m175_MethodInfo,
	NULL
};
static MethodInfo* TurnOffWordBehaviour_t80_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType TurnOffWordBehaviour_t80_0_0_0;
extern Il2CppType TurnOffWordBehaviour_t80_1_0_0;
struct TurnOffWordBehaviour_t80;
TypeInfo TurnOffWordBehaviour_t80_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "TurnOffWordBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TurnOffWordBehaviour_t80_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TurnOffWordBehaviour_t80_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TurnOffWordBehaviour_t80_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TurnOffWordBehaviour_t80_il2cpp_TypeInfo/* cast_class */
	, &TurnOffWordBehaviour_t80_0_0_0/* byval_arg */
	, &TurnOffWordBehaviour_t80_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TurnOffWordBehaviour_t80)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo;
// Vuforia.UserDefinedTargetBuildingBehaviour
#include "AssemblyU2DCSharp_Vuforia_UserDefinedTargetBuildingBehaviourMethodDeclarations.h"

// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBuMethodDeclarations.h"
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour__ctor_m611_MethodInfo;


// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
extern MethodInfo UserDefinedTargetBuildingBehaviour__ctor_m176_MethodInfo;
 void UserDefinedTargetBuildingBehaviour__ctor_m176 (UserDefinedTargetBuildingBehaviour_t81 * __this, MethodInfo* method){
	{
		UserDefinedTargetBuildingAbstractBehaviour__ctor_m611(__this, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour__ctor_m611_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.UserDefinedTargetBuildingBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
MethodInfo UserDefinedTargetBuildingBehaviour__ctor_m176_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingBehaviour__ctor_m176/* method */
	, &UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 177/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UserDefinedTargetBuildingBehaviour_t81_MethodInfos[] =
{
	&UserDefinedTargetBuildingBehaviour__ctor_m176_MethodInfo,
	NULL
};
static MethodInfo* UserDefinedTargetBuildingBehaviour_t81_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_0_0_0;
extern Il2CppType UserDefinedTargetBuildingBehaviour_t81_1_0_0;
extern TypeInfo UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo;
struct UserDefinedTargetBuildingBehaviour_t81;
TypeInfo UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserDefinedTargetBuildingBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, UserDefinedTargetBuildingBehaviour_t81_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UserDefinedTargetBuildingBehaviour_t81_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo/* cast_class */
	, &UserDefinedTargetBuildingBehaviour_t81_0_0_0/* byval_arg */
	, &UserDefinedTargetBuildingBehaviour_t81_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserDefinedTargetBuildingBehaviour_t81)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo VideoBackgroundBehaviour_t83_il2cpp_TypeInfo;
// Vuforia.VideoBackgroundBehaviour
#include "AssemblyU2DCSharp_Vuforia_VideoBackgroundBehaviourMethodDeclarations.h"

// Vuforia.VideoBackgroundAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbstMethodDeclarations.h"
extern MethodInfo VideoBackgroundAbstractBehaviour__ctor_m612_MethodInfo;


// System.Void Vuforia.VideoBackgroundBehaviour::.ctor()
extern MethodInfo VideoBackgroundBehaviour__ctor_m177_MethodInfo;
 void VideoBackgroundBehaviour__ctor_m177 (VideoBackgroundBehaviour_t83 * __this, MethodInfo* method){
	{
		VideoBackgroundAbstractBehaviour__ctor_m612(__this, /*hidden argument*/&VideoBackgroundAbstractBehaviour__ctor_m612_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.VideoBackgroundBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundBehaviour::.ctor()
MethodInfo VideoBackgroundBehaviour__ctor_m177_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VideoBackgroundBehaviour__ctor_m177/* method */
	, &VideoBackgroundBehaviour_t83_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 178/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* VideoBackgroundBehaviour_t83_MethodInfos[] =
{
	&VideoBackgroundBehaviour__ctor_m177_MethodInfo,
	NULL
};
static MethodInfo* VideoBackgroundBehaviour_t83_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern TypeInfo Camera_t3_il2cpp_TypeInfo;
void VideoBackgroundBehaviour_t83_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t155 * tmp;
		tmp = (RequireComponent_t155 *)il2cpp_codegen_object_new (&RequireComponent_t155_il2cpp_TypeInfo);
		RequireComponent__ctor_m474(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&Camera_t3_il2cpp_TypeInfo)), &RequireComponent__ctor_m474_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache VideoBackgroundBehaviour_t83__CustomAttributeCache = {
1,
NULL,
&VideoBackgroundBehaviour_t83_CustomAttributesCacheGenerator
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType VideoBackgroundBehaviour_t83_0_0_0;
extern Il2CppType VideoBackgroundBehaviour_t83_1_0_0;
extern TypeInfo VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo;
struct VideoBackgroundBehaviour_t83;
extern CustomAttributesCache VideoBackgroundBehaviour_t83__CustomAttributeCache;
TypeInfo VideoBackgroundBehaviour_t83_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "VideoBackgroundBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VideoBackgroundBehaviour_t83_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &VideoBackgroundBehaviour_t83_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, VideoBackgroundBehaviour_t83_VTable/* vtable */
	, &VideoBackgroundBehaviour_t83__CustomAttributeCache/* custom_attributes_cache */
	, &VideoBackgroundBehaviour_t83_il2cpp_TypeInfo/* cast_class */
	, &VideoBackgroundBehaviour_t83_0_0_0/* byval_arg */
	, &VideoBackgroundBehaviour_t83_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VideoBackgroundBehaviour_t83)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRenderer.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo VideoTextureRenderer_t85_il2cpp_TypeInfo;
// Vuforia.VideoTextureRenderer
#include "AssemblyU2DCSharp_Vuforia_VideoTextureRendererMethodDeclarations.h"

// Vuforia.VideoTextureRendererAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendereMethodDeclarations.h"
extern MethodInfo VideoTextureRendererAbstractBehaviour__ctor_m613_MethodInfo;


// System.Void Vuforia.VideoTextureRenderer::.ctor()
extern MethodInfo VideoTextureRenderer__ctor_m178_MethodInfo;
 void VideoTextureRenderer__ctor_m178 (VideoTextureRenderer_t85 * __this, MethodInfo* method){
	{
		VideoTextureRendererAbstractBehaviour__ctor_m613(__this, /*hidden argument*/&VideoTextureRendererAbstractBehaviour__ctor_m613_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.VideoTextureRenderer
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRenderer::.ctor()
MethodInfo VideoTextureRenderer__ctor_m178_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VideoTextureRenderer__ctor_m178/* method */
	, &VideoTextureRenderer_t85_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 179/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* VideoTextureRenderer_t85_MethodInfos[] =
{
	&VideoTextureRenderer__ctor_m178_MethodInfo,
	NULL
};
extern MethodInfo VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m614_MethodInfo;
static MethodInfo* VideoTextureRenderer_t85_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m614_MethodInfo,
};
static Il2CppInterfaceOffsetPair VideoTextureRenderer_t85_InterfacesOffsets[] = 
{
	{ &IVideoBackgroundEventHandler_t122_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType VideoTextureRenderer_t85_0_0_0;
extern Il2CppType VideoTextureRenderer_t85_1_0_0;
extern TypeInfo VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo;
struct VideoTextureRenderer_t85;
TypeInfo VideoTextureRenderer_t85_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "VideoTextureRenderer"/* name */
	, "Vuforia"/* namespaze */
	, VideoTextureRenderer_t85_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &VideoTextureRenderer_t85_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, VideoTextureRenderer_t85_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &VideoTextureRenderer_t85_il2cpp_TypeInfo/* cast_class */
	, &VideoTextureRenderer_t85_0_0_0/* byval_arg */
	, &VideoTextureRenderer_t85_1_0_0/* this_arg */
	, VideoTextureRenderer_t85_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VideoTextureRenderer_t85)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo VirtualButtonBehaviour_t87_il2cpp_TypeInfo;
// Vuforia.VirtualButtonBehaviour
#include "AssemblyU2DCSharp_Vuforia_VirtualButtonBehaviourMethodDeclarations.h"

// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstraMethodDeclarations.h"
extern MethodInfo VirtualButtonAbstractBehaviour__ctor_m615_MethodInfo;


// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
extern MethodInfo VirtualButtonBehaviour__ctor_m179_MethodInfo;
 void VirtualButtonBehaviour__ctor_m179 (VirtualButtonBehaviour_t87 * __this, MethodInfo* method){
	{
		VirtualButtonAbstractBehaviour__ctor_m615(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour__ctor_m615_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.VirtualButtonBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
MethodInfo VirtualButtonBehaviour__ctor_m179_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VirtualButtonBehaviour__ctor_m179/* method */
	, &VirtualButtonBehaviour_t87_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 180/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* VirtualButtonBehaviour_t87_MethodInfos[] =
{
	&VirtualButtonBehaviour__ctor_m179_MethodInfo,
	NULL
};
extern MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButtonName_m616_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m617_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m618_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m619_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m620_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m621_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m622_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m623_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m624_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m625_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m626_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m627_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m628_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_UpdatePose_m629_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m630_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m631_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m632_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m633_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m634_MethodInfo;
static MethodInfo* VirtualButtonBehaviour_t87_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_VirtualButtonName_m616_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m617_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m618_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m619_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m620_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m621_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m622_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m623_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m624_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m625_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m626_MethodInfo,
	&VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m627_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m628_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdatePose_m629_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m630_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m631_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m632_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m633_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m634_MethodInfo,
};
extern TypeInfo IEditorVirtualButtonBehaviour_t170_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair VirtualButtonBehaviour_t87_InterfacesOffsets[] = 
{
	{ &IEditorVirtualButtonBehaviour_t170_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType VirtualButtonBehaviour_t87_0_0_0;
extern Il2CppType VirtualButtonBehaviour_t87_1_0_0;
extern TypeInfo VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo;
struct VirtualButtonBehaviour_t87;
TypeInfo VirtualButtonBehaviour_t87_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "VirtualButtonBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VirtualButtonBehaviour_t87_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &VirtualButtonBehaviour_t87_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, VirtualButtonBehaviour_t87_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &VirtualButtonBehaviour_t87_il2cpp_TypeInfo/* cast_class */
	, &VirtualButtonBehaviour_t87_0_0_0/* byval_arg */
	, &VirtualButtonBehaviour_t87_1_0_0/* this_arg */
	, VirtualButtonBehaviour_t87_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VirtualButtonBehaviour_t87)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WebCamBehaviour_t88_il2cpp_TypeInfo;
// Vuforia.WebCamBehaviour
#include "AssemblyU2DCSharp_Vuforia_WebCamBehaviourMethodDeclarations.h"

// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehavMethodDeclarations.h"
extern MethodInfo WebCamAbstractBehaviour__ctor_m635_MethodInfo;


// System.Void Vuforia.WebCamBehaviour::.ctor()
extern MethodInfo WebCamBehaviour__ctor_m180_MethodInfo;
 void WebCamBehaviour__ctor_m180 (WebCamBehaviour_t88 * __this, MethodInfo* method){
	{
		WebCamAbstractBehaviour__ctor_m635(__this, /*hidden argument*/&WebCamAbstractBehaviour__ctor_m635_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.WebCamBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamBehaviour::.ctor()
MethodInfo WebCamBehaviour__ctor_m180_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WebCamBehaviour__ctor_m180/* method */
	, &WebCamBehaviour_t88_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 181/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* WebCamBehaviour_t88_MethodInfos[] =
{
	&WebCamBehaviour__ctor_m180_MethodInfo,
	NULL
};
static MethodInfo* WebCamBehaviour_t88_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType WebCamBehaviour_t88_0_0_0;
extern Il2CppType WebCamBehaviour_t88_1_0_0;
extern TypeInfo WebCamAbstractBehaviour_t89_il2cpp_TypeInfo;
struct WebCamBehaviour_t88;
TypeInfo WebCamBehaviour_t88_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebCamBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WebCamBehaviour_t88_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WebCamBehaviour_t88_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, WebCamBehaviour_t88_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WebCamBehaviour_t88_il2cpp_TypeInfo/* cast_class */
	, &WebCamBehaviour_t88_0_0_0/* byval_arg */
	, &WebCamBehaviour_t88_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebCamBehaviour_t88)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WireframeBehaviour_t90_il2cpp_TypeInfo;
// Vuforia.WireframeBehaviour
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviourMethodDeclarations.h"

// UnityEngine.HideFlags
#include "UnityEngine_UnityEngine_HideFlags.h"
// UnityEngine.Shader
#include "UnityEngine_UnityEngine_Shader.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// Vuforia.QCARManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManager.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
extern TypeInfo Single_t105_il2cpp_TypeInfo;
extern TypeInfo QCARManager_t171_il2cpp_TypeInfo;
extern TypeInfo Transform_t10_il2cpp_TypeInfo;
// Vuforia.QCARManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerMethodDeclarations.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
// UnityEngine.Gizmos
#include "UnityEngine_UnityEngine_GizmosMethodDeclarations.h"
extern MethodInfo Color_get_green_m636_MethodInfo;
extern MethodInfo Material__ctor_m637_MethodInfo;
extern MethodInfo Object_set_hideFlags_m638_MethodInfo;
extern MethodInfo Material_get_shader_m639_MethodInfo;
extern MethodInfo QCARManager_get_Instance_m640_MethodInfo;
extern MethodInfo QCARManager_get_ARCameraTransform_m641_MethodInfo;
extern MethodInfo GameObject_GetComponentsInChildren_TisCamera_t3_m642_MethodInfo;
extern MethodInfo Camera_get_current_m643_MethodInfo;
extern MethodInfo WireframeBehaviour_CreateLineMaterial_m182_MethodInfo;
extern MethodInfo MeshFilter_get_sharedMesh_m644_MethodInfo;
extern MethodInfo Mesh_get_vertices_m645_MethodInfo;
extern MethodInfo Mesh_get_triangles_m646_MethodInfo;
extern MethodInfo GL_PushMatrix_m647_MethodInfo;
extern MethodInfo Transform_get_localToWorldMatrix_m648_MethodInfo;
extern MethodInfo GL_MultMatrix_m649_MethodInfo;
extern MethodInfo Material_SetPass_m650_MethodInfo;
extern MethodInfo GL_Begin_m651_MethodInfo;
extern MethodInfo GL_Vertex_m652_MethodInfo;
extern MethodInfo GL_End_m653_MethodInfo;
extern MethodInfo GL_PopMatrix_m654_MethodInfo;
extern MethodInfo Behaviour_get_enabled_m655_MethodInfo;
extern MethodInfo Transform_get_lossyScale_m656_MethodInfo;
extern MethodInfo Matrix4x4_TRS_m657_MethodInfo;
extern MethodInfo Gizmos_set_matrix_m658_MethodInfo;
extern MethodInfo Gizmos_set_color_m659_MethodInfo;
extern MethodInfo Gizmos_DrawLine_m660_MethodInfo;
struct GameObject_t2;
struct GameObject_t2;
// Declaration !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
 ObjectU5BU5D_t115* GameObject_GetComponentsInChildren_TisObject_t_m661_gshared (GameObject_t2 * __this, MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisObject_t_m661(__this, method) (ObjectU5BU5D_t115*)GameObject_GetComponentsInChildren_TisObject_t_m661_gshared((GameObject_t2 *)__this, method)
// Declaration !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UnityEngine.Camera>()
#define GameObject_GetComponentsInChildren_TisCamera_t3_m642(__this, method) (CameraU5BU5D_t172*)GameObject_GetComponentsInChildren_TisObject_t_m661_gshared((GameObject_t2 *)__this, method)


// System.Void Vuforia.WireframeBehaviour::.ctor()
extern MethodInfo WireframeBehaviour__ctor_m181_MethodInfo;
 void WireframeBehaviour__ctor_m181 (WireframeBehaviour_t90 * __this, MethodInfo* method){
	{
		__this->___ShowLines_3 = 1;
		Color_t19  L_0 = Color_get_green_m636(NULL /*static, unused*/, /*hidden argument*/&Color_get_green_m636_MethodInfo);
		__this->___LineColor_4 = L_0;
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::CreateLineMaterial()
 void WireframeBehaviour_CreateLineMaterial_m182 (WireframeBehaviour_t90 * __this, MethodInfo* method){
	{
		ObjectU5BU5D_t115* L_0 = ((ObjectU5BU5D_t115*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t115_il2cpp_TypeInfo), ((int32_t)9)));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, (String_t*) &_stringLiteral52);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)(String_t*) &_stringLiteral52;
		ObjectU5BU5D_t115* L_1 = L_0;
		Color_t19 * L_2 = &(__this->___LineColor_4);
		NullCheck(L_2);
		float L_3 = (L_2->___r_0);
		float L_4 = L_3;
		Object_t * L_5 = Box(InitializedTypeInfo(&Single_t105_il2cpp_TypeInfo), &L_4);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 1)) = (Object_t *)L_5;
		ObjectU5BU5D_t115* L_6 = L_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, (String_t*) &_stringLiteral8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2)) = (Object_t *)(String_t*) &_stringLiteral8;
		ObjectU5BU5D_t115* L_7 = L_6;
		Color_t19 * L_8 = &(__this->___LineColor_4);
		NullCheck(L_8);
		float L_9 = (L_8->___g_1);
		float L_10 = L_9;
		Object_t * L_11 = Box(InitializedTypeInfo(&Single_t105_il2cpp_TypeInfo), &L_10);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 3);
		ArrayElementTypeCheck (L_7, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 3)) = (Object_t *)L_11;
		ObjectU5BU5D_t115* L_12 = L_7;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, (String_t*) &_stringLiteral8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 4)) = (Object_t *)(String_t*) &_stringLiteral8;
		ObjectU5BU5D_t115* L_13 = L_12;
		Color_t19 * L_14 = &(__this->___LineColor_4);
		NullCheck(L_14);
		float L_15 = (L_14->___b_2);
		float L_16 = L_15;
		Object_t * L_17 = Box(InitializedTypeInfo(&Single_t105_il2cpp_TypeInfo), &L_16);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 5);
		ArrayElementTypeCheck (L_13, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 5)) = (Object_t *)L_17;
		ObjectU5BU5D_t115* L_18 = L_13;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 6);
		ArrayElementTypeCheck (L_18, (String_t*) &_stringLiteral8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 6)) = (Object_t *)(String_t*) &_stringLiteral8;
		ObjectU5BU5D_t115* L_19 = L_18;
		Color_t19 * L_20 = &(__this->___LineColor_4);
		NullCheck(L_20);
		float L_21 = (L_20->___a_3);
		float L_22 = L_21;
		Object_t * L_23 = Box(InitializedTypeInfo(&Single_t105_il2cpp_TypeInfo), &L_22);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		ArrayElementTypeCheck (L_19, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 7)) = (Object_t *)L_23;
		ObjectU5BU5D_t115* L_24 = L_19;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 8);
		ArrayElementTypeCheck (L_24, (String_t*) &_stringLiteral53);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_24, 8)) = (Object_t *)(String_t*) &_stringLiteral53;
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_25 = String_Concat_m338(NULL /*static, unused*/, L_24, /*hidden argument*/&String_Concat_m338_MethodInfo);
		Material_t4 * L_26 = (Material_t4 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Material_t4_il2cpp_TypeInfo));
		Material__ctor_m637(L_26, L_25, /*hidden argument*/&Material__ctor_m637_MethodInfo);
		__this->___mLineMaterial_2 = L_26;
		Material_t4 * L_27 = (__this->___mLineMaterial_2);
		NullCheck(L_27);
		Object_set_hideFlags_m638(L_27, ((int32_t)61), /*hidden argument*/&Object_set_hideFlags_m638_MethodInfo);
		Material_t4 * L_28 = (__this->___mLineMaterial_2);
		NullCheck(L_28);
		Shader_t173 * L_29 = Material_get_shader_m639(L_28, /*hidden argument*/&Material_get_shader_m639_MethodInfo);
		NullCheck(L_29);
		Object_set_hideFlags_m638(L_29, ((int32_t)61), /*hidden argument*/&Object_set_hideFlags_m638_MethodInfo);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
extern MethodInfo WireframeBehaviour_OnRenderObject_m183_MethodInfo;
 void WireframeBehaviour_OnRenderObject_m183 (WireframeBehaviour_t90 * __this, MethodInfo* method){
	GameObject_t2 * V_0 = {0};
	CameraU5BU5D_t172* V_1 = {0};
	bool V_2 = false;
	Camera_t3 * V_3 = {0};
	CameraU5BU5D_t172* V_4 = {0};
	int32_t V_5 = 0;
	MeshFilter_t169 * V_6 = {0};
	Mesh_t174 * V_7 = {0};
	Vector3U5BU5D_t175* V_8 = {0};
	Int32U5BU5D_t21* V_9 = {0};
	int32_t V_10 = 0;
	Vector3_t13  V_11 = {0};
	Vector3_t13  V_12 = {0};
	Vector3_t13  V_13 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARManager_t171_il2cpp_TypeInfo));
		QCARManager_t171 * L_0 = QCARManager_get_Instance_m640(NULL /*static, unused*/, /*hidden argument*/&QCARManager_get_Instance_m640_MethodInfo);
		NullCheck(L_0);
		Transform_t10 * L_1 = (Transform_t10 *)VirtFuncInvoker0< Transform_t10 * >::Invoke(&QCARManager_get_ARCameraTransform_m641_MethodInfo, L_0);
		NullCheck(L_1);
		GameObject_t2 * L_2 = Component_get_gameObject_m322(L_1, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		V_0 = L_2;
		NullCheck(V_0);
		CameraU5BU5D_t172* L_3 = GameObject_GetComponentsInChildren_TisCamera_t3_m642(V_0, /*hidden argument*/&GameObject_GetComponentsInChildren_TisCamera_t3_m642_MethodInfo);
		V_1 = L_3;
		V_2 = 0;
		V_4 = V_1;
		V_5 = 0;
		goto IL_0042;
	}

IL_0024:
	{
		NullCheck(V_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_4, V_5);
		int32_t L_4 = V_5;
		V_3 = (*(Camera_t3 **)(Camera_t3 **)SZArrayLdElema(V_4, L_4));
		Camera_t3 * L_5 = Camera_get_current_m643(NULL /*static, unused*/, /*hidden argument*/&Camera_get_current_m643_MethodInfo);
		bool L_6 = Object_op_Equality_m324(NULL /*static, unused*/, L_5, V_3, /*hidden argument*/&Object_op_Equality_m324_MethodInfo);
		if (!L_6)
		{
			goto IL_003c;
		}
	}
	{
		V_2 = 1;
	}

IL_003c:
	{
		V_5 = ((int32_t)(V_5+1));
	}

IL_0042:
	{
		NullCheck(V_4);
		if ((((int32_t)V_5) < ((int32_t)(((int32_t)(((Array_t *)V_4)->max_length))))))
		{
			goto IL_0024;
		}
	}
	{
		if (V_2)
		{
			goto IL_0054;
		}
	}
	{
		return;
	}

IL_0054:
	{
		bool L_7 = (__this->___ShowLines_3);
		if (L_7)
		{
			goto IL_0060;
		}
	}
	{
		return;
	}

IL_0060:
	{
		MeshFilter_t169 * L_8 = Component_GetComponent_TisMeshFilter_t169_m609(__this, /*hidden argument*/&Component_GetComponent_TisMeshFilter_t169_m609_MethodInfo);
		V_6 = L_8;
		bool L_9 = Object_op_Implicit_m397(NULL /*static, unused*/, V_6, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (L_9)
		{
			goto IL_0075;
		}
	}
	{
		return;
	}

IL_0075:
	{
		Material_t4 * L_10 = (__this->___mLineMaterial_2);
		bool L_11 = Object_op_Equality_m324(NULL /*static, unused*/, L_10, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Equality_m324_MethodInfo);
		if (!L_11)
		{
			goto IL_008c;
		}
	}
	{
		WireframeBehaviour_CreateLineMaterial_m182(__this, /*hidden argument*/&WireframeBehaviour_CreateLineMaterial_m182_MethodInfo);
	}

IL_008c:
	{
		NullCheck(V_6);
		Mesh_t174 * L_12 = MeshFilter_get_sharedMesh_m644(V_6, /*hidden argument*/&MeshFilter_get_sharedMesh_m644_MethodInfo);
		V_7 = L_12;
		NullCheck(V_7);
		Vector3U5BU5D_t175* L_13 = Mesh_get_vertices_m645(V_7, /*hidden argument*/&Mesh_get_vertices_m645_MethodInfo);
		V_8 = L_13;
		NullCheck(V_7);
		Int32U5BU5D_t21* L_14 = Mesh_get_triangles_m646(V_7, /*hidden argument*/&Mesh_get_triangles_m646_MethodInfo);
		V_9 = L_14;
		GL_PushMatrix_m647(NULL /*static, unused*/, /*hidden argument*/&GL_PushMatrix_m647_MethodInfo);
		Transform_t10 * L_15 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_15);
		Matrix4x4_t176  L_16 = Transform_get_localToWorldMatrix_m648(L_15, /*hidden argument*/&Transform_get_localToWorldMatrix_m648_MethodInfo);
		GL_MultMatrix_m649(NULL /*static, unused*/, L_16, /*hidden argument*/&GL_MultMatrix_m649_MethodInfo);
		Material_t4 * L_17 = (__this->___mLineMaterial_2);
		NullCheck(L_17);
		Material_SetPass_m650(L_17, 0, /*hidden argument*/&Material_SetPass_m650_MethodInfo);
		GL_Begin_m651(NULL /*static, unused*/, 1, /*hidden argument*/&GL_Begin_m651_MethodInfo);
		V_10 = 0;
		goto IL_0144;
	}

IL_00d7:
	{
		NullCheck(V_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_9, V_10);
		int32_t L_18 = V_10;
		NullCheck(V_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_8, (*(int32_t*)(int32_t*)SZArrayLdElema(V_9, L_18)));
		V_11 = (*(Vector3_t13 *)((Vector3_t13 *)(Vector3_t13 *)SZArrayLdElema(V_8, (*(int32_t*)(int32_t*)SZArrayLdElema(V_9, L_18)))));
		NullCheck(V_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_9, ((int32_t)(V_10+1)));
		int32_t L_19 = ((int32_t)(V_10+1));
		NullCheck(V_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_8, (*(int32_t*)(int32_t*)SZArrayLdElema(V_9, L_19)));
		V_12 = (*(Vector3_t13 *)((Vector3_t13 *)(Vector3_t13 *)SZArrayLdElema(V_8, (*(int32_t*)(int32_t*)SZArrayLdElema(V_9, L_19)))));
		NullCheck(V_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_9, ((int32_t)(V_10+2)));
		int32_t L_20 = ((int32_t)(V_10+2));
		NullCheck(V_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_8, (*(int32_t*)(int32_t*)SZArrayLdElema(V_9, L_20)));
		V_13 = (*(Vector3_t13 *)((Vector3_t13 *)(Vector3_t13 *)SZArrayLdElema(V_8, (*(int32_t*)(int32_t*)SZArrayLdElema(V_9, L_20)))));
		GL_Vertex_m652(NULL /*static, unused*/, V_11, /*hidden argument*/&GL_Vertex_m652_MethodInfo);
		GL_Vertex_m652(NULL /*static, unused*/, V_12, /*hidden argument*/&GL_Vertex_m652_MethodInfo);
		GL_Vertex_m652(NULL /*static, unused*/, V_12, /*hidden argument*/&GL_Vertex_m652_MethodInfo);
		GL_Vertex_m652(NULL /*static, unused*/, V_13, /*hidden argument*/&GL_Vertex_m652_MethodInfo);
		GL_Vertex_m652(NULL /*static, unused*/, V_13, /*hidden argument*/&GL_Vertex_m652_MethodInfo);
		GL_Vertex_m652(NULL /*static, unused*/, V_11, /*hidden argument*/&GL_Vertex_m652_MethodInfo);
		V_10 = ((int32_t)(V_10+3));
	}

IL_0144:
	{
		NullCheck(V_9);
		if ((((int32_t)V_10) < ((int32_t)(((int32_t)(((Array_t *)V_9)->max_length))))))
		{
			goto IL_00d7;
		}
	}
	{
		GL_End_m653(NULL /*static, unused*/, /*hidden argument*/&GL_End_m653_MethodInfo);
		GL_PopMatrix_m654(NULL /*static, unused*/, /*hidden argument*/&GL_PopMatrix_m654_MethodInfo);
		return;
	}
}
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
extern MethodInfo WireframeBehaviour_OnDrawGizmos_m184_MethodInfo;
 void WireframeBehaviour_OnDrawGizmos_m184 (WireframeBehaviour_t90 * __this, MethodInfo* method){
	MeshFilter_t169 * V_0 = {0};
	Mesh_t174 * V_1 = {0};
	Vector3U5BU5D_t175* V_2 = {0};
	Int32U5BU5D_t21* V_3 = {0};
	int32_t V_4 = 0;
	Vector3_t13  V_5 = {0};
	Vector3_t13  V_6 = {0};
	Vector3_t13  V_7 = {0};
	{
		bool L_0 = (__this->___ShowLines_3);
		if (!L_0)
		{
			goto IL_00ed;
		}
	}
	{
		bool L_1 = Behaviour_get_enabled_m655(__this, /*hidden argument*/&Behaviour_get_enabled_m655_MethodInfo);
		if (!L_1)
		{
			goto IL_00ed;
		}
	}
	{
		MeshFilter_t169 * L_2 = Component_GetComponent_TisMeshFilter_t169_m609(__this, /*hidden argument*/&Component_GetComponent_TisMeshFilter_t169_m609_MethodInfo);
		V_0 = L_2;
		bool L_3 = Object_op_Implicit_m397(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t2 * L_4 = Component_get_gameObject_m322(__this, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		NullCheck(L_4);
		Transform_t10 * L_5 = GameObject_get_transform_m313(L_4, /*hidden argument*/&GameObject_get_transform_m313_MethodInfo);
		NullCheck(L_5);
		Vector3_t13  L_6 = Transform_get_position_m299(L_5, /*hidden argument*/&Transform_get_position_m299_MethodInfo);
		GameObject_t2 * L_7 = Component_get_gameObject_m322(__this, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		NullCheck(L_7);
		Transform_t10 * L_8 = GameObject_get_transform_m313(L_7, /*hidden argument*/&GameObject_get_transform_m313_MethodInfo);
		NullCheck(L_8);
		Quaternion_t12  L_9 = Transform_get_rotation_m301(L_8, /*hidden argument*/&Transform_get_rotation_m301_MethodInfo);
		GameObject_t2 * L_10 = Component_get_gameObject_m322(__this, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		NullCheck(L_10);
		Transform_t10 * L_11 = GameObject_get_transform_m313(L_10, /*hidden argument*/&GameObject_get_transform_m313_MethodInfo);
		NullCheck(L_11);
		Vector3_t13  L_12 = Transform_get_lossyScale_m656(L_11, /*hidden argument*/&Transform_get_lossyScale_m656_MethodInfo);
		Matrix4x4_t176  L_13 = Matrix4x4_TRS_m657(NULL /*static, unused*/, L_6, L_9, L_12, /*hidden argument*/&Matrix4x4_TRS_m657_MethodInfo);
		Gizmos_set_matrix_m658(NULL /*static, unused*/, L_13, /*hidden argument*/&Gizmos_set_matrix_m658_MethodInfo);
		Color_t19  L_14 = (__this->___LineColor_4);
		Gizmos_set_color_m659(NULL /*static, unused*/, L_14, /*hidden argument*/&Gizmos_set_color_m659_MethodInfo);
		NullCheck(V_0);
		Mesh_t174 * L_15 = MeshFilter_get_sharedMesh_m644(V_0, /*hidden argument*/&MeshFilter_get_sharedMesh_m644_MethodInfo);
		V_1 = L_15;
		NullCheck(V_1);
		Vector3U5BU5D_t175* L_16 = Mesh_get_vertices_m645(V_1, /*hidden argument*/&Mesh_get_vertices_m645_MethodInfo);
		V_2 = L_16;
		NullCheck(V_1);
		Int32U5BU5D_t21* L_17 = Mesh_get_triangles_m646(V_1, /*hidden argument*/&Mesh_get_triangles_m646_MethodInfo);
		V_3 = L_17;
		V_4 = 0;
		goto IL_00e3;
	}

IL_008b:
	{
		NullCheck(V_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_3, V_4);
		int32_t L_18 = V_4;
		NullCheck(V_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_2, (*(int32_t*)(int32_t*)SZArrayLdElema(V_3, L_18)));
		V_5 = (*(Vector3_t13 *)((Vector3_t13 *)(Vector3_t13 *)SZArrayLdElema(V_2, (*(int32_t*)(int32_t*)SZArrayLdElema(V_3, L_18)))));
		NullCheck(V_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_3, ((int32_t)(V_4+1)));
		int32_t L_19 = ((int32_t)(V_4+1));
		NullCheck(V_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_2, (*(int32_t*)(int32_t*)SZArrayLdElema(V_3, L_19)));
		V_6 = (*(Vector3_t13 *)((Vector3_t13 *)(Vector3_t13 *)SZArrayLdElema(V_2, (*(int32_t*)(int32_t*)SZArrayLdElema(V_3, L_19)))));
		NullCheck(V_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_3, ((int32_t)(V_4+2)));
		int32_t L_20 = ((int32_t)(V_4+2));
		NullCheck(V_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_2, (*(int32_t*)(int32_t*)SZArrayLdElema(V_3, L_20)));
		V_7 = (*(Vector3_t13 *)((Vector3_t13 *)(Vector3_t13 *)SZArrayLdElema(V_2, (*(int32_t*)(int32_t*)SZArrayLdElema(V_3, L_20)))));
		Gizmos_DrawLine_m660(NULL /*static, unused*/, V_5, V_6, /*hidden argument*/&Gizmos_DrawLine_m660_MethodInfo);
		Gizmos_DrawLine_m660(NULL /*static, unused*/, V_6, V_7, /*hidden argument*/&Gizmos_DrawLine_m660_MethodInfo);
		Gizmos_DrawLine_m660(NULL /*static, unused*/, V_7, V_5, /*hidden argument*/&Gizmos_DrawLine_m660_MethodInfo);
		V_4 = ((int32_t)(V_4+3));
	}

IL_00e3:
	{
		NullCheck(V_3);
		if ((((int32_t)V_4) < ((int32_t)(((int32_t)(((Array_t *)V_3)->max_length))))))
		{
			goto IL_008b;
		}
	}

IL_00ed:
	{
		return;
	}
}
// Metadata Definition Vuforia.WireframeBehaviour
extern Il2CppType Material_t4_0_0_1;
FieldInfo WireframeBehaviour_t90____mLineMaterial_2_FieldInfo = 
{
	"mLineMaterial"/* name */
	, &Material_t4_0_0_1/* type */
	, &WireframeBehaviour_t90_il2cpp_TypeInfo/* parent */
	, offsetof(WireframeBehaviour_t90, ___mLineMaterial_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_6;
FieldInfo WireframeBehaviour_t90____ShowLines_3_FieldInfo = 
{
	"ShowLines"/* name */
	, &Boolean_t106_0_0_6/* type */
	, &WireframeBehaviour_t90_il2cpp_TypeInfo/* parent */
	, offsetof(WireframeBehaviour_t90, ___ShowLines_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Color_t19_0_0_6;
FieldInfo WireframeBehaviour_t90____LineColor_4_FieldInfo = 
{
	"LineColor"/* name */
	, &Color_t19_0_0_6/* type */
	, &WireframeBehaviour_t90_il2cpp_TypeInfo/* parent */
	, offsetof(WireframeBehaviour_t90, ___LineColor_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* WireframeBehaviour_t90_FieldInfos[] =
{
	&WireframeBehaviour_t90____mLineMaterial_2_FieldInfo,
	&WireframeBehaviour_t90____ShowLines_3_FieldInfo,
	&WireframeBehaviour_t90____LineColor_4_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeBehaviour::.ctor()
MethodInfo WireframeBehaviour__ctor_m181_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WireframeBehaviour__ctor_m181/* method */
	, &WireframeBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 182/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeBehaviour::CreateLineMaterial()
MethodInfo WireframeBehaviour_CreateLineMaterial_m182_MethodInfo = 
{
	"CreateLineMaterial"/* name */
	, (methodPointerType)&WireframeBehaviour_CreateLineMaterial_m182/* method */
	, &WireframeBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 183/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
MethodInfo WireframeBehaviour_OnRenderObject_m183_MethodInfo = 
{
	"OnRenderObject"/* name */
	, (methodPointerType)&WireframeBehaviour_OnRenderObject_m183/* method */
	, &WireframeBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 184/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
MethodInfo WireframeBehaviour_OnDrawGizmos_m184_MethodInfo = 
{
	"OnDrawGizmos"/* name */
	, (methodPointerType)&WireframeBehaviour_OnDrawGizmos_m184/* method */
	, &WireframeBehaviour_t90_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 185/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* WireframeBehaviour_t90_MethodInfos[] =
{
	&WireframeBehaviour__ctor_m181_MethodInfo,
	&WireframeBehaviour_CreateLineMaterial_m182_MethodInfo,
	&WireframeBehaviour_OnRenderObject_m183_MethodInfo,
	&WireframeBehaviour_OnDrawGizmos_m184_MethodInfo,
	NULL
};
static MethodInfo* WireframeBehaviour_t90_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType WireframeBehaviour_t90_0_0_0;
extern Il2CppType WireframeBehaviour_t90_1_0_0;
struct WireframeBehaviour_t90;
TypeInfo WireframeBehaviour_t90_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WireframeBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WireframeBehaviour_t90_MethodInfos/* methods */
	, NULL/* properties */
	, WireframeBehaviour_t90_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WireframeBehaviour_t90_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, WireframeBehaviour_t90_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WireframeBehaviour_t90_il2cpp_TypeInfo/* cast_class */
	, &WireframeBehaviour_t90_0_0_0/* byval_arg */
	, &WireframeBehaviour_t90_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WireframeBehaviour_t90)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandler.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WireframeTrackableEventHandler_t91_il2cpp_TypeInfo;
// Vuforia.WireframeTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_WireframeTrackableEventHandlerMethodDeclarations.h"

#include "Assembly-CSharp_ArrayTypes.h"
extern MethodInfo WireframeTrackableEventHandler_OnTrackingFound_m188_MethodInfo;
extern MethodInfo WireframeTrackableEventHandler_OnTrackingLost_m189_MethodInfo;
extern MethodInfo Component_GetComponentsInChildren_TisWireframeBehaviour_t90_m662_MethodInfo;
struct Component_t100;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
// !!0[] UnityEngine.Component::GetComponentsInChildren<Vuforia.WireframeBehaviour>(System.Boolean)
#define Component_GetComponentsInChildren_TisWireframeBehaviour_t90_m662(__this, p0, method) (WireframeBehaviourU5BU5D_t177*)Component_GetComponentsInChildren_TisObject_t_m311_gshared((Component_t100 *)__this, (bool)p0, method)


// System.Void Vuforia.WireframeTrackableEventHandler::.ctor()
extern MethodInfo WireframeTrackableEventHandler__ctor_m185_MethodInfo;
 void WireframeTrackableEventHandler__ctor_m185 (WireframeTrackableEventHandler_t91 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::Start()
extern MethodInfo WireframeTrackableEventHandler_Start_m186_MethodInfo;
 void WireframeTrackableEventHandler_Start_m186 (WireframeTrackableEventHandler_t91 * __this, MethodInfo* method){
	{
		TrackableBehaviour_t44 * L_0 = Component_GetComponent_TisTrackableBehaviour_t44_m413(__this, /*hidden argument*/&Component_GetComponent_TisTrackableBehaviour_t44_m413_MethodInfo);
		__this->___mTrackableBehaviour_2 = L_0;
		TrackableBehaviour_t44 * L_1 = (__this->___mTrackableBehaviour_2);
		bool L_2 = Object_op_Implicit_m397(NULL /*static, unused*/, L_1, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		TrackableBehaviour_t44 * L_3 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_3);
		TrackableBehaviour_RegisterTrackableEventHandler_m414(L_3, __this, /*hidden argument*/&TrackableBehaviour_RegisterTrackableEventHandler_m414_MethodInfo);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
extern MethodInfo WireframeTrackableEventHandler_OnTrackableStateChanged_m187_MethodInfo;
 void WireframeTrackableEventHandler_OnTrackableStateChanged_m187 (WireframeTrackableEventHandler_t91 * __this, int32_t ___previousStatus, int32_t ___newStatus, MethodInfo* method){
	{
		if ((((int32_t)___newStatus) == ((int32_t)2)))
		{
			goto IL_000e;
		}
	}
	{
		if ((((uint32_t)___newStatus) != ((uint32_t)3)))
		{
			goto IL_0019;
		}
	}

IL_000e:
	{
		WireframeTrackableEventHandler_OnTrackingFound_m188(__this, /*hidden argument*/&WireframeTrackableEventHandler_OnTrackingFound_m188_MethodInfo);
		goto IL_001f;
	}

IL_0019:
	{
		WireframeTrackableEventHandler_OnTrackingLost_m189(__this, /*hidden argument*/&WireframeTrackableEventHandler_OnTrackingLost_m189_MethodInfo);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
 void WireframeTrackableEventHandler_OnTrackingFound_m188 (WireframeTrackableEventHandler_t91 * __this, MethodInfo* method){
	RendererU5BU5D_t116* V_0 = {0};
	ColliderU5BU5D_t133* V_1 = {0};
	WireframeBehaviourU5BU5D_t177* V_2 = {0};
	Renderer_t7 * V_3 = {0};
	RendererU5BU5D_t116* V_4 = {0};
	int32_t V_5 = 0;
	Collider_t132 * V_6 = {0};
	ColliderU5BU5D_t133* V_7 = {0};
	int32_t V_8 = 0;
	WireframeBehaviour_t90 * V_9 = {0};
	WireframeBehaviourU5BU5D_t177* V_10 = {0};
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t116* L_0 = Component_GetComponentsInChildren_TisRenderer_t7_m310(__this, 1, /*hidden argument*/&Component_GetComponentsInChildren_TisRenderer_t7_m310_MethodInfo);
		V_0 = L_0;
		ColliderU5BU5D_t133* L_1 = Component_GetComponentsInChildren_TisCollider_t132_m415(__this, 1, /*hidden argument*/&Component_GetComponentsInChildren_TisCollider_t132_m415_MethodInfo);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t177* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t90_m662(__this, 1, /*hidden argument*/&Component_GetComponentsInChildren_TisWireframeBehaviour_t90_m662_MethodInfo);
		V_2 = L_2;
		V_4 = V_0;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		NullCheck(V_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_4, V_5);
		int32_t L_3 = V_5;
		V_3 = (*(Renderer_t7 **)(Renderer_t7 **)SZArrayLdElema(V_4, L_3));
		NullCheck(V_3);
		Renderer_set_enabled_m416(V_3, 1, /*hidden argument*/&Renderer_set_enabled_m416_MethodInfo);
		V_5 = ((int32_t)(V_5+1));
	}

IL_0036:
	{
		NullCheck(V_4);
		if ((((int32_t)V_5) < ((int32_t)(((int32_t)(((Array_t *)V_4)->max_length))))))
		{
			goto IL_0023;
		}
	}
	{
		V_7 = V_1;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		NullCheck(V_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_7, V_8);
		int32_t L_4 = V_8;
		V_6 = (*(Collider_t132 **)(Collider_t132 **)SZArrayLdElema(V_7, L_4));
		NullCheck(V_6);
		Collider_set_enabled_m417(V_6, 1, /*hidden argument*/&Collider_set_enabled_m417_MethodInfo);
		V_8 = ((int32_t)(V_8+1));
	}

IL_0061:
	{
		NullCheck(V_7);
		if ((((int32_t)V_8) < ((int32_t)(((int32_t)(((Array_t *)V_7)->max_length))))))
		{
			goto IL_004c;
		}
	}
	{
		V_10 = V_2;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		NullCheck(V_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_10, V_11);
		int32_t L_5 = V_11;
		V_9 = (*(WireframeBehaviour_t90 **)(WireframeBehaviour_t90 **)SZArrayLdElema(V_10, L_5));
		NullCheck(V_9);
		Behaviour_set_enabled_m216(V_9, 1, /*hidden argument*/&Behaviour_set_enabled_m216_MethodInfo);
		V_11 = ((int32_t)(V_11+1));
	}

IL_008c:
	{
		NullCheck(V_10);
		if ((((int32_t)V_11) < ((int32_t)(((int32_t)(((Array_t *)V_10)->max_length))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t44 * L_6 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_6);
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&TrackableBehaviour_get_TrackableName_m345_MethodInfo, L_6);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_8 = String_Concat_m418(NULL /*static, unused*/, (String_t*) &_stringLiteral46, L_7, (String_t*) &_stringLiteral47, /*hidden argument*/&String_Concat_m418_MethodInfo);
		Debug_Log_m419(NULL /*static, unused*/, L_8, /*hidden argument*/&Debug_Log_m419_MethodInfo);
		return;
	}
}
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
 void WireframeTrackableEventHandler_OnTrackingLost_m189 (WireframeTrackableEventHandler_t91 * __this, MethodInfo* method){
	RendererU5BU5D_t116* V_0 = {0};
	ColliderU5BU5D_t133* V_1 = {0};
	WireframeBehaviourU5BU5D_t177* V_2 = {0};
	Renderer_t7 * V_3 = {0};
	RendererU5BU5D_t116* V_4 = {0};
	int32_t V_5 = 0;
	Collider_t132 * V_6 = {0};
	ColliderU5BU5D_t133* V_7 = {0};
	int32_t V_8 = 0;
	WireframeBehaviour_t90 * V_9 = {0};
	WireframeBehaviourU5BU5D_t177* V_10 = {0};
	int32_t V_11 = 0;
	{
		RendererU5BU5D_t116* L_0 = Component_GetComponentsInChildren_TisRenderer_t7_m310(__this, 1, /*hidden argument*/&Component_GetComponentsInChildren_TisRenderer_t7_m310_MethodInfo);
		V_0 = L_0;
		ColliderU5BU5D_t133* L_1 = Component_GetComponentsInChildren_TisCollider_t132_m415(__this, 1, /*hidden argument*/&Component_GetComponentsInChildren_TisCollider_t132_m415_MethodInfo);
		V_1 = L_1;
		WireframeBehaviourU5BU5D_t177* L_2 = Component_GetComponentsInChildren_TisWireframeBehaviour_t90_m662(__this, 1, /*hidden argument*/&Component_GetComponentsInChildren_TisWireframeBehaviour_t90_m662_MethodInfo);
		V_2 = L_2;
		V_4 = V_0;
		V_5 = 0;
		goto IL_0036;
	}

IL_0023:
	{
		NullCheck(V_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_4, V_5);
		int32_t L_3 = V_5;
		V_3 = (*(Renderer_t7 **)(Renderer_t7 **)SZArrayLdElema(V_4, L_3));
		NullCheck(V_3);
		Renderer_set_enabled_m416(V_3, 0, /*hidden argument*/&Renderer_set_enabled_m416_MethodInfo);
		V_5 = ((int32_t)(V_5+1));
	}

IL_0036:
	{
		NullCheck(V_4);
		if ((((int32_t)V_5) < ((int32_t)(((int32_t)(((Array_t *)V_4)->max_length))))))
		{
			goto IL_0023;
		}
	}
	{
		V_7 = V_1;
		V_8 = 0;
		goto IL_0061;
	}

IL_004c:
	{
		NullCheck(V_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_7, V_8);
		int32_t L_4 = V_8;
		V_6 = (*(Collider_t132 **)(Collider_t132 **)SZArrayLdElema(V_7, L_4));
		NullCheck(V_6);
		Collider_set_enabled_m417(V_6, 0, /*hidden argument*/&Collider_set_enabled_m417_MethodInfo);
		V_8 = ((int32_t)(V_8+1));
	}

IL_0061:
	{
		NullCheck(V_7);
		if ((((int32_t)V_8) < ((int32_t)(((int32_t)(((Array_t *)V_7)->max_length))))))
		{
			goto IL_004c;
		}
	}
	{
		V_10 = V_2;
		V_11 = 0;
		goto IL_008c;
	}

IL_0077:
	{
		NullCheck(V_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_10, V_11);
		int32_t L_5 = V_11;
		V_9 = (*(WireframeBehaviour_t90 **)(WireframeBehaviour_t90 **)SZArrayLdElema(V_10, L_5));
		NullCheck(V_9);
		Behaviour_set_enabled_m216(V_9, 0, /*hidden argument*/&Behaviour_set_enabled_m216_MethodInfo);
		V_11 = ((int32_t)(V_11+1));
	}

IL_008c:
	{
		NullCheck(V_10);
		if ((((int32_t)V_11) < ((int32_t)(((int32_t)(((Array_t *)V_10)->max_length))))))
		{
			goto IL_0077;
		}
	}
	{
		TrackableBehaviour_t44 * L_6 = (__this->___mTrackableBehaviour_2);
		NullCheck(L_6);
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&TrackableBehaviour_get_TrackableName_m345_MethodInfo, L_6);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_8 = String_Concat_m418(NULL /*static, unused*/, (String_t*) &_stringLiteral46, L_7, (String_t*) &_stringLiteral48, /*hidden argument*/&String_Concat_m418_MethodInfo);
		Debug_Log_m419(NULL /*static, unused*/, L_8, /*hidden argument*/&Debug_Log_m419_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.WireframeTrackableEventHandler
extern Il2CppType TrackableBehaviour_t44_0_0_1;
FieldInfo WireframeTrackableEventHandler_t91____mTrackableBehaviour_2_FieldInfo = 
{
	"mTrackableBehaviour"/* name */
	, &TrackableBehaviour_t44_0_0_1/* type */
	, &WireframeTrackableEventHandler_t91_il2cpp_TypeInfo/* parent */
	, offsetof(WireframeTrackableEventHandler_t91, ___mTrackableBehaviour_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* WireframeTrackableEventHandler_t91_FieldInfos[] =
{
	&WireframeTrackableEventHandler_t91____mTrackableBehaviour_2_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::.ctor()
MethodInfo WireframeTrackableEventHandler__ctor_m185_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler__ctor_m185/* method */
	, &WireframeTrackableEventHandler_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 186/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::Start()
MethodInfo WireframeTrackableEventHandler_Start_m186_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler_Start_m186/* method */
	, &WireframeTrackableEventHandler_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 187/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Status_t134_0_0_0;
extern Il2CppType Status_t134_0_0_0;
static ParameterInfo WireframeTrackableEventHandler_t91_WireframeTrackableEventHandler_OnTrackableStateChanged_m187_ParameterInfos[] = 
{
	{"previousStatus", 0, 134217794, &EmptyCustomAttributesCache, &Status_t134_0_0_0},
	{"newStatus", 1, 134217795, &EmptyCustomAttributesCache, &Status_t134_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
MethodInfo WireframeTrackableEventHandler_OnTrackableStateChanged_m187_MethodInfo = 
{
	"OnTrackableStateChanged"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler_OnTrackableStateChanged_m187/* method */
	, &WireframeTrackableEventHandler_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, WireframeTrackableEventHandler_t91_WireframeTrackableEventHandler_OnTrackableStateChanged_m187_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 188/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingFound()
MethodInfo WireframeTrackableEventHandler_OnTrackingFound_m188_MethodInfo = 
{
	"OnTrackingFound"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler_OnTrackingFound_m188/* method */
	, &WireframeTrackableEventHandler_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 189/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WireframeTrackableEventHandler::OnTrackingLost()
MethodInfo WireframeTrackableEventHandler_OnTrackingLost_m189_MethodInfo = 
{
	"OnTrackingLost"/* name */
	, (methodPointerType)&WireframeTrackableEventHandler_OnTrackingLost_m189/* method */
	, &WireframeTrackableEventHandler_t91_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 190/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* WireframeTrackableEventHandler_t91_MethodInfos[] =
{
	&WireframeTrackableEventHandler__ctor_m185_MethodInfo,
	&WireframeTrackableEventHandler_Start_m186_MethodInfo,
	&WireframeTrackableEventHandler_OnTrackableStateChanged_m187_MethodInfo,
	&WireframeTrackableEventHandler_OnTrackingFound_m188_MethodInfo,
	&WireframeTrackableEventHandler_OnTrackingLost_m189_MethodInfo,
	NULL
};
static MethodInfo* WireframeTrackableEventHandler_t91_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&WireframeTrackableEventHandler_OnTrackableStateChanged_m187_MethodInfo,
};
static TypeInfo* WireframeTrackableEventHandler_t91_InterfacesTypeInfos[] = 
{
	&ITrackableEventHandler_t135_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair WireframeTrackableEventHandler_t91_InterfacesOffsets[] = 
{
	{ &ITrackableEventHandler_t135_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType WireframeTrackableEventHandler_t91_0_0_0;
extern Il2CppType WireframeTrackableEventHandler_t91_1_0_0;
struct WireframeTrackableEventHandler_t91;
TypeInfo WireframeTrackableEventHandler_t91_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WireframeTrackableEventHandler"/* name */
	, "Vuforia"/* namespaze */
	, WireframeTrackableEventHandler_t91_MethodInfos/* methods */
	, NULL/* properties */
	, WireframeTrackableEventHandler_t91_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WireframeTrackableEventHandler_t91_il2cpp_TypeInfo/* element_class */
	, WireframeTrackableEventHandler_t91_InterfacesTypeInfos/* implemented_interfaces */
	, WireframeTrackableEventHandler_t91_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WireframeTrackableEventHandler_t91_il2cpp_TypeInfo/* cast_class */
	, &WireframeTrackableEventHandler_t91_0_0_0/* byval_arg */
	, &WireframeTrackableEventHandler_t91_1_0_0/* this_arg */
	, WireframeTrackableEventHandler_t91_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WireframeTrackableEventHandler_t91)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WordBehaviour_t92_il2cpp_TypeInfo;
// Vuforia.WordBehaviour
#include "AssemblyU2DCSharp_Vuforia_WordBehaviourMethodDeclarations.h"

// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavioMethodDeclarations.h"
extern MethodInfo WordAbstractBehaviour__ctor_m663_MethodInfo;


// System.Void Vuforia.WordBehaviour::.ctor()
extern MethodInfo WordBehaviour__ctor_m190_MethodInfo;
 void WordBehaviour__ctor_m190 (WordBehaviour_t92 * __this, MethodInfo* method){
	{
		WordAbstractBehaviour__ctor_m663(__this, /*hidden argument*/&WordAbstractBehaviour__ctor_m663_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.WordBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordBehaviour::.ctor()
MethodInfo WordBehaviour__ctor_m190_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WordBehaviour__ctor_m190/* method */
	, &WordBehaviour_t92_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 191/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* WordBehaviour_t92_MethodInfos[] =
{
	&WordBehaviour__ctor_m190_MethodInfo,
	NULL
};
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m664_MethodInfo;
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m665_MethodInfo;
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m666_MethodInfo;
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m667_MethodInfo;
extern MethodInfo WordAbstractBehaviour_InternalUnregisterTrackable_m668_MethodInfo;
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m669_MethodInfo;
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m670_MethodInfo;
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m671_MethodInfo;
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m672_MethodInfo;
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m673_MethodInfo;
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m674_MethodInfo;
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m675_MethodInfo;
static MethodInfo* WordBehaviour_t92_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m345_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m346_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m347_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m349_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m350_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m351_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m352_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m353_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m354_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m355_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m664_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m665_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m666_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m667_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m360_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m480_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m426_MethodInfo,
	&WordAbstractBehaviour_InternalUnregisterTrackable_m668_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m496_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m669_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m670_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m671_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m672_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m673_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m674_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m675_MethodInfo,
};
extern TypeInfo IEditorWordBehaviour_t178_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair WordBehaviour_t92_InterfacesOffsets[] = 
{
	{ &IEditorWordBehaviour_t178_il2cpp_TypeInfo, 25},
	{ &IEditorTrackableBehaviour_t125_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern Il2CppType WordBehaviour_t92_0_0_0;
extern Il2CppType WordBehaviour_t92_1_0_0;
extern TypeInfo WordAbstractBehaviour_t60_il2cpp_TypeInfo;
struct WordBehaviour_t92;
TypeInfo WordBehaviour_t92_il2cpp_TypeInfo = 
{
	&g_AssemblyU2DCSharp_dll_Image/* image */
	, NULL/* gc_desc */
	, "WordBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WordBehaviour_t92_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WordBehaviour_t92_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, WordBehaviour_t92_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WordBehaviour_t92_il2cpp_TypeInfo/* cast_class */
	, &WordBehaviour_t92_0_0_0/* byval_arg */
	, &WordBehaviour_t92_1_0_0/* this_arg */
	, WordBehaviour_t92_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WordBehaviour_t92)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 32/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
