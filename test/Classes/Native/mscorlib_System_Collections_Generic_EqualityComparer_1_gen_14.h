﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct EqualityComparer_1_t3322;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct EqualityComparer_1_t3322  : public Object_t
{
};
struct EqualityComparer_1_t3322_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.List`1<UnityEngine.UI.Text>>::_default
	EqualityComparer_1_t3322 * ____default_0;
};
