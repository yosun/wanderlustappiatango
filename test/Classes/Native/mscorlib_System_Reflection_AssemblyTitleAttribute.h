﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_t529  : public Attribute_t146
{
	// System.String System.Reflection.AssemblyTitleAttribute::name
	String_t* ___name_0;
};
