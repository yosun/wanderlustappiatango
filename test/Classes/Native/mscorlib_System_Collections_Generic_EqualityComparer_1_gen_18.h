﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct EqualityComparer_1_t3412;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct EqualityComparer_1_t3412  : public Object_t
{
};
struct EqualityComparer_1_t3412_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::_default
	EqualityComparer_1_t3412 * ____default_0;
};
