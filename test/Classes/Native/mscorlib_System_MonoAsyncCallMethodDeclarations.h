﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoAsyncCall
struct MonoAsyncCall_t2228;

// System.Void System.MonoAsyncCall::.ctor()
 void MonoAsyncCall__ctor_m12982 (MonoAsyncCall_t2228 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
