﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.Single
#include "mscorlib_System_Single.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo Single_t105_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t497  : public MulticastDelegate_t325
{
};
