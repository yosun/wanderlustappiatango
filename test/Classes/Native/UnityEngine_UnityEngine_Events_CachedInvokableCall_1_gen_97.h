﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_99.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadAbstractBehaviour>
struct CachedInvokableCall_1_t3734  : public InvokableCall_1_t3735
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
