﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PlayModeEditorUtility
struct PlayModeEditorUtility_t634;
// Vuforia.IPlayModeEditorUtility
struct IPlayModeEditorUtility_t633;

// Vuforia.IPlayModeEditorUtility Vuforia.PlayModeEditorUtility::get_Instance()
 Object_t * PlayModeEditorUtility_get_Instance_m2970 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility::set_Instance(Vuforia.IPlayModeEditorUtility)
 void PlayModeEditorUtility_set_Instance_m2971 (Object_t * __this/* static, unused */, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PlayModeEditorUtility::.ctor()
 void PlayModeEditorUtility__ctor_m2972 (PlayModeEditorUtility_t634 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
