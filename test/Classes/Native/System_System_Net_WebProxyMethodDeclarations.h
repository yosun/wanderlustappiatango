﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.WebProxy
struct WebProxy_t1347;
// System.Uri
struct Uri_t1322;
// System.String[]
struct StringU5BU5D_t112;
// System.Net.ICredentials
struct ICredentials_t1346;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1066;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Net.WebProxy::.ctor()
 void WebProxy__ctor_m6910 (WebProxy_t1347 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.Uri,System.Boolean,System.String[],System.Net.ICredentials)
 void WebProxy__ctor_m6911 (WebProxy_t1347 * __this, Uri_t1322 * ___address, bool ___bypassOnLocal, StringU5BU5D_t112* ___bypassList, Object_t * ___credentials, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void WebProxy__ctor_m6912 (WebProxy_t1347 * __this, SerializationInfo_t1066 * ___serializationInfo, StreamingContext_t1067  ___streamingContext, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m6913 (WebProxy_t1347 * __this, SerializationInfo_t1066 * ___serializationInfo, StreamingContext_t1067  ___streamingContext, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebProxy::get_UseDefaultCredentials()
 bool WebProxy_get_UseDefaultCredentials_m6914 (WebProxy_t1347 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.WebProxy::GetProxy(System.Uri)
 Uri_t1322 * WebProxy_GetProxy_m6915 (WebProxy_t1347 * __this, Uri_t1322 * ___destination, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.WebProxy::IsBypassed(System.Uri)
 bool WebProxy_IsBypassed_m6916 (WebProxy_t1347 * __this, Uri_t1322 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void WebProxy_GetObjectData_m6917 (WebProxy_t1347 * __this, SerializationInfo_t1066 * ___serializationInfo, StreamingContext_t1067  ___streamingContext, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.WebProxy::CheckBypassList()
 void WebProxy_CheckBypassList_m6918 (WebProxy_t1347 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
