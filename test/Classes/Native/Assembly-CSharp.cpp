﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern TypeInfo U3CModuleU3E_t0_il2cpp_TypeInfo;
extern TypeInfo TheCurrentMode_t1_il2cpp_TypeInfo;
extern TypeInfo ARVRModes_t5_il2cpp_TypeInfo;
extern TypeInfo AnimateTexture_t8_il2cpp_TypeInfo;
extern TypeInfo GyroCameraYo_t11_il2cpp_TypeInfo;
extern TypeInfo Input2_t14_il2cpp_TypeInfo;
extern TypeInfo Mathf2_t15_il2cpp_TypeInfo;
extern TypeInfo Raycaster_t20_il2cpp_TypeInfo;
extern TypeInfo SetRenderQueue_t22_il2cpp_TypeInfo;
extern TypeInfo SetRenderQueueChildren_t23_il2cpp_TypeInfo;
extern TypeInfo WorldLoop_t24_il2cpp_TypeInfo;
extern TypeInfo WorldNav_t25_il2cpp_TypeInfo;
extern TypeInfo OrbitCamera_t26_il2cpp_TypeInfo;
extern TypeInfo TestParticles_t28_il2cpp_TypeInfo;
extern TypeInfo BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo;
extern TypeInfo CloudRecoBehaviour_t31_il2cpp_TypeInfo;
extern TypeInfo CylinderTargetBehaviour_t33_il2cpp_TypeInfo;
extern TypeInfo DataSetLoadBehaviour_t35_il2cpp_TypeInfo;
extern TypeInfo DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo;
extern TypeInfo DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo;
extern TypeInfo DefaultTrackableEventHandler_t45_il2cpp_TypeInfo;
extern TypeInfo GLErrorHandler_t46_il2cpp_TypeInfo;
extern TypeInfo HideExcessAreaBehaviour_t47_il2cpp_TypeInfo;
extern TypeInfo ImageTargetBehaviour_t49_il2cpp_TypeInfo;
extern TypeInfo AndroidUnityPlayer_t51_il2cpp_TypeInfo;
extern TypeInfo ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo;
extern TypeInfo IOSUnityPlayer_t53_il2cpp_TypeInfo;
extern TypeInfo VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo;
extern TypeInfo KeepAliveBehaviour_t63_il2cpp_TypeInfo;
extern TypeInfo MarkerBehaviour_t65_il2cpp_TypeInfo;
extern TypeInfo MaskOutBehaviour_t66_il2cpp_TypeInfo;
extern TypeInfo MultiTargetBehaviour_t67_il2cpp_TypeInfo;
extern TypeInfo ObjectTargetBehaviour_t68_il2cpp_TypeInfo;
extern TypeInfo PropBehaviour_t39_il2cpp_TypeInfo;
extern TypeInfo QCARBehaviour_t70_il2cpp_TypeInfo;
extern TypeInfo ReconstructionBehaviour_t38_il2cpp_TypeInfo;
extern TypeInfo ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo;
extern TypeInfo SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo;
extern TypeInfo SurfaceBehaviour_t40_il2cpp_TypeInfo;
extern TypeInfo TextRecoBehaviour_t78_il2cpp_TypeInfo;
extern TypeInfo TurnOffBehaviour_t79_il2cpp_TypeInfo;
extern TypeInfo TurnOffWordBehaviour_t80_il2cpp_TypeInfo;
extern TypeInfo UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo;
extern TypeInfo VideoBackgroundBehaviour_t83_il2cpp_TypeInfo;
extern TypeInfo VideoTextureRenderer_t85_il2cpp_TypeInfo;
extern TypeInfo VirtualButtonBehaviour_t87_il2cpp_TypeInfo;
extern TypeInfo WebCamBehaviour_t88_il2cpp_TypeInfo;
extern TypeInfo WireframeBehaviour_t90_il2cpp_TypeInfo;
extern TypeInfo WireframeTrackableEventHandler_t91_il2cpp_TypeInfo;
extern TypeInfo WordBehaviour_t92_il2cpp_TypeInfo;
#include "utils/RegisterRuntimeInitializeAndCleanup.h"
#include <map>
struct TypeInfo;
struct MethodInfo;
TypeInfo* g_AssemblyU2DCSharp_Assembly_Types[51] = 
{
	&U3CModuleU3E_t0_il2cpp_TypeInfo,
	&TheCurrentMode_t1_il2cpp_TypeInfo,
	&ARVRModes_t5_il2cpp_TypeInfo,
	&AnimateTexture_t8_il2cpp_TypeInfo,
	&GyroCameraYo_t11_il2cpp_TypeInfo,
	&Input2_t14_il2cpp_TypeInfo,
	&Mathf2_t15_il2cpp_TypeInfo,
	&Raycaster_t20_il2cpp_TypeInfo,
	&SetRenderQueue_t22_il2cpp_TypeInfo,
	&SetRenderQueueChildren_t23_il2cpp_TypeInfo,
	&WorldLoop_t24_il2cpp_TypeInfo,
	&WorldNav_t25_il2cpp_TypeInfo,
	&OrbitCamera_t26_il2cpp_TypeInfo,
	&TestParticles_t28_il2cpp_TypeInfo,
	&BackgroundPlaneBehaviour_t29_il2cpp_TypeInfo,
	&CloudRecoBehaviour_t31_il2cpp_TypeInfo,
	&CylinderTargetBehaviour_t33_il2cpp_TypeInfo,
	&DataSetLoadBehaviour_t35_il2cpp_TypeInfo,
	&DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo,
	&DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo,
	&DefaultTrackableEventHandler_t45_il2cpp_TypeInfo,
	&GLErrorHandler_t46_il2cpp_TypeInfo,
	&HideExcessAreaBehaviour_t47_il2cpp_TypeInfo,
	&ImageTargetBehaviour_t49_il2cpp_TypeInfo,
	&AndroidUnityPlayer_t51_il2cpp_TypeInfo,
	&ComponentFactoryStarterBehaviour_t52_il2cpp_TypeInfo,
	&IOSUnityPlayer_t53_il2cpp_TypeInfo,
	&VuforiaBehaviourComponentFactory_t54_il2cpp_TypeInfo,
	&KeepAliveBehaviour_t63_il2cpp_TypeInfo,
	&MarkerBehaviour_t65_il2cpp_TypeInfo,
	&MaskOutBehaviour_t66_il2cpp_TypeInfo,
	&MultiTargetBehaviour_t67_il2cpp_TypeInfo,
	&ObjectTargetBehaviour_t68_il2cpp_TypeInfo,
	&PropBehaviour_t39_il2cpp_TypeInfo,
	&QCARBehaviour_t70_il2cpp_TypeInfo,
	&ReconstructionBehaviour_t38_il2cpp_TypeInfo,
	&ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo,
	&SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo,
	&SurfaceBehaviour_t40_il2cpp_TypeInfo,
	&TextRecoBehaviour_t78_il2cpp_TypeInfo,
	&TurnOffBehaviour_t79_il2cpp_TypeInfo,
	&TurnOffWordBehaviour_t80_il2cpp_TypeInfo,
	&UserDefinedTargetBuildingBehaviour_t81_il2cpp_TypeInfo,
	&VideoBackgroundBehaviour_t83_il2cpp_TypeInfo,
	&VideoTextureRenderer_t85_il2cpp_TypeInfo,
	&VirtualButtonBehaviour_t87_il2cpp_TypeInfo,
	&WebCamBehaviour_t88_il2cpp_TypeInfo,
	&WireframeBehaviour_t90_il2cpp_TypeInfo,
	&WireframeTrackableEventHandler_t91_il2cpp_TypeInfo,
	&WordBehaviour_t92_il2cpp_TypeInfo,
	NULL,
};
extern Il2CppImage g_AssemblyU2DCSharp_dll_Image;
extern CustomAttributesCache g_AssemblyU2DCSharp_Assembly__CustomAttributeCache;
Il2CppAssembly g_AssemblyU2DCSharp_Assembly = 
{
	{ "Assembly-CSharp", 0, 0, 0, { 0 }, 32772, 0, 0, 0, 0, 0, 0 },
	&g_AssemblyU2DCSharp_dll_Image,
	&g_AssemblyU2DCSharp_Assembly__CustomAttributeCache,
};
Il2CppImage g_AssemblyU2DCSharp_dll_Image = 
{
	 "Assembly-CSharp.dll" ,
	&g_AssemblyU2DCSharp_Assembly,
	g_AssemblyU2DCSharp_Assembly_Types,
	50,
	NULL,
};
static void s_AssemblyU2DCSharpRegistration()
{
	RegisterAssembly (&g_AssemblyU2DCSharp_Assembly);
}
static il2cpp::utils::RegisterRuntimeInitializeAndCleanup s_AssemblyU2DCSharpRegistrationVariable(&s_AssemblyU2DCSharpRegistration, NULL);
