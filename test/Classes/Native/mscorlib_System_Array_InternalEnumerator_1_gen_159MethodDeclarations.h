﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTriggerType>
struct InternalEnumerator_1_t3123;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.EventSystems.EventTriggerType
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTriggerType.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTriggerType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m16453 (InternalEnumerator_1_t3123 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTriggerType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16454 (InternalEnumerator_1_t3123 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTriggerType>::Dispose()
 void InternalEnumerator_1_Dispose_m16455 (InternalEnumerator_1_t3123 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTriggerType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m16456 (InternalEnumerator_1_t3123 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.EventTriggerType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m16457 (InternalEnumerator_1_t3123 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
