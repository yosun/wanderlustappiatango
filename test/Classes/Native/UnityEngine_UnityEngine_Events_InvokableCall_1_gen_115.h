﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.WordAbstractBehaviour>
struct UnityAction_1_t4426;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.WordAbstractBehaviour>
struct InvokableCall_1_t4425  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.WordAbstractBehaviour>::Delegate
	UnityAction_1_t4426 * ___Delegate_0;
};
