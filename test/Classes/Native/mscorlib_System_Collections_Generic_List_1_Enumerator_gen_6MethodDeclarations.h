﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Int32>
struct Enumerator_t811;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t727;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
 void Enumerator__ctor_m22404 (Enumerator_t811 * __this, List_1_t727 * ___l, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22405 (Enumerator_t811 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
 void Enumerator_Dispose_m22406 (Enumerator_t811 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
 void Enumerator_VerifyState_m22407 (Enumerator_t811 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
 bool Enumerator_MoveNext_m4863 (Enumerator_t811 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
 int32_t Enumerator_get_Current_m4860 (Enumerator_t811 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
