﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<WorldNav>
struct UnityAction_1_t2763;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<WorldNav>
struct InvokableCall_1_t2762  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<WorldNav>::Delegate
	UnityAction_1_t2763 * ___Delegate_0;
};
