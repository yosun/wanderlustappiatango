﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.OidEnumerator
struct OidEnumerator_t1397;
// System.Object
struct Object_t;
// System.Security.Cryptography.OidCollection
struct OidCollection_t1381;

// System.Void System.Security.Cryptography.OidEnumerator::.ctor(System.Security.Cryptography.OidCollection)
 void OidEnumerator__ctor_m7148 (OidEnumerator_t1397 * __this, OidCollection_t1381 * ___collection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Cryptography.OidEnumerator::System.Collections.IEnumerator.get_Current()
 Object_t * OidEnumerator_System_Collections_IEnumerator_get_Current_m7149 (OidEnumerator_t1397 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.OidEnumerator::MoveNext()
 bool OidEnumerator_MoveNext_m7150 (OidEnumerator_t1397 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
