﻿#pragma once
#include <stdint.h>
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t581;
// UnityEngine.MeshFilter
struct MeshFilter_t169;
// UnityEngine.MeshCollider
struct MeshCollider_t582;
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"
// Vuforia.SmartTerrainTrackableBehaviour
struct SmartTerrainTrackableBehaviour_t583  : public TrackableBehaviour_t44
{
	// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableBehaviour::mSmartTerrainTrackable
	Object_t * ___mSmartTerrainTrackable_9;
	// System.Boolean Vuforia.SmartTerrainTrackableBehaviour::mDisableAutomaticUpdates
	bool ___mDisableAutomaticUpdates_10;
	// UnityEngine.MeshFilter Vuforia.SmartTerrainTrackableBehaviour::mMeshFilterToUpdate
	MeshFilter_t169 * ___mMeshFilterToUpdate_11;
	// UnityEngine.MeshCollider Vuforia.SmartTerrainTrackableBehaviour::mMeshColliderToUpdate
	MeshCollider_t582 * ___mMeshColliderToUpdate_12;
};
