﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.InternalsVisibleToAttribute
struct InternalsVisibleToAttribute_t904;
// System.String
struct String_t;

// System.Void System.Runtime.CompilerServices.InternalsVisibleToAttribute::.ctor(System.String)
 void InternalsVisibleToAttribute__ctor_m5418 (InternalsVisibleToAttribute_t904 * __this, String_t* ___assemblyName, MethodInfo* method) IL2CPP_METHOD_ATTR;
