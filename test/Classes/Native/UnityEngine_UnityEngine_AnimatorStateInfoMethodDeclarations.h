﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AnimatorStateInfo
struct AnimatorStateInfo_t1015;
// System.String
struct String_t;

// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
 bool AnimatorStateInfo_IsName_m6148 (AnimatorStateInfo_t1015 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
 int32_t AnimatorStateInfo_get_fullPathHash_m6149 (AnimatorStateInfo_t1015 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimatorStateInfo::get_nameHash()
 int32_t AnimatorStateInfo_get_nameHash_m6150 (AnimatorStateInfo_t1015 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimatorStateInfo::get_shortNameHash()
 int32_t AnimatorStateInfo_get_shortNameHash_m6151 (AnimatorStateInfo_t1015 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
 float AnimatorStateInfo_get_normalizedTime_m6152 (AnimatorStateInfo_t1015 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimatorStateInfo::get_length()
 float AnimatorStateInfo_get_length_m6153 (AnimatorStateInfo_t1015 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
 int32_t AnimatorStateInfo_get_tagHash_m6154 (AnimatorStateInfo_t1015 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
 bool AnimatorStateInfo_IsTag_m6155 (AnimatorStateInfo_t1015 * __this, String_t* ___tag, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
 bool AnimatorStateInfo_get_loop_m6156 (AnimatorStateInfo_t1015 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
