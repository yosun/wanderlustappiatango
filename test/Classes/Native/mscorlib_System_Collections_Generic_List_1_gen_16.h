﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle[]
struct ToggleU5BU5D_t3534;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
struct List_1_t371  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::_items
	ToggleU5BU5D_t3534* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::_version
	int32_t ____version_3;
};
struct List_1_t371_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::EmptyArray
	ToggleU5BU5D_t3534* ___EmptyArray_4;
};
