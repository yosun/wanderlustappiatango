﻿#pragma once
#include <stdint.h>
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t1054;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<UnityEngine.SocialPlatforms.IScore[]>
struct Action_1_t922  : public MulticastDelegate_t325
{
};
