﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.OrientedBoundingBox
struct OrientedBoundingBox_t590;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void Vuforia.OrientedBoundingBox::.ctor(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
 void OrientedBoundingBox__ctor_m2790 (OrientedBoundingBox_t590 * __this, Vector2_t9  ___center, Vector2_t9  ___halfExtents, float ___rotation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vuforia.OrientedBoundingBox::get_Center()
 Vector2_t9  OrientedBoundingBox_get_Center_m2791 (OrientedBoundingBox_t590 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox::set_Center(UnityEngine.Vector2)
 void OrientedBoundingBox_set_Center_m2792 (OrientedBoundingBox_t590 * __this, Vector2_t9  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vuforia.OrientedBoundingBox::get_HalfExtents()
 Vector2_t9  OrientedBoundingBox_get_HalfExtents_m2793 (OrientedBoundingBox_t590 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox::set_HalfExtents(UnityEngine.Vector2)
 void OrientedBoundingBox_set_HalfExtents_m2794 (OrientedBoundingBox_t590 * __this, Vector2_t9  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.OrientedBoundingBox::get_Rotation()
 float OrientedBoundingBox_get_Rotation_m2795 (OrientedBoundingBox_t590 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.OrientedBoundingBox::set_Rotation(System.Single)
 void OrientedBoundingBox_set_Rotation_m2796 (OrientedBoundingBox_t590 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
