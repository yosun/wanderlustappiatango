﻿#pragma once
#include <stdint.h>
// Vuforia.ITrackerEventHandler
struct ITrackerEventHandler_t761;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.ITrackerEventHandler>
struct Predicate_1_t4318  : public MulticastDelegate_t325
{
};
