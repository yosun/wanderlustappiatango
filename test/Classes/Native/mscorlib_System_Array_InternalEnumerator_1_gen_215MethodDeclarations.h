﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>
struct InternalEnumerator_1_t3433;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m18847 (InternalEnumerator_1_t3433 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18848 (InternalEnumerator_1_t3433 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::Dispose()
 void InternalEnumerator_1_Dispose_m18849 (InternalEnumerator_1_t3433 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m18850 (InternalEnumerator_1_t3433 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::get_Current()
 UILineInfo_t487  InternalEnumerator_1_get_Current_m18851 (InternalEnumerator_1_t3433 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
