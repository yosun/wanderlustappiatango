﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>
struct KeyValuePair_2_t4857;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m29843 (KeyValuePair_2_t4857 * __this, String_t* ___key, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::get_Key()
 String_t* KeyValuePair_2_get_Key_m29844 (KeyValuePair_2_t4857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m29845 (KeyValuePair_2_t4857 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::get_Value()
 bool KeyValuePair_2_get_Value_m29846 (KeyValuePair_2_t4857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m29847 (KeyValuePair_2_t4857 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::ToString()
 String_t* KeyValuePair_2_ToString_m29848 (KeyValuePair_2_t4857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
