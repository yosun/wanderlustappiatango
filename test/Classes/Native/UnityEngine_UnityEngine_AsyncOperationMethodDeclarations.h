﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AsyncOperation
struct AsyncOperation_t909;
struct AsyncOperation_t909_marshaled;

// System.Void UnityEngine.AsyncOperation::.ctor()
 void AsyncOperation__ctor_m5946 (AsyncOperation_t909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
 void AsyncOperation_InternalDestroy_m5947 (AsyncOperation_t909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::Finalize()
 void AsyncOperation_Finalize_m5948 (AsyncOperation_t909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void AsyncOperation_t909_marshal(const AsyncOperation_t909& unmarshaled, AsyncOperation_t909_marshaled& marshaled);
void AsyncOperation_t909_marshal_back(const AsyncOperation_t909_marshaled& marshaled, AsyncOperation_t909& unmarshaled);
void AsyncOperation_t909_marshal_cleanup(AsyncOperation_t909_marshaled& marshaled);
