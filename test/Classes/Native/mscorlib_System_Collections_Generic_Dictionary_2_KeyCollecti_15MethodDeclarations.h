﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct Enumerator_t3313;
// System.Object
struct Object_t;
// UnityEngine.Font
struct Font_t280;
// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct Dictionary_2_t282;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_13MethodDeclarations.h"
#define Enumerator__ctor_m17919(__this, ___host, method) (void)Enumerator__ctor_m17666_gshared((Enumerator_t3285 *)__this, (Dictionary_2_t3275 *)___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17920(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m17667_gshared((Enumerator_t3285 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Dispose()
#define Enumerator_Dispose_m17921(__this, method) (void)Enumerator_Dispose_m17668_gshared((Enumerator_t3285 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::MoveNext()
#define Enumerator_MoveNext_m17922(__this, method) (bool)Enumerator_MoveNext_m17669_gshared((Enumerator_t3285 *)__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Current()
#define Enumerator_get_Current_m17923(__this, method) (Font_t280 *)Enumerator_get_Current_m17670_gshared((Enumerator_t3285 *)__this, method)
