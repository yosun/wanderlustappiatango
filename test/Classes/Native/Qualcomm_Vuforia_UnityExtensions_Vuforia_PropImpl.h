﻿#pragma once
#include <stdint.h>
// Vuforia.SmartTerrainTrackableImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab_0.h"
// Vuforia.OrientedBoundingBox3D
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox_0.h"
// Vuforia.PropImpl
struct PropImpl_t674  : public SmartTerrainTrackableImpl_t666
{
	// Vuforia.OrientedBoundingBox3D Vuforia.PropImpl::mOrientedBoundingBox3D
	OrientedBoundingBox3D_t591  ___mOrientedBoundingBox3D_7;
};
