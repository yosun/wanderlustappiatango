﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>
struct Enumerator_t4707;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1029;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
 void Enumerator__ctor_m28851 (Enumerator_t4707 * __this, List_1_t1029 * ___l, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m28852 (Enumerator_t4707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
 void Enumerator_Dispose_m28853 (Enumerator_t4707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
 void Enumerator_VerifyState_m28854 (Enumerator_t4707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
 bool Enumerator_MoveNext_m28855 (Enumerator_t4707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
 UILineInfo_t487  Enumerator_get_Current_m28856 (Enumerator_t4707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
