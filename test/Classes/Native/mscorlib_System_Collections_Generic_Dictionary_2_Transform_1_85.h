﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo Int32_t93_il2cpp_TypeInfo;
extern TypeInfo VirtualButtonData_t640_il2cpp_TypeInfo;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,System.Collections.DictionaryEntry>
struct Transform_1_t4189  : public MulticastDelegate_t325
{
};
