﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Assembly/ResolveEventHolder
struct ResolveEventHolder_t1916;

// System.Void System.Reflection.Assembly/ResolveEventHolder::.ctor()
 void ResolveEventHolder__ctor_m11131 (ResolveEventHolder_t1916 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
