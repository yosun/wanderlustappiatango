﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Uri/UriScheme>
struct InternalEnumerator_1_t4896;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"

// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30062 (InternalEnumerator_1_t4896 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30063 (InternalEnumerator_1_t4896 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::Dispose()
 void InternalEnumerator_1_Dispose_m30064 (InternalEnumerator_1_t4896 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Uri/UriScheme>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30065 (InternalEnumerator_1_t4896 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Uri/UriScheme>::get_Current()
 UriScheme_t1463  InternalEnumerator_1_get_Current_m30066 (InternalEnumerator_1_t4896 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
