﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
struct InternalEnumerator_1_t2927;
// System.Object
struct Object_t;
// Vuforia.SmartTerrainTrackerAbstractBehaviour
struct SmartTerrainTrackerAbstractBehaviour_t76;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15101(__this, ___array, method) (void)InternalEnumerator_1__ctor_m13978_gshared((InternalEnumerator_1_t2698 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15102(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m15103(__this, method) (void)InternalEnumerator_1_Dispose_m13982_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15104(__this, method) (bool)InternalEnumerator_1_MoveNext_m13984_gshared((InternalEnumerator_1_t2698 *)__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m15105(__this, method) (SmartTerrainTrackerAbstractBehaviour_t76 *)InternalEnumerator_1_get_Current_m13986_gshared((InternalEnumerator_1_t2698 *)__this, method)
