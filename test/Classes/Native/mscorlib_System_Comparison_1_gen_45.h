﻿#pragma once
#include <stdint.h>
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t122;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.IVideoBackgroundEventHandler>
struct Comparison_1_t4333  : public MulticastDelegate_t325
{
};
