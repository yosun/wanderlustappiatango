﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.ScriptableObject>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_117.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.ScriptableObject>
struct CachedInvokableCall_1_t4438  : public InvokableCall_1_t4439
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.ScriptableObject>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
