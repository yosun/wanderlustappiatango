﻿#pragma once
#include <stdint.h>
// System.DBNull
struct DBNull_t2196;
// System.Object
#include "mscorlib_System_Object.h"
// System.DBNull
struct DBNull_t2196  : public Object_t
{
};
struct DBNull_t2196_StaticFields{
	// System.DBNull System.DBNull::Value
	DBNull_t2196 * ___Value_0;
};
