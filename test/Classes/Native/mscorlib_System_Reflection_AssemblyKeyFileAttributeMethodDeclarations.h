﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyKeyFileAttribute
struct AssemblyKeyFileAttribute_t1283;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyKeyFileAttribute::.ctor(System.String)
 void AssemblyKeyFileAttribute__ctor_m6714 (AssemblyKeyFileAttribute_t1283 * __this, String_t* ___keyFile, MethodInfo* method) IL2CPP_METHOD_ATTR;
