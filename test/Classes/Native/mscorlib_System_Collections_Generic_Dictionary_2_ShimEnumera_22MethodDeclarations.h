﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.ImageTarget>
struct ShimEnumerator_t4247;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.ImageTarget>
struct Dictionary_2_t738;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.ImageTarget>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m25774 (ShimEnumerator_t4247 * __this, Dictionary_2_t738 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.ImageTarget>::MoveNext()
 bool ShimEnumerator_MoveNext_m25775 (ShimEnumerator_t4247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.ImageTarget>::get_Entry()
 DictionaryEntry_t1302  ShimEnumerator_get_Entry_m25776 (ShimEnumerator_t4247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.ImageTarget>::get_Key()
 Object_t * ShimEnumerator_get_Key_m25777 (ShimEnumerator_t4247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.ImageTarget>::get_Value()
 Object_t * ShimEnumerator_get_Value_m25778 (ShimEnumerator_t4247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.ImageTarget>::get_Current()
 Object_t * ShimEnumerator_get_Current_m25779 (ShimEnumerator_t4247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
