﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$120
struct $ArrayType$120_t2264;
struct $ArrayType$120_t2264_marshaled;

void $ArrayType$120_t2264_marshal(const $ArrayType$120_t2264& unmarshaled, $ArrayType$120_t2264_marshaled& marshaled);
void $ArrayType$120_t2264_marshal_back(const $ArrayType$120_t2264_marshaled& marshaled, $ArrayType$120_t2264& unmarshaled);
void $ArrayType$120_t2264_marshal_cleanup($ArrayType$120_t2264_marshaled& marshaled);
