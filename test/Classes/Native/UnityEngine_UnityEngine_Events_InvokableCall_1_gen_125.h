﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.ReflectionProbe>
struct UnityAction_1_t4493;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.ReflectionProbe>
struct InvokableCall_1_t4492  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.ReflectionProbe>::Delegate
	UnityAction_1_t4493 * ___Delegate_0;
};
