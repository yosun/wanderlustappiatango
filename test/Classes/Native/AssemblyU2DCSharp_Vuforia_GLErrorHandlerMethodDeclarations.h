﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.GLErrorHandler
struct GLErrorHandler_t46;
// System.String
struct String_t;

// System.Void Vuforia.GLErrorHandler::.ctor()
 void GLErrorHandler__ctor_m108 (GLErrorHandler_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.GLErrorHandler::.cctor()
 void GLErrorHandler__cctor_m109 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.GLErrorHandler::SetError(System.String)
 void GLErrorHandler_SetError_m110 (Object_t * __this/* static, unused */, String_t* ___errorText, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.GLErrorHandler::OnGUI()
 void GLErrorHandler_OnGUI_m111 (GLErrorHandler_t46 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.GLErrorHandler::DrawWindowContent(System.Int32)
 void GLErrorHandler_DrawWindowContent_m112 (GLErrorHandler_t46 * __this, int32_t ___id, MethodInfo* method) IL2CPP_METHOD_ATTR;
