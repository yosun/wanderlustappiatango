﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_16.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
struct CachedInvokableCall_1_t2808  : public InvokableCall_1_t2809
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
