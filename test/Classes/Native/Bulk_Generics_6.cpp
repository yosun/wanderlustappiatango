﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IList_1_t7468_il2cpp_TypeInfo;

// System.Int32
#include "mscorlib_System_Int32.h"
// System.Void
#include "mscorlib_System_Void.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>
extern MethodInfo IList_1_get_Item_m42394_MethodInfo;
extern MethodInfo IList_1_set_Item_m42395_MethodInfo;
static PropertyInfo IList_1_t7468____Item_PropertyInfo = 
{
	&IList_1_t7468_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42394_MethodInfo/* get */
	, &IList_1_set_Item_m42395_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7468_PropertyInfos[] =
{
	&IList_1_t7468____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorPropBehaviour_t160_0_0_0;
extern Il2CppType IEditorPropBehaviour_t160_0_0_0;
static ParameterInfo IList_1_t7468_IList_1_IndexOf_m42396_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorPropBehaviour_t160_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42396_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42396_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7468_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7468_IList_1_IndexOf_m42396_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42396_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorPropBehaviour_t160_0_0_0;
static ParameterInfo IList_1_t7468_IList_1_Insert_m42397_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorPropBehaviour_t160_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42397_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42397_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7468_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7468_IList_1_Insert_m42397_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42397_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7468_IList_1_RemoveAt_m42398_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42398_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42398_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7468_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7468_IList_1_RemoveAt_m42398_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42398_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7468_IList_1_get_Item_m42394_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEditorPropBehaviour_t160_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42394_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42394_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7468_il2cpp_TypeInfo/* declaring_type */
	, &IEditorPropBehaviour_t160_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7468_IList_1_get_Item_m42394_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42394_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorPropBehaviour_t160_0_0_0;
static ParameterInfo IList_1_t7468_IList_1_set_Item_m42395_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorPropBehaviour_t160_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42395_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorPropBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42395_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7468_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7468_IList_1_set_Item_m42395_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42395_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7468_MethodInfos[] =
{
	&IList_1_IndexOf_m42396_MethodInfo,
	&IList_1_Insert_m42397_MethodInfo,
	&IList_1_RemoveAt_m42398_MethodInfo,
	&IList_1_get_Item_m42394_MethodInfo,
	&IList_1_set_Item_m42395_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7467_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7469_il2cpp_TypeInfo;
static TypeInfo* IList_1_t7468_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7467_il2cpp_TypeInfo,
	&IEnumerable_1_t7469_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7468_0_0_0;
extern Il2CppType IList_1_t7468_1_0_0;
struct IList_1_t7468;
extern Il2CppGenericClass IList_1_t7468_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7468_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7468_MethodInfos/* methods */
	, IList_1_t7468_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7468_il2cpp_TypeInfo/* element_class */
	, IList_1_t7468_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7468_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7468_0_0_0/* byval_arg */
	, &IList_1_t7468_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7468_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7470_il2cpp_TypeInfo;

// System.Boolean
#include "mscorlib_System_Boolean.h"
// Vuforia.SmartTerrainTrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackab.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>
extern MethodInfo ICollection_1_get_Count_m42399_MethodInfo;
static PropertyInfo ICollection_1_t7470____Count_PropertyInfo = 
{
	&ICollection_1_t7470_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42399_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42400_MethodInfo;
static PropertyInfo ICollection_1_t7470____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7470_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42400_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7470_PropertyInfos[] =
{
	&ICollection_1_t7470____Count_PropertyInfo,
	&ICollection_1_t7470____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42399_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42399_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7470_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42399_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42400_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42400_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7470_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42400_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackableBehaviour_t583_0_0_0;
extern Il2CppType SmartTerrainTrackableBehaviour_t583_0_0_0;
static ParameterInfo ICollection_1_t7470_ICollection_1_Add_m42401_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackableBehaviour_t583_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42401_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42401_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7470_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7470_ICollection_1_Add_m42401_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42401_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42402_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42402_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7470_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42402_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackableBehaviour_t583_0_0_0;
static ParameterInfo ICollection_1_t7470_ICollection_1_Contains_m42403_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackableBehaviour_t583_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42403_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42403_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7470_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7470_ICollection_1_Contains_m42403_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42403_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackableBehaviourU5BU5D_t820_0_0_0;
extern Il2CppType SmartTerrainTrackableBehaviourU5BU5D_t820_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7470_ICollection_1_CopyTo_m42404_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackableBehaviourU5BU5D_t820_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42404_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42404_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7470_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7470_ICollection_1_CopyTo_m42404_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42404_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackableBehaviour_t583_0_0_0;
static ParameterInfo ICollection_1_t7470_ICollection_1_Remove_m42405_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackableBehaviour_t583_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42405_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackableBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42405_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7470_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7470_ICollection_1_Remove_m42405_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42405_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7470_MethodInfos[] =
{
	&ICollection_1_get_Count_m42399_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42400_MethodInfo,
	&ICollection_1_Add_m42401_MethodInfo,
	&ICollection_1_Clear_m42402_MethodInfo,
	&ICollection_1_Contains_m42403_MethodInfo,
	&ICollection_1_CopyTo_m42404_MethodInfo,
	&ICollection_1_Remove_m42405_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7472_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7470_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7472_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7470_0_0_0;
extern Il2CppType ICollection_1_t7470_1_0_0;
struct ICollection_1_t7470;
extern Il2CppGenericClass ICollection_1_t7470_GenericClass;
TypeInfo ICollection_1_t7470_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7470_MethodInfos/* methods */
	, ICollection_1_t7470_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7470_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7470_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7470_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7470_0_0_0/* byval_arg */
	, &ICollection_1_t7470_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7470_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackableBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackableBehaviour>
extern Il2CppType IEnumerator_1_t5870_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42406_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackableBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42406_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7472_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5870_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42406_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7472_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42406_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7472_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7472_0_0_0;
extern Il2CppType IEnumerable_1_t7472_1_0_0;
struct IEnumerable_1_t7472;
extern Il2CppGenericClass IEnumerable_1_t7472_GenericClass;
TypeInfo IEnumerable_1_t7472_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7472_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7472_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7472_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7472_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7472_0_0_0/* byval_arg */
	, &IEnumerable_1_t7472_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7472_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5870_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42407_MethodInfo;
static PropertyInfo IEnumerator_1_t5870____Current_PropertyInfo = 
{
	&IEnumerator_1_t5870_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42407_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5870_PropertyInfos[] =
{
	&IEnumerator_1_t5870____Current_PropertyInfo,
	NULL
};
extern Il2CppType SmartTerrainTrackableBehaviour_t583_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42407_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42407_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5870_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackableBehaviour_t583_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42407_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5870_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42407_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t266_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t5870_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5870_0_0_0;
extern Il2CppType IEnumerator_1_t5870_1_0_0;
struct IEnumerator_1_t5870;
extern Il2CppGenericClass IEnumerator_1_t5870_GenericClass;
TypeInfo IEnumerator_1_t5870_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5870_MethodInfos/* methods */
	, IEnumerator_1_t5870_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5870_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5870_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5870_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5870_0_0_0/* byval_arg */
	, &IEnumerator_1_t5870_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5870_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_93.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2907_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_93MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
extern TypeInfo SmartTerrainTrackableBehaviour_t583_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1493_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m15025_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7651_MethodInfo;
extern MethodInfo Array_get_Length_m7656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSmartTerrainTrackableBehaviour_t583_m32530_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m31436_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m31436(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.SmartTerrainTrackableBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.SmartTerrainTrackableBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisSmartTerrainTrackableBehaviour_t583_m32530(__this, p0, method) (SmartTerrainTrackableBehaviour_t583 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2907____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2907_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2907, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2907____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2907_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2907, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2907_FieldInfos[] =
{
	&InternalEnumerator_1_t2907____array_0_FieldInfo,
	&InternalEnumerator_1_t2907____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15022_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2907____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2907_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15022_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2907____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2907_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15025_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2907_PropertyInfos[] =
{
	&InternalEnumerator_1_t2907____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2907____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2907_InternalEnumerator_1__ctor_m15021_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15021_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15021_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2907_InternalEnumerator_1__ctor_m15021_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15021_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15022_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15022_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2907_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15022_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15023_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15023_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15023_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15024_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15024_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2907_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15024_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackableBehaviour_t583_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15025_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15025_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2907_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackableBehaviour_t583_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15025_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2907_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15021_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15022_MethodInfo,
	&InternalEnumerator_1_Dispose_m15023_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15024_MethodInfo,
	&InternalEnumerator_1_get_Current_m15025_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m15024_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15023_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2907_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15022_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15024_MethodInfo,
	&InternalEnumerator_1_Dispose_m15023_MethodInfo,
	&InternalEnumerator_1_get_Current_m15025_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2907_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5870_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2907_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5870_il2cpp_TypeInfo, 7},
};
extern TypeInfo SmartTerrainTrackableBehaviour_t583_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2907_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15025_MethodInfo/* Method Usage */,
	&SmartTerrainTrackableBehaviour_t583_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSmartTerrainTrackableBehaviour_t583_m32530_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2907_0_0_0;
extern Il2CppType InternalEnumerator_1_t2907_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t2907_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t2907_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2907_MethodInfos/* methods */
	, InternalEnumerator_1_t2907_PropertyInfos/* properties */
	, InternalEnumerator_1_t2907_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2907_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2907_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2907_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2907_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2907_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2907_1_0_0/* this_arg */
	, InternalEnumerator_1_t2907_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2907_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2907_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2907)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7471_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>
extern MethodInfo IList_1_get_Item_m42408_MethodInfo;
extern MethodInfo IList_1_set_Item_m42409_MethodInfo;
static PropertyInfo IList_1_t7471____Item_PropertyInfo = 
{
	&IList_1_t7471_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42408_MethodInfo/* get */
	, &IList_1_set_Item_m42409_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7471_PropertyInfos[] =
{
	&IList_1_t7471____Item_PropertyInfo,
	NULL
};
extern Il2CppType SmartTerrainTrackableBehaviour_t583_0_0_0;
static ParameterInfo IList_1_t7471_IList_1_IndexOf_m42410_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackableBehaviour_t583_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42410_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42410_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7471_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7471_IList_1_IndexOf_m42410_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42410_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SmartTerrainTrackableBehaviour_t583_0_0_0;
static ParameterInfo IList_1_t7471_IList_1_Insert_m42411_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackableBehaviour_t583_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42411_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42411_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7471_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7471_IList_1_Insert_m42411_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42411_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7471_IList_1_RemoveAt_m42412_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42412_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42412_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7471_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7471_IList_1_RemoveAt_m42412_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42412_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7471_IList_1_get_Item_m42408_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType SmartTerrainTrackableBehaviour_t583_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42408_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42408_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7471_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackableBehaviour_t583_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7471_IList_1_get_Item_m42408_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42408_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SmartTerrainTrackableBehaviour_t583_0_0_0;
static ParameterInfo IList_1_t7471_IList_1_set_Item_m42409_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackableBehaviour_t583_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42409_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackableBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42409_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7471_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7471_IList_1_set_Item_m42409_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42409_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7471_MethodInfos[] =
{
	&IList_1_IndexOf_m42410_MethodInfo,
	&IList_1_Insert_m42411_MethodInfo,
	&IList_1_RemoveAt_m42412_MethodInfo,
	&IList_1_get_Item_m42408_MethodInfo,
	&IList_1_set_Item_m42409_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7471_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7470_il2cpp_TypeInfo,
	&IEnumerable_1_t7472_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7471_0_0_0;
extern Il2CppType IList_1_t7471_1_0_0;
struct IList_1_t7471;
extern Il2CppGenericClass IList_1_t7471_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7471_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7471_MethodInfos/* methods */
	, IList_1_t7471_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7471_il2cpp_TypeInfo/* element_class */
	, IList_1_t7471_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7471_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7471_0_0_0/* byval_arg */
	, &IList_1_t7471_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7471_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_31.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2908_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_31MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// Vuforia.PropBehaviour
#include "AssemblyU2DCSharp_Vuforia_PropBehaviour.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_27.h"
extern TypeInfo ObjectU5BU5D_t115_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo PropBehaviour_t39_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t2909_il2cpp_TypeInfo;
extern TypeInfo Void_t99_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_27MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15028_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15030_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2908____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2908_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2908, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2908_FieldInfos[] =
{
	&CachedInvokableCall_1_t2908____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType PropBehaviour_t39_0_0_0;
extern Il2CppType PropBehaviour_t39_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2908_CachedInvokableCall_1__ctor_m15026_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &PropBehaviour_t39_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15026_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15026_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2908_CachedInvokableCall_1__ctor_m15026_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15026_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2908_CachedInvokableCall_1_Invoke_m15027_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15027_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.PropBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15027_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2908_CachedInvokableCall_1_Invoke_m15027_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15027_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2908_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15026_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15027_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m304_MethodInfo;
extern MethodInfo Object_GetHashCode_m305_MethodInfo;
extern MethodInfo Object_ToString_m306_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m15027_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15031_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2908_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15027_MethodInfo,
	&InvokableCall_1_Find_m15031_MethodInfo,
};
extern Il2CppType UnityAction_1_t2910_0_0_0;
extern TypeInfo UnityAction_1_t2910_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisPropBehaviour_t39_m32540_MethodInfo;
extern TypeInfo PropBehaviour_t39_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15033_MethodInfo;
extern TypeInfo PropBehaviour_t39_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2908_RGCTXData[8] = 
{
	&UnityAction_1_t2910_0_0_0/* Type Usage */,
	&UnityAction_1_t2910_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisPropBehaviour_t39_m32540_MethodInfo/* Method Usage */,
	&PropBehaviour_t39_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15033_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15028_MethodInfo/* Method Usage */,
	&PropBehaviour_t39_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15030_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2908_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2908_1_0_0;
struct CachedInvokableCall_1_t2908;
extern Il2CppGenericClass CachedInvokableCall_1_t2908_GenericClass;
TypeInfo CachedInvokableCall_1_t2908_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2908_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2908_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2909_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2908_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2908_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2908_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2908_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2908_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2908_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2908_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2908)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_34.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t2910_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t507_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_34MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6358_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m395_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m456_MethodInfo;
extern MethodInfo Delegate_Combine_m2149_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6357_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2468_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6359_MethodInfo;
extern MethodInfo Delegate_get_Target_m6536_MethodInfo;
extern MethodInfo Delegate_get_Method_m6534_MethodInfo;
struct BaseInvokableCall_t1075;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.PropBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.PropBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisPropBehaviour_t39_m32540(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>
extern Il2CppType UnityAction_1_t2910_0_0_1;
FieldInfo InvokableCall_1_t2909____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2910_0_0_1/* type */
	, &InvokableCall_1_t2909_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2909, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2909_FieldInfos[] =
{
	&InvokableCall_1_t2909____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2909_InvokableCall_1__ctor_m15028_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15028_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15028_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2909_InvokableCall_1__ctor_m15028_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15028_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2910_0_0_0;
static ParameterInfo InvokableCall_1_t2909_InvokableCall_1__ctor_m15029_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2910_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15029_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15029_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2909_InvokableCall_1__ctor_m15029_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15029_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2909_InvokableCall_1_Invoke_m15030_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15030_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15030_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2909_InvokableCall_1_Invoke_m15030_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15030_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2909_InvokableCall_1_Find_m15031_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15031_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15031_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2909_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2909_InvokableCall_1_Find_m15031_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15031_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2909_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15028_MethodInfo,
	&InvokableCall_1__ctor_m15029_MethodInfo,
	&InvokableCall_1_Invoke_m15030_MethodInfo,
	&InvokableCall_1_Find_m15031_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2909_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m15030_MethodInfo,
	&InvokableCall_1_Find_m15031_MethodInfo,
};
extern TypeInfo UnityAction_1_t2910_il2cpp_TypeInfo;
extern TypeInfo PropBehaviour_t39_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2909_RGCTXData[5] = 
{
	&UnityAction_1_t2910_0_0_0/* Type Usage */,
	&UnityAction_1_t2910_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisPropBehaviour_t39_m32540_MethodInfo/* Method Usage */,
	&PropBehaviour_t39_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15033_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2909_0_0_0;
extern Il2CppType InvokableCall_1_t2909_1_0_0;
extern TypeInfo BaseInvokableCall_t1075_il2cpp_TypeInfo;
struct InvokableCall_1_t2909;
extern Il2CppGenericClass InvokableCall_1_t2909_GenericClass;
TypeInfo InvokableCall_1_t2909_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2909_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2909_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2909_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2909_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2909_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2909_0_0_0/* byval_arg */
	, &InvokableCall_1_t2909_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2909_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2909_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2909)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2910_UnityAction_1__ctor_m15032_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15032_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15032_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2910_UnityAction_1__ctor_m15032_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15032_GenericMethod/* genericMethod */

};
extern Il2CppType PropBehaviour_t39_0_0_0;
static ParameterInfo UnityAction_1_t2910_UnityAction_1_Invoke_m15033_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &PropBehaviour_t39_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15033_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15033_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2910_UnityAction_1_Invoke_m15033_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15033_GenericMethod/* genericMethod */

};
extern Il2CppType PropBehaviour_t39_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2910_UnityAction_1_BeginInvoke_m15034_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &PropBehaviour_t39_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15034_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15034_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2910_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2910_UnityAction_1_BeginInvoke_m15034_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15034_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2910_UnityAction_1_EndInvoke_m15035_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15035_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15035_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2910_UnityAction_1_EndInvoke_m15035_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15035_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2910_MethodInfos[] =
{
	&UnityAction_1__ctor_m15032_MethodInfo,
	&UnityAction_1_Invoke_m15033_MethodInfo,
	&UnityAction_1_BeginInvoke_m15034_MethodInfo,
	&UnityAction_1_EndInvoke_m15035_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2241_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2242_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2243_MethodInfo;
extern MethodInfo Delegate_Clone_m2244_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2245_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2246_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2247_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m15034_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15035_MethodInfo;
static MethodInfo* UnityAction_1_t2910_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m15033_MethodInfo,
	&UnityAction_1_BeginInvoke_m15034_MethodInfo,
	&UnityAction_1_EndInvoke_m15035_MethodInfo,
};
extern TypeInfo ICloneable_t481_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t482_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t2910_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2910_1_0_0;
extern TypeInfo MulticastDelegate_t325_il2cpp_TypeInfo;
struct UnityAction_1_t2910;
extern Il2CppGenericClass UnityAction_1_t2910_GenericClass;
TypeInfo UnityAction_1_t2910_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2910_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2910_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2910_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2910_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2910_0_0_0/* byval_arg */
	, &UnityAction_1_t2910_1_0_0/* this_arg */
	, UnityAction_1_t2910_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2910_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2910)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5872_il2cpp_TypeInfo;

// Vuforia.QCARBehaviour
#include "AssemblyU2DCSharp_Vuforia_QCARBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.QCARBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.QCARBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42413_MethodInfo;
static PropertyInfo IEnumerator_1_t5872____Current_PropertyInfo = 
{
	&IEnumerator_1_t5872_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42413_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5872_PropertyInfos[] =
{
	&IEnumerator_1_t5872____Current_PropertyInfo,
	NULL
};
extern Il2CppType QCARBehaviour_t70_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42413_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.QCARBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42413_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5872_il2cpp_TypeInfo/* declaring_type */
	, &QCARBehaviour_t70_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42413_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5872_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42413_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5872_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5872_0_0_0;
extern Il2CppType IEnumerator_1_t5872_1_0_0;
struct IEnumerator_1_t5872;
extern Il2CppGenericClass IEnumerator_1_t5872_GenericClass;
TypeInfo IEnumerator_1_t5872_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5872_MethodInfos/* methods */
	, IEnumerator_1_t5872_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5872_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5872_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5872_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5872_0_0_0/* byval_arg */
	, &IEnumerator_1_t5872_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5872_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_94.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2911_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_94MethodDeclarations.h"

extern TypeInfo QCARBehaviour_t70_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15040_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisQCARBehaviour_t70_m32542_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.QCARBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.QCARBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisQCARBehaviour_t70_m32542(__this, p0, method) (QCARBehaviour_t70 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2911____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2911_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2911, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2911____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2911_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2911, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2911_FieldInfos[] =
{
	&InternalEnumerator_1_t2911____array_0_FieldInfo,
	&InternalEnumerator_1_t2911____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15037_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2911____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2911_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15037_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2911____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2911_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15040_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2911_PropertyInfos[] =
{
	&InternalEnumerator_1_t2911____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2911____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2911_InternalEnumerator_1__ctor_m15036_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15036_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15036_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2911_InternalEnumerator_1__ctor_m15036_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15036_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15037_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15037_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2911_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15037_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15038_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15038_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15038_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15039_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15039_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2911_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15039_GenericMethod/* genericMethod */

};
extern Il2CppType QCARBehaviour_t70_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15040_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.QCARBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15040_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2911_il2cpp_TypeInfo/* declaring_type */
	, &QCARBehaviour_t70_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15040_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2911_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15036_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15037_MethodInfo,
	&InternalEnumerator_1_Dispose_m15038_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15039_MethodInfo,
	&InternalEnumerator_1_get_Current_m15040_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15039_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15038_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2911_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15037_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15039_MethodInfo,
	&InternalEnumerator_1_Dispose_m15038_MethodInfo,
	&InternalEnumerator_1_get_Current_m15040_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2911_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5872_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2911_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5872_il2cpp_TypeInfo, 7},
};
extern TypeInfo QCARBehaviour_t70_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2911_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15040_MethodInfo/* Method Usage */,
	&QCARBehaviour_t70_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisQCARBehaviour_t70_m32542_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2911_0_0_0;
extern Il2CppType InternalEnumerator_1_t2911_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2911_GenericClass;
TypeInfo InternalEnumerator_1_t2911_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2911_MethodInfos/* methods */
	, InternalEnumerator_1_t2911_PropertyInfos/* properties */
	, InternalEnumerator_1_t2911_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2911_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2911_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2911_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2911_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2911_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2911_1_0_0/* this_arg */
	, InternalEnumerator_1_t2911_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2911_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2911_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2911)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7473_il2cpp_TypeInfo;

#include "Assembly-CSharp_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>
extern MethodInfo ICollection_1_get_Count_m42414_MethodInfo;
static PropertyInfo ICollection_1_t7473____Count_PropertyInfo = 
{
	&ICollection_1_t7473_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42414_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42415_MethodInfo;
static PropertyInfo ICollection_1_t7473____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7473_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42415_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7473_PropertyInfos[] =
{
	&ICollection_1_t7473____Count_PropertyInfo,
	&ICollection_1_t7473____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42414_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42414_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7473_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42414_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42415_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42415_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7473_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42415_GenericMethod/* genericMethod */

};
extern Il2CppType QCARBehaviour_t70_0_0_0;
extern Il2CppType QCARBehaviour_t70_0_0_0;
static ParameterInfo ICollection_1_t7473_ICollection_1_Add_m42416_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t70_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42416_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42416_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7473_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7473_ICollection_1_Add_m42416_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42416_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42417_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42417_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7473_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42417_GenericMethod/* genericMethod */

};
extern Il2CppType QCARBehaviour_t70_0_0_0;
static ParameterInfo ICollection_1_t7473_ICollection_1_Contains_m42418_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t70_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42418_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42418_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7473_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7473_ICollection_1_Contains_m42418_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42418_GenericMethod/* genericMethod */

};
extern Il2CppType QCARBehaviourU5BU5D_t5186_0_0_0;
extern Il2CppType QCARBehaviourU5BU5D_t5186_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7473_ICollection_1_CopyTo_m42419_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &QCARBehaviourU5BU5D_t5186_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42419_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42419_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7473_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7473_ICollection_1_CopyTo_m42419_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42419_GenericMethod/* genericMethod */

};
extern Il2CppType QCARBehaviour_t70_0_0_0;
static ParameterInfo ICollection_1_t7473_ICollection_1_Remove_m42420_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t70_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42420_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42420_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7473_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7473_ICollection_1_Remove_m42420_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42420_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7473_MethodInfos[] =
{
	&ICollection_1_get_Count_m42414_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42415_MethodInfo,
	&ICollection_1_Add_m42416_MethodInfo,
	&ICollection_1_Clear_m42417_MethodInfo,
	&ICollection_1_Contains_m42418_MethodInfo,
	&ICollection_1_CopyTo_m42419_MethodInfo,
	&ICollection_1_Remove_m42420_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7475_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7473_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7475_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7473_0_0_0;
extern Il2CppType ICollection_1_t7473_1_0_0;
struct ICollection_1_t7473;
extern Il2CppGenericClass ICollection_1_t7473_GenericClass;
TypeInfo ICollection_1_t7473_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7473_MethodInfos/* methods */
	, ICollection_1_t7473_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7473_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7473_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7473_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7473_0_0_0/* byval_arg */
	, &ICollection_1_t7473_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7473_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.QCARBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.QCARBehaviour>
extern Il2CppType IEnumerator_1_t5872_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42421_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.QCARBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42421_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7475_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5872_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42421_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7475_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42421_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7475_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7475_0_0_0;
extern Il2CppType IEnumerable_1_t7475_1_0_0;
struct IEnumerable_1_t7475;
extern Il2CppGenericClass IEnumerable_1_t7475_GenericClass;
TypeInfo IEnumerable_1_t7475_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7475_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7475_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7475_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7475_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7475_0_0_0/* byval_arg */
	, &IEnumerable_1_t7475_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7475_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7474_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>
extern MethodInfo IList_1_get_Item_m42422_MethodInfo;
extern MethodInfo IList_1_set_Item_m42423_MethodInfo;
static PropertyInfo IList_1_t7474____Item_PropertyInfo = 
{
	&IList_1_t7474_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42422_MethodInfo/* get */
	, &IList_1_set_Item_m42423_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7474_PropertyInfos[] =
{
	&IList_1_t7474____Item_PropertyInfo,
	NULL
};
extern Il2CppType QCARBehaviour_t70_0_0_0;
static ParameterInfo IList_1_t7474_IList_1_IndexOf_m42424_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t70_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42424_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42424_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7474_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7474_IList_1_IndexOf_m42424_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42424_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType QCARBehaviour_t70_0_0_0;
static ParameterInfo IList_1_t7474_IList_1_Insert_m42425_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t70_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42425_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42425_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7474_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7474_IList_1_Insert_m42425_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42425_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7474_IList_1_RemoveAt_m42426_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42426_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42426_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7474_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7474_IList_1_RemoveAt_m42426_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42426_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7474_IList_1_get_Item_m42422_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType QCARBehaviour_t70_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42422_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42422_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7474_il2cpp_TypeInfo/* declaring_type */
	, &QCARBehaviour_t70_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7474_IList_1_get_Item_m42422_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42422_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType QCARBehaviour_t70_0_0_0;
static ParameterInfo IList_1_t7474_IList_1_set_Item_m42423_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t70_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42423_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42423_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7474_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7474_IList_1_set_Item_m42423_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42423_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7474_MethodInfos[] =
{
	&IList_1_IndexOf_m42424_MethodInfo,
	&IList_1_Insert_m42425_MethodInfo,
	&IList_1_RemoveAt_m42426_MethodInfo,
	&IList_1_get_Item_m42422_MethodInfo,
	&IList_1_set_Item_m42423_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7474_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7473_il2cpp_TypeInfo,
	&IEnumerable_1_t7475_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7474_0_0_0;
extern Il2CppType IList_1_t7474_1_0_0;
struct IList_1_t7474;
extern Il2CppGenericClass IList_1_t7474_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7474_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7474_MethodInfos/* methods */
	, IList_1_t7474_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7474_il2cpp_TypeInfo/* element_class */
	, IList_1_t7474_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7474_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7474_0_0_0/* byval_arg */
	, &IList_1_t7474_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7474_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_32.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2912_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_32MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_28.h"
extern TypeInfo InvokableCall_1_t2913_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_28MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15043_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15045_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2912____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2912_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2912, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2912_FieldInfos[] =
{
	&CachedInvokableCall_1_t2912____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType QCARBehaviour_t70_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2912_CachedInvokableCall_1__ctor_m15041_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t70_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15041_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15041_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2912_CachedInvokableCall_1__ctor_m15041_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15041_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2912_CachedInvokableCall_1_Invoke_m15042_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15042_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15042_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2912_CachedInvokableCall_1_Invoke_m15042_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15042_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2912_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15041_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15042_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15042_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15046_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2912_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15042_MethodInfo,
	&InvokableCall_1_Find_m15046_MethodInfo,
};
extern Il2CppType UnityAction_1_t2914_0_0_0;
extern TypeInfo UnityAction_1_t2914_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisQCARBehaviour_t70_m32552_MethodInfo;
extern TypeInfo QCARBehaviour_t70_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15048_MethodInfo;
extern TypeInfo QCARBehaviour_t70_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2912_RGCTXData[8] = 
{
	&UnityAction_1_t2914_0_0_0/* Type Usage */,
	&UnityAction_1_t2914_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisQCARBehaviour_t70_m32552_MethodInfo/* Method Usage */,
	&QCARBehaviour_t70_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15048_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15043_MethodInfo/* Method Usage */,
	&QCARBehaviour_t70_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15045_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2912_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2912_1_0_0;
struct CachedInvokableCall_1_t2912;
extern Il2CppGenericClass CachedInvokableCall_1_t2912_GenericClass;
TypeInfo CachedInvokableCall_1_t2912_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2912_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2912_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2913_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2912_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2912_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2912_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2912_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2912_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2912_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2912_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2912)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_35.h"
extern TypeInfo UnityAction_1_t2914_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_35MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.QCARBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.QCARBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisQCARBehaviour_t70_m32552(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>
extern Il2CppType UnityAction_1_t2914_0_0_1;
FieldInfo InvokableCall_1_t2913____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2914_0_0_1/* type */
	, &InvokableCall_1_t2913_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2913, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2913_FieldInfos[] =
{
	&InvokableCall_1_t2913____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2913_InvokableCall_1__ctor_m15043_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15043_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15043_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2913_InvokableCall_1__ctor_m15043_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15043_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2914_0_0_0;
static ParameterInfo InvokableCall_1_t2913_InvokableCall_1__ctor_m15044_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2914_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15044_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15044_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2913_InvokableCall_1__ctor_m15044_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15044_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2913_InvokableCall_1_Invoke_m15045_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15045_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15045_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2913_InvokableCall_1_Invoke_m15045_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15045_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2913_InvokableCall_1_Find_m15046_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15046_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.QCARBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15046_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2913_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2913_InvokableCall_1_Find_m15046_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15046_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2913_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15043_MethodInfo,
	&InvokableCall_1__ctor_m15044_MethodInfo,
	&InvokableCall_1_Invoke_m15045_MethodInfo,
	&InvokableCall_1_Find_m15046_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2913_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m15045_MethodInfo,
	&InvokableCall_1_Find_m15046_MethodInfo,
};
extern TypeInfo UnityAction_1_t2914_il2cpp_TypeInfo;
extern TypeInfo QCARBehaviour_t70_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2913_RGCTXData[5] = 
{
	&UnityAction_1_t2914_0_0_0/* Type Usage */,
	&UnityAction_1_t2914_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisQCARBehaviour_t70_m32552_MethodInfo/* Method Usage */,
	&QCARBehaviour_t70_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15048_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2913_0_0_0;
extern Il2CppType InvokableCall_1_t2913_1_0_0;
struct InvokableCall_1_t2913;
extern Il2CppGenericClass InvokableCall_1_t2913_GenericClass;
TypeInfo InvokableCall_1_t2913_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2913_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2913_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2913_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2913_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2913_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2913_0_0_0/* byval_arg */
	, &InvokableCall_1_t2913_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2913_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2913_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2913)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2914_UnityAction_1__ctor_m15047_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15047_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15047_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2914_UnityAction_1__ctor_m15047_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15047_GenericMethod/* genericMethod */

};
extern Il2CppType QCARBehaviour_t70_0_0_0;
static ParameterInfo UnityAction_1_t2914_UnityAction_1_Invoke_m15048_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t70_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15048_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15048_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2914_UnityAction_1_Invoke_m15048_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15048_GenericMethod/* genericMethod */

};
extern Il2CppType QCARBehaviour_t70_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2914_UnityAction_1_BeginInvoke_m15049_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &QCARBehaviour_t70_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15049_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15049_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2914_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2914_UnityAction_1_BeginInvoke_m15049_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15049_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2914_UnityAction_1_EndInvoke_m15050_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15050_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.QCARBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15050_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2914_UnityAction_1_EndInvoke_m15050_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15050_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2914_MethodInfos[] =
{
	&UnityAction_1__ctor_m15047_MethodInfo,
	&UnityAction_1_Invoke_m15048_MethodInfo,
	&UnityAction_1_BeginInvoke_m15049_MethodInfo,
	&UnityAction_1_EndInvoke_m15050_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15049_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15050_MethodInfo;
static MethodInfo* UnityAction_1_t2914_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m15048_MethodInfo,
	&UnityAction_1_BeginInvoke_m15049_MethodInfo,
	&UnityAction_1_EndInvoke_m15050_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2914_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2914_1_0_0;
struct UnityAction_1_t2914;
extern Il2CppGenericClass UnityAction_1_t2914_GenericClass;
TypeInfo UnityAction_1_t2914_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2914_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2914_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2914_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2914_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2914_0_0_0/* byval_arg */
	, &UnityAction_1_t2914_1_0_0/* this_arg */
	, UnityAction_1_t2914_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2914_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2914)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5874_il2cpp_TypeInfo;

// Vuforia.ReconstructionBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42427_MethodInfo;
static PropertyInfo IEnumerator_1_t5874____Current_PropertyInfo = 
{
	&IEnumerator_1_t5874_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42427_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5874_PropertyInfos[] =
{
	&IEnumerator_1_t5874____Current_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionBehaviour_t38_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42427_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42427_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5874_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionBehaviour_t38_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42427_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5874_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42427_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5874_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5874_0_0_0;
extern Il2CppType IEnumerator_1_t5874_1_0_0;
struct IEnumerator_1_t5874;
extern Il2CppGenericClass IEnumerator_1_t5874_GenericClass;
TypeInfo IEnumerator_1_t5874_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5874_MethodInfos/* methods */
	, IEnumerator_1_t5874_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5874_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5874_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5874_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5874_0_0_0/* byval_arg */
	, &IEnumerator_1_t5874_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5874_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_95.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2915_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_95MethodDeclarations.h"

extern TypeInfo ReconstructionBehaviour_t38_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15055_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisReconstructionBehaviour_t38_m32554_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisReconstructionBehaviour_t38_m32554(__this, p0, method) (ReconstructionBehaviour_t38 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2915____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2915, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2915____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2915, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2915_FieldInfos[] =
{
	&InternalEnumerator_1_t2915____array_0_FieldInfo,
	&InternalEnumerator_1_t2915____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15052_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2915____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2915_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15052_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2915____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2915_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15055_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2915_PropertyInfos[] =
{
	&InternalEnumerator_1_t2915____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2915____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2915_InternalEnumerator_1__ctor_m15051_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15051_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15051_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2915_InternalEnumerator_1__ctor_m15051_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15051_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15052_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15052_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15052_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15053_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15053_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15053_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15054_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15054_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15054_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionBehaviour_t38_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15055_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15055_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionBehaviour_t38_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15055_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2915_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15051_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15052_MethodInfo,
	&InternalEnumerator_1_Dispose_m15053_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15054_MethodInfo,
	&InternalEnumerator_1_get_Current_m15055_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15054_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15053_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2915_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15052_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15054_MethodInfo,
	&InternalEnumerator_1_Dispose_m15053_MethodInfo,
	&InternalEnumerator_1_get_Current_m15055_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2915_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5874_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2915_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5874_il2cpp_TypeInfo, 7},
};
extern TypeInfo ReconstructionBehaviour_t38_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2915_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15055_MethodInfo/* Method Usage */,
	&ReconstructionBehaviour_t38_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisReconstructionBehaviour_t38_m32554_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2915_0_0_0;
extern Il2CppType InternalEnumerator_1_t2915_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2915_GenericClass;
TypeInfo InternalEnumerator_1_t2915_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2915_MethodInfos/* methods */
	, InternalEnumerator_1_t2915_PropertyInfos/* properties */
	, InternalEnumerator_1_t2915_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2915_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2915_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2915_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2915_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2915_1_0_0/* this_arg */
	, InternalEnumerator_1_t2915_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2915_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2915_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2915)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7476_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>
extern MethodInfo ICollection_1_get_Count_m42428_MethodInfo;
static PropertyInfo ICollection_1_t7476____Count_PropertyInfo = 
{
	&ICollection_1_t7476_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42428_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42429_MethodInfo;
static PropertyInfo ICollection_1_t7476____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7476_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42429_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7476_PropertyInfos[] =
{
	&ICollection_1_t7476____Count_PropertyInfo,
	&ICollection_1_t7476____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42428_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42428_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7476_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42428_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42429_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42429_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7476_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42429_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionBehaviour_t38_0_0_0;
extern Il2CppType ReconstructionBehaviour_t38_0_0_0;
static ParameterInfo ICollection_1_t7476_ICollection_1_Add_m42430_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t38_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42430_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42430_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7476_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7476_ICollection_1_Add_m42430_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42430_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42431_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42431_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7476_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42431_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionBehaviour_t38_0_0_0;
static ParameterInfo ICollection_1_t7476_ICollection_1_Contains_m42432_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t38_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42432_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42432_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7476_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7476_ICollection_1_Contains_m42432_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42432_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionBehaviourU5BU5D_t5187_0_0_0;
extern Il2CppType ReconstructionBehaviourU5BU5D_t5187_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7476_ICollection_1_CopyTo_m42433_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviourU5BU5D_t5187_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42433_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42433_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7476_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7476_ICollection_1_CopyTo_m42433_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42433_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionBehaviour_t38_0_0_0;
static ParameterInfo ICollection_1_t7476_ICollection_1_Remove_m42434_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t38_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42434_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42434_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7476_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7476_ICollection_1_Remove_m42434_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42434_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7476_MethodInfos[] =
{
	&ICollection_1_get_Count_m42428_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42429_MethodInfo,
	&ICollection_1_Add_m42430_MethodInfo,
	&ICollection_1_Clear_m42431_MethodInfo,
	&ICollection_1_Contains_m42432_MethodInfo,
	&ICollection_1_CopyTo_m42433_MethodInfo,
	&ICollection_1_Remove_m42434_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7478_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7476_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7478_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7476_0_0_0;
extern Il2CppType ICollection_1_t7476_1_0_0;
struct ICollection_1_t7476;
extern Il2CppGenericClass ICollection_1_t7476_GenericClass;
TypeInfo ICollection_1_t7476_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7476_MethodInfos/* methods */
	, ICollection_1_t7476_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7476_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7476_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7476_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7476_0_0_0/* byval_arg */
	, &ICollection_1_t7476_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7476_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionBehaviour>
extern Il2CppType IEnumerator_1_t5874_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42435_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42435_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7478_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5874_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42435_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7478_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42435_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7478_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7478_0_0_0;
extern Il2CppType IEnumerable_1_t7478_1_0_0;
struct IEnumerable_1_t7478;
extern Il2CppGenericClass IEnumerable_1_t7478_GenericClass;
TypeInfo IEnumerable_1_t7478_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7478_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7478_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7478_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7478_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7478_0_0_0/* byval_arg */
	, &IEnumerable_1_t7478_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7478_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7477_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>
extern MethodInfo IList_1_get_Item_m42436_MethodInfo;
extern MethodInfo IList_1_set_Item_m42437_MethodInfo;
static PropertyInfo IList_1_t7477____Item_PropertyInfo = 
{
	&IList_1_t7477_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42436_MethodInfo/* get */
	, &IList_1_set_Item_m42437_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7477_PropertyInfos[] =
{
	&IList_1_t7477____Item_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionBehaviour_t38_0_0_0;
static ParameterInfo IList_1_t7477_IList_1_IndexOf_m42438_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t38_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42438_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42438_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7477_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7477_IList_1_IndexOf_m42438_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42438_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ReconstructionBehaviour_t38_0_0_0;
static ParameterInfo IList_1_t7477_IList_1_Insert_m42439_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t38_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42439_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42439_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7477_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7477_IList_1_Insert_m42439_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42439_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7477_IList_1_RemoveAt_m42440_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42440_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42440_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7477_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7477_IList_1_RemoveAt_m42440_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42440_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7477_IList_1_get_Item_m42436_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ReconstructionBehaviour_t38_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42436_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42436_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7477_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionBehaviour_t38_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7477_IList_1_get_Item_m42436_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42436_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ReconstructionBehaviour_t38_0_0_0;
static ParameterInfo IList_1_t7477_IList_1_set_Item_m42437_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t38_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42437_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42437_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7477_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7477_IList_1_set_Item_m42437_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42437_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7477_MethodInfos[] =
{
	&IList_1_IndexOf_m42438_MethodInfo,
	&IList_1_Insert_m42439_MethodInfo,
	&IList_1_RemoveAt_m42440_MethodInfo,
	&IList_1_get_Item_m42436_MethodInfo,
	&IList_1_set_Item_m42437_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7477_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7476_il2cpp_TypeInfo,
	&IEnumerable_1_t7478_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7477_0_0_0;
extern Il2CppType IList_1_t7477_1_0_0;
struct IList_1_t7477;
extern Il2CppGenericClass IList_1_t7477_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7477_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7477_MethodInfos/* methods */
	, IList_1_t7477_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7477_il2cpp_TypeInfo/* element_class */
	, IList_1_t7477_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7477_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7477_0_0_0/* byval_arg */
	, &IList_1_t7477_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7477_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t3927_il2cpp_TypeInfo;

// Vuforia.ReconstructionAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstr.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m35607_MethodInfo;
static PropertyInfo ICollection_1_t3927____Count_PropertyInfo = 
{
	&ICollection_1_t3927_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m35607_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42441_MethodInfo;
static PropertyInfo ICollection_1_t3927____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t3927_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42441_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t3927_PropertyInfos[] =
{
	&ICollection_1_t3927____Count_PropertyInfo,
	&ICollection_1_t3927____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m35607_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m35607_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t3927_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m35607_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42441_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42441_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t3927_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42441_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionAbstractBehaviour_t72_0_0_0;
extern Il2CppType ReconstructionAbstractBehaviour_t72_0_0_0;
static ParameterInfo ICollection_1_t3927_ICollection_1_Add_m42442_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionAbstractBehaviour_t72_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42442_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42442_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t3927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t3927_ICollection_1_Add_m42442_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42442_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42443_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42443_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t3927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42443_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionAbstractBehaviour_t72_0_0_0;
static ParameterInfo ICollection_1_t3927_ICollection_1_Contains_m35057_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionAbstractBehaviour_t72_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m35057_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m35057_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t3927_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t3927_ICollection_1_Contains_m35057_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m35057_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionAbstractBehaviourU5BU5D_t821_0_0_0;
extern Il2CppType ReconstructionAbstractBehaviourU5BU5D_t821_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t3927_ICollection_1_CopyTo_m42444_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionAbstractBehaviourU5BU5D_t821_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42444_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42444_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t3927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t3927_ICollection_1_CopyTo_m42444_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42444_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionAbstractBehaviour_t72_0_0_0;
static ParameterInfo ICollection_1_t3927_ICollection_1_Remove_m42445_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionAbstractBehaviour_t72_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42445_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42445_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t3927_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t3927_ICollection_1_Remove_m42445_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42445_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t3927_MethodInfos[] =
{
	&ICollection_1_get_Count_m35607_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42441_MethodInfo,
	&ICollection_1_Add_m42442_MethodInfo,
	&ICollection_1_Clear_m42443_MethodInfo,
	&ICollection_1_Contains_m35057_MethodInfo,
	&ICollection_1_CopyTo_m42444_MethodInfo,
	&ICollection_1_Remove_m42445_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t575_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t3927_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t575_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t3927_0_0_0;
extern Il2CppType ICollection_1_t3927_1_0_0;
struct ICollection_1_t3927;
extern Il2CppGenericClass ICollection_1_t3927_GenericClass;
TypeInfo ICollection_1_t3927_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t3927_MethodInfos/* methods */
	, ICollection_1_t3927_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t3927_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t3927_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t3927_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t3927_0_0_0/* byval_arg */
	, &ICollection_1_t3927_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t3927_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour>
extern Il2CppType IEnumerator_1_t785_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m4448_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m4448_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t575_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t785_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m4448_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t575_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m4448_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t575_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t575_0_0_0;
extern Il2CppType IEnumerable_1_t575_1_0_0;
struct IEnumerable_1_t575;
extern Il2CppGenericClass IEnumerable_1_t575_GenericClass;
TypeInfo IEnumerable_1_t575_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t575_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t575_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t575_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t575_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t575_0_0_0/* byval_arg */
	, &IEnumerable_1_t575_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t575_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t785_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m4449_MethodInfo;
static PropertyInfo IEnumerator_1_t785____Current_PropertyInfo = 
{
	&IEnumerator_1_t785_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m4449_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t785_PropertyInfos[] =
{
	&IEnumerator_1_t785____Current_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionAbstractBehaviour_t72_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m4449_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m4449_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t785_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionAbstractBehaviour_t72_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m4449_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t785_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m4449_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t785_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t785_0_0_0;
extern Il2CppType IEnumerator_1_t785_1_0_0;
struct IEnumerator_1_t785;
extern Il2CppGenericClass IEnumerator_1_t785_GenericClass;
TypeInfo IEnumerator_1_t785_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t785_MethodInfos/* methods */
	, IEnumerator_1_t785_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t785_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t785_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t785_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t785_0_0_0/* byval_arg */
	, &IEnumerator_1_t785_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t785_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_96.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2916_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_96MethodDeclarations.h"

extern TypeInfo ReconstructionAbstractBehaviour_t72_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15060_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisReconstructionAbstractBehaviour_t72_m32565_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisReconstructionAbstractBehaviour_t72_m32565(__this, p0, method) (ReconstructionAbstractBehaviour_t72 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2916____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2916, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2916____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2916, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2916_FieldInfos[] =
{
	&InternalEnumerator_1_t2916____array_0_FieldInfo,
	&InternalEnumerator_1_t2916____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15057_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2916____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2916_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15057_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2916____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2916_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15060_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2916_PropertyInfos[] =
{
	&InternalEnumerator_1_t2916____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2916____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2916_InternalEnumerator_1__ctor_m15056_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15056_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15056_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2916_InternalEnumerator_1__ctor_m15056_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15056_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15057_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15057_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15057_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15058_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15058_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15058_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15059_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15059_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15059_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionAbstractBehaviour_t72_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15060_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15060_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionAbstractBehaviour_t72_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15060_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2916_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15056_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15057_MethodInfo,
	&InternalEnumerator_1_Dispose_m15058_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15059_MethodInfo,
	&InternalEnumerator_1_get_Current_m15060_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15059_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15058_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2916_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15057_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15059_MethodInfo,
	&InternalEnumerator_1_Dispose_m15058_MethodInfo,
	&InternalEnumerator_1_get_Current_m15060_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2916_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t785_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2916_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t785_il2cpp_TypeInfo, 7},
};
extern TypeInfo ReconstructionAbstractBehaviour_t72_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2916_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15060_MethodInfo/* Method Usage */,
	&ReconstructionAbstractBehaviour_t72_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisReconstructionAbstractBehaviour_t72_m32565_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2916_0_0_0;
extern Il2CppType InternalEnumerator_1_t2916_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2916_GenericClass;
TypeInfo InternalEnumerator_1_t2916_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2916_MethodInfos/* methods */
	, InternalEnumerator_1_t2916_PropertyInfos/* properties */
	, InternalEnumerator_1_t2916_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2916_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2916_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2916_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2916_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2916_1_0_0/* this_arg */
	, InternalEnumerator_1_t2916_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2916_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2916_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2916)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t3931_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42446_MethodInfo;
extern MethodInfo IList_1_set_Item_m42447_MethodInfo;
static PropertyInfo IList_1_t3931____Item_PropertyInfo = 
{
	&IList_1_t3931_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42446_MethodInfo/* get */
	, &IList_1_set_Item_m42447_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t3931_PropertyInfos[] =
{
	&IList_1_t3931____Item_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionAbstractBehaviour_t72_0_0_0;
static ParameterInfo IList_1_t3931_IList_1_IndexOf_m42448_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionAbstractBehaviour_t72_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42448_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42448_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t3931_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t3931_IList_1_IndexOf_m42448_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42448_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ReconstructionAbstractBehaviour_t72_0_0_0;
static ParameterInfo IList_1_t3931_IList_1_Insert_m42449_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionAbstractBehaviour_t72_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42449_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42449_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t3931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t3931_IList_1_Insert_m42449_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42449_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t3931_IList_1_RemoveAt_m42450_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42450_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42450_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t3931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t3931_IList_1_RemoveAt_m42450_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42450_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t3931_IList_1_get_Item_m42446_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ReconstructionAbstractBehaviour_t72_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42446_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42446_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t3931_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionAbstractBehaviour_t72_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t3931_IList_1_get_Item_m42446_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42446_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ReconstructionAbstractBehaviour_t72_0_0_0;
static ParameterInfo IList_1_t3931_IList_1_set_Item_m42447_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionAbstractBehaviour_t72_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42447_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42447_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t3931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t3931_IList_1_set_Item_m42447_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42447_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t3931_MethodInfos[] =
{
	&IList_1_IndexOf_m42448_MethodInfo,
	&IList_1_Insert_m42449_MethodInfo,
	&IList_1_RemoveAt_m42450_MethodInfo,
	&IList_1_get_Item_m42446_MethodInfo,
	&IList_1_set_Item_m42447_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t3931_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t3927_il2cpp_TypeInfo,
	&IEnumerable_1_t575_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t3931_0_0_0;
extern Il2CppType IList_1_t3931_1_0_0;
struct IList_1_t3931;
extern Il2CppGenericClass IList_1_t3931_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t3931_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t3931_MethodInfos/* methods */
	, IList_1_t3931_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t3931_il2cpp_TypeInfo/* element_class */
	, IList_1_t3931_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t3931_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t3931_0_0_0/* byval_arg */
	, &IList_1_t3931_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t3931_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7479_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>
extern MethodInfo ICollection_1_get_Count_m42451_MethodInfo;
static PropertyInfo ICollection_1_t7479____Count_PropertyInfo = 
{
	&ICollection_1_t7479_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42451_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42452_MethodInfo;
static PropertyInfo ICollection_1_t7479____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7479_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42452_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7479_PropertyInfos[] =
{
	&ICollection_1_t7479____Count_PropertyInfo,
	&ICollection_1_t7479____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42451_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42451_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7479_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42451_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42452_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42452_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7479_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42452_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorReconstructionBehaviour_t164_0_0_0;
extern Il2CppType IEditorReconstructionBehaviour_t164_0_0_0;
static ParameterInfo ICollection_1_t7479_ICollection_1_Add_m42453_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorReconstructionBehaviour_t164_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42453_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42453_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7479_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7479_ICollection_1_Add_m42453_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42453_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42454_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42454_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7479_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42454_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorReconstructionBehaviour_t164_0_0_0;
static ParameterInfo ICollection_1_t7479_ICollection_1_Contains_m42455_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorReconstructionBehaviour_t164_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42455_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42455_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7479_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7479_ICollection_1_Contains_m42455_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42455_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorReconstructionBehaviourU5BU5D_t5512_0_0_0;
extern Il2CppType IEditorReconstructionBehaviourU5BU5D_t5512_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7479_ICollection_1_CopyTo_m42456_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorReconstructionBehaviourU5BU5D_t5512_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42456_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42456_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7479_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7479_ICollection_1_CopyTo_m42456_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42456_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorReconstructionBehaviour_t164_0_0_0;
static ParameterInfo ICollection_1_t7479_ICollection_1_Remove_m42457_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorReconstructionBehaviour_t164_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42457_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorReconstructionBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42457_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7479_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7479_ICollection_1_Remove_m42457_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42457_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7479_MethodInfos[] =
{
	&ICollection_1_get_Count_m42451_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42452_MethodInfo,
	&ICollection_1_Add_m42453_MethodInfo,
	&ICollection_1_Clear_m42454_MethodInfo,
	&ICollection_1_Contains_m42455_MethodInfo,
	&ICollection_1_CopyTo_m42456_MethodInfo,
	&ICollection_1_Remove_m42457_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7481_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7479_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7481_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7479_0_0_0;
extern Il2CppType ICollection_1_t7479_1_0_0;
struct ICollection_1_t7479;
extern Il2CppGenericClass ICollection_1_t7479_GenericClass;
TypeInfo ICollection_1_t7479_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7479_MethodInfos/* methods */
	, ICollection_1_t7479_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7479_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7479_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7479_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7479_0_0_0/* byval_arg */
	, &ICollection_1_t7479_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7479_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorReconstructionBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorReconstructionBehaviour>
extern Il2CppType IEnumerator_1_t5877_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42458_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorReconstructionBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42458_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7481_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5877_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42458_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7481_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42458_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7481_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7481_0_0_0;
extern Il2CppType IEnumerable_1_t7481_1_0_0;
struct IEnumerable_1_t7481;
extern Il2CppGenericClass IEnumerable_1_t7481_GenericClass;
TypeInfo IEnumerable_1_t7481_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7481_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7481_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7481_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7481_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7481_0_0_0/* byval_arg */
	, &IEnumerable_1_t7481_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7481_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5877_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorReconstructionBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42459_MethodInfo;
static PropertyInfo IEnumerator_1_t5877____Current_PropertyInfo = 
{
	&IEnumerator_1_t5877_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42459_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5877_PropertyInfos[] =
{
	&IEnumerator_1_t5877____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorReconstructionBehaviour_t164_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42459_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42459_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5877_il2cpp_TypeInfo/* declaring_type */
	, &IEditorReconstructionBehaviour_t164_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42459_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5877_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42459_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5877_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5877_0_0_0;
extern Il2CppType IEnumerator_1_t5877_1_0_0;
struct IEnumerator_1_t5877;
extern Il2CppGenericClass IEnumerator_1_t5877_GenericClass;
TypeInfo IEnumerator_1_t5877_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5877_MethodInfos/* methods */
	, IEnumerator_1_t5877_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5877_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5877_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5877_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5877_0_0_0/* byval_arg */
	, &IEnumerator_1_t5877_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5877_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_97.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2917_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_97MethodDeclarations.h"

extern TypeInfo IEditorReconstructionBehaviour_t164_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15065_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorReconstructionBehaviour_t164_m32576_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorReconstructionBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorReconstructionBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorReconstructionBehaviour_t164_m32576(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2917____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2917_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2917, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2917____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2917_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2917, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2917_FieldInfos[] =
{
	&InternalEnumerator_1_t2917____array_0_FieldInfo,
	&InternalEnumerator_1_t2917____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15062_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2917____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2917_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15062_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2917____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2917_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15065_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2917_PropertyInfos[] =
{
	&InternalEnumerator_1_t2917____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2917____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2917_InternalEnumerator_1__ctor_m15061_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15061_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15061_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2917_InternalEnumerator_1__ctor_m15061_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15061_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15062_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15062_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2917_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15062_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15063_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15063_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15063_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15064_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15064_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2917_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15064_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorReconstructionBehaviour_t164_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15065_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorReconstructionBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15065_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2917_il2cpp_TypeInfo/* declaring_type */
	, &IEditorReconstructionBehaviour_t164_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15065_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2917_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15061_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15062_MethodInfo,
	&InternalEnumerator_1_Dispose_m15063_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15064_MethodInfo,
	&InternalEnumerator_1_get_Current_m15065_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15064_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15063_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2917_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15062_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15064_MethodInfo,
	&InternalEnumerator_1_Dispose_m15063_MethodInfo,
	&InternalEnumerator_1_get_Current_m15065_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2917_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5877_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2917_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5877_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorReconstructionBehaviour_t164_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2917_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15065_MethodInfo/* Method Usage */,
	&IEditorReconstructionBehaviour_t164_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorReconstructionBehaviour_t164_m32576_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2917_0_0_0;
extern Il2CppType InternalEnumerator_1_t2917_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2917_GenericClass;
TypeInfo InternalEnumerator_1_t2917_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2917_MethodInfos/* methods */
	, InternalEnumerator_1_t2917_PropertyInfos/* properties */
	, InternalEnumerator_1_t2917_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2917_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2917_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2917_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2917_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2917_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2917_1_0_0/* this_arg */
	, InternalEnumerator_1_t2917_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2917_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2917_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2917)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7480_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>
extern MethodInfo IList_1_get_Item_m42460_MethodInfo;
extern MethodInfo IList_1_set_Item_m42461_MethodInfo;
static PropertyInfo IList_1_t7480____Item_PropertyInfo = 
{
	&IList_1_t7480_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42460_MethodInfo/* get */
	, &IList_1_set_Item_m42461_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7480_PropertyInfos[] =
{
	&IList_1_t7480____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorReconstructionBehaviour_t164_0_0_0;
static ParameterInfo IList_1_t7480_IList_1_IndexOf_m42462_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorReconstructionBehaviour_t164_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42462_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42462_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7480_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7480_IList_1_IndexOf_m42462_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42462_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorReconstructionBehaviour_t164_0_0_0;
static ParameterInfo IList_1_t7480_IList_1_Insert_m42463_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorReconstructionBehaviour_t164_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42463_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42463_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7480_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7480_IList_1_Insert_m42463_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42463_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7480_IList_1_RemoveAt_m42464_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42464_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42464_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7480_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7480_IList_1_RemoveAt_m42464_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42464_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7480_IList_1_get_Item_m42460_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEditorReconstructionBehaviour_t164_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42460_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42460_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7480_il2cpp_TypeInfo/* declaring_type */
	, &IEditorReconstructionBehaviour_t164_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7480_IList_1_get_Item_m42460_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42460_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorReconstructionBehaviour_t164_0_0_0;
static ParameterInfo IList_1_t7480_IList_1_set_Item_m42461_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorReconstructionBehaviour_t164_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42461_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorReconstructionBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42461_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7480_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7480_IList_1_set_Item_m42461_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42461_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7480_MethodInfos[] =
{
	&IList_1_IndexOf_m42462_MethodInfo,
	&IList_1_Insert_m42463_MethodInfo,
	&IList_1_RemoveAt_m42464_MethodInfo,
	&IList_1_get_Item_m42460_MethodInfo,
	&IList_1_set_Item_m42461_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7480_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7479_il2cpp_TypeInfo,
	&IEnumerable_1_t7481_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7480_0_0_0;
extern Il2CppType IList_1_t7480_1_0_0;
struct IList_1_t7480;
extern Il2CppGenericClass IList_1_t7480_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7480_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7480_MethodInfos/* methods */
	, IList_1_t7480_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7480_il2cpp_TypeInfo/* element_class */
	, IList_1_t7480_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7480_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7480_0_0_0/* byval_arg */
	, &IList_1_t7480_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7480_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_33.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2918_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_33MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_29.h"
extern TypeInfo InvokableCall_1_t2919_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_29MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15068_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15070_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2918____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2918_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2918, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2918_FieldInfos[] =
{
	&CachedInvokableCall_1_t2918____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType ReconstructionBehaviour_t38_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2918_CachedInvokableCall_1__ctor_m15066_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t38_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15066_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15066_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2918_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2918_CachedInvokableCall_1__ctor_m15066_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15066_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2918_CachedInvokableCall_1_Invoke_m15067_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15067_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15067_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2918_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2918_CachedInvokableCall_1_Invoke_m15067_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15067_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2918_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15066_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15067_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15067_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15071_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2918_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15067_MethodInfo,
	&InvokableCall_1_Find_m15071_MethodInfo,
};
extern Il2CppType UnityAction_1_t2920_0_0_0;
extern TypeInfo UnityAction_1_t2920_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionBehaviour_t38_m32586_MethodInfo;
extern TypeInfo ReconstructionBehaviour_t38_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15073_MethodInfo;
extern TypeInfo ReconstructionBehaviour_t38_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2918_RGCTXData[8] = 
{
	&UnityAction_1_t2920_0_0_0/* Type Usage */,
	&UnityAction_1_t2920_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionBehaviour_t38_m32586_MethodInfo/* Method Usage */,
	&ReconstructionBehaviour_t38_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15073_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15068_MethodInfo/* Method Usage */,
	&ReconstructionBehaviour_t38_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15070_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2918_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2918_1_0_0;
struct CachedInvokableCall_1_t2918;
extern Il2CppGenericClass CachedInvokableCall_1_t2918_GenericClass;
TypeInfo CachedInvokableCall_1_t2918_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2918_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2918_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2919_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2918_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2918_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2918_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2918_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2918_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2918_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2918_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2918)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_36.h"
extern TypeInfo UnityAction_1_t2920_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_36MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.ReconstructionBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.ReconstructionBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionBehaviour_t38_m32586(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>
extern Il2CppType UnityAction_1_t2920_0_0_1;
FieldInfo InvokableCall_1_t2919____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2920_0_0_1/* type */
	, &InvokableCall_1_t2919_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2919, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2919_FieldInfos[] =
{
	&InvokableCall_1_t2919____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2919_InvokableCall_1__ctor_m15068_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15068_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15068_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2919_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2919_InvokableCall_1__ctor_m15068_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15068_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2920_0_0_0;
static ParameterInfo InvokableCall_1_t2919_InvokableCall_1__ctor_m15069_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2920_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15069_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15069_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2919_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2919_InvokableCall_1__ctor_m15069_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15069_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2919_InvokableCall_1_Invoke_m15070_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15070_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15070_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2919_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2919_InvokableCall_1_Invoke_m15070_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15070_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2919_InvokableCall_1_Find_m15071_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15071_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15071_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2919_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2919_InvokableCall_1_Find_m15071_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15071_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2919_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15068_MethodInfo,
	&InvokableCall_1__ctor_m15069_MethodInfo,
	&InvokableCall_1_Invoke_m15070_MethodInfo,
	&InvokableCall_1_Find_m15071_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2919_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m15070_MethodInfo,
	&InvokableCall_1_Find_m15071_MethodInfo,
};
extern TypeInfo UnityAction_1_t2920_il2cpp_TypeInfo;
extern TypeInfo ReconstructionBehaviour_t38_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2919_RGCTXData[5] = 
{
	&UnityAction_1_t2920_0_0_0/* Type Usage */,
	&UnityAction_1_t2920_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionBehaviour_t38_m32586_MethodInfo/* Method Usage */,
	&ReconstructionBehaviour_t38_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15073_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2919_0_0_0;
extern Il2CppType InvokableCall_1_t2919_1_0_0;
struct InvokableCall_1_t2919;
extern Il2CppGenericClass InvokableCall_1_t2919_GenericClass;
TypeInfo InvokableCall_1_t2919_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2919_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2919_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2919_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2919_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2919_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2919_0_0_0/* byval_arg */
	, &InvokableCall_1_t2919_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2919_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2919_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2919)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2920_UnityAction_1__ctor_m15072_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15072_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15072_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2920_UnityAction_1__ctor_m15072_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15072_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionBehaviour_t38_0_0_0;
static ParameterInfo UnityAction_1_t2920_UnityAction_1_Invoke_m15073_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t38_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15073_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15073_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2920_UnityAction_1_Invoke_m15073_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15073_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionBehaviour_t38_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2920_UnityAction_1_BeginInvoke_m15074_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionBehaviour_t38_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15074_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15074_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2920_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2920_UnityAction_1_BeginInvoke_m15074_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15074_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2920_UnityAction_1_EndInvoke_m15075_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15075_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15075_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2920_UnityAction_1_EndInvoke_m15075_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15075_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2920_MethodInfos[] =
{
	&UnityAction_1__ctor_m15072_MethodInfo,
	&UnityAction_1_Invoke_m15073_MethodInfo,
	&UnityAction_1_BeginInvoke_m15074_MethodInfo,
	&UnityAction_1_EndInvoke_m15075_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15074_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15075_MethodInfo;
static MethodInfo* UnityAction_1_t2920_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m15073_MethodInfo,
	&UnityAction_1_BeginInvoke_m15074_MethodInfo,
	&UnityAction_1_EndInvoke_m15075_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2920_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2920_1_0_0;
struct UnityAction_1_t2920;
extern Il2CppGenericClass UnityAction_1_t2920_GenericClass;
TypeInfo UnityAction_1_t2920_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2920_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2920_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2920_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2920_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2920_0_0_0/* byval_arg */
	, &UnityAction_1_t2920_1_0_0/* this_arg */
	, UnityAction_1_t2920_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2920_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2920)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5879_il2cpp_TypeInfo;

// Vuforia.ReconstructionFromTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ReconstructionFromTargetBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42465_MethodInfo;
static PropertyInfo IEnumerator_1_t5879____Current_PropertyInfo = 
{
	&IEnumerator_1_t5879_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42465_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5879_PropertyInfos[] =
{
	&IEnumerator_1_t5879____Current_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionFromTargetBehaviour_t73_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42465_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42465_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5879_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionFromTargetBehaviour_t73_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42465_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5879_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42465_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5879_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5879_0_0_0;
extern Il2CppType IEnumerator_1_t5879_1_0_0;
struct IEnumerator_1_t5879;
extern Il2CppGenericClass IEnumerator_1_t5879_GenericClass;
TypeInfo IEnumerator_1_t5879_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5879_MethodInfos/* methods */
	, IEnumerator_1_t5879_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5879_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5879_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5879_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5879_0_0_0/* byval_arg */
	, &IEnumerator_1_t5879_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5879_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_98.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2921_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_98MethodDeclarations.h"

extern TypeInfo ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15080_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisReconstructionFromTargetBehaviour_t73_m32588_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionFromTargetBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionFromTargetBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisReconstructionFromTargetBehaviour_t73_m32588(__this, p0, method) (ReconstructionFromTargetBehaviour_t73 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2921____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2921, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2921____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2921, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2921_FieldInfos[] =
{
	&InternalEnumerator_1_t2921____array_0_FieldInfo,
	&InternalEnumerator_1_t2921____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15077_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2921____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2921_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15077_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2921____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2921_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15080_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2921_PropertyInfos[] =
{
	&InternalEnumerator_1_t2921____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2921____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2921_InternalEnumerator_1__ctor_m15076_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15076_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15076_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2921_InternalEnumerator_1__ctor_m15076_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15076_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15077_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15077_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15077_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15078_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15078_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15078_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15079_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15079_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15079_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetBehaviour_t73_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15080_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15080_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionFromTargetBehaviour_t73_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15080_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2921_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15076_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15077_MethodInfo,
	&InternalEnumerator_1_Dispose_m15078_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15079_MethodInfo,
	&InternalEnumerator_1_get_Current_m15080_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15079_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15078_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2921_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15077_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15079_MethodInfo,
	&InternalEnumerator_1_Dispose_m15078_MethodInfo,
	&InternalEnumerator_1_get_Current_m15080_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2921_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5879_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2921_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5879_il2cpp_TypeInfo, 7},
};
extern TypeInfo ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2921_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15080_MethodInfo/* Method Usage */,
	&ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisReconstructionFromTargetBehaviour_t73_m32588_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2921_0_0_0;
extern Il2CppType InternalEnumerator_1_t2921_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2921_GenericClass;
TypeInfo InternalEnumerator_1_t2921_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2921_MethodInfos/* methods */
	, InternalEnumerator_1_t2921_PropertyInfos/* properties */
	, InternalEnumerator_1_t2921_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2921_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2921_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2921_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2921_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2921_1_0_0/* this_arg */
	, InternalEnumerator_1_t2921_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2921_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2921_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2921)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7482_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>
extern MethodInfo ICollection_1_get_Count_m42466_MethodInfo;
static PropertyInfo ICollection_1_t7482____Count_PropertyInfo = 
{
	&ICollection_1_t7482_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42466_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42467_MethodInfo;
static PropertyInfo ICollection_1_t7482____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7482_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42467_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7482_PropertyInfos[] =
{
	&ICollection_1_t7482____Count_PropertyInfo,
	&ICollection_1_t7482____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42466_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42466_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7482_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42466_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42467_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42467_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7482_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42467_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetBehaviour_t73_0_0_0;
extern Il2CppType ReconstructionFromTargetBehaviour_t73_0_0_0;
static ParameterInfo ICollection_1_t7482_ICollection_1_Add_m42468_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t73_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42468_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42468_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7482_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7482_ICollection_1_Add_m42468_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42468_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42469_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42469_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7482_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42469_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetBehaviour_t73_0_0_0;
static ParameterInfo ICollection_1_t7482_ICollection_1_Contains_m42470_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t73_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42470_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42470_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7482_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7482_ICollection_1_Contains_m42470_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42470_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetBehaviourU5BU5D_t5188_0_0_0;
extern Il2CppType ReconstructionFromTargetBehaviourU5BU5D_t5188_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7482_ICollection_1_CopyTo_m42471_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviourU5BU5D_t5188_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42471_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42471_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7482_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7482_ICollection_1_CopyTo_m42471_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42471_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetBehaviour_t73_0_0_0;
static ParameterInfo ICollection_1_t7482_ICollection_1_Remove_m42472_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t73_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42472_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42472_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7482_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7482_ICollection_1_Remove_m42472_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42472_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7482_MethodInfos[] =
{
	&ICollection_1_get_Count_m42466_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42467_MethodInfo,
	&ICollection_1_Add_m42468_MethodInfo,
	&ICollection_1_Clear_m42469_MethodInfo,
	&ICollection_1_Contains_m42470_MethodInfo,
	&ICollection_1_CopyTo_m42471_MethodInfo,
	&ICollection_1_Remove_m42472_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7484_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7482_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7484_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7482_0_0_0;
extern Il2CppType ICollection_1_t7482_1_0_0;
struct ICollection_1_t7482;
extern Il2CppGenericClass ICollection_1_t7482_GenericClass;
TypeInfo ICollection_1_t7482_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7482_MethodInfos/* methods */
	, ICollection_1_t7482_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7482_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7482_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7482_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7482_0_0_0/* byval_arg */
	, &ICollection_1_t7482_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7482_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionFromTargetBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionFromTargetBehaviour>
extern Il2CppType IEnumerator_1_t5879_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42473_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionFromTargetBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42473_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7484_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5879_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42473_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7484_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42473_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7484_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7484_0_0_0;
extern Il2CppType IEnumerable_1_t7484_1_0_0;
struct IEnumerable_1_t7484;
extern Il2CppGenericClass IEnumerable_1_t7484_GenericClass;
TypeInfo IEnumerable_1_t7484_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7484_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7484_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7484_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7484_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7484_0_0_0/* byval_arg */
	, &IEnumerable_1_t7484_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7484_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7483_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>
extern MethodInfo IList_1_get_Item_m42474_MethodInfo;
extern MethodInfo IList_1_set_Item_m42475_MethodInfo;
static PropertyInfo IList_1_t7483____Item_PropertyInfo = 
{
	&IList_1_t7483_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42474_MethodInfo/* get */
	, &IList_1_set_Item_m42475_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7483_PropertyInfos[] =
{
	&IList_1_t7483____Item_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionFromTargetBehaviour_t73_0_0_0;
static ParameterInfo IList_1_t7483_IList_1_IndexOf_m42476_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t73_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42476_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42476_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7483_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7483_IList_1_IndexOf_m42476_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42476_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ReconstructionFromTargetBehaviour_t73_0_0_0;
static ParameterInfo IList_1_t7483_IList_1_Insert_m42477_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t73_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42477_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42477_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7483_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7483_IList_1_Insert_m42477_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42477_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7483_IList_1_RemoveAt_m42478_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42478_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42478_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7483_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7483_IList_1_RemoveAt_m42478_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42478_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7483_IList_1_get_Item_m42474_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ReconstructionFromTargetBehaviour_t73_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42474_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42474_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7483_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionFromTargetBehaviour_t73_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7483_IList_1_get_Item_m42474_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42474_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ReconstructionFromTargetBehaviour_t73_0_0_0;
static ParameterInfo IList_1_t7483_IList_1_set_Item_m42475_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t73_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42475_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42475_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7483_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7483_IList_1_set_Item_m42475_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42475_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7483_MethodInfos[] =
{
	&IList_1_IndexOf_m42476_MethodInfo,
	&IList_1_Insert_m42477_MethodInfo,
	&IList_1_RemoveAt_m42478_MethodInfo,
	&IList_1_get_Item_m42474_MethodInfo,
	&IList_1_set_Item_m42475_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7483_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7482_il2cpp_TypeInfo,
	&IEnumerable_1_t7484_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7483_0_0_0;
extern Il2CppType IList_1_t7483_1_0_0;
struct IList_1_t7483;
extern Il2CppGenericClass IList_1_t7483_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7483_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7483_MethodInfos/* methods */
	, IList_1_t7483_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7483_il2cpp_TypeInfo/* element_class */
	, IList_1_t7483_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7483_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7483_0_0_0/* byval_arg */
	, &IList_1_t7483_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7483_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7485_il2cpp_TypeInfo;

// Vuforia.ReconstructionFromTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionFromT.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42479_MethodInfo;
static PropertyInfo ICollection_1_t7485____Count_PropertyInfo = 
{
	&ICollection_1_t7485_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42479_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42480_MethodInfo;
static PropertyInfo ICollection_1_t7485____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7485_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42480_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7485_PropertyInfos[] =
{
	&ICollection_1_t7485____Count_PropertyInfo,
	&ICollection_1_t7485____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42479_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42479_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7485_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42479_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42480_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42480_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7485_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42480_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t74_0_0_0;
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t74_0_0_0;
static ParameterInfo ICollection_1_t7485_ICollection_1_Add_m42481_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetAbstractBehaviour_t74_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42481_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42481_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7485_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7485_ICollection_1_Add_m42481_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42481_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42482_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42482_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7485_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42482_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t74_0_0_0;
static ParameterInfo ICollection_1_t7485_ICollection_1_Contains_m42483_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetAbstractBehaviour_t74_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42483_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42483_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7485_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7485_ICollection_1_Contains_m42483_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42483_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetAbstractBehaviourU5BU5D_t5513_0_0_0;
extern Il2CppType ReconstructionFromTargetAbstractBehaviourU5BU5D_t5513_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7485_ICollection_1_CopyTo_m42484_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetAbstractBehaviourU5BU5D_t5513_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42484_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42484_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7485_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7485_ICollection_1_CopyTo_m42484_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42484_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t74_0_0_0;
static ParameterInfo ICollection_1_t7485_ICollection_1_Remove_m42485_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetAbstractBehaviour_t74_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42485_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42485_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7485_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7485_ICollection_1_Remove_m42485_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42485_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7485_MethodInfos[] =
{
	&ICollection_1_get_Count_m42479_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42480_MethodInfo,
	&ICollection_1_Add_m42481_MethodInfo,
	&ICollection_1_Clear_m42482_MethodInfo,
	&ICollection_1_Contains_m42483_MethodInfo,
	&ICollection_1_CopyTo_m42484_MethodInfo,
	&ICollection_1_Remove_m42485_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7487_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7485_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7487_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7485_0_0_0;
extern Il2CppType ICollection_1_t7485_1_0_0;
struct ICollection_1_t7485;
extern Il2CppGenericClass ICollection_1_t7485_GenericClass;
TypeInfo ICollection_1_t7485_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7485_MethodInfos/* methods */
	, ICollection_1_t7485_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7485_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7485_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7485_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7485_0_0_0/* byval_arg */
	, &ICollection_1_t7485_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7485_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5881_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42486_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42486_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7487_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5881_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42486_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7487_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42486_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7487_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7487_0_0_0;
extern Il2CppType IEnumerable_1_t7487_1_0_0;
struct IEnumerable_1_t7487;
extern Il2CppGenericClass IEnumerable_1_t7487_GenericClass;
TypeInfo IEnumerable_1_t7487_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7487_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7487_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7487_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7487_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7487_0_0_0/* byval_arg */
	, &IEnumerable_1_t7487_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7487_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5881_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42487_MethodInfo;
static PropertyInfo IEnumerator_1_t5881____Current_PropertyInfo = 
{
	&IEnumerator_1_t5881_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42487_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5881_PropertyInfos[] =
{
	&IEnumerator_1_t5881____Current_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t74_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42487_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42487_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5881_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionFromTargetAbstractBehaviour_t74_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42487_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5881_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42487_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5881_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5881_0_0_0;
extern Il2CppType IEnumerator_1_t5881_1_0_0;
struct IEnumerator_1_t5881;
extern Il2CppGenericClass IEnumerator_1_t5881_GenericClass;
TypeInfo IEnumerator_1_t5881_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5881_MethodInfos/* methods */
	, IEnumerator_1_t5881_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5881_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5881_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5881_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5881_0_0_0/* byval_arg */
	, &IEnumerator_1_t5881_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5881_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_99.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2922_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_99MethodDeclarations.h"

extern TypeInfo ReconstructionFromTargetAbstractBehaviour_t74_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15085_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisReconstructionFromTargetAbstractBehaviour_t74_m32599_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionFromTargetAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ReconstructionFromTargetAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisReconstructionFromTargetAbstractBehaviour_t74_m32599(__this, p0, method) (ReconstructionFromTargetAbstractBehaviour_t74 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2922____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2922_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2922, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2922____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2922_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2922, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2922_FieldInfos[] =
{
	&InternalEnumerator_1_t2922____array_0_FieldInfo,
	&InternalEnumerator_1_t2922____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15082_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2922____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2922_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15082_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2922____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2922_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15085_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2922_PropertyInfos[] =
{
	&InternalEnumerator_1_t2922____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2922____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2922_InternalEnumerator_1__ctor_m15081_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15081_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15081_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2922_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2922_InternalEnumerator_1__ctor_m15081_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15081_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15082_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15082_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2922_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15082_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15083_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15083_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2922_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15083_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15084_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15084_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2922_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15084_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t74_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15085_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15085_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2922_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionFromTargetAbstractBehaviour_t74_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15085_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2922_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15081_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15082_MethodInfo,
	&InternalEnumerator_1_Dispose_m15083_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15084_MethodInfo,
	&InternalEnumerator_1_get_Current_m15085_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15084_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15083_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2922_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15082_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15084_MethodInfo,
	&InternalEnumerator_1_Dispose_m15083_MethodInfo,
	&InternalEnumerator_1_get_Current_m15085_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2922_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5881_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2922_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5881_il2cpp_TypeInfo, 7},
};
extern TypeInfo ReconstructionFromTargetAbstractBehaviour_t74_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2922_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15085_MethodInfo/* Method Usage */,
	&ReconstructionFromTargetAbstractBehaviour_t74_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisReconstructionFromTargetAbstractBehaviour_t74_m32599_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2922_0_0_0;
extern Il2CppType InternalEnumerator_1_t2922_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2922_GenericClass;
TypeInfo InternalEnumerator_1_t2922_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2922_MethodInfos/* methods */
	, InternalEnumerator_1_t2922_PropertyInfos/* properties */
	, InternalEnumerator_1_t2922_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2922_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2922_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2922_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2922_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2922_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2922_1_0_0/* this_arg */
	, InternalEnumerator_1_t2922_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2922_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2922_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2922)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7486_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42488_MethodInfo;
extern MethodInfo IList_1_set_Item_m42489_MethodInfo;
static PropertyInfo IList_1_t7486____Item_PropertyInfo = 
{
	&IList_1_t7486_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42488_MethodInfo/* get */
	, &IList_1_set_Item_m42489_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7486_PropertyInfos[] =
{
	&IList_1_t7486____Item_PropertyInfo,
	NULL
};
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t74_0_0_0;
static ParameterInfo IList_1_t7486_IList_1_IndexOf_m42490_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetAbstractBehaviour_t74_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42490_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42490_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7486_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7486_IList_1_IndexOf_m42490_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42490_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t74_0_0_0;
static ParameterInfo IList_1_t7486_IList_1_Insert_m42491_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetAbstractBehaviour_t74_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42491_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42491_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7486_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7486_IList_1_Insert_m42491_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42491_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7486_IList_1_RemoveAt_m42492_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42492_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42492_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7486_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7486_IList_1_RemoveAt_m42492_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42492_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7486_IList_1_get_Item_m42488_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t74_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42488_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42488_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7486_il2cpp_TypeInfo/* declaring_type */
	, &ReconstructionFromTargetAbstractBehaviour_t74_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7486_IList_1_get_Item_m42488_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42488_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ReconstructionFromTargetAbstractBehaviour_t74_0_0_0;
static ParameterInfo IList_1_t7486_IList_1_set_Item_m42489_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetAbstractBehaviour_t74_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42489_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42489_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7486_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7486_IList_1_set_Item_m42489_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42489_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7486_MethodInfos[] =
{
	&IList_1_IndexOf_m42490_MethodInfo,
	&IList_1_Insert_m42491_MethodInfo,
	&IList_1_RemoveAt_m42492_MethodInfo,
	&IList_1_get_Item_m42488_MethodInfo,
	&IList_1_set_Item_m42489_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7486_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7485_il2cpp_TypeInfo,
	&IEnumerable_1_t7487_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7486_0_0_0;
extern Il2CppType IList_1_t7486_1_0_0;
struct IList_1_t7486;
extern Il2CppGenericClass IList_1_t7486_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7486_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7486_MethodInfos/* methods */
	, IList_1_t7486_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7486_il2cpp_TypeInfo/* element_class */
	, IList_1_t7486_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7486_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7486_0_0_0/* byval_arg */
	, &IList_1_t7486_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7486_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_34.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2923_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_34MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_30.h"
extern TypeInfo InvokableCall_1_t2924_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_30MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15088_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15090_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2923____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2923_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2923, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2923_FieldInfos[] =
{
	&CachedInvokableCall_1_t2923____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType ReconstructionFromTargetBehaviour_t73_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2923_CachedInvokableCall_1__ctor_m15086_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t73_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15086_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15086_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2923_CachedInvokableCall_1__ctor_m15086_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15086_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2923_CachedInvokableCall_1_Invoke_m15087_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15087_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15087_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2923_CachedInvokableCall_1_Invoke_m15087_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15087_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2923_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15086_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15087_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15087_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15091_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2923_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15087_MethodInfo,
	&InvokableCall_1_Find_m15091_MethodInfo,
};
extern Il2CppType UnityAction_1_t2925_0_0_0;
extern TypeInfo UnityAction_1_t2925_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionFromTargetBehaviour_t73_m32609_MethodInfo;
extern TypeInfo ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15093_MethodInfo;
extern TypeInfo ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2923_RGCTXData[8] = 
{
	&UnityAction_1_t2925_0_0_0/* Type Usage */,
	&UnityAction_1_t2925_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionFromTargetBehaviour_t73_m32609_MethodInfo/* Method Usage */,
	&ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15093_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15088_MethodInfo/* Method Usage */,
	&ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15090_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2923_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2923_1_0_0;
struct CachedInvokableCall_1_t2923;
extern Il2CppGenericClass CachedInvokableCall_1_t2923_GenericClass;
TypeInfo CachedInvokableCall_1_t2923_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2923_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2923_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2924_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2923_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2923_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2923_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2923_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2923_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2923_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2923_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2923)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_37.h"
extern TypeInfo UnityAction_1_t2925_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_37MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.ReconstructionFromTargetBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.ReconstructionFromTargetBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionFromTargetBehaviour_t73_m32609(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>
extern Il2CppType UnityAction_1_t2925_0_0_1;
FieldInfo InvokableCall_1_t2924____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2925_0_0_1/* type */
	, &InvokableCall_1_t2924_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2924, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2924_FieldInfos[] =
{
	&InvokableCall_1_t2924____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2924_InvokableCall_1__ctor_m15088_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15088_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15088_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2924_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2924_InvokableCall_1__ctor_m15088_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15088_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2925_0_0_0;
static ParameterInfo InvokableCall_1_t2924_InvokableCall_1__ctor_m15089_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2925_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15089_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15089_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2924_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2924_InvokableCall_1__ctor_m15089_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15089_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2924_InvokableCall_1_Invoke_m15090_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15090_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15090_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2924_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2924_InvokableCall_1_Invoke_m15090_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15090_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2924_InvokableCall_1_Find_m15091_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15091_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15091_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2924_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2924_InvokableCall_1_Find_m15091_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15091_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2924_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15088_MethodInfo,
	&InvokableCall_1__ctor_m15089_MethodInfo,
	&InvokableCall_1_Invoke_m15090_MethodInfo,
	&InvokableCall_1_Find_m15091_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2924_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m15090_MethodInfo,
	&InvokableCall_1_Find_m15091_MethodInfo,
};
extern TypeInfo UnityAction_1_t2925_il2cpp_TypeInfo;
extern TypeInfo ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2924_RGCTXData[5] = 
{
	&UnityAction_1_t2925_0_0_0/* Type Usage */,
	&UnityAction_1_t2925_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisReconstructionFromTargetBehaviour_t73_m32609_MethodInfo/* Method Usage */,
	&ReconstructionFromTargetBehaviour_t73_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15093_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2924_0_0_0;
extern Il2CppType InvokableCall_1_t2924_1_0_0;
struct InvokableCall_1_t2924;
extern Il2CppGenericClass InvokableCall_1_t2924_GenericClass;
TypeInfo InvokableCall_1_t2924_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2924_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2924_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2924_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2924_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2924_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2924_0_0_0/* byval_arg */
	, &InvokableCall_1_t2924_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2924_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2924_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2924)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2925_UnityAction_1__ctor_m15092_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15092_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15092_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2925_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2925_UnityAction_1__ctor_m15092_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15092_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetBehaviour_t73_0_0_0;
static ParameterInfo UnityAction_1_t2925_UnityAction_1_Invoke_m15093_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t73_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15093_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15093_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2925_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2925_UnityAction_1_Invoke_m15093_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15093_GenericMethod/* genericMethod */

};
extern Il2CppType ReconstructionFromTargetBehaviour_t73_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2925_UnityAction_1_BeginInvoke_m15094_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ReconstructionFromTargetBehaviour_t73_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15094_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15094_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2925_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2925_UnityAction_1_BeginInvoke_m15094_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15094_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2925_UnityAction_1_EndInvoke_m15095_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15095_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionFromTargetBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15095_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2925_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2925_UnityAction_1_EndInvoke_m15095_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15095_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2925_MethodInfos[] =
{
	&UnityAction_1__ctor_m15092_MethodInfo,
	&UnityAction_1_Invoke_m15093_MethodInfo,
	&UnityAction_1_BeginInvoke_m15094_MethodInfo,
	&UnityAction_1_EndInvoke_m15095_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15094_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15095_MethodInfo;
static MethodInfo* UnityAction_1_t2925_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m15093_MethodInfo,
	&UnityAction_1_BeginInvoke_m15094_MethodInfo,
	&UnityAction_1_EndInvoke_m15095_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2925_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2925_1_0_0;
struct UnityAction_1_t2925;
extern Il2CppGenericClass UnityAction_1_t2925_GenericClass;
TypeInfo UnityAction_1_t2925_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2925_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2925_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2925_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2925_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2925_0_0_0/* byval_arg */
	, &UnityAction_1_t2925_1_0_0/* this_arg */
	, UnityAction_1_t2925_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2925_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2925)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5883_il2cpp_TypeInfo;

// Vuforia.SmartTerrainTrackerBehaviour
#include "AssemblyU2DCSharp_Vuforia_SmartTerrainTrackerBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42493_MethodInfo;
static PropertyInfo IEnumerator_1_t5883____Current_PropertyInfo = 
{
	&IEnumerator_1_t5883_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42493_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5883_PropertyInfos[] =
{
	&IEnumerator_1_t5883____Current_PropertyInfo,
	NULL
};
extern Il2CppType SmartTerrainTrackerBehaviour_t75_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42493_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42493_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5883_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackerBehaviour_t75_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42493_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5883_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42493_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5883_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5883_0_0_0;
extern Il2CppType IEnumerator_1_t5883_1_0_0;
struct IEnumerator_1_t5883;
extern Il2CppGenericClass IEnumerator_1_t5883_GenericClass;
TypeInfo IEnumerator_1_t5883_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5883_MethodInfos/* methods */
	, IEnumerator_1_t5883_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5883_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5883_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5883_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5883_0_0_0/* byval_arg */
	, &IEnumerator_1_t5883_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5883_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_100.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2926_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_100MethodDeclarations.h"

extern TypeInfo SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15100_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSmartTerrainTrackerBehaviour_t75_m32611_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.SmartTerrainTrackerBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.SmartTerrainTrackerBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisSmartTerrainTrackerBehaviour_t75_m32611(__this, p0, method) (SmartTerrainTrackerBehaviour_t75 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2926____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2926, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2926____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2926, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2926_FieldInfos[] =
{
	&InternalEnumerator_1_t2926____array_0_FieldInfo,
	&InternalEnumerator_1_t2926____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15097_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2926____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2926_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15097_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2926____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2926_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15100_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2926_PropertyInfos[] =
{
	&InternalEnumerator_1_t2926____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2926____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2926_InternalEnumerator_1__ctor_m15096_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15096_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15096_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2926_InternalEnumerator_1__ctor_m15096_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15096_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15097_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15097_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15097_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15098_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15098_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15098_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15099_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15099_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15099_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerBehaviour_t75_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15100_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15100_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackerBehaviour_t75_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15100_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2926_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15096_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15097_MethodInfo,
	&InternalEnumerator_1_Dispose_m15098_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15099_MethodInfo,
	&InternalEnumerator_1_get_Current_m15100_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15099_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15098_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2926_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15097_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15099_MethodInfo,
	&InternalEnumerator_1_Dispose_m15098_MethodInfo,
	&InternalEnumerator_1_get_Current_m15100_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2926_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5883_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2926_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5883_il2cpp_TypeInfo, 7},
};
extern TypeInfo SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2926_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15100_MethodInfo/* Method Usage */,
	&SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSmartTerrainTrackerBehaviour_t75_m32611_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2926_0_0_0;
extern Il2CppType InternalEnumerator_1_t2926_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2926_GenericClass;
TypeInfo InternalEnumerator_1_t2926_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2926_MethodInfos/* methods */
	, InternalEnumerator_1_t2926_PropertyInfos/* properties */
	, InternalEnumerator_1_t2926_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2926_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2926_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2926_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2926_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2926_1_0_0/* this_arg */
	, InternalEnumerator_1_t2926_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2926_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2926_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2926)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7488_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>
extern MethodInfo ICollection_1_get_Count_m42494_MethodInfo;
static PropertyInfo ICollection_1_t7488____Count_PropertyInfo = 
{
	&ICollection_1_t7488_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42494_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42495_MethodInfo;
static PropertyInfo ICollection_1_t7488____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7488_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42495_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7488_PropertyInfos[] =
{
	&ICollection_1_t7488____Count_PropertyInfo,
	&ICollection_1_t7488____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42494_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42494_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7488_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42494_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42495_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42495_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7488_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42495_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerBehaviour_t75_0_0_0;
extern Il2CppType SmartTerrainTrackerBehaviour_t75_0_0_0;
static ParameterInfo ICollection_1_t7488_ICollection_1_Add_m42496_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t75_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42496_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42496_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7488_ICollection_1_Add_m42496_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42496_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42497_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42497_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42497_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerBehaviour_t75_0_0_0;
static ParameterInfo ICollection_1_t7488_ICollection_1_Contains_m42498_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t75_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42498_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42498_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7488_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7488_ICollection_1_Contains_m42498_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42498_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerBehaviourU5BU5D_t5189_0_0_0;
extern Il2CppType SmartTerrainTrackerBehaviourU5BU5D_t5189_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7488_ICollection_1_CopyTo_m42499_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviourU5BU5D_t5189_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42499_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42499_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7488_ICollection_1_CopyTo_m42499_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42499_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerBehaviour_t75_0_0_0;
static ParameterInfo ICollection_1_t7488_ICollection_1_Remove_m42500_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t75_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42500_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42500_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7488_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7488_ICollection_1_Remove_m42500_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42500_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7488_MethodInfos[] =
{
	&ICollection_1_get_Count_m42494_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42495_MethodInfo,
	&ICollection_1_Add_m42496_MethodInfo,
	&ICollection_1_Clear_m42497_MethodInfo,
	&ICollection_1_Contains_m42498_MethodInfo,
	&ICollection_1_CopyTo_m42499_MethodInfo,
	&ICollection_1_Remove_m42500_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7490_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7488_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7490_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7488_0_0_0;
extern Il2CppType ICollection_1_t7488_1_0_0;
struct ICollection_1_t7488;
extern Il2CppGenericClass ICollection_1_t7488_GenericClass;
TypeInfo ICollection_1_t7488_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7488_MethodInfos/* methods */
	, ICollection_1_t7488_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7488_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7488_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7488_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7488_0_0_0/* byval_arg */
	, &ICollection_1_t7488_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7488_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackerBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackerBehaviour>
extern Il2CppType IEnumerator_1_t5883_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42501_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackerBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42501_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7490_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5883_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42501_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7490_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42501_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7490_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7490_0_0_0;
extern Il2CppType IEnumerable_1_t7490_1_0_0;
struct IEnumerable_1_t7490;
extern Il2CppGenericClass IEnumerable_1_t7490_GenericClass;
TypeInfo IEnumerable_1_t7490_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7490_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7490_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7490_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7490_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7490_0_0_0/* byval_arg */
	, &IEnumerable_1_t7490_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7490_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7489_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>
extern MethodInfo IList_1_get_Item_m42502_MethodInfo;
extern MethodInfo IList_1_set_Item_m42503_MethodInfo;
static PropertyInfo IList_1_t7489____Item_PropertyInfo = 
{
	&IList_1_t7489_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42502_MethodInfo/* get */
	, &IList_1_set_Item_m42503_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7489_PropertyInfos[] =
{
	&IList_1_t7489____Item_PropertyInfo,
	NULL
};
extern Il2CppType SmartTerrainTrackerBehaviour_t75_0_0_0;
static ParameterInfo IList_1_t7489_IList_1_IndexOf_m42504_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t75_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42504_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42504_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7489_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7489_IList_1_IndexOf_m42504_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42504_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SmartTerrainTrackerBehaviour_t75_0_0_0;
static ParameterInfo IList_1_t7489_IList_1_Insert_m42505_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t75_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42505_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42505_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7489_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7489_IList_1_Insert_m42505_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42505_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7489_IList_1_RemoveAt_m42506_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42506_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42506_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7489_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7489_IList_1_RemoveAt_m42506_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42506_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7489_IList_1_get_Item_m42502_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType SmartTerrainTrackerBehaviour_t75_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42502_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42502_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7489_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackerBehaviour_t75_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7489_IList_1_get_Item_m42502_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42502_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SmartTerrainTrackerBehaviour_t75_0_0_0;
static ParameterInfo IList_1_t7489_IList_1_set_Item_m42503_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t75_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42503_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42503_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7489_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7489_IList_1_set_Item_m42503_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42503_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7489_MethodInfos[] =
{
	&IList_1_IndexOf_m42504_MethodInfo,
	&IList_1_Insert_m42505_MethodInfo,
	&IList_1_RemoveAt_m42506_MethodInfo,
	&IList_1_get_Item_m42502_MethodInfo,
	&IList_1_set_Item_m42503_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7489_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7488_il2cpp_TypeInfo,
	&IEnumerable_1_t7490_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7489_0_0_0;
extern Il2CppType IList_1_t7489_1_0_0;
struct IList_1_t7489;
extern Il2CppGenericClass IList_1_t7489_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7489_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7489_MethodInfos/* methods */
	, IList_1_t7489_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7489_il2cpp_TypeInfo/* element_class */
	, IList_1_t7489_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7489_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7489_0_0_0/* byval_arg */
	, &IList_1_t7489_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7489_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7491_il2cpp_TypeInfo;

// Vuforia.SmartTerrainTrackerAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainTracker.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42507_MethodInfo;
static PropertyInfo ICollection_1_t7491____Count_PropertyInfo = 
{
	&ICollection_1_t7491_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42507_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42508_MethodInfo;
static PropertyInfo ICollection_1_t7491____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7491_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42508_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7491_PropertyInfos[] =
{
	&ICollection_1_t7491____Count_PropertyInfo,
	&ICollection_1_t7491____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42507_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42507_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7491_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42507_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42508_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42508_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7491_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42508_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t76_0_0_0;
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t76_0_0_0;
static ParameterInfo ICollection_1_t7491_ICollection_1_Add_m42509_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerAbstractBehaviour_t76_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42509_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42509_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7491_ICollection_1_Add_m42509_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42509_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42510_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42510_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42510_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t76_0_0_0;
static ParameterInfo ICollection_1_t7491_ICollection_1_Contains_m42511_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerAbstractBehaviour_t76_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42511_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42511_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7491_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7491_ICollection_1_Contains_m42511_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42511_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerAbstractBehaviourU5BU5D_t5514_0_0_0;
extern Il2CppType SmartTerrainTrackerAbstractBehaviourU5BU5D_t5514_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7491_ICollection_1_CopyTo_m42512_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerAbstractBehaviourU5BU5D_t5514_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42512_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42512_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7491_ICollection_1_CopyTo_m42512_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42512_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t76_0_0_0;
static ParameterInfo ICollection_1_t7491_ICollection_1_Remove_m42513_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerAbstractBehaviour_t76_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42513_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42513_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7491_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7491_ICollection_1_Remove_m42513_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42513_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7491_MethodInfos[] =
{
	&ICollection_1_get_Count_m42507_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42508_MethodInfo,
	&ICollection_1_Add_m42509_MethodInfo,
	&ICollection_1_Clear_m42510_MethodInfo,
	&ICollection_1_Contains_m42511_MethodInfo,
	&ICollection_1_CopyTo_m42512_MethodInfo,
	&ICollection_1_Remove_m42513_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7493_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7491_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7493_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7491_0_0_0;
extern Il2CppType ICollection_1_t7491_1_0_0;
struct ICollection_1_t7491;
extern Il2CppGenericClass ICollection_1_t7491_GenericClass;
TypeInfo ICollection_1_t7491_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7491_MethodInfos/* methods */
	, ICollection_1_t7491_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7491_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7491_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7491_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7491_0_0_0/* byval_arg */
	, &ICollection_1_t7491_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7491_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5885_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42514_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42514_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7493_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5885_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42514_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7493_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42514_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7493_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7493_0_0_0;
extern Il2CppType IEnumerable_1_t7493_1_0_0;
struct IEnumerable_1_t7493;
extern Il2CppGenericClass IEnumerable_1_t7493_GenericClass;
TypeInfo IEnumerable_1_t7493_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7493_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7493_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7493_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7493_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7493_0_0_0/* byval_arg */
	, &IEnumerable_1_t7493_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7493_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5885_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42515_MethodInfo;
static PropertyInfo IEnumerator_1_t5885____Current_PropertyInfo = 
{
	&IEnumerator_1_t5885_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42515_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5885_PropertyInfos[] =
{
	&IEnumerator_1_t5885____Current_PropertyInfo,
	NULL
};
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t76_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42515_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42515_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5885_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackerAbstractBehaviour_t76_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42515_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5885_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42515_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5885_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5885_0_0_0;
extern Il2CppType IEnumerator_1_t5885_1_0_0;
struct IEnumerator_1_t5885;
extern Il2CppGenericClass IEnumerator_1_t5885_GenericClass;
TypeInfo IEnumerator_1_t5885_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5885_MethodInfos/* methods */
	, IEnumerator_1_t5885_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5885_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5885_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5885_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5885_0_0_0/* byval_arg */
	, &IEnumerator_1_t5885_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5885_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_101.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2927_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_101MethodDeclarations.h"

extern TypeInfo SmartTerrainTrackerAbstractBehaviour_t76_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15105_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSmartTerrainTrackerAbstractBehaviour_t76_m32622_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.SmartTerrainTrackerAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.SmartTerrainTrackerAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisSmartTerrainTrackerAbstractBehaviour_t76_m32622(__this, p0, method) (SmartTerrainTrackerAbstractBehaviour_t76 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2927____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2927, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2927____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2927, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2927_FieldInfos[] =
{
	&InternalEnumerator_1_t2927____array_0_FieldInfo,
	&InternalEnumerator_1_t2927____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15102_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2927____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2927_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15102_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2927____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2927_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15105_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2927_PropertyInfos[] =
{
	&InternalEnumerator_1_t2927____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2927____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2927_InternalEnumerator_1__ctor_m15101_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15101_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15101_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2927_InternalEnumerator_1__ctor_m15101_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15101_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15102_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15102_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15102_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15103_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15103_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15103_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15104_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15104_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15104_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t76_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15105_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15105_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackerAbstractBehaviour_t76_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15105_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2927_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15101_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15102_MethodInfo,
	&InternalEnumerator_1_Dispose_m15103_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15104_MethodInfo,
	&InternalEnumerator_1_get_Current_m15105_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15104_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15103_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2927_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15102_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15104_MethodInfo,
	&InternalEnumerator_1_Dispose_m15103_MethodInfo,
	&InternalEnumerator_1_get_Current_m15105_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2927_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5885_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2927_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5885_il2cpp_TypeInfo, 7},
};
extern TypeInfo SmartTerrainTrackerAbstractBehaviour_t76_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2927_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15105_MethodInfo/* Method Usage */,
	&SmartTerrainTrackerAbstractBehaviour_t76_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSmartTerrainTrackerAbstractBehaviour_t76_m32622_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2927_0_0_0;
extern Il2CppType InternalEnumerator_1_t2927_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2927_GenericClass;
TypeInfo InternalEnumerator_1_t2927_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2927_MethodInfos/* methods */
	, InternalEnumerator_1_t2927_PropertyInfos/* properties */
	, InternalEnumerator_1_t2927_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2927_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2927_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2927_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2927_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2927_1_0_0/* this_arg */
	, InternalEnumerator_1_t2927_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2927_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2927_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2927)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7492_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42516_MethodInfo;
extern MethodInfo IList_1_set_Item_m42517_MethodInfo;
static PropertyInfo IList_1_t7492____Item_PropertyInfo = 
{
	&IList_1_t7492_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42516_MethodInfo/* get */
	, &IList_1_set_Item_m42517_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7492_PropertyInfos[] =
{
	&IList_1_t7492____Item_PropertyInfo,
	NULL
};
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t76_0_0_0;
static ParameterInfo IList_1_t7492_IList_1_IndexOf_m42518_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerAbstractBehaviour_t76_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42518_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42518_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7492_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7492_IList_1_IndexOf_m42518_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42518_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t76_0_0_0;
static ParameterInfo IList_1_t7492_IList_1_Insert_m42519_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerAbstractBehaviour_t76_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42519_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42519_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7492_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7492_IList_1_Insert_m42519_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42519_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7492_IList_1_RemoveAt_m42520_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42520_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42520_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7492_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7492_IList_1_RemoveAt_m42520_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42520_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7492_IList_1_get_Item_m42516_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t76_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42516_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42516_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7492_il2cpp_TypeInfo/* declaring_type */
	, &SmartTerrainTrackerAbstractBehaviour_t76_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7492_IList_1_get_Item_m42516_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42516_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SmartTerrainTrackerAbstractBehaviour_t76_0_0_0;
static ParameterInfo IList_1_t7492_IList_1_set_Item_m42517_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerAbstractBehaviour_t76_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42517_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42517_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7492_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7492_IList_1_set_Item_m42517_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42517_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7492_MethodInfos[] =
{
	&IList_1_IndexOf_m42518_MethodInfo,
	&IList_1_Insert_m42519_MethodInfo,
	&IList_1_RemoveAt_m42520_MethodInfo,
	&IList_1_get_Item_m42516_MethodInfo,
	&IList_1_set_Item_m42517_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7492_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7491_il2cpp_TypeInfo,
	&IEnumerable_1_t7493_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7492_0_0_0;
extern Il2CppType IList_1_t7492_1_0_0;
struct IList_1_t7492;
extern Il2CppGenericClass IList_1_t7492_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7492_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7492_MethodInfos/* methods */
	, IList_1_t7492_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7492_il2cpp_TypeInfo/* element_class */
	, IList_1_t7492_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7492_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7492_0_0_0/* byval_arg */
	, &IList_1_t7492_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7492_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7494_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>
extern MethodInfo ICollection_1_get_Count_m42521_MethodInfo;
static PropertyInfo ICollection_1_t7494____Count_PropertyInfo = 
{
	&ICollection_1_t7494_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42521_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42522_MethodInfo;
static PropertyInfo ICollection_1_t7494____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7494_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42522_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7494_PropertyInfos[] =
{
	&ICollection_1_t7494____Count_PropertyInfo,
	&ICollection_1_t7494____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42521_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42521_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7494_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42521_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42522_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42522_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7494_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42522_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t165_0_0_0;
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t165_0_0_0;
static ParameterInfo ICollection_1_t7494_ICollection_1_Add_m42523_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSmartTerrainTrackerBehaviour_t165_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42523_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42523_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7494_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7494_ICollection_1_Add_m42523_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42523_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42524_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42524_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7494_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42524_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t165_0_0_0;
static ParameterInfo ICollection_1_t7494_ICollection_1_Contains_m42525_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSmartTerrainTrackerBehaviour_t165_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42525_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42525_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7494_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7494_ICollection_1_Contains_m42525_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42525_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSmartTerrainTrackerBehaviourU5BU5D_t5515_0_0_0;
extern Il2CppType IEditorSmartTerrainTrackerBehaviourU5BU5D_t5515_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7494_ICollection_1_CopyTo_m42526_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSmartTerrainTrackerBehaviourU5BU5D_t5515_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42526_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42526_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7494_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7494_ICollection_1_CopyTo_m42526_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42526_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t165_0_0_0;
static ParameterInfo ICollection_1_t7494_ICollection_1_Remove_m42527_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSmartTerrainTrackerBehaviour_t165_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42527_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42527_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7494_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7494_ICollection_1_Remove_m42527_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42527_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7494_MethodInfos[] =
{
	&ICollection_1_get_Count_m42521_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42522_MethodInfo,
	&ICollection_1_Add_m42523_MethodInfo,
	&ICollection_1_Clear_m42524_MethodInfo,
	&ICollection_1_Contains_m42525_MethodInfo,
	&ICollection_1_CopyTo_m42526_MethodInfo,
	&ICollection_1_Remove_m42527_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7496_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7494_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7496_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7494_0_0_0;
extern Il2CppType ICollection_1_t7494_1_0_0;
struct ICollection_1_t7494;
extern Il2CppGenericClass ICollection_1_t7494_GenericClass;
TypeInfo ICollection_1_t7494_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7494_MethodInfos/* methods */
	, ICollection_1_t7494_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7494_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7494_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7494_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7494_0_0_0/* byval_arg */
	, &ICollection_1_t7494_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7494_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>
extern Il2CppType IEnumerator_1_t5887_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42528_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42528_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7496_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5887_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42528_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7496_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42528_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7496_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7496_0_0_0;
extern Il2CppType IEnumerable_1_t7496_1_0_0;
struct IEnumerable_1_t7496;
extern Il2CppGenericClass IEnumerable_1_t7496_GenericClass;
TypeInfo IEnumerable_1_t7496_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7496_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7496_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7496_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7496_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7496_0_0_0/* byval_arg */
	, &IEnumerable_1_t7496_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7496_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5887_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42529_MethodInfo;
static PropertyInfo IEnumerator_1_t5887____Current_PropertyInfo = 
{
	&IEnumerator_1_t5887_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42529_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5887_PropertyInfos[] =
{
	&IEnumerator_1_t5887____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t165_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42529_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42529_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5887_il2cpp_TypeInfo/* declaring_type */
	, &IEditorSmartTerrainTrackerBehaviour_t165_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42529_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5887_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42529_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5887_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5887_0_0_0;
extern Il2CppType IEnumerator_1_t5887_1_0_0;
struct IEnumerator_1_t5887;
extern Il2CppGenericClass IEnumerator_1_t5887_GenericClass;
TypeInfo IEnumerator_1_t5887_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5887_MethodInfos/* methods */
	, IEnumerator_1_t5887_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5887_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5887_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5887_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5887_0_0_0/* byval_arg */
	, &IEnumerator_1_t5887_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5887_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_102.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2928_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_102MethodDeclarations.h"

extern TypeInfo IEditorSmartTerrainTrackerBehaviour_t165_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15110_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorSmartTerrainTrackerBehaviour_t165_m32633_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorSmartTerrainTrackerBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorSmartTerrainTrackerBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorSmartTerrainTrackerBehaviour_t165_m32633(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2928____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2928_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2928, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2928____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2928_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2928, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2928_FieldInfos[] =
{
	&InternalEnumerator_1_t2928____array_0_FieldInfo,
	&InternalEnumerator_1_t2928____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15107_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2928____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2928_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15107_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2928____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2928_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15110_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2928_PropertyInfos[] =
{
	&InternalEnumerator_1_t2928____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2928____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2928_InternalEnumerator_1__ctor_m15106_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15106_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15106_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2928_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2928_InternalEnumerator_1__ctor_m15106_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15106_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15107_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15107_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2928_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15107_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15108_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15108_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2928_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15108_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15109_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15109_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2928_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15109_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t165_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15110_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15110_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2928_il2cpp_TypeInfo/* declaring_type */
	, &IEditorSmartTerrainTrackerBehaviour_t165_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15110_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2928_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15106_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15107_MethodInfo,
	&InternalEnumerator_1_Dispose_m15108_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15109_MethodInfo,
	&InternalEnumerator_1_get_Current_m15110_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15109_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15108_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2928_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15107_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15109_MethodInfo,
	&InternalEnumerator_1_Dispose_m15108_MethodInfo,
	&InternalEnumerator_1_get_Current_m15110_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2928_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5887_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2928_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5887_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorSmartTerrainTrackerBehaviour_t165_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2928_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15110_MethodInfo/* Method Usage */,
	&IEditorSmartTerrainTrackerBehaviour_t165_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorSmartTerrainTrackerBehaviour_t165_m32633_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2928_0_0_0;
extern Il2CppType InternalEnumerator_1_t2928_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2928_GenericClass;
TypeInfo InternalEnumerator_1_t2928_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2928_MethodInfos/* methods */
	, InternalEnumerator_1_t2928_PropertyInfos/* properties */
	, InternalEnumerator_1_t2928_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2928_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2928_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2928_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2928_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2928_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2928_1_0_0/* this_arg */
	, InternalEnumerator_1_t2928_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2928_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2928_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2928)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7495_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>
extern MethodInfo IList_1_get_Item_m42530_MethodInfo;
extern MethodInfo IList_1_set_Item_m42531_MethodInfo;
static PropertyInfo IList_1_t7495____Item_PropertyInfo = 
{
	&IList_1_t7495_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42530_MethodInfo/* get */
	, &IList_1_set_Item_m42531_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7495_PropertyInfos[] =
{
	&IList_1_t7495____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t165_0_0_0;
static ParameterInfo IList_1_t7495_IList_1_IndexOf_m42532_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSmartTerrainTrackerBehaviour_t165_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42532_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42532_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7495_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7495_IList_1_IndexOf_m42532_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42532_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t165_0_0_0;
static ParameterInfo IList_1_t7495_IList_1_Insert_m42533_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorSmartTerrainTrackerBehaviour_t165_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42533_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42533_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7495_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7495_IList_1_Insert_m42533_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42533_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7495_IList_1_RemoveAt_m42534_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42534_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42534_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7495_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7495_IList_1_RemoveAt_m42534_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42534_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7495_IList_1_get_Item_m42530_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t165_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42530_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42530_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7495_il2cpp_TypeInfo/* declaring_type */
	, &IEditorSmartTerrainTrackerBehaviour_t165_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7495_IList_1_get_Item_m42530_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42530_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorSmartTerrainTrackerBehaviour_t165_0_0_0;
static ParameterInfo IList_1_t7495_IList_1_set_Item_m42531_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorSmartTerrainTrackerBehaviour_t165_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42531_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSmartTerrainTrackerBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42531_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7495_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7495_IList_1_set_Item_m42531_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42531_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7495_MethodInfos[] =
{
	&IList_1_IndexOf_m42532_MethodInfo,
	&IList_1_Insert_m42533_MethodInfo,
	&IList_1_RemoveAt_m42534_MethodInfo,
	&IList_1_get_Item_m42530_MethodInfo,
	&IList_1_set_Item_m42531_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7495_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7494_il2cpp_TypeInfo,
	&IEnumerable_1_t7496_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7495_0_0_0;
extern Il2CppType IList_1_t7495_1_0_0;
struct IList_1_t7495;
extern Il2CppGenericClass IList_1_t7495_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7495_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7495_MethodInfos/* methods */
	, IList_1_t7495_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7495_il2cpp_TypeInfo/* element_class */
	, IList_1_t7495_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7495_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7495_0_0_0/* byval_arg */
	, &IList_1_t7495_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7495_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_35.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2929_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_35MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_31.h"
extern TypeInfo InvokableCall_1_t2930_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_31MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15113_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15115_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2929____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2929_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2929, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2929_FieldInfos[] =
{
	&CachedInvokableCall_1_t2929____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType SmartTerrainTrackerBehaviour_t75_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2929_CachedInvokableCall_1__ctor_m15111_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t75_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15111_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15111_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2929_CachedInvokableCall_1__ctor_m15111_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15111_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2929_CachedInvokableCall_1_Invoke_m15112_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15112_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15112_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2929_CachedInvokableCall_1_Invoke_m15112_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15112_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2929_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15111_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15112_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15112_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15116_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2929_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15112_MethodInfo,
	&InvokableCall_1_Find_m15116_MethodInfo,
};
extern Il2CppType UnityAction_1_t2931_0_0_0;
extern TypeInfo UnityAction_1_t2931_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisSmartTerrainTrackerBehaviour_t75_m32643_MethodInfo;
extern TypeInfo SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15118_MethodInfo;
extern TypeInfo SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2929_RGCTXData[8] = 
{
	&UnityAction_1_t2931_0_0_0/* Type Usage */,
	&UnityAction_1_t2931_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSmartTerrainTrackerBehaviour_t75_m32643_MethodInfo/* Method Usage */,
	&SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15118_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15113_MethodInfo/* Method Usage */,
	&SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15115_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2929_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2929_1_0_0;
struct CachedInvokableCall_1_t2929;
extern Il2CppGenericClass CachedInvokableCall_1_t2929_GenericClass;
TypeInfo CachedInvokableCall_1_t2929_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2929_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2929_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2930_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2929_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2929_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2929_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2929_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2929_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2929_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2929_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2929)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_38.h"
extern TypeInfo UnityAction_1_t2931_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_38MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.SmartTerrainTrackerBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.SmartTerrainTrackerBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisSmartTerrainTrackerBehaviour_t75_m32643(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>
extern Il2CppType UnityAction_1_t2931_0_0_1;
FieldInfo InvokableCall_1_t2930____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2931_0_0_1/* type */
	, &InvokableCall_1_t2930_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2930, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2930_FieldInfos[] =
{
	&InvokableCall_1_t2930____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2930_InvokableCall_1__ctor_m15113_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15113_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15113_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2930_InvokableCall_1__ctor_m15113_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15113_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2931_0_0_0;
static ParameterInfo InvokableCall_1_t2930_InvokableCall_1__ctor_m15114_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2931_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15114_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15114_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2930_InvokableCall_1__ctor_m15114_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15114_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2930_InvokableCall_1_Invoke_m15115_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15115_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15115_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2930_InvokableCall_1_Invoke_m15115_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15115_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2930_InvokableCall_1_Find_m15116_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15116_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15116_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2930_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2930_InvokableCall_1_Find_m15116_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15116_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2930_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15113_MethodInfo,
	&InvokableCall_1__ctor_m15114_MethodInfo,
	&InvokableCall_1_Invoke_m15115_MethodInfo,
	&InvokableCall_1_Find_m15116_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2930_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m15115_MethodInfo,
	&InvokableCall_1_Find_m15116_MethodInfo,
};
extern TypeInfo UnityAction_1_t2931_il2cpp_TypeInfo;
extern TypeInfo SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2930_RGCTXData[5] = 
{
	&UnityAction_1_t2931_0_0_0/* Type Usage */,
	&UnityAction_1_t2931_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSmartTerrainTrackerBehaviour_t75_m32643_MethodInfo/* Method Usage */,
	&SmartTerrainTrackerBehaviour_t75_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15118_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2930_0_0_0;
extern Il2CppType InvokableCall_1_t2930_1_0_0;
struct InvokableCall_1_t2930;
extern Il2CppGenericClass InvokableCall_1_t2930_GenericClass;
TypeInfo InvokableCall_1_t2930_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2930_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2930_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2930_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2930_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2930_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2930_0_0_0/* byval_arg */
	, &InvokableCall_1_t2930_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2930_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2930_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2930)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2931_UnityAction_1__ctor_m15117_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15117_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15117_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2931_UnityAction_1__ctor_m15117_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15117_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerBehaviour_t75_0_0_0;
static ParameterInfo UnityAction_1_t2931_UnityAction_1_Invoke_m15118_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t75_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15118_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15118_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2931_UnityAction_1_Invoke_m15118_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15118_GenericMethod/* genericMethod */

};
extern Il2CppType SmartTerrainTrackerBehaviour_t75_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2931_UnityAction_1_BeginInvoke_m15119_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SmartTerrainTrackerBehaviour_t75_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15119_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15119_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2931_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2931_UnityAction_1_BeginInvoke_m15119_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15119_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2931_UnityAction_1_EndInvoke_m15120_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15120_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SmartTerrainTrackerBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15120_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2931_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2931_UnityAction_1_EndInvoke_m15120_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15120_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2931_MethodInfos[] =
{
	&UnityAction_1__ctor_m15117_MethodInfo,
	&UnityAction_1_Invoke_m15118_MethodInfo,
	&UnityAction_1_BeginInvoke_m15119_MethodInfo,
	&UnityAction_1_EndInvoke_m15120_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15119_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15120_MethodInfo;
static MethodInfo* UnityAction_1_t2931_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m15118_MethodInfo,
	&UnityAction_1_BeginInvoke_m15119_MethodInfo,
	&UnityAction_1_EndInvoke_m15120_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2931_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2931_1_0_0;
struct UnityAction_1_t2931;
extern Il2CppGenericClass UnityAction_1_t2931_GenericClass;
TypeInfo UnityAction_1_t2931_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2931_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2931_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2931_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2931_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2931_0_0_0/* byval_arg */
	, &UnityAction_1_t2931_1_0_0/* this_arg */
	, UnityAction_1_t2931_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2931_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2931)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5889_il2cpp_TypeInfo;

// Vuforia.SurfaceBehaviour
#include "AssemblyU2DCSharp_Vuforia_SurfaceBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.SurfaceBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.SurfaceBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42535_MethodInfo;
static PropertyInfo IEnumerator_1_t5889____Current_PropertyInfo = 
{
	&IEnumerator_1_t5889_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42535_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5889_PropertyInfos[] =
{
	&IEnumerator_1_t5889____Current_PropertyInfo,
	NULL
};
extern Il2CppType SurfaceBehaviour_t40_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42535_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.SurfaceBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42535_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5889_il2cpp_TypeInfo/* declaring_type */
	, &SurfaceBehaviour_t40_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42535_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5889_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42535_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5889_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5889_0_0_0;
extern Il2CppType IEnumerator_1_t5889_1_0_0;
struct IEnumerator_1_t5889;
extern Il2CppGenericClass IEnumerator_1_t5889_GenericClass;
TypeInfo IEnumerator_1_t5889_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5889_MethodInfos/* methods */
	, IEnumerator_1_t5889_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5889_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5889_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5889_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5889_0_0_0/* byval_arg */
	, &IEnumerator_1_t5889_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5889_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_103.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2932_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_103MethodDeclarations.h"

extern TypeInfo SurfaceBehaviour_t40_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15125_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSurfaceBehaviour_t40_m32645_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.SurfaceBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.SurfaceBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisSurfaceBehaviour_t40_m32645(__this, p0, method) (SurfaceBehaviour_t40 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2932____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2932, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2932____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2932, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2932_FieldInfos[] =
{
	&InternalEnumerator_1_t2932____array_0_FieldInfo,
	&InternalEnumerator_1_t2932____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15122_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2932____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2932_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15122_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2932____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2932_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15125_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2932_PropertyInfos[] =
{
	&InternalEnumerator_1_t2932____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2932____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2932_InternalEnumerator_1__ctor_m15121_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15121_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15121_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2932_InternalEnumerator_1__ctor_m15121_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15121_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15122_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15122_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15122_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15123_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15123_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15123_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15124_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15124_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15124_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceBehaviour_t40_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15125_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.SurfaceBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15125_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* declaring_type */
	, &SurfaceBehaviour_t40_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15125_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2932_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15121_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15122_MethodInfo,
	&InternalEnumerator_1_Dispose_m15123_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15124_MethodInfo,
	&InternalEnumerator_1_get_Current_m15125_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15124_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15123_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2932_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15122_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15124_MethodInfo,
	&InternalEnumerator_1_Dispose_m15123_MethodInfo,
	&InternalEnumerator_1_get_Current_m15125_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2932_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5889_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2932_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5889_il2cpp_TypeInfo, 7},
};
extern TypeInfo SurfaceBehaviour_t40_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2932_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15125_MethodInfo/* Method Usage */,
	&SurfaceBehaviour_t40_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSurfaceBehaviour_t40_m32645_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2932_0_0_0;
extern Il2CppType InternalEnumerator_1_t2932_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2932_GenericClass;
TypeInfo InternalEnumerator_1_t2932_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2932_MethodInfos/* methods */
	, InternalEnumerator_1_t2932_PropertyInfos/* properties */
	, InternalEnumerator_1_t2932_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2932_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2932_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2932_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2932_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2932_1_0_0/* this_arg */
	, InternalEnumerator_1_t2932_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2932_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2932_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2932)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7497_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>
extern MethodInfo ICollection_1_get_Count_m42536_MethodInfo;
static PropertyInfo ICollection_1_t7497____Count_PropertyInfo = 
{
	&ICollection_1_t7497_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42536_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42537_MethodInfo;
static PropertyInfo ICollection_1_t7497____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7497_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42537_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7497_PropertyInfos[] =
{
	&ICollection_1_t7497____Count_PropertyInfo,
	&ICollection_1_t7497____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42536_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42536_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7497_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42536_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42537_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42537_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7497_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42537_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceBehaviour_t40_0_0_0;
extern Il2CppType SurfaceBehaviour_t40_0_0_0;
static ParameterInfo ICollection_1_t7497_ICollection_1_Add_m42538_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t40_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42538_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42538_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7497_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7497_ICollection_1_Add_m42538_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42538_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42539_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42539_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7497_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42539_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceBehaviour_t40_0_0_0;
static ParameterInfo ICollection_1_t7497_ICollection_1_Contains_m42540_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t40_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42540_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42540_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7497_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7497_ICollection_1_Contains_m42540_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42540_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceBehaviourU5BU5D_t5190_0_0_0;
extern Il2CppType SurfaceBehaviourU5BU5D_t5190_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7497_ICollection_1_CopyTo_m42541_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviourU5BU5D_t5190_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42541_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42541_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7497_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7497_ICollection_1_CopyTo_m42541_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42541_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceBehaviour_t40_0_0_0;
static ParameterInfo ICollection_1_t7497_ICollection_1_Remove_m42542_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t40_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42542_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42542_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7497_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7497_ICollection_1_Remove_m42542_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42542_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7497_MethodInfos[] =
{
	&ICollection_1_get_Count_m42536_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42537_MethodInfo,
	&ICollection_1_Add_m42538_MethodInfo,
	&ICollection_1_Clear_m42539_MethodInfo,
	&ICollection_1_Contains_m42540_MethodInfo,
	&ICollection_1_CopyTo_m42541_MethodInfo,
	&ICollection_1_Remove_m42542_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7499_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7497_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7499_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7497_0_0_0;
extern Il2CppType ICollection_1_t7497_1_0_0;
struct ICollection_1_t7497;
extern Il2CppGenericClass ICollection_1_t7497_GenericClass;
TypeInfo ICollection_1_t7497_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7497_MethodInfos/* methods */
	, ICollection_1_t7497_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7497_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7497_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7497_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7497_0_0_0/* byval_arg */
	, &ICollection_1_t7497_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7497_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SurfaceBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.SurfaceBehaviour>
extern Il2CppType IEnumerator_1_t5889_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42543_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SurfaceBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42543_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7499_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5889_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42543_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7499_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42543_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7499_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7499_0_0_0;
extern Il2CppType IEnumerable_1_t7499_1_0_0;
struct IEnumerable_1_t7499;
extern Il2CppGenericClass IEnumerable_1_t7499_GenericClass;
TypeInfo IEnumerable_1_t7499_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7499_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7499_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7499_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7499_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7499_0_0_0/* byval_arg */
	, &IEnumerable_1_t7499_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7499_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7498_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>
extern MethodInfo IList_1_get_Item_m42544_MethodInfo;
extern MethodInfo IList_1_set_Item_m42545_MethodInfo;
static PropertyInfo IList_1_t7498____Item_PropertyInfo = 
{
	&IList_1_t7498_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42544_MethodInfo/* get */
	, &IList_1_set_Item_m42545_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7498_PropertyInfos[] =
{
	&IList_1_t7498____Item_PropertyInfo,
	NULL
};
extern Il2CppType SurfaceBehaviour_t40_0_0_0;
static ParameterInfo IList_1_t7498_IList_1_IndexOf_m42546_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t40_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42546_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42546_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7498_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7498_IList_1_IndexOf_m42546_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42546_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SurfaceBehaviour_t40_0_0_0;
static ParameterInfo IList_1_t7498_IList_1_Insert_m42547_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t40_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42547_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42547_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7498_IList_1_Insert_m42547_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42547_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7498_IList_1_RemoveAt_m42548_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42548_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42548_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7498_IList_1_RemoveAt_m42548_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42548_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7498_IList_1_get_Item_m42544_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType SurfaceBehaviour_t40_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42544_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42544_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7498_il2cpp_TypeInfo/* declaring_type */
	, &SurfaceBehaviour_t40_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7498_IList_1_get_Item_m42544_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42544_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SurfaceBehaviour_t40_0_0_0;
static ParameterInfo IList_1_t7498_IList_1_set_Item_m42545_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t40_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42545_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42545_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7498_IList_1_set_Item_m42545_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42545_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7498_MethodInfos[] =
{
	&IList_1_IndexOf_m42546_MethodInfo,
	&IList_1_Insert_m42547_MethodInfo,
	&IList_1_RemoveAt_m42548_MethodInfo,
	&IList_1_get_Item_m42544_MethodInfo,
	&IList_1_set_Item_m42545_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7498_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7497_il2cpp_TypeInfo,
	&IEnumerable_1_t7499_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7498_0_0_0;
extern Il2CppType IList_1_t7498_1_0_0;
struct IList_1_t7498;
extern Il2CppGenericClass IList_1_t7498_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7498_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7498_MethodInfos/* methods */
	, IList_1_t7498_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7498_il2cpp_TypeInfo/* element_class */
	, IList_1_t7498_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7498_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7498_0_0_0/* byval_arg */
	, &IList_1_t7498_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7498_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7500_il2cpp_TypeInfo;

// Vuforia.SurfaceAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceAbstractBeha.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42549_MethodInfo;
static PropertyInfo ICollection_1_t7500____Count_PropertyInfo = 
{
	&ICollection_1_t7500_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42549_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42550_MethodInfo;
static PropertyInfo ICollection_1_t7500____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7500_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42550_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7500_PropertyInfos[] =
{
	&ICollection_1_t7500____Count_PropertyInfo,
	&ICollection_1_t7500____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42549_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42549_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7500_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42549_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42550_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42550_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7500_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42550_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceAbstractBehaviour_t77_0_0_0;
extern Il2CppType SurfaceAbstractBehaviour_t77_0_0_0;
static ParameterInfo ICollection_1_t7500_ICollection_1_Add_m42551_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceAbstractBehaviour_t77_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42551_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42551_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7500_ICollection_1_Add_m42551_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42551_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42552_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42552_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42552_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceAbstractBehaviour_t77_0_0_0;
static ParameterInfo ICollection_1_t7500_ICollection_1_Contains_m42553_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceAbstractBehaviour_t77_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42553_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42553_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7500_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7500_ICollection_1_Contains_m42553_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42553_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceAbstractBehaviourU5BU5D_t4092_0_0_0;
extern Il2CppType SurfaceAbstractBehaviourU5BU5D_t4092_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7500_ICollection_1_CopyTo_m42554_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceAbstractBehaviourU5BU5D_t4092_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42554_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42554_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7500_ICollection_1_CopyTo_m42554_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42554_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceAbstractBehaviour_t77_0_0_0;
static ParameterInfo ICollection_1_t7500_ICollection_1_Remove_m42555_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceAbstractBehaviour_t77_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42555_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.SurfaceAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42555_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7500_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7500_ICollection_1_Remove_m42555_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42555_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7500_MethodInfos[] =
{
	&ICollection_1_get_Count_m42549_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42550_MethodInfo,
	&ICollection_1_Add_m42551_MethodInfo,
	&ICollection_1_Clear_m42552_MethodInfo,
	&ICollection_1_Contains_m42553_MethodInfo,
	&ICollection_1_CopyTo_m42554_MethodInfo,
	&ICollection_1_Remove_m42555_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7502_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7500_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7502_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7500_0_0_0;
extern Il2CppType ICollection_1_t7500_1_0_0;
struct ICollection_1_t7500;
extern Il2CppGenericClass ICollection_1_t7500_GenericClass;
TypeInfo ICollection_1_t7500_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7500_MethodInfos/* methods */
	, ICollection_1_t7500_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7500_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7500_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7500_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7500_0_0_0/* byval_arg */
	, &ICollection_1_t7500_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7500_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SurfaceAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.SurfaceAbstractBehaviour>
extern Il2CppType IEnumerator_1_t4102_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42556_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.SurfaceAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42556_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7502_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t4102_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42556_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7502_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42556_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7502_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7502_0_0_0;
extern Il2CppType IEnumerable_1_t7502_1_0_0;
struct IEnumerable_1_t7502;
extern Il2CppGenericClass IEnumerable_1_t7502_GenericClass;
TypeInfo IEnumerable_1_t7502_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7502_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7502_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7502_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7502_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7502_0_0_0/* byval_arg */
	, &IEnumerable_1_t7502_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7502_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t4102_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.SurfaceAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42557_MethodInfo;
static PropertyInfo IEnumerator_1_t4102____Current_PropertyInfo = 
{
	&IEnumerator_1_t4102_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42557_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t4102_PropertyInfos[] =
{
	&IEnumerator_1_t4102____Current_PropertyInfo,
	NULL
};
extern Il2CppType SurfaceAbstractBehaviour_t77_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42557_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42557_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t4102_il2cpp_TypeInfo/* declaring_type */
	, &SurfaceAbstractBehaviour_t77_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42557_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t4102_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42557_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t4102_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t4102_0_0_0;
extern Il2CppType IEnumerator_1_t4102_1_0_0;
struct IEnumerator_1_t4102;
extern Il2CppGenericClass IEnumerator_1_t4102_GenericClass;
TypeInfo IEnumerator_1_t4102_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t4102_MethodInfos/* methods */
	, IEnumerator_1_t4102_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t4102_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t4102_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t4102_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t4102_0_0_0/* byval_arg */
	, &IEnumerator_1_t4102_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t4102_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_104.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2933_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_104MethodDeclarations.h"

extern TypeInfo SurfaceAbstractBehaviour_t77_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15130_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSurfaceAbstractBehaviour_t77_m32656_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.SurfaceAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.SurfaceAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisSurfaceAbstractBehaviour_t77_m32656(__this, p0, method) (SurfaceAbstractBehaviour_t77 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2933____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2933, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2933____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2933, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2933_FieldInfos[] =
{
	&InternalEnumerator_1_t2933____array_0_FieldInfo,
	&InternalEnumerator_1_t2933____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15127_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2933____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2933_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15127_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2933____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2933_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15130_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2933_PropertyInfos[] =
{
	&InternalEnumerator_1_t2933____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2933____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2933_InternalEnumerator_1__ctor_m15126_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15126_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15126_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2933_InternalEnumerator_1__ctor_m15126_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15126_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15127_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15127_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15127_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15128_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15128_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15128_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15129_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15129_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15129_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceAbstractBehaviour_t77_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15130_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.SurfaceAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15130_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* declaring_type */
	, &SurfaceAbstractBehaviour_t77_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15130_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2933_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15126_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15127_MethodInfo,
	&InternalEnumerator_1_Dispose_m15128_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15129_MethodInfo,
	&InternalEnumerator_1_get_Current_m15130_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15129_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15128_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2933_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15127_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15129_MethodInfo,
	&InternalEnumerator_1_Dispose_m15128_MethodInfo,
	&InternalEnumerator_1_get_Current_m15130_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2933_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t4102_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2933_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t4102_il2cpp_TypeInfo, 7},
};
extern TypeInfo SurfaceAbstractBehaviour_t77_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2933_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15130_MethodInfo/* Method Usage */,
	&SurfaceAbstractBehaviour_t77_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSurfaceAbstractBehaviour_t77_m32656_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2933_0_0_0;
extern Il2CppType InternalEnumerator_1_t2933_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2933_GenericClass;
TypeInfo InternalEnumerator_1_t2933_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2933_MethodInfos/* methods */
	, InternalEnumerator_1_t2933_PropertyInfos/* properties */
	, InternalEnumerator_1_t2933_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2933_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2933_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2933_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2933_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2933_1_0_0/* this_arg */
	, InternalEnumerator_1_t2933_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2933_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2933_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2933)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7501_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42558_MethodInfo;
extern MethodInfo IList_1_set_Item_m42559_MethodInfo;
static PropertyInfo IList_1_t7501____Item_PropertyInfo = 
{
	&IList_1_t7501_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42558_MethodInfo/* get */
	, &IList_1_set_Item_m42559_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7501_PropertyInfos[] =
{
	&IList_1_t7501____Item_PropertyInfo,
	NULL
};
extern Il2CppType SurfaceAbstractBehaviour_t77_0_0_0;
static ParameterInfo IList_1_t7501_IList_1_IndexOf_m42560_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceAbstractBehaviour_t77_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42560_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42560_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7501_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7501_IList_1_IndexOf_m42560_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42560_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SurfaceAbstractBehaviour_t77_0_0_0;
static ParameterInfo IList_1_t7501_IList_1_Insert_m42561_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SurfaceAbstractBehaviour_t77_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42561_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42561_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7501_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7501_IList_1_Insert_m42561_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42561_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7501_IList_1_RemoveAt_m42562_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42562_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42562_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7501_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7501_IList_1_RemoveAt_m42562_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42562_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7501_IList_1_get_Item_m42558_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType SurfaceAbstractBehaviour_t77_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42558_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42558_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7501_il2cpp_TypeInfo/* declaring_type */
	, &SurfaceAbstractBehaviour_t77_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7501_IList_1_get_Item_m42558_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42558_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SurfaceAbstractBehaviour_t77_0_0_0;
static ParameterInfo IList_1_t7501_IList_1_set_Item_m42559_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SurfaceAbstractBehaviour_t77_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42559_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.SurfaceAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42559_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7501_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7501_IList_1_set_Item_m42559_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42559_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7501_MethodInfos[] =
{
	&IList_1_IndexOf_m42560_MethodInfo,
	&IList_1_Insert_m42561_MethodInfo,
	&IList_1_RemoveAt_m42562_MethodInfo,
	&IList_1_get_Item_m42558_MethodInfo,
	&IList_1_set_Item_m42559_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7501_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7500_il2cpp_TypeInfo,
	&IEnumerable_1_t7502_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7501_0_0_0;
extern Il2CppType IList_1_t7501_1_0_0;
struct IList_1_t7501;
extern Il2CppGenericClass IList_1_t7501_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7501_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7501_MethodInfos/* methods */
	, IList_1_t7501_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7501_il2cpp_TypeInfo/* element_class */
	, IList_1_t7501_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7501_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7501_0_0_0/* byval_arg */
	, &IList_1_t7501_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7501_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7503_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>
extern MethodInfo ICollection_1_get_Count_m42563_MethodInfo;
static PropertyInfo ICollection_1_t7503____Count_PropertyInfo = 
{
	&ICollection_1_t7503_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42563_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42564_MethodInfo;
static PropertyInfo ICollection_1_t7503____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7503_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42564_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7503_PropertyInfos[] =
{
	&ICollection_1_t7503____Count_PropertyInfo,
	&ICollection_1_t7503____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42563_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42563_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7503_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42563_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42564_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42564_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7503_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42564_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSurfaceBehaviour_t166_0_0_0;
extern Il2CppType IEditorSurfaceBehaviour_t166_0_0_0;
static ParameterInfo ICollection_1_t7503_ICollection_1_Add_m42565_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSurfaceBehaviour_t166_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42565_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42565_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7503_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7503_ICollection_1_Add_m42565_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42565_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42566_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42566_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7503_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42566_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSurfaceBehaviour_t166_0_0_0;
static ParameterInfo ICollection_1_t7503_ICollection_1_Contains_m42567_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSurfaceBehaviour_t166_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42567_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42567_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7503_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7503_ICollection_1_Contains_m42567_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42567_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSurfaceBehaviourU5BU5D_t5516_0_0_0;
extern Il2CppType IEditorSurfaceBehaviourU5BU5D_t5516_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7503_ICollection_1_CopyTo_m42568_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSurfaceBehaviourU5BU5D_t5516_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42568_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42568_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7503_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7503_ICollection_1_CopyTo_m42568_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42568_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSurfaceBehaviour_t166_0_0_0;
static ParameterInfo ICollection_1_t7503_ICollection_1_Remove_m42569_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSurfaceBehaviour_t166_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42569_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorSurfaceBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42569_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7503_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7503_ICollection_1_Remove_m42569_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42569_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7503_MethodInfos[] =
{
	&ICollection_1_get_Count_m42563_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42564_MethodInfo,
	&ICollection_1_Add_m42565_MethodInfo,
	&ICollection_1_Clear_m42566_MethodInfo,
	&ICollection_1_Contains_m42567_MethodInfo,
	&ICollection_1_CopyTo_m42568_MethodInfo,
	&ICollection_1_Remove_m42569_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7505_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7503_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7505_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7503_0_0_0;
extern Il2CppType ICollection_1_t7503_1_0_0;
struct ICollection_1_t7503;
extern Il2CppGenericClass ICollection_1_t7503_GenericClass;
TypeInfo ICollection_1_t7503_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7503_MethodInfos/* methods */
	, ICollection_1_t7503_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7503_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7503_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7503_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7503_0_0_0/* byval_arg */
	, &ICollection_1_t7503_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7503_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorSurfaceBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorSurfaceBehaviour>
extern Il2CppType IEnumerator_1_t5891_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42570_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorSurfaceBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42570_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7505_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5891_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42570_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7505_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42570_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7505_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7505_0_0_0;
extern Il2CppType IEnumerable_1_t7505_1_0_0;
struct IEnumerable_1_t7505;
extern Il2CppGenericClass IEnumerable_1_t7505_GenericClass;
TypeInfo IEnumerable_1_t7505_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7505_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7505_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7505_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7505_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7505_0_0_0/* byval_arg */
	, &IEnumerable_1_t7505_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7505_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5891_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorSurfaceBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42571_MethodInfo;
static PropertyInfo IEnumerator_1_t5891____Current_PropertyInfo = 
{
	&IEnumerator_1_t5891_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42571_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5891_PropertyInfos[] =
{
	&IEnumerator_1_t5891____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorSurfaceBehaviour_t166_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42571_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42571_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5891_il2cpp_TypeInfo/* declaring_type */
	, &IEditorSurfaceBehaviour_t166_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42571_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5891_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42571_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5891_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5891_0_0_0;
extern Il2CppType IEnumerator_1_t5891_1_0_0;
struct IEnumerator_1_t5891;
extern Il2CppGenericClass IEnumerator_1_t5891_GenericClass;
TypeInfo IEnumerator_1_t5891_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5891_MethodInfos/* methods */
	, IEnumerator_1_t5891_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5891_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5891_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5891_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5891_0_0_0/* byval_arg */
	, &IEnumerator_1_t5891_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5891_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_105.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2934_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_105MethodDeclarations.h"

extern TypeInfo IEditorSurfaceBehaviour_t166_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15135_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorSurfaceBehaviour_t166_m32667_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorSurfaceBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorSurfaceBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorSurfaceBehaviour_t166_m32667(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2934____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2934_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2934, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2934____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2934_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2934, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2934_FieldInfos[] =
{
	&InternalEnumerator_1_t2934____array_0_FieldInfo,
	&InternalEnumerator_1_t2934____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15132_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2934____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2934_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15132_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2934____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2934_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15135_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2934_PropertyInfos[] =
{
	&InternalEnumerator_1_t2934____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2934____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2934_InternalEnumerator_1__ctor_m15131_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15131_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15131_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2934_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2934_InternalEnumerator_1__ctor_m15131_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15131_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15132_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15132_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2934_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15132_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15133_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15133_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2934_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15133_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15134_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15134_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2934_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15134_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorSurfaceBehaviour_t166_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15135_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorSurfaceBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15135_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2934_il2cpp_TypeInfo/* declaring_type */
	, &IEditorSurfaceBehaviour_t166_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15135_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2934_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15131_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15132_MethodInfo,
	&InternalEnumerator_1_Dispose_m15133_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15134_MethodInfo,
	&InternalEnumerator_1_get_Current_m15135_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15134_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15133_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2934_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15132_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15134_MethodInfo,
	&InternalEnumerator_1_Dispose_m15133_MethodInfo,
	&InternalEnumerator_1_get_Current_m15135_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2934_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5891_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2934_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5891_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorSurfaceBehaviour_t166_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2934_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15135_MethodInfo/* Method Usage */,
	&IEditorSurfaceBehaviour_t166_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorSurfaceBehaviour_t166_m32667_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2934_0_0_0;
extern Il2CppType InternalEnumerator_1_t2934_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2934_GenericClass;
TypeInfo InternalEnumerator_1_t2934_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2934_MethodInfos/* methods */
	, InternalEnumerator_1_t2934_PropertyInfos/* properties */
	, InternalEnumerator_1_t2934_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2934_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2934_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2934_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2934_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2934_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2934_1_0_0/* this_arg */
	, InternalEnumerator_1_t2934_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2934_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2934_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2934)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7504_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>
extern MethodInfo IList_1_get_Item_m42572_MethodInfo;
extern MethodInfo IList_1_set_Item_m42573_MethodInfo;
static PropertyInfo IList_1_t7504____Item_PropertyInfo = 
{
	&IList_1_t7504_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42572_MethodInfo/* get */
	, &IList_1_set_Item_m42573_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7504_PropertyInfos[] =
{
	&IList_1_t7504____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorSurfaceBehaviour_t166_0_0_0;
static ParameterInfo IList_1_t7504_IList_1_IndexOf_m42574_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorSurfaceBehaviour_t166_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42574_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42574_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7504_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7504_IList_1_IndexOf_m42574_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42574_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorSurfaceBehaviour_t166_0_0_0;
static ParameterInfo IList_1_t7504_IList_1_Insert_m42575_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorSurfaceBehaviour_t166_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42575_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42575_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7504_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7504_IList_1_Insert_m42575_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42575_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7504_IList_1_RemoveAt_m42576_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42576_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42576_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7504_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7504_IList_1_RemoveAt_m42576_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42576_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7504_IList_1_get_Item_m42572_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEditorSurfaceBehaviour_t166_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42572_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42572_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7504_il2cpp_TypeInfo/* declaring_type */
	, &IEditorSurfaceBehaviour_t166_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7504_IList_1_get_Item_m42572_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42572_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorSurfaceBehaviour_t166_0_0_0;
static ParameterInfo IList_1_t7504_IList_1_set_Item_m42573_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorSurfaceBehaviour_t166_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42573_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorSurfaceBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42573_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7504_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7504_IList_1_set_Item_m42573_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42573_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7504_MethodInfos[] =
{
	&IList_1_IndexOf_m42574_MethodInfo,
	&IList_1_Insert_m42575_MethodInfo,
	&IList_1_RemoveAt_m42576_MethodInfo,
	&IList_1_get_Item_m42572_MethodInfo,
	&IList_1_set_Item_m42573_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7504_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7503_il2cpp_TypeInfo,
	&IEnumerable_1_t7505_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7504_0_0_0;
extern Il2CppType IList_1_t7504_1_0_0;
struct IList_1_t7504;
extern Il2CppGenericClass IList_1_t7504_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7504_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7504_MethodInfos/* methods */
	, IList_1_t7504_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7504_il2cpp_TypeInfo/* element_class */
	, IList_1_t7504_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7504_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7504_0_0_0/* byval_arg */
	, &IList_1_t7504_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7504_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_36.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2935_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_36MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_32.h"
extern TypeInfo InvokableCall_1_t2936_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_32MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15138_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15140_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2935____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2935_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2935, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2935_FieldInfos[] =
{
	&CachedInvokableCall_1_t2935____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType SurfaceBehaviour_t40_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2935_CachedInvokableCall_1__ctor_m15136_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t40_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15136_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15136_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2935_CachedInvokableCall_1__ctor_m15136_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15136_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2935_CachedInvokableCall_1_Invoke_m15137_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15137_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15137_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2935_CachedInvokableCall_1_Invoke_m15137_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15137_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2935_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15136_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15137_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15137_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15141_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2935_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15137_MethodInfo,
	&InvokableCall_1_Find_m15141_MethodInfo,
};
extern Il2CppType UnityAction_1_t2937_0_0_0;
extern TypeInfo UnityAction_1_t2937_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisSurfaceBehaviour_t40_m32677_MethodInfo;
extern TypeInfo SurfaceBehaviour_t40_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15143_MethodInfo;
extern TypeInfo SurfaceBehaviour_t40_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2935_RGCTXData[8] = 
{
	&UnityAction_1_t2937_0_0_0/* Type Usage */,
	&UnityAction_1_t2937_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSurfaceBehaviour_t40_m32677_MethodInfo/* Method Usage */,
	&SurfaceBehaviour_t40_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15143_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15138_MethodInfo/* Method Usage */,
	&SurfaceBehaviour_t40_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15140_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2935_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2935_1_0_0;
struct CachedInvokableCall_1_t2935;
extern Il2CppGenericClass CachedInvokableCall_1_t2935_GenericClass;
TypeInfo CachedInvokableCall_1_t2935_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2935_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2935_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2936_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2935_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2935_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2935_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2935_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2935_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2935_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2935_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2935)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_39.h"
extern TypeInfo UnityAction_1_t2937_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_39MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.SurfaceBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.SurfaceBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisSurfaceBehaviour_t40_m32677(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>
extern Il2CppType UnityAction_1_t2937_0_0_1;
FieldInfo InvokableCall_1_t2936____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2937_0_0_1/* type */
	, &InvokableCall_1_t2936_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2936, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2936_FieldInfos[] =
{
	&InvokableCall_1_t2936____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2936_InvokableCall_1__ctor_m15138_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15138_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m15138_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2936_InvokableCall_1__ctor_m15138_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15138_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2937_0_0_0;
static ParameterInfo InvokableCall_1_t2936_InvokableCall_1__ctor_m15139_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2937_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m15139_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m15139_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2936_InvokableCall_1__ctor_m15139_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m15139_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2936_InvokableCall_1_Invoke_m15140_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m15140_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m15140_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2936_InvokableCall_1_Invoke_m15140_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m15140_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2936_InvokableCall_1_Find_m15141_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m15141_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m15141_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2936_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2936_InvokableCall_1_Find_m15141_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m15141_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2936_MethodInfos[] =
{
	&InvokableCall_1__ctor_m15138_MethodInfo,
	&InvokableCall_1__ctor_m15139_MethodInfo,
	&InvokableCall_1_Invoke_m15140_MethodInfo,
	&InvokableCall_1_Find_m15141_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2936_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m15140_MethodInfo,
	&InvokableCall_1_Find_m15141_MethodInfo,
};
extern TypeInfo UnityAction_1_t2937_il2cpp_TypeInfo;
extern TypeInfo SurfaceBehaviour_t40_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2936_RGCTXData[5] = 
{
	&UnityAction_1_t2937_0_0_0/* Type Usage */,
	&UnityAction_1_t2937_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSurfaceBehaviour_t40_m32677_MethodInfo/* Method Usage */,
	&SurfaceBehaviour_t40_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15143_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2936_0_0_0;
extern Il2CppType InvokableCall_1_t2936_1_0_0;
struct InvokableCall_1_t2936;
extern Il2CppGenericClass InvokableCall_1_t2936_GenericClass;
TypeInfo InvokableCall_1_t2936_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2936_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2936_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2936_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2936_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2936_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2936_0_0_0/* byval_arg */
	, &InvokableCall_1_t2936_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2936_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2936_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2936)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2937_UnityAction_1__ctor_m15142_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m15142_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m15142_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2937_UnityAction_1__ctor_m15142_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m15142_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceBehaviour_t40_0_0_0;
static ParameterInfo UnityAction_1_t2937_UnityAction_1_Invoke_m15143_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t40_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m15143_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m15143_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2937_UnityAction_1_Invoke_m15143_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m15143_GenericMethod/* genericMethod */

};
extern Il2CppType SurfaceBehaviour_t40_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2937_UnityAction_1_BeginInvoke_m15144_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SurfaceBehaviour_t40_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m15144_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m15144_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2937_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2937_UnityAction_1_BeginInvoke_m15144_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m15144_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2937_UnityAction_1_EndInvoke_m15145_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m15145_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m15145_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2937_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2937_UnityAction_1_EndInvoke_m15145_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m15145_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2937_MethodInfos[] =
{
	&UnityAction_1__ctor_m15142_MethodInfo,
	&UnityAction_1_Invoke_m15143_MethodInfo,
	&UnityAction_1_BeginInvoke_m15144_MethodInfo,
	&UnityAction_1_EndInvoke_m15145_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m15144_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m15145_MethodInfo;
static MethodInfo* UnityAction_1_t2937_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m15143_MethodInfo,
	&UnityAction_1_BeginInvoke_m15144_MethodInfo,
	&UnityAction_1_EndInvoke_m15145_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2937_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2937_1_0_0;
struct UnityAction_1_t2937;
extern Il2CppGenericClass UnityAction_1_t2937_GenericClass;
TypeInfo UnityAction_1_t2937_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2937_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2937_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2937_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2937_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2937_0_0_0/* byval_arg */
	, &UnityAction_1_t2937_1_0_0/* this_arg */
	, UnityAction_1_t2937_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2937_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2937)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5893_il2cpp_TypeInfo;

// Vuforia.TextRecoBehaviour
#include "AssemblyU2DCSharp_Vuforia_TextRecoBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.TextRecoBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.TextRecoBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42577_MethodInfo;
static PropertyInfo IEnumerator_1_t5893____Current_PropertyInfo = 
{
	&IEnumerator_1_t5893_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42577_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5893_PropertyInfos[] =
{
	&IEnumerator_1_t5893____Current_PropertyInfo,
	NULL
};
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42577_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.TextRecoBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42577_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5893_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoBehaviour_t78_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42577_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5893_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42577_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5893_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5893_0_0_0;
extern Il2CppType IEnumerator_1_t5893_1_0_0;
struct IEnumerator_1_t5893;
extern Il2CppGenericClass IEnumerator_1_t5893_GenericClass;
TypeInfo IEnumerator_1_t5893_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5893_MethodInfos/* methods */
	, IEnumerator_1_t5893_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5893_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5893_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5893_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5893_0_0_0/* byval_arg */
	, &IEnumerator_1_t5893_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5893_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_106.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2938_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_106MethodDeclarations.h"

extern TypeInfo TextRecoBehaviour_t78_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15150_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTextRecoBehaviour_t78_m32679_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.TextRecoBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.TextRecoBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisTextRecoBehaviour_t78_m32679(__this, p0, method) (TextRecoBehaviour_t78 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2938____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2938, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2938____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2938, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2938_FieldInfos[] =
{
	&InternalEnumerator_1_t2938____array_0_FieldInfo,
	&InternalEnumerator_1_t2938____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15147_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2938____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2938_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15147_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2938____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2938_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15150_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2938_PropertyInfos[] =
{
	&InternalEnumerator_1_t2938____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2938____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2938_InternalEnumerator_1__ctor_m15146_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15146_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15146_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2938_InternalEnumerator_1__ctor_m15146_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15146_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15147_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15147_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15147_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15148_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15148_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15148_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15149_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15149_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15149_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15150_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.TextRecoBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15150_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoBehaviour_t78_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15150_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2938_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15146_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15147_MethodInfo,
	&InternalEnumerator_1_Dispose_m15148_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15149_MethodInfo,
	&InternalEnumerator_1_get_Current_m15150_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15149_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15148_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2938_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15147_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15149_MethodInfo,
	&InternalEnumerator_1_Dispose_m15148_MethodInfo,
	&InternalEnumerator_1_get_Current_m15150_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2938_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5893_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2938_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5893_il2cpp_TypeInfo, 7},
};
extern TypeInfo TextRecoBehaviour_t78_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2938_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15150_MethodInfo/* Method Usage */,
	&TextRecoBehaviour_t78_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTextRecoBehaviour_t78_m32679_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2938_0_0_0;
extern Il2CppType InternalEnumerator_1_t2938_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2938_GenericClass;
TypeInfo InternalEnumerator_1_t2938_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2938_MethodInfos/* methods */
	, InternalEnumerator_1_t2938_PropertyInfos/* properties */
	, InternalEnumerator_1_t2938_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2938_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2938_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2938_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2938_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2938_1_0_0/* this_arg */
	, InternalEnumerator_1_t2938_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2938_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2938_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2938)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7506_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>
extern MethodInfo ICollection_1_get_Count_m42578_MethodInfo;
static PropertyInfo ICollection_1_t7506____Count_PropertyInfo = 
{
	&ICollection_1_t7506_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42578_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42579_MethodInfo;
static PropertyInfo ICollection_1_t7506____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7506_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42579_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7506_PropertyInfos[] =
{
	&ICollection_1_t7506____Count_PropertyInfo,
	&ICollection_1_t7506____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42578_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42578_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7506_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42578_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42579_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42579_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7506_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42579_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
static ParameterInfo ICollection_1_t7506_ICollection_1_Add_m42580_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t78_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42580_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42580_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7506_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7506_ICollection_1_Add_m42580_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42580_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42581_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42581_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7506_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42581_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
static ParameterInfo ICollection_1_t7506_ICollection_1_Contains_m42582_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t78_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42582_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42582_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7506_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7506_ICollection_1_Contains_m42582_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42582_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoBehaviourU5BU5D_t5191_0_0_0;
extern Il2CppType TextRecoBehaviourU5BU5D_t5191_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7506_ICollection_1_CopyTo_m42583_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviourU5BU5D_t5191_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42583_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42583_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7506_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7506_ICollection_1_CopyTo_m42583_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42583_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
static ParameterInfo ICollection_1_t7506_ICollection_1_Remove_m42584_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t78_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42584_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42584_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7506_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7506_ICollection_1_Remove_m42584_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42584_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7506_MethodInfos[] =
{
	&ICollection_1_get_Count_m42578_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42579_MethodInfo,
	&ICollection_1_Add_m42580_MethodInfo,
	&ICollection_1_Clear_m42581_MethodInfo,
	&ICollection_1_Contains_m42582_MethodInfo,
	&ICollection_1_CopyTo_m42583_MethodInfo,
	&ICollection_1_Remove_m42584_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7508_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7506_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7508_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7506_0_0_0;
extern Il2CppType ICollection_1_t7506_1_0_0;
struct ICollection_1_t7506;
extern Il2CppGenericClass ICollection_1_t7506_GenericClass;
TypeInfo ICollection_1_t7506_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7506_MethodInfos/* methods */
	, ICollection_1_t7506_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7506_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7506_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7506_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7506_0_0_0/* byval_arg */
	, &ICollection_1_t7506_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7506_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TextRecoBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.TextRecoBehaviour>
extern Il2CppType IEnumerator_1_t5893_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42585_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TextRecoBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42585_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7508_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5893_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42585_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7508_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42585_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7508_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7508_0_0_0;
extern Il2CppType IEnumerable_1_t7508_1_0_0;
struct IEnumerable_1_t7508;
extern Il2CppGenericClass IEnumerable_1_t7508_GenericClass;
TypeInfo IEnumerable_1_t7508_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7508_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7508_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7508_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7508_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7508_0_0_0/* byval_arg */
	, &IEnumerable_1_t7508_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7508_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7507_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>
extern MethodInfo IList_1_get_Item_m42586_MethodInfo;
extern MethodInfo IList_1_set_Item_m42587_MethodInfo;
static PropertyInfo IList_1_t7507____Item_PropertyInfo = 
{
	&IList_1_t7507_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42586_MethodInfo/* get */
	, &IList_1_set_Item_m42587_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7507_PropertyInfos[] =
{
	&IList_1_t7507____Item_PropertyInfo,
	NULL
};
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
static ParameterInfo IList_1_t7507_IList_1_IndexOf_m42588_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t78_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42588_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42588_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7507_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7507_IList_1_IndexOf_m42588_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42588_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
static ParameterInfo IList_1_t7507_IList_1_Insert_m42589_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t78_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42589_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42589_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7507_IList_1_Insert_m42589_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42589_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7507_IList_1_RemoveAt_m42590_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42590_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42590_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7507_IList_1_RemoveAt_m42590_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42590_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7507_IList_1_get_Item_m42586_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42586_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42586_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7507_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoBehaviour_t78_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7507_IList_1_get_Item_m42586_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42586_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
static ParameterInfo IList_1_t7507_IList_1_set_Item_m42587_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t78_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42587_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42587_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7507_IList_1_set_Item_m42587_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42587_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7507_MethodInfos[] =
{
	&IList_1_IndexOf_m42588_MethodInfo,
	&IList_1_Insert_m42589_MethodInfo,
	&IList_1_RemoveAt_m42590_MethodInfo,
	&IList_1_get_Item_m42586_MethodInfo,
	&IList_1_set_Item_m42587_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7507_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7506_il2cpp_TypeInfo,
	&IEnumerable_1_t7508_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7507_0_0_0;
extern Il2CppType IList_1_t7507_1_0_0;
struct IList_1_t7507;
extern Il2CppGenericClass IList_1_t7507_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7507_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7507_MethodInfos/* methods */
	, IList_1_t7507_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7507_il2cpp_TypeInfo/* element_class */
	, IList_1_t7507_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7507_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7507_0_0_0/* byval_arg */
	, &IList_1_t7507_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7507_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7509_il2cpp_TypeInfo;

// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBeh.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m42591_MethodInfo;
static PropertyInfo ICollection_1_t7509____Count_PropertyInfo = 
{
	&ICollection_1_t7509_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42591_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42592_MethodInfo;
static PropertyInfo ICollection_1_t7509____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7509_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42592_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7509_PropertyInfos[] =
{
	&ICollection_1_t7509____Count_PropertyInfo,
	&ICollection_1_t7509____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42591_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42591_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7509_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42591_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42592_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42592_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7509_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42592_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoAbstractBehaviour_t61_0_0_0;
extern Il2CppType TextRecoAbstractBehaviour_t61_0_0_0;
static ParameterInfo ICollection_1_t7509_ICollection_1_Add_m42593_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoAbstractBehaviour_t61_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42593_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42593_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7509_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7509_ICollection_1_Add_m42593_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42593_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42594_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42594_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7509_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42594_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoAbstractBehaviour_t61_0_0_0;
static ParameterInfo ICollection_1_t7509_ICollection_1_Contains_m42595_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoAbstractBehaviour_t61_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42595_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42595_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7509_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7509_ICollection_1_Contains_m42595_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42595_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoAbstractBehaviourU5BU5D_t5517_0_0_0;
extern Il2CppType TextRecoAbstractBehaviourU5BU5D_t5517_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7509_ICollection_1_CopyTo_m42596_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoAbstractBehaviourU5BU5D_t5517_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42596_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42596_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7509_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7509_ICollection_1_CopyTo_m42596_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42596_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoAbstractBehaviour_t61_0_0_0;
static ParameterInfo ICollection_1_t7509_ICollection_1_Remove_m42597_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoAbstractBehaviour_t61_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42597_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.TextRecoAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42597_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7509_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7509_ICollection_1_Remove_m42597_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42597_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7509_MethodInfos[] =
{
	&ICollection_1_get_Count_m42591_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42592_MethodInfo,
	&ICollection_1_Add_m42593_MethodInfo,
	&ICollection_1_Clear_m42594_MethodInfo,
	&ICollection_1_Contains_m42595_MethodInfo,
	&ICollection_1_CopyTo_m42596_MethodInfo,
	&ICollection_1_Remove_m42597_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7511_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7509_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7511_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7509_0_0_0;
extern Il2CppType ICollection_1_t7509_1_0_0;
struct ICollection_1_t7509;
extern Il2CppGenericClass ICollection_1_t7509_GenericClass;
TypeInfo ICollection_1_t7509_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7509_MethodInfos/* methods */
	, ICollection_1_t7509_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7509_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7509_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7509_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7509_0_0_0/* byval_arg */
	, &ICollection_1_t7509_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7509_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TextRecoAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.TextRecoAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5895_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42598_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.TextRecoAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42598_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7511_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5895_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42598_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7511_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42598_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7511_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7511_0_0_0;
extern Il2CppType IEnumerable_1_t7511_1_0_0;
struct IEnumerable_1_t7511;
extern Il2CppGenericClass IEnumerable_1_t7511_GenericClass;
TypeInfo IEnumerable_1_t7511_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7511_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7511_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7511_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7511_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7511_0_0_0/* byval_arg */
	, &IEnumerable_1_t7511_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7511_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5895_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.TextRecoAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42599_MethodInfo;
static PropertyInfo IEnumerator_1_t5895____Current_PropertyInfo = 
{
	&IEnumerator_1_t5895_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42599_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5895_PropertyInfos[] =
{
	&IEnumerator_1_t5895____Current_PropertyInfo,
	NULL
};
extern Il2CppType TextRecoAbstractBehaviour_t61_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42599_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42599_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5895_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoAbstractBehaviour_t61_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42599_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5895_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42599_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5895_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5895_0_0_0;
extern Il2CppType IEnumerator_1_t5895_1_0_0;
struct IEnumerator_1_t5895;
extern Il2CppGenericClass IEnumerator_1_t5895_GenericClass;
TypeInfo IEnumerator_1_t5895_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5895_MethodInfos/* methods */
	, IEnumerator_1_t5895_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5895_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5895_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5895_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5895_0_0_0/* byval_arg */
	, &IEnumerator_1_t5895_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5895_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_107.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2939_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_107MethodDeclarations.h"

extern TypeInfo TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15155_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTextRecoAbstractBehaviour_t61_m32690_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.TextRecoAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.TextRecoAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisTextRecoAbstractBehaviour_t61_m32690(__this, p0, method) (TextRecoAbstractBehaviour_t61 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2939____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2939, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2939____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2939, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2939_FieldInfos[] =
{
	&InternalEnumerator_1_t2939____array_0_FieldInfo,
	&InternalEnumerator_1_t2939____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15152_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2939____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2939_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15152_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2939____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2939_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15155_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2939_PropertyInfos[] =
{
	&InternalEnumerator_1_t2939____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2939____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2939_InternalEnumerator_1__ctor_m15151_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15151_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15151_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2939_InternalEnumerator_1__ctor_m15151_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15151_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15152_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15152_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15152_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15153_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15153_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15153_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15154_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15154_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15154_GenericMethod/* genericMethod */

};
extern Il2CppType TextRecoAbstractBehaviour_t61_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15155_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.TextRecoAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15155_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoAbstractBehaviour_t61_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15155_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2939_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15151_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15152_MethodInfo,
	&InternalEnumerator_1_Dispose_m15153_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15154_MethodInfo,
	&InternalEnumerator_1_get_Current_m15155_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15154_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15153_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2939_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15152_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15154_MethodInfo,
	&InternalEnumerator_1_Dispose_m15153_MethodInfo,
	&InternalEnumerator_1_get_Current_m15155_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2939_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5895_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2939_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5895_il2cpp_TypeInfo, 7},
};
extern TypeInfo TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2939_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15155_MethodInfo/* Method Usage */,
	&TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTextRecoAbstractBehaviour_t61_m32690_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2939_0_0_0;
extern Il2CppType InternalEnumerator_1_t2939_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2939_GenericClass;
TypeInfo InternalEnumerator_1_t2939_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2939_MethodInfos/* methods */
	, InternalEnumerator_1_t2939_PropertyInfos/* properties */
	, InternalEnumerator_1_t2939_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2939_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2939_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2939_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2939_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2939_1_0_0/* this_arg */
	, InternalEnumerator_1_t2939_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2939_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2939_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2939)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7510_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m42600_MethodInfo;
extern MethodInfo IList_1_set_Item_m42601_MethodInfo;
static PropertyInfo IList_1_t7510____Item_PropertyInfo = 
{
	&IList_1_t7510_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42600_MethodInfo/* get */
	, &IList_1_set_Item_m42601_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7510_PropertyInfos[] =
{
	&IList_1_t7510____Item_PropertyInfo,
	NULL
};
extern Il2CppType TextRecoAbstractBehaviour_t61_0_0_0;
static ParameterInfo IList_1_t7510_IList_1_IndexOf_m42602_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TextRecoAbstractBehaviour_t61_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42602_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42602_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7510_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7510_IList_1_IndexOf_m42602_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42602_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TextRecoAbstractBehaviour_t61_0_0_0;
static ParameterInfo IList_1_t7510_IList_1_Insert_m42603_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TextRecoAbstractBehaviour_t61_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42603_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42603_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7510_IList_1_Insert_m42603_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42603_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7510_IList_1_RemoveAt_m42604_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42604_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42604_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7510_IList_1_RemoveAt_m42604_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42604_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7510_IList_1_get_Item_m42600_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType TextRecoAbstractBehaviour_t61_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42600_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42600_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7510_il2cpp_TypeInfo/* declaring_type */
	, &TextRecoAbstractBehaviour_t61_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7510_IList_1_get_Item_m42600_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42600_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TextRecoAbstractBehaviour_t61_0_0_0;
static ParameterInfo IList_1_t7510_IList_1_set_Item_m42601_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TextRecoAbstractBehaviour_t61_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42601_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.TextRecoAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42601_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7510_IList_1_set_Item_m42601_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42601_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7510_MethodInfos[] =
{
	&IList_1_IndexOf_m42602_MethodInfo,
	&IList_1_Insert_m42603_MethodInfo,
	&IList_1_RemoveAt_m42604_MethodInfo,
	&IList_1_get_Item_m42600_MethodInfo,
	&IList_1_set_Item_m42601_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7510_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7509_il2cpp_TypeInfo,
	&IEnumerable_1_t7511_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7510_0_0_0;
extern Il2CppType IList_1_t7510_1_0_0;
struct IList_1_t7510;
extern Il2CppGenericClass IList_1_t7510_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7510_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7510_MethodInfos/* methods */
	, IList_1_t7510_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7510_il2cpp_TypeInfo/* element_class */
	, IList_1_t7510_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7510_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7510_0_0_0/* byval_arg */
	, &IList_1_t7510_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7510_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7512_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>
extern MethodInfo ICollection_1_get_Count_m42605_MethodInfo;
static PropertyInfo ICollection_1_t7512____Count_PropertyInfo = 
{
	&ICollection_1_t7512_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m42605_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m42606_MethodInfo;
static PropertyInfo ICollection_1_t7512____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7512_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m42606_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7512_PropertyInfos[] =
{
	&ICollection_1_t7512____Count_PropertyInfo,
	&ICollection_1_t7512____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m42605_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m42605_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7512_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m42605_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m42606_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m42606_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7512_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m42606_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTextRecoBehaviour_t167_0_0_0;
extern Il2CppType IEditorTextRecoBehaviour_t167_0_0_0;
static ParameterInfo ICollection_1_t7512_ICollection_1_Add_m42607_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTextRecoBehaviour_t167_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m42607_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m42607_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7512_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7512_ICollection_1_Add_m42607_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m42607_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m42608_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m42608_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7512_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m42608_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTextRecoBehaviour_t167_0_0_0;
static ParameterInfo ICollection_1_t7512_ICollection_1_Contains_m42609_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTextRecoBehaviour_t167_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m42609_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m42609_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7512_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7512_ICollection_1_Contains_m42609_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m42609_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTextRecoBehaviourU5BU5D_t5518_0_0_0;
extern Il2CppType IEditorTextRecoBehaviourU5BU5D_t5518_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7512_ICollection_1_CopyTo_m42610_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTextRecoBehaviourU5BU5D_t5518_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m42610_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m42610_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7512_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7512_ICollection_1_CopyTo_m42610_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m42610_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTextRecoBehaviour_t167_0_0_0;
static ParameterInfo ICollection_1_t7512_ICollection_1_Remove_m42611_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTextRecoBehaviour_t167_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m42611_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorTextRecoBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m42611_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7512_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7512_ICollection_1_Remove_m42611_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m42611_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7512_MethodInfos[] =
{
	&ICollection_1_get_Count_m42605_MethodInfo,
	&ICollection_1_get_IsReadOnly_m42606_MethodInfo,
	&ICollection_1_Add_m42607_MethodInfo,
	&ICollection_1_Clear_m42608_MethodInfo,
	&ICollection_1_Contains_m42609_MethodInfo,
	&ICollection_1_CopyTo_m42610_MethodInfo,
	&ICollection_1_Remove_m42611_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7514_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7512_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7514_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7512_0_0_0;
extern Il2CppType ICollection_1_t7512_1_0_0;
struct ICollection_1_t7512;
extern Il2CppGenericClass ICollection_1_t7512_GenericClass;
TypeInfo ICollection_1_t7512_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7512_MethodInfos/* methods */
	, ICollection_1_t7512_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7512_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7512_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7512_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7512_0_0_0/* byval_arg */
	, &ICollection_1_t7512_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7512_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorTextRecoBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorTextRecoBehaviour>
extern Il2CppType IEnumerator_1_t5897_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m42612_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorTextRecoBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m42612_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7514_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5897_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m42612_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7514_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m42612_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7514_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7514_0_0_0;
extern Il2CppType IEnumerable_1_t7514_1_0_0;
struct IEnumerable_1_t7514;
extern Il2CppGenericClass IEnumerable_1_t7514_GenericClass;
TypeInfo IEnumerable_1_t7514_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7514_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7514_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7514_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7514_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7514_0_0_0/* byval_arg */
	, &IEnumerable_1_t7514_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7514_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5897_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorTextRecoBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m42613_MethodInfo;
static PropertyInfo IEnumerator_1_t5897____Current_PropertyInfo = 
{
	&IEnumerator_1_t5897_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m42613_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5897_PropertyInfos[] =
{
	&IEnumerator_1_t5897____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorTextRecoBehaviour_t167_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m42613_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m42613_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5897_il2cpp_TypeInfo/* declaring_type */
	, &IEditorTextRecoBehaviour_t167_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m42613_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5897_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m42613_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5897_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5897_0_0_0;
extern Il2CppType IEnumerator_1_t5897_1_0_0;
struct IEnumerator_1_t5897;
extern Il2CppGenericClass IEnumerator_1_t5897_GenericClass;
TypeInfo IEnumerator_1_t5897_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5897_MethodInfos/* methods */
	, IEnumerator_1_t5897_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5897_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5897_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5897_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5897_0_0_0/* byval_arg */
	, &IEnumerator_1_t5897_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5897_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_108.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2940_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_108MethodDeclarations.h"

extern TypeInfo IEditorTextRecoBehaviour_t167_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m15160_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorTextRecoBehaviour_t167_m32701_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorTextRecoBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorTextRecoBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorTextRecoBehaviour_t167_m32701(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2940____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2940_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2940, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2940____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2940_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2940, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2940_FieldInfos[] =
{
	&InternalEnumerator_1_t2940____array_0_FieldInfo,
	&InternalEnumerator_1_t2940____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15157_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2940____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2940_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15157_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2940____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2940_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m15160_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2940_PropertyInfos[] =
{
	&InternalEnumerator_1_t2940____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2940____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2940_InternalEnumerator_1__ctor_m15156_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m15156_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m15156_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2940_InternalEnumerator_1__ctor_m15156_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m15156_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15157_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15157_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2940_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15157_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m15158_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m15158_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2940_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m15158_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m15159_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m15159_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2940_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m15159_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorTextRecoBehaviour_t167_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m15160_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorTextRecoBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m15160_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2940_il2cpp_TypeInfo/* declaring_type */
	, &IEditorTextRecoBehaviour_t167_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m15160_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2940_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m15156_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15157_MethodInfo,
	&InternalEnumerator_1_Dispose_m15158_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15159_MethodInfo,
	&InternalEnumerator_1_get_Current_m15160_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m15159_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m15158_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2940_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15157_MethodInfo,
	&InternalEnumerator_1_MoveNext_m15159_MethodInfo,
	&InternalEnumerator_1_Dispose_m15158_MethodInfo,
	&InternalEnumerator_1_get_Current_m15160_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2940_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5897_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2940_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5897_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorTextRecoBehaviour_t167_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2940_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m15160_MethodInfo/* Method Usage */,
	&IEditorTextRecoBehaviour_t167_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorTextRecoBehaviour_t167_m32701_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2940_0_0_0;
extern Il2CppType InternalEnumerator_1_t2940_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2940_GenericClass;
TypeInfo InternalEnumerator_1_t2940_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2940_MethodInfos/* methods */
	, InternalEnumerator_1_t2940_PropertyInfos/* properties */
	, InternalEnumerator_1_t2940_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2940_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2940_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2940_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2940_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2940_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2940_1_0_0/* this_arg */
	, InternalEnumerator_1_t2940_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2940_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2940_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2940)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7513_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>
extern MethodInfo IList_1_get_Item_m42614_MethodInfo;
extern MethodInfo IList_1_set_Item_m42615_MethodInfo;
static PropertyInfo IList_1_t7513____Item_PropertyInfo = 
{
	&IList_1_t7513_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m42614_MethodInfo/* get */
	, &IList_1_set_Item_m42615_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7513_PropertyInfos[] =
{
	&IList_1_t7513____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorTextRecoBehaviour_t167_0_0_0;
static ParameterInfo IList_1_t7513_IList_1_IndexOf_m42616_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorTextRecoBehaviour_t167_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m42616_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m42616_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7513_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7513_IList_1_IndexOf_m42616_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m42616_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorTextRecoBehaviour_t167_0_0_0;
static ParameterInfo IList_1_t7513_IList_1_Insert_m42617_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorTextRecoBehaviour_t167_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m42617_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m42617_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7513_IList_1_Insert_m42617_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m42617_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7513_IList_1_RemoveAt_m42618_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m42618_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m42618_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7513_IList_1_RemoveAt_m42618_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m42618_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7513_IList_1_get_Item_m42614_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEditorTextRecoBehaviour_t167_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m42614_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m42614_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7513_il2cpp_TypeInfo/* declaring_type */
	, &IEditorTextRecoBehaviour_t167_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7513_IList_1_get_Item_m42614_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m42614_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorTextRecoBehaviour_t167_0_0_0;
static ParameterInfo IList_1_t7513_IList_1_set_Item_m42615_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorTextRecoBehaviour_t167_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m42615_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorTextRecoBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m42615_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7513_IList_1_set_Item_m42615_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m42615_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7513_MethodInfos[] =
{
	&IList_1_IndexOf_m42616_MethodInfo,
	&IList_1_Insert_m42617_MethodInfo,
	&IList_1_RemoveAt_m42618_MethodInfo,
	&IList_1_get_Item_m42614_MethodInfo,
	&IList_1_set_Item_m42615_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7513_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7512_il2cpp_TypeInfo,
	&IEnumerable_1_t7514_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7513_0_0_0;
extern Il2CppType IList_1_t7513_1_0_0;
struct IList_1_t7513;
extern Il2CppGenericClass IList_1_t7513_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7513_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7513_MethodInfos/* methods */
	, IList_1_t7513_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7513_il2cpp_TypeInfo/* element_class */
	, IList_1_t7513_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7513_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7513_0_0_0/* byval_arg */
	, &IList_1_t7513_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7513_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_37.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2941_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_37MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_33.h"
extern TypeInfo InvokableCall_1_t2942_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_33MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m15163_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m15165_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2941____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2941_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2941, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2941_FieldInfos[] =
{
	&CachedInvokableCall_1_t2941____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType TextRecoBehaviour_t78_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2941_CachedInvokableCall_1__ctor_m15161_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &TextRecoBehaviour_t78_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m15161_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m15161_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2941_CachedInvokableCall_1__ctor_m15161_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m15161_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2941_CachedInvokableCall_1_Invoke_m15162_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m15162_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m15162_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2941_CachedInvokableCall_1_Invoke_m15162_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m15162_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2941_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m15161_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15162_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m15162_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m15166_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2941_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m15162_MethodInfo,
	&InvokableCall_1_Find_m15166_MethodInfo,
};
extern Il2CppType UnityAction_1_t2943_0_0_0;
extern TypeInfo UnityAction_1_t2943_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisTextRecoBehaviour_t78_m32711_MethodInfo;
extern TypeInfo TextRecoBehaviour_t78_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m15168_MethodInfo;
extern TypeInfo TextRecoBehaviour_t78_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2941_RGCTXData[8] = 
{
	&UnityAction_1_t2943_0_0_0/* Type Usage */,
	&UnityAction_1_t2943_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisTextRecoBehaviour_t78_m32711_MethodInfo/* Method Usage */,
	&TextRecoBehaviour_t78_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m15168_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m15163_MethodInfo/* Method Usage */,
	&TextRecoBehaviour_t78_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m15165_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2941_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2941_1_0_0;
struct CachedInvokableCall_1_t2941;
extern Il2CppGenericClass CachedInvokableCall_1_t2941_GenericClass;
TypeInfo CachedInvokableCall_1_t2941_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2941_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2941_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2942_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2941_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2941_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2941_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2941_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2941_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2941_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2941_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2941)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
