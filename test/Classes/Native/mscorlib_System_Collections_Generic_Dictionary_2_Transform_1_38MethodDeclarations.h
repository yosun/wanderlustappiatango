﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>>
struct Transform_1_t3839;
// System.Object
struct Object_t;
// Vuforia.VirtualButton
struct VirtualButton_t595;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_11.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m21750 (Transform_1_t3839 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>>::Invoke(TKey,TValue)
 KeyValuePair_2_t3832  Transform_1_Invoke_m21751 (Transform_1_t3839 * __this, int32_t ___key, VirtualButton_t595 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m21752 (Transform_1_t3839 * __this, int32_t ___key, VirtualButton_t595 * ___value, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>>::EndInvoke(System.IAsyncResult)
 KeyValuePair_2_t3832  Transform_1_EndInvoke_m21753 (Transform_1_t3839 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
