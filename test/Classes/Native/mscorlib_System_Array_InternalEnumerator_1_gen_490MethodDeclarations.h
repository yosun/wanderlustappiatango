﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.IO.FileAccess>
struct InternalEnumerator_1_t4851;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.IO.FileAccess
#include "mscorlib_System_IO_FileAccess.h"

// System.Void System.Array/InternalEnumerator`1<System.IO.FileAccess>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29790 (InternalEnumerator_1_t4851 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.IO.FileAccess>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29791 (InternalEnumerator_1_t4851 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileAccess>::Dispose()
 void InternalEnumerator_1_Dispose_m29792 (InternalEnumerator_1_t4851 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileAccess>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29793 (InternalEnumerator_1_t4851 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.IO.FileAccess>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29794 (InternalEnumerator_1_t4851 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
