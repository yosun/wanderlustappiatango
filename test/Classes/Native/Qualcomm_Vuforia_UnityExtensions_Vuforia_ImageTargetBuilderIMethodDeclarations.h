﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ImageTargetBuilderImpl
struct ImageTargetBuilderImpl_t612;
// System.String
struct String_t;
// Vuforia.TrackableSource
struct TrackableSource_t586;
// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"

// System.Boolean Vuforia.ImageTargetBuilderImpl::Build(System.String,System.Single)
 bool ImageTargetBuilderImpl_Build_m2905 (ImageTargetBuilderImpl_t612 * __this, String_t* ___targetName, float ___sceenSizeWidth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetBuilderImpl::StartScan()
 void ImageTargetBuilderImpl_StartScan_m2906 (ImageTargetBuilderImpl_t612 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetBuilderImpl::StopScan()
 void ImageTargetBuilderImpl_StopScan_m2907 (ImageTargetBuilderImpl_t612 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetBuilder/FrameQuality Vuforia.ImageTargetBuilderImpl::GetFrameQuality()
 int32_t ImageTargetBuilderImpl_GetFrameQuality_m2908 (ImageTargetBuilderImpl_t612 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TrackableSource Vuforia.ImageTargetBuilderImpl::GetTrackableSource()
 TrackableSource_t586 * ImageTargetBuilderImpl_GetTrackableSource_m2909 (ImageTargetBuilderImpl_t612 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetBuilderImpl::.ctor()
 void ImageTargetBuilderImpl__ctor_m2910 (ImageTargetBuilderImpl_t612 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
