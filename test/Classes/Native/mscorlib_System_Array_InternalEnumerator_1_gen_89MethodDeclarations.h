﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>
struct InternalEnumerator_1_t2900;
// System.Object
struct Object_t;
// Vuforia.IEditorObjectTargetBehaviour
struct IEditorObjectTargetBehaviour_t159;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m14991(__this, ___array, method) (void)InternalEnumerator_1__ctor_m13978_gshared((InternalEnumerator_1_t2698 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14992(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m14993(__this, method) (void)InternalEnumerator_1_Dispose_m13982_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m14994(__this, method) (bool)InternalEnumerator_1_MoveNext_m13984_gshared((InternalEnumerator_1_t2698 *)__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.IEditorObjectTargetBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m14995(__this, method) (Object_t *)InternalEnumerator_1_get_Current_m13986_gshared((InternalEnumerator_1_t2698 *)__this, method)
