﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.MarkerBehaviour>
struct UnityAction_1_t2886;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.MarkerBehaviour>
struct InvokableCall_1_t2885  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.MarkerBehaviour>::Delegate
	UnityAction_1_t2886 * ___Delegate_0;
};
