﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.Slider>
struct UnityAction_1_t3501;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Slider>
struct InvokableCall_1_t3500  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Slider>::Delegate
	UnityAction_1_t3501 * ___Delegate_0;
};
