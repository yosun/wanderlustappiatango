﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyCompanyAttribute
struct AssemblyCompanyAttribute_t532;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyCompanyAttribute::.ctor(System.String)
 void AssemblyCompanyAttribute__ctor_m2583 (AssemblyCompanyAttribute_t532 * __this, String_t* ___company, MethodInfo* method) IL2CPP_METHOD_ATTR;
