﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct TriggerEvent_t191;

// System.Void UnityEngine.EventSystems.EventTrigger/TriggerEvent::.ctor()
 void TriggerEvent__ctor_m706 (TriggerEvent_t191 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
