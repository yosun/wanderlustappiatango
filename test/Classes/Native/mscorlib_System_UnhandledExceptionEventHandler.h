﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t2254;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.UnhandledExceptionEventHandler
struct UnhandledExceptionEventHandler_t2186  : public MulticastDelegate_t325
{
};
