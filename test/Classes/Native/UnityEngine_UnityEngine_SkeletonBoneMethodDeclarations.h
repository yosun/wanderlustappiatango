﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SkeletonBone
struct SkeletonBone_t1022;
struct SkeletonBone_t1022_marshaled;

void SkeletonBone_t1022_marshal(const SkeletonBone_t1022& unmarshaled, SkeletonBone_t1022_marshaled& marshaled);
void SkeletonBone_t1022_marshal_back(const SkeletonBone_t1022_marshaled& marshaled, SkeletonBone_t1022& unmarshaled);
void SkeletonBone_t1022_marshal_cleanup(SkeletonBone_t1022_marshaled& marshaled);
