﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoTypeInfo
struct MonoTypeInfo_t2233;

// System.Void System.MonoTypeInfo::.ctor()
 void MonoTypeInfo__ctor_m13000 (MonoTypeInfo_t2233 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
