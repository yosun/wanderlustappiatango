﻿#pragma once
#include <stdint.h>
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t445;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.SpriteRenderer>
struct CastHelper_1_t3207 
{
	// T UnityEngine.CastHelper`1<UnityEngine.SpriteRenderer>::t
	SpriteRenderer_t445 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.SpriteRenderer>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
