﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.WordResult>
struct List_1_t694;
// System.Object
struct Object_t;
// Vuforia.WordResult
struct WordResult_t702;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult>
struct IEnumerable_1_t689;
// System.Collections.Generic.IEnumerator`1<Vuforia.WordResult>
struct IEnumerator_1_t900;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.ICollection`1<Vuforia.WordResult>
struct ICollection_1_t3976;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordResult>
struct ReadOnlyCollection_1_t3977;
// Vuforia.WordResult[]
struct WordResultU5BU5D_t3960;
// System.Predicate`1<Vuforia.WordResult>
struct Predicate_1_t3978;
// System.Comparison`1<Vuforia.WordResult>
struct Comparison_1_t3979;
// System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_10.h"

// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
#define List_1__ctor_m5041(__this, method) (void)List_1__ctor_m14543_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23087(__this, ___collection, method) (void)List_1__ctor_m14545_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.ctor(System.Int32)
#define List_1__ctor_m23088(__this, ___capacity, method) (void)List_1__ctor_m14547_gshared((List_1_t150 *)__this, (int32_t)___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::.cctor()
#define List_1__cctor_m23089(__this/* static, unused */, method) (void)List_1__cctor_m14549_gshared((Object_t *)__this/* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23090(__this, method) (Object_t*)List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14551_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23091(__this, ___array, ___arrayIndex, method) (void)List_1_System_Collections_ICollection_CopyTo_m14553_gshared((List_1_t150 *)__this, (Array_t *)___array, (int32_t)___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23092(__this, method) (Object_t *)List_1_System_Collections_IEnumerable_GetEnumerator_m14555_gshared((List_1_t150 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23093(__this, ___item, method) (int32_t)List_1_System_Collections_IList_Add_m14557_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23094(__this, ___item, method) (bool)List_1_System_Collections_IList_Contains_m14559_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23095(__this, ___item, method) (int32_t)List_1_System_Collections_IList_IndexOf_m14561_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23096(__this, ___index, ___item, method) (void)List_1_System_Collections_IList_Insert_m14563_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23097(__this, ___item, method) (void)List_1_System_Collections_IList_Remove_m14565_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23098(__this, method) (bool)List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14567_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23099(__this, method) (bool)List_1_System_Collections_ICollection_get_IsSynchronized_m14569_gshared((List_1_t150 *)__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23100(__this, method) (Object_t *)List_1_System_Collections_ICollection_get_SyncRoot_m14571_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23101(__this, method) (bool)List_1_System_Collections_IList_get_IsFixedSize_m14573_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23102(__this, method) (bool)List_1_System_Collections_IList_get_IsReadOnly_m14575_gshared((List_1_t150 *)__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23103(__this, ___index, method) (Object_t *)List_1_System_Collections_IList_get_Item_m14577_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23104(__this, ___index, ___value, method) (void)List_1_System_Collections_IList_set_Item_m14579_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Add(T)
#define List_1_Add_m5003(__this, ___item, method) (void)List_1_Add_m14581_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23105(__this, ___newCount, method) (void)List_1_GrowIfNeeded_m14583_gshared((List_1_t150 *)__this, (int32_t)___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23106(__this, ___collection, method) (void)List_1_AddCollection_m14585_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23107(__this, ___enumerable, method) (void)List_1_AddEnumerable_m14587_gshared((List_1_t150 *)__this, (Object_t*)___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m23108(__this, ___collection, method) (void)List_1_AddRange_m14588_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.WordResult>::AsReadOnly()
#define List_1_AsReadOnly_m23109(__this, method) (ReadOnlyCollection_1_t3977 *)List_1_AsReadOnly_m14590_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Clear()
#define List_1_Clear_m4997(__this, method) (void)List_1_Clear_m14592_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::Contains(T)
#define List_1_Contains_m23110(__this, ___item, method) (bool)List_1_Contains_m14594_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23111(__this, ___array, ___arrayIndex, method) (void)List_1_CopyTo_m14596_gshared((List_1_t150 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.WordResult>::Find(System.Predicate`1<T>)
#define List_1_Find_m23112(__this, ___match, method) (WordResult_t702 *)List_1_Find_m14598_gshared((List_1_t150 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23113(__this/* static, unused */, ___match, method) (void)List_1_CheckMatch_m14600_gshared((Object_t *)__this/* static, unused */, (Predicate_1_t2845 *)___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23114(__this, ___startIndex, ___count, ___match, method) (int32_t)List_1_GetIndex_m14602_gshared((List_1_t150 *)__this, (int32_t)___startIndex, (int32_t)___count, (Predicate_1_t2845 *)___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.WordResult>::GetEnumerator()
 Enumerator_t832  List_1_GetEnumerator_m5020 (List_1_t694 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::IndexOf(T)
#define List_1_IndexOf_m23115(__this, ___item, method) (int32_t)List_1_IndexOf_m14604_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23116(__this, ___start, ___delta, method) (void)List_1_Shift_m14606_gshared((List_1_t150 *)__this, (int32_t)___start, (int32_t)___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23117(__this, ___index, method) (void)List_1_CheckIndex_m14608_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Insert(System.Int32,T)
#define List_1_Insert_m23118(__this, ___index, ___item, method) (void)List_1_Insert_m14610_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23119(__this, ___collection, method) (void)List_1_CheckCollection_m14612_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordResult>::Remove(T)
#define List_1_Remove_m23120(__this, ___item, method) (bool)List_1_Remove_m14614_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23121(__this, ___match, method) (int32_t)List_1_RemoveAll_m14616_gshared((List_1_t150 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m23122(__this, ___index, method) (void)List_1_RemoveAt_m14618_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Reverse()
#define List_1_Reverse_m23123(__this, method) (void)List_1_Reverse_m14620_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Sort()
#define List_1_Sort_m23124(__this, method) (void)List_1_Sort_m14622_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23125(__this, ___comparison, method) (void)List_1_Sort_m14624_gshared((List_1_t150 *)__this, (Comparison_1_t2846 *)___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.WordResult>::ToArray()
#define List_1_ToArray_m23126(__this, method) (WordResultU5BU5D_t3960*)List_1_ToArray_m14626_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::TrimExcess()
#define List_1_TrimExcess_m23127(__this, method) (void)List_1_TrimExcess_m14628_gshared((List_1_t150 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::get_Capacity()
#define List_1_get_Capacity_m23128(__this, method) (int32_t)List_1_get_Capacity_m14630_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23129(__this, ___value, method) (void)List_1_set_Capacity_m14632_gshared((List_1_t150 *)__this, (int32_t)___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordResult>::get_Count()
#define List_1_get_Count_m23130(__this, method) (int32_t)List_1_get_Count_m14634_gshared((List_1_t150 *)__this, method)
// T System.Collections.Generic.List`1<Vuforia.WordResult>::get_Item(System.Int32)
#define List_1_get_Item_m23131(__this, ___index, method) (WordResult_t702 *)List_1_get_Item_m14636_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordResult>::set_Item(System.Int32,T)
#define List_1_set_Item_m23132(__this, ___index, ___value, method) (void)List_1_set_Item_m14638_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___value, method)
