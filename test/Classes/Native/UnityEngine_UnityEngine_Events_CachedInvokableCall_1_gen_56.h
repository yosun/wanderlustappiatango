﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.Physics2DRaycaster>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_53.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.Physics2DRaycaster>
struct CachedInvokableCall_1_t3203  : public InvokableCall_1_t3204
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.Physics2DRaycaster>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
