﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonBehaviour>
struct UnityAction_1_t2975;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>
struct InvokableCall_1_t2974  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonBehaviour>::Delegate
	UnityAction_1_t2975 * ___Delegate_0;
};
