﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t396;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct UnityAction_1_t408  : public MulticastDelegate_t325
{
};
