﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.X509Certificates.X509ChainElementCollection
struct X509ChainElementCollection_t1371;
// System.Security.Cryptography.X509Certificates.X509ChainElement
struct X509ChainElement_t1375;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator
struct X509ChainElementEnumerator_t1380;
// System.Security.Cryptography.X509Certificates.X509Certificate2
struct X509Certificate2_t1364;

// System.Void System.Security.Cryptography.X509Certificates.X509ChainElementCollection::.ctor()
 void X509ChainElementCollection__ctor_m7039 (X509ChainElementCollection_t1371 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509ChainElementCollection::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void X509ChainElementCollection_System_Collections_ICollection_CopyTo_m7040 (X509ChainElementCollection_t1371 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Security.Cryptography.X509Certificates.X509ChainElementCollection::System.Collections.IEnumerable.GetEnumerator()
 Object_t * X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m7041 (X509ChainElementCollection_t1371 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.X509Certificates.X509ChainElementCollection::get_Count()
 int32_t X509ChainElementCollection_get_Count_m7042 (X509ChainElementCollection_t1371 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.X509Certificates.X509ChainElementCollection::get_IsSynchronized()
 bool X509ChainElementCollection_get_IsSynchronized_m7043 (X509ChainElementCollection_t1371 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509ChainElement System.Security.Cryptography.X509Certificates.X509ChainElementCollection::get_Item(System.Int32)
 X509ChainElement_t1375 * X509ChainElementCollection_get_Item_m7044 (X509ChainElementCollection_t1371 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Security.Cryptography.X509Certificates.X509ChainElementCollection::get_SyncRoot()
 Object_t * X509ChainElementCollection_get_SyncRoot_m7045 (X509ChainElementCollection_t1371 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.X509Certificates.X509ChainElementEnumerator System.Security.Cryptography.X509Certificates.X509ChainElementCollection::GetEnumerator()
 X509ChainElementEnumerator_t1380 * X509ChainElementCollection_GetEnumerator_m7046 (X509ChainElementCollection_t1371 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509ChainElementCollection::Add(System.Security.Cryptography.X509Certificates.X509Certificate2)
 void X509ChainElementCollection_Add_m7047 (X509ChainElementCollection_t1371 * __this, X509Certificate2_t1364 * ___certificate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.X509Certificates.X509ChainElementCollection::Clear()
 void X509ChainElementCollection_Clear_m7048 (X509ChainElementCollection_t1371 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.X509Certificates.X509ChainElementCollection::Contains(System.Security.Cryptography.X509Certificates.X509Certificate2)
 bool X509ChainElementCollection_Contains_m7049 (X509ChainElementCollection_t1371 * __this, X509Certificate2_t1364 * ___certificate, MethodInfo* method) IL2CPP_METHOD_ATTR;
