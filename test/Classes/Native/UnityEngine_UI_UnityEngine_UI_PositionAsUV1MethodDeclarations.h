﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.PositionAsUV1
struct PositionAsUV1_t414;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t295;

// System.Void UnityEngine.UI.PositionAsUV1::.ctor()
 void PositionAsUV1__ctor_m1845 (PositionAsUV1_t414 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.PositionAsUV1::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
 void PositionAsUV1_ModifyVertices_m1846 (PositionAsUV1_t414 * __this, List_1_t295 * ___verts, MethodInfo* method) IL2CPP_METHOD_ATTR;
