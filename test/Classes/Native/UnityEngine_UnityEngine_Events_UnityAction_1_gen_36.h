﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// Vuforia.ReconstructionBehaviour
struct ReconstructionBehaviour_t38;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Vuforia.ReconstructionBehaviour>
struct UnityAction_1_t2920  : public MulticastDelegate_t325
{
};
