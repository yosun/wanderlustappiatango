﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct Stack_1_t3623;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct IEnumerator_1_t3625;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t396;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_3.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m20071(__this, method) (void)Stack_1__ctor_m15717_gshared((Stack_1_t3035 *)__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m20072(__this, method) (bool)Stack_1_System_Collections_ICollection_get_IsSynchronized_m15718_gshared((Stack_1_t3035 *)__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m20073(__this, method) (Object_t *)Stack_1_System_Collections_ICollection_get_SyncRoot_m15719_gshared((Stack_1_t3035 *)__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m20074(__this, ___dest, ___idx, method) (void)Stack_1_System_Collections_ICollection_CopyTo_m15720_gshared((Stack_1_t3035 *)__this, (Array_t *)___dest, (int32_t)___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20075(__this, method) (Object_t*)Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15721_gshared((Stack_1_t3035 *)__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m20076(__this, method) (Object_t *)Stack_1_System_Collections_IEnumerable_GetEnumerator_m15722_gshared((Stack_1_t3035 *)__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Peek()
#define Stack_1_Peek_m20077(__this, method) (List_1_t396 *)Stack_1_Peek_m15723_gshared((Stack_1_t3035 *)__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Pop()
#define Stack_1_Pop_m20078(__this, method) (List_1_t396 *)Stack_1_Pop_m15724_gshared((Stack_1_t3035 *)__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Push(T)
#define Stack_1_Push_m20079(__this, ___t, method) (void)Stack_1_Push_m15725_gshared((Stack_1_t3035 *)__this, (Object_t *)___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Count()
#define Stack_1_get_Count_m20080(__this, method) (int32_t)Stack_1_get_Count_m15726_gshared((Stack_1_t3035 *)__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::GetEnumerator()
 Enumerator_t3626  Stack_1_GetEnumerator_m20081 (Stack_1_t3623 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
