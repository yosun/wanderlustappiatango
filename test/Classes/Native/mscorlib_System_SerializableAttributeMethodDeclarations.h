﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.SerializableAttribute
struct SerializableAttribute_t1688;

// System.Void System.SerializableAttribute::.ctor()
 void SerializableAttribute__ctor_m9002 (SerializableAttribute_t1688 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
