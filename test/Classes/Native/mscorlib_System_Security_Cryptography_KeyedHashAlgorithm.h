﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t609;
// System.Security.Cryptography.HashAlgorithm
#include "mscorlib_System_Security_Cryptography_HashAlgorithm.h"
// System.Security.Cryptography.KeyedHashAlgorithm
struct KeyedHashAlgorithm_t1583  : public HashAlgorithm_t1559
{
	// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::KeyValue
	ByteU5BU5D_t609* ___KeyValue_4;
};
