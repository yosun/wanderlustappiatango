﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutElement>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_80.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutElement>
struct CachedInvokableCall_1_t3601  : public InvokableCall_1_t3602
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutElement>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
