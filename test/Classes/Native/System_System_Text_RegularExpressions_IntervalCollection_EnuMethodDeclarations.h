﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.IntervalCollection/Enumerator
struct Enumerator_t1436;
// System.Object
struct Object_t;
// System.Collections.IList
struct IList_t1435;

// System.Void System.Text.RegularExpressions.IntervalCollection/Enumerator::.ctor(System.Collections.IList)
 void Enumerator__ctor_m7355 (Enumerator_t1436 * __this, Object_t * ___list, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.RegularExpressions.IntervalCollection/Enumerator::get_Current()
 Object_t * Enumerator_get_Current_m7356 (Enumerator_t1436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.IntervalCollection/Enumerator::MoveNext()
 bool Enumerator_MoveNext_m7357 (Enumerator_t1436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.IntervalCollection/Enumerator::Reset()
 void Enumerator_Reset_m7358 (Enumerator_t1436 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
