﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.ObjectTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_26.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ObjectTargetBehaviour>
struct CachedInvokableCall_1_t2901  : public InvokableCall_1_t2902
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.ObjectTargetBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
