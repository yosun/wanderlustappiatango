﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.StencilMaterial/MatEntry
struct MatEntry_t362;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.StencilMaterial/MatEntry>
struct Predicate_1_t3510  : public MulticastDelegate_t325
{
};
