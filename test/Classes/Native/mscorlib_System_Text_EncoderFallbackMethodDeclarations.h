﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.EncoderFallback
struct EncoderFallback_t2150;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t2141;

// System.Void System.Text.EncoderFallback::.ctor()
 void EncoderFallback__ctor_m12266 (EncoderFallback_t2150 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.EncoderFallback::.cctor()
 void EncoderFallback__cctor_m12267 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.EncoderFallback::get_ExceptionFallback()
 EncoderFallback_t2150 * EncoderFallback_get_ExceptionFallback_m12268 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.EncoderFallback::get_ReplacementFallback()
 EncoderFallback_t2150 * EncoderFallback_get_ReplacementFallback_m12269 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.EncoderFallback::get_StandardSafeFallback()
 EncoderFallback_t2150 * EncoderFallback_get_StandardSafeFallback_m12270 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallbackBuffer System.Text.EncoderFallback::CreateFallbackBuffer()
