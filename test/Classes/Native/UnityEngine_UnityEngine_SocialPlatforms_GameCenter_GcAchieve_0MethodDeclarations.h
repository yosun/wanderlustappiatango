﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData
struct GcAchievementData_t931;
struct GcAchievementData_t931_marshaled;
// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t1044;

// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
 Achievement_t1044 * GcAchievementData_ToAchievement_m6247 (GcAchievementData_t931 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void GcAchievementData_t931_marshal(const GcAchievementData_t931& unmarshaled, GcAchievementData_t931_marshaled& marshaled);
void GcAchievementData_t931_marshal_back(const GcAchievementData_t931_marshaled& marshaled, GcAchievementData_t931& unmarshaled);
void GcAchievementData_t931_marshal_cleanup(GcAchievementData_t931_marshaled& marshaled);
