﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$48
struct $ArrayType$48_t1651;
struct $ArrayType$48_t1651_marshaled;

void $ArrayType$48_t1651_marshal(const $ArrayType$48_t1651& unmarshaled, $ArrayType$48_t1651_marshaled& marshaled);
void $ArrayType$48_t1651_marshal_back(const $ArrayType$48_t1651_marshaled& marshaled, $ArrayType$48_t1651& unmarshaled);
void $ArrayType$48_t1651_marshal_cleanup($ArrayType$48_t1651_marshaled& marshaled);
