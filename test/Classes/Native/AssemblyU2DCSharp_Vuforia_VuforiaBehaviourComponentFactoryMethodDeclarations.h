﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VuforiaBehaviourComponentFactory
struct VuforiaBehaviourComponentFactory_t54;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t55;
// UnityEngine.GameObject
struct GameObject_t2;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t56;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t57;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t50;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t58;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t59;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t34;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t60;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t61;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t62;

// System.Void Vuforia.VuforiaBehaviourComponentFactory::.ctor()
 void VuforiaBehaviourComponentFactory__ctor_m147 (VuforiaBehaviourComponentFactory_t54 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MaskOutAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
 MaskOutAbstractBehaviour_t55 * VuforiaBehaviourComponentFactory_AddMaskOutBehaviour_m148 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
 VirtualButtonAbstractBehaviour_t56 * VuforiaBehaviourComponentFactory_AddVirtualButtonBehaviour_m149 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TurnOffAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
 TurnOffAbstractBehaviour_t57 * VuforiaBehaviourComponentFactory_AddTurnOffBehaviour_m150 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
 ImageTargetAbstractBehaviour_t50 * VuforiaBehaviourComponentFactory_AddImageTargetBehaviour_m151 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
 MarkerAbstractBehaviour_t58 * VuforiaBehaviourComponentFactory_AddMarkerBehaviour_m152 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
 MultiTargetAbstractBehaviour_t59 * VuforiaBehaviourComponentFactory_AddMultiTargetBehaviour_m153 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
 CylinderTargetAbstractBehaviour_t34 * VuforiaBehaviourComponentFactory_AddCylinderTargetBehaviour_m154 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
 WordAbstractBehaviour_t60 * VuforiaBehaviourComponentFactory_AddWordBehaviour_m155 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextRecoAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
 TextRecoAbstractBehaviour_t61 * VuforiaBehaviourComponentFactory_AddTextRecoBehaviour_m156 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.VuforiaBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
 ObjectTargetAbstractBehaviour_t62 * VuforiaBehaviourComponentFactory_AddObjectTargetBehaviour_m157 (VuforiaBehaviourComponentFactory_t54 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
