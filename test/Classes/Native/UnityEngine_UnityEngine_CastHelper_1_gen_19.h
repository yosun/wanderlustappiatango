﻿#pragma once
#include <stdint.h>
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t69;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Vuforia.PropAbstractBehaviour>
struct CastHelper_1_t4153 
{
	// T UnityEngine.CastHelper`1<Vuforia.PropAbstractBehaviour>::t
	PropAbstractBehaviour_t69 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Vuforia.PropAbstractBehaviour>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
