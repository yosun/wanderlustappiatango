﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_108.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoAbstractBehaviour>
struct CachedInvokableCall_1_t4344  : public InvokableCall_1_t4345
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
