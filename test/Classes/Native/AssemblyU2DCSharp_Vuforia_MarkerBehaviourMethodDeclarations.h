﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerBehaviour
struct MarkerBehaviour_t65;

// System.Void Vuforia.MarkerBehaviour::.ctor()
 void MarkerBehaviour__ctor_m159 (MarkerBehaviour_t65 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
