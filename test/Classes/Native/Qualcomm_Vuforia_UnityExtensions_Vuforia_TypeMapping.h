﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t681;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.TypeMapping
struct TypeMapping_t682  : public Object_t
{
};
struct TypeMapping_t682_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16> Vuforia.TypeMapping::sTypes
	Dictionary_2_t681 * ___sTypes_0;
};
