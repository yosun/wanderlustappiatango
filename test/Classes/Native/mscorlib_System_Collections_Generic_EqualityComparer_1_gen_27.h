﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Type>
struct EqualityComparer_1_t3723;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Type>
struct EqualityComparer_1_t3723  : public Object_t
{
};
struct EqualityComparer_1_t3723_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Type>::_default
	EqualityComparer_1_t3723 * ____default_0;
};
