﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
struct Enumerator_t4392;
// System.Object
struct Object_t;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t4389;

// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.HashSet`1<T>)
 void Enumerator__ctor_m26928_gshared (Enumerator_t4392 * __this, HashSet_1_t4389 * ___hashset, MethodInfo* method);
#define Enumerator__ctor_m26928(__this, ___hashset, method) (void)Enumerator__ctor_m26928_gshared((Enumerator_t4392 *)__this, (HashSet_1_t4389 *)___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26929_gshared (Enumerator_t4392 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m26929(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m26929_gshared((Enumerator_t4392 *)__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<System.Object>::MoveNext()
 bool Enumerator_MoveNext_m26930_gshared (Enumerator_t4392 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m26930(__this, method) (bool)Enumerator_MoveNext_m26930_gshared((Enumerator_t4392 *)__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<System.Object>::get_Current()
 Object_t * Enumerator_get_Current_m26931_gshared (Enumerator_t4392 * __this, MethodInfo* method);
#define Enumerator_get_Current_m26931(__this, method) (Object_t *)Enumerator_get_Current_m26931_gshared((Enumerator_t4392 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::Dispose()
 void Enumerator_Dispose_m26932_gshared (Enumerator_t4392 * __this, MethodInfo* method);
#define Enumerator_Dispose_m26932(__this, method) (void)Enumerator_Dispose_m26932_gshared((Enumerator_t4392 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<System.Object>::CheckState()
 void Enumerator_CheckState_m26933_gshared (Enumerator_t4392 * __this, MethodInfo* method);
#define Enumerator_CheckState_m26933(__this, method) (void)Enumerator_CheckState_m26933_gshared((Enumerator_t4392 *)__this, method)
