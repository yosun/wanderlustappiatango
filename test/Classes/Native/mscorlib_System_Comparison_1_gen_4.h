﻿#pragma once
#include <stdint.h>
// System.Reflection.MethodInfo
struct MethodInfo_t142;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<System.Reflection.MethodInfo>
struct Comparison_1_t2844  : public MulticastDelegate_t325
{
};
