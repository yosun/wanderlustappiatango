﻿#pragma once
#include <stdint.h>
// System.Reflection.Assembly
struct Assembly_t1503;
// System.Object
struct Object_t;
// System.ResolveEventArgs
struct ResolveEventArgs_t2243;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.ResolveEventHandler
struct ResolveEventHandler_t2184  : public MulticastDelegate_t325
{
};
