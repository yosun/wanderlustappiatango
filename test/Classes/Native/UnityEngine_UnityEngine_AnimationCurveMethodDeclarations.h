﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AnimationCurve
struct AnimationCurve_t1018;
struct AnimationCurve_t1018_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1019;

// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
 void AnimationCurve__ctor_m6143 (AnimationCurve_t1018 * __this, KeyframeU5BU5D_t1019* ___keys, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
 void AnimationCurve__ctor_m6144 (AnimationCurve_t1018 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
 void AnimationCurve_Cleanup_m6145 (AnimationCurve_t1018 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Finalize()
 void AnimationCurve_Finalize_m6146 (AnimationCurve_t1018 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
 void AnimationCurve_Init_m6147 (AnimationCurve_t1018 * __this, KeyframeU5BU5D_t1019* ___keys, MethodInfo* method) IL2CPP_METHOD_ATTR;
void AnimationCurve_t1018_marshal(const AnimationCurve_t1018& unmarshaled, AnimationCurve_t1018_marshaled& marshaled);
void AnimationCurve_t1018_marshal_back(const AnimationCurve_t1018_marshaled& marshaled, AnimationCurve_t1018& unmarshaled);
void AnimationCurve_t1018_marshal_cleanup(AnimationCurve_t1018_marshaled& marshaled);
