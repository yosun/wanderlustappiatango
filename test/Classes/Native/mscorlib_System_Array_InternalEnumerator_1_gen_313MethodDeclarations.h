﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.TextTrackerImpl/UpDirection>
struct InternalEnumerator_1_t3935;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.TextTrackerImpl/UpDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrackerImpl_UpD.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.TextTrackerImpl/UpDirection>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m22787 (InternalEnumerator_1_t3935 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.TextTrackerImpl/UpDirection>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22788 (InternalEnumerator_1_t3935 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.TextTrackerImpl/UpDirection>::Dispose()
 void InternalEnumerator_1_Dispose_m22789 (InternalEnumerator_1_t3935 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.TextTrackerImpl/UpDirection>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m22790 (InternalEnumerator_1_t3935 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.TextTrackerImpl/UpDirection>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m22791 (InternalEnumerator_1_t3935 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
