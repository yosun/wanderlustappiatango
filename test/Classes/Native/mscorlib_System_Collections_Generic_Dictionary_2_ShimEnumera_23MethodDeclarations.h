﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,Vuforia.WebCamProfile/ProfileData>
struct ShimEnumerator_t4279;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t748;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m26064 (ShimEnumerator_t4279 * __this, Dictionary_2_t748 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,Vuforia.WebCamProfile/ProfileData>::MoveNext()
 bool ShimEnumerator_MoveNext_m26065 (ShimEnumerator_t4279 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_Entry()
 DictionaryEntry_t1302  ShimEnumerator_get_Entry_m26066 (ShimEnumerator_t4279 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_Key()
 Object_t * ShimEnumerator_get_Key_m26067 (ShimEnumerator_t4279 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_Value()
 Object_t * ShimEnumerator_get_Value_m26068 (ShimEnumerator_t4279 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,Vuforia.WebCamProfile/ProfileData>::get_Current()
 Object_t * ShimEnumerator_get_Current_m26069 (ShimEnumerator_t4279 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
