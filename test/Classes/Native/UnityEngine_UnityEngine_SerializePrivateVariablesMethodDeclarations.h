﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SerializePrivateVariables
struct SerializePrivateVariables_t984;

// System.Void UnityEngine.SerializePrivateVariables::.ctor()
 void SerializePrivateVariables__ctor_m5910 (SerializePrivateVariables_t984 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
