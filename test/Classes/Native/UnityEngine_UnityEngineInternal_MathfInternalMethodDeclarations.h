﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngineInternal.MathfInternal
struct MathfInternal_t978;

// System.Void UnityEngineInternal.MathfInternal::.cctor()
 void MathfInternal__cctor_m5887 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
