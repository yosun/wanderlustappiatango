﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ILayoutElement
struct ILayoutElement_t399;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct Func_2_t397  : public MulticastDelegate_t325
{
};
