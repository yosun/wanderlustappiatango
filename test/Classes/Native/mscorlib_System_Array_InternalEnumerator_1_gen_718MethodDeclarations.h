﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Security.Cryptography.CipherMode>
struct InternalEnumerator_1_t5103;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Security.Cryptography.CipherMode
#include "mscorlib_System_Security_Cryptography_CipherMode.h"

// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.CipherMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31095 (InternalEnumerator_1_t5103 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.CipherMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31096 (InternalEnumerator_1_t5103 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.CipherMode>::Dispose()
 void InternalEnumerator_1_Dispose_m31097 (InternalEnumerator_1_t5103 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.CipherMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31098 (InternalEnumerator_1_t5103 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.CipherMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31099 (InternalEnumerator_1_t5103 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
