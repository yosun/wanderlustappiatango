﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.TrackableBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_89.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TrackableBehaviour>
struct CachedInvokableCall_1_t3655  : public InvokableCall_1_t3656
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.TrackableBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
