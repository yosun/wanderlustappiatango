﻿#pragma once
#include <stdint.h>
// Vuforia.Trackable
struct Trackable_t550;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.Trackable>
struct Predicate_1_t3817  : public MulticastDelegate_t325
{
};
