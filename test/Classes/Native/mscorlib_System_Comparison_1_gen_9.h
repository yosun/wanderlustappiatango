﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.EventTrigger/Entry
struct Entry_t193;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct Comparison_1_t3112  : public MulticastDelegate_t325
{
};
