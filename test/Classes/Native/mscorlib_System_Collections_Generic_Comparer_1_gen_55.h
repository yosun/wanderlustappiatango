﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.Security.Policy.StrongName>
struct Comparer_1_t5120;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.Security.Policy.StrongName>
struct Comparer_1_t5120  : public Object_t
{
};
struct Comparer_1_t5120_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Security.Policy.StrongName>::_default
	Comparer_1_t5120 * ____default_0;
};
