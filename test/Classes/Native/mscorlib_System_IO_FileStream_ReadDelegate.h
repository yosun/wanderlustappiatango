﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t609;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo Int32_t93_il2cpp_TypeInfo;
// System.IO.FileStream/ReadDelegate
struct ReadDelegate_t1876  : public MulticastDelegate_t325
{
};
