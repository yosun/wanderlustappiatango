﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButton>
struct ShimEnumerator_t3840;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButton>
struct Dictionary_2_t613;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButton>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m21754 (ShimEnumerator_t3840 * __this, Dictionary_2_t613 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButton>::MoveNext()
 bool ShimEnumerator_MoveNext_m21755 (ShimEnumerator_t3840 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButton>::get_Entry()
 DictionaryEntry_t1302  ShimEnumerator_get_Entry_m21756 (ShimEnumerator_t3840 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButton>::get_Key()
 Object_t * ShimEnumerator_get_Key_m21757 (ShimEnumerator_t3840 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButton>::get_Value()
 Object_t * ShimEnumerator_get_Value_m21758 (ShimEnumerator_t3840 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButton>::get_Current()
 Object_t * ShimEnumerator_get_Current_m21759 (ShimEnumerator_t3840 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
