﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.SortedList/Enumerator
struct Enumerator_t1838;
// System.Object
struct Object_t;
// System.Collections.SortedList
struct SortedList_t1502;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.SortedList/EnumeratorMode
#include "mscorlib_System_Collections_SortedList_EnumeratorMode.h"

// System.Void System.Collections.SortedList/Enumerator::.ctor(System.Collections.SortedList,System.Collections.SortedList/EnumeratorMode)
 void Enumerator__ctor_m10409 (Enumerator_t1838 * __this, SortedList_t1502 * ___host, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/Enumerator::.cctor()
 void Enumerator__cctor_m10410 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.SortedList/Enumerator::Reset()
 void Enumerator_Reset_m10411 (Enumerator_t1838 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.SortedList/Enumerator::MoveNext()
 bool Enumerator_MoveNext_m10412 (Enumerator_t1838 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.SortedList/Enumerator::get_Entry()
 DictionaryEntry_t1302  Enumerator_get_Entry_m10413 (Enumerator_t1838 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/Enumerator::get_Key()
 Object_t * Enumerator_get_Key_m10414 (Enumerator_t1838 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/Enumerator::get_Value()
 Object_t * Enumerator_get_Value_m10415 (Enumerator_t1838 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.SortedList/Enumerator::get_Current()
 Object_t * Enumerator_get_Current_m10416 (Enumerator_t1838 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
