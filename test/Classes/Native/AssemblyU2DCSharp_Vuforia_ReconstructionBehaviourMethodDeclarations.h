﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionBehaviour
struct ReconstructionBehaviour_t38;

// System.Void Vuforia.ReconstructionBehaviour::.ctor()
 void ReconstructionBehaviour__ctor_m167 (ReconstructionBehaviour_t38 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
