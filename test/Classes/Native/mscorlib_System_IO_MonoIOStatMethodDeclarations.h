﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.MonoIOStat
struct MonoIOStat_t1879;
struct MonoIOStat_t1879_marshaled;

void MonoIOStat_t1879_marshal(const MonoIOStat_t1879& unmarshaled, MonoIOStat_t1879_marshaled& marshaled);
void MonoIOStat_t1879_marshal_back(const MonoIOStat_t1879_marshaled& marshaled, MonoIOStat_t1879& unmarshaled);
void MonoIOStat_t1879_marshal_cleanup(MonoIOStat_t1879_marshaled& marshaled);
