﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseInputModule>
struct EqualityComparer_1_t3014;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseInputModule>
struct EqualityComparer_1_t3014  : public Object_t
{
};
struct EqualityComparer_1_t3014_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.EventSystems.BaseInputModule>::_default
	EqualityComparer_1_t3014 * ____default_0;
};
