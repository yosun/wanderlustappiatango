﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.SpriteState
struct SpriteState_t355;
// UnityEngine.Sprite
struct Sprite_t310;

// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_highlightedSprite()
 Sprite_t310 * SpriteState_get_highlightedSprite_m1542 (SpriteState_t355 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_highlightedSprite(UnityEngine.Sprite)
 void SpriteState_set_highlightedSprite_m1543 (SpriteState_t355 * __this, Sprite_t310 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_pressedSprite()
 Sprite_t310 * SpriteState_get_pressedSprite_m1544 (SpriteState_t355 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_pressedSprite(UnityEngine.Sprite)
 void SpriteState_set_pressedSprite_m1545 (SpriteState_t355 * __this, Sprite_t310 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite UnityEngine.UI.SpriteState::get_disabledSprite()
 Sprite_t310 * SpriteState_get_disabledSprite_m1546 (SpriteState_t355 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.SpriteState::set_disabledSprite(UnityEngine.Sprite)
 void SpriteState_set_disabledSprite_m1547 (SpriteState_t355 * __this, Sprite_t310 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
