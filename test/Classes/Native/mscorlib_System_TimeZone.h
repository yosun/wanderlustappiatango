﻿#pragma once
#include <stdint.h>
// System.TimeZone
struct TimeZone_t2249;
// System.Object
#include "mscorlib_System_Object.h"
// System.TimeZone
struct TimeZone_t2249  : public Object_t
{
};
struct TimeZone_t2249_StaticFields{
	// System.TimeZone System.TimeZone::currentTimeZone
	TimeZone_t2249 * ___currentTimeZone_0;
};
