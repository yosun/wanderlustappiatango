﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TargetFinderImpl/TargetFinderState
struct TargetFinderState_t735;
struct TargetFinderState_t735_marshaled;

void TargetFinderState_t735_marshal(const TargetFinderState_t735& unmarshaled, TargetFinderState_t735_marshaled& marshaled);
void TargetFinderState_t735_marshal_back(const TargetFinderState_t735_marshaled& marshaled, TargetFinderState_t735& unmarshaled);
void TargetFinderState_t735_marshal_cleanup(TargetFinderState_t735_marshaled& marshaled);
