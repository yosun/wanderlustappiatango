﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$12
struct $ArrayType$12_t1653;
struct $ArrayType$12_t1653_marshaled;

void $ArrayType$12_t1653_marshal(const $ArrayType$12_t1653& unmarshaled, $ArrayType$12_t1653_marshaled& marshaled);
void $ArrayType$12_t1653_marshal_back(const $ArrayType$12_t1653_marshaled& marshaled, $ArrayType$12_t1653& unmarshaled);
void $ArrayType$12_t1653_marshal_cleanup($ArrayType$12_t1653_marshaled& marshaled);
