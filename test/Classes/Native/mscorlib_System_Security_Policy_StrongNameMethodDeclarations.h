﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Policy.StrongName
struct StrongName_t2133;
// System.String
struct String_t;
// System.Security.Permissions.StrongNamePublicKeyBlob
struct StrongNamePublicKeyBlob_t2128;
// System.Version
struct Version_t1333;
// System.Object
struct Object_t;

// System.String System.Security.Policy.StrongName::get_Name()
 String_t* StrongName_get_Name_m12168 (StrongName_t2133 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Permissions.StrongNamePublicKeyBlob System.Security.Policy.StrongName::get_PublicKey()
 StrongNamePublicKeyBlob_t2128 * StrongName_get_PublicKey_m12169 (StrongName_t2133 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Version System.Security.Policy.StrongName::get_Version()
 Version_t1333 * StrongName_get_Version_m12170 (StrongName_t2133 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Policy.StrongName::Equals(System.Object)
 bool StrongName_Equals_m12171 (StrongName_t2133 * __this, Object_t * ___o, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Policy.StrongName::GetHashCode()
 int32_t StrongName_GetHashCode_m12172 (StrongName_t2133 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Security.Policy.StrongName::ToString()
 String_t* StrongName_ToString_m12173 (StrongName_t2133 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
