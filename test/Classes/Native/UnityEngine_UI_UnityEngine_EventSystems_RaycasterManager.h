﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster>
struct List_1_t242;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.EventSystems.RaycasterManager
struct RaycasterManager_t243  : public Object_t
{
};
struct RaycasterManager_t243_StaticFields{
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseRaycaster> UnityEngine.EventSystems.RaycasterManager::s_Raycasters
	List_1_t242 * ___s_Raycasters_0;
};
