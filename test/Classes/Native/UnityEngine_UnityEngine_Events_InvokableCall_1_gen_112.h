﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.VideoTextureRendererAbstractBehaviour>
struct UnityAction_1_t4402;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRendererAbstractBehaviour>
struct InvokableCall_1_t4401  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.VideoTextureRendererAbstractBehaviour>::Delegate
	UnityAction_1_t4402 * ___Delegate_0;
};
