﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t268;

// System.Void UnityEngine.UI.Button/ButtonClickedEvent::.ctor()
 void ButtonClickedEvent__ctor_m971 (ButtonClickedEvent_t268 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
