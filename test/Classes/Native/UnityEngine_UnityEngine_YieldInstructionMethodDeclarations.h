﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.YieldInstruction
struct YieldInstruction_t917;
struct YieldInstruction_t917_marshaled;

// System.Void UnityEngine.YieldInstruction::.ctor()
 void YieldInstruction__ctor_m6076 (YieldInstruction_t917 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void YieldInstruction_t917_marshal(const YieldInstruction_t917& unmarshaled, YieldInstruction_t917_marshaled& marshaled);
void YieldInstruction_t917_marshal_back(const YieldInstruction_t917_marshaled& marshaled, YieldInstruction_t917& unmarshaled);
void YieldInstruction_t917_marshal_cleanup(YieldInstruction_t917_marshaled& marshaled);
