﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.NullWebCamTexAdaptor
struct NullWebCamTexAdaptor_t629;
// UnityEngine.Texture
struct Texture_t294;
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"

// System.Boolean Vuforia.NullWebCamTexAdaptor::get_DidUpdateThisFrame()
 bool NullWebCamTexAdaptor_get_DidUpdateThisFrame_m2960 (NullWebCamTexAdaptor_t629 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.NullWebCamTexAdaptor::get_IsPlaying()
 bool NullWebCamTexAdaptor_get_IsPlaying_m2961 (NullWebCamTexAdaptor_t629 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture Vuforia.NullWebCamTexAdaptor::get_Texture()
 Texture_t294 * NullWebCamTexAdaptor_get_Texture_m2962 (NullWebCamTexAdaptor_t629 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullWebCamTexAdaptor::.ctor(System.Int32,Vuforia.QCARRenderer/Vec2I)
 void NullWebCamTexAdaptor__ctor_m2963 (NullWebCamTexAdaptor_t629 * __this, int32_t ___requestedFPS, Vec2I_t630  ___requestedTextureSize, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullWebCamTexAdaptor::Play()
 void NullWebCamTexAdaptor_Play_m2964 (NullWebCamTexAdaptor_t629 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullWebCamTexAdaptor::Stop()
 void NullWebCamTexAdaptor_Stop_m2965 (NullWebCamTexAdaptor_t629 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
