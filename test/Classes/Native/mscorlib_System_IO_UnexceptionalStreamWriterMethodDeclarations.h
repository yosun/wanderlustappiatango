﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.UnexceptionalStreamWriter
struct UnexceptionalStreamWriter_t1895;
// System.IO.Stream
struct Stream_t1599;
// System.Text.Encoding
struct Encoding_t1513;
// System.Char[]
struct CharU5BU5D_t108;
// System.String
struct String_t;

// System.Void System.IO.UnexceptionalStreamWriter::.ctor(System.IO.Stream,System.Text.Encoding)
 void UnexceptionalStreamWriter__ctor_m10930 (UnexceptionalStreamWriter_t1895 * __this, Stream_t1599 * ___stream, Encoding_t1513 * ___encoding, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Flush()
 void UnexceptionalStreamWriter_Flush_m10931 (UnexceptionalStreamWriter_t1895 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Write(System.Char[],System.Int32,System.Int32)
 void UnexceptionalStreamWriter_Write_m10932 (UnexceptionalStreamWriter_t1895 * __this, CharU5BU5D_t108* ___buffer, int32_t ___index, int32_t ___count, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Write(System.Char)
 void UnexceptionalStreamWriter_Write_m10933 (UnexceptionalStreamWriter_t1895 * __this, uint16_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Write(System.Char[])
 void UnexceptionalStreamWriter_Write_m10934 (UnexceptionalStreamWriter_t1895 * __this, CharU5BU5D_t108* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.UnexceptionalStreamWriter::Write(System.String)
 void UnexceptionalStreamWriter_Write_m10935 (UnexceptionalStreamWriter_t1895 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
