﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t291;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t6;
// UnityEngine.UI.CoroutineTween.ColorTween
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
 void TweenRunner_1__ctor_m2112 (TweenRunner_1_t291 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
 Object_t * TweenRunner_1_Start_m18172 (Object_t * __this/* static, unused */, ColorTween_t262  ___tweenInfo, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
 void TweenRunner_1_Init_m2113 (TweenRunner_1_t291 * __this, MonoBehaviour_t6 * ___coroutineContainer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
 void TweenRunner_1_StartTween_m2147 (TweenRunner_1_t291 * __this, ColorTween_t262  ___info, MethodInfo* method) IL2CPP_METHOD_ATTR;
