﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.ISmartTerrainEventHandler>
struct EqualityComparer_1_t4074;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.ISmartTerrainEventHandler>
struct EqualityComparer_1_t4074  : public Object_t
{
};
struct EqualityComparer_1_t4074_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.ISmartTerrainEventHandler>::_default
	EqualityComparer_1_t4074 * ____default_0;
};
