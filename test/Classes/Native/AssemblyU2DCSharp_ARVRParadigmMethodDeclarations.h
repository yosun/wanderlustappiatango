﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ARVRParadigm
struct ARVRParadigm_t1;

// System.Void ARVRParadigm::.ctor()
 void ARVRParadigm__ctor_m0 (ARVRParadigm_t1 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRParadigm::Start()
 void ARVRParadigm_Start_m1 (ARVRParadigm_t1 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRParadigm::Update()
 void ARVRParadigm_Update_m2 (ARVRParadigm_t1 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
