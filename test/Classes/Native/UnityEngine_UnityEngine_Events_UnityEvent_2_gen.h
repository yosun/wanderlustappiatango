﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
struct UnityEvent_2_t4826  : public UnityEventBase_t1084
{
	// System.Object[] UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::m_InvokeArray
	ObjectU5BU5D_t115* ___m_InvokeArray_4;
};
