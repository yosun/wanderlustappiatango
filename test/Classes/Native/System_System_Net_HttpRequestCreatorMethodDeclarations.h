﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.HttpRequestCreator
struct HttpRequestCreator_t1332;
// System.Net.WebRequest
struct WebRequest_t1321;
// System.Uri
struct Uri_t1322;

// System.Void System.Net.HttpRequestCreator::.ctor()
 void HttpRequestCreator__ctor_m6820 (HttpRequestCreator_t1332 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.HttpRequestCreator::Create(System.Uri)
 WebRequest_t1321 * HttpRequestCreator_Create_m6821 (HttpRequestCreator_t1332 * __this, Uri_t1322 * ___uri, MethodInfo* method) IL2CPP_METHOD_ATTR;
