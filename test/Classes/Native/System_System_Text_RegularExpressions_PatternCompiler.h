﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t1308;
// System.Object
#include "mscorlib_System_Object.h"
// System.Text.RegularExpressions.PatternCompiler
struct PatternCompiler_t1425  : public Object_t
{
	// System.Collections.ArrayList System.Text.RegularExpressions.PatternCompiler::pgm
	ArrayList_t1308 * ___pgm_0;
};
