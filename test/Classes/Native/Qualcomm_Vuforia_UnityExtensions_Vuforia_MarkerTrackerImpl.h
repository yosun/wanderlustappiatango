﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker>
struct Dictionary_2_t625;
// Vuforia.MarkerTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_MarkerTracker.h"
// Vuforia.MarkerTrackerImpl
struct MarkerTrackerImpl_t626  : public MarkerTracker_t622
{
	// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Marker> Vuforia.MarkerTrackerImpl::mMarkerDict
	Dictionary_2_t625 * ___mMarkerDict_1;
};
