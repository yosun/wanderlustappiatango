﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.String>
struct List_1_t587;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t3737;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_t3738;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t3739;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.String>
struct ReadOnlyCollection_1_t3740;
// System.String[]
struct StringU5BU5D_t112;
// System.Predicate`1<System.String>
struct Predicate_1_t3741;
// System.Comparison`1<System.String>
struct Comparison_1_t3742;
// System.Collections.Generic.List`1/Enumerator<System.String>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2.h"

// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
#define List_1__ctor_m4558(__this, method) (void)List_1__ctor_m14543_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m20772(__this, ___collection, method) (void)List_1__ctor_m14545_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Int32)
#define List_1__ctor_m20773(__this, ___capacity, method) (void)List_1__ctor_m14547_gshared((List_1_t150 *)__this, (int32_t)___capacity, method)
// System.Void System.Collections.Generic.List`1<System.String>::.cctor()
#define List_1__cctor_m20774(__this/* static, unused */, method) (void)List_1__cctor_m14549_gshared((Object_t *)__this/* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.String>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20775(__this, method) (Object_t*)List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14551_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m20776(__this, ___array, ___arrayIndex, method) (void)List_1_System_Collections_ICollection_CopyTo_m14553_gshared((List_1_t150 *)__this, (Array_t *)___array, (int32_t)___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.String>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m20777(__this, method) (Object_t *)List_1_System_Collections_IEnumerable_GetEnumerator_m14555_gshared((List_1_t150 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m20778(__this, ___item, method) (int32_t)List_1_System_Collections_IList_Add_m14557_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m20779(__this, ___item, method) (bool)List_1_System_Collections_IList_Contains_m14559_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m20780(__this, ___item, method) (int32_t)List_1_System_Collections_IList_IndexOf_m14561_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m20781(__this, ___index, ___item, method) (void)List_1_System_Collections_IList_Insert_m14563_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m20782(__this, ___item, method) (void)List_1_System_Collections_IList_Remove_m14565_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20783(__this, method) (bool)List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14567_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m20784(__this, method) (bool)List_1_System_Collections_ICollection_get_IsSynchronized_m14569_gshared((List_1_t150 *)__this, method)
// System.Object System.Collections.Generic.List`1<System.String>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m20785(__this, method) (Object_t *)List_1_System_Collections_ICollection_get_SyncRoot_m14571_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m20786(__this, method) (bool)List_1_System_Collections_IList_get_IsFixedSize_m14573_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m20787(__this, method) (bool)List_1_System_Collections_IList_get_IsReadOnly_m14575_gshared((List_1_t150 *)__this, method)
// System.Object System.Collections.Generic.List`1<System.String>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m20788(__this, ___index, method) (Object_t *)List_1_System_Collections_IList_get_Item_m14577_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m20789(__this, ___index, ___value, method) (void)List_1_System_Collections_IList_set_Item_m14579_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.Generic.List`1<System.String>::Add(T)
#define List_1_Add_m4557(__this, ___item, method) (void)List_1_Add_m14581_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m20790(__this, ___newCount, method) (void)List_1_GrowIfNeeded_m14583_gshared((List_1_t150 *)__this, (int32_t)___newCount, method)
// System.Void System.Collections.Generic.List`1<System.String>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m20791(__this, ___collection, method) (void)List_1_AddCollection_m14585_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<System.String>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m20792(__this, ___enumerable, method) (void)List_1_AddEnumerable_m14587_gshared((List_1_t150 *)__this, (Object_t*)___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.String>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m20793(__this, ___collection, method) (void)List_1_AddRange_m14588_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.String>::AsReadOnly()
#define List_1_AsReadOnly_m20794(__this, method) (ReadOnlyCollection_1_t3740 *)List_1_AsReadOnly_m14590_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::Clear()
#define List_1_Clear_m20795(__this, method) (void)List_1_Clear_m14592_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::Contains(T)
#define List_1_Contains_m4553(__this, ___item, method) (bool)List_1_Contains_m14594_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m20796(__this, ___array, ___arrayIndex, method) (void)List_1_CopyTo_m14596_gshared((List_1_t150 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___arrayIndex, method)
// T System.Collections.Generic.List`1<System.String>::Find(System.Predicate`1<T>)
#define List_1_Find_m20797(__this, ___match, method) (String_t*)List_1_Find_m14598_gshared((List_1_t150 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void System.Collections.Generic.List`1<System.String>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m20798(__this/* static, unused */, ___match, method) (void)List_1_CheckMatch_m14600_gshared((Object_t *)__this/* static, unused */, (Predicate_1_t2845 *)___match, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m20799(__this, ___startIndex, ___count, ___match, method) (int32_t)List_1_GetIndex_m14602_gshared((List_1_t150 *)__this, (int32_t)___startIndex, (int32_t)___count, (Predicate_1_t2845 *)___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.String>::GetEnumerator()
 Enumerator_t789  List_1_GetEnumerator_m4548 (List_1_t587 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<System.String>::IndexOf(T)
#define List_1_IndexOf_m20800(__this, ___item, method) (int32_t)List_1_IndexOf_m14604_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m20801(__this, ___start, ___delta, method) (void)List_1_Shift_m14606_gshared((List_1_t150 *)__this, (int32_t)___start, (int32_t)___delta, method)
// System.Void System.Collections.Generic.List`1<System.String>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m20802(__this, ___index, method) (void)List_1_CheckIndex_m14608_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::Insert(System.Int32,T)
#define List_1_Insert_m20803(__this, ___index, ___item, method) (void)List_1_Insert_m14610_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m20804(__this, ___collection, method) (void)List_1_CheckCollection_m14612_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::Remove(T)
#define List_1_Remove_m20805(__this, ___item, method) (bool)List_1_Remove_m14614_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m20806(__this, ___match, method) (int32_t)List_1_RemoveAll_m14616_gshared((List_1_t150 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void System.Collections.Generic.List`1<System.String>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m20807(__this, ___index, method) (void)List_1_RemoveAt_m14618_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::Reverse()
#define List_1_Reverse_m20808(__this, method) (void)List_1_Reverse_m14620_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::Sort()
#define List_1_Sort_m20809(__this, method) (void)List_1_Sort_m14622_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m20810(__this, ___comparison, method) (void)List_1_Sort_m14624_gshared((List_1_t150 *)__this, (Comparison_1_t2846 *)___comparison, method)
// T[] System.Collections.Generic.List`1<System.String>::ToArray()
#define List_1_ToArray_m13348(__this, method) (StringU5BU5D_t112*)List_1_ToArray_m14626_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::TrimExcess()
#define List_1_TrimExcess_m20811(__this, method) (void)List_1_TrimExcess_m14628_gshared((List_1_t150 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Capacity()
#define List_1_get_Capacity_m20812(__this, method) (int32_t)List_1_get_Capacity_m14630_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m20813(__this, ___value, method) (void)List_1_set_Capacity_m14632_gshared((List_1_t150 *)__this, (int32_t)___value, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
#define List_1_get_Count_m4551(__this, method) (int32_t)List_1_get_Count_m14634_gshared((List_1_t150 *)__this, method)
// T System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
#define List_1_get_Item_m20814(__this, ___index, method) (String_t*)List_1_get_Item_m14636_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::set_Item(System.Int32,T)
#define List_1_set_Item_m20815(__this, ___index, ___value, method) (void)List_1_set_Item_m14638_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___value, method)
