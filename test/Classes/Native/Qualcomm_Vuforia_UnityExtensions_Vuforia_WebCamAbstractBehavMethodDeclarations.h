﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamAbstractBehaviour
struct WebCamAbstractBehaviour_t89;
// System.String
struct String_t;
// Vuforia.WebCamImpl
struct WebCamImpl_t604;

// System.Boolean Vuforia.WebCamAbstractBehaviour::get_PlayModeRenderVideo()
 bool WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4229 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_PlayModeRenderVideo(System.Boolean)
 void WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4230 (WebCamAbstractBehaviour_t89 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WebCamAbstractBehaviour::get_DeviceName()
 String_t* WebCamAbstractBehaviour_get_DeviceName_m4231 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_DeviceName(System.String)
 void WebCamAbstractBehaviour_set_DeviceName_m4232 (WebCamAbstractBehaviour_t89 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_FlipHorizontally()
 bool WebCamAbstractBehaviour_get_FlipHorizontally_m4233 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_FlipHorizontally(System.Boolean)
 void WebCamAbstractBehaviour_set_FlipHorizontally_m4234 (WebCamAbstractBehaviour_t89 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_TurnOffWebCam()
 bool WebCamAbstractBehaviour_get_TurnOffWebCam_m4235 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::set_TurnOffWebCam(System.Boolean)
 void WebCamAbstractBehaviour_set_TurnOffWebCam_m4236 (WebCamAbstractBehaviour_t89 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_IsPlaying()
 bool WebCamAbstractBehaviour_get_IsPlaying_m4237 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::IsWebCamUsed()
 bool WebCamAbstractBehaviour_IsWebCamUsed_m4238 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WebCamImpl Vuforia.WebCamAbstractBehaviour::get_ImplementationClass()
 WebCamImpl_t604 * WebCamAbstractBehaviour_get_ImplementationClass_m4239 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::InitCamera()
 void WebCamAbstractBehaviour_InitCamera_m4240 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamAbstractBehaviour::CheckNativePluginSupport()
 bool WebCamAbstractBehaviour_CheckNativePluginSupport_m4241 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::OnLevelWasLoaded()
 void WebCamAbstractBehaviour_OnLevelWasLoaded_m4242 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::OnDestroy()
 void WebCamAbstractBehaviour_OnDestroy_m4243 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::Update()
 void WebCamAbstractBehaviour_Update_m4244 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WebCamAbstractBehaviour::qcarCheckNativePluginSupport()
 int32_t WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4245 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamAbstractBehaviour::.ctor()
 void WebCamAbstractBehaviour__ctor_m635 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
