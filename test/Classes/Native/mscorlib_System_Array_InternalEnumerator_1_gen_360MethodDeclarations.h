﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>
struct InternalEnumerator_1_t4393;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.HashSet`1/Link<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m26923 (InternalEnumerator_1_t4393 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26924 (InternalEnumerator_1_t4393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
 void InternalEnumerator_1_Dispose_m26925 (InternalEnumerator_1_t4393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m26926 (InternalEnumerator_1_t4393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
 Link_t4391  InternalEnumerator_1_get_Current_m26927 (InternalEnumerator_1_t4393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
