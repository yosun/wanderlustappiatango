﻿#pragma once
#include <stdint.h>
// System.Boolean[]
struct BooleanU5BU5D_t1345;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Reflection.ParameterModifier
struct ParameterModifier_t1164 
{
	// System.Boolean[] System.Reflection.ParameterModifier::_byref
	BooleanU5BU5D_t1345* ____byref_0;
};
// Native definition for marshalling of: System.Reflection.ParameterModifier
struct ParameterModifier_t1164_marshaled
{
	int32_t* ____byref_0;
};
