﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Collider
struct Collider_t132;
// UnityEngine.Rigidbody
struct Rigidbody_t1006;

// System.Void UnityEngine.Collider::set_enabled(System.Boolean)
 void Collider_set_enabled_m417 (Collider_t132 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
 Rigidbody_t1006 * Collider_get_attachedRigidbody_m6098 (Collider_t132 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
