﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>
struct UnityAction_1_t2800;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>
struct InvokableCall_1_t2799  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::Delegate
	UnityAction_1_t2800 * ___Delegate_0;
};
