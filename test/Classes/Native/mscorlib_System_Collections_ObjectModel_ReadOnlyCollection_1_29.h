﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.Marker>
struct IList_1_t3891;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Marker>
struct ReadOnlyCollection_1_t3888  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Marker>::list
	Object_t* ___list_0;
};
