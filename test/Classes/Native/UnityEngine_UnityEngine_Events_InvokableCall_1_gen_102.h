﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.PropAbstractBehaviour>
struct UnityAction_1_t4158;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.PropAbstractBehaviour>
struct InvokableCall_1_t4157  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.PropAbstractBehaviour>::Delegate
	UnityAction_1_t4158 * ___Delegate_0;
};
