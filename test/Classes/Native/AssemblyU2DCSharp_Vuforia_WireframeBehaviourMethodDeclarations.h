﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t90;

// System.Void Vuforia.WireframeBehaviour::.ctor()
 void WireframeBehaviour__ctor_m181 (WireframeBehaviour_t90 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeBehaviour::CreateLineMaterial()
 void WireframeBehaviour_CreateLineMaterial_m182 (WireframeBehaviour_t90 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeBehaviour::OnRenderObject()
 void WireframeBehaviour_OnRenderObject_m183 (WireframeBehaviour_t90 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WireframeBehaviour::OnDrawGizmos()
 void WireframeBehaviour_OnDrawGizmos_m184 (WireframeBehaviour_t90 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
