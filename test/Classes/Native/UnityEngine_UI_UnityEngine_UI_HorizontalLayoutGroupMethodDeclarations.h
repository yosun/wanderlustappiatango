﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.HorizontalLayoutGroup
struct HorizontalLayoutGroup_t388;

// System.Void UnityEngine.UI.HorizontalLayoutGroup::.ctor()
 void HorizontalLayoutGroup__ctor_m1707 (HorizontalLayoutGroup_t388 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputHorizontal()
 void HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1708 (HorizontalLayoutGroup_t388 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalLayoutGroup::CalculateLayoutInputVertical()
 void HorizontalLayoutGroup_CalculateLayoutInputVertical_m1709 (HorizontalLayoutGroup_t388 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutHorizontal()
 void HorizontalLayoutGroup_SetLayoutHorizontal_m1710 (HorizontalLayoutGroup_t388 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalLayoutGroup::SetLayoutVertical()
 void HorizontalLayoutGroup_SetLayoutVertical_m1711 (HorizontalLayoutGroup_t388 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
