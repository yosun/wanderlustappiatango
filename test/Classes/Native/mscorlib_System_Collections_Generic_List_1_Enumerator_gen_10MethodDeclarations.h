﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>
struct Enumerator_t832;
// System.Object
struct Object_t;
// Vuforia.WordResult
struct WordResult_t702;
// System.Collections.Generic.List`1<Vuforia.WordResult>
struct List_1_t694;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m23133(__this, ___l, method) (void)Enumerator__ctor_m14640_gshared((Enumerator_t2850 *)__this, (List_1_t150 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23134(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14641_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::Dispose()
#define Enumerator_Dispose_m23135(__this, method) (void)Enumerator_Dispose_m14642_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::VerifyState()
#define Enumerator_VerifyState_m23136(__this, method) (void)Enumerator_VerifyState_m14643_gshared((Enumerator_t2850 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::MoveNext()
#define Enumerator_MoveNext_m5022(__this, method) (bool)Enumerator_MoveNext_m14644_gshared((Enumerator_t2850 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.WordResult>::get_Current()
#define Enumerator_get_Current_m5021(__this, method) (WordResult_t702 *)Enumerator_get_Current_m14645_gshared((Enumerator_t2850 *)__this, method)
