﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Trackable>
struct ShimEnumerator_t3812;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Trackable>
struct Dictionary_2_t606;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Trackable>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m21465 (ShimEnumerator_t3812 * __this, Dictionary_2_t606 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Trackable>::MoveNext()
 bool ShimEnumerator_MoveNext_m21466 (ShimEnumerator_t3812 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Trackable>::get_Entry()
 DictionaryEntry_t1302  ShimEnumerator_get_Entry_m21467 (ShimEnumerator_t3812 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Trackable>::get_Key()
 Object_t * ShimEnumerator_get_Key_m21468 (ShimEnumerator_t3812 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Trackable>::get_Value()
 Object_t * ShimEnumerator_get_Value_m21469 (ShimEnumerator_t3812 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Trackable>::get_Current()
 Object_t * ShimEnumerator_get_Current_m21470 (ShimEnumerator_t3812 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
