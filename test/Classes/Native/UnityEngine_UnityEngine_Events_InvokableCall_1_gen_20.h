﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.ImageTargetBehaviour>
struct UnityAction_1_t2834;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.ImageTargetBehaviour>
struct InvokableCall_1_t2833  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.ImageTargetBehaviour>::Delegate
	UnityAction_1_t2834 * ___Delegate_0;
};
