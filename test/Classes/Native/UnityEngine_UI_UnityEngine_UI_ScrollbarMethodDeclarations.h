﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Scrollbar
struct Scrollbar_t343;
// UnityEngine.RectTransform
struct RectTransform_t287;
// UnityEngine.UI.Scrollbar/ScrollEvent
struct ScrollEvent_t340;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t188;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t196;
// UnityEngine.UI.Selectable
struct Selectable_t272;
// UnityEngine.Transform
struct Transform_t10;
// UnityEngine.UI.Scrollbar/Direction
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction.h"
// UnityEngine.UI.Scrollbar/Axis
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis.h"
// UnityEngine.UI.CanvasUpdate
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate.h"

// System.Void UnityEngine.UI.Scrollbar::.ctor()
 void Scrollbar__ctor_m1340 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.Scrollbar::get_handleRect()
 RectTransform_t287 * Scrollbar_get_handleRect_m1341 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::set_handleRect(UnityEngine.RectTransform)
 void Scrollbar_set_handleRect_m1342 (Scrollbar_t343 * __this, RectTransform_t287 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Scrollbar/Direction UnityEngine.UI.Scrollbar::get_direction()
 int32_t Scrollbar_get_direction_m1343 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::set_direction(UnityEngine.UI.Scrollbar/Direction)
 void Scrollbar_set_direction_m1344 (Scrollbar_t343 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.Scrollbar::get_value()
 float Scrollbar_get_value_m1345 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::set_value(System.Single)
 void Scrollbar_set_value_m1346 (Scrollbar_t343 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.Scrollbar::get_size()
 float Scrollbar_get_size_m1347 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::set_size(System.Single)
 void Scrollbar_set_size_m1348 (Scrollbar_t343 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.Scrollbar::get_numberOfSteps()
 int32_t Scrollbar_get_numberOfSteps_m1349 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::set_numberOfSteps(System.Int32)
 void Scrollbar_set_numberOfSteps_m1350 (Scrollbar_t343 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Scrollbar/ScrollEvent UnityEngine.UI.Scrollbar::get_onValueChanged()
 ScrollEvent_t340 * Scrollbar_get_onValueChanged_m1351 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::set_onValueChanged(UnityEngine.UI.Scrollbar/ScrollEvent)
 void Scrollbar_set_onValueChanged_m1352 (Scrollbar_t343 * __this, ScrollEvent_t340 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.Scrollbar::get_stepSize()
 float Scrollbar_get_stepSize_m1353 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::Rebuild(UnityEngine.UI.CanvasUpdate)
 void Scrollbar_Rebuild_m1354 (Scrollbar_t343 * __this, int32_t ___executing, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnEnable()
 void Scrollbar_OnEnable_m1355 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnDisable()
 void Scrollbar_OnDisable_m1356 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::UpdateCachedReferences()
 void Scrollbar_UpdateCachedReferences_m1357 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::Set(System.Single)
 void Scrollbar_Set_m1358 (Scrollbar_t343 * __this, float ___input, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::Set(System.Single,System.Boolean)
 void Scrollbar_Set_m1359 (Scrollbar_t343 * __this, float ___input, bool ___sendCallback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnRectTransformDimensionsChange()
 void Scrollbar_OnRectTransformDimensionsChange_m1360 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Scrollbar/Axis UnityEngine.UI.Scrollbar::get_axis()
 int32_t Scrollbar_get_axis_m1361 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Scrollbar::get_reverseValue()
 bool Scrollbar_get_reverseValue_m1362 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::UpdateVisuals()
 void Scrollbar_UpdateVisuals_m1363 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::UpdateDrag(UnityEngine.EventSystems.PointerEventData)
 void Scrollbar_UpdateDrag_m1364 (Scrollbar_t343 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Scrollbar::MayDrag(UnityEngine.EventSystems.PointerEventData)
 bool Scrollbar_MayDrag_m1365 (Scrollbar_t343 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
 void Scrollbar_OnBeginDrag_m1366 (Scrollbar_t343 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnDrag(UnityEngine.EventSystems.PointerEventData)
 void Scrollbar_OnDrag_m1367 (Scrollbar_t343 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
 void Scrollbar_OnPointerDown_m1368 (Scrollbar_t343 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.UI.Scrollbar::ClickRepeat(UnityEngine.EventSystems.PointerEventData)
 Object_t * Scrollbar_ClickRepeat_m1369 (Scrollbar_t343 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
 void Scrollbar_OnPointerUp_m1370 (Scrollbar_t343 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnMove(UnityEngine.EventSystems.AxisEventData)
 void Scrollbar_OnMove_m1371 (Scrollbar_t343 * __this, AxisEventData_t196 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Scrollbar::FindSelectableOnLeft()
 Selectable_t272 * Scrollbar_FindSelectableOnLeft_m1372 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Scrollbar::FindSelectableOnRight()
 Selectable_t272 * Scrollbar_FindSelectableOnRight_m1373 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Scrollbar::FindSelectableOnUp()
 Selectable_t272 * Scrollbar_FindSelectableOnUp_m1374 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Scrollbar::FindSelectableOnDown()
 Selectable_t272 * Scrollbar_FindSelectableOnDown_m1375 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
 void Scrollbar_OnInitializePotentialDrag_m1376 (Scrollbar_t343 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Scrollbar::SetDirection(UnityEngine.UI.Scrollbar/Direction,System.Boolean)
 void Scrollbar_SetDirection_m1377 (Scrollbar_t343 * __this, int32_t ___direction, bool ___includeRectLayouts, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Scrollbar::UnityEngine.UI.ICanvasElement.IsDestroyed()
 bool Scrollbar_UnityEngine_UI_ICanvasElement_IsDestroyed_m1378 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.UI.Scrollbar::UnityEngine.UI.ICanvasElement.get_transform()
 Transform_t10 * Scrollbar_UnityEngine_UI_ICanvasElement_get_transform_m1379 (Scrollbar_t343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
