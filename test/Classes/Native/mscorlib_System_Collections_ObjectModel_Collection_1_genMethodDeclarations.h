﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.Object>
struct Collection_1_t2852;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t455;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t2851;

// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::.ctor()
 void Collection_1__ctor_m14676_gshared (Collection_1_t2852 * __this, MethodInfo* method);
#define Collection_1__ctor_m14676(__this, method) (void)Collection_1__ctor_m14676_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
 bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14677_gshared (Collection_1_t2852 * __this, MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14677(__this, method) (bool)Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14677_gshared((Collection_1_t2852 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void Collection_1_System_Collections_ICollection_CopyTo_m14678_gshared (Collection_1_t2852 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m14678(__this, ___array, ___index, method) (void)Collection_1_System_Collections_ICollection_CopyTo_m14678_gshared((Collection_1_t2852 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m14679_gshared (Collection_1_t2852 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m14679(__this, method) (Object_t *)Collection_1_System_Collections_IEnumerable_GetEnumerator_m14679_gshared((Collection_1_t2852 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Add(System.Object)
 int32_t Collection_1_System_Collections_IList_Add_m14680_gshared (Collection_1_t2852 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m14680(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_Add_m14680_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Contains(System.Object)
 bool Collection_1_System_Collections_IList_Contains_m14681_gshared (Collection_1_t2852 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m14681(__this, ___value, method) (bool)Collection_1_System_Collections_IList_Contains_m14681_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
 int32_t Collection_1_System_Collections_IList_IndexOf_m14682_gshared (Collection_1_t2852 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m14682(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_IndexOf_m14682_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
 void Collection_1_System_Collections_IList_Insert_m14683_gshared (Collection_1_t2852 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m14683(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_Insert_m14683_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Remove(System.Object)
 void Collection_1_System_Collections_IList_Remove_m14684_gshared (Collection_1_t2852 * __this, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m14684(__this, ___value, method) (void)Collection_1_System_Collections_IList_Remove_m14684_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
 bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m14685_gshared (Collection_1_t2852 * __this, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m14685(__this, method) (bool)Collection_1_System_Collections_ICollection_get_IsSynchronized_m14685_gshared((Collection_1_t2852 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
 Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m14686_gshared (Collection_1_t2852 * __this, MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m14686(__this, method) (Object_t *)Collection_1_System_Collections_ICollection_get_SyncRoot_m14686_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_IsFixedSize()
 bool Collection_1_System_Collections_IList_get_IsFixedSize_m14687_gshared (Collection_1_t2852 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m14687(__this, method) (bool)Collection_1_System_Collections_IList_get_IsFixedSize_m14687_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_IsReadOnly()
 bool Collection_1_System_Collections_IList_get_IsReadOnly_m14688_gshared (Collection_1_t2852 * __this, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m14688(__this, method) (bool)Collection_1_System_Collections_IList_get_IsReadOnly_m14688_gshared((Collection_1_t2852 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
 Object_t * Collection_1_System_Collections_IList_get_Item_m14689_gshared (Collection_1_t2852 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m14689(__this, ___index, method) (Object_t *)Collection_1_System_Collections_IList_get_Item_m14689_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
 void Collection_1_System_Collections_IList_set_Item_m14690_gshared (Collection_1_t2852 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m14690(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_set_Item_m14690_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(T)
 void Collection_1_Add_m14691_gshared (Collection_1_t2852 * __this, Object_t * ___item, MethodInfo* method);
#define Collection_1_Add_m14691(__this, ___item, method) (void)Collection_1_Add_m14691_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Clear()
 void Collection_1_Clear_m14692_gshared (Collection_1_t2852 * __this, MethodInfo* method);
#define Collection_1_Clear_m14692(__this, method) (void)Collection_1_Clear_m14692_gshared((Collection_1_t2852 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::ClearItems()
 void Collection_1_ClearItems_m14693_gshared (Collection_1_t2852 * __this, MethodInfo* method);
#define Collection_1_ClearItems_m14693(__this, method) (void)Collection_1_ClearItems_m14693_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Contains(T)
 bool Collection_1_Contains_m14694_gshared (Collection_1_t2852 * __this, Object_t * ___item, MethodInfo* method);
#define Collection_1_Contains_m14694(__this, ___item, method) (bool)Collection_1_Contains_m14694_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CopyTo(T[],System.Int32)
 void Collection_1_CopyTo_m14695_gshared (Collection_1_t2852 * __this, ObjectU5BU5D_t115* ___array, int32_t ___index, MethodInfo* method);
#define Collection_1_CopyTo_m14695(__this, ___array, ___index, method) (void)Collection_1_CopyTo_m14695_gshared((Collection_1_t2852 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Object>::GetEnumerator()
 Object_t* Collection_1_GetEnumerator_m14696_gshared (Collection_1_t2852 * __this, MethodInfo* method);
#define Collection_1_GetEnumerator_m14696(__this, method) (Object_t*)Collection_1_GetEnumerator_m14696_gshared((Collection_1_t2852 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::IndexOf(T)
 int32_t Collection_1_IndexOf_m14697_gshared (Collection_1_t2852 * __this, Object_t * ___item, MethodInfo* method);
#define Collection_1_IndexOf_m14697(__this, ___item, method) (int32_t)Collection_1_IndexOf_m14697_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Insert(System.Int32,T)
 void Collection_1_Insert_m14698_gshared (Collection_1_t2852 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define Collection_1_Insert_m14698(__this, ___index, ___item, method) (void)Collection_1_Insert_m14698_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T)
 void Collection_1_InsertItem_m14699_gshared (Collection_1_t2852 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define Collection_1_InsertItem_m14699(__this, ___index, ___item, method) (void)Collection_1_InsertItem_m14699_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(T)
 bool Collection_1_Remove_m14700_gshared (Collection_1_t2852 * __this, Object_t * ___item, MethodInfo* method);
#define Collection_1_Remove_m14700(__this, ___item, method) (bool)Collection_1_Remove_m14700_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveAt(System.Int32)
 void Collection_1_RemoveAt_m14701_gshared (Collection_1_t2852 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_RemoveAt_m14701(__this, ___index, method) (void)Collection_1_RemoveAt_m14701_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32)
 void Collection_1_RemoveItem_m14702_gshared (Collection_1_t2852 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_RemoveItem_m14702(__this, ___index, method) (void)Collection_1_RemoveItem_m14702_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count()
 int32_t Collection_1_get_Count_m14703_gshared (Collection_1_t2852 * __this, MethodInfo* method);
#define Collection_1_get_Count_m14703(__this, method) (int32_t)Collection_1_get_Count_m14703_gshared((Collection_1_t2852 *)__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32)
 Object_t * Collection_1_get_Item_m14704_gshared (Collection_1_t2852 * __this, int32_t ___index, MethodInfo* method);
#define Collection_1_get_Item_m14704(__this, ___index, method) (Object_t *)Collection_1_get_Item_m14704_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::set_Item(System.Int32,T)
 void Collection_1_set_Item_m14705_gshared (Collection_1_t2852 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define Collection_1_set_Item_m14705(__this, ___index, ___value, method) (void)Collection_1_set_Item_m14705_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T)
 void Collection_1_SetItem_m14706_gshared (Collection_1_t2852 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define Collection_1_SetItem_m14706(__this, ___index, ___item, method) (void)Collection_1_SetItem_m14706_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsValidItem(System.Object)
 bool Collection_1_IsValidItem_m14707_gshared (Object_t * __this/* static, unused */, Object_t * ___item, MethodInfo* method);
#define Collection_1_IsValidItem_m14707(__this/* static, unused */, ___item, method) (bool)Collection_1_IsValidItem_m14707_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::ConvertItem(System.Object)
 Object_t * Collection_1_ConvertItem_m14708_gshared (Object_t * __this/* static, unused */, Object_t * ___item, MethodInfo* method);
#define Collection_1_ConvertItem_m14708(__this/* static, unused */, ___item, method) (Object_t *)Collection_1_ConvertItem_m14708_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CheckWritable(System.Collections.Generic.IList`1<T>)
 void Collection_1_CheckWritable_m14709_gshared (Object_t * __this/* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_CheckWritable_m14709(__this/* static, unused */, ___list, method) (void)Collection_1_CheckWritable_m14709_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsSynchronized(System.Collections.Generic.IList`1<T>)
 bool Collection_1_IsSynchronized_m14710_gshared (Object_t * __this/* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_IsSynchronized_m14710(__this/* static, unused */, ___list, method) (bool)Collection_1_IsSynchronized_m14710_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsFixedSize(System.Collections.Generic.IList`1<T>)
 bool Collection_1_IsFixedSize_m14711_gshared (Object_t * __this/* static, unused */, Object_t* ___list, MethodInfo* method);
#define Collection_1_IsFixedSize_m14711(__this/* static, unused */, ___list, method) (bool)Collection_1_IsFixedSize_m14711_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
