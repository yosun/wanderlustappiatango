﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Channels.CrossAppDomainData
struct CrossAppDomainData_t1995;

// System.Void System.Runtime.Remoting.Channels.CrossAppDomainData::.ctor(System.Int32)
 void CrossAppDomainData__ctor_m11457 (CrossAppDomainData_t1995 * __this, int32_t ___domainId, MethodInfo* method) IL2CPP_METHOD_ATTR;
