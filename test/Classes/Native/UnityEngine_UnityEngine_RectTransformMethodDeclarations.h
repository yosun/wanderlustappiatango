﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RectTransform
struct RectTransform_t287;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t514;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t175;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.RectTransform/Edge
#include "UnityEngine_UnityEngine_RectTransform_Edge.h"
// UnityEngine.RectTransform/Axis
#include "UnityEngine_UnityEngine_RectTransform_Axis.h"

// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
 void RectTransform_add_reapplyDrivenProperties_m2531 (Object_t * __this/* static, unused */, ReapplyDrivenProperties_t514 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
 void RectTransform_remove_reapplyDrivenProperties_m5892 (Object_t * __this/* static, unused */, ReapplyDrivenProperties_t514 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
 void RectTransform_INTERNAL_get_rect_m5893 (RectTransform_t287 * __this, Rect_t118 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
 Rect_t118  RectTransform_get_rect_m2124 (RectTransform_t287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_get_anchorMin_m5894 (RectTransform_t287 * __this, Vector2_t9 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_set_anchorMin_m5895 (RectTransform_t287 * __this, Vector2_t9 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
 Vector2_t9  RectTransform_get_anchorMin_m2206 (RectTransform_t287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
 void RectTransform_set_anchorMin_m2333 (RectTransform_t287 * __this, Vector2_t9  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_get_anchorMax_m5896 (RectTransform_t287 * __this, Vector2_t9 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_set_anchorMax_m5897 (RectTransform_t287 * __this, Vector2_t9 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
 Vector2_t9  RectTransform_get_anchorMax_m2328 (RectTransform_t287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
 void RectTransform_set_anchorMax_m2207 (RectTransform_t287 * __this, Vector2_t9  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_get_anchoredPosition_m5898 (RectTransform_t287 * __this, Vector2_t9 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_set_anchoredPosition_m5899 (RectTransform_t287 * __this, Vector2_t9 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
 Vector2_t9  RectTransform_get_anchoredPosition_m2329 (RectTransform_t287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
 void RectTransform_set_anchoredPosition_m2334 (RectTransform_t287 * __this, Vector2_t9  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_get_sizeDelta_m5900 (RectTransform_t287 * __this, Vector2_t9 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_set_sizeDelta_m5901 (RectTransform_t287 * __this, Vector2_t9 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
 Vector2_t9  RectTransform_get_sizeDelta_m2330 (RectTransform_t287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
 void RectTransform_set_sizeDelta_m2208 (RectTransform_t287 * __this, Vector2_t9  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_get_pivot_m5902 (RectTransform_t287 * __this, Vector2_t9 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
 void RectTransform_INTERNAL_set_pivot_m5903 (RectTransform_t287 * __this, Vector2_t9 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
 Vector2_t9  RectTransform_get_pivot_m2202 (RectTransform_t287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
 void RectTransform_set_pivot_m2335 (RectTransform_t287 * __this, Vector2_t9  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
 void RectTransform_SendReapplyDrivenProperties_m5904 (Object_t * __this/* static, unused */, RectTransform_t287 * ___driven, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
 void RectTransform_GetLocalCorners_m5905 (RectTransform_t287 * __this, Vector3U5BU5D_t175* ___fourCornersArray, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
 void RectTransform_GetWorldCorners_m2393 (RectTransform_t287 * __this, Vector3U5BU5D_t175* ___fourCornersArray, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
 void RectTransform_SetInsetAndSizeFromParentEdge_m2527 (RectTransform_t287 * __this, int32_t ___edge, float ___inset, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
 void RectTransform_SetSizeWithCurrentAnchors_m2479 (RectTransform_t287 * __this, int32_t ___axis, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
 Vector2_t9  RectTransform_GetParentSize_m5906 (RectTransform_t287 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
