﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>
struct Collection_1_t4056;
// System.Object
struct Object_t;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t44;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// Vuforia.TrackableBehaviour[]
struct TrackableBehaviourU5BU5D_t813;
// System.Collections.Generic.IEnumerator`1<Vuforia.TrackableBehaviour>
struct IEnumerator_1_t892;
// System.Collections.Generic.IList`1<Vuforia.TrackableBehaviour>
struct IList_1_t4055;

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::.ctor()
// System.Collections.ObjectModel.Collection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_Collection_1_genMethodDeclarations.h"
#define Collection_1__ctor_m23966(__this, method) (void)Collection_1__ctor_m14676_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23967(__this, method) (bool)Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14677_gshared((Collection_1_t2852 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m23968(__this, ___array, ___index, method) (void)Collection_1_System_Collections_ICollection_CopyTo_m14678_gshared((Collection_1_t2852 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m23969(__this, method) (Object_t *)Collection_1_System_Collections_IEnumerable_GetEnumerator_m14679_gshared((Collection_1_t2852 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m23970(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_Add_m14680_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m23971(__this, ___value, method) (bool)Collection_1_System_Collections_IList_Contains_m14681_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m23972(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_IndexOf_m14682_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m23973(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_Insert_m14683_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m23974(__this, ___value, method) (void)Collection_1_System_Collections_IList_Remove_m14684_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m23975(__this, method) (bool)Collection_1_System_Collections_ICollection_get_IsSynchronized_m14685_gshared((Collection_1_t2852 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m23976(__this, method) (Object_t *)Collection_1_System_Collections_ICollection_get_SyncRoot_m14686_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m23977(__this, method) (bool)Collection_1_System_Collections_IList_get_IsFixedSize_m14687_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m23978(__this, method) (bool)Collection_1_System_Collections_IList_get_IsReadOnly_m14688_gshared((Collection_1_t2852 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m23979(__this, ___index, method) (Object_t *)Collection_1_System_Collections_IList_get_Item_m14689_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m23980(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_set_Item_m14690_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::Add(T)
#define Collection_1_Add_m23981(__this, ___item, method) (void)Collection_1_Add_m14691_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::Clear()
#define Collection_1_Clear_m23982(__this, method) (void)Collection_1_Clear_m14692_gshared((Collection_1_t2852 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::ClearItems()
#define Collection_1_ClearItems_m23983(__this, method) (void)Collection_1_ClearItems_m14693_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::Contains(T)
#define Collection_1_Contains_m23984(__this, ___item, method) (bool)Collection_1_Contains_m14694_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m23985(__this, ___array, ___index, method) (void)Collection_1_CopyTo_m14695_gshared((Collection_1_t2852 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::GetEnumerator()
#define Collection_1_GetEnumerator_m23986(__this, method) (Object_t*)Collection_1_GetEnumerator_m14696_gshared((Collection_1_t2852 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::IndexOf(T)
#define Collection_1_IndexOf_m23987(__this, ___item, method) (int32_t)Collection_1_IndexOf_m14697_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::Insert(System.Int32,T)
#define Collection_1_Insert_m23988(__this, ___index, ___item, method) (void)Collection_1_Insert_m14698_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m23989(__this, ___index, ___item, method) (void)Collection_1_InsertItem_m14699_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::Remove(T)
#define Collection_1_Remove_m23990(__this, ___item, method) (bool)Collection_1_Remove_m14700_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m23991(__this, ___index, method) (void)Collection_1_RemoveAt_m14701_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m23992(__this, ___index, method) (void)Collection_1_RemoveItem_m14702_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::get_Count()
#define Collection_1_get_Count_m23993(__this, method) (int32_t)Collection_1_get_Count_m14703_gshared((Collection_1_t2852 *)__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::get_Item(System.Int32)
#define Collection_1_get_Item_m23994(__this, ___index, method) (TrackableBehaviour_t44 *)Collection_1_get_Item_m14704_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m23995(__this, ___index, ___value, method) (void)Collection_1_set_Item_m14705_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m23996(__this, ___index, ___item, method) (void)Collection_1_SetItem_m14706_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m23997(__this/* static, unused */, ___item, method) (bool)Collection_1_IsValidItem_m14707_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m23998(__this/* static, unused */, ___item, method) (TrackableBehaviour_t44 *)Collection_1_ConvertItem_m14708_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m23999(__this/* static, unused */, ___list, method) (void)Collection_1_CheckWritable_m14709_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m24000(__this/* static, unused */, ___list, method) (bool)Collection_1_IsSynchronized_m14710_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TrackableBehaviour>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m24001(__this/* static, unused */, ___list, method) (bool)Collection_1_IsFixedSize_m14711_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
