﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_29.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>
struct CachedInvokableCall_1_t2918  : public InvokableCall_1_t2919
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
