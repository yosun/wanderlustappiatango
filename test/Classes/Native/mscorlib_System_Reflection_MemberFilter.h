﻿#pragma once
#include <stdint.h>
// System.Reflection.MemberInfo
struct MemberInfo_t145;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Reflection.MemberFilter
struct MemberFilter_t1700  : public MulticastDelegate_t325
{
};
