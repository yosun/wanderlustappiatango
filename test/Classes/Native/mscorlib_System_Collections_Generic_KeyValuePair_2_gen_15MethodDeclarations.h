﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct KeyValuePair_2_t4019;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t697;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m23639(__this, ___key, ___value, method) (void)KeyValuePair_2__ctor_m17641_gshared((KeyValuePair_2_t3281 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Key()
#define KeyValuePair_2_get_Key_m23640(__this, method) (String_t*)KeyValuePair_2_get_Key_m17642_gshared((KeyValuePair_2_t3281 *)__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m23641(__this, ___value, method) (void)KeyValuePair_2_set_Key_m17643_gshared((KeyValuePair_2_t3281 *)__this, (Object_t *)___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Value()
#define KeyValuePair_2_get_Value_m23642(__this, method) (List_1_t697 *)KeyValuePair_2_get_Value_m17644_gshared((KeyValuePair_2_t3281 *)__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m23643(__this, ___value, method) (void)KeyValuePair_2_set_Value_m17645_gshared((KeyValuePair_2_t3281 *)__this, (Object_t *)___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::ToString()
#define KeyValuePair_2_ToString_m23644(__this, method) (String_t*)KeyValuePair_2_ToString_m17646_gshared((KeyValuePair_2_t3281 *)__this, method)
