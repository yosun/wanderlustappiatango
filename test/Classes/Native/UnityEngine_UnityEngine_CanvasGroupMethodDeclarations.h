﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CanvasGroup
struct CanvasGroup_t468;
// UnityEngine.Camera
struct Camera_t3;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Boolean UnityEngine.CanvasGroup::get_interactable()
 bool CanvasGroup_get_interactable_m2411 (CanvasGroup_t468 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
 bool CanvasGroup_get_blocksRaycasts_m6221 (CanvasGroup_t468 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
 bool CanvasGroup_get_ignoreParentGroups_m2138 (CanvasGroup_t468 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CanvasGroup::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
 bool CanvasGroup_IsRaycastLocationValid_m6222 (CanvasGroup_t468 * __this, Vector2_t9  ___sp, Camera_t3 * ___eventCamera, MethodInfo* method) IL2CPP_METHOD_ATTR;
