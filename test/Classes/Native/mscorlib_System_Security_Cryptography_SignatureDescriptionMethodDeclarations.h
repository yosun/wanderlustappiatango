﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.SignatureDescription
struct SignatureDescription_t2122;
// System.String
struct String_t;

// System.Void System.Security.Cryptography.SignatureDescription::.ctor()
 void SignatureDescription__ctor_m12115 (SignatureDescription_t2122 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_DeformatterAlgorithm(System.String)
 void SignatureDescription_set_DeformatterAlgorithm_m12116 (SignatureDescription_t2122 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_DigestAlgorithm(System.String)
 void SignatureDescription_set_DigestAlgorithm_m12117 (SignatureDescription_t2122 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_FormatterAlgorithm(System.String)
 void SignatureDescription_set_FormatterAlgorithm_m12118 (SignatureDescription_t2122 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.SignatureDescription::set_KeyAlgorithm(System.String)
 void SignatureDescription_set_KeyAlgorithm_m12119 (SignatureDescription_t2122 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
