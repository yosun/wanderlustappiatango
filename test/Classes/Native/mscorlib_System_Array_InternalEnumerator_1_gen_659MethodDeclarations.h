﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>
struct InternalEnumerator_1_t5044;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.TypeAttributes
#include "mscorlib_System_Reflection_TypeAttributes.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30800 (InternalEnumerator_1_t5044 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30801 (InternalEnumerator_1_t5044 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::Dispose()
 void InternalEnumerator_1_Dispose_m30802 (InternalEnumerator_1_t5044 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30803 (InternalEnumerator_1_t5044 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Reflection.TypeAttributes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30804 (InternalEnumerator_1_t5044 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
