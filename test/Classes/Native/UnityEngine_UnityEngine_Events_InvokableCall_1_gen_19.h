﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>
struct UnityAction_1_t2828;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>
struct InvokableCall_1_t2827  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::Delegate
	UnityAction_1_t2828 * ___Delegate_0;
};
