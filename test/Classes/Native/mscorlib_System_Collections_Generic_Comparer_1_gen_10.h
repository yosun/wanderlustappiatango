﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.UI.Text>
struct Comparer_1_t3307;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.UI.Text>
struct Comparer_1_t3307  : public Object_t
{
};
struct Comparer_1_t3307_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UI.Text>::_default
	Comparer_1_t3307 * ____default_0;
};
