﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_138.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Component>
struct CachedInvokableCall_1_t4617  : public InvokableCall_1_t4618
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Component>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
