﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Globalization.GregorianCalendar
struct GregorianCalendar_t1857;
// System.Int32[]
struct Int32U5BU5D_t21;
// System.Globalization.GregorianCalendarTypes
#include "mscorlib_System_Globalization_GregorianCalendarTypes.h"
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.DayOfWeek
#include "mscorlib_System_DayOfWeek.h"

// System.Void System.Globalization.GregorianCalendar::.ctor(System.Globalization.GregorianCalendarTypes)
 void GregorianCalendar__ctor_m10587 (GregorianCalendar_t1857 * __this, int32_t ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::.ctor()
 void GregorianCalendar__ctor_m10588 (GregorianCalendar_t1857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System.Globalization.GregorianCalendar::get_Eras()
 Int32U5BU5D_t21* GregorianCalendar_get_Eras_m10589 (GregorianCalendar_t1857 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.GregorianCalendar::set_CalendarType(System.Globalization.GregorianCalendarTypes)
 void GregorianCalendar_set_CalendarType_m10590 (GregorianCalendar_t1857 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetDayOfMonth(System.DateTime)
 int32_t GregorianCalendar_GetDayOfMonth_m10591 (GregorianCalendar_t1857 * __this, DateTime_t110  ___time, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek System.Globalization.GregorianCalendar::GetDayOfWeek(System.DateTime)
 int32_t GregorianCalendar_GetDayOfWeek_m10592 (GregorianCalendar_t1857 * __this, DateTime_t110  ___time, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetEra(System.DateTime)
 int32_t GregorianCalendar_GetEra_m10593 (GregorianCalendar_t1857 * __this, DateTime_t110  ___time, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetMonth(System.DateTime)
 int32_t GregorianCalendar_GetMonth_m10594 (GregorianCalendar_t1857 * __this, DateTime_t110  ___time, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.GregorianCalendar::GetYear(System.DateTime)
 int32_t GregorianCalendar_GetYear_m10595 (GregorianCalendar_t1857 * __this, DateTime_t110  ___time, MethodInfo* method) IL2CPP_METHOD_ATTR;
