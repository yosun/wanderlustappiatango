﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>
struct InternalEnumerator_1_t2709;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentMode.h"

// System.Void System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m14027 (InternalEnumerator_1_t2709 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14028 (InternalEnumerator_1_t2709 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::Dispose()
 void InternalEnumerator_1_Dispose_m14029 (InternalEnumerator_1_t2709 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m14030 (InternalEnumerator_1_t2709 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m14031 (InternalEnumerator_1_t2709 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
