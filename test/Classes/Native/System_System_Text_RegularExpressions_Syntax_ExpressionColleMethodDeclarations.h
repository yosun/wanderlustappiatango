﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.Syntax.ExpressionCollection
struct ExpressionCollection_t1445;
// System.Text.RegularExpressions.Syntax.Expression
struct Expression_t1443;
// System.Object
struct Object_t;

// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::.ctor()
 void ExpressionCollection__ctor_m7412 (ExpressionCollection_t1445 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::Add(System.Text.RegularExpressions.Syntax.Expression)
 void ExpressionCollection_Add_m7413 (ExpressionCollection_t1445 * __this, Expression_t1443 * ___e, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.ExpressionCollection::get_Item(System.Int32)
 Expression_t1443 * ExpressionCollection_get_Item_m7414 (ExpressionCollection_t1445 * __this, int32_t ___i, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::set_Item(System.Int32,System.Text.RegularExpressions.Syntax.Expression)
 void ExpressionCollection_set_Item_m7415 (ExpressionCollection_t1445 * __this, int32_t ___i, Expression_t1443 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.ExpressionCollection::OnValidate(System.Object)
 void ExpressionCollection_OnValidate_m7416 (ExpressionCollection_t1445 * __this, Object_t * ___o, MethodInfo* method) IL2CPP_METHOD_ATTR;
