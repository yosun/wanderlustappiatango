﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Terrain
struct Terrain_t1025;

// System.Void UnityEngine.Terrain::.ctor()
 void Terrain__ctor_m6173 (Terrain_t1025 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
