﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.Guid>
struct GenericComparer_1_t2672;
// System.Guid
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.Guid>::.ctor()
 void GenericComparer_1__ctor_m13953 (GenericComparer_1_t2672 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Guid>::Compare(T,T)
 int32_t GenericComparer_1_Compare_m31361 (GenericComparer_1_t2672 * __this, Guid_t107  ___x, Guid_t107  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
