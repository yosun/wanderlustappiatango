﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t551;
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t135;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>
struct Enumerator_t781 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::l
	List_1_t551 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.ITrackableEventHandler>::current
	Object_t * ___current_3;
};
