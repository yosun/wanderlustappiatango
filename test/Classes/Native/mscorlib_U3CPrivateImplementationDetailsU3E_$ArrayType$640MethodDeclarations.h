﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$640
struct $ArrayType$640_t2278;
struct $ArrayType$640_t2278_marshaled;

void $ArrayType$640_t2278_marshal(const $ArrayType$640_t2278& unmarshaled, $ArrayType$640_t2278_marshaled& marshaled);
void $ArrayType$640_t2278_marshal_back(const $ArrayType$640_t2278_marshaled& marshaled, $ArrayType$640_t2278& unmarshaled);
void $ArrayType$640_t2278_marshal_cleanup($ArrayType$640_t2278_marshaled& marshaled);
