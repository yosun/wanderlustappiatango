﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.LinkStack
struct LinkStack_t1424;
// System.Object
struct Object_t;

// System.Void System.Text.RegularExpressions.LinkStack::.ctor()
 void LinkStack__ctor_m7301 (LinkStack_t1424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.LinkStack::Push()
 void LinkStack_Push_m7302 (LinkStack_t1424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.LinkStack::Pop()
 bool LinkStack_Pop_m7303 (LinkStack_t1424 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.RegularExpressions.LinkStack::GetCurrent()
// System.Void System.Text.RegularExpressions.LinkStack::SetCurrent(System.Object)
