﻿#pragma once
#include <stdint.h>
// Vuforia.Prop[]
struct PropU5BU5D_t4108;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.Prop>
struct List_1_t721  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.Prop>::_items
	PropU5BU5D_t4108* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::_version
	int32_t ____version_3;
};
struct List_1_t721_StaticFields{
	// System.Int32 System.Collections.Generic.List`1<Vuforia.Prop>::DefaultCapacity
	int32_t ___DefaultCapacity_0;
	// T[] System.Collections.Generic.List`1<Vuforia.Prop>::EmptyArray
	PropU5BU5D_t4108* ___EmptyArray_4;
};
