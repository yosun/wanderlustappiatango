﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo PIXEL_FORMAT_t608_il2cpp_TypeInfo;
// System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparison_1_t3792  : public MulticastDelegate_t325
{
};
