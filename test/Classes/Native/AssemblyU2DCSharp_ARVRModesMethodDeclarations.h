﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// ARVRModes
struct ARVRModes_t5;

// System.Void ARVRModes::.ctor()
 void ARVRModes__ctor_m0 (ARVRModes_t5 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRModes::SwitchARToVR()
 void ARVRModes_SwitchARToVR_m1 (ARVRModes_t5 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRModes::SwitchVRToAR()
 void ARVRModes_SwitchVRToAR_m2 (ARVRModes_t5 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRModes::SwitchModes()
 void ARVRModes_SwitchModes_m3 (ARVRModes_t5 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ARVRModes::Update()
 void ARVRModes_Update_m4 (ARVRModes_t5 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
