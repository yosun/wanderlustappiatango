﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARManagerImpl/WordResultData
struct WordResultData_t643;
struct WordResultData_t643_marshaled;

void WordResultData_t643_marshal(const WordResultData_t643& unmarshaled, WordResultData_t643_marshaled& marshaled);
void WordResultData_t643_marshal_back(const WordResultData_t643_marshaled& marshaled, WordResultData_t643& unmarshaled);
void WordResultData_t643_marshal_cleanup(WordResultData_t643_marshaled& marshaled);
