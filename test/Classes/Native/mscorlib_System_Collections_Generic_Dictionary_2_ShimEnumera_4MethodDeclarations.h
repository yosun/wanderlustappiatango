﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct ShimEnumerator_t3319;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct Dictionary_2_t282;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_3MethodDeclarations.h"
#define ShimEnumerator__ctor_m17971(__this, ___host, method) (void)ShimEnumerator__ctor_m17714_gshared((ShimEnumerator_t3289 *)__this, (Dictionary_2_t3275 *)___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::MoveNext()
#define ShimEnumerator_MoveNext_m17972(__this, method) (bool)ShimEnumerator_MoveNext_m17715_gshared((ShimEnumerator_t3289 *)__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Entry()
#define ShimEnumerator_get_Entry_m17973(__this, method) (DictionaryEntry_t1302 )ShimEnumerator_get_Entry_m17716_gshared((ShimEnumerator_t3289 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Key()
#define ShimEnumerator_get_Key_m17974(__this, method) (Object_t *)ShimEnumerator_get_Key_m17717_gshared((ShimEnumerator_t3289 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Value()
#define ShimEnumerator_get_Value_m17975(__this, method) (Object_t *)ShimEnumerator_get_Value_m17718_gshared((ShimEnumerator_t3289 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Current()
#define ShimEnumerator_get_Current_m17976(__this, method) (Object_t *)ShimEnumerator_get_Current_m17719_gshared((ShimEnumerator_t3289 *)__this, method)
