﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Color>
struct InternalEnumerator_1_t3828;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m21636 (InternalEnumerator_1_t3828 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21637 (InternalEnumerator_1_t3828 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::Dispose()
 void InternalEnumerator_1_Dispose_m21638 (InternalEnumerator_1_t3828 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m21639 (InternalEnumerator_1_t3828 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.Color>::get_Current()
 Color_t19  InternalEnumerator_1_get_Current_m21640 (InternalEnumerator_1_t3828 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
