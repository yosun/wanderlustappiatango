﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$64
struct $ArrayType$64_t2269;
struct $ArrayType$64_t2269_marshaled;

void $ArrayType$64_t2269_marshal(const $ArrayType$64_t2269& unmarshaled, $ArrayType$64_t2269_marshaled& marshaled);
void $ArrayType$64_t2269_marshal_back(const $ArrayType$64_t2269_marshaled& marshaled, $ArrayType$64_t2269& unmarshaled);
void $ArrayType$64_t2269_marshal_cleanup($ArrayType$64_t2269_marshaled& marshaled);
