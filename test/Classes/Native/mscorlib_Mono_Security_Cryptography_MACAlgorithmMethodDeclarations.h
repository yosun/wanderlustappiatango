﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Cryptography.MACAlgorithm
struct MACAlgorithm_t1765;
// System.Security.Cryptography.SymmetricAlgorithm
struct SymmetricAlgorithm_t1566;
// System.Byte[]
struct ByteU5BU5D_t609;

// System.Void Mono.Security.Cryptography.MACAlgorithm::.ctor(System.Security.Cryptography.SymmetricAlgorithm)
 void MACAlgorithm__ctor_m10016 (MACAlgorithm_t1765 * __this, SymmetricAlgorithm_t1566 * ___algorithm, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.MACAlgorithm::Initialize(System.Byte[])
 void MACAlgorithm_Initialize_m10017 (MACAlgorithm_t1765 * __this, ByteU5BU5D_t609* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Cryptography.MACAlgorithm::Core(System.Byte[],System.Int32,System.Int32)
 void MACAlgorithm_Core_m10018 (MACAlgorithm_t1765 * __this, ByteU5BU5D_t609* ___rgb, int32_t ___ib, int32_t ___cb, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Cryptography.MACAlgorithm::Final()
 ByteU5BU5D_t609* MACAlgorithm_Final_m10019 (MACAlgorithm_t1765 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
