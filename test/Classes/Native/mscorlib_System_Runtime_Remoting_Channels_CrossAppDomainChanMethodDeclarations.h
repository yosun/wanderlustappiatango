﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Channels.CrossAppDomainChannel
struct CrossAppDomainChannel_t1996;
// System.String
struct String_t;
// System.Object
struct Object_t;

// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::.ctor()
 void CrossAppDomainChannel__ctor_m11458 (CrossAppDomainChannel_t1996 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::.cctor()
 void CrossAppDomainChannel__cctor_m11459 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::RegisterCrossAppDomainChannel()
 void CrossAppDomainChannel_RegisterCrossAppDomainChannel_m11460 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelName()
 String_t* CrossAppDomainChannel_get_ChannelName_m11461 (CrossAppDomainChannel_t1996 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelPriority()
 int32_t CrossAppDomainChannel_get_ChannelPriority_m11462 (CrossAppDomainChannel_t1996 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Remoting.Channels.CrossAppDomainChannel::get_ChannelData()
 Object_t * CrossAppDomainChannel_get_ChannelData_m11463 (CrossAppDomainChannel_t1996 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Channels.CrossAppDomainChannel::StartListening(System.Object)
 void CrossAppDomainChannel_StartListening_m11464 (CrossAppDomainChannel_t1996 * __this, Object_t * ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
