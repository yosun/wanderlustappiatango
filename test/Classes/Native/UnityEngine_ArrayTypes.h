﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityEngine.MonoBehaviour[]
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t5409  : public Array_t
{
};
// UnityEngine.Behaviour[]
// UnityEngine.Behaviour[]
struct BehaviourU5BU5D_t5410  : public Array_t
{
};
// UnityEngine.Component[]
// UnityEngine.Component[]
struct ComponentU5BU5D_t3043  : public Array_t
{
};
// UnityEngine.Object[]
// UnityEngine.Object[]
struct ObjectU5BU5D_t816  : public Array_t
{
};
// UnityEngine.Material[]
// UnityEngine.Material[]
struct MaterialU5BU5D_t114  : public Array_t
{
};
// UnityEngine.Renderer[]
// UnityEngine.Renderer[]
struct RendererU5BU5D_t116  : public Array_t
{
};
// UnityEngine.GameObject[]
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t27  : public Array_t
{
};
// UnityEngine.Collider[]
// UnityEngine.Collider[]
struct ColliderU5BU5D_t133  : public Array_t
{
};
// UnityEngine.Camera[]
// UnityEngine.Camera[]
struct CameraU5BU5D_t172  : public Array_t
{
};
struct CameraU5BU5D_t172_StaticFields{
};
// UnityEngine.Vector3[]
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t175  : public Array_t
{
};
// UnityEngine.Transform[]
// UnityEngine.Transform[]
struct TransformU5BU5D_t3124  : public Array_t
{
};
// UnityEngine.RaycastHit2D[]
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t446  : public Array_t
{
};
// UnityEngine.RaycastHit[]
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t449  : public Array_t
{
};
// UnityEngine.Font[]
// UnityEngine.Font[]
struct FontU5BU5D_t3266  : public Array_t
{
};
struct FontU5BU5D_t3266_StaticFields{
};
// UnityEngine.UIVertex[]
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t330  : public Array_t
{
};
struct UIVertexU5BU5D_t330_StaticFields{
};
// UnityEngine.Canvas[]
// UnityEngine.Canvas[]
struct CanvasU5BU5D_t3347  : public Array_t
{
};
struct CanvasU5BU5D_t3347_StaticFields{
};
// UnityEngine.ICanvasRaycastFilter[]
// UnityEngine.ICanvasRaycastFilter[]
struct ICanvasRaycastFilterU5BU5D_t5411  : public Array_t
{
};
// UnityEngine.ISerializationCallbackReceiver[]
// UnityEngine.ISerializationCallbackReceiver[]
struct ISerializationCallbackReceiverU5BU5D_t5412  : public Array_t
{
};
// UnityEngine.Vector2[]
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t311  : public Array_t
{
};
// UnityEngine.UILineInfo[]
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1031  : public Array_t
{
};
// UnityEngine.UICharInfo[]
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t1030  : public Array_t
{
};
// UnityEngine.CanvasGroup[]
// UnityEngine.CanvasGroup[]
struct CanvasGroupU5BU5D_t3480  : public Array_t
{
};
// UnityEngine.RectTransform[]
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t3573  : public Array_t
{
};
struct RectTransformU5BU5D_t3573_StaticFields{
};
// UnityEngine.Color32[]
// UnityEngine.Color32[]
struct Color32U5BU5D_t610  : public Array_t
{
};
// UnityEngine.Color[]
// UnityEngine.Color[]
struct ColorU5BU5D_t797  : public Array_t
{
};
// UnityEngine.MeshFilter[]
// UnityEngine.MeshFilter[]
struct MeshFilterU5BU5D_t856  : public Array_t
{
};
// UnityEngine.MeshRenderer[]
// UnityEngine.MeshRenderer[]
struct MeshRendererU5BU5D_t4386  : public Array_t
{
};
// UnityEngine.WebCamDevice[]
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t886  : public Array_t
{
};
// UnityEngine.AssetBundle[]
// UnityEngine.AssetBundle[]
struct AssetBundleU5BU5D_t5413  : public Array_t
{
};
// UnityEngine.SendMessageOptions[]
// UnityEngine.SendMessageOptions[]
struct SendMessageOptionsU5BU5D_t5414  : public Array_t
{
};
// UnityEngine.PrimitiveType[]
// UnityEngine.PrimitiveType[]
struct PrimitiveTypeU5BU5D_t5415  : public Array_t
{
};
// UnityEngine.Space[]
// UnityEngine.Space[]
struct SpaceU5BU5D_t5416  : public Array_t
{
};
// UnityEngine.RuntimePlatform[]
// UnityEngine.RuntimePlatform[]
struct RuntimePlatformU5BU5D_t5417  : public Array_t
{
};
// UnityEngine.LogType[]
// UnityEngine.LogType[]
struct LogTypeU5BU5D_t5418  : public Array_t
{
};
// UnityEngine.ScriptableObject[]
// UnityEngine.ScriptableObject[]
struct ScriptableObjectU5BU5D_t5419  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IAchievementDescription[]
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct IAchievementDescriptionU5BU5D_t1104  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IAchievement[]
// UnityEngine.SocialPlatforms.IAchievement[]
struct IAchievementU5BU5D_t1107  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IScore[]
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t1054  : public Array_t
{
};
// UnityEngine.SocialPlatforms.IUserProfile[]
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t1052  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t924  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t925  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct GcLeaderboardU5BU5D_t4447  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t930  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Achievement[]
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct AchievementU5BU5D_t1106  : public Array_t
{
};
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t932  : public Array_t
{
};
// UnityEngine.SocialPlatforms.Impl.Score[]
// UnityEngine.SocialPlatforms.Impl.Score[]
struct ScoreU5BU5D_t1108  : public Array_t
{
};
// UnityEngine.Mesh[]
// UnityEngine.Mesh[]
struct MeshU5BU5D_t5420  : public Array_t
{
};
// UnityEngine.Texture[]
// UnityEngine.Texture[]
struct TextureU5BU5D_t5421  : public Array_t
{
};
// UnityEngine.Texture2D[]
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t5422  : public Array_t
{
};
// UnityEngine.RenderTexture[]
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t5423  : public Array_t
{
};
// UnityEngine.ReflectionProbe[]
// UnityEngine.ReflectionProbe[]
struct ReflectionProbeU5BU5D_t5424  : public Array_t
{
};
// UnityEngine.GUIElement[]
// UnityEngine.GUIElement[]
struct GUIElementU5BU5D_t5425  : public Array_t
{
};
// UnityEngine.GUILayer[]
// UnityEngine.GUILayer[]
struct GUILayerU5BU5D_t5426  : public Array_t
{
};
// UnityEngine.GUILayoutOption[]
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t960  : public Array_t
{
};
// UnityEngine.GUILayoutUtility/LayoutCache[]
// UnityEngine.GUILayoutUtility/LayoutCache[]
struct LayoutCacheU5BU5D_t4503  : public Array_t
{
};
// UnityEngine.GUILayoutEntry[]
// UnityEngine.GUILayoutEntry[]
struct GUILayoutEntryU5BU5D_t4522  : public Array_t
{
};
struct GUILayoutEntryU5BU5D_t4522_StaticFields{
};
// UnityEngine.GUILayoutOption/Type[]
// UnityEngine.GUILayoutOption/Type[]
struct TypeU5BU5D_t5427  : public Array_t
{
};
// UnityEngine.GUISkin[]
// UnityEngine.GUISkin[]
struct GUISkinU5BU5D_t5428  : public Array_t
{
};
struct GUISkinU5BU5D_t5428_StaticFields{
};
// UnityEngine.GUIStyle[]
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t969  : public Array_t
{
};
struct GUIStyleU5BU5D_t969_StaticFields{
};
// UnityEngine.FontStyle[]
// UnityEngine.FontStyle[]
struct FontStyleU5BU5D_t5429  : public Array_t
{
};
// UnityEngine.TouchScreenKeyboardType[]
// UnityEngine.TouchScreenKeyboardType[]
struct TouchScreenKeyboardTypeU5BU5D_t5430  : public Array_t
{
};
// UnityEngine.KeyCode[]
// UnityEngine.KeyCode[]
struct KeyCodeU5BU5D_t5431  : public Array_t
{
};
// UnityEngine.EventType[]
// UnityEngine.EventType[]
struct EventTypeU5BU5D_t5432  : public Array_t
{
};
// UnityEngine.EventModifiers[]
// UnityEngine.EventModifiers[]
struct EventModifiersU5BU5D_t5433  : public Array_t
{
};
// UnityEngine.DrivenTransformProperties[]
// UnityEngine.DrivenTransformProperties[]
struct DrivenTransformPropertiesU5BU5D_t5434  : public Array_t
{
};
// UnityEngine.RectTransform/Edge[]
// UnityEngine.RectTransform/Edge[]
struct EdgeU5BU5D_t5435  : public Array_t
{
};
// UnityEngine.RectTransform/Axis[]
// UnityEngine.RectTransform/Axis[]
struct AxisU5BU5D_t5436  : public Array_t
{
};
// UnityEngine.SerializePrivateVariables[]
// UnityEngine.SerializePrivateVariables[]
struct SerializePrivateVariablesU5BU5D_t5437  : public Array_t
{
};
// UnityEngine.SerializeField[]
// UnityEngine.SerializeField[]
struct SerializeFieldU5BU5D_t5438  : public Array_t
{
};
// UnityEngine.Shader[]
// UnityEngine.Shader[]
struct ShaderU5BU5D_t5439  : public Array_t
{
};
// UnityEngine.Sprite[]
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t5440  : public Array_t
{
};
// UnityEngine.SpriteRenderer[]
// UnityEngine.SpriteRenderer[]
struct SpriteRendererU5BU5D_t5441  : public Array_t
{
};
// UnityEngine.Display[]
// UnityEngine.Display[]
struct DisplayU5BU5D_t994  : public Array_t
{
};
struct DisplayU5BU5D_t994_StaticFields{
};
// UnityEngine.TouchPhase[]
// UnityEngine.TouchPhase[]
struct TouchPhaseU5BU5D_t5442  : public Array_t
{
};
// UnityEngine.IMECompositionMode[]
// UnityEngine.IMECompositionMode[]
struct IMECompositionModeU5BU5D_t5443  : public Array_t
{
};
// UnityEngine.HideFlags[]
// UnityEngine.HideFlags[]
struct HideFlagsU5BU5D_t5444  : public Array_t
{
};
// UnityEngine.Rigidbody[]
// UnityEngine.Rigidbody[]
struct RigidbodyU5BU5D_t5445  : public Array_t
{
};
// UnityEngine.BoxCollider[]
// UnityEngine.BoxCollider[]
struct BoxColliderU5BU5D_t5446  : public Array_t
{
};
// UnityEngine.MeshCollider[]
// UnityEngine.MeshCollider[]
struct MeshColliderU5BU5D_t5447  : public Array_t
{
};
// UnityEngine.Rigidbody2D[]
// UnityEngine.Rigidbody2D[]
struct Rigidbody2DU5BU5D_t4640  : public Array_t
{
};
// UnityEngine.Collider2D[]
// UnityEngine.Collider2D[]
struct Collider2DU5BU5D_t5448  : public Array_t
{
};
// UnityEngine.AudioClip[]
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t5449  : public Array_t
{
};
// UnityEngine.WebCamTexture[]
// UnityEngine.WebCamTexture[]
struct WebCamTextureU5BU5D_t5450  : public Array_t
{
};
// UnityEngine.AnimationEventSource[]
// UnityEngine.AnimationEventSource[]
struct AnimationEventSourceU5BU5D_t5451  : public Array_t
{
};
// UnityEngine.Keyframe[]
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1019  : public Array_t
{
};
// UnityEngine.Animator[]
// UnityEngine.Animator[]
struct AnimatorU5BU5D_t5452  : public Array_t
{
};
// UnityEngine.RuntimeAnimatorController[]
// UnityEngine.RuntimeAnimatorController[]
struct RuntimeAnimatorControllerU5BU5D_t5453  : public Array_t
{
};
// UnityEngine.Terrain[]
// UnityEngine.Terrain[]
struct TerrainU5BU5D_t5454  : public Array_t
{
};
// UnityEngine.TextAnchor[]
// UnityEngine.TextAnchor[]
struct TextAnchorU5BU5D_t5455  : public Array_t
{
};
// UnityEngine.HorizontalWrapMode[]
// UnityEngine.HorizontalWrapMode[]
struct HorizontalWrapModeU5BU5D_t5456  : public Array_t
{
};
// UnityEngine.VerticalWrapMode[]
// UnityEngine.VerticalWrapMode[]
struct VerticalWrapModeU5BU5D_t5457  : public Array_t
{
};
// UnityEngine.RenderMode[]
// UnityEngine.RenderMode[]
struct RenderModeU5BU5D_t5458  : public Array_t
{
};
// UnityEngine.CanvasRenderer[]
// UnityEngine.CanvasRenderer[]
struct CanvasRendererU5BU5D_t5459  : public Array_t
{
};
// UnityEngine.WrapperlessIcall[]
// UnityEngine.WrapperlessIcall[]
struct WrapperlessIcallU5BU5D_t5460  : public Array_t
{
};
// UnityEngine.DisallowMultipleComponent[]
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t1034  : public Array_t
{
};
// UnityEngine.ExecuteInEditMode[]
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t1035  : public Array_t
{
};
// UnityEngine.RequireComponent[]
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t1036  : public Array_t
{
};
// UnityEngine.AddComponentMenu[]
// UnityEngine.AddComponentMenu[]
struct AddComponentMenuU5BU5D_t5461  : public Array_t
{
};
// UnityEngine.HideInInspector[]
// UnityEngine.HideInInspector[]
struct HideInInspectorU5BU5D_t5462  : public Array_t
{
};
// UnityEngine.WritableAttribute[]
// UnityEngine.WritableAttribute[]
struct WritableAttributeU5BU5D_t5463  : public Array_t
{
};
// UnityEngine.AssemblyIsEditorAssembly[]
// UnityEngine.AssemblyIsEditorAssembly[]
struct AssemblyIsEditorAssemblyU5BU5D_t5464  : public Array_t
{
};
// UnityEngine.CameraClearFlags[]
// UnityEngine.CameraClearFlags[]
struct CameraClearFlagsU5BU5D_t5465  : public Array_t
{
};
// UnityEngine.ScreenOrientation[]
// UnityEngine.ScreenOrientation[]
struct ScreenOrientationU5BU5D_t5466  : public Array_t
{
};
// UnityEngine.FilterMode[]
// UnityEngine.FilterMode[]
struct FilterModeU5BU5D_t5467  : public Array_t
{
};
// UnityEngine.TextureWrapMode[]
// UnityEngine.TextureWrapMode[]
struct TextureWrapModeU5BU5D_t5468  : public Array_t
{
};
// UnityEngine.TextureFormat[]
// UnityEngine.TextureFormat[]
struct TextureFormatU5BU5D_t5469  : public Array_t
{
};
// UnityEngine.RenderTextureFormat[]
// UnityEngine.RenderTextureFormat[]
struct RenderTextureFormatU5BU5D_t5470  : public Array_t
{
};
// UnityEngine.RenderTextureReadWrite[]
// UnityEngine.RenderTextureReadWrite[]
struct RenderTextureReadWriteU5BU5D_t5471  : public Array_t
{
};
// UnityEngine.SendMouseEvents/HitInfo[]
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t1057  : public Array_t
{
};
// UnityEngine.SocialPlatforms.UserState[]
// UnityEngine.SocialPlatforms.UserState[]
struct UserStateU5BU5D_t5472  : public Array_t
{
};
// UnityEngine.SocialPlatforms.UserScope[]
// UnityEngine.SocialPlatforms.UserScope[]
struct UserScopeU5BU5D_t5473  : public Array_t
{
};
// UnityEngine.SocialPlatforms.TimeScope[]
// UnityEngine.SocialPlatforms.TimeScope[]
struct TimeScopeU5BU5D_t5474  : public Array_t
{
};
// UnityEngine.PropertyAttribute[]
// UnityEngine.PropertyAttribute[]
struct PropertyAttributeU5BU5D_t5475  : public Array_t
{
};
// UnityEngine.TooltipAttribute[]
// UnityEngine.TooltipAttribute[]
struct TooltipAttributeU5BU5D_t5476  : public Array_t
{
};
// UnityEngine.SpaceAttribute[]
// UnityEngine.SpaceAttribute[]
struct SpaceAttributeU5BU5D_t5477  : public Array_t
{
};
// UnityEngine.RangeAttribute[]
// UnityEngine.RangeAttribute[]
struct RangeAttributeU5BU5D_t5478  : public Array_t
{
};
// UnityEngine.TextAreaAttribute[]
// UnityEngine.TextAreaAttribute[]
struct TextAreaAttributeU5BU5D_t5479  : public Array_t
{
};
// UnityEngine.SelectionBaseAttribute[]
// UnityEngine.SelectionBaseAttribute[]
struct SelectionBaseAttributeU5BU5D_t5480  : public Array_t
{
};
// UnityEngine.SharedBetweenAnimatorsAttribute[]
// UnityEngine.SharedBetweenAnimatorsAttribute[]
struct SharedBetweenAnimatorsAttributeU5BU5D_t5481  : public Array_t
{
};
// UnityEngine.StateMachineBehaviour[]
// UnityEngine.StateMachineBehaviour[]
struct StateMachineBehaviourU5BU5D_t5482  : public Array_t
{
};
// UnityEngine.Event[]
// UnityEngine.Event[]
struct EventU5BU5D_t4759  : public Array_t
{
};
struct EventU5BU5D_t4759_StaticFields{
};
// UnityEngine.TextEditor/TextEditOp[]
// UnityEngine.TextEditor/TextEditOp[]
struct TextEditOpU5BU5D_t4760  : public Array_t
{
};
// UnityEngine.TextEditor/DblClickSnapping[]
// UnityEngine.TextEditor/DblClickSnapping[]
struct DblClickSnappingU5BU5D_t5483  : public Array_t
{
};
// UnityEngine.Events.PersistentListenerMode[]
// UnityEngine.Events.PersistentListenerMode[]
struct PersistentListenerModeU5BU5D_t5484  : public Array_t
{
};
// UnityEngine.Events.UnityEventCallState[]
// UnityEngine.Events.UnityEventCallState[]
struct UnityEventCallStateU5BU5D_t5485  : public Array_t
{
};
// UnityEngine.Events.PersistentCall[]
// UnityEngine.Events.PersistentCall[]
struct PersistentCallU5BU5D_t4795  : public Array_t
{
};
// UnityEngine.Events.BaseInvokableCall[]
// UnityEngine.Events.BaseInvokableCall[]
struct BaseInvokableCallU5BU5D_t4809  : public Array_t
{
};
// UnityEngine.UserAuthorizationDialog[]
// UnityEngine.UserAuthorizationDialog[]
struct UserAuthorizationDialogU5BU5D_t5486  : public Array_t
{
};
// UnityEngine.Internal.DefaultValueAttribute[]
// UnityEngine.Internal.DefaultValueAttribute[]
struct DefaultValueAttributeU5BU5D_t5487  : public Array_t
{
};
// UnityEngine.Internal.ExcludeFromDocsAttribute[]
// UnityEngine.Internal.ExcludeFromDocsAttribute[]
struct ExcludeFromDocsAttributeU5BU5D_t5488  : public Array_t
{
};
// UnityEngine.Serialization.FormerlySerializedAsAttribute[]
// UnityEngine.Serialization.FormerlySerializedAsAttribute[]
struct FormerlySerializedAsAttributeU5BU5D_t5489  : public Array_t
{
};
// UnityEngineInternal.TypeInferenceRules[]
// UnityEngineInternal.TypeInferenceRules[]
struct TypeInferenceRulesU5BU5D_t5490  : public Array_t
{
};
// UnityEngineInternal.TypeInferenceRuleAttribute[]
// UnityEngineInternal.TypeInferenceRuleAttribute[]
struct TypeInferenceRuleAttributeU5BU5D_t5491  : public Array_t
{
};
