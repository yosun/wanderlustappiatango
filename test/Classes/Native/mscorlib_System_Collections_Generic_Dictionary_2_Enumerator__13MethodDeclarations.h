﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>
struct Enumerator_t3943;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t681;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void Enumerator__ctor_m22879 (Enumerator_t3943 * __this, Dictionary_2_t681 * ___dictionary, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22880 (Enumerator_t3943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Entry()
 DictionaryEntry_t1302  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22881 (Enumerator_t3943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Key()
 Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22882 (Enumerator_t3943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Value()
 Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22883 (Enumerator_t3943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::MoveNext()
 bool Enumerator_MoveNext_m22884 (Enumerator_t3943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_Current()
 KeyValuePair_2_t3941  Enumerator_get_Current_m22885 (Enumerator_t3943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_CurrentKey()
 Type_t * Enumerator_get_CurrentKey_m22886 (Enumerator_t3943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::get_CurrentValue()
 uint16_t Enumerator_get_CurrentValue_m22887 (Enumerator_t3943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::VerifyState()
 void Enumerator_VerifyState_m22888 (Enumerator_t3943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::VerifyCurrent()
 void Enumerator_VerifyCurrent_m22889 (Enumerator_t3943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,System.UInt16>::Dispose()
 void Enumerator_Dispose_m22890 (Enumerator_t3943 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
