﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t267;
// System.String
struct String_t;

// System.Void UnityEngine.UI.AnimationTriggers::.ctor()
 void AnimationTriggers__ctor_m962 (AnimationTriggers_t267 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_normalTrigger()
 String_t* AnimationTriggers_get_normalTrigger_m963 (AnimationTriggers_t267 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_normalTrigger(System.String)
 void AnimationTriggers_set_normalTrigger_m964 (AnimationTriggers_t267 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_highlightedTrigger()
 String_t* AnimationTriggers_get_highlightedTrigger_m965 (AnimationTriggers_t267 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_highlightedTrigger(System.String)
 void AnimationTriggers_set_highlightedTrigger_m966 (AnimationTriggers_t267 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_pressedTrigger()
 String_t* AnimationTriggers_get_pressedTrigger_m967 (AnimationTriggers_t267 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_pressedTrigger(System.String)
 void AnimationTriggers_set_pressedTrigger_m968 (AnimationTriggers_t267 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UI.AnimationTriggers::get_disabledTrigger()
 String_t* AnimationTriggers_get_disabledTrigger_m969 (AnimationTriggers_t267 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AnimationTriggers::set_disabledTrigger(System.String)
 void AnimationTriggers_set_disabledTrigger_m970 (AnimationTriggers_t267 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
