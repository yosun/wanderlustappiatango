﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.IVideoBackgroundEventHandler>
struct EqualityComparer_1_t4336;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.IVideoBackgroundEventHandler>
struct EqualityComparer_1_t4336  : public Object_t
{
};
struct EqualityComparer_1_t4336_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.IVideoBackgroundEventHandler>::_default
	EqualityComparer_1_t4336 * ____default_0;
};
