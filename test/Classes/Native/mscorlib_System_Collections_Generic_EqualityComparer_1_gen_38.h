﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.WordResult>
struct EqualityComparer_1_t3974;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.WordResult>
struct EqualityComparer_1_t3974  : public Object_t
{
};
struct EqualityComparer_1_t3974_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.WordResult>::_default
	EqualityComparer_1_t3974 * ____default_0;
};
