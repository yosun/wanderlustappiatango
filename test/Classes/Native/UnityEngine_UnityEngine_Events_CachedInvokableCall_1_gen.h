﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<System.Single>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_65.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t1188  : public InvokableCall_1_t3452
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<System.Single>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
