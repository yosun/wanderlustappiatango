﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.UIBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_47.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.UIBehaviour>
struct CachedInvokableCall_1_t3138  : public InvokableCall_1_t3139
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.UIBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
