﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// UnityEngine.RectTransform
struct RectTransform_t287;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t514  : public MulticastDelegate_t325
{
};
