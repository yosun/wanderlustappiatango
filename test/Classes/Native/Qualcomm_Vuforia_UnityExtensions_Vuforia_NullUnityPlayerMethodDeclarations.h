﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.NullUnityPlayer
struct NullUnityPlayer_t161;
// System.String
struct String_t;
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void Vuforia.NullUnityPlayer::LoadNativeLibraries()
 void NullUnityPlayer_LoadNativeLibraries_m2702 (NullUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::InitializePlatform()
 void NullUnityPlayer_InitializePlatform_m2703 (NullUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARUnity/InitError Vuforia.NullUnityPlayer::Start(System.String)
 int32_t NullUnityPlayer_Start_m2704 (NullUnityPlayer_t161 * __this, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::Update()
 void NullUnityPlayer_Update_m2705 (NullUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::Dispose()
 void NullUnityPlayer_Dispose_m2706 (NullUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnPause()
 void NullUnityPlayer_OnPause_m2707 (NullUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnResume()
 void NullUnityPlayer_OnResume_m2708 (NullUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnDestroy()
 void NullUnityPlayer_OnDestroy_m2709 (NullUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::.ctor()
 void NullUnityPlayer__ctor_m538 (NullUnityPlayer_t161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
