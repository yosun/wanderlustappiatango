﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct Stack_1_t3033;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
struct IEnumerator_1_t3038;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
struct List_1_t240;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"
#define Stack_1__ctor_m15733(__this, method) (void)Stack_1__ctor_m15717_gshared((Stack_1_t3035 *)__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m15734(__this, method) (bool)Stack_1_System_Collections_ICollection_get_IsSynchronized_m15718_gshared((Stack_1_t3035 *)__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m15735(__this, method) (Object_t *)Stack_1_System_Collections_ICollection_get_SyncRoot_m15719_gshared((Stack_1_t3035 *)__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m15736(__this, ___dest, ___idx, method) (void)Stack_1_System_Collections_ICollection_CopyTo_m15720_gshared((Stack_1_t3035 *)__this, (Array_t *)___dest, (int32_t)___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15737(__this, method) (Object_t*)Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15721_gshared((Stack_1_t3035 *)__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m15738(__this, method) (Object_t *)Stack_1_System_Collections_IEnumerable_GetEnumerator_m15722_gshared((Stack_1_t3035 *)__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Peek()
#define Stack_1_Peek_m15739(__this, method) (List_1_t240 *)Stack_1_Peek_m15723_gshared((Stack_1_t3035 *)__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Pop()
#define Stack_1_Pop_m15740(__this, method) (List_1_t240 *)Stack_1_Pop_m15724_gshared((Stack_1_t3035 *)__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Push(T)
#define Stack_1_Push_m15741(__this, ___t, method) (void)Stack_1_Push_m15725_gshared((Stack_1_t3035 *)__this, (Object_t *)___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_Count()
#define Stack_1_get_Count_m15742(__this, method) (int32_t)Stack_1_get_Count_m15726_gshared((Stack_1_t3035 *)__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::GetEnumerator()
 Enumerator_t3039  Stack_1_GetEnumerator_m15743 (Stack_1_t3033 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
