﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARNativeIosWrapper
struct QCARNativeIosWrapper_t705;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t418;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceInitCamera(System.Int32)
 int32_t QCARNativeIosWrapper_CameraDeviceInitCamera_m3150 (QCARNativeIosWrapper_t705 * __this, int32_t ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceDeinitCamera()
 int32_t QCARNativeIosWrapper_CameraDeviceDeinitCamera_m3151 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceStartCamera()
 int32_t QCARNativeIosWrapper_CameraDeviceStartCamera_m3152 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceStopCamera()
 int32_t QCARNativeIosWrapper_CameraDeviceStopCamera_m3153 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceGetNumVideoModes()
 int32_t QCARNativeIosWrapper_CameraDeviceGetNumVideoModes_m3154 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::CameraDeviceGetVideoMode(System.Int32,System.IntPtr)
 void QCARNativeIosWrapper_CameraDeviceGetVideoMode_m3155 (QCARNativeIosWrapper_t705 * __this, int32_t ___idx, IntPtr_t121 ___videoMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceSelectVideoMode(System.Int32)
 int32_t QCARNativeIosWrapper_CameraDeviceSelectVideoMode_m3156 (QCARNativeIosWrapper_t705 * __this, int32_t ___idx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceSetFlashTorchMode(System.Int32)
 int32_t QCARNativeIosWrapper_CameraDeviceSetFlashTorchMode_m3157 (QCARNativeIosWrapper_t705 * __this, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceSetFocusMode(System.Int32)
 int32_t QCARNativeIosWrapper_CameraDeviceSetFocusMode_m3158 (QCARNativeIosWrapper_t705 * __this, int32_t ___focusMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CameraDeviceSetCameraConfiguration(System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_CameraDeviceSetCameraConfiguration_m3159 (QCARNativeIosWrapper_t705 * __this, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarSetFrameFormat(System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_QcarSetFrameFormat_m3160 (QCARNativeIosWrapper_t705 * __this, int32_t ___format, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetExists(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_DataSetExists_m3161 (QCARNativeIosWrapper_t705 * __this, String_t* ___relativePath, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetLoad(System.String,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_DataSetLoad_m3162 (QCARNativeIosWrapper_t705 * __this, String_t* ___relativePath, int32_t ___storageType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_DataSetGetNumTrackableType_m3163 (QCARNativeIosWrapper_t705 * __this, int32_t ___trackableType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_DataSetGetTrackablesOfType_m3164 (QCARNativeIosWrapper_t705 * __this, int32_t ___trackableType, IntPtr_t121 ___trackableDataArray, int32_t ___trackableDataArrayLength, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNativeIosWrapper_DataSetGetTrackableName_m3165 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, StringBuilder_t418 * ___trackableName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetCreateTrackable(System.IntPtr,System.IntPtr,System.Text.StringBuilder,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_DataSetCreateTrackable_m3166 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, IntPtr_t121 ___trackableSourcePtr, StringBuilder_t418 * ___trackableName, int32_t ___nameMaxLength, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetDestroyTrackable(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_DataSetDestroyTrackable_m3167 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::DataSetHasReachedTrackableLimit(System.IntPtr)
 int32_t QCARNativeIosWrapper_DataSetHasReachedTrackableLimit_m3168 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::GetCameraThreadID()
 int32_t QCARNativeIosWrapper_GetCameraThreadID_m3169 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetBuilderBuild(System.String,System.Single)
 int32_t QCARNativeIosWrapper_ImageTargetBuilderBuild_m3170 (QCARNativeIosWrapper_t705 * __this, String_t* ___targetName, float ___sceenSizeWidth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::FrameCounterGetBenchmarkingData(System.IntPtr,System.Boolean)
 void QCARNativeIosWrapper_FrameCounterGetBenchmarkingData_m3171 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___benchmarkingData, bool ___isStereoRendering, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::ImageTargetBuilderStartScan()
 void QCARNativeIosWrapper_ImageTargetBuilderStartScan_m3172 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::ImageTargetBuilderStopScan()
 void QCARNativeIosWrapper_ImageTargetBuilderStopScan_m3173 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetBuilderGetFrameQuality()
 int32_t QCARNativeIosWrapper_ImageTargetBuilderGetFrameQuality_m3174 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::ImageTargetBuilderGetTrackableSource()
 IntPtr_t121 QCARNativeIosWrapper_ImageTargetBuilderGetTrackableSource_m3175 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_ImageTargetCreateVirtualButton_m3176 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String)
 int32_t QCARNativeIosWrapper_ImageTargetDestroyVirtualButton_m3177 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::VirtualButtonGetId(System.IntPtr,System.String,System.String)
 int32_t QCARNativeIosWrapper_VirtualButtonGetId_m3178 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetGetNumVirtualButtons(System.IntPtr,System.String)
 int32_t QCARNativeIosWrapper_ImageTargetGetNumVirtualButtons_m3179 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String)
 int32_t QCARNativeIosWrapper_ImageTargetGetVirtualButtons_m3180 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___virtualButtonDataArray, IntPtr_t121 ___rectangleDataArray, int32_t ___virtualButtonDataArrayLength, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ImageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNativeIosWrapper_ImageTargetGetVirtualButtonName_m3181 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, int32_t ___idx, StringBuilder_t418 * ___vbName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_CylinderTargetGetDimensions_m3182 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___dimensions, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CylinderTargetSetSideLength(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeIosWrapper_CylinderTargetSetSideLength_m3183 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___sideLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CylinderTargetSetTopDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeIosWrapper_CylinderTargetSetTopDiameter_m3184 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___topDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::CylinderTargetSetBottomDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeIosWrapper_CylinderTargetSetBottomDiameter_m3185 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___bottomDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTargetSetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_ObjectTargetSetSize_m3186 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTargetGetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_ObjectTargetGetSize_m3187 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerStart()
 int32_t QCARNativeIosWrapper_ObjectTrackerStart_m3188 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::ObjectTrackerStop()
 void QCARNativeIosWrapper_ObjectTrackerStop_m3189 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::ObjectTrackerCreateDataSet()
 IntPtr_t121 QCARNativeIosWrapper_ObjectTrackerCreateDataSet_m3190 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerDestroyDataSet(System.IntPtr)
 int32_t QCARNativeIosWrapper_ObjectTrackerDestroyDataSet_m3191 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerActivateDataSet(System.IntPtr)
 int32_t QCARNativeIosWrapper_ObjectTrackerActivateDataSet_m3192 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerDeactivateDataSet(System.IntPtr)
 int32_t QCARNativeIosWrapper_ObjectTrackerDeactivateDataSet_m3193 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerPersistExtendedTracking(System.Int32)
 int32_t QCARNativeIosWrapper_ObjectTrackerPersistExtendedTracking_m3194 (QCARNativeIosWrapper_t705 * __this, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ObjectTrackerResetExtendedTracking()
 int32_t QCARNativeIosWrapper_ObjectTrackerResetExtendedTracking_m3195 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::MarkerSetSize(System.Int32,System.Single)
 int32_t QCARNativeIosWrapper_MarkerSetSize_m3196 (QCARNativeIosWrapper_t705 * __this, int32_t ___trackableIndex, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::MarkerTrackerStart()
 int32_t QCARNativeIosWrapper_MarkerTrackerStart_m3197 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::MarkerTrackerStop()
 void QCARNativeIosWrapper_MarkerTrackerStop_m3198 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::MarkerTrackerCreateMarker(System.Int32,System.String,System.Single)
 int32_t QCARNativeIosWrapper_MarkerTrackerCreateMarker_m3199 (QCARNativeIosWrapper_t705 * __this, int32_t ___id, String_t* ___trackableName, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::MarkerTrackerDestroyMarker(System.Int32)
 int32_t QCARNativeIosWrapper_MarkerTrackerDestroyMarker_m3200 (QCARNativeIosWrapper_t705 * __this, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::InitPlatformNative()
 void QCARNativeIosWrapper_InitPlatformNative_m3201 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::InitFrameState(System.IntPtr)
 void QCARNativeIosWrapper_InitFrameState_m3202 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::DeinitFrameState(System.IntPtr)
 void QCARNativeIosWrapper_DeinitFrameState_m3203 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::OnSurfaceChanged(System.Int32,System.Int32)
 void QCARNativeIosWrapper_OnSurfaceChanged_m3204 (QCARNativeIosWrapper_t705 * __this, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::OnPause()
 void QCARNativeIosWrapper_OnPause_m3205 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::OnResume()
 void QCARNativeIosWrapper_OnResume_m3206 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::HasSurfaceBeenRecreated()
 bool QCARNativeIosWrapper_HasSurfaceBeenRecreated_m3207 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::UpdateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_UpdateQCAR_m3208 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___imageHeaderDataArray, int32_t ___imageHeaderArrayLength, IntPtr_t121 ___frameIndex, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::RendererEnd()
 void QCARNativeIosWrapper_RendererEnd_m3209 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarGetBufferSize(System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_QcarGetBufferSize_m3210 (QCARNativeIosWrapper_t705 * __this, int32_t ___width, int32_t ___height, int32_t ___format, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::QcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 void QCARNativeIosWrapper_QcarAddCameraFrame_m3211 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___pixels, int32_t ___width, int32_t ___height, int32_t ___format, int32_t ___stride, int32_t ___frameIdx, int32_t ___flipHorizontally, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::RendererSetVideoBackgroundCfg(System.IntPtr)
 void QCARNativeIosWrapper_RendererSetVideoBackgroundCfg_m3212 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::RendererGetVideoBackgroundCfg(System.IntPtr)
 void QCARNativeIosWrapper_RendererGetVideoBackgroundCfg_m3213 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::RendererGetVideoBackgroundTextureInfo(System.IntPtr)
 void QCARNativeIosWrapper_RendererGetVideoBackgroundTextureInfo_m3214 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___texInfo, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::RendererSetVideoBackgroundTextureID(System.Int32)
 int32_t QCARNativeIosWrapper_RendererSetVideoBackgroundTextureID_m3215 (QCARNativeIosWrapper_t705 * __this, int32_t ___textureID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::RendererIsVideoBackgroundTextureInfoAvailable()
 int32_t QCARNativeIosWrapper_RendererIsVideoBackgroundTextureInfoAvailable_m3216 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarSetHint(System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_QcarSetHint_m3217 (QCARNativeIosWrapper_t705 * __this, int32_t ___hint, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::GetProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_GetProjectionGL_m3218 (QCARNativeIosWrapper_t705 * __this, float ___nearClip, float ___farClip, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::SetApplicationEnvironment(System.Int32,System.Int32,System.Int32)
 void QCARNativeIosWrapper_SetApplicationEnvironment_m3219 (QCARNativeIosWrapper_t705 * __this, int32_t ___unityVersionMajor, int32_t ___unityVersionMinor, int32_t ___unityVersionChange, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::SetStateBufferSize(System.Int32)
 void QCARNativeIosWrapper_SetStateBufferSize_m3220 (QCARNativeIosWrapper_t705 * __this, int32_t ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerStart()
 int32_t QCARNativeIosWrapper_SmartTerrainTrackerStart_m3221 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerStop()
 void QCARNativeIosWrapper_SmartTerrainTrackerStop_m3222 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerSetScaleToMillimeter(System.Single)
 bool QCARNativeIosWrapper_SmartTerrainTrackerSetScaleToMillimeter_m3223 (QCARNativeIosWrapper_t705 * __this, float ___scaleFactor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerInitBuilder()
 bool QCARNativeIosWrapper_SmartTerrainTrackerInitBuilder_m3224 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainTrackerDeinitBuilder()
 bool QCARNativeIosWrapper_SmartTerrainTrackerDeinitBuilder_m3225 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderCreateReconstructionFromTarget()
 IntPtr_t121 QCARNativeIosWrapper_SmartTerrainBuilderCreateReconstructionFromTarget_m3226 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderCreateReconstructionFromEnvironment()
 IntPtr_t121 QCARNativeIosWrapper_SmartTerrainBuilderCreateReconstructionFromEnvironment_m3227 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderAddReconstruction(System.IntPtr)
 bool QCARNativeIosWrapper_SmartTerrainBuilderAddReconstruction_m3228 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderRemoveReconstruction(System.IntPtr)
 bool QCARNativeIosWrapper_SmartTerrainBuilderRemoveReconstruction_m3229 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::SmartTerrainBuilderDestroyReconstruction(System.IntPtr)
 bool QCARNativeIosWrapper_SmartTerrainBuilderDestroyReconstruction_m3230 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionStart(System.IntPtr)
 bool QCARNativeIosWrapper_ReconstructionStart_m3231 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionStop(System.IntPtr)
 bool QCARNativeIosWrapper_ReconstructionStop_m3232 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionIsReconstructing(System.IntPtr)
 bool QCARNativeIosWrapper_ReconstructionIsReconstructing_m3233 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionReset(System.IntPtr)
 bool QCARNativeIosWrapper_ReconstructionReset_m3234 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::ReconstructionSetNavMeshPadding(System.IntPtr,System.Single)
 void QCARNativeIosWrapper_ReconstructionSetNavMeshPadding_m3235 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___reconstruction, float ___padding, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single)
 bool QCARNativeIosWrapper_ReconstructionFromTargetSetInitializationTarget_m3236 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___reconstruction, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, IntPtr_t121 ___occluderMin, IntPtr_t121 ___occluderMax, IntPtr_t121 ___offsetToOccluder, IntPtr_t121 ___rotationAxisToOccluder, float ___rotationAngleToOccluder, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::ReconstructionSetMaximumArea(System.IntPtr,System.IntPtr)
 bool QCARNativeIosWrapper_ReconstructionSetMaximumArea_m3237 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___reconstruction, IntPtr_t121 ___maximumArea, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::ReconstructioFromEnvironmentGetReconstructionState(System.IntPtr)
 int32_t QCARNativeIosWrapper_ReconstructioFromEnvironmentGetReconstructionState_m3238 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderStartInit(System.String,System.String)
 int32_t QCARNativeIosWrapper_TargetFinderStartInit_m3239 (QCARNativeIosWrapper_t705 * __this, String_t* ___userKey, String_t* ___secretKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderGetInitState()
 int32_t QCARNativeIosWrapper_TargetFinderGetInitState_m3240 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderDeinit()
 int32_t QCARNativeIosWrapper_TargetFinderDeinit_m3241 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderStartRecognition()
 int32_t QCARNativeIosWrapper_TargetFinderStartRecognition_m3242 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderStop()
 int32_t QCARNativeIosWrapper_TargetFinderStop_m3243 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderSetUIScanlineColor(System.Single,System.Single,System.Single)
 void QCARNativeIosWrapper_TargetFinderSetUIScanlineColor_m3244 (QCARNativeIosWrapper_t705 * __this, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderSetUIPointColor(System.Single,System.Single,System.Single)
 void QCARNativeIosWrapper_TargetFinderSetUIPointColor_m3245 (QCARNativeIosWrapper_t705 * __this, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderUpdate(System.IntPtr)
 void QCARNativeIosWrapper_TargetFinderUpdate_m3246 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___targetFinderState, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderGetResults(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_TargetFinderGetResults_m3247 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___searchResultArray, int32_t ___searchResultArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TargetFinderEnableTracking(System.IntPtr,System.IntPtr)
 int32_t QCARNativeIosWrapper_TargetFinderEnableTracking_m3248 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___searchResult, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderGetImageTargets(System.IntPtr,System.Int32)
 void QCARNativeIosWrapper_TargetFinderGetImageTargets_m3249 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___trackableIdArray, int32_t ___trackableIdArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TargetFinderClearTrackables()
 void QCARNativeIosWrapper_TargetFinderClearTrackables_m3250 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TextTrackerStart()
 int32_t QCARNativeIosWrapper_TextTrackerStart_m3251 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TextTrackerStop()
 void QCARNativeIosWrapper_TextTrackerStop_m3252 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TextTrackerSetRegionOfInterest(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_TextTrackerSetRegionOfInterest_m3253 (QCARNativeIosWrapper_t705 * __this, int32_t ___detectionLeftTopX, int32_t ___detectionLeftTopY, int32_t ___detectionRightBottomX, int32_t ___detectionRightBottomY, int32_t ___trackingLeftTopX, int32_t ___trackingLeftTopY, int32_t ___trackingRightBottomX, int32_t ___trackingRightBottomY, int32_t ___upDirection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::TextTrackerGetRegionOfInterest(System.IntPtr,System.IntPtr)
 void QCARNativeIosWrapper_TextTrackerGetRegionOfInterest_m3254 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___detectionROI, IntPtr_t121 ___trackingROI, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListLoadWordList(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_WordListLoadWordList_m3255 (QCARNativeIosWrapper_t705 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListAddWordsFromFile(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_WordListAddWordsFromFile_m3256 (QCARNativeIosWrapper_t705 * __this, String_t* ___path, int32_t ___storagetType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListAddWordU(System.IntPtr)
 int32_t QCARNativeIosWrapper_WordListAddWordU_m3257 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListRemoveWordU(System.IntPtr)
 int32_t QCARNativeIosWrapper_WordListRemoveWordU_m3258 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListContainsWordU(System.IntPtr)
 int32_t QCARNativeIosWrapper_WordListContainsWordU_m3259 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListUnloadAllLists()
 int32_t QCARNativeIosWrapper_WordListUnloadAllLists_m3260 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListSetFilterMode(System.Int32)
 int32_t QCARNativeIosWrapper_WordListSetFilterMode_m3261 (QCARNativeIosWrapper_t705 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListGetFilterMode()
 int32_t QCARNativeIosWrapper_WordListGetFilterMode_m3262 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListLoadFilterList(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_WordListLoadFilterList_m3263 (QCARNativeIosWrapper_t705 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListAddWordToFilterListU(System.IntPtr)
 int32_t QCARNativeIosWrapper_WordListAddWordToFilterListU_m3264 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListRemoveWordFromFilterListU(System.IntPtr)
 int32_t QCARNativeIosWrapper_WordListRemoveWordFromFilterListU_m3265 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListClearFilterList()
 int32_t QCARNativeIosWrapper_WordListClearFilterList_m3266 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordListGetFilterListWordCount()
 int32_t QCARNativeIosWrapper_WordListGetFilterListWordCount_m3267 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::WordListGetFilterListWordU(System.Int32)
 IntPtr_t121 QCARNativeIosWrapper_WordListGetFilterListWordU_m3268 (QCARNativeIosWrapper_t705 * __this, int32_t ___i, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordGetLetterMask(System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_WordGetLetterMask_m3269 (QCARNativeIosWrapper_t705 * __this, int32_t ___wordID, IntPtr_t121 ___letterMaskImage, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::WordGetLetterBoundingBoxes(System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_WordGetLetterBoundingBoxes_m3270 (QCARNativeIosWrapper_t705 * __this, int32_t ___wordID, IntPtr_t121 ___letterBoundingBoxes, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TrackerManagerInitTracker(System.Int32)
 int32_t QCARNativeIosWrapper_TrackerManagerInitTracker_m3271 (QCARNativeIosWrapper_t705 * __this, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::TrackerManagerDeinitTracker(System.Int32)
 int32_t QCARNativeIosWrapper_TrackerManagerDeinitTracker_m3272 (QCARNativeIosWrapper_t705 * __this, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::VirtualButtonSetEnabled(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNativeIosWrapper_VirtualButtonSetEnabled_m3273 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::VirtualButtonSetSensitivity(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNativeIosWrapper_VirtualButtonSetSensitivity_m3274 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___sensitivity, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::VirtualButtonSetAreaRectangle(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_VirtualButtonSetAreaRectangle_m3275 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarInit(System.String)
 int32_t QCARNativeIosWrapper_QcarInit_m3276 (QCARNativeIosWrapper_t705 * __this, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::QcarDeinit()
 int32_t QCARNativeIosWrapper_QcarDeinit_m3277 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::StartExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_StartExtendedTracking_m3278 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::StopExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_StopExtendedTracking_m3279 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsSupportedDeviceDetected()
 bool QCARNativeIosWrapper_EyewearIsSupportedDeviceDetected_m3280 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsSeeThru()
 bool QCARNativeIosWrapper_EyewearIsSeeThru_m3281 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearGetScreenOrientation()
 int32_t QCARNativeIosWrapper_EyewearGetScreenOrientation_m3282 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsStereoCapable()
 bool QCARNativeIosWrapper_EyewearIsStereoCapable_m3283 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsStereoEnabled()
 bool QCARNativeIosWrapper_EyewearIsStereoEnabled_m3284 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearIsStereoGLOnly()
 bool QCARNativeIosWrapper_EyewearIsStereoGLOnly_m3285 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearSetStereo(System.Boolean)
 bool QCARNativeIosWrapper_EyewearSetStereo_m3286 (QCARNativeIosWrapper_t705 * __this, bool ___enable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearGetDefaultSceneScale(System.IntPtr)
 int32_t QCARNativeIosWrapper_EyewearGetDefaultSceneScale_m3287 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_EyewearGetProjectionMatrix_m3288 (QCARNativeIosWrapper_t705 * __this, int32_t ___eyeID, int32_t ___profileID, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearCPMGetMaxCount()
 int32_t QCARNativeIosWrapper_EyewearCPMGetMaxCount_m3289 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearCPMGetUsedCount()
 int32_t QCARNativeIosWrapper_EyewearCPMGetUsedCount_m3290 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMIsProfileUsed(System.Int32)
 bool QCARNativeIosWrapper_EyewearCPMIsProfileUsed_m3291 (QCARNativeIosWrapper_t705 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearCPMGetActiveProfile()
 int32_t QCARNativeIosWrapper_EyewearCPMGetActiveProfile_m3292 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMSetActiveProfile(System.Int32)
 bool QCARNativeIosWrapper_EyewearCPMSetActiveProfile_m3293 (QCARNativeIosWrapper_t705 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::EyewearCPMGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_EyewearCPMGetProjectionMatrix_m3294 (QCARNativeIosWrapper_t705 * __this, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMSetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 bool QCARNativeIosWrapper_EyewearCPMSetProjectionMatrix_m3295 (QCARNativeIosWrapper_t705 * __this, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::EyewearCPMGetProfileName(System.Int32)
 IntPtr_t121 QCARNativeIosWrapper_EyewearCPMGetProfileName_m3296 (QCARNativeIosWrapper_t705 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMSetProfileName(System.Int32,System.IntPtr)
 bool QCARNativeIosWrapper_EyewearCPMSetProfileName_m3297 (QCARNativeIosWrapper_t705 * __this, int32_t ___profileID, IntPtr_t121 ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearCPMClearProfile(System.Int32)
 bool QCARNativeIosWrapper_EyewearCPMClearProfile_m3298 (QCARNativeIosWrapper_t705 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorInit(System.Int32,System.Int32,System.Int32,System.Int32)
 bool QCARNativeIosWrapper_EyewearUserCalibratorInit_m3299 (QCARNativeIosWrapper_t705 * __this, int32_t ___surfaceWidth, int32_t ___surfaceHeight, int32_t ___targetWidth, int32_t ___targetHeight, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorGetMinScaleHint()
 float QCARNativeIosWrapper_EyewearUserCalibratorGetMinScaleHint_m3300 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorGetMaxScaleHint()
 float QCARNativeIosWrapper_EyewearUserCalibratorGetMaxScaleHint_m3301 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorIsStereoStretched()
 bool QCARNativeIosWrapper_EyewearUserCalibratorIsStereoStretched_m3302 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::EyewearUserCalibratorGetProjectionMatrix(System.IntPtr,System.Int32,System.IntPtr)
 bool QCARNativeIosWrapper_EyewearUserCalibratorGetProjectionMatrix_m3303 (QCARNativeIosWrapper_t705 * __this, IntPtr_t121 ___readingsArray, int32_t ___numReadings, IntPtr_t121 ___calibrationResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::smartTerrainTrackerStart()
 int32_t QCARNativeIosWrapper_smartTerrainTrackerStart_m3304 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::smartTerrainTrackerStop()
 void QCARNativeIosWrapper_smartTerrainTrackerStop_m3305 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainTrackerSetScaleToMillimeter(System.Single)
 bool QCARNativeIosWrapper_smartTerrainTrackerSetScaleToMillimeter_m3306 (Object_t * __this/* static, unused */, float ___scaleFactor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainTrackerInitBuilder()
 bool QCARNativeIosWrapper_smartTerrainTrackerInitBuilder_m3307 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainTrackerDeinitBuilder()
 bool QCARNativeIosWrapper_smartTerrainTrackerDeinitBuilder_m3308 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::smartTerrainBuilderCreateReconstructionFromTarget()
 IntPtr_t121 QCARNativeIosWrapper_smartTerrainBuilderCreateReconstructionFromTarget_m3309 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::smartTerrainBuilderCreateReconstructionFromEnvironment()
 IntPtr_t121 QCARNativeIosWrapper_smartTerrainBuilderCreateReconstructionFromEnvironment_m3310 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainBuilderAddReconstruction(System.IntPtr)
 bool QCARNativeIosWrapper_smartTerrainBuilderAddReconstruction_m3311 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainBuilderRemoveReconstruction(System.IntPtr)
 bool QCARNativeIosWrapper_smartTerrainBuilderRemoveReconstruction_m3312 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::smartTerrainBuilderDestroyReconstruction(System.IntPtr)
 bool QCARNativeIosWrapper_smartTerrainBuilderDestroyReconstruction_m3313 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionStart(System.IntPtr)
 bool QCARNativeIosWrapper_reconstructionStart_m3314 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionStop(System.IntPtr)
 bool QCARNativeIosWrapper_reconstructionStop_m3315 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionIsReconstructing(System.IntPtr)
 bool QCARNativeIosWrapper_reconstructionIsReconstructing_m3316 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionReset(System.IntPtr)
 bool QCARNativeIosWrapper_reconstructionReset_m3317 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::reconstructionSetNavMeshPadding(System.IntPtr,System.Single)
 void QCARNativeIosWrapper_reconstructionSetNavMeshPadding_m3318 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, float ___padding, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single)
 bool QCARNativeIosWrapper_reconstructionFromTargetSetInitializationTarget_m3319 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, IntPtr_t121 ___occluderMin, IntPtr_t121 ___occluderMax, IntPtr_t121 ___offsetToOccluder, IntPtr_t121 ___rotationAxisToOccluder, float ___rotationAngleToOccluder, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::reconstructionSetMaximumArea(System.IntPtr,System.IntPtr)
 bool QCARNativeIosWrapper_reconstructionSetMaximumArea_m3320 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, IntPtr_t121 ___maximumArea, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::reconstructioFromEnvironmentGetReconstructionState(System.IntPtr)
 int32_t QCARNativeIosWrapper_reconstructioFromEnvironmentGetReconstructionState_m3321 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceInitCamera(System.Int32)
 int32_t QCARNativeIosWrapper_cameraDeviceInitCamera_m3322 (Object_t * __this/* static, unused */, int32_t ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceDeinitCamera()
 int32_t QCARNativeIosWrapper_cameraDeviceDeinitCamera_m3323 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceStartCamera()
 int32_t QCARNativeIosWrapper_cameraDeviceStartCamera_m3324 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceStopCamera()
 int32_t QCARNativeIosWrapper_cameraDeviceStopCamera_m3325 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceGetNumVideoModes()
 int32_t QCARNativeIosWrapper_cameraDeviceGetNumVideoModes_m3326 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::cameraDeviceGetVideoMode(System.Int32,System.IntPtr)
 void QCARNativeIosWrapper_cameraDeviceGetVideoMode_m3327 (Object_t * __this/* static, unused */, int32_t ___idx, IntPtr_t121 ___videoMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceSelectVideoMode(System.Int32)
 int32_t QCARNativeIosWrapper_cameraDeviceSelectVideoMode_m3328 (Object_t * __this/* static, unused */, int32_t ___idx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceSetFlashTorchMode(System.Int32)
 int32_t QCARNativeIosWrapper_cameraDeviceSetFlashTorchMode_m3329 (Object_t * __this/* static, unused */, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceSetFocusMode(System.Int32)
 int32_t QCARNativeIosWrapper_cameraDeviceSetFocusMode_m3330 (Object_t * __this/* static, unused */, int32_t ___focusMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cameraDeviceSetCameraConfiguration(System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_cameraDeviceSetCameraConfiguration_m3331 (Object_t * __this/* static, unused */, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarSetFrameFormat(System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_qcarSetFrameFormat_m3332 (Object_t * __this/* static, unused */, int32_t ___format, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetExists(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_dataSetExists_m3333 (Object_t * __this/* static, unused */, String_t* ___relativePath, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetLoad(System.String,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_dataSetLoad_m3334 (Object_t * __this/* static, unused */, String_t* ___relativePath, int32_t ___storageType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetGetNumTrackableType(System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_dataSetGetNumTrackableType_m3335 (Object_t * __this/* static, unused */, int32_t ___trackableType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_dataSetGetTrackablesOfType_m3336 (Object_t * __this/* static, unused */, int32_t ___trackableType, IntPtr_t121 ___trackableDataArray, int32_t ___trackableDataArrayLength, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNativeIosWrapper_dataSetGetTrackableName_m3337 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, StringBuilder_t418 * ___trackableName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetCreateTrackable(System.IntPtr,System.IntPtr,System.Text.StringBuilder,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_dataSetCreateTrackable_m3338 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, IntPtr_t121 ___trackableSourcePtr, StringBuilder_t418 * ___trackableName, int32_t ___nameMaxLength, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetDestroyTrackable(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_dataSetDestroyTrackable_m3339 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::dataSetHasReachedTrackableLimit(System.IntPtr)
 int32_t QCARNativeIosWrapper_dataSetHasReachedTrackableLimit_m3340 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::getCameraThreadID()
 int32_t QCARNativeIosWrapper_getCameraThreadID_m3341 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetBuilderBuild(System.String,System.Single)
 int32_t QCARNativeIosWrapper_imageTargetBuilderBuild_m3342 (Object_t * __this/* static, unused */, String_t* ___targetName, float ___sceenSizeWidth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::frameCounterGetBenchmarkingData(System.IntPtr,System.Boolean)
 void QCARNativeIosWrapper_frameCounterGetBenchmarkingData_m3343 (Object_t * __this/* static, unused */, IntPtr_t121 ___benchmarkingData, bool ___isStereoRendering, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::imageTargetBuilderStartScan()
 void QCARNativeIosWrapper_imageTargetBuilderStartScan_m3344 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::imageTargetBuilderStopScan()
 void QCARNativeIosWrapper_imageTargetBuilderStopScan_m3345 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetBuilderGetFrameQuality()
 int32_t QCARNativeIosWrapper_imageTargetBuilderGetFrameQuality_m3346 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::imageTargetBuilderGetTrackableSource()
 IntPtr_t121 QCARNativeIosWrapper_imageTargetBuilderGetTrackableSource_m3347 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_imageTargetCreateVirtualButton_m3348 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String)
 int32_t QCARNativeIosWrapper_imageTargetDestroyVirtualButton_m3349 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::virtualButtonGetId(System.IntPtr,System.String,System.String)
 int32_t QCARNativeIosWrapper_virtualButtonGetId_m3350 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetGetNumVirtualButtons(System.IntPtr,System.String)
 int32_t QCARNativeIosWrapper_imageTargetGetNumVirtualButtons_m3351 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String)
 int32_t QCARNativeIosWrapper_imageTargetGetVirtualButtons_m3352 (Object_t * __this/* static, unused */, IntPtr_t121 ___virtualButtonDataArray, IntPtr_t121 ___rectangleDataArray, int32_t ___virtualButtonDataArrayLength, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::imageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNativeIosWrapper_imageTargetGetVirtualButtonName_m3353 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, int32_t ___idx, StringBuilder_t418 * ___vbName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_cylinderTargetGetDimensions_m3354 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___dimensions, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cylinderTargetSetSideLength(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeIosWrapper_cylinderTargetSetSideLength_m3355 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___sideLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cylinderTargetSetTopDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeIosWrapper_cylinderTargetSetTopDiameter_m3356 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___topDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::cylinderTargetSetBottomDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeIosWrapper_cylinderTargetSetBottomDiameter_m3357 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___bottomDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTargetSetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_objectTargetSetSize_m3358 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTargetGetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_objectTargetGetSize_m3359 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerStart()
 int32_t QCARNativeIosWrapper_objectTrackerStart_m3360 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::objectTrackerStop()
 void QCARNativeIosWrapper_objectTrackerStop_m3361 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::objectTrackerCreateDataSet()
 IntPtr_t121 QCARNativeIosWrapper_objectTrackerCreateDataSet_m3362 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerDestroyDataSet(System.IntPtr)
 int32_t QCARNativeIosWrapper_objectTrackerDestroyDataSet_m3363 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerActivateDataSet(System.IntPtr)
 int32_t QCARNativeIosWrapper_objectTrackerActivateDataSet_m3364 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerDeactivateDataSet(System.IntPtr)
 int32_t QCARNativeIosWrapper_objectTrackerDeactivateDataSet_m3365 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerPersistExtendedTracking(System.Int32)
 int32_t QCARNativeIosWrapper_objectTrackerPersistExtendedTracking_m3366 (Object_t * __this/* static, unused */, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::objectTrackerResetExtendedTracking()
 int32_t QCARNativeIosWrapper_objectTrackerResetExtendedTracking_m3367 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::markerSetSize(System.Int32,System.Single)
 int32_t QCARNativeIosWrapper_markerSetSize_m3368 (Object_t * __this/* static, unused */, int32_t ___trackableIndex, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::markerTrackerStart()
 int32_t QCARNativeIosWrapper_markerTrackerStart_m3369 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::markerTrackerStop()
 void QCARNativeIosWrapper_markerTrackerStop_m3370 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::markerTrackerCreateMarker(System.Int32,System.String,System.Single)
 int32_t QCARNativeIosWrapper_markerTrackerCreateMarker_m3371 (Object_t * __this/* static, unused */, int32_t ___id, String_t* ___trackableName, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::markerTrackerDestroyMarker(System.Int32)
 int32_t QCARNativeIosWrapper_markerTrackerDestroyMarker_m3372 (Object_t * __this/* static, unused */, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::initPlatformNative()
 void QCARNativeIosWrapper_initPlatformNative_m3373 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::initFrameState(System.IntPtr)
 void QCARNativeIosWrapper_initFrameState_m3374 (Object_t * __this/* static, unused */, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::deinitFrameState(System.IntPtr)
 void QCARNativeIosWrapper_deinitFrameState_m3375 (Object_t * __this/* static, unused */, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::onSurfaceChanged(System.Int32,System.Int32)
 void QCARNativeIosWrapper_onSurfaceChanged_m3376 (Object_t * __this/* static, unused */, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::onPause()
 void QCARNativeIosWrapper_onPause_m3377 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::onResume()
 void QCARNativeIosWrapper_onResume_m3378 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeIosWrapper::hasSurfaceBeenRecreated()
 bool QCARNativeIosWrapper_hasSurfaceBeenRecreated_m3379 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::updateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_updateQCAR_m3380 (Object_t * __this/* static, unused */, IntPtr_t121 ___imageHeaderDataArray, int32_t ___imageHeaderArrayLength, IntPtr_t121 ___frameIndex, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::rendererEnd()
 void QCARNativeIosWrapper_rendererEnd_m3381 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarGetBufferSize(System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_qcarGetBufferSize_m3382 (Object_t * __this/* static, unused */, int32_t ___width, int32_t ___height, int32_t ___format, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::qcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 void QCARNativeIosWrapper_qcarAddCameraFrame_m3383 (Object_t * __this/* static, unused */, IntPtr_t121 ___pixels, int32_t ___width, int32_t ___height, int32_t ___format, int32_t ___stride, int32_t ___frameIdx, int32_t ___flipHorizontally, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::rendererSetVideoBackgroundCfg(System.IntPtr)
 void QCARNativeIosWrapper_rendererSetVideoBackgroundCfg_m3384 (Object_t * __this/* static, unused */, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::rendererGetVideoBackgroundCfg(System.IntPtr)
 void QCARNativeIosWrapper_rendererGetVideoBackgroundCfg_m3385 (Object_t * __this/* static, unused */, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::rendererGetVideoBackgroundTextureInfo(System.IntPtr)
 void QCARNativeIosWrapper_rendererGetVideoBackgroundTextureInfo_m3386 (Object_t * __this/* static, unused */, IntPtr_t121 ___texInfo, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::rendererSetVideoBackgroundTextureID(System.Int32)
 int32_t QCARNativeIosWrapper_rendererSetVideoBackgroundTextureID_m3387 (Object_t * __this/* static, unused */, int32_t ___textureID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::rendererIsVideoBackgroundTextureInfoAvailable()
 int32_t QCARNativeIosWrapper_rendererIsVideoBackgroundTextureInfoAvailable_m3388 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarInit(System.String)
 int32_t QCARNativeIosWrapper_qcarInit_m3389 (Object_t * __this/* static, unused */, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarSetHint(System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_qcarSetHint_m3390 (Object_t * __this/* static, unused */, int32_t ___hint, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::getProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_getProjectionGL_m3391 (Object_t * __this/* static, unused */, float ___nearClip, float ___farClip, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::setApplicationEnvironment(System.Int32,System.Int32,System.Int32)
 void QCARNativeIosWrapper_setApplicationEnvironment_m3392 (Object_t * __this/* static, unused */, int32_t ___unityVersionMajor, int32_t ___unityVersionMinor, int32_t ___unityVersionChange, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::setStateBufferSize(System.Int32)
 void QCARNativeIosWrapper_setStateBufferSize_m3393 (Object_t * __this/* static, unused */, int32_t ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderStartInit(System.String,System.String)
 int32_t QCARNativeIosWrapper_targetFinderStartInit_m3394 (Object_t * __this/* static, unused */, String_t* ___userKey, String_t* ___secretKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderGetInitState()
 int32_t QCARNativeIosWrapper_targetFinderGetInitState_m3395 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderDeinit()
 int32_t QCARNativeIosWrapper_targetFinderDeinit_m3396 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderStartRecognition()
 int32_t QCARNativeIosWrapper_targetFinderStartRecognition_m3397 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderStop()
 int32_t QCARNativeIosWrapper_targetFinderStop_m3398 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderSetUIScanlineColor(System.Single,System.Single,System.Single)
 void QCARNativeIosWrapper_targetFinderSetUIScanlineColor_m3399 (Object_t * __this/* static, unused */, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderSetUIPointColor(System.Single,System.Single,System.Single)
 void QCARNativeIosWrapper_targetFinderSetUIPointColor_m3400 (Object_t * __this/* static, unused */, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderUpdate(System.IntPtr)
 void QCARNativeIosWrapper_targetFinderUpdate_m3401 (Object_t * __this/* static, unused */, IntPtr_t121 ___targetFinderState, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderGetResults(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_targetFinderGetResults_m3402 (Object_t * __this/* static, unused */, IntPtr_t121 ___searchResultArray, int32_t ___searchResultArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::targetFinderEnableTracking(System.IntPtr,System.IntPtr)
 int32_t QCARNativeIosWrapper_targetFinderEnableTracking_m3403 (Object_t * __this/* static, unused */, IntPtr_t121 ___searchResult, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderGetImageTargets(System.IntPtr,System.Int32)
 void QCARNativeIosWrapper_targetFinderGetImageTargets_m3404 (Object_t * __this/* static, unused */, IntPtr_t121 ___trackableIdArray, int32_t ___trackableIdArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::targetFinderClearTrackables()
 void QCARNativeIosWrapper_targetFinderClearTrackables_m3405 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::textTrackerStart()
 int32_t QCARNativeIosWrapper_textTrackerStart_m3406 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::textTrackerStop()
 void QCARNativeIosWrapper_textTrackerStop_m3407 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::textTrackerSetRegionOfInterest(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_textTrackerSetRegionOfInterest_m3408 (Object_t * __this/* static, unused */, int32_t ___detectionLeftTopX, int32_t ___detectionLeftTopY, int32_t ___detectionRightBottomX, int32_t ___detectionRightBottomY, int32_t ___trackingLeftTopX, int32_t ___trackingLeftTopY, int32_t ___trackingRightBottomX, int32_t ___trackingRightBottomY, int32_t ___upDirection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::textTrackerGetRegionOfInterest(System.IntPtr,System.IntPtr)
 int32_t QCARNativeIosWrapper_textTrackerGetRegionOfInterest_m3409 (Object_t * __this/* static, unused */, IntPtr_t121 ___detectionROI, IntPtr_t121 ___trackingROI, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListLoadWordList(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_wordListLoadWordList_m3410 (Object_t * __this/* static, unused */, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListAddWordsFromFile(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_wordListAddWordsFromFile_m3411 (Object_t * __this/* static, unused */, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListAddWordU(System.IntPtr)
 int32_t QCARNativeIosWrapper_wordListAddWordU_m3412 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListRemoveWordU(System.IntPtr)
 int32_t QCARNativeIosWrapper_wordListRemoveWordU_m3413 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListContainsWordU(System.IntPtr)
 int32_t QCARNativeIosWrapper_wordListContainsWordU_m3414 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListUnloadAllLists()
 int32_t QCARNativeIosWrapper_wordListUnloadAllLists_m3415 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListSetFilterMode(System.Int32)
 int32_t QCARNativeIosWrapper_wordListSetFilterMode_m3416 (Object_t * __this/* static, unused */, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListGetFilterMode()
 int32_t QCARNativeIosWrapper_wordListGetFilterMode_m3417 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListAddWordToFilterListU(System.IntPtr)
 int32_t QCARNativeIosWrapper_wordListAddWordToFilterListU_m3418 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListRemoveWordFromFilterListU(System.IntPtr)
 int32_t QCARNativeIosWrapper_wordListRemoveWordFromFilterListU_m3419 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListClearFilterList()
 int32_t QCARNativeIosWrapper_wordListClearFilterList_m3420 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListLoadFilterList(System.String,System.Int32)
 int32_t QCARNativeIosWrapper_wordListLoadFilterList_m3421 (Object_t * __this/* static, unused */, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordListGetFilterListWordCount()
 int32_t QCARNativeIosWrapper_wordListGetFilterListWordCount_m3422 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::wordListGetFilterListWordU(System.Int32)
 IntPtr_t121 QCARNativeIosWrapper_wordListGetFilterListWordU_m3423 (Object_t * __this/* static, unused */, int32_t ___i, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordGetLetterMask(System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_wordGetLetterMask_m3424 (Object_t * __this/* static, unused */, int32_t ___wordID, IntPtr_t121 ___letterMaskImage, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::wordGetLetterBoundingBoxes(System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_wordGetLetterBoundingBoxes_m3425 (Object_t * __this/* static, unused */, int32_t ___wordID, IntPtr_t121 ___letterBoundingBoxes, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::trackerManagerInitTracker(System.Int32)
 int32_t QCARNativeIosWrapper_trackerManagerInitTracker_m3426 (Object_t * __this/* static, unused */, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::trackerManagerDeinitTracker(System.Int32)
 int32_t QCARNativeIosWrapper_trackerManagerDeinitTracker_m3427 (Object_t * __this/* static, unused */, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::virtualButtonSetEnabled(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNativeIosWrapper_virtualButtonSetEnabled_m3428 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::virtualButtonSetSensitivity(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNativeIosWrapper_virtualButtonSetSensitivity_m3429 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___sensitivity, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::virtualButtonSetAreaRectangle(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNativeIosWrapper_virtualButtonSetAreaRectangle_m3430 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::qcarDeinit()
 int32_t QCARNativeIosWrapper_qcarDeinit_m3431 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::startExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_startExtendedTracking_m3432 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::stopExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_stopExtendedTracking_m3433 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsSupportedDeviceDetected()
 int32_t QCARNativeIosWrapper_eyewearIsSupportedDeviceDetected_m3434 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsSeeThru()
 int32_t QCARNativeIosWrapper_eyewearIsSeeThru_m3435 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearGetScreenOrientation()
 int32_t QCARNativeIosWrapper_eyewearGetScreenOrientation_m3436 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsStereoCapable()
 int32_t QCARNativeIosWrapper_eyewearIsStereoCapable_m3437 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsStereoEnabled()
 int32_t QCARNativeIosWrapper_eyewearIsStereoEnabled_m3438 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearIsStereoGLOnly()
 int32_t QCARNativeIosWrapper_eyewearIsStereoGLOnly_m3439 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearSetStereo(System.Boolean)
 int32_t QCARNativeIosWrapper_eyewearSetStereo_m3440 (Object_t * __this/* static, unused */, bool ___enable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearGetDefaultSceneScale(System.IntPtr)
 int32_t QCARNativeIosWrapper_eyewearGetDefaultSceneScale_m3441 (Object_t * __this/* static, unused */, IntPtr_t121 ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNativeIosWrapper_eyewearGetProjectionMatrix_m3442 (Object_t * __this/* static, unused */, int32_t ___eyeID, int32_t ___profileID, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMGetMaxCount()
 int32_t QCARNativeIosWrapper_eyewearCPMGetMaxCount_m3443 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMGetUsedCount()
 int32_t QCARNativeIosWrapper_eyewearCPMGetUsedCount_m3444 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMIsProfileUsed(System.Int32)
 int32_t QCARNativeIosWrapper_eyewearCPMIsProfileUsed_m3445 (Object_t * __this/* static, unused */, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMGetActiveProfile()
 int32_t QCARNativeIosWrapper_eyewearCPMGetActiveProfile_m3446 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMSetActiveProfile(System.Int32)
 int32_t QCARNativeIosWrapper_eyewearCPMSetActiveProfile_m3447 (Object_t * __this/* static, unused */, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_eyewearCPMGetProjectionMatrix_m3448 (Object_t * __this/* static, unused */, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMSetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_eyewearCPMSetProjectionMatrix_m3449 (Object_t * __this/* static, unused */, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeIosWrapper::eyewearCPMGetProfileName(System.Int32)
 IntPtr_t121 QCARNativeIosWrapper_eyewearCPMGetProfileName_m3450 (Object_t * __this/* static, unused */, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMSetProfileName(System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_eyewearCPMSetProfileName_m3451 (Object_t * __this/* static, unused */, int32_t ___profileID, IntPtr_t121 ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearCPMClearProfile(System.Int32)
 int32_t QCARNativeIosWrapper_eyewearCPMClearProfile_m3452 (Object_t * __this/* static, unused */, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorInit(System.Int32,System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeIosWrapper_eyewearUserCalibratorInit_m3453 (Object_t * __this/* static, unused */, int32_t ___surfaceWidth, int32_t ___surfaceHeight, int32_t ___targetWidth, int32_t ___targetHeight, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorGetMinScaleHint()
 float QCARNativeIosWrapper_eyewearUserCalibratorGetMinScaleHint_m3454 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorGetMaxScaleHint()
 float QCARNativeIosWrapper_eyewearUserCalibratorGetMaxScaleHint_m3455 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorIsStereoStretched()
 int32_t QCARNativeIosWrapper_eyewearUserCalibratorIsStereoStretched_m3456 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeIosWrapper::eyewearUserCalibratorGetProjectionMatrix(System.IntPtr,System.Int32,System.IntPtr)
 int32_t QCARNativeIosWrapper_eyewearUserCalibratorGetProjectionMatrix_m3457 (Object_t * __this/* static, unused */, IntPtr_t121 ___readingsArray, int32_t ___numReadings, IntPtr_t121 ___calibrationResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeIosWrapper::.ctor()
 void QCARNativeIosWrapper__ctor_m3458 (QCARNativeIosWrapper_t705 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
