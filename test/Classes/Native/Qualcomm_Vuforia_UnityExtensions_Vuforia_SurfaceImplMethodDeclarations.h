﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SurfaceImpl
struct SurfaceImpl_t668;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t581;
// UnityEngine.Mesh
struct Mesh_t174;
// System.Int32[]
struct Int32U5BU5D_t21;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void Vuforia.SurfaceImpl::.ctor(System.Int32,Vuforia.SmartTerrainTrackable)
 void SurfaceImpl__ctor_m3043 (SurfaceImpl_t668 * __this, int32_t ___id, Object_t * ___parent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceImpl::SetID(System.Int32)
 void SurfaceImpl_SetID_m3044 (SurfaceImpl_t668 * __this, int32_t ___trackableID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceImpl::SetMesh(System.Int32,UnityEngine.Mesh,UnityEngine.Mesh,System.Int32[])
 void SurfaceImpl_SetMesh_m3045 (SurfaceImpl_t668 * __this, int32_t ___meshRev, Mesh_t174 * ___mesh, Mesh_t174 * ___navMesh, Int32U5BU5D_t21* ___meshBoundaries, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceImpl::SetBoundingBox(UnityEngine.Rect)
 void SurfaceImpl_SetBoundingBox_m3046 (SurfaceImpl_t668 * __this, Rect_t118  ___boundingBox, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh Vuforia.SurfaceImpl::GetNavMesh()
 Mesh_t174 * SurfaceImpl_GetNavMesh_m3047 (SurfaceImpl_t668 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Vuforia.SurfaceImpl::GetMeshBoundaries()
 Int32U5BU5D_t21* SurfaceImpl_GetMeshBoundaries_m3048 (SurfaceImpl_t668 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Vuforia.SurfaceImpl::get_BoundingBox()
 Rect_t118  SurfaceImpl_get_BoundingBox_m3049 (SurfaceImpl_t668 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.SurfaceImpl::GetArea()
 float SurfaceImpl_GetArea_m3050 (SurfaceImpl_t668 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
