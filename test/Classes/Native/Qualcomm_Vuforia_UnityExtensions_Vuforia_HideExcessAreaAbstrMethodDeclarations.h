﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.HideExcessAreaAbstractBehaviour
struct HideExcessAreaAbstractBehaviour_t48;
// UnityEngine.GameObject
struct GameObject_t2;
// System.String
struct String_t;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// UnityEngine.GameObject Vuforia.HideExcessAreaAbstractBehaviour::CreateQuad(UnityEngine.GameObject,System.String,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.Int32)
 GameObject_t2 * HideExcessAreaAbstractBehaviour_CreateQuad_m2681 (HideExcessAreaAbstractBehaviour_t48 * __this, GameObject_t2 * ___parent, String_t* ___name, Vector3_t13  ___position, Quaternion_t12  ___rotation, Vector3_t13  ___scale, int32_t ___layer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::SetPlanesActive(System.Boolean)
 void HideExcessAreaAbstractBehaviour_SetPlanesActive_m2682 (HideExcessAreaAbstractBehaviour_t48 * __this, bool ___activeflag, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnQCARStarted()
 void HideExcessAreaAbstractBehaviour_OnQCARStarted_m2683 (HideExcessAreaAbstractBehaviour_t48 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::HasCalculationDataChanged()
 bool HideExcessAreaAbstractBehaviour_HasCalculationDataChanged_m2684 (HideExcessAreaAbstractBehaviour_t48 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Start()
 void HideExcessAreaAbstractBehaviour_Start_m2685 (HideExcessAreaAbstractBehaviour_t48 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::OnDestroy()
 void HideExcessAreaAbstractBehaviour_OnDestroy_m2686 (HideExcessAreaAbstractBehaviour_t48 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::Update()
 void HideExcessAreaAbstractBehaviour_Update_m2687 (HideExcessAreaAbstractBehaviour_t48 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.HideExcessAreaAbstractBehaviour::.ctor()
 void HideExcessAreaAbstractBehaviour__ctor_m420 (HideExcessAreaAbstractBehaviour_t48 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
