﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>
struct Enumerator_t3919;
// System.Object
struct Object_t;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t581;
// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
struct List_1_t665;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m22567(__this, ___l, method) (void)Enumerator__ctor_m14640_gshared((Enumerator_t2850 *)__this, (List_1_t150 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22568(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14641_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::Dispose()
#define Enumerator_Dispose_m22569(__this, method) (void)Enumerator_Dispose_m14642_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::VerifyState()
#define Enumerator_VerifyState_m22570(__this, method) (void)Enumerator_VerifyState_m14643_gshared((Enumerator_t2850 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::MoveNext()
#define Enumerator_MoveNext_m22571(__this, method) (bool)Enumerator_MoveNext_m14644_gshared((Enumerator_t2850 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::get_Current()
#define Enumerator_get_Current_m22572(__this, method) (Object_t *)Enumerator_get_Current_m14645_gshared((Enumerator_t2850 *)__this, method)
