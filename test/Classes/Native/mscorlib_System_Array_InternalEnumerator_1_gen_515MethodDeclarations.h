﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Category>
struct InternalEnumerator_1_t4893;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Text.RegularExpressions.Category
#include "System_System_Text_RegularExpressions_Category.h"

// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Category>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30047 (InternalEnumerator_1_t4893 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Category>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30048 (InternalEnumerator_1_t4893 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Category>::Dispose()
 void InternalEnumerator_1_Dispose_m30049 (InternalEnumerator_1_t4893 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Category>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30050 (InternalEnumerator_1_t4893 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Category>::get_Current()
 uint16_t InternalEnumerator_1_get_Current_m30051 (InternalEnumerator_1_t4893 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
