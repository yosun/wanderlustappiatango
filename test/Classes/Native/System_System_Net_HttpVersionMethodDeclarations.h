﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.HttpVersion
struct HttpVersion_t1334;

// System.Void System.Net.HttpVersion::.cctor()
 void HttpVersion__cctor_m6822 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
