﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Button>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_56.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Button>
struct CachedInvokableCall_1_t3216  : public InvokableCall_1_t3217
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Button>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
