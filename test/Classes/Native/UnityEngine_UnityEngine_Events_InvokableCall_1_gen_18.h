﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>
struct UnityAction_1_t2823;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>
struct InvokableCall_1_t2822  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::Delegate
	UnityAction_1_t2823 * ___Delegate_0;
};
