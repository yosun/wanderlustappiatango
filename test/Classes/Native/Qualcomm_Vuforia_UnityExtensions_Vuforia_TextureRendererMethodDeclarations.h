﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TextureRenderer
struct TextureRenderer_t741;
// UnityEngine.Texture
struct Texture_t294;
// UnityEngine.RenderTexture
struct RenderTexture_t742;
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"

// System.Int32 Vuforia.TextureRenderer::get_Width()
 int32_t TextureRenderer_get_Width_m4035 (TextureRenderer_t741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.TextureRenderer::get_Height()
 int32_t TextureRenderer_get_Height_m4036 (TextureRenderer_t741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TextureRenderer::.ctor(UnityEngine.Texture,System.Int32,Vuforia.QCARRenderer/Vec2I)
 void TextureRenderer__ctor_m4037 (TextureRenderer_t741 * __this, Texture_t294 * ___textureToRender, int32_t ___renderTextureLayer, Vec2I_t630  ___requestedTextureSize, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture Vuforia.TextureRenderer::Render()
 RenderTexture_t742 * TextureRenderer_Render_m4038 (TextureRenderer_t741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TextureRenderer::Destroy()
 void TextureRenderer_Destroy_m4039 (TextureRenderer_t741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
