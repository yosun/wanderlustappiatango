﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyInformationalVersionAttribute
struct AssemblyInformationalVersionAttribute_t1280;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyInformationalVersionAttribute::.ctor(System.String)
 void AssemblyInformationalVersionAttribute__ctor_m6710 (AssemblyInformationalVersionAttribute_t1280 * __this, String_t* ___informationalVersion, MethodInfo* method) IL2CPP_METHOD_ATTR;
