﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffAbstractBehaviour>
struct CachedInvokableCall_1_t4361;
// UnityEngine.Object
struct Object_t117;
struct Object_t117_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t142;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t57;
// System.Object[]
struct ObjectU5BU5D_t115;

// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffAbstractBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_4MethodDeclarations.h"
#define CachedInvokableCall_1__ctor_m26709(__this, ___target, ___theFunction, ___argument, method) (void)CachedInvokableCall_1__ctor_m14008_gshared((CachedInvokableCall_1_t2705 *)__this, (Object_t117 *)___target, (MethodInfo_t142 *)___theFunction, (Object_t *)___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffAbstractBehaviour>::Invoke(System.Object[])
#define CachedInvokableCall_1_Invoke_m26710(__this, ___args, method) (void)CachedInvokableCall_1_Invoke_m14010_gshared((CachedInvokableCall_1_t2705 *)__this, (ObjectU5BU5D_t115*)___args, method)
