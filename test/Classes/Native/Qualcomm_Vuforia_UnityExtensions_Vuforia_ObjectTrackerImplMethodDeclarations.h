﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ObjectTrackerImpl
struct ObjectTrackerImpl_t620;
// Vuforia.ImageTargetBuilder
struct ImageTargetBuilder_t601;
// Vuforia.TargetFinder
struct TargetFinder_t616;
// Vuforia.DataSet
struct DataSet_t568;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet>
struct IEnumerable_1_t617;

// Vuforia.ImageTargetBuilder Vuforia.ObjectTrackerImpl::get_ImageTargetBuilder()
 ImageTargetBuilder_t601 * ObjectTrackerImpl_get_ImageTargetBuilder_m2924 (ObjectTrackerImpl_t620 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TargetFinder Vuforia.ObjectTrackerImpl::get_TargetFinder()
 TargetFinder_t616 * ObjectTrackerImpl_get_TargetFinder_m2925 (ObjectTrackerImpl_t620 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTrackerImpl::.ctor()
 void ObjectTrackerImpl__ctor_m2926 (ObjectTrackerImpl_t620 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::Start()
 bool ObjectTrackerImpl_Start_m2927 (ObjectTrackerImpl_t620 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTrackerImpl::Stop()
 void ObjectTrackerImpl_Stop_m2928 (ObjectTrackerImpl_t620 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DataSet Vuforia.ObjectTrackerImpl::CreateDataSet()
 DataSet_t568 * ObjectTrackerImpl_CreateDataSet_m2929 (ObjectTrackerImpl_t620 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::DestroyDataSet(Vuforia.DataSet,System.Boolean)
 bool ObjectTrackerImpl_DestroyDataSet_m2930 (ObjectTrackerImpl_t620 * __this, DataSet_t568 * ___dataSet, bool ___destroyTrackables, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::ActivateDataSet(Vuforia.DataSet)
 bool ObjectTrackerImpl_ActivateDataSet_m2931 (ObjectTrackerImpl_t620 * __this, DataSet_t568 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::DeactivateDataSet(Vuforia.DataSet)
 bool ObjectTrackerImpl_DeactivateDataSet_m2932 (ObjectTrackerImpl_t620 * __this, DataSet_t568 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTrackerImpl::GetActiveDataSets()
 Object_t* ObjectTrackerImpl_GetActiveDataSets_m2933 (ObjectTrackerImpl_t620 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet> Vuforia.ObjectTrackerImpl::GetDataSets()
 Object_t* ObjectTrackerImpl_GetDataSets_m2934 (ObjectTrackerImpl_t620 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTrackerImpl::DestroyAllDataSets(System.Boolean)
 void ObjectTrackerImpl_DestroyAllDataSets_m2935 (ObjectTrackerImpl_t620 * __this, bool ___destroyTrackables, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::PersistExtendedTracking(System.Boolean)
 bool ObjectTrackerImpl_PersistExtendedTracking_m2936 (ObjectTrackerImpl_t620 * __this, bool ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTrackerImpl::ResetExtendedTracking()
 bool ObjectTrackerImpl_ResetExtendedTracking_m2937 (ObjectTrackerImpl_t620 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
