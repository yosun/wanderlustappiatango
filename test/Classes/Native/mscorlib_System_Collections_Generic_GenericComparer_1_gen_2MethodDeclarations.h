﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct GenericComparer_1_t2684;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
 void GenericComparer_1__ctor_m13958 (GenericComparer_1_t2684 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
 int32_t GenericComparer_1_Compare_m31408 (GenericComparer_1_t2684 * __this, TimeSpan_t113  ___x, TimeSpan_t113  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
