﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>
struct KeyValuePair_2_t4161;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t44;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m24999 (KeyValuePair_2_t4161 * __this, int32_t ___key, TrackableBehaviour_t44 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::get_Key()
 int32_t KeyValuePair_2_get_Key_m25000 (KeyValuePair_2_t4161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m25001 (KeyValuePair_2_t4161 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::get_Value()
 TrackableBehaviour_t44 * KeyValuePair_2_get_Value_m25002 (KeyValuePair_2_t4161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m25003 (KeyValuePair_2_t4161 * __this, TrackableBehaviour_t44 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>::ToString()
 String_t* KeyValuePair_2_ToString_m25004 (KeyValuePair_2_t4161 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
