﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIStyleState
struct GUIStyleState_t971;
// UnityEngine.GUIStyle
struct GUIStyle_t953;
// UnityEngine.Texture2D
struct Texture2D_t285;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.GUIStyleState::.ctor()
 void GUIStyleState__ctor_m5711 (GUIStyleState_t971 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::.ctor(UnityEngine.GUIStyle,System.IntPtr)
 void GUIStyleState__ctor_m5712 (GUIStyleState_t971 * __this, GUIStyle_t953 * ___sourceStyle, IntPtr_t121 ___source, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::Finalize()
 void GUIStyleState_Finalize_m5713 (GUIStyleState_t971 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::Init()
 void GUIStyleState_Init_m5714 (GUIStyleState_t971 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::Cleanup()
 void GUIStyleState_Cleanup_m5715 (GUIStyleState_t971 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D UnityEngine.GUIStyleState::GetBackgroundInternal()
 Texture2D_t285 * GUIStyleState_GetBackgroundInternal_m5716 (GUIStyleState_t971 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::INTERNAL_set_textColor(UnityEngine.Color&)
 void GUIStyleState_INTERNAL_set_textColor_m5717 (GUIStyleState_t971 * __this, Color_t19 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIStyleState::set_textColor(UnityEngine.Color)
 void GUIStyleState_set_textColor_m5718 (GUIStyleState_t971 * __this, Color_t19  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
