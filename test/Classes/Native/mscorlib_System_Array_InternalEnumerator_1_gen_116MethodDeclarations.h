﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>
struct InternalEnumerator_1_t2965;
// System.Object
struct Object_t;
// Vuforia.VideoTextureRenderer
struct VideoTextureRenderer_t85;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15246(__this, ___array, method) (void)InternalEnumerator_1__ctor_m13978_gshared((InternalEnumerator_1_t2698 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15247(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::Dispose()
#define InternalEnumerator_1_Dispose_m15248(__this, method) (void)InternalEnumerator_1_Dispose_m13982_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15249(__this, method) (bool)InternalEnumerator_1_MoveNext_m13984_gshared((InternalEnumerator_1_t2698 *)__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.VideoTextureRenderer>::get_Current()
#define InternalEnumerator_1_get_Current_m15250(__this, method) (VideoTextureRenderer_t85 *)InternalEnumerator_1_get_Current_m13986_gshared((InternalEnumerator_1_t2698 *)__this, method)
