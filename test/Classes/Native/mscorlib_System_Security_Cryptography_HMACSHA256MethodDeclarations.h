﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACSHA256
struct HMACSHA256_t2098;
// System.Byte[]
struct ByteU5BU5D_t609;

// System.Void System.Security.Cryptography.HMACSHA256::.ctor()
 void HMACSHA256__ctor_m11921 (HMACSHA256_t2098 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA256::.ctor(System.Byte[])
 void HMACSHA256__ctor_m11922 (HMACSHA256_t2098 * __this, ByteU5BU5D_t609* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
