﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_35.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>
struct CachedInvokableCall_1_t2952  : public InvokableCall_1_t2953
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.TurnOffWordBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
