﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.CanvasRenderer>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_155.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.CanvasRenderer>
struct CachedInvokableCall_1_t4721  : public InvokableCall_1_t4722
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.CanvasRenderer>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
