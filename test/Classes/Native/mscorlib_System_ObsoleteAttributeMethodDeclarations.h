﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ObsoleteAttribute
struct ObsoleteAttribute_t443;
// System.String
struct String_t;

// System.Void System.ObsoleteAttribute::.ctor()
 void ObsoleteAttribute__ctor_m7966 (ObsoleteAttribute_t443 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor(System.String)
 void ObsoleteAttribute__ctor_m4547 (ObsoleteAttribute_t443 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor(System.String,System.Boolean)
 void ObsoleteAttribute__ctor_m1999 (ObsoleteAttribute_t443 * __this, String_t* ___message, bool ___error, MethodInfo* method) IL2CPP_METHOD_ATTR;
