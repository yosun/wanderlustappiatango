﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// System.Object[]
// System.Object[]
struct ObjectU5BU5D_t115  : public Array_t
{
};
// System.Enum[]
// System.Enum[]
struct EnumU5BU5D_t5201  : public Array_t
{
};
struct EnumU5BU5D_t5201_StaticFields{
};
// System.IFormattable[]
// System.IFormattable[]
struct IFormattableU5BU5D_t5202  : public Array_t
{
};
// System.IConvertible[]
// System.IConvertible[]
struct IConvertibleU5BU5D_t5203  : public Array_t
{
};
// System.IComparable[]
// System.IComparable[]
struct IComparableU5BU5D_t5204  : public Array_t
{
};
// System.ValueType[]
// System.ValueType[]
struct ValueTypeU5BU5D_t5205  : public Array_t
{
};
// System.String[]
// System.String[]
struct StringU5BU5D_t112  : public Array_t
{
};
struct StringU5BU5D_t112_StaticFields{
};
// System.Collections.IEnumerable[]
// System.Collections.IEnumerable[]
struct IEnumerableU5BU5D_t5206  : public Array_t
{
};
// System.ICloneable[]
// System.ICloneable[]
struct ICloneableU5BU5D_t5207  : public Array_t
{
};
// System.IComparable`1<System.String>[]
// System.IComparable`1<System.String>[]
struct IComparable_1U5BU5D_t5208  : public Array_t
{
};
// System.IEquatable`1<System.String>[]
// System.IEquatable`1<System.String>[]
struct IEquatable_1U5BU5D_t5209  : public Array_t
{
};
// System.Char[]
// System.Char[]
struct CharU5BU5D_t108  : public Array_t
{
};
struct CharU5BU5D_t108_StaticFields{
};
// System.IComparable`1<System.Char>[]
// System.IComparable`1<System.Char>[]
struct IComparable_1U5BU5D_t5210  : public Array_t
{
};
// System.IEquatable`1<System.Char>[]
// System.IEquatable`1<System.Char>[]
struct IEquatable_1U5BU5D_t5211  : public Array_t
{
};
// System.Int32[]
// System.Int32[]
struct Int32U5BU5D_t21  : public Array_t
{
};
// System.IComparable`1<System.Int32>[]
// System.IComparable`1<System.Int32>[]
struct IComparable_1U5BU5D_t5212  : public Array_t
{
};
// System.IEquatable`1<System.Int32>[]
// System.IEquatable`1<System.Int32>[]
struct IEquatable_1U5BU5D_t5213  : public Array_t
{
};
// System.Type[]
// System.Type[]
struct TypeU5BU5D_t878  : public Array_t
{
};
struct TypeU5BU5D_t878_StaticFields{
};
// System.Reflection.IReflect[]
// System.Reflection.IReflect[]
struct IReflectU5BU5D_t5214  : public Array_t
{
};
// System.Runtime.InteropServices._Type[]
// System.Runtime.InteropServices._Type[]
struct _TypeU5BU5D_t5215  : public Array_t
{
};
// System.Reflection.MemberInfo[]
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t2062  : public Array_t
{
};
// System.Reflection.ICustomAttributeProvider[]
// System.Reflection.ICustomAttributeProvider[]
struct ICustomAttributeProviderU5BU5D_t5216  : public Array_t
{
};
// System.Runtime.InteropServices._MemberInfo[]
// System.Runtime.InteropServices._MemberInfo[]
struct _MemberInfoU5BU5D_t5217  : public Array_t
{
};
// System.Double[]
// System.Double[]
struct DoubleU5BU5D_t1699  : public Array_t
{
};
// System.IComparable`1<System.Double>[]
// System.IComparable`1<System.Double>[]
struct IComparable_1U5BU5D_t5218  : public Array_t
{
};
// System.IEquatable`1<System.Double>[]
// System.IEquatable`1<System.Double>[]
struct IEquatable_1U5BU5D_t5219  : public Array_t
{
};
// System.Reflection.MethodInfo[]
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t141  : public Array_t
{
};
// System.Runtime.InteropServices._MethodInfo[]
// System.Runtime.InteropServices._MethodInfo[]
struct _MethodInfoU5BU5D_t5220  : public Array_t
{
};
// System.Reflection.MethodBase[]
// System.Reflection.MethodBase[]
struct MethodBaseU5BU5D_t1922  : public Array_t
{
};
// System.Runtime.InteropServices._MethodBase[]
// System.Runtime.InteropServices._MethodBase[]
struct _MethodBaseU5BU5D_t5221  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
struct List_1U5BU5D_t3037  : public Array_t
{
};
struct List_1U5BU5D_t3037_StaticFields{
};
// System.Collections.ICollection[]
// System.Collections.ICollection[]
struct ICollectionU5BU5D_t5222  : public Array_t
{
};
// System.Collections.IList[]
// System.Collections.IList[]
struct IListU5BU5D_t5223  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
struct KeyValuePair_2U5BU5D_t3155  : public Array_t
{
};
// System.Collections.Generic.Link[]
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t2553  : public Array_t
{
};
// System.Collections.DictionaryEntry[]
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t5224  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3229  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3255  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t3276  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
struct KeyValuePair_2U5BU5D_t3272  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
struct List_1U5BU5D_t3267  : public Array_t
{
};
struct List_1U5BU5D_t3267_StaticFields{
};
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
struct List_1U5BU5D_t3330  : public Array_t
{
};
struct List_1U5BU5D_t3330_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3392  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
struct KeyValuePair_2U5BU5D_t3384  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
struct List_1U5BU5D_t3619  : public Array_t
{
};
struct List_1U5BU5D_t3619_StaticFields{
};
// System.Collections.Generic.List`1<UnityEngine.Component>[]
// System.Collections.Generic.List`1<UnityEngine.Component>[]
struct List_1U5BU5D_t3624  : public Array_t
{
};
struct List_1U5BU5D_t3624_StaticFields{
};
// System.Attribute[]
// System.Attribute[]
struct AttributeU5BU5D_t5225  : public Array_t
{
};
// System.Runtime.InteropServices._Attribute[]
// System.Runtime.InteropServices._Attribute[]
struct _AttributeU5BU5D_t5226  : public Array_t
{
};
// System.Single[]
// System.Single[]
struct SingleU5BU5D_t578  : public Array_t
{
};
// System.IComparable`1<System.Single>[]
// System.IComparable`1<System.Single>[]
struct IComparable_1U5BU5D_t5227  : public Array_t
{
};
// System.IEquatable`1<System.Single>[]
// System.IEquatable`1<System.Single>[]
struct IEquatable_1U5BU5D_t5228  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>[]
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>[]
struct KeyValuePair_2U5BU5D_t3770  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>[]
struct KeyValuePair_2U5BU5D_t3801  : public Array_t
{
};
// System.Byte[]
// System.Byte[]
struct ByteU5BU5D_t609  : public Array_t
{
};
// System.IComparable`1<System.Byte>[]
// System.IComparable`1<System.Byte>[]
struct IComparable_1U5BU5D_t5229  : public Array_t
{
};
// System.IEquatable`1<System.Byte>[]
// System.IEquatable`1<System.Byte>[]
struct IEquatable_1U5BU5D_t5230  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>[]
struct KeyValuePair_2U5BU5D_t3831  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>[]
struct KeyValuePair_2U5BU5D_t3873  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
struct KeyValuePair_2U5BU5D_t3940  : public Array_t
{
};
// System.UInt16[]
// System.UInt16[]
struct UInt16U5BU5D_t1337  : public Array_t
{
};
// System.IComparable`1<System.UInt16>[]
// System.IComparable`1<System.UInt16>[]
struct IComparable_1U5BU5D_t5231  : public Array_t
{
};
// System.IEquatable`1<System.UInt16>[]
// System.IEquatable`1<System.UInt16>[]
struct IEquatable_1U5BU5D_t5232  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>[]
struct KeyValuePair_2U5BU5D_t3962  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t3998  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
struct KeyValuePair_2U5BU5D_t4018  : public Array_t
{
};
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
struct List_1U5BU5D_t4016  : public Array_t
{
};
struct List_1U5BU5D_t4016_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
struct KeyValuePair_2U5BU5D_t4080  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t4095  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
struct KeyValuePair_2U5BU5D_t4110  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t4124  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>[]
struct KeyValuePair_2U5BU5D_t4160  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>[]
struct KeyValuePair_2U5BU5D_t4174  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>[]
struct KeyValuePair_2U5BU5D_t4192  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>[]
struct KeyValuePair_2U5BU5D_t4234  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
struct KeyValuePair_2U5BU5D_t4267  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>[]
struct KeyValuePair_2U5BU5D_t4287  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
struct KeyValuePair_2U5BU5D_t4507  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
struct KeyValuePair_2U5BU5D_t4544  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct KeyValuePair_2U5BU5D_t4562  : public Array_t
{
};
// System.IntPtr[]
// System.IntPtr[]
struct IntPtrU5BU5D_t996  : public Array_t
{
};
struct IntPtrU5BU5D_t996_StaticFields{
};
// System.Runtime.Serialization.ISerializable[]
// System.Runtime.Serialization.ISerializable[]
struct ISerializableU5BU5D_t5233  : public Array_t
{
};
// System.Reflection.ParameterModifier[]
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t1163  : public Array_t
{
};
// System.Reflection.ParameterInfo[]
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t1168  : public Array_t
{
};
// System.Runtime.InteropServices._ParameterInfo[]
// System.Runtime.InteropServices._ParameterInfo[]
struct _ParameterInfoU5BU5D_t5234  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct KeyValuePair_2U5BU5D_t4765  : public Array_t
{
};
// System.Boolean[]
// System.Boolean[]
struct BooleanU5BU5D_t1345  : public Array_t
{
};
struct BooleanU5BU5D_t1345_StaticFields{
};
// System.IComparable`1<System.Boolean>[]
// System.IComparable`1<System.Boolean>[]
struct IComparable_1U5BU5D_t5235  : public Array_t
{
};
// System.IEquatable`1<System.Boolean>[]
// System.IEquatable`1<System.Boolean>[]
struct IEquatable_1U5BU5D_t5236  : public Array_t
{
};
// System.IO.FileAccess[]
// System.IO.FileAccess[]
struct FileAccessU5BU5D_t5237  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t4856  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509Certificate[]
// System.Security.Cryptography.X509Certificates.X509Certificate[]
struct X509CertificateU5BU5D_t1370  : public Array_t
{
};
// System.Runtime.Serialization.IDeserializationCallback[]
// System.Runtime.Serialization.IDeserializationCallback[]
struct IDeserializationCallbackU5BU5D_t5238  : public Array_t
{
};
// System.UInt32[]
// System.UInt32[]
struct UInt32U5BU5D_t1541  : public Array_t
{
};
// System.IComparable`1<System.UInt32>[]
// System.IComparable`1<System.UInt32>[]
struct IComparable_1U5BU5D_t5239  : public Array_t
{
};
// System.IEquatable`1<System.UInt32>[]
// System.IEquatable`1<System.UInt32>[]
struct IEquatable_1U5BU5D_t5240  : public Array_t
{
};
// System.Security.Cryptography.KeySizes[]
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t1565  : public Array_t
{
};
// System.Byte[][]
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t1674  : public Array_t
{
};
// System.SerializableAttribute[]
// System.SerializableAttribute[]
struct SerializableAttributeU5BU5D_t5241  : public Array_t
{
};
// System.AttributeUsageAttribute[]
// System.AttributeUsageAttribute[]
struct AttributeUsageAttributeU5BU5D_t5242  : public Array_t
{
};
// System.Runtime.InteropServices.ComVisibleAttribute[]
// System.Runtime.InteropServices.ComVisibleAttribute[]
struct ComVisibleAttributeU5BU5D_t5243  : public Array_t
{
};
// System.Int64[]
// System.Int64[]
struct Int64U5BU5D_t1698  : public Array_t
{
};
// System.IComparable`1<System.Int64>[]
// System.IComparable`1<System.Int64>[]
struct IComparable_1U5BU5D_t5244  : public Array_t
{
};
// System.IEquatable`1<System.Int64>[]
// System.IEquatable`1<System.Int64>[]
struct IEquatable_1U5BU5D_t5245  : public Array_t
{
};
// System.CLSCompliantAttribute[]
// System.CLSCompliantAttribute[]
struct CLSCompliantAttributeU5BU5D_t5246  : public Array_t
{
};
// System.UInt64[]
// System.UInt64[]
struct UInt64U5BU5D_t2117  : public Array_t
{
};
// System.IComparable`1<System.UInt64>[]
// System.IComparable`1<System.UInt64>[]
struct IComparable_1U5BU5D_t5247  : public Array_t
{
};
// System.IEquatable`1<System.UInt64>[]
// System.IEquatable`1<System.UInt64>[]
struct IEquatable_1U5BU5D_t5248  : public Array_t
{
};
// System.SByte[]
// System.SByte[]
struct SByteU5BU5D_t2160  : public Array_t
{
};
// System.IComparable`1<System.SByte>[]
// System.IComparable`1<System.SByte>[]
struct IComparable_1U5BU5D_t5249  : public Array_t
{
};
// System.IEquatable`1<System.SByte>[]
// System.IEquatable`1<System.SByte>[]
struct IEquatable_1U5BU5D_t5250  : public Array_t
{
};
// System.Int16[]
// System.Int16[]
struct Int16U5BU5D_t2340  : public Array_t
{
};
// System.IComparable`1<System.Int16>[]
// System.IComparable`1<System.Int16>[]
struct IComparable_1U5BU5D_t5251  : public Array_t
{
};
// System.IEquatable`1<System.Int16>[]
// System.IEquatable`1<System.Int16>[]
struct IEquatable_1U5BU5D_t5252  : public Array_t
{
};
// System.Single[,]
// System.Single[,]
struct SingleU5BU2CU5D_t5253  : public Array_t
{
};
// System.UIntPtr[]
// System.UIntPtr[]
struct UIntPtrU5BU5D_t5254  : public Array_t
{
};
struct UIntPtrU5BU5D_t5254_StaticFields{
};
// System.Delegate[]
// System.Delegate[]
struct DelegateU5BU5D_t1691  : public Array_t
{
};
// System.FlagsAttribute[]
// System.FlagsAttribute[]
struct FlagsAttributeU5BU5D_t5255  : public Array_t
{
};
// System.Reflection.ConstructorInfo[]
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t1706  : public Array_t
{
};
struct ConstructorInfoU5BU5D_t1706_StaticFields{
};
// System.Runtime.InteropServices._ConstructorInfo[]
// System.Runtime.InteropServices._ConstructorInfo[]
struct _ConstructorInfoU5BU5D_t5256  : public Array_t
{
};
// System.MonoType[]
// System.MonoType[]
struct MonoTypeU5BU5D_t2644  : public Array_t
{
};
// System.ParamArrayAttribute[]
// System.ParamArrayAttribute[]
struct ParamArrayAttributeU5BU5D_t5257  : public Array_t
{
};
// System.Runtime.InteropServices.OutAttribute[]
// System.Runtime.InteropServices.OutAttribute[]
struct OutAttributeU5BU5D_t5258  : public Array_t
{
};
// System.ObsoleteAttribute[]
// System.ObsoleteAttribute[]
struct ObsoleteAttributeU5BU5D_t5259  : public Array_t
{
};
// System.Runtime.InteropServices.DllImportAttribute[]
// System.Runtime.InteropServices.DllImportAttribute[]
struct DllImportAttributeU5BU5D_t5260  : public Array_t
{
};
// System.Runtime.InteropServices.MarshalAsAttribute[]
// System.Runtime.InteropServices.MarshalAsAttribute[]
struct MarshalAsAttributeU5BU5D_t5261  : public Array_t
{
};
// System.Runtime.InteropServices.InAttribute[]
// System.Runtime.InteropServices.InAttribute[]
struct InAttributeU5BU5D_t5262  : public Array_t
{
};
// System.Runtime.InteropServices.GuidAttribute[]
// System.Runtime.InteropServices.GuidAttribute[]
struct GuidAttributeU5BU5D_t5263  : public Array_t
{
};
// System.Runtime.InteropServices.ComImportAttribute[]
// System.Runtime.InteropServices.ComImportAttribute[]
struct ComImportAttributeU5BU5D_t5264  : public Array_t
{
};
// System.Runtime.InteropServices.OptionalAttribute[]
// System.Runtime.InteropServices.OptionalAttribute[]
struct OptionalAttributeU5BU5D_t5265  : public Array_t
{
};
// System.Runtime.CompilerServices.CompilerGeneratedAttribute[]
// System.Runtime.CompilerServices.CompilerGeneratedAttribute[]
struct CompilerGeneratedAttributeU5BU5D_t5266  : public Array_t
{
};
// System.Runtime.CompilerServices.InternalsVisibleToAttribute[]
// System.Runtime.CompilerServices.InternalsVisibleToAttribute[]
struct InternalsVisibleToAttributeU5BU5D_t5267  : public Array_t
{
};
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute[]
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute[]
struct RuntimeCompatibilityAttributeU5BU5D_t5268  : public Array_t
{
};
// System.Diagnostics.DebuggerHiddenAttribute[]
// System.Diagnostics.DebuggerHiddenAttribute[]
struct DebuggerHiddenAttributeU5BU5D_t5269  : public Array_t
{
};
// System.Reflection.DefaultMemberAttribute[]
// System.Reflection.DefaultMemberAttribute[]
struct DefaultMemberAttributeU5BU5D_t5270  : public Array_t
{
};
// System.Runtime.CompilerServices.DecimalConstantAttribute[]
// System.Runtime.CompilerServices.DecimalConstantAttribute[]
struct DecimalConstantAttributeU5BU5D_t5271  : public Array_t
{
};
// System.Runtime.InteropServices.FieldOffsetAttribute[]
// System.Runtime.InteropServices.FieldOffsetAttribute[]
struct FieldOffsetAttributeU5BU5D_t5272  : public Array_t
{
};
// System.MonoTODOAttribute[]
// System.MonoTODOAttribute[]
struct MonoTODOAttributeU5BU5D_t5273  : public Array_t
{
};
// System.MonoDocumentationNoteAttribute[]
// System.MonoDocumentationNoteAttribute[]
struct MonoDocumentationNoteAttributeU5BU5D_t5274  : public Array_t
{
};
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct TableRangeU5BU5D_t1729  : public Array_t
{
};
// Mono.Globalization.Unicode.TailoringInfo[]
// Mono.Globalization.Unicode.TailoringInfo[]
struct TailoringInfoU5BU5D_t1736  : public Array_t
{
};
// Mono.Globalization.Unicode.Contraction[]
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t1738  : public Array_t
{
};
// Mono.Globalization.Unicode.Level2Map[]
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t1739  : public Array_t
{
};
// Mono.Globalization.Unicode.SimpleCollator/ExtenderType[]
// Mono.Globalization.Unicode.SimpleCollator/ExtenderType[]
struct ExtenderTypeU5BU5D_t5275  : public Array_t
{
};
// Mono.Math.Prime.ConfidenceFactor[]
// Mono.Math.Prime.ConfidenceFactor[]
struct ConfidenceFactorU5BU5D_t5276  : public Array_t
{
};
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct BigIntegerU5BU5D_t1758  : public Array_t
{
};
struct BigIntegerU5BU5D_t1758_StaticFields{
};
// Mono.Math.BigInteger/Sign[]
// Mono.Math.BigInteger/Sign[]
struct SignU5BU5D_t5277  : public Array_t
{
};
// System.Collections.Hashtable/Slot[]
// System.Collections.Hashtable/Slot[]
struct SlotU5BU5D_t1835  : public Array_t
{
};
// System.Collections.Hashtable/EnumeratorMode[]
// System.Collections.Hashtable/EnumeratorMode[]
struct EnumeratorModeU5BU5D_t5278  : public Array_t
{
};
// System.Collections.SortedList/Slot[]
// System.Collections.SortedList/Slot[]
struct SlotU5BU5D_t1839  : public Array_t
{
};
// System.Collections.SortedList/EnumeratorMode[]
// System.Collections.SortedList/EnumeratorMode[]
struct EnumeratorModeU5BU5D_t5279  : public Array_t
{
};
// System.Configuration.Assemblies.AssemblyHashAlgorithm[]
// System.Configuration.Assemblies.AssemblyHashAlgorithm[]
struct AssemblyHashAlgorithmU5BU5D_t5280  : public Array_t
{
};
// System.Configuration.Assemblies.AssemblyVersionCompatibility[]
// System.Configuration.Assemblies.AssemblyVersionCompatibility[]
struct AssemblyVersionCompatibilityU5BU5D_t5281  : public Array_t
{
};
// System.Diagnostics.DebuggableAttribute[]
// System.Diagnostics.DebuggableAttribute[]
struct DebuggableAttributeU5BU5D_t5282  : public Array_t
{
};
// System.Diagnostics.DebuggableAttribute/DebuggingModes[]
// System.Diagnostics.DebuggableAttribute/DebuggingModes[]
struct DebuggingModesU5BU5D_t5283  : public Array_t
{
};
// System.Diagnostics.DebuggerDisplayAttribute[]
// System.Diagnostics.DebuggerDisplayAttribute[]
struct DebuggerDisplayAttributeU5BU5D_t5284  : public Array_t
{
};
// System.Diagnostics.DebuggerStepThroughAttribute[]
// System.Diagnostics.DebuggerStepThroughAttribute[]
struct DebuggerStepThroughAttributeU5BU5D_t5285  : public Array_t
{
};
// System.Diagnostics.DebuggerTypeProxyAttribute[]
// System.Diagnostics.DebuggerTypeProxyAttribute[]
struct DebuggerTypeProxyAttributeU5BU5D_t5286  : public Array_t
{
};
// System.Diagnostics.StackFrame[]
// System.Diagnostics.StackFrame[]
struct StackFrameU5BU5D_t1847  : public Array_t
{
};
// System.Globalization.CompareOptions[]
// System.Globalization.CompareOptions[]
struct CompareOptionsU5BU5D_t5287  : public Array_t
{
};
// System.Globalization.Calendar[]
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t1853  : public Array_t
{
};
// System.Globalization.DateTimeFormatFlags[]
// System.Globalization.DateTimeFormatFlags[]
struct DateTimeFormatFlagsU5BU5D_t5288  : public Array_t
{
};
// System.Globalization.DateTimeStyles[]
// System.Globalization.DateTimeStyles[]
struct DateTimeStylesU5BU5D_t5289  : public Array_t
{
};
// System.Globalization.GregorianCalendarTypes[]
// System.Globalization.GregorianCalendarTypes[]
struct GregorianCalendarTypesU5BU5D_t5290  : public Array_t
{
};
// System.Globalization.NumberStyles[]
// System.Globalization.NumberStyles[]
struct NumberStylesU5BU5D_t5291  : public Array_t
{
};
// System.Globalization.UnicodeCategory[]
// System.Globalization.UnicodeCategory[]
struct UnicodeCategoryU5BU5D_t5292  : public Array_t
{
};
// System.IO.FileAttributes[]
// System.IO.FileAttributes[]
struct FileAttributesU5BU5D_t5293  : public Array_t
{
};
// System.IO.FileMode[]
// System.IO.FileMode[]
struct FileModeU5BU5D_t5294  : public Array_t
{
};
// System.IO.FileOptions[]
// System.IO.FileOptions[]
struct FileOptionsU5BU5D_t5295  : public Array_t
{
};
// System.IO.FileShare[]
// System.IO.FileShare[]
struct FileShareU5BU5D_t5296  : public Array_t
{
};
// System.IO.MonoFileType[]
// System.IO.MonoFileType[]
struct MonoFileTypeU5BU5D_t5297  : public Array_t
{
};
// System.IO.MonoIOError[]
// System.IO.MonoIOError[]
struct MonoIOErrorU5BU5D_t5298  : public Array_t
{
};
// System.IO.SeekOrigin[]
// System.IO.SeekOrigin[]
struct SeekOriginU5BU5D_t5299  : public Array_t
{
};
// System.Reflection.Emit.ModuleBuilder[]
// System.Reflection.Emit.ModuleBuilder[]
struct ModuleBuilderU5BU5D_t1896  : public Array_t
{
};
struct ModuleBuilderU5BU5D_t1896_StaticFields{
};
// System.Runtime.InteropServices._ModuleBuilder[]
// System.Runtime.InteropServices._ModuleBuilder[]
struct _ModuleBuilderU5BU5D_t5300  : public Array_t
{
};
// System.Reflection.Module[]
// System.Reflection.Module[]
struct ModuleU5BU5D_t1898  : public Array_t
{
};
struct ModuleU5BU5D_t1898_StaticFields{
};
// System.Runtime.InteropServices._Module[]
// System.Runtime.InteropServices._Module[]
struct _ModuleU5BU5D_t5301  : public Array_t
{
};
// System.Reflection.Emit.ParameterBuilder[]
// System.Reflection.Emit.ParameterBuilder[]
struct ParameterBuilderU5BU5D_t1901  : public Array_t
{
};
// System.Runtime.InteropServices._ParameterBuilder[]
// System.Runtime.InteropServices._ParameterBuilder[]
struct _ParameterBuilderU5BU5D_t5302  : public Array_t
{
};
// System.Reflection.Emit.GenericTypeParameterBuilder[]
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct GenericTypeParameterBuilderU5BU5D_t1909  : public Array_t
{
};
// System.Reflection.Emit.MethodBuilder[]
// System.Reflection.Emit.MethodBuilder[]
struct MethodBuilderU5BU5D_t1912  : public Array_t
{
};
// System.Runtime.InteropServices._MethodBuilder[]
// System.Runtime.InteropServices._MethodBuilder[]
struct _MethodBuilderU5BU5D_t5303  : public Array_t
{
};
// System.Reflection.Emit.ConstructorBuilder[]
// System.Reflection.Emit.ConstructorBuilder[]
struct ConstructorBuilderU5BU5D_t1913  : public Array_t
{
};
// System.Runtime.InteropServices._ConstructorBuilder[]
// System.Runtime.InteropServices._ConstructorBuilder[]
struct _ConstructorBuilderU5BU5D_t5304  : public Array_t
{
};
// System.Reflection.Emit.FieldBuilder[]
// System.Reflection.Emit.FieldBuilder[]
struct FieldBuilderU5BU5D_t1914  : public Array_t
{
};
// System.Runtime.InteropServices._FieldBuilder[]
// System.Runtime.InteropServices._FieldBuilder[]
struct _FieldBuilderU5BU5D_t5305  : public Array_t
{
};
// System.Reflection.FieldInfo[]
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t5306  : public Array_t
{
};
// System.Runtime.InteropServices._FieldInfo[]
// System.Runtime.InteropServices._FieldInfo[]
struct _FieldInfoU5BU5D_t5307  : public Array_t
{
};
// System.Reflection.AssemblyCompanyAttribute[]
// System.Reflection.AssemblyCompanyAttribute[]
struct AssemblyCompanyAttributeU5BU5D_t5308  : public Array_t
{
};
// System.Reflection.AssemblyConfigurationAttribute[]
// System.Reflection.AssemblyConfigurationAttribute[]
struct AssemblyConfigurationAttributeU5BU5D_t5309  : public Array_t
{
};
// System.Reflection.AssemblyCopyrightAttribute[]
// System.Reflection.AssemblyCopyrightAttribute[]
struct AssemblyCopyrightAttributeU5BU5D_t5310  : public Array_t
{
};
// System.Reflection.AssemblyDefaultAliasAttribute[]
// System.Reflection.AssemblyDefaultAliasAttribute[]
struct AssemblyDefaultAliasAttributeU5BU5D_t5311  : public Array_t
{
};
// System.Reflection.AssemblyDelaySignAttribute[]
// System.Reflection.AssemblyDelaySignAttribute[]
struct AssemblyDelaySignAttributeU5BU5D_t5312  : public Array_t
{
};
// System.Reflection.AssemblyDescriptionAttribute[]
// System.Reflection.AssemblyDescriptionAttribute[]
struct AssemblyDescriptionAttributeU5BU5D_t5313  : public Array_t
{
};
// System.Reflection.AssemblyFileVersionAttribute[]
// System.Reflection.AssemblyFileVersionAttribute[]
struct AssemblyFileVersionAttributeU5BU5D_t5314  : public Array_t
{
};
// System.Reflection.AssemblyInformationalVersionAttribute[]
// System.Reflection.AssemblyInformationalVersionAttribute[]
struct AssemblyInformationalVersionAttributeU5BU5D_t5315  : public Array_t
{
};
// System.Reflection.AssemblyKeyFileAttribute[]
// System.Reflection.AssemblyKeyFileAttribute[]
struct AssemblyKeyFileAttributeU5BU5D_t5316  : public Array_t
{
};
// System.Reflection.AssemblyNameFlags[]
// System.Reflection.AssemblyNameFlags[]
struct AssemblyNameFlagsU5BU5D_t5317  : public Array_t
{
};
// System.Reflection.AssemblyProductAttribute[]
// System.Reflection.AssemblyProductAttribute[]
struct AssemblyProductAttributeU5BU5D_t5318  : public Array_t
{
};
// System.Reflection.AssemblyTitleAttribute[]
// System.Reflection.AssemblyTitleAttribute[]
struct AssemblyTitleAttributeU5BU5D_t5319  : public Array_t
{
};
// System.Reflection.AssemblyTrademarkAttribute[]
// System.Reflection.AssemblyTrademarkAttribute[]
struct AssemblyTrademarkAttributeU5BU5D_t5320  : public Array_t
{
};
// System.Reflection.PropertyInfo[]
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1923  : public Array_t
{
};
// System.Runtime.InteropServices._PropertyInfo[]
// System.Runtime.InteropServices._PropertyInfo[]
struct _PropertyInfoU5BU5D_t5321  : public Array_t
{
};
// System.Reflection.BindingFlags[]
// System.Reflection.BindingFlags[]
struct BindingFlagsU5BU5D_t5322  : public Array_t
{
};
// System.Reflection.CallingConventions[]
// System.Reflection.CallingConventions[]
struct CallingConventionsU5BU5D_t5323  : public Array_t
{
};
// System.Reflection.EventAttributes[]
// System.Reflection.EventAttributes[]
struct EventAttributesU5BU5D_t5324  : public Array_t
{
};
// System.Reflection.FieldAttributes[]
// System.Reflection.FieldAttributes[]
struct FieldAttributesU5BU5D_t5325  : public Array_t
{
};
// System.Reflection.MemberTypes[]
// System.Reflection.MemberTypes[]
struct MemberTypesU5BU5D_t5326  : public Array_t
{
};
// System.Reflection.MethodAttributes[]
// System.Reflection.MethodAttributes[]
struct MethodAttributesU5BU5D_t5327  : public Array_t
{
};
// System.Reflection.MethodImplAttributes[]
// System.Reflection.MethodImplAttributes[]
struct MethodImplAttributesU5BU5D_t5328  : public Array_t
{
};
// System.Reflection.PInfo[]
// System.Reflection.PInfo[]
struct PInfoU5BU5D_t5329  : public Array_t
{
};
// System.Reflection.ParameterAttributes[]
// System.Reflection.ParameterAttributes[]
struct ParameterAttributesU5BU5D_t5330  : public Array_t
{
};
// System.Reflection.ProcessorArchitecture[]
// System.Reflection.ProcessorArchitecture[]
struct ProcessorArchitectureU5BU5D_t5331  : public Array_t
{
};
// System.Reflection.PropertyAttributes[]
// System.Reflection.PropertyAttributes[]
struct PropertyAttributesU5BU5D_t5332  : public Array_t
{
};
// System.Reflection.TypeAttributes[]
// System.Reflection.TypeAttributes[]
struct TypeAttributesU5BU5D_t5333  : public Array_t
{
};
// System.Resources.NeutralResourcesLanguageAttribute[]
// System.Resources.NeutralResourcesLanguageAttribute[]
struct NeutralResourcesLanguageAttributeU5BU5D_t5334  : public Array_t
{
};
// System.Resources.SatelliteContractVersionAttribute[]
// System.Resources.SatelliteContractVersionAttribute[]
struct SatelliteContractVersionAttributeU5BU5D_t5335  : public Array_t
{
};
// System.Runtime.CompilerServices.CompilationRelaxations[]
// System.Runtime.CompilerServices.CompilationRelaxations[]
struct CompilationRelaxationsU5BU5D_t5336  : public Array_t
{
};
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute[]
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute[]
struct CompilationRelaxationsAttributeU5BU5D_t5337  : public Array_t
{
};
// System.Runtime.CompilerServices.DefaultDependencyAttribute[]
// System.Runtime.CompilerServices.DefaultDependencyAttribute[]
struct DefaultDependencyAttributeU5BU5D_t5338  : public Array_t
{
};
// System.Runtime.CompilerServices.LoadHint[]
// System.Runtime.CompilerServices.LoadHint[]
struct LoadHintU5BU5D_t5339  : public Array_t
{
};
// System.Runtime.CompilerServices.StringFreezingAttribute[]
// System.Runtime.CompilerServices.StringFreezingAttribute[]
struct StringFreezingAttributeU5BU5D_t5340  : public Array_t
{
};
// System.Runtime.ConstrainedExecution.Cer[]
// System.Runtime.ConstrainedExecution.Cer[]
struct CerU5BU5D_t5341  : public Array_t
{
};
// System.Runtime.ConstrainedExecution.Consistency[]
// System.Runtime.ConstrainedExecution.Consistency[]
struct ConsistencyU5BU5D_t5342  : public Array_t
{
};
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute[]
// System.Runtime.ConstrainedExecution.ReliabilityContractAttribute[]
struct ReliabilityContractAttributeU5BU5D_t5343  : public Array_t
{
};
// System.Runtime.InteropServices.CallingConvention[]
// System.Runtime.InteropServices.CallingConvention[]
struct CallingConventionU5BU5D_t5344  : public Array_t
{
};
// System.Runtime.InteropServices.CharSet[]
// System.Runtime.InteropServices.CharSet[]
struct CharSetU5BU5D_t5345  : public Array_t
{
};
// System.Runtime.InteropServices.ClassInterfaceAttribute[]
// System.Runtime.InteropServices.ClassInterfaceAttribute[]
struct ClassInterfaceAttributeU5BU5D_t5346  : public Array_t
{
};
// System.Runtime.InteropServices.ClassInterfaceType[]
// System.Runtime.InteropServices.ClassInterfaceType[]
struct ClassInterfaceTypeU5BU5D_t5347  : public Array_t
{
};
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute[]
// System.Runtime.InteropServices.ComDefaultInterfaceAttribute[]
struct ComDefaultInterfaceAttributeU5BU5D_t5348  : public Array_t
{
};
// System.Runtime.InteropServices.ComInterfaceType[]
// System.Runtime.InteropServices.ComInterfaceType[]
struct ComInterfaceTypeU5BU5D_t5349  : public Array_t
{
};
// System.Runtime.InteropServices.DispIdAttribute[]
// System.Runtime.InteropServices.DispIdAttribute[]
struct DispIdAttributeU5BU5D_t5350  : public Array_t
{
};
// System.Runtime.InteropServices.GCHandleType[]
// System.Runtime.InteropServices.GCHandleType[]
struct GCHandleTypeU5BU5D_t5351  : public Array_t
{
};
// System.Runtime.InteropServices.InterfaceTypeAttribute[]
// System.Runtime.InteropServices.InterfaceTypeAttribute[]
struct InterfaceTypeAttributeU5BU5D_t5352  : public Array_t
{
};
// System.Runtime.InteropServices.PreserveSigAttribute[]
// System.Runtime.InteropServices.PreserveSigAttribute[]
struct PreserveSigAttributeU5BU5D_t5353  : public Array_t
{
};
// System.Runtime.InteropServices.TypeLibImportClassAttribute[]
// System.Runtime.InteropServices.TypeLibImportClassAttribute[]
struct TypeLibImportClassAttributeU5BU5D_t5354  : public Array_t
{
};
// System.Runtime.InteropServices.TypeLibVersionAttribute[]
// System.Runtime.InteropServices.TypeLibVersionAttribute[]
struct TypeLibVersionAttributeU5BU5D_t5355  : public Array_t
{
};
// System.Runtime.InteropServices.UnmanagedType[]
// System.Runtime.InteropServices.UnmanagedType[]
struct UnmanagedTypeU5BU5D_t5356  : public Array_t
{
};
// System.Runtime.Remoting.Activation.UrlAttribute[]
// System.Runtime.Remoting.Activation.UrlAttribute[]
struct UrlAttributeU5BU5D_t5357  : public Array_t
{
};
// System.Runtime.Remoting.Contexts.ContextAttribute[]
// System.Runtime.Remoting.Contexts.ContextAttribute[]
struct ContextAttributeU5BU5D_t5358  : public Array_t
{
};
// System.Runtime.Remoting.Contexts.IContextAttribute[]
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct IContextAttributeU5BU5D_t2039  : public Array_t
{
};
// System.Runtime.Remoting.Contexts.IContextProperty[]
// System.Runtime.Remoting.Contexts.IContextProperty[]
struct IContextPropertyU5BU5D_t5359  : public Array_t
{
};
// System.Runtime.Remoting.Contexts.SynchronizationAttribute[]
// System.Runtime.Remoting.Contexts.SynchronizationAttribute[]
struct SynchronizationAttributeU5BU5D_t5360  : public Array_t
{
};
// System.Runtime.Remoting.Contexts.IContributeClientContextSink[]
// System.Runtime.Remoting.Contexts.IContributeClientContextSink[]
struct IContributeClientContextSinkU5BU5D_t5361  : public Array_t
{
};
// System.Runtime.Remoting.Contexts.IContributeServerContextSink[]
// System.Runtime.Remoting.Contexts.IContributeServerContextSink[]
struct IContributeServerContextSinkU5BU5D_t5362  : public Array_t
{
};
// System.Runtime.Remoting.Messaging.ArgInfoType[]
// System.Runtime.Remoting.Messaging.ArgInfoType[]
struct ArgInfoTypeU5BU5D_t5363  : public Array_t
{
};
// System.Runtime.Remoting.Messaging.Header[]
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t2016  : public Array_t
{
};
// System.Runtime.Remoting.Proxies.ProxyAttribute[]
// System.Runtime.Remoting.Proxies.ProxyAttribute[]
struct ProxyAttributeU5BU5D_t5364  : public Array_t
{
};
// System.Runtime.Remoting.Services.ITrackingHandler[]
// System.Runtime.Remoting.Services.ITrackingHandler[]
struct ITrackingHandlerU5BU5D_t2639  : public Array_t
{
};
// System.Runtime.Remoting.WellKnownObjectMode[]
// System.Runtime.Remoting.WellKnownObjectMode[]
struct WellKnownObjectModeU5BU5D_t5365  : public Array_t
{
};
// System.Runtime.Serialization.Formatters.Binary.BinaryElement[]
// System.Runtime.Serialization.Formatters.Binary.BinaryElement[]
struct BinaryElementU5BU5D_t5366  : public Array_t
{
};
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct TypeTagU5BU5D_t2643  : public Array_t
{
};
// System.Runtime.Serialization.Formatters.Binary.MethodFlags[]
// System.Runtime.Serialization.Formatters.Binary.MethodFlags[]
struct MethodFlagsU5BU5D_t5367  : public Array_t
{
};
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag[]
// System.Runtime.Serialization.Formatters.Binary.ReturnTypeTag[]
struct ReturnTypeTagU5BU5D_t5368  : public Array_t
{
};
// System.DateTime[]
// System.DateTime[]
struct DateTimeU5BU5D_t2640  : public Array_t
{
};
struct DateTimeU5BU5D_t2640_StaticFields{
};
// System.IComparable`1<System.DateTime>[]
// System.IComparable`1<System.DateTime>[]
struct IComparable_1U5BU5D_t5369  : public Array_t
{
};
// System.IEquatable`1<System.DateTime>[]
// System.IEquatable`1<System.DateTime>[]
struct IEquatable_1U5BU5D_t5370  : public Array_t
{
};
// System.Decimal[]
// System.Decimal[]
struct DecimalU5BU5D_t2641  : public Array_t
{
};
struct DecimalU5BU5D_t2641_StaticFields{
};
// System.IComparable`1<System.Decimal>[]
// System.IComparable`1<System.Decimal>[]
struct IComparable_1U5BU5D_t5371  : public Array_t
{
};
// System.IEquatable`1<System.Decimal>[]
// System.IEquatable`1<System.Decimal>[]
struct IEquatable_1U5BU5D_t5372  : public Array_t
{
};
// System.TimeSpan[]
// System.TimeSpan[]
struct TimeSpanU5BU5D_t2642  : public Array_t
{
};
struct TimeSpanU5BU5D_t2642_StaticFields{
};
// System.IComparable`1<System.TimeSpan>[]
// System.IComparable`1<System.TimeSpan>[]
struct IComparable_1U5BU5D_t5373  : public Array_t
{
};
// System.IEquatable`1<System.TimeSpan>[]
// System.IEquatable`1<System.TimeSpan>[]
struct IEquatable_1U5BU5D_t5374  : public Array_t
{
};
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle[]
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle[]
struct FormatterAssemblyStyleU5BU5D_t5375  : public Array_t
{
};
// System.Runtime.Serialization.Formatters.FormatterTypeStyle[]
// System.Runtime.Serialization.Formatters.FormatterTypeStyle[]
struct FormatterTypeStyleU5BU5D_t5376  : public Array_t
{
};
// System.Runtime.Serialization.Formatters.TypeFilterLevel[]
// System.Runtime.Serialization.Formatters.TypeFilterLevel[]
struct TypeFilterLevelU5BU5D_t5377  : public Array_t
{
};
// System.Runtime.Serialization.ObjectRecordStatus[]
// System.Runtime.Serialization.ObjectRecordStatus[]
struct ObjectRecordStatusU5BU5D_t5378  : public Array_t
{
};
// System.Runtime.Serialization.OnDeserializedAttribute[]
// System.Runtime.Serialization.OnDeserializedAttribute[]
struct OnDeserializedAttributeU5BU5D_t5379  : public Array_t
{
};
// System.Runtime.Serialization.OnDeserializingAttribute[]
// System.Runtime.Serialization.OnDeserializingAttribute[]
struct OnDeserializingAttributeU5BU5D_t5380  : public Array_t
{
};
// System.Runtime.Serialization.OnSerializedAttribute[]
// System.Runtime.Serialization.OnSerializedAttribute[]
struct OnSerializedAttributeU5BU5D_t5381  : public Array_t
{
};
// System.Runtime.Serialization.OnSerializingAttribute[]
// System.Runtime.Serialization.OnSerializingAttribute[]
struct OnSerializingAttributeU5BU5D_t5382  : public Array_t
{
};
// System.Runtime.Serialization.StreamingContextStates[]
// System.Runtime.Serialization.StreamingContextStates[]
struct StreamingContextStatesU5BU5D_t5383  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509KeyStorageFlags[]
// System.Security.Cryptography.X509Certificates.X509KeyStorageFlags[]
struct X509KeyStorageFlagsU5BU5D_t5384  : public Array_t
{
};
// System.Security.Cryptography.CipherMode[]
// System.Security.Cryptography.CipherMode[]
struct CipherModeU5BU5D_t5385  : public Array_t
{
};
// System.Security.Cryptography.CspProviderFlags[]
// System.Security.Cryptography.CspProviderFlags[]
struct CspProviderFlagsU5BU5D_t5386  : public Array_t
{
};
// System.Byte[,]
// System.Byte[,]
struct ByteU5BU2CU5D_t2092  : public Array_t
{
};
// System.Security.Cryptography.PaddingMode[]
// System.Security.Cryptography.PaddingMode[]
struct PaddingModeU5BU5D_t5387  : public Array_t
{
};
// System.Security.Policy.StrongName[]
// System.Security.Policy.StrongName[]
struct StrongNameU5BU5D_t5109  : public Array_t
{
};
// System.Security.Policy.IBuiltInEvidence[]
// System.Security.Policy.IBuiltInEvidence[]
struct IBuiltInEvidenceU5BU5D_t5388  : public Array_t
{
};
// System.Security.Policy.IIdentityPermissionFactory[]
// System.Security.Policy.IIdentityPermissionFactory[]
struct IIdentityPermissionFactoryU5BU5D_t5389  : public Array_t
{
};
// System.Security.Principal.PrincipalPolicy[]
// System.Security.Principal.PrincipalPolicy[]
struct PrincipalPolicyU5BU5D_t5390  : public Array_t
{
};
// System.Security.SecuritySafeCriticalAttribute[]
// System.Security.SecuritySafeCriticalAttribute[]
struct SecuritySafeCriticalAttributeU5BU5D_t5391  : public Array_t
{
};
// System.Security.SuppressUnmanagedCodeSecurityAttribute[]
// System.Security.SuppressUnmanagedCodeSecurityAttribute[]
struct SuppressUnmanagedCodeSecurityAttributeU5BU5D_t5392  : public Array_t
{
};
// System.Security.UnverifiableCodeAttribute[]
// System.Security.UnverifiableCodeAttribute[]
struct UnverifiableCodeAttributeU5BU5D_t5393  : public Array_t
{
};
// System.Threading.EventResetMode[]
// System.Threading.EventResetMode[]
struct EventResetModeU5BU5D_t5394  : public Array_t
{
};
// System.Threading.ThreadState[]
// System.Threading.ThreadState[]
struct ThreadStateU5BU5D_t5395  : public Array_t
{
};
// System.AttributeTargets[]
// System.AttributeTargets[]
struct AttributeTargetsU5BU5D_t5396  : public Array_t
{
};
// System.DateTime/Which[]
// System.DateTime/Which[]
struct WhichU5BU5D_t5397  : public Array_t
{
};
// System.DateTimeKind[]
// System.DateTimeKind[]
struct DateTimeKindU5BU5D_t5398  : public Array_t
{
};
// System.DayOfWeek[]
// System.DayOfWeek[]
struct DayOfWeekU5BU5D_t5399  : public Array_t
{
};
// System.Environment/SpecialFolder[]
// System.Environment/SpecialFolder[]
struct SpecialFolderU5BU5D_t5400  : public Array_t
{
};
// System.LoaderOptimization[]
// System.LoaderOptimization[]
struct LoaderOptimizationU5BU5D_t5401  : public Array_t
{
};
// System.NonSerializedAttribute[]
// System.NonSerializedAttribute[]
struct NonSerializedAttributeU5BU5D_t5402  : public Array_t
{
};
// System.PlatformID[]
// System.PlatformID[]
struct PlatformIDU5BU5D_t5403  : public Array_t
{
};
// System.StringComparison[]
// System.StringComparison[]
struct StringComparisonU5BU5D_t5404  : public Array_t
{
};
// System.StringSplitOptions[]
// System.StringSplitOptions[]
struct StringSplitOptionsU5BU5D_t5405  : public Array_t
{
};
// System.ThreadStaticAttribute[]
// System.ThreadStaticAttribute[]
struct ThreadStaticAttributeU5BU5D_t5406  : public Array_t
{
};
// System.TypeCode[]
// System.TypeCode[]
struct TypeCodeU5BU5D_t5407  : public Array_t
{
};
// System.UnitySerializationHolder/UnityType[]
// System.UnitySerializationHolder/UnityType[]
struct UnityTypeU5BU5D_t5408  : public Array_t
{
};
