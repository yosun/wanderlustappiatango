﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.UnityAction`1<Vuforia.DataSetTrackableBehaviour>
struct UnityAction_1_t3674;
// System.Object
struct Object_t;
// Vuforia.DataSetTrackableBehaviour
struct DataSetTrackableBehaviour_t552;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetTrackableBehaviour>::.ctor(System.Object,System.IntPtr)
// UnityEngine.Events.UnityAction`1<System.Object>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_6MethodDeclarations.h"
#define UnityAction_1__ctor_m20364(__this, ___object, ___method, method) (void)UnityAction_1__ctor_m14015_gshared((UnityAction_1_t2707 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetTrackableBehaviour>::Invoke(T0)
#define UnityAction_1_Invoke_m20365(__this, ___arg0, method) (void)UnityAction_1_Invoke_m14016_gshared((UnityAction_1_t2707 *)__this, (Object_t *)___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DataSetTrackableBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m20366(__this, ___arg0, ___callback, ___object, method) (Object_t *)UnityAction_1_BeginInvoke_m14017_gshared((UnityAction_1_t2707 *)__this, (Object_t *)___arg0, (AsyncCallback_t200 *)___callback, (Object_t *)___object, method)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetTrackableBehaviour>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m20367(__this, ___result, method) (void)UnityAction_1_EndInvoke_m14018_gshared((UnityAction_1_t2707 *)__this, (Object_t *)___result, method)
