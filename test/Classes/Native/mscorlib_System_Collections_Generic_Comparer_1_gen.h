﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<System.Object>
struct Comparer_1_t2861;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<System.Object>
struct Comparer_1_t2861  : public Object_t
{
};
struct Comparer_1_t2861_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<System.Object>::_default
	Comparer_1_t2861 * ____default_0;
};
