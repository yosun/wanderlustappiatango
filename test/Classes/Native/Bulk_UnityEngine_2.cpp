﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo UserState_t1059_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserState
#include "UnityEngine_UnityEngine_SocialPlatforms_UserStateMethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// Metadata Definition UnityEngine.SocialPlatforms.UserState
extern Il2CppType Int32_t93_0_0_1542;
FieldInfo UserState_t1059____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t93_0_0_1542/* type */
	, &UserState_t1059_il2cpp_TypeInfo/* parent */
	, offsetof(UserState_t1059, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserState_t1059_0_0_32854;
FieldInfo UserState_t1059____Online_2_FieldInfo = 
{
	"Online"/* name */
	, &UserState_t1059_0_0_32854/* type */
	, &UserState_t1059_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserState_t1059_0_0_32854;
FieldInfo UserState_t1059____OnlineAndAway_3_FieldInfo = 
{
	"OnlineAndAway"/* name */
	, &UserState_t1059_0_0_32854/* type */
	, &UserState_t1059_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserState_t1059_0_0_32854;
FieldInfo UserState_t1059____OnlineAndBusy_4_FieldInfo = 
{
	"OnlineAndBusy"/* name */
	, &UserState_t1059_0_0_32854/* type */
	, &UserState_t1059_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserState_t1059_0_0_32854;
FieldInfo UserState_t1059____Offline_5_FieldInfo = 
{
	"Offline"/* name */
	, &UserState_t1059_0_0_32854/* type */
	, &UserState_t1059_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserState_t1059_0_0_32854;
FieldInfo UserState_t1059____Playing_6_FieldInfo = 
{
	"Playing"/* name */
	, &UserState_t1059_0_0_32854/* type */
	, &UserState_t1059_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UserState_t1059_FieldInfos[] =
{
	&UserState_t1059____value___1_FieldInfo,
	&UserState_t1059____Online_2_FieldInfo,
	&UserState_t1059____OnlineAndAway_3_FieldInfo,
	&UserState_t1059____OnlineAndBusy_4_FieldInfo,
	&UserState_t1059____Offline_5_FieldInfo,
	&UserState_t1059____Playing_6_FieldInfo,
	NULL
};
static const int32_t UserState_t1059____Online_2_DefaultValueData = 0;
extern Il2CppType Int32_t93_0_0_0;
static Il2CppFieldDefaultValueEntry UserState_t1059____Online_2_DefaultValue = 
{
	&UserState_t1059____Online_2_FieldInfo/* field */
	, { (char*)&UserState_t1059____Online_2_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t UserState_t1059____OnlineAndAway_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry UserState_t1059____OnlineAndAway_3_DefaultValue = 
{
	&UserState_t1059____OnlineAndAway_3_FieldInfo/* field */
	, { (char*)&UserState_t1059____OnlineAndAway_3_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t UserState_t1059____OnlineAndBusy_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry UserState_t1059____OnlineAndBusy_4_DefaultValue = 
{
	&UserState_t1059____OnlineAndBusy_4_FieldInfo/* field */
	, { (char*)&UserState_t1059____OnlineAndBusy_4_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t UserState_t1059____Offline_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry UserState_t1059____Offline_5_DefaultValue = 
{
	&UserState_t1059____Offline_5_FieldInfo/* field */
	, { (char*)&UserState_t1059____Offline_5_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t UserState_t1059____Playing_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry UserState_t1059____Playing_6_DefaultValue = 
{
	&UserState_t1059____Playing_6_FieldInfo/* field */
	, { (char*)&UserState_t1059____Playing_6_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UserState_t1059_FieldDefaultValues[] = 
{
	&UserState_t1059____Online_2_DefaultValue,
	&UserState_t1059____OnlineAndAway_3_DefaultValue,
	&UserState_t1059____OnlineAndBusy_4_DefaultValue,
	&UserState_t1059____Offline_5_DefaultValue,
	&UserState_t1059____Playing_6_DefaultValue,
	NULL
};
static MethodInfo* UserState_t1059_MethodInfos[] =
{
	NULL
};
extern MethodInfo Enum_Equals_m191_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo Enum_GetHashCode_m193_MethodInfo;
extern MethodInfo Enum_ToString_m194_MethodInfo;
extern MethodInfo Enum_ToString_m195_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToBoolean_m196_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToByte_m197_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToChar_m198_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDateTime_m199_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDecimal_m200_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDouble_m201_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt16_m202_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt32_m203_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt64_m204_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSByte_m205_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSingle_m206_MethodInfo;
extern MethodInfo Enum_ToString_m207_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToType_m208_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt16_m209_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt32_m210_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt64_m211_MethodInfo;
extern MethodInfo Enum_CompareTo_m212_MethodInfo;
extern MethodInfo Enum_GetTypeCode_m213_MethodInfo;
static MethodInfo* UserState_t1059_VTable[] =
{
	&Enum_Equals_m191_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Enum_GetHashCode_m193_MethodInfo,
	&Enum_ToString_m194_MethodInfo,
	&Enum_ToString_m195_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m196_MethodInfo,
	&Enum_System_IConvertible_ToByte_m197_MethodInfo,
	&Enum_System_IConvertible_ToChar_m198_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m199_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m200_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m201_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m202_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m203_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m204_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m205_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m206_MethodInfo,
	&Enum_ToString_m207_MethodInfo,
	&Enum_System_IConvertible_ToType_m208_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m209_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m210_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m211_MethodInfo,
	&Enum_CompareTo_m212_MethodInfo,
	&Enum_GetTypeCode_m213_MethodInfo,
};
extern TypeInfo IFormattable_t94_il2cpp_TypeInfo;
extern TypeInfo IConvertible_t95_il2cpp_TypeInfo;
extern TypeInfo IComparable_t96_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UserState_t1059_InterfacesOffsets[] = 
{
	{ &IFormattable_t94_il2cpp_TypeInfo, 4},
	{ &IConvertible_t95_il2cpp_TypeInfo, 5},
	{ &IComparable_t96_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UserState_t1059_0_0_0;
extern Il2CppType UserState_t1059_1_0_0;
extern TypeInfo Enum_t97_il2cpp_TypeInfo;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t93_il2cpp_TypeInfo;
TypeInfo UserState_t1059_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserState"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, UserState_t1059_MethodInfos/* methods */
	, NULL/* properties */
	, UserState_t1059_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t97_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t93_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UserState_t1059_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &UserState_t1059_0_0_0/* byval_arg */
	, &UserState_t1059_1_0_0/* this_arg */
	, UserState_t1059_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UserState_t1059_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserState_t1059)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IUserProfile_t1110_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.IUserProfile
static MethodInfo* IUserProfile_t1110_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IUserProfile_t1110_0_0_0;
extern Il2CppType IUserProfile_t1110_1_0_0;
struct IUserProfile_t1110;
TypeInfo IUserProfile_t1110_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IUserProfile"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IUserProfile_t1110_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IUserProfile_t1110_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IUserProfile_t1110_il2cpp_TypeInfo/* cast_class */
	, &IUserProfile_t1110_0_0_0/* byval_arg */
	, &IUserProfile_t1110_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IAchievement_t935_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.IAchievement
static MethodInfo* IAchievement_t935_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IAchievement_t935_0_0_0;
extern Il2CppType IAchievement_t935_1_0_0;
struct IAchievement_t935;
TypeInfo IAchievement_t935_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievement"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievement_t935_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IAchievement_t935_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IAchievement_t935_il2cpp_TypeInfo/* cast_class */
	, &IAchievement_t935_0_0_0/* byval_arg */
	, &IAchievement_t935_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IAchievementDescription_t1105_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.IAchievementDescription
static MethodInfo* IAchievementDescription_t1105_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IAchievementDescription_t1105_0_0_0;
extern Il2CppType IAchievementDescription_t1105_1_0_0;
struct IAchievementDescription_t1105;
TypeInfo IAchievementDescription_t1105_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IAchievementDescription"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IAchievementDescription_t1105_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IAchievementDescription_t1105_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IAchievementDescription_t1105_il2cpp_TypeInfo/* cast_class */
	, &IAchievementDescription_t1105_0_0_0/* byval_arg */
	, &IAchievementDescription_t1105_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IScore_t1053_il2cpp_TypeInfo;



// Metadata Definition UnityEngine.SocialPlatforms.IScore
static MethodInfo* IScore_t1053_MethodInfos[] =
{
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType IScore_t1053_0_0_0;
extern Il2CppType IScore_t1053_1_0_0;
struct IScore_t1053;
TypeInfo IScore_t1053_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "IScore"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, IScore_t1053_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IScore_t1053_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IScore_t1053_il2cpp_TypeInfo/* cast_class */
	, &IScore_t1053_0_0_0/* byval_arg */
	, &IScore_t1053_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UserScope_t1060_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScopeMethodDeclarations.h"



// Metadata Definition UnityEngine.SocialPlatforms.UserScope
extern Il2CppType Int32_t93_0_0_1542;
FieldInfo UserScope_t1060____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t93_0_0_1542/* type */
	, &UserScope_t1060_il2cpp_TypeInfo/* parent */
	, offsetof(UserScope_t1060, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserScope_t1060_0_0_32854;
FieldInfo UserScope_t1060____Global_2_FieldInfo = 
{
	"Global"/* name */
	, &UserScope_t1060_0_0_32854/* type */
	, &UserScope_t1060_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UserScope_t1060_0_0_32854;
FieldInfo UserScope_t1060____FriendsOnly_3_FieldInfo = 
{
	"FriendsOnly"/* name */
	, &UserScope_t1060_0_0_32854/* type */
	, &UserScope_t1060_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UserScope_t1060_FieldInfos[] =
{
	&UserScope_t1060____value___1_FieldInfo,
	&UserScope_t1060____Global_2_FieldInfo,
	&UserScope_t1060____FriendsOnly_3_FieldInfo,
	NULL
};
static const int32_t UserScope_t1060____Global_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry UserScope_t1060____Global_2_DefaultValue = 
{
	&UserScope_t1060____Global_2_FieldInfo/* field */
	, { (char*)&UserScope_t1060____Global_2_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t UserScope_t1060____FriendsOnly_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry UserScope_t1060____FriendsOnly_3_DefaultValue = 
{
	&UserScope_t1060____FriendsOnly_3_FieldInfo/* field */
	, { (char*)&UserScope_t1060____FriendsOnly_3_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UserScope_t1060_FieldDefaultValues[] = 
{
	&UserScope_t1060____Global_2_DefaultValue,
	&UserScope_t1060____FriendsOnly_3_DefaultValue,
	NULL
};
static MethodInfo* UserScope_t1060_MethodInfos[] =
{
	NULL
};
static MethodInfo* UserScope_t1060_VTable[] =
{
	&Enum_Equals_m191_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Enum_GetHashCode_m193_MethodInfo,
	&Enum_ToString_m194_MethodInfo,
	&Enum_ToString_m195_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m196_MethodInfo,
	&Enum_System_IConvertible_ToByte_m197_MethodInfo,
	&Enum_System_IConvertible_ToChar_m198_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m199_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m200_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m201_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m202_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m203_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m204_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m205_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m206_MethodInfo,
	&Enum_ToString_m207_MethodInfo,
	&Enum_System_IConvertible_ToType_m208_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m209_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m210_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m211_MethodInfo,
	&Enum_CompareTo_m212_MethodInfo,
	&Enum_GetTypeCode_m213_MethodInfo,
};
static Il2CppInterfaceOffsetPair UserScope_t1060_InterfacesOffsets[] = 
{
	{ &IFormattable_t94_il2cpp_TypeInfo, 4},
	{ &IConvertible_t95_il2cpp_TypeInfo, 5},
	{ &IComparable_t96_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UserScope_t1060_0_0_0;
extern Il2CppType UserScope_t1060_1_0_0;
TypeInfo UserScope_t1060_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, UserScope_t1060_MethodInfos/* methods */
	, NULL/* properties */
	, UserScope_t1060_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t97_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t93_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UserScope_t1060_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &UserScope_t1060_0_0_0/* byval_arg */
	, &UserScope_t1060_1_0_0/* this_arg */
	, UserScope_t1060_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UserScope_t1060_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserScope_t1060)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TimeScope_t1061_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScopeMethodDeclarations.h"



// Metadata Definition UnityEngine.SocialPlatforms.TimeScope
extern Il2CppType Int32_t93_0_0_1542;
FieldInfo TimeScope_t1061____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t93_0_0_1542/* type */
	, &TimeScope_t1061_il2cpp_TypeInfo/* parent */
	, offsetof(TimeScope_t1061, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TimeScope_t1061_0_0_32854;
FieldInfo TimeScope_t1061____Today_2_FieldInfo = 
{
	"Today"/* name */
	, &TimeScope_t1061_0_0_32854/* type */
	, &TimeScope_t1061_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TimeScope_t1061_0_0_32854;
FieldInfo TimeScope_t1061____Week_3_FieldInfo = 
{
	"Week"/* name */
	, &TimeScope_t1061_0_0_32854/* type */
	, &TimeScope_t1061_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TimeScope_t1061_0_0_32854;
FieldInfo TimeScope_t1061____AllTime_4_FieldInfo = 
{
	"AllTime"/* name */
	, &TimeScope_t1061_0_0_32854/* type */
	, &TimeScope_t1061_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TimeScope_t1061_FieldInfos[] =
{
	&TimeScope_t1061____value___1_FieldInfo,
	&TimeScope_t1061____Today_2_FieldInfo,
	&TimeScope_t1061____Week_3_FieldInfo,
	&TimeScope_t1061____AllTime_4_FieldInfo,
	NULL
};
static const int32_t TimeScope_t1061____Today_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry TimeScope_t1061____Today_2_DefaultValue = 
{
	&TimeScope_t1061____Today_2_FieldInfo/* field */
	, { (char*)&TimeScope_t1061____Today_2_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TimeScope_t1061____Week_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TimeScope_t1061____Week_3_DefaultValue = 
{
	&TimeScope_t1061____Week_3_FieldInfo/* field */
	, { (char*)&TimeScope_t1061____Week_3_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TimeScope_t1061____AllTime_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry TimeScope_t1061____AllTime_4_DefaultValue = 
{
	&TimeScope_t1061____AllTime_4_FieldInfo/* field */
	, { (char*)&TimeScope_t1061____AllTime_4_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TimeScope_t1061_FieldDefaultValues[] = 
{
	&TimeScope_t1061____Today_2_DefaultValue,
	&TimeScope_t1061____Week_3_DefaultValue,
	&TimeScope_t1061____AllTime_4_DefaultValue,
	NULL
};
static MethodInfo* TimeScope_t1061_MethodInfos[] =
{
	NULL
};
static MethodInfo* TimeScope_t1061_VTable[] =
{
	&Enum_Equals_m191_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Enum_GetHashCode_m193_MethodInfo,
	&Enum_ToString_m194_MethodInfo,
	&Enum_ToString_m195_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m196_MethodInfo,
	&Enum_System_IConvertible_ToByte_m197_MethodInfo,
	&Enum_System_IConvertible_ToChar_m198_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m199_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m200_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m201_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m202_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m203_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m204_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m205_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m206_MethodInfo,
	&Enum_ToString_m207_MethodInfo,
	&Enum_System_IConvertible_ToType_m208_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m209_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m210_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m211_MethodInfo,
	&Enum_CompareTo_m212_MethodInfo,
	&Enum_GetTypeCode_m213_MethodInfo,
};
static Il2CppInterfaceOffsetPair TimeScope_t1061_InterfacesOffsets[] = 
{
	{ &IFormattable_t94_il2cpp_TypeInfo, 4},
	{ &IConvertible_t95_il2cpp_TypeInfo, 5},
	{ &IComparable_t96_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TimeScope_t1061_0_0_0;
extern Il2CppType TimeScope_t1061_1_0_0;
TypeInfo TimeScope_t1061_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TimeScope"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, TimeScope_t1061_MethodInfos/* methods */
	, NULL/* properties */
	, TimeScope_t1061_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t97_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t93_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TimeScope_t1061_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &TimeScope_t1061_0_0_0/* byval_arg */
	, &TimeScope_t1061_1_0_0/* this_arg */
	, TimeScope_t1061_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TimeScope_t1061_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TimeScope_t1061)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Range_t1055_il2cpp_TypeInfo;
// UnityEngine.SocialPlatforms.Range
#include "UnityEngine_UnityEngine_SocialPlatforms_RangeMethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"


// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
extern MethodInfo Range__ctor_m6313_MethodInfo;
 void Range__ctor_m6313 (Range_t1055 * __this, int32_t ___fromValue, int32_t ___valueCount, MethodInfo* method){
	{
		__this->___from_0 = ___fromValue;
		__this->___count_1 = ___valueCount;
		return;
	}
}
// Metadata Definition UnityEngine.SocialPlatforms.Range
extern Il2CppType Int32_t93_0_0_6;
FieldInfo Range_t1055____from_0_FieldInfo = 
{
	"from"/* name */
	, &Int32_t93_0_0_6/* type */
	, &Range_t1055_il2cpp_TypeInfo/* parent */
	, offsetof(Range_t1055, ___from_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_6;
FieldInfo Range_t1055____count_1_FieldInfo = 
{
	"count"/* name */
	, &Int32_t93_0_0_6/* type */
	, &Range_t1055_il2cpp_TypeInfo/* parent */
	, offsetof(Range_t1055, ___count_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Range_t1055_FieldInfos[] =
{
	&Range_t1055____from_0_FieldInfo,
	&Range_t1055____count_1_FieldInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo Range_t1055_Range__ctor_m6313_ParameterInfos[] = 
{
	{"fromValue", 0, 134219238, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"valueCount", 1, 134219239, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SocialPlatforms.Range::.ctor(System.Int32,System.Int32)
MethodInfo Range__ctor_m6313_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Range__ctor_m6313/* method */
	, &Range_t1055_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, Range_t1055_Range__ctor_m6313_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* Range_t1055_MethodInfos[] =
{
	&Range__ctor_m6313_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
static MethodInfo* Range_t1055_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType Range_t1055_0_0_0;
extern Il2CppType Range_t1055_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
TypeInfo Range_t1055_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "Range"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, Range_t1055_MethodInfos/* methods */
	, NULL/* properties */
	, Range_t1055_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Range_t1055_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Range_t1055_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Range_t1055_il2cpp_TypeInfo/* cast_class */
	, &Range_t1055_0_0_0/* byval_arg */
	, &Range_t1055_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Range_t1055)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, sizeof(Range_t1055 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, true/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ILeaderboard_t934_il2cpp_TypeInfo;

// System.String
#include "mscorlib_System_String.h"


// System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id()
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope()
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range()
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope()
// Metadata Definition UnityEngine.SocialPlatforms.ILeaderboard
extern MethodInfo ILeaderboard_get_id_m6410_MethodInfo;
static PropertyInfo ILeaderboard_t934____id_PropertyInfo = 
{
	&ILeaderboard_t934_il2cpp_TypeInfo/* parent */
	, "id"/* name */
	, &ILeaderboard_get_id_m6410_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ILeaderboard_get_userScope_m6413_MethodInfo;
static PropertyInfo ILeaderboard_t934____userScope_PropertyInfo = 
{
	&ILeaderboard_t934_il2cpp_TypeInfo/* parent */
	, "userScope"/* name */
	, &ILeaderboard_get_userScope_m6413_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ILeaderboard_get_range_m6412_MethodInfo;
static PropertyInfo ILeaderboard_t934____range_PropertyInfo = 
{
	&ILeaderboard_t934_il2cpp_TypeInfo/* parent */
	, "range"/* name */
	, &ILeaderboard_get_range_m6412_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ILeaderboard_get_timeScope_m6411_MethodInfo;
static PropertyInfo ILeaderboard_t934____timeScope_PropertyInfo = 
{
	&ILeaderboard_t934_il2cpp_TypeInfo/* parent */
	, "timeScope"/* name */
	, &ILeaderboard_get_timeScope_m6411_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ILeaderboard_t934_PropertyInfos[] =
{
	&ILeaderboard_t934____id_PropertyInfo,
	&ILeaderboard_t934____userScope_PropertyInfo,
	&ILeaderboard_t934____range_PropertyInfo,
	&ILeaderboard_t934____timeScope_PropertyInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.SocialPlatforms.ILeaderboard::get_id()
MethodInfo ILeaderboard_get_id_m6410_MethodInfo = 
{
	"get_id"/* name */
	, NULL/* method */
	, &ILeaderboard_t934_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UserScope_t1060_0_0_0;
extern void* RuntimeInvoker_UserScope_t1060 (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.UserScope UnityEngine.SocialPlatforms.ILeaderboard::get_userScope()
MethodInfo ILeaderboard_get_userScope_m6413_MethodInfo = 
{
	"get_userScope"/* name */
	, NULL/* method */
	, &ILeaderboard_t934_il2cpp_TypeInfo/* declaring_type */
	, &UserScope_t1060_0_0_0/* return_type */
	, RuntimeInvoker_UserScope_t1060/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Range_t1055_0_0_0;
extern void* RuntimeInvoker_Range_t1055 (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.Range UnityEngine.SocialPlatforms.ILeaderboard::get_range()
MethodInfo ILeaderboard_get_range_m6412_MethodInfo = 
{
	"get_range"/* name */
	, NULL/* method */
	, &ILeaderboard_t934_il2cpp_TypeInfo/* declaring_type */
	, &Range_t1055_0_0_0/* return_type */
	, RuntimeInvoker_Range_t1055/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TimeScope_t1061_0_0_0;
extern void* RuntimeInvoker_TimeScope_t1061 (MethodInfo* method, void* obj, void** args);
// UnityEngine.SocialPlatforms.TimeScope UnityEngine.SocialPlatforms.ILeaderboard::get_timeScope()
MethodInfo ILeaderboard_get_timeScope_m6411_MethodInfo = 
{
	"get_timeScope"/* name */
	, NULL/* method */
	, &ILeaderboard_t934_il2cpp_TypeInfo/* declaring_type */
	, &TimeScope_t1061_0_0_0/* return_type */
	, RuntimeInvoker_TimeScope_t1061/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ILeaderboard_t934_MethodInfos[] =
{
	&ILeaderboard_get_id_m6410_MethodInfo,
	&ILeaderboard_get_userScope_m6413_MethodInfo,
	&ILeaderboard_get_range_m6412_MethodInfo,
	&ILeaderboard_get_timeScope_m6411_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ILeaderboard_t934_0_0_0;
extern Il2CppType ILeaderboard_t934_1_0_0;
struct ILeaderboard_t934;
TypeInfo ILeaderboard_t934_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ILeaderboard"/* name */
	, "UnityEngine.SocialPlatforms"/* namespaze */
	, ILeaderboard_t934_MethodInfos/* methods */
	, ILeaderboard_t934_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ILeaderboard_t934_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ILeaderboard_t934_il2cpp_TypeInfo/* cast_class */
	, &ILeaderboard_t934_0_0_0/* byval_arg */
	, &ILeaderboard_t934_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 4/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo PropertyAttribute_t1062_il2cpp_TypeInfo;
// UnityEngine.PropertyAttribute
#include "UnityEngine_UnityEngine_PropertyAttributeMethodDeclarations.h"

// System.Attribute
#include "mscorlib_System_AttributeMethodDeclarations.h"
extern MethodInfo Attribute__ctor_m4248_MethodInfo;


// System.Void UnityEngine.PropertyAttribute::.ctor()
extern MethodInfo PropertyAttribute__ctor_m6314_MethodInfo;
 void PropertyAttribute__ctor_m6314 (PropertyAttribute_t1062 * __this, MethodInfo* method){
	{
		Attribute__ctor_m4248(__this, /*hidden argument*/&Attribute__ctor_m4248_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngine.PropertyAttribute
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.PropertyAttribute::.ctor()
MethodInfo PropertyAttribute__ctor_m6314_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PropertyAttribute__ctor_m6314/* method */
	, &PropertyAttribute_t1062_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PropertyAttribute_t1062_MethodInfos[] =
{
	&PropertyAttribute__ctor_m6314_MethodInfo,
	NULL
};
extern MethodInfo Attribute_Equals_m4249_MethodInfo;
extern MethodInfo Attribute_GetHashCode_m4250_MethodInfo;
extern MethodInfo Object_ToString_m306_MethodInfo;
static MethodInfo* PropertyAttribute_t1062_VTable[] =
{
	&Attribute_Equals_m4249_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Attribute_GetHashCode_m4250_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
extern TypeInfo _Attribute_t775_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair PropertyAttribute_t1062_InterfacesOffsets[] = 
{
	{ &_Attribute_t775_il2cpp_TypeInfo, 4},
};
extern TypeInfo AttributeUsageAttribute_t1160_il2cpp_TypeInfo;
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"
// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttributeMethodDeclarations.h"
extern MethodInfo AttributeUsageAttribute__ctor_m6500_MethodInfo;
extern MethodInfo AttributeUsageAttribute_set_Inherited_m6501_MethodInfo;
extern MethodInfo AttributeUsageAttribute_set_AllowMultiple_m6502_MethodInfo;
void PropertyAttribute_t1062_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1160 * tmp;
		tmp = (AttributeUsageAttribute_t1160 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1160_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6500(tmp, 256, &AttributeUsageAttribute__ctor_m6500_MethodInfo);
		AttributeUsageAttribute_set_Inherited_m6501(tmp, true, &AttributeUsageAttribute_set_Inherited_m6501_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6502(tmp, false, &AttributeUsageAttribute_set_AllowMultiple_m6502_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache PropertyAttribute_t1062__CustomAttributeCache = {
1,
NULL,
&PropertyAttribute_t1062_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType PropertyAttribute_t1062_0_0_0;
extern Il2CppType PropertyAttribute_t1062_1_0_0;
extern TypeInfo Attribute_t146_il2cpp_TypeInfo;
struct PropertyAttribute_t1062;
extern CustomAttributesCache PropertyAttribute_t1062__CustomAttributeCache;
TypeInfo PropertyAttribute_t1062_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PropertyAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, PropertyAttribute_t1062_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Attribute_t146_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &PropertyAttribute_t1062_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, PropertyAttribute_t1062_VTable/* vtable */
	, &PropertyAttribute_t1062__CustomAttributeCache/* custom_attributes_cache */
	, &PropertyAttribute_t1062_il2cpp_TypeInfo/* cast_class */
	, &PropertyAttribute_t1062_0_0_0/* byval_arg */
	, &PropertyAttribute_t1062_1_0_0/* this_arg */
	, PropertyAttribute_t1062_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PropertyAttribute_t1062)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TooltipAttribute_t500_il2cpp_TypeInfo;
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"



// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern MethodInfo TooltipAttribute__ctor_m2430_MethodInfo;
 void TooltipAttribute__ctor_m2430 (TooltipAttribute_t500 * __this, String_t* ___tooltip, MethodInfo* method){
	{
		PropertyAttribute__ctor_m6314(__this, /*hidden argument*/&PropertyAttribute__ctor_m6314_MethodInfo);
		__this->___tooltip_0 = ___tooltip;
		return;
	}
}
// Metadata Definition UnityEngine.TooltipAttribute
extern Il2CppType String_t_0_0_38;
FieldInfo TooltipAttribute_t500____tooltip_0_FieldInfo = 
{
	"tooltip"/* name */
	, &String_t_0_0_38/* type */
	, &TooltipAttribute_t500_il2cpp_TypeInfo/* parent */
	, offsetof(TooltipAttribute_t500, ___tooltip_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TooltipAttribute_t500_FieldInfos[] =
{
	&TooltipAttribute_t500____tooltip_0_FieldInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo TooltipAttribute_t500_TooltipAttribute__ctor_m2430_ParameterInfos[] = 
{
	{"tooltip", 0, 134219240, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
MethodInfo TooltipAttribute__ctor_m2430_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TooltipAttribute__ctor_m2430/* method */
	, &TooltipAttribute_t500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, TooltipAttribute_t500_TooltipAttribute__ctor_m2430_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1433/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TooltipAttribute_t500_MethodInfos[] =
{
	&TooltipAttribute__ctor_m2430_MethodInfo,
	NULL
};
static MethodInfo* TooltipAttribute_t500_VTable[] =
{
	&Attribute_Equals_m4249_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Attribute_GetHashCode_m4250_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
static Il2CppInterfaceOffsetPair TooltipAttribute_t500_InterfacesOffsets[] = 
{
	{ &_Attribute_t775_il2cpp_TypeInfo, 4},
};
void TooltipAttribute_t500_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1160 * tmp;
		tmp = (AttributeUsageAttribute_t1160 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1160_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6500(tmp, 256, &AttributeUsageAttribute__ctor_m6500_MethodInfo);
		AttributeUsageAttribute_set_Inherited_m6501(tmp, true, &AttributeUsageAttribute_set_Inherited_m6501_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6502(tmp, false, &AttributeUsageAttribute_set_AllowMultiple_m6502_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache TooltipAttribute_t500__CustomAttributeCache = {
1,
NULL,
&TooltipAttribute_t500_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TooltipAttribute_t500_0_0_0;
extern Il2CppType TooltipAttribute_t500_1_0_0;
struct TooltipAttribute_t500;
extern CustomAttributesCache TooltipAttribute_t500__CustomAttributeCache;
TypeInfo TooltipAttribute_t500_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TooltipAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, TooltipAttribute_t500_MethodInfos/* methods */
	, NULL/* properties */
	, TooltipAttribute_t500_FieldInfos/* fields */
	, NULL/* events */
	, &PropertyAttribute_t1062_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TooltipAttribute_t500_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TooltipAttribute_t500_VTable/* vtable */
	, &TooltipAttribute_t500__CustomAttributeCache/* custom_attributes_cache */
	, &TooltipAttribute_t500_il2cpp_TypeInfo/* cast_class */
	, &TooltipAttribute_t500_0_0_0/* byval_arg */
	, &TooltipAttribute_t500_1_0_0/* this_arg */
	, TooltipAttribute_t500_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TooltipAttribute_t500)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SpaceAttribute_t496_il2cpp_TypeInfo;
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttributeMethodDeclarations.h"

// System.Single
#include "mscorlib_System_Single.h"


// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern MethodInfo SpaceAttribute__ctor_m2371_MethodInfo;
 void SpaceAttribute__ctor_m2371 (SpaceAttribute_t496 * __this, float ___height, MethodInfo* method){
	{
		PropertyAttribute__ctor_m6314(__this, /*hidden argument*/&PropertyAttribute__ctor_m6314_MethodInfo);
		__this->___height_0 = ___height;
		return;
	}
}
// Metadata Definition UnityEngine.SpaceAttribute
extern Il2CppType Single_t105_0_0_38;
FieldInfo SpaceAttribute_t496____height_0_FieldInfo = 
{
	"height"/* name */
	, &Single_t105_0_0_38/* type */
	, &SpaceAttribute_t496_il2cpp_TypeInfo/* parent */
	, offsetof(SpaceAttribute_t496, ___height_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* SpaceAttribute_t496_FieldInfos[] =
{
	&SpaceAttribute_t496____height_0_FieldInfo,
	NULL
};
extern Il2CppType Single_t105_0_0_0;
extern Il2CppType Single_t105_0_0_0;
static ParameterInfo SpaceAttribute_t496_SpaceAttribute__ctor_m2371_ParameterInfos[] = 
{
	{"height", 0, 134219241, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Single_t105 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
MethodInfo SpaceAttribute__ctor_m2371_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SpaceAttribute__ctor_m2371/* method */
	, &SpaceAttribute_t496_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Single_t105/* invoker_method */
	, SpaceAttribute_t496_SpaceAttribute__ctor_m2371_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1434/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SpaceAttribute_t496_MethodInfos[] =
{
	&SpaceAttribute__ctor_m2371_MethodInfo,
	NULL
};
static MethodInfo* SpaceAttribute_t496_VTable[] =
{
	&Attribute_Equals_m4249_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Attribute_GetHashCode_m4250_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
static Il2CppInterfaceOffsetPair SpaceAttribute_t496_InterfacesOffsets[] = 
{
	{ &_Attribute_t775_il2cpp_TypeInfo, 4},
};
void SpaceAttribute_t496_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1160 * tmp;
		tmp = (AttributeUsageAttribute_t1160 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1160_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6500(tmp, 256, &AttributeUsageAttribute__ctor_m6500_MethodInfo);
		AttributeUsageAttribute_set_Inherited_m6501(tmp, true, &AttributeUsageAttribute_set_Inherited_m6501_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6502(tmp, true, &AttributeUsageAttribute_set_AllowMultiple_m6502_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache SpaceAttribute_t496__CustomAttributeCache = {
1,
NULL,
&SpaceAttribute_t496_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SpaceAttribute_t496_0_0_0;
extern Il2CppType SpaceAttribute_t496_1_0_0;
struct SpaceAttribute_t496;
extern CustomAttributesCache SpaceAttribute_t496__CustomAttributeCache;
TypeInfo SpaceAttribute_t496_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SpaceAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SpaceAttribute_t496_MethodInfos/* methods */
	, NULL/* properties */
	, SpaceAttribute_t496_FieldInfos/* fields */
	, NULL/* events */
	, &PropertyAttribute_t1062_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SpaceAttribute_t496_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SpaceAttribute_t496_VTable/* vtable */
	, &SpaceAttribute_t496__CustomAttributeCache/* custom_attributes_cache */
	, &SpaceAttribute_t496_il2cpp_TypeInfo/* cast_class */
	, &SpaceAttribute_t496_0_0_0/* byval_arg */
	, &SpaceAttribute_t496_1_0_0/* this_arg */
	, SpaceAttribute_t496_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SpaceAttribute_t496)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo RangeAttribute_t457_il2cpp_TypeInfo;
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"



// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern MethodInfo RangeAttribute__ctor_m2091_MethodInfo;
 void RangeAttribute__ctor_m2091 (RangeAttribute_t457 * __this, float ___min, float ___max, MethodInfo* method){
	{
		PropertyAttribute__ctor_m6314(__this, /*hidden argument*/&PropertyAttribute__ctor_m6314_MethodInfo);
		__this->___min_0 = ___min;
		__this->___max_1 = ___max;
		return;
	}
}
// Metadata Definition UnityEngine.RangeAttribute
extern Il2CppType Single_t105_0_0_38;
FieldInfo RangeAttribute_t457____min_0_FieldInfo = 
{
	"min"/* name */
	, &Single_t105_0_0_38/* type */
	, &RangeAttribute_t457_il2cpp_TypeInfo/* parent */
	, offsetof(RangeAttribute_t457, ___min_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_38;
FieldInfo RangeAttribute_t457____max_1_FieldInfo = 
{
	"max"/* name */
	, &Single_t105_0_0_38/* type */
	, &RangeAttribute_t457_il2cpp_TypeInfo/* parent */
	, offsetof(RangeAttribute_t457, ___max_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* RangeAttribute_t457_FieldInfos[] =
{
	&RangeAttribute_t457____min_0_FieldInfo,
	&RangeAttribute_t457____max_1_FieldInfo,
	NULL
};
extern Il2CppType Single_t105_0_0_0;
extern Il2CppType Single_t105_0_0_0;
static ParameterInfo RangeAttribute_t457_RangeAttribute__ctor_m2091_ParameterInfos[] = 
{
	{"min", 0, 134219242, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
	{"max", 1, 134219243, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Single_t105_Single_t105 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
MethodInfo RangeAttribute__ctor_m2091_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&RangeAttribute__ctor_m2091/* method */
	, &RangeAttribute_t457_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Single_t105_Single_t105/* invoker_method */
	, RangeAttribute_t457_RangeAttribute__ctor_m2091_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1435/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* RangeAttribute_t457_MethodInfos[] =
{
	&RangeAttribute__ctor_m2091_MethodInfo,
	NULL
};
static MethodInfo* RangeAttribute_t457_VTable[] =
{
	&Attribute_Equals_m4249_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Attribute_GetHashCode_m4250_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
static Il2CppInterfaceOffsetPair RangeAttribute_t457_InterfacesOffsets[] = 
{
	{ &_Attribute_t775_il2cpp_TypeInfo, 4},
};
void RangeAttribute_t457_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1160 * tmp;
		tmp = (AttributeUsageAttribute_t1160 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1160_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6500(tmp, 256, &AttributeUsageAttribute__ctor_m6500_MethodInfo);
		AttributeUsageAttribute_set_Inherited_m6501(tmp, true, &AttributeUsageAttribute_set_Inherited_m6501_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6502(tmp, false, &AttributeUsageAttribute_set_AllowMultiple_m6502_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache RangeAttribute_t457__CustomAttributeCache = {
1,
NULL,
&RangeAttribute_t457_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType RangeAttribute_t457_0_0_0;
extern Il2CppType RangeAttribute_t457_1_0_0;
struct RangeAttribute_t457;
extern CustomAttributesCache RangeAttribute_t457__CustomAttributeCache;
TypeInfo RangeAttribute_t457_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "RangeAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, RangeAttribute_t457_MethodInfos/* methods */
	, NULL/* properties */
	, RangeAttribute_t457_FieldInfos/* fields */
	, NULL/* events */
	, &PropertyAttribute_t1062_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &RangeAttribute_t457_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, RangeAttribute_t457_VTable/* vtable */
	, &RangeAttribute_t457__CustomAttributeCache/* custom_attributes_cache */
	, &RangeAttribute_t457_il2cpp_TypeInfo/* cast_class */
	, &RangeAttribute_t457_0_0_0/* byval_arg */
	, &RangeAttribute_t457_1_0_0/* this_arg */
	, RangeAttribute_t457_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (RangeAttribute_t457)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TextAreaAttribute_t506_il2cpp_TypeInfo;
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttributeMethodDeclarations.h"



// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern MethodInfo TextAreaAttribute__ctor_m2460_MethodInfo;
 void TextAreaAttribute__ctor_m2460 (TextAreaAttribute_t506 * __this, int32_t ___minLines, int32_t ___maxLines, MethodInfo* method){
	{
		PropertyAttribute__ctor_m6314(__this, /*hidden argument*/&PropertyAttribute__ctor_m6314_MethodInfo);
		__this->___minLines_0 = ___minLines;
		__this->___maxLines_1 = ___maxLines;
		return;
	}
}
// Metadata Definition UnityEngine.TextAreaAttribute
extern Il2CppType Int32_t93_0_0_38;
FieldInfo TextAreaAttribute_t506____minLines_0_FieldInfo = 
{
	"minLines"/* name */
	, &Int32_t93_0_0_38/* type */
	, &TextAreaAttribute_t506_il2cpp_TypeInfo/* parent */
	, offsetof(TextAreaAttribute_t506, ___minLines_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_38;
FieldInfo TextAreaAttribute_t506____maxLines_1_FieldInfo = 
{
	"maxLines"/* name */
	, &Int32_t93_0_0_38/* type */
	, &TextAreaAttribute_t506_il2cpp_TypeInfo/* parent */
	, offsetof(TextAreaAttribute_t506, ___maxLines_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TextAreaAttribute_t506_FieldInfos[] =
{
	&TextAreaAttribute_t506____minLines_0_FieldInfo,
	&TextAreaAttribute_t506____maxLines_1_FieldInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo TextAreaAttribute_t506_TextAreaAttribute__ctor_m2460_ParameterInfos[] = 
{
	{"minLines", 0, 134219244, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"maxLines", 1, 134219245, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
MethodInfo TextAreaAttribute__ctor_m2460_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextAreaAttribute__ctor_m2460/* method */
	, &TextAreaAttribute_t506_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, TextAreaAttribute_t506_TextAreaAttribute__ctor_m2460_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1436/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TextAreaAttribute_t506_MethodInfos[] =
{
	&TextAreaAttribute__ctor_m2460_MethodInfo,
	NULL
};
static MethodInfo* TextAreaAttribute_t506_VTable[] =
{
	&Attribute_Equals_m4249_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Attribute_GetHashCode_m4250_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
static Il2CppInterfaceOffsetPair TextAreaAttribute_t506_InterfacesOffsets[] = 
{
	{ &_Attribute_t775_il2cpp_TypeInfo, 4},
};
void TextAreaAttribute_t506_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1160 * tmp;
		tmp = (AttributeUsageAttribute_t1160 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1160_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6500(tmp, 256, &AttributeUsageAttribute__ctor_m6500_MethodInfo);
		AttributeUsageAttribute_set_Inherited_m6501(tmp, true, &AttributeUsageAttribute_set_Inherited_m6501_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6502(tmp, false, &AttributeUsageAttribute_set_AllowMultiple_m6502_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache TextAreaAttribute_t506__CustomAttributeCache = {
1,
NULL,
&TextAreaAttribute_t506_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextAreaAttribute_t506_0_0_0;
extern Il2CppType TextAreaAttribute_t506_1_0_0;
struct TextAreaAttribute_t506;
extern CustomAttributesCache TextAreaAttribute_t506__CustomAttributeCache;
TypeInfo TextAreaAttribute_t506_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextAreaAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, TextAreaAttribute_t506_MethodInfos/* methods */
	, NULL/* properties */
	, TextAreaAttribute_t506_FieldInfos/* fields */
	, NULL/* events */
	, &PropertyAttribute_t1062_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TextAreaAttribute_t506_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TextAreaAttribute_t506_VTable/* vtable */
	, &TextAreaAttribute_t506__CustomAttributeCache/* custom_attributes_cache */
	, &TextAreaAttribute_t506_il2cpp_TypeInfo/* cast_class */
	, &TextAreaAttribute_t506_0_0_0/* byval_arg */
	, &TextAreaAttribute_t506_1_0_0/* this_arg */
	, TextAreaAttribute_t506_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextAreaAttribute_t506)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SelectionBaseAttribute_t498_il2cpp_TypeInfo;
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttributeMethodDeclarations.h"



// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern MethodInfo SelectionBaseAttribute__ctor_m2399_MethodInfo;
 void SelectionBaseAttribute__ctor_m2399 (SelectionBaseAttribute_t498 * __this, MethodInfo* method){
	{
		Attribute__ctor_m4248(__this, /*hidden argument*/&Attribute__ctor_m4248_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngine.SelectionBaseAttribute
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
MethodInfo SelectionBaseAttribute__ctor_m2399_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SelectionBaseAttribute__ctor_m2399/* method */
	, &SelectionBaseAttribute_t498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1437/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SelectionBaseAttribute_t498_MethodInfos[] =
{
	&SelectionBaseAttribute__ctor_m2399_MethodInfo,
	NULL
};
static MethodInfo* SelectionBaseAttribute_t498_VTable[] =
{
	&Attribute_Equals_m4249_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Attribute_GetHashCode_m4250_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
static Il2CppInterfaceOffsetPair SelectionBaseAttribute_t498_InterfacesOffsets[] = 
{
	{ &_Attribute_t775_il2cpp_TypeInfo, 4},
};
void SelectionBaseAttribute_t498_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1160 * tmp;
		tmp = (AttributeUsageAttribute_t1160 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1160_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6500(tmp, 4, &AttributeUsageAttribute__ctor_m6500_MethodInfo);
		AttributeUsageAttribute_set_Inherited_m6501(tmp, true, &AttributeUsageAttribute_set_Inherited_m6501_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6502(tmp, false, &AttributeUsageAttribute_set_AllowMultiple_m6502_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache SelectionBaseAttribute_t498__CustomAttributeCache = {
1,
NULL,
&SelectionBaseAttribute_t498_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SelectionBaseAttribute_t498_0_0_0;
extern Il2CppType SelectionBaseAttribute_t498_1_0_0;
struct SelectionBaseAttribute_t498;
extern CustomAttributesCache SelectionBaseAttribute_t498__CustomAttributeCache;
TypeInfo SelectionBaseAttribute_t498_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SelectionBaseAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SelectionBaseAttribute_t498_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Attribute_t146_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SelectionBaseAttribute_t498_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SelectionBaseAttribute_t498_VTable/* vtable */
	, &SelectionBaseAttribute_t498__CustomAttributeCache/* custom_attributes_cache */
	, &SelectionBaseAttribute_t498_il2cpp_TypeInfo/* cast_class */
	, &SelectionBaseAttribute_t498_0_0_0/* byval_arg */
	, &SelectionBaseAttribute_t498_1_0_0/* this_arg */
	, SelectionBaseAttribute_t498_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SelectionBaseAttribute_t498)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderState.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SliderState_t1063_il2cpp_TypeInfo;
// UnityEngine.SliderState
#include "UnityEngine_UnityEngine_SliderStateMethodDeclarations.h"

// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern MethodInfo Object__ctor_m271_MethodInfo;


// System.Void UnityEngine.SliderState::.ctor()
extern MethodInfo SliderState__ctor_m6315_MethodInfo;
 void SliderState__ctor_m6315 (SliderState_t1063 * __this, MethodInfo* method){
	{
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngine.SliderState
extern Il2CppType Single_t105_0_0_6;
FieldInfo SliderState_t1063____dragStartPos_0_FieldInfo = 
{
	"dragStartPos"/* name */
	, &Single_t105_0_0_6/* type */
	, &SliderState_t1063_il2cpp_TypeInfo/* parent */
	, offsetof(SliderState_t1063, ___dragStartPos_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_6;
FieldInfo SliderState_t1063____dragStartValue_1_FieldInfo = 
{
	"dragStartValue"/* name */
	, &Single_t105_0_0_6/* type */
	, &SliderState_t1063_il2cpp_TypeInfo/* parent */
	, offsetof(SliderState_t1063, ___dragStartValue_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_6;
FieldInfo SliderState_t1063____isDragging_2_FieldInfo = 
{
	"isDragging"/* name */
	, &Boolean_t106_0_0_6/* type */
	, &SliderState_t1063_il2cpp_TypeInfo/* parent */
	, offsetof(SliderState_t1063, ___isDragging_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* SliderState_t1063_FieldInfos[] =
{
	&SliderState_t1063____dragStartPos_0_FieldInfo,
	&SliderState_t1063____dragStartValue_1_FieldInfo,
	&SliderState_t1063____isDragging_2_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SliderState::.ctor()
MethodInfo SliderState__ctor_m6315_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SliderState__ctor_m6315/* method */
	, &SliderState_t1063_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1438/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SliderState_t1063_MethodInfos[] =
{
	&SliderState__ctor_m6315_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m304_MethodInfo;
extern MethodInfo Object_GetHashCode_m305_MethodInfo;
static MethodInfo* SliderState_t1063_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SliderState_t1063_0_0_0;
extern Il2CppType SliderState_t1063_1_0_0;
extern TypeInfo Object_t_il2cpp_TypeInfo;
struct SliderState_t1063;
TypeInfo SliderState_t1063_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SliderState"/* name */
	, "UnityEngine"/* namespaze */
	, SliderState_t1063_MethodInfos/* methods */
	, NULL/* properties */
	, SliderState_t1063_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SliderState_t1063_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SliderState_t1063_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SliderState_t1063_il2cpp_TypeInfo/* cast_class */
	, &SliderState_t1063_0_0_0/* byval_arg */
	, &SliderState_t1063_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SliderState_t1063)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtility.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo StackTraceUtility_t1064_il2cpp_TypeInfo;
// UnityEngine.StackTraceUtility
#include "UnityEngine_UnityEngine_StackTraceUtilityMethodDeclarations.h"

// System.Diagnostics.StackTrace
#include "mscorlib_System_Diagnostics_StackTrace.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.Exception
#include "mscorlib_System_Exception.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilder.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfo.h"
#include "mscorlib_ArrayTypes.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.Diagnostics.StackFrame
#include "mscorlib_System_Diagnostics_StackFrame.h"
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBase.h"
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfo.h"
extern TypeInfo String_t_il2cpp_TypeInfo;
extern TypeInfo StackTrace_t1065_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t507_il2cpp_TypeInfo;
extern TypeInfo Exception_t152_il2cpp_TypeInfo;
extern TypeInfo StringBuilder_t418_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo MemberInfo_t145_il2cpp_TypeInfo;
extern TypeInfo CharU5BU5D_t108_il2cpp_TypeInfo;
extern TypeInfo Char_t109_il2cpp_TypeInfo;
extern TypeInfo StackFrame_t1166_il2cpp_TypeInfo;
extern TypeInfo MethodBase_t1167_il2cpp_TypeInfo;
extern TypeInfo ParameterInfoU5BU5D_t1168_il2cpp_TypeInfo;
extern TypeInfo ParameterInfo_t1169_il2cpp_TypeInfo;
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// System.Diagnostics.StackTrace
#include "mscorlib_System_Diagnostics_StackTraceMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// System.Exception
#include "mscorlib_System_ExceptionMethodDeclarations.h"
// System.Text.StringBuilder
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
// System.Reflection.MemberInfo
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
// System.Diagnostics.StackFrame
#include "mscorlib_System_Diagnostics_StackFrameMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Reflection.MethodBase
#include "mscorlib_System_Reflection_MethodBaseMethodDeclarations.h"
// System.Reflection.ParameterInfo
#include "mscorlib_System_Reflection_ParameterInfoMethodDeclarations.h"
// System.Int32
#include "mscorlib_System_Int32MethodDeclarations.h"
extern MethodInfo StackTrace__ctor_m6509_MethodInfo;
extern MethodInfo StackTraceUtility_ExtractFormattedStackTrace_m6324_MethodInfo;
extern MethodInfo String_ToString_m6510_MethodInfo;
extern MethodInfo String_StartsWith_m6511_MethodInfo;
extern MethodInfo StackTraceUtility_ExtractStringFromExceptionInternal_m6322_MethodInfo;
extern MethodInfo String_Concat_m418_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2468_MethodInfo;
extern MethodInfo Exception_get_StackTrace_m6438_MethodInfo;
extern MethodInfo String_get_Length_m2255_MethodInfo;
extern MethodInfo StringBuilder__ctor_m4616_MethodInfo;
extern MethodInfo Exception_GetType_m6439_MethodInfo;
extern MethodInfo MemberInfo_get_Name_m6512_MethodInfo;
extern MethodInfo Exception_get_Message_m2227_MethodInfo;
extern MethodInfo String_Trim_m6513_MethodInfo;
extern MethodInfo String_Concat_m282_MethodInfo;
extern MethodInfo Exception_get_InnerException_m6436_MethodInfo;
extern MethodInfo String_Concat_m4591_MethodInfo;
extern MethodInfo StringBuilder_Append_m6514_MethodInfo;
extern MethodInfo StringBuilder_ToString_m1897_MethodInfo;
extern MethodInfo String_Split_m293_MethodInfo;
extern MethodInfo String_get_Chars_m292_MethodInfo;
extern MethodInfo StackTraceUtility_IsSystemStacktraceType_m6320_MethodInfo;
extern MethodInfo String_IndexOf_m6515_MethodInfo;
extern MethodInfo String_Substring_m2281_MethodInfo;
extern MethodInfo String_EndsWith_m4556_MethodInfo;
extern MethodInfo String_Remove_m2305_MethodInfo;
extern MethodInfo String_IndexOf_m6516_MethodInfo;
extern MethodInfo String_Replace_m291_MethodInfo;
extern MethodInfo String_Replace_m6517_MethodInfo;
extern MethodInfo String_LastIndexOf_m6518_MethodInfo;
extern MethodInfo String_Insert_m2307_MethodInfo;
extern MethodInfo StackTrace_GetFrame_m6519_MethodInfo;
extern MethodInfo StackFrame_GetMethod_m6520_MethodInfo;
extern MethodInfo MemberInfo_get_DeclaringType_m6521_MethodInfo;
extern MethodInfo Type_get_Namespace_m6522_MethodInfo;
extern MethodInfo MethodBase_GetParameters_m6523_MethodInfo;
extern MethodInfo ParameterInfo_get_ParameterType_m6524_MethodInfo;
extern MethodInfo StackFrame_GetFileName_m6525_MethodInfo;
extern MethodInfo String_op_Equality_m227_MethodInfo;
extern MethodInfo StackFrame_GetFileLineNumber_m6526_MethodInfo;
extern MethodInfo Int32_ToString_m281_MethodInfo;
extern MethodInfo StackTrace_get_FrameCount_m6527_MethodInfo;


// System.Void UnityEngine.StackTraceUtility::.ctor()
extern MethodInfo StackTraceUtility__ctor_m6316_MethodInfo;
 void StackTraceUtility__ctor_m6316 (StackTraceUtility_t1064 * __this, MethodInfo* method){
	{
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.StackTraceUtility::.cctor()
extern MethodInfo StackTraceUtility__cctor_m6317_MethodInfo;
 void StackTraceUtility__cctor_m6317 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		((StackTraceUtility_t1064_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo)->static_fields)->___projectFolder_0 = (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		return;
	}
}
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern MethodInfo StackTraceUtility_SetProjectFolder_m6318_MethodInfo;
 void StackTraceUtility_SetProjectFolder_m6318 (Object_t * __this/* static, unused */, String_t* ___folder, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo));
		((StackTraceUtility_t1064_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo)->static_fields)->___projectFolder_0 = ___folder;
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern MethodInfo StackTraceUtility_ExtractStackTrace_m6319_MethodInfo;
 String_t* StackTraceUtility_ExtractStackTrace_m6319 (Object_t * __this/* static, unused */, MethodInfo* method){
	StackTrace_t1065 * V_0 = {0};
	String_t* V_1 = {0};
	{
		StackTrace_t1065 * L_0 = (StackTrace_t1065 *)il2cpp_codegen_object_new (InitializedTypeInfo(&StackTrace_t1065_il2cpp_TypeInfo));
		StackTrace__ctor_m6509(L_0, 1, 1, /*hidden argument*/&StackTrace__ctor_m6509_MethodInfo);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo));
		String_t* L_1 = StackTraceUtility_ExtractFormattedStackTrace_m6324(NULL /*static, unused*/, V_0, /*hidden argument*/&StackTraceUtility_ExtractFormattedStackTrace_m6324_MethodInfo);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&String_ToString_m6510_MethodInfo, L_1);
		V_1 = L_2;
		return V_1;
	}
}
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
 bool StackTraceUtility_IsSystemStacktraceType_m6320 (Object_t * __this/* static, unused */, Object_t * ___name, MethodInfo* method){
	String_t* V_0 = {0};
	int32_t G_B7_0 = 0;
	{
		V_0 = ((String_t*)Castclass(___name, (&String_t_il2cpp_TypeInfo)));
		NullCheck(V_0);
		bool L_0 = String_StartsWith_m6511(V_0, (String_t*) &_stringLiteral408, /*hidden argument*/&String_StartsWith_m6511_MethodInfo);
		if (L_0)
		{
			goto IL_0064;
		}
	}
	{
		NullCheck(V_0);
		bool L_1 = String_StartsWith_m6511(V_0, (String_t*) &_stringLiteral409, /*hidden argument*/&String_StartsWith_m6511_MethodInfo);
		if (L_1)
		{
			goto IL_0064;
		}
	}
	{
		NullCheck(V_0);
		bool L_2 = String_StartsWith_m6511(V_0, (String_t*) &_stringLiteral410, /*hidden argument*/&String_StartsWith_m6511_MethodInfo);
		if (L_2)
		{
			goto IL_0064;
		}
	}
	{
		NullCheck(V_0);
		bool L_3 = String_StartsWith_m6511(V_0, (String_t*) &_stringLiteral411, /*hidden argument*/&String_StartsWith_m6511_MethodInfo);
		if (L_3)
		{
			goto IL_0064;
		}
	}
	{
		NullCheck(V_0);
		bool L_4 = String_StartsWith_m6511(V_0, (String_t*) &_stringLiteral412, /*hidden argument*/&String_StartsWith_m6511_MethodInfo);
		if (L_4)
		{
			goto IL_0064;
		}
	}
	{
		NullCheck(V_0);
		bool L_5 = String_StartsWith_m6511(V_0, (String_t*) &_stringLiteral413, /*hidden argument*/&String_StartsWith_m6511_MethodInfo);
		G_B7_0 = ((int32_t)(L_5));
		goto IL_0065;
	}

IL_0064:
	{
		G_B7_0 = 1;
	}

IL_0065:
	{
		return G_B7_0;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractStringFromException(System.Object)
extern MethodInfo StackTraceUtility_ExtractStringFromException_m6321_MethodInfo;
 String_t* StackTraceUtility_ExtractStringFromException_m6321 (Object_t * __this/* static, unused */, Object_t * ___exception, MethodInfo* method){
	String_t* V_0 = {0};
	String_t* V_1 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		V_0 = (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		V_1 = (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo));
		StackTraceUtility_ExtractStringFromExceptionInternal_m6322(NULL /*static, unused*/, ___exception, (&V_0), (&V_1), /*hidden argument*/&StackTraceUtility_ExtractStringFromExceptionInternal_m6322_MethodInfo);
		String_t* L_0 = String_Concat_m418(NULL /*static, unused*/, V_0, (String_t*) &_stringLiteral316, V_1, /*hidden argument*/&String_Concat_m418_MethodInfo);
		return L_0;
	}
}
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
 void StackTraceUtility_ExtractStringFromExceptionInternal_m6322 (Object_t * __this/* static, unused */, Object_t * ___exceptiono, String_t** ___message, String_t** ___stackTrace, MethodInfo* method){
	Exception_t152 * V_0 = {0};
	StringBuilder_t418 * V_1 = {0};
	String_t* V_2 = {0};
	String_t* V_3 = {0};
	String_t* V_4 = {0};
	StackTrace_t1065 * V_5 = {0};
	int32_t G_B7_0 = 0;
	{
		if (___exceptiono)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentException_t507 * L_0 = (ArgumentException_t507 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t507_il2cpp_TypeInfo));
		ArgumentException__ctor_m2468(L_0, (String_t*) &_stringLiteral414, /*hidden argument*/&ArgumentException__ctor_m2468_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0011:
	{
		V_0 = ((Exception_t152 *)IsInst(___exceptiono, InitializedTypeInfo(&Exception_t152_il2cpp_TypeInfo)));
		if (V_0)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentException_t507 * L_1 = (ArgumentException_t507 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t507_il2cpp_TypeInfo));
		ArgumentException__ctor_m2468(L_1, (String_t*) &_stringLiteral415, /*hidden argument*/&ArgumentException__ctor_m2468_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0029:
	{
		NullCheck(V_0);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Exception_get_StackTrace_m6438_MethodInfo, V_0);
		if (L_2)
		{
			goto IL_003e;
		}
	}
	{
		G_B7_0 = ((int32_t)512);
		goto IL_004b;
	}

IL_003e:
	{
		NullCheck(V_0);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Exception_get_StackTrace_m6438_MethodInfo, V_0);
		NullCheck(L_3);
		int32_t L_4 = String_get_Length_m2255(L_3, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		G_B7_0 = ((int32_t)((int32_t)L_4*(int32_t)2));
	}

IL_004b:
	{
		StringBuilder_t418 * L_5 = (StringBuilder_t418 *)il2cpp_codegen_object_new (InitializedTypeInfo(&StringBuilder_t418_il2cpp_TypeInfo));
		StringBuilder__ctor_m4616(L_5, G_B7_0, /*hidden argument*/&StringBuilder__ctor_m4616_MethodInfo);
		V_1 = L_5;
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		*((Object_t **)(___message)) = (Object_t *)(((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		V_2 = (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		goto IL_00ff;
	}

IL_0063:
	{
		NullCheck(V_2);
		int32_t L_6 = String_get_Length_m2255(V_2, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		if (L_6)
		{
			goto IL_007a;
		}
	}
	{
		NullCheck(V_0);
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Exception_get_StackTrace_m6438_MethodInfo, V_0);
		V_2 = L_7;
		goto IL_008c;
	}

IL_007a:
	{
		NullCheck(V_0);
		String_t* L_8 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Exception_get_StackTrace_m6438_MethodInfo, V_0);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_9 = String_Concat_m418(NULL /*static, unused*/, L_8, (String_t*) &_stringLiteral316, V_2, /*hidden argument*/&String_Concat_m418_MethodInfo);
		V_2 = L_9;
	}

IL_008c:
	{
		NullCheck(V_0);
		Type_t * L_10 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(&Exception_GetType_m6439_MethodInfo, V_0);
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&MemberInfo_get_Name_m6512_MethodInfo, L_10);
		V_3 = L_11;
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		V_4 = (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
		NullCheck(V_0);
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Exception_get_Message_m2227_MethodInfo, V_0);
		if (!L_12)
		{
			goto IL_00b2;
		}
	}
	{
		NullCheck(V_0);
		String_t* L_13 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Exception_get_Message_m2227_MethodInfo, V_0);
		V_4 = L_13;
	}

IL_00b2:
	{
		NullCheck(V_4);
		String_t* L_14 = String_Trim_m6513(V_4, /*hidden argument*/&String_Trim_m6513_MethodInfo);
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m2255(L_14, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		if (!L_15)
		{
			goto IL_00d8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_16 = String_Concat_m282(NULL /*static, unused*/, V_3, (String_t*) &_stringLiteral416, /*hidden argument*/&String_Concat_m282_MethodInfo);
		V_3 = L_16;
		String_t* L_17 = String_Concat_m282(NULL /*static, unused*/, V_3, V_4, /*hidden argument*/&String_Concat_m282_MethodInfo);
		V_3 = L_17;
	}

IL_00d8:
	{
		*((Object_t **)(___message)) = (Object_t *)V_3;
		NullCheck(V_0);
		Exception_t152 * L_18 = (Exception_t152 *)VirtFuncInvoker0< Exception_t152 * >::Invoke(&Exception_get_InnerException_m6436_MethodInfo, V_0);
		if (!L_18)
		{
			goto IL_00f8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_19 = String_Concat_m4591(NULL /*static, unused*/, (String_t*) &_stringLiteral417, V_3, (String_t*) &_stringLiteral316, V_2, /*hidden argument*/&String_Concat_m4591_MethodInfo);
		V_2 = L_19;
	}

IL_00f8:
	{
		NullCheck(V_0);
		Exception_t152 * L_20 = (Exception_t152 *)VirtFuncInvoker0< Exception_t152 * >::Invoke(&Exception_get_InnerException_m6436_MethodInfo, V_0);
		V_0 = L_20;
	}

IL_00ff:
	{
		if (V_0)
		{
			goto IL_0063;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_21 = String_Concat_m282(NULL /*static, unused*/, V_2, (String_t*) &_stringLiteral316, /*hidden argument*/&String_Concat_m282_MethodInfo);
		NullCheck(V_1);
		StringBuilder_Append_m6514(V_1, L_21, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
		StackTrace_t1065 * L_22 = (StackTrace_t1065 *)il2cpp_codegen_object_new (InitializedTypeInfo(&StackTrace_t1065_il2cpp_TypeInfo));
		StackTrace__ctor_m6509(L_22, 1, 1, /*hidden argument*/&StackTrace__ctor_m6509_MethodInfo);
		V_5 = L_22;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo));
		String_t* L_23 = StackTraceUtility_ExtractFormattedStackTrace_m6324(NULL /*static, unused*/, V_5, /*hidden argument*/&StackTraceUtility_ExtractFormattedStackTrace_m6324_MethodInfo);
		NullCheck(V_1);
		StringBuilder_Append_m6514(V_1, L_23, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
		NullCheck(V_1);
		String_t* L_24 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&StringBuilder_ToString_m1897_MethodInfo, V_1);
		*((Object_t **)(___stackTrace)) = (Object_t *)L_24;
		return;
	}
}
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
extern MethodInfo StackTraceUtility_PostprocessStacktrace_m6323_MethodInfo;
 String_t* StackTraceUtility_PostprocessStacktrace_m6323 (Object_t * __this/* static, unused */, String_t* ___oldString, bool ___stripEngineInternalInformation, MethodInfo* method){
	StringU5BU5D_t112* V_0 = {0};
	StringBuilder_t418 * V_1 = {0};
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = {0};
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		if (___oldString)
		{
			goto IL_000c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		return (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2);
	}

IL_000c:
	{
		CharU5BU5D_t108* L_0 = ((CharU5BU5D_t108*)SZArrayNew(InitializedTypeInfo(&CharU5BU5D_t108_il2cpp_TypeInfo), 1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_0, 0)) = (uint16_t)((int32_t)10);
		NullCheck(___oldString);
		StringU5BU5D_t112* L_1 = String_Split_m293(___oldString, L_0, /*hidden argument*/&String_Split_m293_MethodInfo);
		V_0 = L_1;
		NullCheck(___oldString);
		int32_t L_2 = String_get_Length_m2255(___oldString, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		StringBuilder_t418 * L_3 = (StringBuilder_t418 *)il2cpp_codegen_object_new (InitializedTypeInfo(&StringBuilder_t418_il2cpp_TypeInfo));
		StringBuilder__ctor_m4616(L_3, L_2, /*hidden argument*/&StringBuilder__ctor_m4616_MethodInfo);
		V_1 = L_3;
		V_2 = 0;
		goto IL_0040;
	}

IL_0031:
	{
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, V_2);
		int32_t L_4 = V_2;
		NullCheck((*(String_t**)(String_t**)SZArrayLdElema(V_0, L_4)));
		String_t* L_5 = String_Trim_m6513((*(String_t**)(String_t**)SZArrayLdElema(V_0, L_4)), /*hidden argument*/&String_Trim_m6513_MethodInfo);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, V_2);
		ArrayElementTypeCheck (V_0, L_5);
		*((String_t**)(String_t**)SZArrayLdElema(V_0, V_2)) = (String_t*)L_5;
		V_2 = ((int32_t)(V_2+1));
	}

IL_0040:
	{
		NullCheck(V_0);
		if ((((int32_t)V_2) < ((int32_t)(((int32_t)(((Array_t *)V_0)->max_length))))))
		{
			goto IL_0031;
		}
	}
	{
		V_3 = 0;
		goto IL_0265;
	}

IL_0050:
	{
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, V_3);
		int32_t L_6 = V_3;
		V_4 = (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_6));
		NullCheck(V_4);
		int32_t L_7 = String_get_Length_m2255(V_4, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		if (!L_7)
		{
			goto IL_0070;
		}
	}
	{
		NullCheck(V_4);
		uint16_t L_8 = String_get_Chars_m292(V_4, 0, /*hidden argument*/&String_get_Chars_m292_MethodInfo);
		if ((((uint32_t)L_8) != ((uint32_t)((int32_t)10))))
		{
			goto IL_0075;
		}
	}

IL_0070:
	{
		goto IL_0261;
	}

IL_0075:
	{
		NullCheck(V_4);
		bool L_9 = String_StartsWith_m6511(V_4, (String_t*) &_stringLiteral418, /*hidden argument*/&String_StartsWith_m6511_MethodInfo);
		if (!L_9)
		{
			goto IL_008b;
		}
	}
	{
		goto IL_0261;
	}

IL_008b:
	{
		if (!___stripEngineInternalInformation)
		{
			goto IL_00a7;
		}
	}
	{
		NullCheck(V_4);
		bool L_10 = String_StartsWith_m6511(V_4, (String_t*) &_stringLiteral419, /*hidden argument*/&String_StartsWith_m6511_MethodInfo);
		if (!L_10)
		{
			goto IL_00a7;
		}
	}
	{
		goto IL_026e;
	}

IL_00a7:
	{
		if (!___stripEngineInternalInformation)
		{
			goto IL_00fa;
		}
	}
	{
		NullCheck(V_0);
		if ((((int32_t)V_3) >= ((int32_t)((int32_t)((((int32_t)(((Array_t *)V_0)->max_length)))-1)))))
		{
			goto IL_00fa;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo));
		bool L_11 = StackTraceUtility_IsSystemStacktraceType_m6320(NULL /*static, unused*/, V_4, /*hidden argument*/&StackTraceUtility_IsSystemStacktraceType_m6320_MethodInfo);
		if (!L_11)
		{
			goto IL_00fa;
		}
	}
	{
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ((int32_t)(V_3+1)));
		int32_t L_12 = ((int32_t)(V_3+1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo));
		bool L_13 = StackTraceUtility_IsSystemStacktraceType_m6320(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_12)), /*hidden argument*/&StackTraceUtility_IsSystemStacktraceType_m6320_MethodInfo);
		if (!L_13)
		{
			goto IL_00d8;
		}
	}
	{
		goto IL_0261;
	}

IL_00d8:
	{
		NullCheck(V_4);
		int32_t L_14 = String_IndexOf_m6515(V_4, (String_t*) &_stringLiteral420, /*hidden argument*/&String_IndexOf_m6515_MethodInfo);
		V_5 = L_14;
		if ((((int32_t)V_5) == ((int32_t)(-1))))
		{
			goto IL_00fa;
		}
	}
	{
		NullCheck(V_4);
		String_t* L_15 = String_Substring_m2281(V_4, 0, V_5, /*hidden argument*/&String_Substring_m2281_MethodInfo);
		V_4 = L_15;
	}

IL_00fa:
	{
		NullCheck(V_4);
		int32_t L_16 = String_IndexOf_m6515(V_4, (String_t*) &_stringLiteral421, /*hidden argument*/&String_IndexOf_m6515_MethodInfo);
		if ((((int32_t)L_16) == ((int32_t)(-1))))
		{
			goto IL_0111;
		}
	}
	{
		goto IL_0261;
	}

IL_0111:
	{
		NullCheck(V_4);
		int32_t L_17 = String_IndexOf_m6515(V_4, (String_t*) &_stringLiteral422, /*hidden argument*/&String_IndexOf_m6515_MethodInfo);
		if ((((int32_t)L_17) == ((int32_t)(-1))))
		{
			goto IL_0128;
		}
	}
	{
		goto IL_0261;
	}

IL_0128:
	{
		NullCheck(V_4);
		int32_t L_18 = String_IndexOf_m6515(V_4, (String_t*) &_stringLiteral423, /*hidden argument*/&String_IndexOf_m6515_MethodInfo);
		if ((((int32_t)L_18) == ((int32_t)(-1))))
		{
			goto IL_013f;
		}
	}
	{
		goto IL_0261;
	}

IL_013f:
	{
		if (!___stripEngineInternalInformation)
		{
			goto IL_016c;
		}
	}
	{
		NullCheck(V_4);
		bool L_19 = String_StartsWith_m6511(V_4, (String_t*) &_stringLiteral424, /*hidden argument*/&String_StartsWith_m6511_MethodInfo);
		if (!L_19)
		{
			goto IL_016c;
		}
	}
	{
		NullCheck(V_4);
		bool L_20 = String_EndsWith_m4556(V_4, (String_t*) &_stringLiteral425, /*hidden argument*/&String_EndsWith_m4556_MethodInfo);
		if (!L_20)
		{
			goto IL_016c;
		}
	}
	{
		goto IL_0261;
	}

IL_016c:
	{
		NullCheck(V_4);
		bool L_21 = String_StartsWith_m6511(V_4, (String_t*) &_stringLiteral426, /*hidden argument*/&String_StartsWith_m6511_MethodInfo);
		if (!L_21)
		{
			goto IL_0188;
		}
	}
	{
		NullCheck(V_4);
		String_t* L_22 = String_Remove_m2305(V_4, 0, 3, /*hidden argument*/&String_Remove_m2305_MethodInfo);
		V_4 = L_22;
	}

IL_0188:
	{
		NullCheck(V_4);
		int32_t L_23 = String_IndexOf_m6515(V_4, (String_t*) &_stringLiteral427, /*hidden argument*/&String_IndexOf_m6515_MethodInfo);
		V_6 = L_23;
		V_7 = (-1);
		if ((((int32_t)V_6) == ((int32_t)(-1))))
		{
			goto IL_01b1;
		}
	}
	{
		NullCheck(V_4);
		int32_t L_24 = String_IndexOf_m6516(V_4, (String_t*) &_stringLiteral425, V_6, /*hidden argument*/&String_IndexOf_m6516_MethodInfo);
		V_7 = L_24;
	}

IL_01b1:
	{
		if ((((int32_t)V_6) == ((int32_t)(-1))))
		{
			goto IL_01d4;
		}
	}
	{
		if ((((int32_t)V_7) <= ((int32_t)V_6)))
		{
			goto IL_01d4;
		}
	}
	{
		NullCheck(V_4);
		String_t* L_25 = String_Remove_m2305(V_4, V_6, ((int32_t)(((int32_t)(V_7-V_6))+1)), /*hidden argument*/&String_Remove_m2305_MethodInfo);
		V_4 = L_25;
	}

IL_01d4:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		NullCheck(V_4);
		String_t* L_26 = String_Replace_m291(V_4, (String_t*) &_stringLiteral428, (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), /*hidden argument*/&String_Replace_m291_MethodInfo);
		V_4 = L_26;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo));
		NullCheck(V_4);
		String_t* L_27 = String_Replace_m291(V_4, (((StackTraceUtility_t1064_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo)->static_fields)->___projectFolder_0), (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), /*hidden argument*/&String_Replace_m291_MethodInfo);
		V_4 = L_27;
		NullCheck(V_4);
		String_t* L_28 = String_Replace_m6517(V_4, ((int32_t)92), ((int32_t)47), /*hidden argument*/&String_Replace_m6517_MethodInfo);
		V_4 = L_28;
		NullCheck(V_4);
		int32_t L_29 = String_LastIndexOf_m6518(V_4, (String_t*) &_stringLiteral429, /*hidden argument*/&String_LastIndexOf_m6518_MethodInfo);
		V_8 = L_29;
		if ((((int32_t)V_8) == ((int32_t)(-1))))
		{
			goto IL_024e;
		}
	}
	{
		NullCheck(V_4);
		String_t* L_30 = String_Remove_m2305(V_4, V_8, 5, /*hidden argument*/&String_Remove_m2305_MethodInfo);
		V_4 = L_30;
		NullCheck(V_4);
		String_t* L_31 = String_Insert_m2307(V_4, V_8, (String_t*) &_stringLiteral430, /*hidden argument*/&String_Insert_m2307_MethodInfo);
		V_4 = L_31;
		NullCheck(V_4);
		int32_t L_32 = String_get_Length_m2255(V_4, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		NullCheck(V_4);
		String_t* L_33 = String_Insert_m2307(V_4, L_32, (String_t*) &_stringLiteral7, /*hidden argument*/&String_Insert_m2307_MethodInfo);
		V_4 = L_33;
	}

IL_024e:
	{
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_34 = String_Concat_m282(NULL /*static, unused*/, V_4, (String_t*) &_stringLiteral316, /*hidden argument*/&String_Concat_m282_MethodInfo);
		NullCheck(V_1);
		StringBuilder_Append_m6514(V_1, L_34, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
	}

IL_0261:
	{
		V_3 = ((int32_t)(V_3+1));
	}

IL_0265:
	{
		NullCheck(V_0);
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((Array_t *)V_0)->max_length))))))
		{
			goto IL_0050;
		}
	}

IL_026e:
	{
		NullCheck(V_1);
		String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&StringBuilder_ToString_m1897_MethodInfo, V_1);
		return L_35;
	}
}
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
 String_t* StackTraceUtility_ExtractFormattedStackTrace_m6324 (Object_t * __this/* static, unused */, StackTrace_t1065 * ___stackTrace, MethodInfo* method){
	StringBuilder_t418 * V_0 = {0};
	int32_t V_1 = 0;
	StackFrame_t1166 * V_2 = {0};
	MethodBase_t1167 * V_3 = {0};
	Type_t * V_4 = {0};
	String_t* V_5 = {0};
	int32_t V_6 = 0;
	ParameterInfoU5BU5D_t1168* V_7 = {0};
	bool V_8 = false;
	String_t* V_9 = {0};
	int32_t V_10 = 0;
	{
		StringBuilder_t418 * L_0 = (StringBuilder_t418 *)il2cpp_codegen_object_new (InitializedTypeInfo(&StringBuilder_t418_il2cpp_TypeInfo));
		StringBuilder__ctor_m4616(L_0, ((int32_t)255), /*hidden argument*/&StringBuilder__ctor_m4616_MethodInfo);
		V_0 = L_0;
		V_1 = 0;
		goto IL_01c9;
	}

IL_0012:
	{
		NullCheck(___stackTrace);
		StackFrame_t1166 * L_1 = (StackFrame_t1166 *)VirtFuncInvoker1< StackFrame_t1166 *, int32_t >::Invoke(&StackTrace_GetFrame_m6519_MethodInfo, ___stackTrace, V_1);
		V_2 = L_1;
		NullCheck(V_2);
		MethodBase_t1167 * L_2 = (MethodBase_t1167 *)VirtFuncInvoker0< MethodBase_t1167 * >::Invoke(&StackFrame_GetMethod_m6520_MethodInfo, V_2);
		V_3 = L_2;
		if (V_3)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_01c5;
	}

IL_002c:
	{
		NullCheck(V_3);
		Type_t * L_3 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(&MemberInfo_get_DeclaringType_m6521_MethodInfo, V_3);
		V_4 = L_3;
		if (V_4)
		{
			goto IL_0040;
		}
	}
	{
		goto IL_01c5;
	}

IL_0040:
	{
		NullCheck(V_4);
		String_t* L_4 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Type_get_Namespace_m6522_MethodInfo, V_4);
		V_5 = L_4;
		if (!V_5)
		{
			goto IL_0071;
		}
	}
	{
		NullCheck(V_5);
		int32_t L_5 = String_get_Length_m2255(V_5, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		if (!L_5)
		{
			goto IL_0071;
		}
	}
	{
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, V_5, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, (String_t*) &_stringLiteral98, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
	}

IL_0071:
	{
		NullCheck(V_4);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&MemberInfo_get_Name_m6512_MethodInfo, V_4);
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, L_6, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, (String_t*) &_stringLiteral431, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
		NullCheck(V_3);
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&MemberInfo_get_Name_m6512_MethodInfo, V_3);
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, L_7, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, (String_t*) &_stringLiteral9, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
		V_6 = 0;
		NullCheck(V_3);
		ParameterInfoU5BU5D_t1168* L_8 = (ParameterInfoU5BU5D_t1168*)VirtFuncInvoker0< ParameterInfoU5BU5D_t1168* >::Invoke(&MethodBase_GetParameters_m6523_MethodInfo, V_3);
		V_7 = L_8;
		V_8 = 1;
		goto IL_00ee;
	}

IL_00b7:
	{
		if (V_8)
		{
			goto IL_00cf;
		}
	}
	{
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, (String_t*) &_stringLiteral432, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
		goto IL_00d2;
	}

IL_00cf:
	{
		V_8 = 0;
	}

IL_00d2:
	{
		NullCheck(V_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_7, V_6);
		int32_t L_9 = V_6;
		NullCheck((*(ParameterInfo_t1169 **)(ParameterInfo_t1169 **)SZArrayLdElema(V_7, L_9)));
		Type_t * L_10 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(&ParameterInfo_get_ParameterType_m6524_MethodInfo, (*(ParameterInfo_t1169 **)(ParameterInfo_t1169 **)SZArrayLdElema(V_7, L_9)));
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&MemberInfo_get_Name_m6512_MethodInfo, L_10);
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, L_11, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
		V_6 = ((int32_t)(V_6+1));
	}

IL_00ee:
	{
		NullCheck(V_7);
		if ((((int32_t)V_6) < ((int32_t)(((int32_t)(((Array_t *)V_7)->max_length))))))
		{
			goto IL_00b7;
		}
	}
	{
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, (String_t*) &_stringLiteral7, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
		NullCheck(V_2);
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&StackFrame_GetFileName_m6525_MethodInfo, V_2);
		V_9 = L_12;
		if (!V_9)
		{
			goto IL_01b9;
		}
	}
	{
		NullCheck(V_4);
		String_t* L_13 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&MemberInfo_get_Name_m6512_MethodInfo, V_4);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_14 = String_op_Equality_m227(NULL /*static, unused*/, L_13, (String_t*) &_stringLiteral433, /*hidden argument*/&String_op_Equality_m227_MethodInfo);
		if (!L_14)
		{
			goto IL_0140;
		}
	}
	{
		NullCheck(V_4);
		String_t* L_15 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Type_get_Namespace_m6522_MethodInfo, V_4);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_16 = String_op_Equality_m227(NULL /*static, unused*/, L_15, (String_t*) &_stringLiteral434, /*hidden argument*/&String_op_Equality_m227_MethodInfo);
		if (L_16)
		{
			goto IL_01b9;
		}
	}

IL_0140:
	{
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, (String_t*) &_stringLiteral430, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo));
		NullCheck(V_9);
		bool L_17 = String_StartsWith_m6511(V_9, (((StackTraceUtility_t1064_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo)->static_fields)->___projectFolder_0), /*hidden argument*/&String_StartsWith_m6511_MethodInfo);
		if (!L_17)
		{
			goto IL_0182;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo));
		NullCheck((((StackTraceUtility_t1064_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo)->static_fields)->___projectFolder_0));
		int32_t L_18 = String_get_Length_m2255((((StackTraceUtility_t1064_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo)->static_fields)->___projectFolder_0), /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		NullCheck(V_9);
		int32_t L_19 = String_get_Length_m2255(V_9, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		NullCheck((((StackTraceUtility_t1064_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo)->static_fields)->___projectFolder_0));
		int32_t L_20 = String_get_Length_m2255((((StackTraceUtility_t1064_StaticFields*)InitializedTypeInfo(&StackTraceUtility_t1064_il2cpp_TypeInfo)->static_fields)->___projectFolder_0), /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		NullCheck(V_9);
		String_t* L_21 = String_Substring_m2281(V_9, L_18, ((int32_t)(L_19-L_20)), /*hidden argument*/&String_Substring_m2281_MethodInfo);
		V_9 = L_21;
	}

IL_0182:
	{
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, V_9, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, (String_t*) &_stringLiteral431, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
		NullCheck(V_2);
		int32_t L_22 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&StackFrame_GetFileLineNumber_m6526_MethodInfo, V_2);
		V_10 = L_22;
		String_t* L_23 = Int32_ToString_m281((&V_10), /*hidden argument*/&Int32_ToString_m281_MethodInfo);
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, L_23, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, (String_t*) &_stringLiteral7, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
	}

IL_01b9:
	{
		NullCheck(V_0);
		StringBuilder_Append_m6514(V_0, (String_t*) &_stringLiteral316, /*hidden argument*/&StringBuilder_Append_m6514_MethodInfo);
	}

IL_01c5:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_01c9:
	{
		NullCheck(___stackTrace);
		int32_t L_24 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&StackTrace_get_FrameCount_m6527_MethodInfo, ___stackTrace);
		if ((((int32_t)V_1) < ((int32_t)L_24)))
		{
			goto IL_0012;
		}
	}
	{
		NullCheck(V_0);
		String_t* L_25 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&StringBuilder_ToString_m1897_MethodInfo, V_0);
		return L_25;
	}
}
// Metadata Definition UnityEngine.StackTraceUtility
extern Il2CppType String_t_0_0_17;
FieldInfo StackTraceUtility_t1064____projectFolder_0_FieldInfo = 
{
	"projectFolder"/* name */
	, &String_t_0_0_17/* type */
	, &StackTraceUtility_t1064_il2cpp_TypeInfo/* parent */
	, offsetof(StackTraceUtility_t1064_StaticFields, ___projectFolder_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* StackTraceUtility_t1064_FieldInfos[] =
{
	&StackTraceUtility_t1064____projectFolder_0_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::.ctor()
MethodInfo StackTraceUtility__ctor_m6316_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StackTraceUtility__ctor_m6316/* method */
	, &StackTraceUtility_t1064_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1439/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::.cctor()
MethodInfo StackTraceUtility__cctor_m6317_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&StackTraceUtility__cctor_m6317/* method */
	, &StackTraceUtility_t1064_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6161/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1440/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo StackTraceUtility_t1064_StackTraceUtility_SetProjectFolder_m6318_ParameterInfos[] = 
{
	{"folder", 0, 134219246, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
MethodInfo StackTraceUtility_SetProjectFolder_m6318_MethodInfo = 
{
	"SetProjectFolder"/* name */
	, (methodPointerType)&StackTraceUtility_SetProjectFolder_m6318/* method */
	, &StackTraceUtility_t1064_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, StackTraceUtility_t1064_StackTraceUtility_SetProjectFolder_m6318_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1441/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache StackTraceUtility_t1064__CustomAttributeCache_StackTraceUtility_ExtractStackTrace_m6319;
// System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
MethodInfo StackTraceUtility_ExtractStackTrace_m6319_MethodInfo = 
{
	"ExtractStackTrace"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStackTrace_m6319/* method */
	, &StackTraceUtility_t1064_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &StackTraceUtility_t1064__CustomAttributeCache_StackTraceUtility_ExtractStackTrace_m6319/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1442/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo StackTraceUtility_t1064_StackTraceUtility_IsSystemStacktraceType_m6320_ParameterInfos[] = 
{
	{"name", 0, 134219247, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.StackTraceUtility::IsSystemStacktraceType(System.Object)
MethodInfo StackTraceUtility_IsSystemStacktraceType_m6320_MethodInfo = 
{
	"IsSystemStacktraceType"/* name */
	, (methodPointerType)&StackTraceUtility_IsSystemStacktraceType_m6320/* method */
	, &StackTraceUtility_t1064_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, StackTraceUtility_t1064_StackTraceUtility_IsSystemStacktraceType_m6320_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1443/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo StackTraceUtility_t1064_StackTraceUtility_ExtractStringFromException_m6321_ParameterInfos[] = 
{
	{"exception", 0, 134219248, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::ExtractStringFromException(System.Object)
MethodInfo StackTraceUtility_ExtractStringFromException_m6321_MethodInfo = 
{
	"ExtractStringFromException"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStringFromException_m6321/* method */
	, &StackTraceUtility_t1064_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StackTraceUtility_t1064_StackTraceUtility_ExtractStringFromException_m6321_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1444/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType String_t_1_0_2;
extern Il2CppType String_t_1_0_0;
extern Il2CppType String_t_1_0_2;
static ParameterInfo StackTraceUtility_t1064_StackTraceUtility_ExtractStringFromExceptionInternal_m6322_ParameterInfos[] = 
{
	{"exceptiono", 0, 134219249, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"message", 1, 134219250, &EmptyCustomAttributesCache, &String_t_1_0_2},
	{"stackTrace", 2, 134219251, &EmptyCustomAttributesCache, &String_t_1_0_2},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_StringU26_t1170_StringU26_t1170 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache StackTraceUtility_t1064__CustomAttributeCache_StackTraceUtility_ExtractStringFromExceptionInternal_m6322;
// System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
MethodInfo StackTraceUtility_ExtractStringFromExceptionInternal_m6322_MethodInfo = 
{
	"ExtractStringFromExceptionInternal"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractStringFromExceptionInternal_m6322/* method */
	, &StackTraceUtility_t1064_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_StringU26_t1170_StringU26_t1170/* invoker_method */
	, StackTraceUtility_t1064_StackTraceUtility_ExtractStringFromExceptionInternal_m6322_ParameterInfos/* parameters */
	, &StackTraceUtility_t1064__CustomAttributeCache_StackTraceUtility_ExtractStringFromExceptionInternal_m6322/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1445/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Boolean_t106_0_0_0;
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo StackTraceUtility_t1064_StackTraceUtility_PostprocessStacktrace_m6323_ParameterInfos[] = 
{
	{"oldString", 0, 134219252, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"stripEngineInternalInformation", 1, 134219253, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_SByte_t129 (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.StackTraceUtility::PostprocessStacktrace(System.String,System.Boolean)
MethodInfo StackTraceUtility_PostprocessStacktrace_m6323_MethodInfo = 
{
	"PostprocessStacktrace"/* name */
	, (methodPointerType)&StackTraceUtility_PostprocessStacktrace_m6323/* method */
	, &StackTraceUtility_t1064_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_SByte_t129/* invoker_method */
	, StackTraceUtility_t1064_StackTraceUtility_PostprocessStacktrace_m6323_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1446/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType StackTrace_t1065_0_0_0;
extern Il2CppType StackTrace_t1065_0_0_0;
static ParameterInfo StackTraceUtility_t1064_StackTraceUtility_ExtractFormattedStackTrace_m6324_ParameterInfos[] = 
{
	{"stackTrace", 0, 134219254, &EmptyCustomAttributesCache, &StackTrace_t1065_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache StackTraceUtility_t1064__CustomAttributeCache_StackTraceUtility_ExtractFormattedStackTrace_m6324;
// System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
MethodInfo StackTraceUtility_ExtractFormattedStackTrace_m6324_MethodInfo = 
{
	"ExtractFormattedStackTrace"/* name */
	, (methodPointerType)&StackTraceUtility_ExtractFormattedStackTrace_m6324/* method */
	, &StackTraceUtility_t1064_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, StackTraceUtility_t1064_StackTraceUtility_ExtractFormattedStackTrace_m6324_ParameterInfos/* parameters */
	, &StackTraceUtility_t1064__CustomAttributeCache_StackTraceUtility_ExtractFormattedStackTrace_m6324/* custom_attributes_cache */
	, 147/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1447/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* StackTraceUtility_t1064_MethodInfos[] =
{
	&StackTraceUtility__ctor_m6316_MethodInfo,
	&StackTraceUtility__cctor_m6317_MethodInfo,
	&StackTraceUtility_SetProjectFolder_m6318_MethodInfo,
	&StackTraceUtility_ExtractStackTrace_m6319_MethodInfo,
	&StackTraceUtility_IsSystemStacktraceType_m6320_MethodInfo,
	&StackTraceUtility_ExtractStringFromException_m6321_MethodInfo,
	&StackTraceUtility_ExtractStringFromExceptionInternal_m6322_MethodInfo,
	&StackTraceUtility_PostprocessStacktrace_m6323_MethodInfo,
	&StackTraceUtility_ExtractFormattedStackTrace_m6324_MethodInfo,
	NULL
};
static MethodInfo* StackTraceUtility_t1064_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
extern TypeInfo SecuritySafeCriticalAttribute_t1143_il2cpp_TypeInfo;
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttribute.h"
// System.Security.SecuritySafeCriticalAttribute
#include "mscorlib_System_Security_SecuritySafeCriticalAttributeMethodDeclarations.h"
extern MethodInfo SecuritySafeCriticalAttribute__ctor_m6476_MethodInfo;
void StackTraceUtility_t1064_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m6319(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1143 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1143 *)il2cpp_codegen_object_new (&SecuritySafeCriticalAttribute_t1143_il2cpp_TypeInfo);
		SecuritySafeCriticalAttribute__ctor_m6476(tmp, &SecuritySafeCriticalAttribute__ctor_m6476_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void StackTraceUtility_t1064_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m6322(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1143 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1143 *)il2cpp_codegen_object_new (&SecuritySafeCriticalAttribute_t1143_il2cpp_TypeInfo);
		SecuritySafeCriticalAttribute__ctor_m6476(tmp, &SecuritySafeCriticalAttribute__ctor_m6476_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void StackTraceUtility_t1064_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m6324(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SecuritySafeCriticalAttribute_t1143 * tmp;
		tmp = (SecuritySafeCriticalAttribute_t1143 *)il2cpp_codegen_object_new (&SecuritySafeCriticalAttribute_t1143_il2cpp_TypeInfo);
		SecuritySafeCriticalAttribute__ctor_m6476(tmp, &SecuritySafeCriticalAttribute__ctor_m6476_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache StackTraceUtility_t1064__CustomAttributeCache_StackTraceUtility_ExtractStackTrace_m6319 = {
1,
NULL,
&StackTraceUtility_t1064_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStackTrace_m6319
};
CustomAttributesCache StackTraceUtility_t1064__CustomAttributeCache_StackTraceUtility_ExtractStringFromExceptionInternal_m6322 = {
1,
NULL,
&StackTraceUtility_t1064_CustomAttributesCacheGenerator_StackTraceUtility_ExtractStringFromExceptionInternal_m6322
};
CustomAttributesCache StackTraceUtility_t1064__CustomAttributeCache_StackTraceUtility_ExtractFormattedStackTrace_m6324 = {
1,
NULL,
&StackTraceUtility_t1064_CustomAttributesCacheGenerator_StackTraceUtility_ExtractFormattedStackTrace_m6324
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType StackTraceUtility_t1064_0_0_0;
extern Il2CppType StackTraceUtility_t1064_1_0_0;
struct StackTraceUtility_t1064;
extern CustomAttributesCache StackTraceUtility_t1064__CustomAttributeCache_StackTraceUtility_ExtractStackTrace_m6319;
extern CustomAttributesCache StackTraceUtility_t1064__CustomAttributeCache_StackTraceUtility_ExtractStringFromExceptionInternal_m6322;
extern CustomAttributesCache StackTraceUtility_t1064__CustomAttributeCache_StackTraceUtility_ExtractFormattedStackTrace_m6324;
TypeInfo StackTraceUtility_t1064_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StackTraceUtility"/* name */
	, "UnityEngine"/* namespaze */
	, StackTraceUtility_t1064_MethodInfos/* methods */
	, NULL/* properties */
	, StackTraceUtility_t1064_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &StackTraceUtility_t1064_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, StackTraceUtility_t1064_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &StackTraceUtility_t1064_il2cpp_TypeInfo/* cast_class */
	, &StackTraceUtility_t1064_0_0_0/* byval_arg */
	, &StackTraceUtility_t1064_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StackTraceUtility_t1064)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(StackTraceUtility_t1064_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 9/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityException.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityException_t476_il2cpp_TypeInfo;
// UnityEngine.UnityException
#include "UnityEngine_UnityEngine_UnityExceptionMethodDeclarations.h"

// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
extern MethodInfo Exception__ctor_m6528_MethodInfo;
extern MethodInfo Exception_set_HResult_m6529_MethodInfo;
extern MethodInfo Exception__ctor_m6530_MethodInfo;
extern MethodInfo Exception__ctor_m6531_MethodInfo;


// System.Void UnityEngine.UnityException::.ctor()
extern MethodInfo UnityException__ctor_m6325_MethodInfo;
 void UnityException__ctor_m6325 (UnityException_t476 * __this, MethodInfo* method){
	{
		Exception__ctor_m6528(__this, (String_t*) &_stringLiteral435, /*hidden argument*/&Exception__ctor_m6528_MethodInfo);
		Exception_set_HResult_m6529(__this, ((int32_t)-2147467261), /*hidden argument*/&Exception_set_HResult_m6529_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern MethodInfo UnityException__ctor_m6326_MethodInfo;
 void UnityException__ctor_m6326 (UnityException_t476 * __this, String_t* ___message, MethodInfo* method){
	{
		Exception__ctor_m6528(__this, ___message, /*hidden argument*/&Exception__ctor_m6528_MethodInfo);
		Exception_set_HResult_m6529(__this, ((int32_t)-2147467261), /*hidden argument*/&Exception_set_HResult_m6529_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
extern MethodInfo UnityException__ctor_m6327_MethodInfo;
 void UnityException__ctor_m6327 (UnityException_t476 * __this, String_t* ___message, Exception_t152 * ___innerException, MethodInfo* method){
	{
		Exception__ctor_m6530(__this, ___message, ___innerException, /*hidden argument*/&Exception__ctor_m6530_MethodInfo);
		Exception_set_HResult_m6529(__this, ((int32_t)-2147467261), /*hidden argument*/&Exception_set_HResult_m6529_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern MethodInfo UnityException__ctor_m6328_MethodInfo;
 void UnityException__ctor_m6328 (UnityException_t476 * __this, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___context, MethodInfo* method){
	{
		Exception__ctor_m6531(__this, ___info, ___context, /*hidden argument*/&Exception__ctor_m6531_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngine.UnityException
extern Il2CppType Int32_t93_0_0_32849;
FieldInfo UnityException_t476____Result_11_FieldInfo = 
{
	"Result"/* name */
	, &Int32_t93_0_0_32849/* type */
	, &UnityException_t476_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo UnityException_t476____unityStackTrace_12_FieldInfo = 
{
	"unityStackTrace"/* name */
	, &String_t_0_0_1/* type */
	, &UnityException_t476_il2cpp_TypeInfo/* parent */
	, offsetof(UnityException_t476, ___unityStackTrace_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityException_t476_FieldInfos[] =
{
	&UnityException_t476____Result_11_FieldInfo,
	&UnityException_t476____unityStackTrace_12_FieldInfo,
	NULL
};
static const int32_t UnityException_t476____Result_11_DefaultValueData = -2147467261;
static Il2CppFieldDefaultValueEntry UnityException_t476____Result_11_DefaultValue = 
{
	&UnityException_t476____Result_11_FieldInfo/* field */
	, { (char*)&UnityException_t476____Result_11_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UnityException_t476_FieldDefaultValues[] = 
{
	&UnityException_t476____Result_11_DefaultValue,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor()
MethodInfo UnityException__ctor_m6325_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6325/* method */
	, &UnityException_t476_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1448/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityException_t476_UnityException__ctor_m6326_ParameterInfos[] = 
{
	{"message", 0, 134219255, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.String)
MethodInfo UnityException__ctor_m6326_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6326/* method */
	, &UnityException_t476_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityException_t476_UnityException__ctor_m6326_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1449/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Exception_t152_0_0_0;
extern Il2CppType Exception_t152_0_0_0;
static ParameterInfo UnityException_t476_UnityException__ctor_m6327_ParameterInfos[] = 
{
	{"message", 0, 134219256, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"innerException", 1, 134219257, &EmptyCustomAttributesCache, &Exception_t152_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
MethodInfo UnityException__ctor_m6327_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6327/* method */
	, &UnityException_t476_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, UnityException_t476_UnityException__ctor_m6327_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1450/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType SerializationInfo_t1066_0_0_0;
extern Il2CppType SerializationInfo_t1066_0_0_0;
extern Il2CppType StreamingContext_t1067_0_0_0;
extern Il2CppType StreamingContext_t1067_0_0_0;
static ParameterInfo UnityException_t476_UnityException__ctor_m6328_ParameterInfos[] = 
{
	{"info", 0, 134219258, &EmptyCustomAttributesCache, &SerializationInfo_t1066_0_0_0},
	{"context", 1, 134219259, &EmptyCustomAttributesCache, &StreamingContext_t1067_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_StreamingContext_t1067 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo UnityException__ctor_m6328_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityException__ctor_m6328/* method */
	, &UnityException_t476_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_StreamingContext_t1067/* invoker_method */
	, UnityException_t476_UnityException__ctor_m6328_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityException_t476_MethodInfos[] =
{
	&UnityException__ctor_m6325_MethodInfo,
	&UnityException__ctor_m6326_MethodInfo,
	&UnityException__ctor_m6327_MethodInfo,
	&UnityException__ctor_m6328_MethodInfo,
	NULL
};
extern MethodInfo Exception_ToString_m6434_MethodInfo;
extern MethodInfo Exception_GetObjectData_m6435_MethodInfo;
extern MethodInfo Exception_get_Source_m6437_MethodInfo;
static MethodInfo* UnityException_t476_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Exception_ToString_m6434_MethodInfo,
	&Exception_GetObjectData_m6435_MethodInfo,
	&Exception_get_InnerException_m6436_MethodInfo,
	&Exception_get_Message_m2227_MethodInfo,
	&Exception_get_Source_m6437_MethodInfo,
	&Exception_get_StackTrace_m6438_MethodInfo,
	&Exception_GetObjectData_m6435_MethodInfo,
	&Exception_GetType_m6439_MethodInfo,
};
extern TypeInfo ISerializable_t482_il2cpp_TypeInfo;
extern TypeInfo _Exception_t1118_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityException_t476_InterfacesOffsets[] = 
{
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
	{ &_Exception_t1118_il2cpp_TypeInfo, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityException_t476_0_0_0;
extern Il2CppType UnityException_t476_1_0_0;
struct UnityException_t476;
TypeInfo UnityException_t476_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityException"/* name */
	, "UnityEngine"/* namespaze */
	, UnityException_t476_MethodInfos/* methods */
	, NULL/* properties */
	, UnityException_t476_FieldInfos/* fields */
	, NULL/* events */
	, &Exception_t152_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityException_t476_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityException_t476_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityException_t476_il2cpp_TypeInfo/* cast_class */
	, &UnityException_t476_0_0_0/* byval_arg */
	, &UnityException_t476_1_0_0/* this_arg */
	, UnityException_t476_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UnityException_t476_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityException_t476)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SharedBetweenAnimatorsAttribute_t1068_il2cpp_TypeInfo;
// UnityEngine.SharedBetweenAnimatorsAttribute
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttributeMethodDeclarations.h"



// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
extern MethodInfo SharedBetweenAnimatorsAttribute__ctor_m6329_MethodInfo;
 void SharedBetweenAnimatorsAttribute__ctor_m6329 (SharedBetweenAnimatorsAttribute_t1068 * __this, MethodInfo* method){
	{
		Attribute__ctor_m4248(__this, /*hidden argument*/&Attribute__ctor_m4248_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngine.SharedBetweenAnimatorsAttribute
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
MethodInfo SharedBetweenAnimatorsAttribute__ctor_m6329_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&SharedBetweenAnimatorsAttribute__ctor_m6329/* method */
	, &SharedBetweenAnimatorsAttribute_t1068_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SharedBetweenAnimatorsAttribute_t1068_MethodInfos[] =
{
	&SharedBetweenAnimatorsAttribute__ctor_m6329_MethodInfo,
	NULL
};
static MethodInfo* SharedBetweenAnimatorsAttribute_t1068_VTable[] =
{
	&Attribute_Equals_m4249_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Attribute_GetHashCode_m4250_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
static Il2CppInterfaceOffsetPair SharedBetweenAnimatorsAttribute_t1068_InterfacesOffsets[] = 
{
	{ &_Attribute_t775_il2cpp_TypeInfo, 4},
};
void SharedBetweenAnimatorsAttribute_t1068_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1160 * tmp;
		tmp = (AttributeUsageAttribute_t1160 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1160_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6500(tmp, 4, &AttributeUsageAttribute__ctor_m6500_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6502(tmp, false, &AttributeUsageAttribute_set_AllowMultiple_m6502_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache SharedBetweenAnimatorsAttribute_t1068__CustomAttributeCache = {
1,
NULL,
&SharedBetweenAnimatorsAttribute_t1068_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType SharedBetweenAnimatorsAttribute_t1068_0_0_0;
extern Il2CppType SharedBetweenAnimatorsAttribute_t1068_1_0_0;
struct SharedBetweenAnimatorsAttribute_t1068;
extern CustomAttributesCache SharedBetweenAnimatorsAttribute_t1068__CustomAttributeCache;
TypeInfo SharedBetweenAnimatorsAttribute_t1068_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "SharedBetweenAnimatorsAttribute"/* name */
	, "UnityEngine"/* namespaze */
	, SharedBetweenAnimatorsAttribute_t1068_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Attribute_t146_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SharedBetweenAnimatorsAttribute_t1068_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SharedBetweenAnimatorsAttribute_t1068_VTable/* vtable */
	, &SharedBetweenAnimatorsAttribute_t1068__CustomAttributeCache/* custom_attributes_cache */
	, &SharedBetweenAnimatorsAttribute_t1068_il2cpp_TypeInfo/* cast_class */
	, &SharedBetweenAnimatorsAttribute_t1068_0_0_0/* byval_arg */
	, &SharedBetweenAnimatorsAttribute_t1068_1_0_0/* this_arg */
	, SharedBetweenAnimatorsAttribute_t1068_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SharedBetweenAnimatorsAttribute_t1068)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviour.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo StateMachineBehaviour_t1069_il2cpp_TypeInfo;
// UnityEngine.StateMachineBehaviour
#include "UnityEngine_UnityEngine_StateMachineBehaviourMethodDeclarations.h"

// UnityEngine.Animator
#include "UnityEngine_UnityEngine_Animator.h"
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
// UnityEngine.ScriptableObject
#include "UnityEngine_UnityEngine_ScriptableObjectMethodDeclarations.h"
extern MethodInfo ScriptableObject__ctor_m5439_MethodInfo;


// System.Void UnityEngine.StateMachineBehaviour::.ctor()
extern MethodInfo StateMachineBehaviour__ctor_m6330_MethodInfo;
 void StateMachineBehaviour__ctor_m6330 (StateMachineBehaviour_t1069 * __this, MethodInfo* method){
	{
		ScriptableObject__ctor_m5439(__this, /*hidden argument*/&ScriptableObject__ctor_m5439_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern MethodInfo StateMachineBehaviour_OnStateEnter_m6331_MethodInfo;
 void StateMachineBehaviour_OnStateEnter_m6331 (StateMachineBehaviour_t1069 * __this, Animator_t356 * ___animator, AnimatorStateInfo_t1015  ___stateInfo, int32_t ___layerIndex, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern MethodInfo StateMachineBehaviour_OnStateUpdate_m6332_MethodInfo;
 void StateMachineBehaviour_OnStateUpdate_m6332 (StateMachineBehaviour_t1069 * __this, Animator_t356 * ___animator, AnimatorStateInfo_t1015  ___stateInfo, int32_t ___layerIndex, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern MethodInfo StateMachineBehaviour_OnStateExit_m6333_MethodInfo;
 void StateMachineBehaviour_OnStateExit_m6333 (StateMachineBehaviour_t1069 * __this, Animator_t356 * ___animator, AnimatorStateInfo_t1015  ___stateInfo, int32_t ___layerIndex, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern MethodInfo StateMachineBehaviour_OnStateMove_m6334_MethodInfo;
 void StateMachineBehaviour_OnStateMove_m6334 (StateMachineBehaviour_t1069 * __this, Animator_t356 * ___animator, AnimatorStateInfo_t1015  ___stateInfo, int32_t ___layerIndex, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern MethodInfo StateMachineBehaviour_OnStateIK_m6335_MethodInfo;
 void StateMachineBehaviour_OnStateIK_m6335 (StateMachineBehaviour_t1069 * __this, Animator_t356 * ___animator, AnimatorStateInfo_t1015  ___stateInfo, int32_t ___layerIndex, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
extern MethodInfo StateMachineBehaviour_OnStateMachineEnter_m6336_MethodInfo;
 void StateMachineBehaviour_OnStateMachineEnter_m6336 (StateMachineBehaviour_t1069 * __this, Animator_t356 * ___animator, int32_t ___stateMachinePathHash, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
extern MethodInfo StateMachineBehaviour_OnStateMachineExit_m6337_MethodInfo;
 void StateMachineBehaviour_OnStateMachineExit_m6337 (StateMachineBehaviour_t1069 * __this, Animator_t356 * ___animator, int32_t ___stateMachinePathHash, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition UnityEngine.StateMachineBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
MethodInfo StateMachineBehaviour__ctor_m6330_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&StateMachineBehaviour__ctor_m6330/* method */
	, &StateMachineBehaviour_t1069_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t356_0_0_0;
extern Il2CppType Animator_t356_0_0_0;
extern Il2CppType AnimatorStateInfo_t1015_0_0_0;
extern Il2CppType AnimatorStateInfo_t1015_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo StateMachineBehaviour_t1069_StateMachineBehaviour_OnStateEnter_m6331_ParameterInfos[] = 
{
	{"animator", 0, 134219260, &EmptyCustomAttributesCache, &Animator_t356_0_0_0},
	{"stateInfo", 1, 134219261, &EmptyCustomAttributesCache, &AnimatorStateInfo_t1015_0_0_0},
	{"layerIndex", 2, 134219262, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_AnimatorStateInfo_t1015_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateEnter_m6331_MethodInfo = 
{
	"OnStateEnter"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateEnter_m6331/* method */
	, &StateMachineBehaviour_t1069_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_AnimatorStateInfo_t1015_Int32_t93/* invoker_method */
	, StateMachineBehaviour_t1069_StateMachineBehaviour_OnStateEnter_m6331_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t356_0_0_0;
extern Il2CppType AnimatorStateInfo_t1015_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo StateMachineBehaviour_t1069_StateMachineBehaviour_OnStateUpdate_m6332_ParameterInfos[] = 
{
	{"animator", 0, 134219263, &EmptyCustomAttributesCache, &Animator_t356_0_0_0},
	{"stateInfo", 1, 134219264, &EmptyCustomAttributesCache, &AnimatorStateInfo_t1015_0_0_0},
	{"layerIndex", 2, 134219265, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_AnimatorStateInfo_t1015_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateUpdate_m6332_MethodInfo = 
{
	"OnStateUpdate"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateUpdate_m6332/* method */
	, &StateMachineBehaviour_t1069_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_AnimatorStateInfo_t1015_Int32_t93/* invoker_method */
	, StateMachineBehaviour_t1069_StateMachineBehaviour_OnStateUpdate_m6332_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t356_0_0_0;
extern Il2CppType AnimatorStateInfo_t1015_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo StateMachineBehaviour_t1069_StateMachineBehaviour_OnStateExit_m6333_ParameterInfos[] = 
{
	{"animator", 0, 134219266, &EmptyCustomAttributesCache, &Animator_t356_0_0_0},
	{"stateInfo", 1, 134219267, &EmptyCustomAttributesCache, &AnimatorStateInfo_t1015_0_0_0},
	{"layerIndex", 2, 134219268, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_AnimatorStateInfo_t1015_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateExit_m6333_MethodInfo = 
{
	"OnStateExit"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateExit_m6333/* method */
	, &StateMachineBehaviour_t1069_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_AnimatorStateInfo_t1015_Int32_t93/* invoker_method */
	, StateMachineBehaviour_t1069_StateMachineBehaviour_OnStateExit_m6333_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t356_0_0_0;
extern Il2CppType AnimatorStateInfo_t1015_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo StateMachineBehaviour_t1069_StateMachineBehaviour_OnStateMove_m6334_ParameterInfos[] = 
{
	{"animator", 0, 134219269, &EmptyCustomAttributesCache, &Animator_t356_0_0_0},
	{"stateInfo", 1, 134219270, &EmptyCustomAttributesCache, &AnimatorStateInfo_t1015_0_0_0},
	{"layerIndex", 2, 134219271, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_AnimatorStateInfo_t1015_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateMove_m6334_MethodInfo = 
{
	"OnStateMove"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMove_m6334/* method */
	, &StateMachineBehaviour_t1069_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_AnimatorStateInfo_t1015_Int32_t93/* invoker_method */
	, StateMachineBehaviour_t1069_StateMachineBehaviour_OnStateMove_m6334_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t356_0_0_0;
extern Il2CppType AnimatorStateInfo_t1015_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo StateMachineBehaviour_t1069_StateMachineBehaviour_OnStateIK_m6335_ParameterInfos[] = 
{
	{"animator", 0, 134219272, &EmptyCustomAttributesCache, &Animator_t356_0_0_0},
	{"stateInfo", 1, 134219273, &EmptyCustomAttributesCache, &AnimatorStateInfo_t1015_0_0_0},
	{"layerIndex", 2, 134219274, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_AnimatorStateInfo_t1015_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
MethodInfo StateMachineBehaviour_OnStateIK_m6335_MethodInfo = 
{
	"OnStateIK"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateIK_m6335/* method */
	, &StateMachineBehaviour_t1069_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_AnimatorStateInfo_t1015_Int32_t93/* invoker_method */
	, StateMachineBehaviour_t1069_StateMachineBehaviour_OnStateIK_m6335_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t356_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo StateMachineBehaviour_t1069_StateMachineBehaviour_OnStateMachineEnter_m6336_ParameterInfos[] = 
{
	{"animator", 0, 134219275, &EmptyCustomAttributesCache, &Animator_t356_0_0_0},
	{"stateMachinePathHash", 1, 134219276, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
MethodInfo StateMachineBehaviour_OnStateMachineEnter_m6336_MethodInfo = 
{
	"OnStateMachineEnter"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMachineEnter_m6336/* method */
	, &StateMachineBehaviour_t1069_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, StateMachineBehaviour_t1069_StateMachineBehaviour_OnStateMachineEnter_m6336_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Animator_t356_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo StateMachineBehaviour_t1069_StateMachineBehaviour_OnStateMachineExit_m6337_ParameterInfos[] = 
{
	{"animator", 0, 134219277, &EmptyCustomAttributesCache, &Animator_t356_0_0_0},
	{"stateMachinePathHash", 1, 134219278, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
MethodInfo StateMachineBehaviour_OnStateMachineExit_m6337_MethodInfo = 
{
	"OnStateMachineExit"/* name */
	, (methodPointerType)&StateMachineBehaviour_OnStateMachineExit_m6337/* method */
	, &StateMachineBehaviour_t1069_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, StateMachineBehaviour_t1069_StateMachineBehaviour_OnStateMachineExit_m6337_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* StateMachineBehaviour_t1069_MethodInfos[] =
{
	&StateMachineBehaviour__ctor_m6330_MethodInfo,
	&StateMachineBehaviour_OnStateEnter_m6331_MethodInfo,
	&StateMachineBehaviour_OnStateUpdate_m6332_MethodInfo,
	&StateMachineBehaviour_OnStateExit_m6333_MethodInfo,
	&StateMachineBehaviour_OnStateMove_m6334_MethodInfo,
	&StateMachineBehaviour_OnStateIK_m6335_MethodInfo,
	&StateMachineBehaviour_OnStateMachineEnter_m6336_MethodInfo,
	&StateMachineBehaviour_OnStateMachineExit_m6337_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m229_MethodInfo;
extern MethodInfo Object_GetHashCode_m230_MethodInfo;
extern MethodInfo Object_ToString_m231_MethodInfo;
static MethodInfo* StateMachineBehaviour_t1069_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&StateMachineBehaviour_OnStateEnter_m6331_MethodInfo,
	&StateMachineBehaviour_OnStateUpdate_m6332_MethodInfo,
	&StateMachineBehaviour_OnStateExit_m6333_MethodInfo,
	&StateMachineBehaviour_OnStateMove_m6334_MethodInfo,
	&StateMachineBehaviour_OnStateIK_m6335_MethodInfo,
	&StateMachineBehaviour_OnStateMachineEnter_m6336_MethodInfo,
	&StateMachineBehaviour_OnStateMachineExit_m6337_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType StateMachineBehaviour_t1069_0_0_0;
extern Il2CppType StateMachineBehaviour_t1069_1_0_0;
extern TypeInfo ScriptableObject_t919_il2cpp_TypeInfo;
struct StateMachineBehaviour_t1069;
TypeInfo StateMachineBehaviour_t1069_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "StateMachineBehaviour"/* name */
	, "UnityEngine"/* namespaze */
	, StateMachineBehaviour_t1069_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ScriptableObject_t919_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &StateMachineBehaviour_t1069_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, StateMachineBehaviour_t1069_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &StateMachineBehaviour_t1069_il2cpp_TypeInfo/* cast_class */
	, &StateMachineBehaviour_t1069_0_0_0/* byval_arg */
	, &StateMachineBehaviour_t1069_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (StateMachineBehaviour_t1069)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 8/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 11/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnapping.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo DblClickSnapping_t1070_il2cpp_TypeInfo;
// UnityEngine.TextEditor/DblClickSnapping
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappingMethodDeclarations.h"



// Metadata Definition UnityEngine.TextEditor/DblClickSnapping
extern Il2CppType Byte_t796_0_0_1542;
FieldInfo DblClickSnapping_t1070____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Byte_t796_0_0_1542/* type */
	, &DblClickSnapping_t1070_il2cpp_TypeInfo/* parent */
	, offsetof(DblClickSnapping_t1070, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType DblClickSnapping_t1070_0_0_32854;
FieldInfo DblClickSnapping_t1070____WORDS_2_FieldInfo = 
{
	"WORDS"/* name */
	, &DblClickSnapping_t1070_0_0_32854/* type */
	, &DblClickSnapping_t1070_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType DblClickSnapping_t1070_0_0_32854;
FieldInfo DblClickSnapping_t1070____PARAGRAPHS_3_FieldInfo = 
{
	"PARAGRAPHS"/* name */
	, &DblClickSnapping_t1070_0_0_32854/* type */
	, &DblClickSnapping_t1070_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* DblClickSnapping_t1070_FieldInfos[] =
{
	&DblClickSnapping_t1070____value___1_FieldInfo,
	&DblClickSnapping_t1070____WORDS_2_FieldInfo,
	&DblClickSnapping_t1070____PARAGRAPHS_3_FieldInfo,
	NULL
};
static const uint8_t DblClickSnapping_t1070____WORDS_2_DefaultValueData = 0;
extern Il2CppType Byte_t796_0_0_0;
static Il2CppFieldDefaultValueEntry DblClickSnapping_t1070____WORDS_2_DefaultValue = 
{
	&DblClickSnapping_t1070____WORDS_2_FieldInfo/* field */
	, { (char*)&DblClickSnapping_t1070____WORDS_2_DefaultValueData, &Byte_t796_0_0_0 }/* value */

};
static const uint8_t DblClickSnapping_t1070____PARAGRAPHS_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry DblClickSnapping_t1070____PARAGRAPHS_3_DefaultValue = 
{
	&DblClickSnapping_t1070____PARAGRAPHS_3_FieldInfo/* field */
	, { (char*)&DblClickSnapping_t1070____PARAGRAPHS_3_DefaultValueData, &Byte_t796_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* DblClickSnapping_t1070_FieldDefaultValues[] = 
{
	&DblClickSnapping_t1070____WORDS_2_DefaultValue,
	&DblClickSnapping_t1070____PARAGRAPHS_3_DefaultValue,
	NULL
};
static MethodInfo* DblClickSnapping_t1070_MethodInfos[] =
{
	NULL
};
static MethodInfo* DblClickSnapping_t1070_VTable[] =
{
	&Enum_Equals_m191_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Enum_GetHashCode_m193_MethodInfo,
	&Enum_ToString_m194_MethodInfo,
	&Enum_ToString_m195_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m196_MethodInfo,
	&Enum_System_IConvertible_ToByte_m197_MethodInfo,
	&Enum_System_IConvertible_ToChar_m198_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m199_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m200_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m201_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m202_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m203_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m204_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m205_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m206_MethodInfo,
	&Enum_ToString_m207_MethodInfo,
	&Enum_System_IConvertible_ToType_m208_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m209_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m210_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m211_MethodInfo,
	&Enum_CompareTo_m212_MethodInfo,
	&Enum_GetTypeCode_m213_MethodInfo,
};
static Il2CppInterfaceOffsetPair DblClickSnapping_t1070_InterfacesOffsets[] = 
{
	{ &IFormattable_t94_il2cpp_TypeInfo, 4},
	{ &IConvertible_t95_il2cpp_TypeInfo, 5},
	{ &IComparable_t96_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType DblClickSnapping_t1070_0_0_0;
extern Il2CppType DblClickSnapping_t1070_1_0_0;
// System.Byte
#include "mscorlib_System_Byte.h"
extern TypeInfo Byte_t796_il2cpp_TypeInfo;
extern TypeInfo TextEditor_t484_il2cpp_TypeInfo;
TypeInfo DblClickSnapping_t1070_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DblClickSnapping"/* name */
	, ""/* namespaze */
	, DblClickSnapping_t1070_MethodInfos/* methods */
	, NULL/* properties */
	, DblClickSnapping_t1070_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t97_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &TextEditor_t484_il2cpp_TypeInfo/* nested_in */
	, &Byte_t796_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DblClickSnapping_t1070_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Byte_t796_il2cpp_TypeInfo/* cast_class */
	, &DblClickSnapping_t1070_0_0_0/* byval_arg */
	, &DblClickSnapping_t1070_1_0_0/* this_arg */
	, DblClickSnapping_t1070_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, DblClickSnapping_t1070_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DblClickSnapping_t1070)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (uint8_t)/* element_size */
	, sizeof(uint8_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 258/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TextEditOp_t1071_il2cpp_TypeInfo;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOpMethodDeclarations.h"



// Metadata Definition UnityEngine.TextEditor/TextEditOp
extern Il2CppType Int32_t93_0_0_1542;
FieldInfo TextEditOp_t1071____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t93_0_0_1542/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditOp_t1071, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveLeft_2_FieldInfo = 
{
	"MoveLeft"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveRight_3_FieldInfo = 
{
	"MoveRight"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveUp_4_FieldInfo = 
{
	"MoveUp"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveDown_5_FieldInfo = 
{
	"MoveDown"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveLineStart_6_FieldInfo = 
{
	"MoveLineStart"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveLineEnd_7_FieldInfo = 
{
	"MoveLineEnd"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveTextStart_8_FieldInfo = 
{
	"MoveTextStart"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveTextEnd_9_FieldInfo = 
{
	"MoveTextEnd"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MovePageUp_10_FieldInfo = 
{
	"MovePageUp"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MovePageDown_11_FieldInfo = 
{
	"MovePageDown"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveGraphicalLineStart_12_FieldInfo = 
{
	"MoveGraphicalLineStart"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveGraphicalLineEnd_13_FieldInfo = 
{
	"MoveGraphicalLineEnd"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveWordLeft_14_FieldInfo = 
{
	"MoveWordLeft"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveWordRight_15_FieldInfo = 
{
	"MoveWordRight"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveParagraphForward_16_FieldInfo = 
{
	"MoveParagraphForward"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveParagraphBackward_17_FieldInfo = 
{
	"MoveParagraphBackward"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveToStartOfNextWord_18_FieldInfo = 
{
	"MoveToStartOfNextWord"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____MoveToEndOfPreviousWord_19_FieldInfo = 
{
	"MoveToEndOfPreviousWord"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectLeft_20_FieldInfo = 
{
	"SelectLeft"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectRight_21_FieldInfo = 
{
	"SelectRight"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectUp_22_FieldInfo = 
{
	"SelectUp"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectDown_23_FieldInfo = 
{
	"SelectDown"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectTextStart_24_FieldInfo = 
{
	"SelectTextStart"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectTextEnd_25_FieldInfo = 
{
	"SelectTextEnd"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectPageUp_26_FieldInfo = 
{
	"SelectPageUp"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectPageDown_27_FieldInfo = 
{
	"SelectPageDown"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____ExpandSelectGraphicalLineStart_28_FieldInfo = 
{
	"ExpandSelectGraphicalLineStart"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____ExpandSelectGraphicalLineEnd_29_FieldInfo = 
{
	"ExpandSelectGraphicalLineEnd"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectGraphicalLineStart_30_FieldInfo = 
{
	"SelectGraphicalLineStart"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectGraphicalLineEnd_31_FieldInfo = 
{
	"SelectGraphicalLineEnd"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectWordLeft_32_FieldInfo = 
{
	"SelectWordLeft"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectWordRight_33_FieldInfo = 
{
	"SelectWordRight"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectToEndOfPreviousWord_34_FieldInfo = 
{
	"SelectToEndOfPreviousWord"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectToStartOfNextWord_35_FieldInfo = 
{
	"SelectToStartOfNextWord"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectParagraphBackward_36_FieldInfo = 
{
	"SelectParagraphBackward"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectParagraphForward_37_FieldInfo = 
{
	"SelectParagraphForward"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____Delete_38_FieldInfo = 
{
	"Delete"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____Backspace_39_FieldInfo = 
{
	"Backspace"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____DeleteWordBack_40_FieldInfo = 
{
	"DeleteWordBack"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____DeleteWordForward_41_FieldInfo = 
{
	"DeleteWordForward"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____DeleteLineBack_42_FieldInfo = 
{
	"DeleteLineBack"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____Cut_43_FieldInfo = 
{
	"Cut"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____Copy_44_FieldInfo = 
{
	"Copy"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____Paste_45_FieldInfo = 
{
	"Paste"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectAll_46_FieldInfo = 
{
	"SelectAll"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____SelectNone_47_FieldInfo = 
{
	"SelectNone"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____ScrollStart_48_FieldInfo = 
{
	"ScrollStart"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____ScrollEnd_49_FieldInfo = 
{
	"ScrollEnd"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____ScrollPageUp_50_FieldInfo = 
{
	"ScrollPageUp"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextEditOp_t1071_0_0_32854;
FieldInfo TextEditOp_t1071____ScrollPageDown_51_FieldInfo = 
{
	"ScrollPageDown"/* name */
	, &TextEditOp_t1071_0_0_32854/* type */
	, &TextEditOp_t1071_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TextEditOp_t1071_FieldInfos[] =
{
	&TextEditOp_t1071____value___1_FieldInfo,
	&TextEditOp_t1071____MoveLeft_2_FieldInfo,
	&TextEditOp_t1071____MoveRight_3_FieldInfo,
	&TextEditOp_t1071____MoveUp_4_FieldInfo,
	&TextEditOp_t1071____MoveDown_5_FieldInfo,
	&TextEditOp_t1071____MoveLineStart_6_FieldInfo,
	&TextEditOp_t1071____MoveLineEnd_7_FieldInfo,
	&TextEditOp_t1071____MoveTextStart_8_FieldInfo,
	&TextEditOp_t1071____MoveTextEnd_9_FieldInfo,
	&TextEditOp_t1071____MovePageUp_10_FieldInfo,
	&TextEditOp_t1071____MovePageDown_11_FieldInfo,
	&TextEditOp_t1071____MoveGraphicalLineStart_12_FieldInfo,
	&TextEditOp_t1071____MoveGraphicalLineEnd_13_FieldInfo,
	&TextEditOp_t1071____MoveWordLeft_14_FieldInfo,
	&TextEditOp_t1071____MoveWordRight_15_FieldInfo,
	&TextEditOp_t1071____MoveParagraphForward_16_FieldInfo,
	&TextEditOp_t1071____MoveParagraphBackward_17_FieldInfo,
	&TextEditOp_t1071____MoveToStartOfNextWord_18_FieldInfo,
	&TextEditOp_t1071____MoveToEndOfPreviousWord_19_FieldInfo,
	&TextEditOp_t1071____SelectLeft_20_FieldInfo,
	&TextEditOp_t1071____SelectRight_21_FieldInfo,
	&TextEditOp_t1071____SelectUp_22_FieldInfo,
	&TextEditOp_t1071____SelectDown_23_FieldInfo,
	&TextEditOp_t1071____SelectTextStart_24_FieldInfo,
	&TextEditOp_t1071____SelectTextEnd_25_FieldInfo,
	&TextEditOp_t1071____SelectPageUp_26_FieldInfo,
	&TextEditOp_t1071____SelectPageDown_27_FieldInfo,
	&TextEditOp_t1071____ExpandSelectGraphicalLineStart_28_FieldInfo,
	&TextEditOp_t1071____ExpandSelectGraphicalLineEnd_29_FieldInfo,
	&TextEditOp_t1071____SelectGraphicalLineStart_30_FieldInfo,
	&TextEditOp_t1071____SelectGraphicalLineEnd_31_FieldInfo,
	&TextEditOp_t1071____SelectWordLeft_32_FieldInfo,
	&TextEditOp_t1071____SelectWordRight_33_FieldInfo,
	&TextEditOp_t1071____SelectToEndOfPreviousWord_34_FieldInfo,
	&TextEditOp_t1071____SelectToStartOfNextWord_35_FieldInfo,
	&TextEditOp_t1071____SelectParagraphBackward_36_FieldInfo,
	&TextEditOp_t1071____SelectParagraphForward_37_FieldInfo,
	&TextEditOp_t1071____Delete_38_FieldInfo,
	&TextEditOp_t1071____Backspace_39_FieldInfo,
	&TextEditOp_t1071____DeleteWordBack_40_FieldInfo,
	&TextEditOp_t1071____DeleteWordForward_41_FieldInfo,
	&TextEditOp_t1071____DeleteLineBack_42_FieldInfo,
	&TextEditOp_t1071____Cut_43_FieldInfo,
	&TextEditOp_t1071____Copy_44_FieldInfo,
	&TextEditOp_t1071____Paste_45_FieldInfo,
	&TextEditOp_t1071____SelectAll_46_FieldInfo,
	&TextEditOp_t1071____SelectNone_47_FieldInfo,
	&TextEditOp_t1071____ScrollStart_48_FieldInfo,
	&TextEditOp_t1071____ScrollEnd_49_FieldInfo,
	&TextEditOp_t1071____ScrollPageUp_50_FieldInfo,
	&TextEditOp_t1071____ScrollPageDown_51_FieldInfo,
	NULL
};
static const int32_t TextEditOp_t1071____MoveLeft_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveLeft_2_DefaultValue = 
{
	&TextEditOp_t1071____MoveLeft_2_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveLeft_2_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveRight_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveRight_3_DefaultValue = 
{
	&TextEditOp_t1071____MoveRight_3_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveRight_3_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveUp_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveUp_4_DefaultValue = 
{
	&TextEditOp_t1071____MoveUp_4_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveUp_4_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveDown_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveDown_5_DefaultValue = 
{
	&TextEditOp_t1071____MoveDown_5_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveDown_5_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveLineStart_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveLineStart_6_DefaultValue = 
{
	&TextEditOp_t1071____MoveLineStart_6_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveLineStart_6_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveLineEnd_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveLineEnd_7_DefaultValue = 
{
	&TextEditOp_t1071____MoveLineEnd_7_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveLineEnd_7_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveTextStart_8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveTextStart_8_DefaultValue = 
{
	&TextEditOp_t1071____MoveTextStart_8_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveTextStart_8_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveTextEnd_9_DefaultValueData = 7;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveTextEnd_9_DefaultValue = 
{
	&TextEditOp_t1071____MoveTextEnd_9_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveTextEnd_9_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MovePageUp_10_DefaultValueData = 8;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MovePageUp_10_DefaultValue = 
{
	&TextEditOp_t1071____MovePageUp_10_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MovePageUp_10_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MovePageDown_11_DefaultValueData = 9;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MovePageDown_11_DefaultValue = 
{
	&TextEditOp_t1071____MovePageDown_11_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MovePageDown_11_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveGraphicalLineStart_12_DefaultValueData = 10;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveGraphicalLineStart_12_DefaultValue = 
{
	&TextEditOp_t1071____MoveGraphicalLineStart_12_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveGraphicalLineStart_12_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveGraphicalLineEnd_13_DefaultValueData = 11;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveGraphicalLineEnd_13_DefaultValue = 
{
	&TextEditOp_t1071____MoveGraphicalLineEnd_13_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveGraphicalLineEnd_13_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveWordLeft_14_DefaultValueData = 12;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveWordLeft_14_DefaultValue = 
{
	&TextEditOp_t1071____MoveWordLeft_14_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveWordLeft_14_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveWordRight_15_DefaultValueData = 13;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveWordRight_15_DefaultValue = 
{
	&TextEditOp_t1071____MoveWordRight_15_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveWordRight_15_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveParagraphForward_16_DefaultValueData = 14;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveParagraphForward_16_DefaultValue = 
{
	&TextEditOp_t1071____MoveParagraphForward_16_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveParagraphForward_16_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveParagraphBackward_17_DefaultValueData = 15;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveParagraphBackward_17_DefaultValue = 
{
	&TextEditOp_t1071____MoveParagraphBackward_17_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveParagraphBackward_17_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveToStartOfNextWord_18_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveToStartOfNextWord_18_DefaultValue = 
{
	&TextEditOp_t1071____MoveToStartOfNextWord_18_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveToStartOfNextWord_18_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____MoveToEndOfPreviousWord_19_DefaultValueData = 17;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____MoveToEndOfPreviousWord_19_DefaultValue = 
{
	&TextEditOp_t1071____MoveToEndOfPreviousWord_19_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____MoveToEndOfPreviousWord_19_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectLeft_20_DefaultValueData = 18;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectLeft_20_DefaultValue = 
{
	&TextEditOp_t1071____SelectLeft_20_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectLeft_20_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectRight_21_DefaultValueData = 19;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectRight_21_DefaultValue = 
{
	&TextEditOp_t1071____SelectRight_21_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectRight_21_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectUp_22_DefaultValueData = 20;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectUp_22_DefaultValue = 
{
	&TextEditOp_t1071____SelectUp_22_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectUp_22_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectDown_23_DefaultValueData = 21;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectDown_23_DefaultValue = 
{
	&TextEditOp_t1071____SelectDown_23_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectDown_23_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectTextStart_24_DefaultValueData = 22;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectTextStart_24_DefaultValue = 
{
	&TextEditOp_t1071____SelectTextStart_24_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectTextStart_24_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectTextEnd_25_DefaultValueData = 23;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectTextEnd_25_DefaultValue = 
{
	&TextEditOp_t1071____SelectTextEnd_25_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectTextEnd_25_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectPageUp_26_DefaultValueData = 24;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectPageUp_26_DefaultValue = 
{
	&TextEditOp_t1071____SelectPageUp_26_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectPageUp_26_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectPageDown_27_DefaultValueData = 25;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectPageDown_27_DefaultValue = 
{
	&TextEditOp_t1071____SelectPageDown_27_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectPageDown_27_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____ExpandSelectGraphicalLineStart_28_DefaultValueData = 26;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____ExpandSelectGraphicalLineStart_28_DefaultValue = 
{
	&TextEditOp_t1071____ExpandSelectGraphicalLineStart_28_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____ExpandSelectGraphicalLineStart_28_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____ExpandSelectGraphicalLineEnd_29_DefaultValueData = 27;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____ExpandSelectGraphicalLineEnd_29_DefaultValue = 
{
	&TextEditOp_t1071____ExpandSelectGraphicalLineEnd_29_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____ExpandSelectGraphicalLineEnd_29_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectGraphicalLineStart_30_DefaultValueData = 28;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectGraphicalLineStart_30_DefaultValue = 
{
	&TextEditOp_t1071____SelectGraphicalLineStart_30_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectGraphicalLineStart_30_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectGraphicalLineEnd_31_DefaultValueData = 29;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectGraphicalLineEnd_31_DefaultValue = 
{
	&TextEditOp_t1071____SelectGraphicalLineEnd_31_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectGraphicalLineEnd_31_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectWordLeft_32_DefaultValueData = 30;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectWordLeft_32_DefaultValue = 
{
	&TextEditOp_t1071____SelectWordLeft_32_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectWordLeft_32_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectWordRight_33_DefaultValueData = 31;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectWordRight_33_DefaultValue = 
{
	&TextEditOp_t1071____SelectWordRight_33_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectWordRight_33_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectToEndOfPreviousWord_34_DefaultValueData = 32;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectToEndOfPreviousWord_34_DefaultValue = 
{
	&TextEditOp_t1071____SelectToEndOfPreviousWord_34_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectToEndOfPreviousWord_34_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectToStartOfNextWord_35_DefaultValueData = 33;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectToStartOfNextWord_35_DefaultValue = 
{
	&TextEditOp_t1071____SelectToStartOfNextWord_35_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectToStartOfNextWord_35_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectParagraphBackward_36_DefaultValueData = 34;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectParagraphBackward_36_DefaultValue = 
{
	&TextEditOp_t1071____SelectParagraphBackward_36_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectParagraphBackward_36_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectParagraphForward_37_DefaultValueData = 35;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectParagraphForward_37_DefaultValue = 
{
	&TextEditOp_t1071____SelectParagraphForward_37_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectParagraphForward_37_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____Delete_38_DefaultValueData = 36;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____Delete_38_DefaultValue = 
{
	&TextEditOp_t1071____Delete_38_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____Delete_38_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____Backspace_39_DefaultValueData = 37;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____Backspace_39_DefaultValue = 
{
	&TextEditOp_t1071____Backspace_39_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____Backspace_39_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____DeleteWordBack_40_DefaultValueData = 38;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____DeleteWordBack_40_DefaultValue = 
{
	&TextEditOp_t1071____DeleteWordBack_40_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____DeleteWordBack_40_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____DeleteWordForward_41_DefaultValueData = 39;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____DeleteWordForward_41_DefaultValue = 
{
	&TextEditOp_t1071____DeleteWordForward_41_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____DeleteWordForward_41_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____DeleteLineBack_42_DefaultValueData = 40;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____DeleteLineBack_42_DefaultValue = 
{
	&TextEditOp_t1071____DeleteLineBack_42_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____DeleteLineBack_42_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____Cut_43_DefaultValueData = 41;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____Cut_43_DefaultValue = 
{
	&TextEditOp_t1071____Cut_43_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____Cut_43_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____Copy_44_DefaultValueData = 42;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____Copy_44_DefaultValue = 
{
	&TextEditOp_t1071____Copy_44_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____Copy_44_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____Paste_45_DefaultValueData = 43;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____Paste_45_DefaultValue = 
{
	&TextEditOp_t1071____Paste_45_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____Paste_45_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectAll_46_DefaultValueData = 44;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectAll_46_DefaultValue = 
{
	&TextEditOp_t1071____SelectAll_46_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectAll_46_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____SelectNone_47_DefaultValueData = 45;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____SelectNone_47_DefaultValue = 
{
	&TextEditOp_t1071____SelectNone_47_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____SelectNone_47_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____ScrollStart_48_DefaultValueData = 46;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____ScrollStart_48_DefaultValue = 
{
	&TextEditOp_t1071____ScrollStart_48_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____ScrollStart_48_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____ScrollEnd_49_DefaultValueData = 47;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____ScrollEnd_49_DefaultValue = 
{
	&TextEditOp_t1071____ScrollEnd_49_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____ScrollEnd_49_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____ScrollPageUp_50_DefaultValueData = 48;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____ScrollPageUp_50_DefaultValue = 
{
	&TextEditOp_t1071____ScrollPageUp_50_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____ScrollPageUp_50_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TextEditOp_t1071____ScrollPageDown_51_DefaultValueData = 49;
static Il2CppFieldDefaultValueEntry TextEditOp_t1071____ScrollPageDown_51_DefaultValue = 
{
	&TextEditOp_t1071____ScrollPageDown_51_FieldInfo/* field */
	, { (char*)&TextEditOp_t1071____ScrollPageDown_51_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TextEditOp_t1071_FieldDefaultValues[] = 
{
	&TextEditOp_t1071____MoveLeft_2_DefaultValue,
	&TextEditOp_t1071____MoveRight_3_DefaultValue,
	&TextEditOp_t1071____MoveUp_4_DefaultValue,
	&TextEditOp_t1071____MoveDown_5_DefaultValue,
	&TextEditOp_t1071____MoveLineStart_6_DefaultValue,
	&TextEditOp_t1071____MoveLineEnd_7_DefaultValue,
	&TextEditOp_t1071____MoveTextStart_8_DefaultValue,
	&TextEditOp_t1071____MoveTextEnd_9_DefaultValue,
	&TextEditOp_t1071____MovePageUp_10_DefaultValue,
	&TextEditOp_t1071____MovePageDown_11_DefaultValue,
	&TextEditOp_t1071____MoveGraphicalLineStart_12_DefaultValue,
	&TextEditOp_t1071____MoveGraphicalLineEnd_13_DefaultValue,
	&TextEditOp_t1071____MoveWordLeft_14_DefaultValue,
	&TextEditOp_t1071____MoveWordRight_15_DefaultValue,
	&TextEditOp_t1071____MoveParagraphForward_16_DefaultValue,
	&TextEditOp_t1071____MoveParagraphBackward_17_DefaultValue,
	&TextEditOp_t1071____MoveToStartOfNextWord_18_DefaultValue,
	&TextEditOp_t1071____MoveToEndOfPreviousWord_19_DefaultValue,
	&TextEditOp_t1071____SelectLeft_20_DefaultValue,
	&TextEditOp_t1071____SelectRight_21_DefaultValue,
	&TextEditOp_t1071____SelectUp_22_DefaultValue,
	&TextEditOp_t1071____SelectDown_23_DefaultValue,
	&TextEditOp_t1071____SelectTextStart_24_DefaultValue,
	&TextEditOp_t1071____SelectTextEnd_25_DefaultValue,
	&TextEditOp_t1071____SelectPageUp_26_DefaultValue,
	&TextEditOp_t1071____SelectPageDown_27_DefaultValue,
	&TextEditOp_t1071____ExpandSelectGraphicalLineStart_28_DefaultValue,
	&TextEditOp_t1071____ExpandSelectGraphicalLineEnd_29_DefaultValue,
	&TextEditOp_t1071____SelectGraphicalLineStart_30_DefaultValue,
	&TextEditOp_t1071____SelectGraphicalLineEnd_31_DefaultValue,
	&TextEditOp_t1071____SelectWordLeft_32_DefaultValue,
	&TextEditOp_t1071____SelectWordRight_33_DefaultValue,
	&TextEditOp_t1071____SelectToEndOfPreviousWord_34_DefaultValue,
	&TextEditOp_t1071____SelectToStartOfNextWord_35_DefaultValue,
	&TextEditOp_t1071____SelectParagraphBackward_36_DefaultValue,
	&TextEditOp_t1071____SelectParagraphForward_37_DefaultValue,
	&TextEditOp_t1071____Delete_38_DefaultValue,
	&TextEditOp_t1071____Backspace_39_DefaultValue,
	&TextEditOp_t1071____DeleteWordBack_40_DefaultValue,
	&TextEditOp_t1071____DeleteWordForward_41_DefaultValue,
	&TextEditOp_t1071____DeleteLineBack_42_DefaultValue,
	&TextEditOp_t1071____Cut_43_DefaultValue,
	&TextEditOp_t1071____Copy_44_DefaultValue,
	&TextEditOp_t1071____Paste_45_DefaultValue,
	&TextEditOp_t1071____SelectAll_46_DefaultValue,
	&TextEditOp_t1071____SelectNone_47_DefaultValue,
	&TextEditOp_t1071____ScrollStart_48_DefaultValue,
	&TextEditOp_t1071____ScrollEnd_49_DefaultValue,
	&TextEditOp_t1071____ScrollPageUp_50_DefaultValue,
	&TextEditOp_t1071____ScrollPageDown_51_DefaultValue,
	NULL
};
static MethodInfo* TextEditOp_t1071_MethodInfos[] =
{
	NULL
};
static MethodInfo* TextEditOp_t1071_VTable[] =
{
	&Enum_Equals_m191_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Enum_GetHashCode_m193_MethodInfo,
	&Enum_ToString_m194_MethodInfo,
	&Enum_ToString_m195_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m196_MethodInfo,
	&Enum_System_IConvertible_ToByte_m197_MethodInfo,
	&Enum_System_IConvertible_ToChar_m198_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m199_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m200_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m201_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m202_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m203_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m204_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m205_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m206_MethodInfo,
	&Enum_ToString_m207_MethodInfo,
	&Enum_System_IConvertible_ToType_m208_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m209_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m210_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m211_MethodInfo,
	&Enum_CompareTo_m212_MethodInfo,
	&Enum_GetTypeCode_m213_MethodInfo,
};
static Il2CppInterfaceOffsetPair TextEditOp_t1071_InterfacesOffsets[] = 
{
	{ &IFormattable_t94_il2cpp_TypeInfo, 4},
	{ &IConvertible_t95_il2cpp_TypeInfo, 5},
	{ &IComparable_t96_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextEditOp_t1071_0_0_0;
extern Il2CppType TextEditOp_t1071_1_0_0;
TypeInfo TextEditOp_t1071_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditOp"/* name */
	, ""/* namespaze */
	, TextEditOp_t1071_MethodInfos/* methods */
	, NULL/* properties */
	, TextEditOp_t1071_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t97_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &TextEditor_t484_il2cpp_TypeInfo/* nested_in */
	, &Int32_t93_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TextEditOp_t1071_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &TextEditOp_t1071_0_0_0/* byval_arg */
	, &TextEditOp_t1071_1_0_0/* this_arg */
	, TextEditOp_t1071_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TextEditOp_t1071_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditOp_t1071)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 51/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditor.h"
#ifndef _MSC_VER
#else
#endif
// UnityEngine.TextEditor
#include "UnityEngine_UnityEngine_TextEditorMethodDeclarations.h"

// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContent.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyle.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffset.h"
extern TypeInfo GUIContent_t485_il2cpp_TypeInfo;
extern TypeInfo GUIStyle_t953_il2cpp_TypeInfo;
extern TypeInfo Rect_t118_il2cpp_TypeInfo;
extern TypeInfo Vector2_t9_il2cpp_TypeInfo;
extern TypeInfo GUIUtility_t966_il2cpp_TypeInfo;
// UnityEngine.GUIContent
#include "UnityEngine_UnityEngine_GUIContentMethodDeclarations.h"
// UnityEngine.GUIStyle
#include "UnityEngine_UnityEngine_GUIStyleMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.RectOffset
#include "UnityEngine_UnityEngine_RectOffsetMethodDeclarations.h"
// UnityEngine.GUIUtility
#include "UnityEngine_UnityEngine_GUIUtilityMethodDeclarations.h"
extern MethodInfo GUIContent__ctor_m5706_MethodInfo;
extern MethodInfo GUIStyle_get_none_m5754_MethodInfo;
extern MethodInfo Vector2_get_zero_m232_MethodInfo;
extern MethodInfo TextEditor_SelectAll_m6339_MethodInfo;
extern MethodInfo GUIContent_get_text_m2270_MethodInfo;
extern MethodInfo TextEditor_ClearCursorPos_m6338_MethodInfo;
extern MethodInfo GUIContent_set_text_m5708_MethodInfo;
extern MethodInfo TextEditor_DeleteSelection_m6340_MethodInfo;
extern MethodInfo TextEditor_UpdateScrollOffset_m6342_MethodInfo;
extern MethodInfo Rect_get_width_m2125_MethodInfo;
extern MethodInfo Rect_get_height_m2126_MethodInfo;
extern MethodInfo Rect__ctor_m332_MethodInfo;
extern MethodInfo GUIStyle_GetCursorPixelPosition_m5755_MethodInfo;
extern MethodInfo GUIStyle_get_padding_m5743_MethodInfo;
extern MethodInfo RectOffset_Remove_m5730_MethodInfo;
extern MethodInfo GUIStyle_CalcSize_m5758_MethodInfo;
extern MethodInfo GUIStyle_CalcHeight_m5760_MethodInfo;
extern MethodInfo Vector2__ctor_m233_MethodInfo;
extern MethodInfo RectOffset_get_left_m2501_MethodInfo;
extern MethodInfo GUIStyle_get_lineHeight_m5752_MethodInfo;
extern MethodInfo RectOffset_get_top_m2502_MethodInfo;
extern MethodInfo RectOffset_get_bottom_m5728_MethodInfo;
extern MethodInfo GUIUtility_set_systemCopyBuffer_m5638_MethodInfo;
extern MethodInfo GUIUtility_get_systemCopyBuffer_m5637_MethodInfo;
extern MethodInfo String_op_Inequality_m2280_MethodInfo;
extern MethodInfo TextEditor_ReplaceNewlinesWithSpaces_m6343_MethodInfo;
extern MethodInfo TextEditor_ReplaceSelection_m6341_MethodInfo;


// System.Void UnityEngine.TextEditor::.ctor()
extern MethodInfo TextEditor__ctor_m2268_MethodInfo;
 void TextEditor__ctor_m2268 (TextEditor_t484 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUIContent_t485_il2cpp_TypeInfo));
		GUIContent_t485 * L_0 = (GUIContent_t485 *)il2cpp_codegen_object_new (InitializedTypeInfo(&GUIContent_t485_il2cpp_TypeInfo));
		GUIContent__ctor_m5706(L_0, /*hidden argument*/&GUIContent__ctor_m5706_MethodInfo);
		__this->___content_4 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUIStyle_t953_il2cpp_TypeInfo));
		GUIStyle_t953 * L_1 = GUIStyle_get_none_m5754(NULL /*static, unused*/, /*hidden argument*/&GUIStyle_get_none_m5754_MethodInfo);
		__this->___style_5 = L_1;
		Vector2_t9  L_2 = Vector2_get_zero_m232(NULL /*static, unused*/, /*hidden argument*/&Vector2_get_zero_m232_MethodInfo);
		__this->___scrollOffset_11 = L_2;
		__this->___m_iAltCursorPos_19 = (-1);
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.TextEditor::ClearCursorPos()
 void TextEditor_ClearCursorPos_m6338 (TextEditor_t484 * __this, MethodInfo* method){
	{
		__this->___hasHorizontalCursorPos_8 = 0;
		__this->___m_iAltCursorPos_19 = (-1);
		return;
	}
}
// System.Void UnityEngine.TextEditor::OnFocus()
extern MethodInfo TextEditor_OnFocus_m2272_MethodInfo;
 void TextEditor_OnFocus_m2272 (TextEditor_t484 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	{
		bool L_0 = (__this->___multiline_7);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		int32_t L_1 = 0;
		V_0 = L_1;
		__this->___selectPos_2 = L_1;
		__this->___pos_1 = V_0;
		goto IL_0026;
	}

IL_0020:
	{
		TextEditor_SelectAll_m6339(__this, /*hidden argument*/&TextEditor_SelectAll_m6339_MethodInfo);
	}

IL_0026:
	{
		__this->___m_HasFocus_10 = 1;
		return;
	}
}
// System.Void UnityEngine.TextEditor::SelectAll()
 void TextEditor_SelectAll_m6339 (TextEditor_t484 * __this, MethodInfo* method){
	{
		__this->___pos_1 = 0;
		GUIContent_t485 * L_0 = (__this->___content_4);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m2270(L_0, /*hidden argument*/&GUIContent_get_text_m2270_MethodInfo);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m2255(L_1, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		__this->___selectPos_2 = L_2;
		TextEditor_ClearCursorPos_m6338(__this, /*hidden argument*/&TextEditor_ClearCursorPos_m6338_MethodInfo);
		return;
	}
}
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
 bool TextEditor_DeleteSelection_m6340 (TextEditor_t484 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	{
		GUIContent_t485 * L_0 = (__this->___content_4);
		NullCheck(L_0);
		String_t* L_1 = GUIContent_get_text_m2270(L_0, /*hidden argument*/&GUIContent_get_text_m2270_MethodInfo);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m2255(L_1, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		V_0 = L_2;
		int32_t L_3 = (__this->___pos_1);
		if ((((int32_t)L_3) <= ((int32_t)V_0)))
		{
			goto IL_0024;
		}
	}
	{
		__this->___pos_1 = V_0;
	}

IL_0024:
	{
		int32_t L_4 = (__this->___selectPos_2);
		if ((((int32_t)L_4) <= ((int32_t)V_0)))
		{
			goto IL_0037;
		}
	}
	{
		__this->___selectPos_2 = V_0;
	}

IL_0037:
	{
		int32_t L_5 = (__this->___pos_1);
		int32_t L_6 = (__this->___selectPos_2);
		if ((((uint32_t)L_5) != ((uint32_t)L_6)))
		{
			goto IL_004a;
		}
	}
	{
		return 0;
	}

IL_004a:
	{
		int32_t L_7 = (__this->___pos_1);
		int32_t L_8 = (__this->___selectPos_2);
		if ((((int32_t)L_7) >= ((int32_t)L_8)))
		{
			goto IL_00c0;
		}
	}
	{
		GUIContent_t485 * L_9 = (__this->___content_4);
		GUIContent_t485 * L_10 = (__this->___content_4);
		NullCheck(L_10);
		String_t* L_11 = GUIContent_get_text_m2270(L_10, /*hidden argument*/&GUIContent_get_text_m2270_MethodInfo);
		int32_t L_12 = (__this->___pos_1);
		NullCheck(L_11);
		String_t* L_13 = String_Substring_m2281(L_11, 0, L_12, /*hidden argument*/&String_Substring_m2281_MethodInfo);
		GUIContent_t485 * L_14 = (__this->___content_4);
		NullCheck(L_14);
		String_t* L_15 = GUIContent_get_text_m2270(L_14, /*hidden argument*/&GUIContent_get_text_m2270_MethodInfo);
		int32_t L_16 = (__this->___selectPos_2);
		GUIContent_t485 * L_17 = (__this->___content_4);
		NullCheck(L_17);
		String_t* L_18 = GUIContent_get_text_m2270(L_17, /*hidden argument*/&GUIContent_get_text_m2270_MethodInfo);
		NullCheck(L_18);
		int32_t L_19 = String_get_Length_m2255(L_18, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		int32_t L_20 = (__this->___selectPos_2);
		NullCheck(L_15);
		String_t* L_21 = String_Substring_m2281(L_15, L_16, ((int32_t)(L_19-L_20)), /*hidden argument*/&String_Substring_m2281_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_22 = String_Concat_m282(NULL /*static, unused*/, L_13, L_21, /*hidden argument*/&String_Concat_m282_MethodInfo);
		NullCheck(L_9);
		GUIContent_set_text_m5708(L_9, L_22, /*hidden argument*/&GUIContent_set_text_m5708_MethodInfo);
		int32_t L_23 = (__this->___pos_1);
		__this->___selectPos_2 = L_23;
		goto IL_0120;
	}

IL_00c0:
	{
		GUIContent_t485 * L_24 = (__this->___content_4);
		GUIContent_t485 * L_25 = (__this->___content_4);
		NullCheck(L_25);
		String_t* L_26 = GUIContent_get_text_m2270(L_25, /*hidden argument*/&GUIContent_get_text_m2270_MethodInfo);
		int32_t L_27 = (__this->___selectPos_2);
		NullCheck(L_26);
		String_t* L_28 = String_Substring_m2281(L_26, 0, L_27, /*hidden argument*/&String_Substring_m2281_MethodInfo);
		GUIContent_t485 * L_29 = (__this->___content_4);
		NullCheck(L_29);
		String_t* L_30 = GUIContent_get_text_m2270(L_29, /*hidden argument*/&GUIContent_get_text_m2270_MethodInfo);
		int32_t L_31 = (__this->___pos_1);
		GUIContent_t485 * L_32 = (__this->___content_4);
		NullCheck(L_32);
		String_t* L_33 = GUIContent_get_text_m2270(L_32, /*hidden argument*/&GUIContent_get_text_m2270_MethodInfo);
		NullCheck(L_33);
		int32_t L_34 = String_get_Length_m2255(L_33, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		int32_t L_35 = (__this->___pos_1);
		NullCheck(L_30);
		String_t* L_36 = String_Substring_m2281(L_30, L_31, ((int32_t)(L_34-L_35)), /*hidden argument*/&String_Substring_m2281_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_37 = String_Concat_m282(NULL /*static, unused*/, L_28, L_36, /*hidden argument*/&String_Concat_m282_MethodInfo);
		NullCheck(L_24);
		GUIContent_set_text_m5708(L_24, L_37, /*hidden argument*/&GUIContent_set_text_m5708_MethodInfo);
		int32_t L_38 = (__this->___selectPos_2);
		__this->___pos_1 = L_38;
	}

IL_0120:
	{
		TextEditor_ClearCursorPos_m6338(__this, /*hidden argument*/&TextEditor_ClearCursorPos_m6338_MethodInfo);
		return 1;
	}
}
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
 void TextEditor_ReplaceSelection_m6341 (TextEditor_t484 * __this, String_t* ___replace, MethodInfo* method){
	int32_t V_0 = 0;
	{
		TextEditor_DeleteSelection_m6340(__this, /*hidden argument*/&TextEditor_DeleteSelection_m6340_MethodInfo);
		GUIContent_t485 * L_0 = (__this->___content_4);
		GUIContent_t485 * L_1 = (__this->___content_4);
		NullCheck(L_1);
		String_t* L_2 = GUIContent_get_text_m2270(L_1, /*hidden argument*/&GUIContent_get_text_m2270_MethodInfo);
		int32_t L_3 = (__this->___pos_1);
		NullCheck(L_2);
		String_t* L_4 = String_Insert_m2307(L_2, L_3, ___replace, /*hidden argument*/&String_Insert_m2307_MethodInfo);
		NullCheck(L_0);
		GUIContent_set_text_m5708(L_0, L_4, /*hidden argument*/&GUIContent_set_text_m5708_MethodInfo);
		int32_t L_5 = (__this->___pos_1);
		NullCheck(___replace);
		int32_t L_6 = String_get_Length_m2255(___replace, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		int32_t L_7 = ((int32_t)(L_5+L_6));
		V_0 = L_7;
		__this->___pos_1 = L_7;
		__this->___selectPos_2 = V_0;
		TextEditor_ClearCursorPos_m6338(__this, /*hidden argument*/&TextEditor_ClearCursorPos_m6338_MethodInfo);
		TextEditor_UpdateScrollOffset_m6342(__this, /*hidden argument*/&TextEditor_UpdateScrollOffset_m6342_MethodInfo);
		__this->___m_TextHeightPotentiallyChanged_12 = 1;
		return;
	}
}
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
 void TextEditor_UpdateScrollOffset_m6342 (TextEditor_t484 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	Rect_t118  V_1 = {0};
	Vector2_t9  V_2 = {0};
	Vector2_t9  V_3 = {0};
	Vector2_t9 * G_B17_0 = {0};
	Vector2_t9 * G_B16_0 = {0};
	float G_B18_0 = 0.0f;
	Vector2_t9 * G_B18_1 = {0};
	{
		int32_t L_0 = (__this->___pos_1);
		V_0 = L_0;
		GUIStyle_t953 * L_1 = (__this->___style_5);
		Rect_t118 * L_2 = &(__this->___position_6);
		float L_3 = Rect_get_width_m2125(L_2, /*hidden argument*/&Rect_get_width_m2125_MethodInfo);
		Rect_t118 * L_4 = &(__this->___position_6);
		float L_5 = Rect_get_height_m2126(L_4, /*hidden argument*/&Rect_get_height_m2126_MethodInfo);
		Rect_t118  L_6 = {0};
		Rect__ctor_m332(&L_6, (0.0f), (0.0f), L_3, L_5, /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		GUIContent_t485 * L_7 = (__this->___content_4);
		NullCheck(L_1);
		Vector2_t9  L_8 = GUIStyle_GetCursorPixelPosition_m5755(L_1, L_6, L_7, V_0, /*hidden argument*/&GUIStyle_GetCursorPixelPosition_m5755_MethodInfo);
		__this->___graphicalCursorPos_13 = L_8;
		GUIStyle_t953 * L_9 = (__this->___style_5);
		NullCheck(L_9);
		RectOffset_t391 * L_10 = GUIStyle_get_padding_m5743(L_9, /*hidden argument*/&GUIStyle_get_padding_m5743_MethodInfo);
		Rect_t118  L_11 = (__this->___position_6);
		NullCheck(L_10);
		Rect_t118  L_12 = RectOffset_Remove_m5730(L_10, L_11, /*hidden argument*/&RectOffset_Remove_m5730_MethodInfo);
		V_1 = L_12;
		GUIStyle_t953 * L_13 = (__this->___style_5);
		GUIContent_t485 * L_14 = (__this->___content_4);
		NullCheck(L_13);
		Vector2_t9  L_15 = GUIStyle_CalcSize_m5758(L_13, L_14, /*hidden argument*/&GUIStyle_CalcSize_m5758_MethodInfo);
		V_3 = L_15;
		NullCheck((&V_3));
		float L_16 = ((&V_3)->___x_1);
		GUIStyle_t953 * L_17 = (__this->___style_5);
		GUIContent_t485 * L_18 = (__this->___content_4);
		Rect_t118 * L_19 = &(__this->___position_6);
		float L_20 = Rect_get_width_m2125(L_19, /*hidden argument*/&Rect_get_width_m2125_MethodInfo);
		NullCheck(L_17);
		float L_21 = GUIStyle_CalcHeight_m5760(L_17, L_18, L_20, /*hidden argument*/&GUIStyle_CalcHeight_m5760_MethodInfo);
		Vector2__ctor_m233((&V_2), L_16, L_21, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		NullCheck((&V_2));
		float L_22 = ((&V_2)->___x_1);
		Rect_t118 * L_23 = &(__this->___position_6);
		float L_24 = Rect_get_width_m2125(L_23, /*hidden argument*/&Rect_get_width_m2125_MethodInfo);
		if ((((float)L_22) >= ((float)L_24)))
		{
			goto IL_00c3;
		}
	}
	{
		Vector2_t9 * L_25 = &(__this->___scrollOffset_11);
		NullCheck(L_25);
		L_25->___x_1 = (0.0f);
		goto IL_015f;
	}

IL_00c3:
	{
		Vector2_t9 * L_26 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_26);
		float L_27 = (L_26->___x_1);
		Vector2_t9 * L_28 = &(__this->___scrollOffset_11);
		NullCheck(L_28);
		float L_29 = (L_28->___x_1);
		float L_30 = Rect_get_width_m2125((&V_1), /*hidden argument*/&Rect_get_width_m2125_MethodInfo);
		if ((((float)((float)(L_27+(1.0f)))) <= ((float)((float)(L_29+L_30)))))
		{
			goto IL_010a;
		}
	}
	{
		Vector2_t9 * L_31 = &(__this->___scrollOffset_11);
		Vector2_t9 * L_32 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_32);
		float L_33 = (L_32->___x_1);
		float L_34 = Rect_get_width_m2125((&V_1), /*hidden argument*/&Rect_get_width_m2125_MethodInfo);
		NullCheck(L_31);
		L_31->___x_1 = ((float)(L_33-L_34));
	}

IL_010a:
	{
		Vector2_t9 * L_35 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_35);
		float L_36 = (L_35->___x_1);
		Vector2_t9 * L_37 = &(__this->___scrollOffset_11);
		NullCheck(L_37);
		float L_38 = (L_37->___x_1);
		GUIStyle_t953 * L_39 = (__this->___style_5);
		NullCheck(L_39);
		RectOffset_t391 * L_40 = GUIStyle_get_padding_m5743(L_39, /*hidden argument*/&GUIStyle_get_padding_m5743_MethodInfo);
		NullCheck(L_40);
		int32_t L_41 = RectOffset_get_left_m2501(L_40, /*hidden argument*/&RectOffset_get_left_m2501_MethodInfo);
		if ((((float)L_36) >= ((float)((float)(L_38+(((float)L_41)))))))
		{
			goto IL_015f;
		}
	}
	{
		Vector2_t9 * L_42 = &(__this->___scrollOffset_11);
		Vector2_t9 * L_43 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_43);
		float L_44 = (L_43->___x_1);
		GUIStyle_t953 * L_45 = (__this->___style_5);
		NullCheck(L_45);
		RectOffset_t391 * L_46 = GUIStyle_get_padding_m5743(L_45, /*hidden argument*/&GUIStyle_get_padding_m5743_MethodInfo);
		NullCheck(L_46);
		int32_t L_47 = RectOffset_get_left_m2501(L_46, /*hidden argument*/&RectOffset_get_left_m2501_MethodInfo);
		NullCheck(L_42);
		L_42->___x_1 = ((float)(L_44-(((float)L_47))));
	}

IL_015f:
	{
		NullCheck((&V_2));
		float L_48 = ((&V_2)->___y_2);
		float L_49 = Rect_get_height_m2126((&V_1), /*hidden argument*/&Rect_get_height_m2126_MethodInfo);
		if ((((float)L_48) >= ((float)L_49)))
		{
			goto IL_0187;
		}
	}
	{
		Vector2_t9 * L_50 = &(__this->___scrollOffset_11);
		NullCheck(L_50);
		L_50->___y_2 = (0.0f);
		goto IL_0259;
	}

IL_0187:
	{
		Vector2_t9 * L_51 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_51);
		float L_52 = (L_51->___y_2);
		GUIStyle_t953 * L_53 = (__this->___style_5);
		NullCheck(L_53);
		float L_54 = GUIStyle_get_lineHeight_m5752(L_53, /*hidden argument*/&GUIStyle_get_lineHeight_m5752_MethodInfo);
		Vector2_t9 * L_55 = &(__this->___scrollOffset_11);
		NullCheck(L_55);
		float L_56 = (L_55->___y_2);
		float L_57 = Rect_get_height_m2126((&V_1), /*hidden argument*/&Rect_get_height_m2126_MethodInfo);
		GUIStyle_t953 * L_58 = (__this->___style_5);
		NullCheck(L_58);
		RectOffset_t391 * L_59 = GUIStyle_get_padding_m5743(L_58, /*hidden argument*/&GUIStyle_get_padding_m5743_MethodInfo);
		NullCheck(L_59);
		int32_t L_60 = RectOffset_get_top_m2502(L_59, /*hidden argument*/&RectOffset_get_top_m2502_MethodInfo);
		if ((((float)((float)(L_52+L_54))) <= ((float)((float)(((float)(L_56+L_57))+(((float)L_60)))))))
		{
			goto IL_0204;
		}
	}
	{
		Vector2_t9 * L_61 = &(__this->___scrollOffset_11);
		Vector2_t9 * L_62 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_62);
		float L_63 = (L_62->___y_2);
		float L_64 = Rect_get_height_m2126((&V_1), /*hidden argument*/&Rect_get_height_m2126_MethodInfo);
		GUIStyle_t953 * L_65 = (__this->___style_5);
		NullCheck(L_65);
		RectOffset_t391 * L_66 = GUIStyle_get_padding_m5743(L_65, /*hidden argument*/&GUIStyle_get_padding_m5743_MethodInfo);
		NullCheck(L_66);
		int32_t L_67 = RectOffset_get_top_m2502(L_66, /*hidden argument*/&RectOffset_get_top_m2502_MethodInfo);
		GUIStyle_t953 * L_68 = (__this->___style_5);
		NullCheck(L_68);
		float L_69 = GUIStyle_get_lineHeight_m5752(L_68, /*hidden argument*/&GUIStyle_get_lineHeight_m5752_MethodInfo);
		NullCheck(L_61);
		L_61->___y_2 = ((float)(((float)(((float)(L_63-L_64))-(((float)L_67))))+L_69));
	}

IL_0204:
	{
		Vector2_t9 * L_70 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_70);
		float L_71 = (L_70->___y_2);
		Vector2_t9 * L_72 = &(__this->___scrollOffset_11);
		NullCheck(L_72);
		float L_73 = (L_72->___y_2);
		GUIStyle_t953 * L_74 = (__this->___style_5);
		NullCheck(L_74);
		RectOffset_t391 * L_75 = GUIStyle_get_padding_m5743(L_74, /*hidden argument*/&GUIStyle_get_padding_m5743_MethodInfo);
		NullCheck(L_75);
		int32_t L_76 = RectOffset_get_top_m2502(L_75, /*hidden argument*/&RectOffset_get_top_m2502_MethodInfo);
		if ((((float)L_71) >= ((float)((float)(L_73+(((float)L_76)))))))
		{
			goto IL_0259;
		}
	}
	{
		Vector2_t9 * L_77 = &(__this->___scrollOffset_11);
		Vector2_t9 * L_78 = &(__this->___graphicalCursorPos_13);
		NullCheck(L_78);
		float L_79 = (L_78->___y_2);
		GUIStyle_t953 * L_80 = (__this->___style_5);
		NullCheck(L_80);
		RectOffset_t391 * L_81 = GUIStyle_get_padding_m5743(L_80, /*hidden argument*/&GUIStyle_get_padding_m5743_MethodInfo);
		NullCheck(L_81);
		int32_t L_82 = RectOffset_get_top_m2502(L_81, /*hidden argument*/&RectOffset_get_top_m2502_MethodInfo);
		NullCheck(L_77);
		L_77->___y_2 = ((float)(L_79-(((float)L_82))));
	}

IL_0259:
	{
		Vector2_t9 * L_83 = &(__this->___scrollOffset_11);
		NullCheck(L_83);
		float L_84 = (L_83->___y_2);
		if ((((float)L_84) <= ((float)(0.0f))))
		{
			goto IL_02cb;
		}
	}
	{
		NullCheck((&V_2));
		float L_85 = ((&V_2)->___y_2);
		Vector2_t9 * L_86 = &(__this->___scrollOffset_11);
		NullCheck(L_86);
		float L_87 = (L_86->___y_2);
		float L_88 = Rect_get_height_m2126((&V_1), /*hidden argument*/&Rect_get_height_m2126_MethodInfo);
		if ((((float)((float)(L_85-L_87))) >= ((float)L_88)))
		{
			goto IL_02cb;
		}
	}
	{
		Vector2_t9 * L_89 = &(__this->___scrollOffset_11);
		NullCheck((&V_2));
		float L_90 = ((&V_2)->___y_2);
		float L_91 = Rect_get_height_m2126((&V_1), /*hidden argument*/&Rect_get_height_m2126_MethodInfo);
		GUIStyle_t953 * L_92 = (__this->___style_5);
		NullCheck(L_92);
		RectOffset_t391 * L_93 = GUIStyle_get_padding_m5743(L_92, /*hidden argument*/&GUIStyle_get_padding_m5743_MethodInfo);
		NullCheck(L_93);
		int32_t L_94 = RectOffset_get_top_m2502(L_93, /*hidden argument*/&RectOffset_get_top_m2502_MethodInfo);
		GUIStyle_t953 * L_95 = (__this->___style_5);
		NullCheck(L_95);
		RectOffset_t391 * L_96 = GUIStyle_get_padding_m5743(L_95, /*hidden argument*/&GUIStyle_get_padding_m5743_MethodInfo);
		NullCheck(L_96);
		int32_t L_97 = RectOffset_get_bottom_m5728(L_96, /*hidden argument*/&RectOffset_get_bottom_m5728_MethodInfo);
		NullCheck(L_89);
		L_89->___y_2 = ((float)(((float)(((float)(L_90-L_91))-(((float)L_94))))-(((float)L_97))));
	}

IL_02cb:
	{
		Vector2_t9 * L_98 = &(__this->___scrollOffset_11);
		Vector2_t9 * L_99 = &(__this->___scrollOffset_11);
		NullCheck(L_99);
		float L_100 = (L_99->___y_2);
		G_B16_0 = L_98;
		if ((((float)L_100) >= ((float)(0.0f))))
		{
			G_B17_0 = L_98;
			goto IL_02f0;
		}
	}
	{
		G_B18_0 = (0.0f);
		G_B18_1 = G_B16_0;
		goto IL_02fb;
	}

IL_02f0:
	{
		Vector2_t9 * L_101 = &(__this->___scrollOffset_11);
		NullCheck(L_101);
		float L_102 = (L_101->___y_2);
		G_B18_0 = L_102;
		G_B18_1 = G_B17_0;
	}

IL_02fb:
	{
		NullCheck(G_B18_1);
		G_B18_1->___y_2 = G_B18_0;
		return;
	}
}
// System.Void UnityEngine.TextEditor::Copy()
extern MethodInfo TextEditor_Copy_m2273_MethodInfo;
 void TextEditor_Copy_m2273 (TextEditor_t484 * __this, MethodInfo* method){
	String_t* V_0 = {0};
	{
		int32_t L_0 = (__this->___selectPos_2);
		int32_t L_1 = (__this->___pos_1);
		if ((((uint32_t)L_0) != ((uint32_t)L_1)))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		bool L_2 = (__this->___isPasswordField_9);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_001e:
	{
		int32_t L_3 = (__this->___pos_1);
		int32_t L_4 = (__this->___selectPos_2);
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0058;
		}
	}
	{
		GUIContent_t485 * L_5 = (__this->___content_4);
		NullCheck(L_5);
		String_t* L_6 = GUIContent_get_text_m2270(L_5, /*hidden argument*/&GUIContent_get_text_m2270_MethodInfo);
		int32_t L_7 = (__this->___pos_1);
		int32_t L_8 = (__this->___selectPos_2);
		int32_t L_9 = (__this->___pos_1);
		NullCheck(L_6);
		String_t* L_10 = String_Substring_m2281(L_6, L_7, ((int32_t)(L_8-L_9)), /*hidden argument*/&String_Substring_m2281_MethodInfo);
		V_0 = L_10;
		goto IL_007c;
	}

IL_0058:
	{
		GUIContent_t485 * L_11 = (__this->___content_4);
		NullCheck(L_11);
		String_t* L_12 = GUIContent_get_text_m2270(L_11, /*hidden argument*/&GUIContent_get_text_m2270_MethodInfo);
		int32_t L_13 = (__this->___selectPos_2);
		int32_t L_14 = (__this->___pos_1);
		int32_t L_15 = (__this->___selectPos_2);
		NullCheck(L_12);
		String_t* L_16 = String_Substring_m2281(L_12, L_13, ((int32_t)(L_14-L_15)), /*hidden argument*/&String_Substring_m2281_MethodInfo);
		V_0 = L_16;
	}

IL_007c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUIUtility_t966_il2cpp_TypeInfo));
		GUIUtility_set_systemCopyBuffer_m5638(NULL /*static, unused*/, V_0, /*hidden argument*/&GUIUtility_set_systemCopyBuffer_m5638_MethodInfo);
		return;
	}
}
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
 String_t* TextEditor_ReplaceNewlinesWithSpaces_m6343 (Object_t * __this/* static, unused */, String_t* ___value, MethodInfo* method){
	{
		NullCheck(___value);
		String_t* L_0 = String_Replace_m291(___value, (String_t*) &_stringLiteral436, (String_t*) &_stringLiteral149, /*hidden argument*/&String_Replace_m291_MethodInfo);
		___value = L_0;
		NullCheck(___value);
		String_t* L_1 = String_Replace_m6517(___value, ((int32_t)10), ((int32_t)32), /*hidden argument*/&String_Replace_m6517_MethodInfo);
		___value = L_1;
		NullCheck(___value);
		String_t* L_2 = String_Replace_m6517(___value, ((int32_t)13), ((int32_t)32), /*hidden argument*/&String_Replace_m6517_MethodInfo);
		___value = L_2;
		return ___value;
	}
}
// System.Boolean UnityEngine.TextEditor::Paste()
extern MethodInfo TextEditor_Paste_m2269_MethodInfo;
 bool TextEditor_Paste_m2269 (TextEditor_t484 * __this, MethodInfo* method){
	String_t* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&GUIUtility_t966_il2cpp_TypeInfo));
		String_t* L_0 = GUIUtility_get_systemCopyBuffer_m5637(NULL /*static, unused*/, /*hidden argument*/&GUIUtility_get_systemCopyBuffer_m5637_MethodInfo);
		V_0 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_1 = String_op_Inequality_m2280(NULL /*static, unused*/, V_0, (((String_t_StaticFields*)(&String_t_il2cpp_TypeInfo)->static_fields)->___Empty_2), /*hidden argument*/&String_op_Inequality_m2280_MethodInfo);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		bool L_2 = (__this->___multiline_7);
		if (L_2)
		{
			goto IL_0028;
		}
	}
	{
		String_t* L_3 = TextEditor_ReplaceNewlinesWithSpaces_m6343(NULL /*static, unused*/, V_0, /*hidden argument*/&TextEditor_ReplaceNewlinesWithSpaces_m6343_MethodInfo);
		V_0 = L_3;
	}

IL_0028:
	{
		TextEditor_ReplaceSelection_m6341(__this, V_0, /*hidden argument*/&TextEditor_ReplaceSelection_m6341_MethodInfo);
		return 1;
	}

IL_0031:
	{
		return 0;
	}
}
// Metadata Definition UnityEngine.TextEditor
extern Il2CppType TouchScreenKeyboard_t329_0_0_6;
FieldInfo TextEditor_t484____keyboardOnScreen_0_FieldInfo = 
{
	"keyboardOnScreen"/* name */
	, &TouchScreenKeyboard_t329_0_0_6/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___keyboardOnScreen_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_6;
FieldInfo TextEditor_t484____pos_1_FieldInfo = 
{
	"pos"/* name */
	, &Int32_t93_0_0_6/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___pos_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_6;
FieldInfo TextEditor_t484____selectPos_2_FieldInfo = 
{
	"selectPos"/* name */
	, &Int32_t93_0_0_6/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___selectPos_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_6;
FieldInfo TextEditor_t484____controlID_3_FieldInfo = 
{
	"controlID"/* name */
	, &Int32_t93_0_0_6/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___controlID_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GUIContent_t485_0_0_6;
FieldInfo TextEditor_t484____content_4_FieldInfo = 
{
	"content"/* name */
	, &GUIContent_t485_0_0_6/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___content_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType GUIStyle_t953_0_0_6;
FieldInfo TextEditor_t484____style_5_FieldInfo = 
{
	"style"/* name */
	, &GUIStyle_t953_0_0_6/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___style_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Rect_t118_0_0_6;
FieldInfo TextEditor_t484____position_6_FieldInfo = 
{
	"position"/* name */
	, &Rect_t118_0_0_6/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___position_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_6;
FieldInfo TextEditor_t484____multiline_7_FieldInfo = 
{
	"multiline"/* name */
	, &Boolean_t106_0_0_6/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___multiline_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_6;
FieldInfo TextEditor_t484____hasHorizontalCursorPos_8_FieldInfo = 
{
	"hasHorizontalCursorPos"/* name */
	, &Boolean_t106_0_0_6/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___hasHorizontalCursorPos_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_6;
FieldInfo TextEditor_t484____isPasswordField_9_FieldInfo = 
{
	"isPasswordField"/* name */
	, &Boolean_t106_0_0_6/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___isPasswordField_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_3;
FieldInfo TextEditor_t484____m_HasFocus_10_FieldInfo = 
{
	"m_HasFocus"/* name */
	, &Boolean_t106_0_0_3/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___m_HasFocus_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t9_0_0_6;
FieldInfo TextEditor_t484____scrollOffset_11_FieldInfo = 
{
	"scrollOffset"/* name */
	, &Vector2_t9_0_0_6/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___scrollOffset_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo TextEditor_t484____m_TextHeightPotentiallyChanged_12_FieldInfo = 
{
	"m_TextHeightPotentiallyChanged"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___m_TextHeightPotentiallyChanged_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t9_0_0_6;
FieldInfo TextEditor_t484____graphicalCursorPos_13_FieldInfo = 
{
	"graphicalCursorPos"/* name */
	, &Vector2_t9_0_0_6/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___graphicalCursorPos_13)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t9_0_0_6;
FieldInfo TextEditor_t484____graphicalSelectCursorPos_14_FieldInfo = 
{
	"graphicalSelectCursorPos"/* name */
	, &Vector2_t9_0_0_6/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___graphicalSelectCursorPos_14)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo TextEditor_t484____m_MouseDragSelectsWholeWords_15_FieldInfo = 
{
	"m_MouseDragSelectsWholeWords"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___m_MouseDragSelectsWholeWords_15)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo TextEditor_t484____m_DblClickInitPos_16_FieldInfo = 
{
	"m_DblClickInitPos"/* name */
	, &Int32_t93_0_0_1/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___m_DblClickInitPos_16)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType DblClickSnapping_t1070_0_0_1;
FieldInfo TextEditor_t484____m_DblClickSnap_17_FieldInfo = 
{
	"m_DblClickSnap"/* name */
	, &DblClickSnapping_t1070_0_0_1/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___m_DblClickSnap_17)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo TextEditor_t484____m_bJustSelected_18_FieldInfo = 
{
	"m_bJustSelected"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___m_bJustSelected_18)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo TextEditor_t484____m_iAltCursorPos_19_FieldInfo = 
{
	"m_iAltCursorPos"/* name */
	, &Int32_t93_0_0_1/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___m_iAltCursorPos_19)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
FieldInfo TextEditor_t484____oldText_20_FieldInfo = 
{
	"oldText"/* name */
	, &String_t_0_0_1/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___oldText_20)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo TextEditor_t484____oldPos_21_FieldInfo = 
{
	"oldPos"/* name */
	, &Int32_t93_0_0_1/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___oldPos_21)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo TextEditor_t484____oldSelectPos_22_FieldInfo = 
{
	"oldSelectPos"/* name */
	, &Int32_t93_0_0_1/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484, ___oldSelectPos_22)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Dictionary_2_t1072_0_0_17;
FieldInfo TextEditor_t484____s_Keyactions_23_FieldInfo = 
{
	"s_Keyactions"/* name */
	, &Dictionary_2_t1072_0_0_17/* type */
	, &TextEditor_t484_il2cpp_TypeInfo/* parent */
	, offsetof(TextEditor_t484_StaticFields, ___s_Keyactions_23)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TextEditor_t484_FieldInfos[] =
{
	&TextEditor_t484____keyboardOnScreen_0_FieldInfo,
	&TextEditor_t484____pos_1_FieldInfo,
	&TextEditor_t484____selectPos_2_FieldInfo,
	&TextEditor_t484____controlID_3_FieldInfo,
	&TextEditor_t484____content_4_FieldInfo,
	&TextEditor_t484____style_5_FieldInfo,
	&TextEditor_t484____position_6_FieldInfo,
	&TextEditor_t484____multiline_7_FieldInfo,
	&TextEditor_t484____hasHorizontalCursorPos_8_FieldInfo,
	&TextEditor_t484____isPasswordField_9_FieldInfo,
	&TextEditor_t484____m_HasFocus_10_FieldInfo,
	&TextEditor_t484____scrollOffset_11_FieldInfo,
	&TextEditor_t484____m_TextHeightPotentiallyChanged_12_FieldInfo,
	&TextEditor_t484____graphicalCursorPos_13_FieldInfo,
	&TextEditor_t484____graphicalSelectCursorPos_14_FieldInfo,
	&TextEditor_t484____m_MouseDragSelectsWholeWords_15_FieldInfo,
	&TextEditor_t484____m_DblClickInitPos_16_FieldInfo,
	&TextEditor_t484____m_DblClickSnap_17_FieldInfo,
	&TextEditor_t484____m_bJustSelected_18_FieldInfo,
	&TextEditor_t484____m_iAltCursorPos_19_FieldInfo,
	&TextEditor_t484____oldText_20_FieldInfo,
	&TextEditor_t484____oldPos_21_FieldInfo,
	&TextEditor_t484____oldSelectPos_22_FieldInfo,
	&TextEditor_t484____s_Keyactions_23_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::.ctor()
MethodInfo TextEditor__ctor_m2268_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextEditor__ctor_m2268/* method */
	, &TextEditor_t484_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ClearCursorPos()
MethodInfo TextEditor_ClearCursorPos_m6338_MethodInfo = 
{
	"ClearCursorPos"/* name */
	, (methodPointerType)&TextEditor_ClearCursorPos_m6338/* method */
	, &TextEditor_t484_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::OnFocus()
MethodInfo TextEditor_OnFocus_m2272_MethodInfo = 
{
	"OnFocus"/* name */
	, (methodPointerType)&TextEditor_OnFocus_m2272/* method */
	, &TextEditor_t484_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::SelectAll()
MethodInfo TextEditor_SelectAll_m6339_MethodInfo = 
{
	"SelectAll"/* name */
	, (methodPointerType)&TextEditor_SelectAll_m6339/* method */
	, &TextEditor_t484_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextEditor::DeleteSelection()
MethodInfo TextEditor_DeleteSelection_m6340_MethodInfo = 
{
	"DeleteSelection"/* name */
	, (methodPointerType)&TextEditor_DeleteSelection_m6340/* method */
	, &TextEditor_t484_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextEditor_t484_TextEditor_ReplaceSelection_m6341_ParameterInfos[] = 
{
	{"replace", 0, 134219279, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::ReplaceSelection(System.String)
MethodInfo TextEditor_ReplaceSelection_m6341_MethodInfo = 
{
	"ReplaceSelection"/* name */
	, (methodPointerType)&TextEditor_ReplaceSelection_m6341/* method */
	, &TextEditor_t484_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, TextEditor_t484_TextEditor_ReplaceSelection_m6341_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::UpdateScrollOffset()
MethodInfo TextEditor_UpdateScrollOffset_m6342_MethodInfo = 
{
	"UpdateScrollOffset"/* name */
	, (methodPointerType)&TextEditor_UpdateScrollOffset_m6342/* method */
	, &TextEditor_t484_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.TextEditor::Copy()
MethodInfo TextEditor_Copy_m2273_MethodInfo = 
{
	"Copy"/* name */
	, (methodPointerType)&TextEditor_Copy_m2273/* method */
	, &TextEditor_t484_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextEditor_t484_TextEditor_ReplaceNewlinesWithSpaces_m6343_ParameterInfos[] = 
{
	{"value", 0, 134219280, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.TextEditor::ReplaceNewlinesWithSpaces(System.String)
MethodInfo TextEditor_ReplaceNewlinesWithSpaces_m6343_MethodInfo = 
{
	"ReplaceNewlinesWithSpaces"/* name */
	, (methodPointerType)&TextEditor_ReplaceNewlinesWithSpaces_m6343/* method */
	, &TextEditor_t484_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, TextEditor_t484_TextEditor_ReplaceNewlinesWithSpaces_m6343_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextEditor::Paste()
MethodInfo TextEditor_Paste_m2269_MethodInfo = 
{
	"Paste"/* name */
	, (methodPointerType)&TextEditor_Paste_m2269/* method */
	, &TextEditor_t484_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TextEditor_t484_MethodInfos[] =
{
	&TextEditor__ctor_m2268_MethodInfo,
	&TextEditor_ClearCursorPos_m6338_MethodInfo,
	&TextEditor_OnFocus_m2272_MethodInfo,
	&TextEditor_SelectAll_m6339_MethodInfo,
	&TextEditor_DeleteSelection_m6340_MethodInfo,
	&TextEditor_ReplaceSelection_m6341_MethodInfo,
	&TextEditor_UpdateScrollOffset_m6342_MethodInfo,
	&TextEditor_Copy_m2273_MethodInfo,
	&TextEditor_ReplaceNewlinesWithSpaces_m6343_MethodInfo,
	&TextEditor_Paste_m2269_MethodInfo,
	NULL
};
extern TypeInfo DblClickSnapping_t1070_il2cpp_TypeInfo;
extern TypeInfo TextEditOp_t1071_il2cpp_TypeInfo;
static TypeInfo* TextEditor_t484_il2cpp_TypeInfo__nestedTypes[3] =
{
	&DblClickSnapping_t1070_il2cpp_TypeInfo,
	&TextEditOp_t1071_il2cpp_TypeInfo,
	NULL
};
static MethodInfo* TextEditor_t484_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextEditor_t484_0_0_0;
extern Il2CppType TextEditor_t484_1_0_0;
struct TextEditor_t484;
TypeInfo TextEditor_t484_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextEditor"/* name */
	, "UnityEngine"/* namespaze */
	, TextEditor_t484_MethodInfos/* methods */
	, NULL/* properties */
	, TextEditor_t484_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, TextEditor_t484_il2cpp_TypeInfo__nestedTypes/* nested_types */
	, NULL/* nested_in */
	, &TextEditor_t484_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TextEditor_t484_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TextEditor_t484_il2cpp_TypeInfo/* cast_class */
	, &TextEditor_t484_0_0_0/* byval_arg */
	, &TextEditor_t484_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextEditor_t484)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(TextEditor_t484_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 10/* method_count */
	, 0/* property_count */
	, 24/* field_count */
	, 0/* event_count */
	, 2/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TextGenerationSettings_t365_il2cpp_TypeInfo;
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"

// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"
// UnityEngine.TextAnchor
#include "UnityEngine_UnityEngine_TextAnchor.h"
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
// UnityEngine.VerticalWrapMode
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
// UnityEngine.Font
#include "UnityEngine_UnityEngine_Font.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
extern TypeInfo Color32_t415_il2cpp_TypeInfo;
extern TypeInfo Boolean_t106_il2cpp_TypeInfo;
extern TypeInfo Mathf_t101_il2cpp_TypeInfo;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
// System.ValueType
#include "mscorlib_System_ValueTypeMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
extern MethodInfo Color32_op_Implicit_m2134_MethodInfo;
extern MethodInfo Mathf_Approximately_m2003_MethodInfo;
extern MethodInfo TextGenerationSettings_CompareColors_m6344_MethodInfo;
extern MethodInfo TextGenerationSettings_CompareVector2_m6345_MethodInfo;
extern MethodInfo Object_op_Equality_m324_MethodInfo;


// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
 bool TextGenerationSettings_CompareColors_m6344 (TextGenerationSettings_t365 * __this, Color_t19  ___left, Color_t19  ___right, MethodInfo* method){
	Color32_t415  V_0 = {0};
	Color32_t415  V_1 = {0};
	{
		Color32_t415  L_0 = Color32_op_Implicit_m2134(NULL /*static, unused*/, ___left, /*hidden argument*/&Color32_op_Implicit_m2134_MethodInfo);
		V_0 = L_0;
		Color32_t415  L_1 = Color32_op_Implicit_m2134(NULL /*static, unused*/, ___right, /*hidden argument*/&Color32_op_Implicit_m2134_MethodInfo);
		V_1 = L_1;
		Color32_t415  L_2 = V_0;
		Object_t * L_3 = Box(InitializedTypeInfo(&Color32_t415_il2cpp_TypeInfo), &L_2);
		Color32_t415  L_4 = V_1;
		Object_t * L_5 = Box(InitializedTypeInfo(&Color32_t415_il2cpp_TypeInfo), &L_4);
		NullCheck(L_3);
		bool L_6 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&ValueType_Equals_m1961_MethodInfo, L_3, L_5);
		return L_6;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
 bool TextGenerationSettings_CompareVector2_m6345 (TextGenerationSettings_t365 * __this, Vector2_t9  ___left, Vector2_t9  ___right, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		NullCheck((&___left));
		float L_0 = ((&___left)->___x_1);
		NullCheck((&___right));
		float L_1 = ((&___right)->___x_1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		bool L_2 = Mathf_Approximately_m2003(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/&Mathf_Approximately_m2003_MethodInfo);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		NullCheck((&___left));
		float L_3 = ((&___left)->___y_2);
		NullCheck((&___right));
		float L_4 = ((&___right)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		bool L_5 = Mathf_Approximately_m2003(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/&Mathf_Approximately_m2003_MethodInfo);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002e;
	}

IL_002d:
	{
		G_B3_0 = 0;
	}

IL_002e:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
extern MethodInfo TextGenerationSettings_Equals_m6346_MethodInfo;
 bool TextGenerationSettings_Equals_m6346 (TextGenerationSettings_t365 * __this, TextGenerationSettings_t365  ___other, MethodInfo* method){
	int32_t G_B19_0 = 0;
	{
		Color_t19  L_0 = (__this->___color_1);
		NullCheck((&___other));
		Color_t19  L_1 = ((&___other)->___color_1);
		bool L_2 = TextGenerationSettings_CompareColors_m6344(__this, L_0, L_1, /*hidden argument*/&TextGenerationSettings_CompareColors_m6344_MethodInfo);
		if (!L_2)
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_3 = (__this->___fontSize_2);
		NullCheck((&___other));
		int32_t L_4 = ((&___other)->___fontSize_2);
		if ((((uint32_t)L_3) != ((uint32_t)L_4)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_5 = (__this->___resizeTextMinSize_8);
		NullCheck((&___other));
		int32_t L_6 = ((&___other)->___resizeTextMinSize_8);
		if ((((uint32_t)L_5) != ((uint32_t)L_6)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_7 = (__this->___resizeTextMaxSize_9);
		NullCheck((&___other));
		int32_t L_8 = ((&___other)->___resizeTextMaxSize_9);
		if ((((uint32_t)L_7) != ((uint32_t)L_8)))
		{
			goto IL_015d;
		}
	}
	{
		float L_9 = (__this->___lineSpacing_3);
		NullCheck((&___other));
		float L_10 = ((&___other)->___lineSpacing_3);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		bool L_11 = Mathf_Approximately_m2003(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/&Mathf_Approximately_m2003_MethodInfo);
		if (!L_11)
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_12 = (__this->___fontStyle_5);
		NullCheck((&___other));
		int32_t L_13 = ((&___other)->___fontStyle_5);
		if ((((uint32_t)L_12) != ((uint32_t)L_13)))
		{
			goto IL_015d;
		}
	}
	{
		bool L_14 = (__this->___richText_4);
		NullCheck((&___other));
		bool L_15 = ((&___other)->___richText_4);
		if ((((uint32_t)L_14) != ((uint32_t)L_15)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_16 = (__this->___textAnchor_6);
		NullCheck((&___other));
		int32_t L_17 = ((&___other)->___textAnchor_6);
		if ((((uint32_t)L_16) != ((uint32_t)L_17)))
		{
			goto IL_015d;
		}
	}
	{
		bool L_18 = (__this->___resizeTextForBestFit_7);
		NullCheck((&___other));
		bool L_19 = ((&___other)->___resizeTextForBestFit_7);
		if ((((uint32_t)L_18) != ((uint32_t)L_19)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_20 = (__this->___resizeTextMinSize_8);
		NullCheck((&___other));
		int32_t L_21 = ((&___other)->___resizeTextMinSize_8);
		if ((((uint32_t)L_20) != ((uint32_t)L_21)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_22 = (__this->___resizeTextMaxSize_9);
		NullCheck((&___other));
		int32_t L_23 = ((&___other)->___resizeTextMaxSize_9);
		if ((((uint32_t)L_22) != ((uint32_t)L_23)))
		{
			goto IL_015d;
		}
	}
	{
		bool L_24 = (__this->___resizeTextForBestFit_7);
		NullCheck((&___other));
		bool L_25 = ((&___other)->___resizeTextForBestFit_7);
		if ((((uint32_t)L_24) != ((uint32_t)L_25)))
		{
			goto IL_015d;
		}
	}
	{
		bool L_26 = (__this->___updateBounds_10);
		NullCheck((&___other));
		bool L_27 = ((&___other)->___updateBounds_10);
		if ((((uint32_t)L_26) != ((uint32_t)L_27)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_28 = (__this->___horizontalOverflow_12);
		NullCheck((&___other));
		int32_t L_29 = ((&___other)->___horizontalOverflow_12);
		if ((((uint32_t)L_28) != ((uint32_t)L_29)))
		{
			goto IL_015d;
		}
	}
	{
		int32_t L_30 = (__this->___verticalOverflow_11);
		NullCheck((&___other));
		int32_t L_31 = ((&___other)->___verticalOverflow_11);
		if ((((uint32_t)L_30) != ((uint32_t)L_31)))
		{
			goto IL_015d;
		}
	}
	{
		Vector2_t9  L_32 = (__this->___generationExtents_13);
		NullCheck((&___other));
		Vector2_t9  L_33 = ((&___other)->___generationExtents_13);
		bool L_34 = TextGenerationSettings_CompareVector2_m6345(__this, L_32, L_33, /*hidden argument*/&TextGenerationSettings_CompareVector2_m6345_MethodInfo);
		if (!L_34)
		{
			goto IL_015d;
		}
	}
	{
		Vector2_t9  L_35 = (__this->___pivot_14);
		NullCheck((&___other));
		Vector2_t9  L_36 = ((&___other)->___pivot_14);
		bool L_37 = TextGenerationSettings_CompareVector2_m6345(__this, L_35, L_36, /*hidden argument*/&TextGenerationSettings_CompareVector2_m6345_MethodInfo);
		if (!L_37)
		{
			goto IL_015d;
		}
	}
	{
		Font_t280 * L_38 = (__this->___font_0);
		NullCheck((&___other));
		Font_t280 * L_39 = ((&___other)->___font_0);
		bool L_40 = Object_op_Equality_m324(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/&Object_op_Equality_m324_MethodInfo);
		G_B19_0 = ((int32_t)(L_40));
		goto IL_015e;
	}

IL_015d:
	{
		G_B19_0 = 0;
	}

IL_015e:
	{
		return G_B19_0;
	}
}
// Metadata Definition UnityEngine.TextGenerationSettings
extern Il2CppType Font_t280_0_0_6;
FieldInfo TextGenerationSettings_t365____font_0_FieldInfo = 
{
	"font"/* name */
	, &Font_t280_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___font_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Color_t19_0_0_6;
FieldInfo TextGenerationSettings_t365____color_1_FieldInfo = 
{
	"color"/* name */
	, &Color_t19_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___color_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_6;
FieldInfo TextGenerationSettings_t365____fontSize_2_FieldInfo = 
{
	"fontSize"/* name */
	, &Int32_t93_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___fontSize_2) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_6;
FieldInfo TextGenerationSettings_t365____lineSpacing_3_FieldInfo = 
{
	"lineSpacing"/* name */
	, &Single_t105_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___lineSpacing_3) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_6;
FieldInfo TextGenerationSettings_t365____richText_4_FieldInfo = 
{
	"richText"/* name */
	, &Boolean_t106_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___richText_4) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType FontStyle_t458_0_0_6;
FieldInfo TextGenerationSettings_t365____fontStyle_5_FieldInfo = 
{
	"fontStyle"/* name */
	, &FontStyle_t458_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___fontStyle_5) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TextAnchor_t459_0_0_6;
FieldInfo TextGenerationSettings_t365____textAnchor_6_FieldInfo = 
{
	"textAnchor"/* name */
	, &TextAnchor_t459_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___textAnchor_6) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_6;
FieldInfo TextGenerationSettings_t365____resizeTextForBestFit_7_FieldInfo = 
{
	"resizeTextForBestFit"/* name */
	, &Boolean_t106_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___resizeTextForBestFit_7) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_6;
FieldInfo TextGenerationSettings_t365____resizeTextMinSize_8_FieldInfo = 
{
	"resizeTextMinSize"/* name */
	, &Int32_t93_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___resizeTextMinSize_8) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_6;
FieldInfo TextGenerationSettings_t365____resizeTextMaxSize_9_FieldInfo = 
{
	"resizeTextMaxSize"/* name */
	, &Int32_t93_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___resizeTextMaxSize_9) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_6;
FieldInfo TextGenerationSettings_t365____updateBounds_10_FieldInfo = 
{
	"updateBounds"/* name */
	, &Boolean_t106_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___updateBounds_10) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType VerticalWrapMode_t461_0_0_6;
FieldInfo TextGenerationSettings_t365____verticalOverflow_11_FieldInfo = 
{
	"verticalOverflow"/* name */
	, &VerticalWrapMode_t461_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___verticalOverflow_11) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType HorizontalWrapMode_t460_0_0_6;
FieldInfo TextGenerationSettings_t365____horizontalOverflow_12_FieldInfo = 
{
	"horizontalOverflow"/* name */
	, &HorizontalWrapMode_t460_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___horizontalOverflow_12) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t9_0_0_6;
FieldInfo TextGenerationSettings_t365____generationExtents_13_FieldInfo = 
{
	"generationExtents"/* name */
	, &Vector2_t9_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___generationExtents_13) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t9_0_0_6;
FieldInfo TextGenerationSettings_t365____pivot_14_FieldInfo = 
{
	"pivot"/* name */
	, &Vector2_t9_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___pivot_14) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_6;
FieldInfo TextGenerationSettings_t365____generateOutOfBounds_15_FieldInfo = 
{
	"generateOutOfBounds"/* name */
	, &Boolean_t106_0_0_6/* type */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* parent */
	, offsetof(TextGenerationSettings_t365, ___generateOutOfBounds_15) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TextGenerationSettings_t365_FieldInfos[] =
{
	&TextGenerationSettings_t365____font_0_FieldInfo,
	&TextGenerationSettings_t365____color_1_FieldInfo,
	&TextGenerationSettings_t365____fontSize_2_FieldInfo,
	&TextGenerationSettings_t365____lineSpacing_3_FieldInfo,
	&TextGenerationSettings_t365____richText_4_FieldInfo,
	&TextGenerationSettings_t365____fontStyle_5_FieldInfo,
	&TextGenerationSettings_t365____textAnchor_6_FieldInfo,
	&TextGenerationSettings_t365____resizeTextForBestFit_7_FieldInfo,
	&TextGenerationSettings_t365____resizeTextMinSize_8_FieldInfo,
	&TextGenerationSettings_t365____resizeTextMaxSize_9_FieldInfo,
	&TextGenerationSettings_t365____updateBounds_10_FieldInfo,
	&TextGenerationSettings_t365____verticalOverflow_11_FieldInfo,
	&TextGenerationSettings_t365____horizontalOverflow_12_FieldInfo,
	&TextGenerationSettings_t365____generationExtents_13_FieldInfo,
	&TextGenerationSettings_t365____pivot_14_FieldInfo,
	&TextGenerationSettings_t365____generateOutOfBounds_15_FieldInfo,
	NULL
};
extern Il2CppType Color_t19_0_0_0;
extern Il2CppType Color_t19_0_0_0;
extern Il2CppType Color_t19_0_0_0;
static ParameterInfo TextGenerationSettings_t365_TextGenerationSettings_CompareColors_m6344_ParameterInfos[] = 
{
	{"left", 0, 134219281, &EmptyCustomAttributesCache, &Color_t19_0_0_0},
	{"right", 1, 134219282, &EmptyCustomAttributesCache, &Color_t19_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Color_t19_Color_t19 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
MethodInfo TextGenerationSettings_CompareColors_m6344_MethodInfo = 
{
	"CompareColors"/* name */
	, (methodPointerType)&TextGenerationSettings_CompareColors_m6344/* method */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Color_t19_Color_t19/* invoker_method */
	, TextGenerationSettings_t365_TextGenerationSettings_CompareColors_m6344_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t9_0_0_0;
extern Il2CppType Vector2_t9_0_0_0;
extern Il2CppType Vector2_t9_0_0_0;
static ParameterInfo TextGenerationSettings_t365_TextGenerationSettings_CompareVector2_m6345_ParameterInfos[] = 
{
	{"left", 0, 134219283, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
	{"right", 1, 134219284, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Vector2_t9_Vector2_t9 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
MethodInfo TextGenerationSettings_CompareVector2_m6345_MethodInfo = 
{
	"CompareVector2"/* name */
	, (methodPointerType)&TextGenerationSettings_CompareVector2_m6345/* method */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Vector2_t9_Vector2_t9/* invoker_method */
	, TextGenerationSettings_t365_TextGenerationSettings_CompareVector2_m6345_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TextGenerationSettings_t365_0_0_0;
extern Il2CppType TextGenerationSettings_t365_0_0_0;
static ParameterInfo TextGenerationSettings_t365_TextGenerationSettings_Equals_m6346_ParameterInfos[] = 
{
	{"other", 0, 134219285, &EmptyCustomAttributesCache, &TextGenerationSettings_t365_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_TextGenerationSettings_t365 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
MethodInfo TextGenerationSettings_Equals_m6346_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TextGenerationSettings_Equals_m6346/* method */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_TextGenerationSettings_t365/* invoker_method */
	, TextGenerationSettings_t365_TextGenerationSettings_Equals_m6346_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TextGenerationSettings_t365_MethodInfos[] =
{
	&TextGenerationSettings_CompareColors_m6344_MethodInfo,
	&TextGenerationSettings_CompareVector2_m6345_MethodInfo,
	&TextGenerationSettings_Equals_m6346_MethodInfo,
	NULL
};
static MethodInfo* TextGenerationSettings_t365_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TextGenerationSettings_t365_1_0_0;
TypeInfo TextGenerationSettings_t365_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextGenerationSettings"/* name */
	, "UnityEngine"/* namespaze */
	, TextGenerationSettings_t365_MethodInfos/* methods */
	, NULL/* properties */
	, TextGenerationSettings_t365_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TextGenerationSettings_t365_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TextGenerationSettings_t365_il2cpp_TypeInfo/* cast_class */
	, &TextGenerationSettings_t365_0_0_0/* byval_arg */
	, &TextGenerationSettings_t365_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextGenerationSettings_t365)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048841/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 16/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReference.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TrackedReference_t1020_il2cpp_TypeInfo;
// UnityEngine.TrackedReference
#include "UnityEngine_UnityEngine_TrackedReferenceMethodDeclarations.h"

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
extern TypeInfo IntPtr_t121_il2cpp_TypeInfo;
// System.IntPtr
#include "mscorlib_System_IntPtrMethodDeclarations.h"
extern MethodInfo TrackedReference_op_Equality_m6349_MethodInfo;
extern MethodInfo IntPtr_op_Explicit_m6532_MethodInfo;
extern MethodInfo IntPtr_op_Equality_m4612_MethodInfo;


// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern MethodInfo TrackedReference_Equals_m6347_MethodInfo;
 bool TrackedReference_Equals_m6347 (TrackedReference_t1020 * __this, Object_t * ___o, MethodInfo* method){
	{
		bool L_0 = TrackedReference_op_Equality_m6349(NULL /*static, unused*/, ((TrackedReference_t1020 *)IsInst(___o, InitializedTypeInfo(&TrackedReference_t1020_il2cpp_TypeInfo))), __this, /*hidden argument*/&TrackedReference_op_Equality_m6349_MethodInfo);
		return L_0;
	}
}
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern MethodInfo TrackedReference_GetHashCode_m6348_MethodInfo;
 int32_t TrackedReference_GetHashCode_m6348 (TrackedReference_t1020 * __this, MethodInfo* method){
	{
		IntPtr_t121 L_0 = (__this->___m_Ptr_0);
		int32_t L_1 = IntPtr_op_Explicit_m6532(NULL /*static, unused*/, L_0, /*hidden argument*/&IntPtr_op_Explicit_m6532_MethodInfo);
		return L_1;
	}
}
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
 bool TrackedReference_op_Equality_m6349 (Object_t * __this/* static, unused */, TrackedReference_t1020 * ___x, TrackedReference_t1020 * ___y, MethodInfo* method){
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	{
		V_0 = ___x;
		V_1 = ___y;
		if (V_1)
		{
			goto IL_0012;
		}
	}
	{
		if (V_0)
		{
			goto IL_0012;
		}
	}
	{
		return 1;
	}

IL_0012:
	{
		if (V_1)
		{
			goto IL_0029;
		}
	}
	{
		NullCheck(___x);
		IntPtr_t121 L_0 = (___x->___m_Ptr_0);
		bool L_1 = IntPtr_op_Equality_m4612(NULL /*static, unused*/, L_0, (((IntPtr_t121_StaticFields*)InitializedTypeInfo(&IntPtr_t121_il2cpp_TypeInfo)->static_fields)->___Zero_1), /*hidden argument*/&IntPtr_op_Equality_m4612_MethodInfo);
		return L_1;
	}

IL_0029:
	{
		if (V_0)
		{
			goto IL_0040;
		}
	}
	{
		NullCheck(___y);
		IntPtr_t121 L_2 = (___y->___m_Ptr_0);
		bool L_3 = IntPtr_op_Equality_m4612(NULL /*static, unused*/, L_2, (((IntPtr_t121_StaticFields*)InitializedTypeInfo(&IntPtr_t121_il2cpp_TypeInfo)->static_fields)->___Zero_1), /*hidden argument*/&IntPtr_op_Equality_m4612_MethodInfo);
		return L_3;
	}

IL_0040:
	{
		NullCheck(___x);
		IntPtr_t121 L_4 = (___x->___m_Ptr_0);
		NullCheck(___y);
		IntPtr_t121 L_5 = (___y->___m_Ptr_0);
		bool L_6 = IntPtr_op_Equality_m4612(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/&IntPtr_op_Equality_m4612_MethodInfo);
		return L_6;
	}
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
void TrackedReference_t1020_marshal(const TrackedReference_t1020& unmarshaled, TrackedReference_t1020_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = unmarshaled.___m_Ptr_0;
}
void TrackedReference_t1020_marshal_back(const TrackedReference_t1020_marshaled& marshaled, TrackedReference_t1020& unmarshaled)
{
	unmarshaled.___m_Ptr_0 = marshaled.___m_Ptr_0;
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
void TrackedReference_t1020_marshal_cleanup(TrackedReference_t1020_marshaled& marshaled)
{
}
// Metadata Definition UnityEngine.TrackedReference
extern Il2CppType IntPtr_t121_0_0_3;
FieldInfo TrackedReference_t1020____m_Ptr_0_FieldInfo = 
{
	"m_Ptr"/* name */
	, &IntPtr_t121_0_0_3/* type */
	, &TrackedReference_t1020_il2cpp_TypeInfo/* parent */
	, offsetof(TrackedReference_t1020, ___m_Ptr_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TrackedReference_t1020_FieldInfos[] =
{
	&TrackedReference_t1020____m_Ptr_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo TrackedReference_t1020_TrackedReference_Equals_m6347_ParameterInfos[] = 
{
	{"o", 0, 134219286, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
MethodInfo TrackedReference_Equals_m6347_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&TrackedReference_Equals_m6347/* method */
	, &TrackedReference_t1020_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, TrackedReference_t1020_TrackedReference_Equals_m6347_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
MethodInfo TrackedReference_GetHashCode_m6348_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&TrackedReference_GetHashCode_m6348/* method */
	, &TrackedReference_t1020_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType TrackedReference_t1020_0_0_0;
extern Il2CppType TrackedReference_t1020_0_0_0;
extern Il2CppType TrackedReference_t1020_0_0_0;
static ParameterInfo TrackedReference_t1020_TrackedReference_op_Equality_m6349_ParameterInfos[] = 
{
	{"x", 0, 134219287, &EmptyCustomAttributesCache, &TrackedReference_t1020_0_0_0},
	{"y", 1, 134219288, &EmptyCustomAttributesCache, &TrackedReference_t1020_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
MethodInfo TrackedReference_op_Equality_m6349_MethodInfo = 
{
	"op_Equality"/* name */
	, (methodPointerType)&TrackedReference_op_Equality_m6349/* method */
	, &TrackedReference_t1020_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, TrackedReference_t1020_TrackedReference_op_Equality_m6349_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TrackedReference_t1020_MethodInfos[] =
{
	&TrackedReference_Equals_m6347_MethodInfo,
	&TrackedReference_GetHashCode_m6348_MethodInfo,
	&TrackedReference_op_Equality_m6349_MethodInfo,
	NULL
};
static MethodInfo* TrackedReference_t1020_VTable[] =
{
	&TrackedReference_Equals_m6347_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&TrackedReference_GetHashCode_m6348_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TrackedReference_t1020_1_0_0;
struct TrackedReference_t1020;
TypeInfo TrackedReference_t1020_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TrackedReference"/* name */
	, "UnityEngine"/* namespaze */
	, TrackedReference_t1020_MethodInfos/* methods */
	, NULL/* properties */
	, TrackedReference_t1020_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TrackedReference_t1020_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TrackedReference_t1020_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TrackedReference_t1020_il2cpp_TypeInfo/* cast_class */
	, &TrackedReference_t1020_0_0_0/* byval_arg */
	, &TrackedReference_t1020_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)TrackedReference_t1020_marshal/* marshal_to_native_func */
	, (methodPointerType)TrackedReference_t1020_marshal_back/* marshal_from_native_func */
	, (methodPointerType)TrackedReference_t1020_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (TrackedReference_t1020)/* instance_size */
	, 0/* element_size */
	, sizeof(TrackedReference_t1020_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048585/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo PersistentListenerMode_t1073_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerModeMethodDeclarations.h"



// Metadata Definition UnityEngine.Events.PersistentListenerMode
extern Il2CppType Int32_t93_0_0_1542;
FieldInfo PersistentListenerMode_t1073____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t93_0_0_1542/* type */
	, &PersistentListenerMode_t1073_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentListenerMode_t1073, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1073_0_0_32854;
FieldInfo PersistentListenerMode_t1073____EventDefined_2_FieldInfo = 
{
	"EventDefined"/* name */
	, &PersistentListenerMode_t1073_0_0_32854/* type */
	, &PersistentListenerMode_t1073_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1073_0_0_32854;
FieldInfo PersistentListenerMode_t1073____Void_3_FieldInfo = 
{
	"Void"/* name */
	, &PersistentListenerMode_t1073_0_0_32854/* type */
	, &PersistentListenerMode_t1073_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1073_0_0_32854;
FieldInfo PersistentListenerMode_t1073____Object_4_FieldInfo = 
{
	"Object"/* name */
	, &PersistentListenerMode_t1073_0_0_32854/* type */
	, &PersistentListenerMode_t1073_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1073_0_0_32854;
FieldInfo PersistentListenerMode_t1073____Int_5_FieldInfo = 
{
	"Int"/* name */
	, &PersistentListenerMode_t1073_0_0_32854/* type */
	, &PersistentListenerMode_t1073_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1073_0_0_32854;
FieldInfo PersistentListenerMode_t1073____Float_6_FieldInfo = 
{
	"Float"/* name */
	, &PersistentListenerMode_t1073_0_0_32854/* type */
	, &PersistentListenerMode_t1073_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1073_0_0_32854;
FieldInfo PersistentListenerMode_t1073____String_7_FieldInfo = 
{
	"String"/* name */
	, &PersistentListenerMode_t1073_0_0_32854/* type */
	, &PersistentListenerMode_t1073_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1073_0_0_32854;
FieldInfo PersistentListenerMode_t1073____Bool_8_FieldInfo = 
{
	"Bool"/* name */
	, &PersistentListenerMode_t1073_0_0_32854/* type */
	, &PersistentListenerMode_t1073_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* PersistentListenerMode_t1073_FieldInfos[] =
{
	&PersistentListenerMode_t1073____value___1_FieldInfo,
	&PersistentListenerMode_t1073____EventDefined_2_FieldInfo,
	&PersistentListenerMode_t1073____Void_3_FieldInfo,
	&PersistentListenerMode_t1073____Object_4_FieldInfo,
	&PersistentListenerMode_t1073____Int_5_FieldInfo,
	&PersistentListenerMode_t1073____Float_6_FieldInfo,
	&PersistentListenerMode_t1073____String_7_FieldInfo,
	&PersistentListenerMode_t1073____Bool_8_FieldInfo,
	NULL
};
static const int32_t PersistentListenerMode_t1073____EventDefined_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1073____EventDefined_2_DefaultValue = 
{
	&PersistentListenerMode_t1073____EventDefined_2_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1073____EventDefined_2_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1073____Void_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1073____Void_3_DefaultValue = 
{
	&PersistentListenerMode_t1073____Void_3_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1073____Void_3_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1073____Object_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1073____Object_4_DefaultValue = 
{
	&PersistentListenerMode_t1073____Object_4_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1073____Object_4_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1073____Int_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1073____Int_5_DefaultValue = 
{
	&PersistentListenerMode_t1073____Int_5_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1073____Int_5_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1073____Float_6_DefaultValueData = 4;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1073____Float_6_DefaultValue = 
{
	&PersistentListenerMode_t1073____Float_6_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1073____Float_6_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1073____String_7_DefaultValueData = 5;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1073____String_7_DefaultValue = 
{
	&PersistentListenerMode_t1073____String_7_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1073____String_7_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t PersistentListenerMode_t1073____Bool_8_DefaultValueData = 6;
static Il2CppFieldDefaultValueEntry PersistentListenerMode_t1073____Bool_8_DefaultValue = 
{
	&PersistentListenerMode_t1073____Bool_8_FieldInfo/* field */
	, { (char*)&PersistentListenerMode_t1073____Bool_8_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* PersistentListenerMode_t1073_FieldDefaultValues[] = 
{
	&PersistentListenerMode_t1073____EventDefined_2_DefaultValue,
	&PersistentListenerMode_t1073____Void_3_DefaultValue,
	&PersistentListenerMode_t1073____Object_4_DefaultValue,
	&PersistentListenerMode_t1073____Int_5_DefaultValue,
	&PersistentListenerMode_t1073____Float_6_DefaultValue,
	&PersistentListenerMode_t1073____String_7_DefaultValue,
	&PersistentListenerMode_t1073____Bool_8_DefaultValue,
	NULL
};
static MethodInfo* PersistentListenerMode_t1073_MethodInfos[] =
{
	NULL
};
static MethodInfo* PersistentListenerMode_t1073_VTable[] =
{
	&Enum_Equals_m191_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Enum_GetHashCode_m193_MethodInfo,
	&Enum_ToString_m194_MethodInfo,
	&Enum_ToString_m195_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m196_MethodInfo,
	&Enum_System_IConvertible_ToByte_m197_MethodInfo,
	&Enum_System_IConvertible_ToChar_m198_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m199_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m200_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m201_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m202_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m203_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m204_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m205_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m206_MethodInfo,
	&Enum_ToString_m207_MethodInfo,
	&Enum_System_IConvertible_ToType_m208_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m209_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m210_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m211_MethodInfo,
	&Enum_CompareTo_m212_MethodInfo,
	&Enum_GetTypeCode_m213_MethodInfo,
};
static Il2CppInterfaceOffsetPair PersistentListenerMode_t1073_InterfacesOffsets[] = 
{
	{ &IFormattable_t94_il2cpp_TypeInfo, 4},
	{ &IConvertible_t95_il2cpp_TypeInfo, 5},
	{ &IComparable_t96_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType PersistentListenerMode_t1073_0_0_0;
extern Il2CppType PersistentListenerMode_t1073_1_0_0;
TypeInfo PersistentListenerMode_t1073_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentListenerMode"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentListenerMode_t1073_MethodInfos/* methods */
	, NULL/* properties */
	, PersistentListenerMode_t1073_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t97_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t93_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, PersistentListenerMode_t1073_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &PersistentListenerMode_t1073_0_0_0/* byval_arg */
	, &PersistentListenerMode_t1073_1_0_0/* this_arg */
	, PersistentListenerMode_t1073_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, PersistentListenerMode_t1073_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentListenerMode_t1073)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 8449/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCache.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ArgumentCache_t1074_il2cpp_TypeInfo;
// UnityEngine.Events.ArgumentCache
#include "UnityEngine_UnityEngine_Events_ArgumentCacheMethodDeclarations.h"



// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern MethodInfo ArgumentCache__ctor_m6350_MethodInfo;
 void ArgumentCache__ctor_m6350 (ArgumentCache_t1074 * __this, MethodInfo* method){
	{
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern MethodInfo ArgumentCache_get_unityObjectArgument_m6351_MethodInfo;
 Object_t117 * ArgumentCache_get_unityObjectArgument_m6351 (ArgumentCache_t1074 * __this, MethodInfo* method){
	{
		Object_t117 * L_0 = (__this->___m_ObjectArgument_0);
		return L_0;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern MethodInfo ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352_MethodInfo;
 String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352 (ArgumentCache_t1074 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___m_ObjectArgumentAssemblyTypeName_1);
		return L_0;
	}
}
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern MethodInfo ArgumentCache_get_intArgument_m6353_MethodInfo;
 int32_t ArgumentCache_get_intArgument_m6353 (ArgumentCache_t1074 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___m_IntArgument_2);
		return L_0;
	}
}
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern MethodInfo ArgumentCache_get_floatArgument_m6354_MethodInfo;
 float ArgumentCache_get_floatArgument_m6354 (ArgumentCache_t1074 * __this, MethodInfo* method){
	{
		float L_0 = (__this->___m_FloatArgument_3);
		return L_0;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern MethodInfo ArgumentCache_get_stringArgument_m6355_MethodInfo;
 String_t* ArgumentCache_get_stringArgument_m6355 (ArgumentCache_t1074 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___m_StringArgument_4);
		return L_0;
	}
}
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern MethodInfo ArgumentCache_get_boolArgument_m6356_MethodInfo;
 bool ArgumentCache_get_boolArgument_m6356 (ArgumentCache_t1074 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___m_BoolArgument_5);
		return L_0;
	}
}
// Metadata Definition UnityEngine.Events.ArgumentCache
extern Il2CppType Object_t117_0_0_1;
extern CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_ObjectArgument;
FieldInfo ArgumentCache_t1074____m_ObjectArgument_0_FieldInfo = 
{
	"m_ObjectArgument"/* name */
	, &Object_t117_0_0_1/* type */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1074, ___m_ObjectArgument_0)/* data */
	, &ArgumentCache_t1074__CustomAttributeCache_m_ObjectArgument/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_ObjectArgumentAssemblyTypeName;
FieldInfo ArgumentCache_t1074____m_ObjectArgumentAssemblyTypeName_1_FieldInfo = 
{
	"m_ObjectArgumentAssemblyTypeName"/* name */
	, &String_t_0_0_1/* type */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1074, ___m_ObjectArgumentAssemblyTypeName_1)/* data */
	, &ArgumentCache_t1074__CustomAttributeCache_m_ObjectArgumentAssemblyTypeName/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
extern CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_IntArgument;
FieldInfo ArgumentCache_t1074____m_IntArgument_2_FieldInfo = 
{
	"m_IntArgument"/* name */
	, &Int32_t93_0_0_1/* type */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1074, ___m_IntArgument_2)/* data */
	, &ArgumentCache_t1074__CustomAttributeCache_m_IntArgument/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_1;
extern CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_FloatArgument;
FieldInfo ArgumentCache_t1074____m_FloatArgument_3_FieldInfo = 
{
	"m_FloatArgument"/* name */
	, &Single_t105_0_0_1/* type */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1074, ___m_FloatArgument_3)/* data */
	, &ArgumentCache_t1074__CustomAttributeCache_m_FloatArgument/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_StringArgument;
FieldInfo ArgumentCache_t1074____m_StringArgument_4_FieldInfo = 
{
	"m_StringArgument"/* name */
	, &String_t_0_0_1/* type */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1074, ___m_StringArgument_4)/* data */
	, &ArgumentCache_t1074__CustomAttributeCache_m_StringArgument/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
extern CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_BoolArgument;
FieldInfo ArgumentCache_t1074____m_BoolArgument_5_FieldInfo = 
{
	"m_BoolArgument"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* parent */
	, offsetof(ArgumentCache_t1074, ___m_BoolArgument_5)/* data */
	, &ArgumentCache_t1074__CustomAttributeCache_m_BoolArgument/* custom_attributes_cache */

};
static FieldInfo* ArgumentCache_t1074_FieldInfos[] =
{
	&ArgumentCache_t1074____m_ObjectArgument_0_FieldInfo,
	&ArgumentCache_t1074____m_ObjectArgumentAssemblyTypeName_1_FieldInfo,
	&ArgumentCache_t1074____m_IntArgument_2_FieldInfo,
	&ArgumentCache_t1074____m_FloatArgument_3_FieldInfo,
	&ArgumentCache_t1074____m_StringArgument_4_FieldInfo,
	&ArgumentCache_t1074____m_BoolArgument_5_FieldInfo,
	NULL
};
static PropertyInfo ArgumentCache_t1074____unityObjectArgument_PropertyInfo = 
{
	&ArgumentCache_t1074_il2cpp_TypeInfo/* parent */
	, "unityObjectArgument"/* name */
	, &ArgumentCache_get_unityObjectArgument_m6351_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo ArgumentCache_t1074____unityObjectArgumentAssemblyTypeName_PropertyInfo = 
{
	&ArgumentCache_t1074_il2cpp_TypeInfo/* parent */
	, "unityObjectArgumentAssemblyTypeName"/* name */
	, &ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo ArgumentCache_t1074____intArgument_PropertyInfo = 
{
	&ArgumentCache_t1074_il2cpp_TypeInfo/* parent */
	, "intArgument"/* name */
	, &ArgumentCache_get_intArgument_m6353_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo ArgumentCache_t1074____floatArgument_PropertyInfo = 
{
	&ArgumentCache_t1074_il2cpp_TypeInfo/* parent */
	, "floatArgument"/* name */
	, &ArgumentCache_get_floatArgument_m6354_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo ArgumentCache_t1074____stringArgument_PropertyInfo = 
{
	&ArgumentCache_t1074_il2cpp_TypeInfo/* parent */
	, "stringArgument"/* name */
	, &ArgumentCache_get_stringArgument_m6355_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo ArgumentCache_t1074____boolArgument_PropertyInfo = 
{
	&ArgumentCache_t1074_il2cpp_TypeInfo/* parent */
	, "boolArgument"/* name */
	, &ArgumentCache_get_boolArgument_m6356_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ArgumentCache_t1074_PropertyInfos[] =
{
	&ArgumentCache_t1074____unityObjectArgument_PropertyInfo,
	&ArgumentCache_t1074____unityObjectArgumentAssemblyTypeName_PropertyInfo,
	&ArgumentCache_t1074____intArgument_PropertyInfo,
	&ArgumentCache_t1074____floatArgument_PropertyInfo,
	&ArgumentCache_t1074____stringArgument_PropertyInfo,
	&ArgumentCache_t1074____boolArgument_PropertyInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
MethodInfo ArgumentCache__ctor_m6350_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ArgumentCache__ctor_m6350/* method */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t117_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
MethodInfo ArgumentCache_get_unityObjectArgument_m6351_MethodInfo = 
{
	"get_unityObjectArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_unityObjectArgument_m6351/* method */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* declaring_type */
	, &Object_t117_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
MethodInfo ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352_MethodInfo = 
{
	"get_unityObjectArgumentAssemblyTypeName"/* name */
	, (methodPointerType)&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352/* method */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
MethodInfo ArgumentCache_get_intArgument_m6353_MethodInfo = 
{
	"get_intArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_intArgument_m6353/* method */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t105_0_0_0;
extern void* RuntimeInvoker_Single_t105 (MethodInfo* method, void* obj, void** args);
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
MethodInfo ArgumentCache_get_floatArgument_m6354_MethodInfo = 
{
	"get_floatArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_floatArgument_m6354/* method */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* declaring_type */
	, &Single_t105_0_0_0/* return_type */
	, RuntimeInvoker_Single_t105/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
MethodInfo ArgumentCache_get_stringArgument_m6355_MethodInfo = 
{
	"get_stringArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_stringArgument_m6355/* method */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
MethodInfo ArgumentCache_get_boolArgument_m6356_MethodInfo = 
{
	"get_boolArgument"/* name */
	, (methodPointerType)&ArgumentCache_get_boolArgument_m6356/* method */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ArgumentCache_t1074_MethodInfos[] =
{
	&ArgumentCache__ctor_m6350_MethodInfo,
	&ArgumentCache_get_unityObjectArgument_m6351_MethodInfo,
	&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352_MethodInfo,
	&ArgumentCache_get_intArgument_m6353_MethodInfo,
	&ArgumentCache_get_floatArgument_m6354_MethodInfo,
	&ArgumentCache_get_stringArgument_m6355_MethodInfo,
	&ArgumentCache_get_boolArgument_m6356_MethodInfo,
	NULL
};
static MethodInfo* ArgumentCache_t1074_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
extern TypeInfo SerializeField_t103_il2cpp_TypeInfo;
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern MethodInfo SerializeField__ctor_m259_MethodInfo;
extern TypeInfo FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
extern MethodInfo FormerlySerializedAsAttribute__ctor_m1900_MethodInfo;
void ArgumentCache_t1074_CustomAttributesCacheGenerator_m_ObjectArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t420 * tmp;
		tmp = (FormerlySerializedAsAttribute_t420 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m1900(tmp, il2cpp_codegen_string_new_wrapper("objectArgument"), &FormerlySerializedAsAttribute__ctor_m1900_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void ArgumentCache_t1074_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t420 * tmp;
		tmp = (FormerlySerializedAsAttribute_t420 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m1900(tmp, il2cpp_codegen_string_new_wrapper("objectArgumentAssemblyTypeName"), &FormerlySerializedAsAttribute__ctor_m1900_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void ArgumentCache_t1074_CustomAttributesCacheGenerator_m_IntArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t420 * tmp;
		tmp = (FormerlySerializedAsAttribute_t420 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m1900(tmp, il2cpp_codegen_string_new_wrapper("intArgument"), &FormerlySerializedAsAttribute__ctor_m1900_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void ArgumentCache_t1074_CustomAttributesCacheGenerator_m_FloatArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t420 * tmp;
		tmp = (FormerlySerializedAsAttribute_t420 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m1900(tmp, il2cpp_codegen_string_new_wrapper("floatArgument"), &FormerlySerializedAsAttribute__ctor_m1900_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void ArgumentCache_t1074_CustomAttributesCacheGenerator_m_StringArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t420 * tmp;
		tmp = (FormerlySerializedAsAttribute_t420 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m1900(tmp, il2cpp_codegen_string_new_wrapper("stringArgument"), &FormerlySerializedAsAttribute__ctor_m1900_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void ArgumentCache_t1074_CustomAttributesCacheGenerator_m_BoolArgument(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_ObjectArgument = {
2,
NULL,
&ArgumentCache_t1074_CustomAttributesCacheGenerator_m_ObjectArgument
};
CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_ObjectArgumentAssemblyTypeName = {
2,
NULL,
&ArgumentCache_t1074_CustomAttributesCacheGenerator_m_ObjectArgumentAssemblyTypeName
};
CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_IntArgument = {
2,
NULL,
&ArgumentCache_t1074_CustomAttributesCacheGenerator_m_IntArgument
};
CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_FloatArgument = {
2,
NULL,
&ArgumentCache_t1074_CustomAttributesCacheGenerator_m_FloatArgument
};
CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_StringArgument = {
2,
NULL,
&ArgumentCache_t1074_CustomAttributesCacheGenerator_m_StringArgument
};
CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_BoolArgument = {
1,
NULL,
&ArgumentCache_t1074_CustomAttributesCacheGenerator_m_BoolArgument
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ArgumentCache_t1074_0_0_0;
extern Il2CppType ArgumentCache_t1074_1_0_0;
struct ArgumentCache_t1074;
extern CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_ObjectArgument;
extern CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_ObjectArgumentAssemblyTypeName;
extern CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_IntArgument;
extern CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_FloatArgument;
extern CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_StringArgument;
extern CustomAttributesCache ArgumentCache_t1074__CustomAttributeCache_m_BoolArgument;
TypeInfo ArgumentCache_t1074_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ArgumentCache"/* name */
	, "UnityEngine.Events"/* namespaze */
	, ArgumentCache_t1074_MethodInfos/* methods */
	, ArgumentCache_t1074_PropertyInfos/* properties */
	, ArgumentCache_t1074_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ArgumentCache_t1074_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ArgumentCache_t1074_il2cpp_TypeInfo/* cast_class */
	, &ArgumentCache_t1074_0_0_0/* byval_arg */
	, &ArgumentCache_t1074_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ArgumentCache_t1074)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 6/* property_count */
	, 6/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo BaseInvokableCall_t1075_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"

// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
extern TypeInfo ArgumentNullException_t1171_il2cpp_TypeInfo;
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
extern MethodInfo ArgumentNullException__ctor_m6533_MethodInfo;
extern MethodInfo Delegate_get_Method_m6534_MethodInfo;
extern MethodInfo MethodBase_get_IsStatic_m6535_MethodInfo;
extern MethodInfo Delegate_get_Target_m6536_MethodInfo;


// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern MethodInfo BaseInvokableCall__ctor_m6357_MethodInfo;
 void BaseInvokableCall__ctor_m6357 (BaseInvokableCall_t1075 * __this, MethodInfo* method){
	{
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern MethodInfo BaseInvokableCall__ctor_m6358_MethodInfo;
 void BaseInvokableCall__ctor_m6358 (BaseInvokableCall_t1075 * __this, Object_t * ___target, MethodInfo_t142 * ___function, MethodInfo* method){
	{
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		if (___target)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_t1171 * L_0 = (ArgumentNullException_t1171 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1171_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6533(L_0, (String_t*) &_stringLiteral437, /*hidden argument*/&ArgumentNullException__ctor_m6533_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0017:
	{
		if (___function)
		{
			goto IL_0028;
		}
	}
	{
		ArgumentNullException_t1171 * L_1 = (ArgumentNullException_t1171 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1171_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6533(L_1, (String_t*) &_stringLiteral438, /*hidden argument*/&ArgumentNullException__ctor_m6533_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0028:
	{
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern MethodInfo BaseInvokableCall_AllowInvoke_m6359_MethodInfo;
 bool BaseInvokableCall_AllowInvoke_m6359 (Object_t * __this/* static, unused */, Delegate_t153 * ___delegate, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		NullCheck(___delegate);
		MethodInfo_t142 * L_0 = Delegate_get_Method_m6534(___delegate, /*hidden argument*/&Delegate_get_Method_m6534_MethodInfo);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker0< bool >::Invoke(&MethodBase_get_IsStatic_m6535_MethodInfo, L_0);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		NullCheck(___delegate);
		Object_t * L_2 = Delegate_get_Target_m6536(___delegate, /*hidden argument*/&Delegate_get_Target_m6536_MethodInfo);
		G_B3_0 = ((((int32_t)((((Object_t *)L_2) == ((Object_t *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 1;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.BaseInvokableCall
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
MethodInfo BaseInvokableCall__ctor_m6357_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseInvokableCall__ctor_m6357/* method */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo BaseInvokableCall_t1075_BaseInvokableCall__ctor_m6358_ParameterInfos[] = 
{
	{"target", 0, 134219289, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"function", 1, 134219290, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo BaseInvokableCall__ctor_m6358_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&BaseInvokableCall__ctor_m6358/* method */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, BaseInvokableCall_t1075_BaseInvokableCall__ctor_m6358_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo BaseInvokableCall_t1075_BaseInvokableCall_Invoke_m6537_ParameterInfos[] = 
{
	{"args", 0, 134219291, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[])
MethodInfo BaseInvokableCall_Invoke_m6537_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, BaseInvokableCall_t1075_BaseInvokableCall_Invoke_m6537_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo BaseInvokableCall_t1075_BaseInvokableCall_ThrowOnInvalidArg_m6538_ParameterInfos[] = 
{
	{"arg", 0, 134219292, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern Il2CppGenericContainer BaseInvokableCall_ThrowOnInvalidArg_m6538_Il2CppGenericContainer;
extern TypeInfo BaseInvokableCall_ThrowOnInvalidArg_m6538_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull BaseInvokableCall_ThrowOnInvalidArg_m6538_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &BaseInvokableCall_ThrowOnInvalidArg_m6538_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* BaseInvokableCall_ThrowOnInvalidArg_m6538_Il2CppGenericParametersArray[1] = 
{
	&BaseInvokableCall_ThrowOnInvalidArg_m6538_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_m6538_MethodInfo;
Il2CppGenericContainer BaseInvokableCall_ThrowOnInvalidArg_m6538_Il2CppGenericContainer = { { NULL, NULL }, NULL, &BaseInvokableCall_ThrowOnInvalidArg_m6538_MethodInfo, 1, 1, BaseInvokableCall_ThrowOnInvalidArg_m6538_Il2CppGenericParametersArray };
extern Il2CppType BaseInvokableCall_ThrowOnInvalidArg_m6538_gp_0_0_0_0;
static Il2CppRGCTXDefinition BaseInvokableCall_ThrowOnInvalidArg_m6538_RGCTXData[3] = 
{
	{ IL2CPP_RGCTX_DATA_CLASS, &BaseInvokableCall_ThrowOnInvalidArg_m6538_gp_0_0_0_0 }/* Class Definition */,
	{ IL2CPP_RGCTX_DATA_TYPE, &BaseInvokableCall_ThrowOnInvalidArg_m6538_gp_0_0_0_0 }/* Type Definition */,
	{IL2CPP_RGCTX_DATA_INVALID, NULL},
};
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg(System.Object)
MethodInfo BaseInvokableCall_ThrowOnInvalidArg_m6538_MethodInfo = 
{
	"ThrowOnInvalidArg"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, BaseInvokableCall_t1075_BaseInvokableCall_ThrowOnInvalidArg_m6538_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, true/* is_generic */
	, false/* is_inflated */
	, 1487/* token */
	, BaseInvokableCall_ThrowOnInvalidArg_m6538_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &BaseInvokableCall_ThrowOnInvalidArg_m6538_Il2CppGenericContainer/* genericContainer */

};
extern Il2CppType Delegate_t153_0_0_0;
extern Il2CppType Delegate_t153_0_0_0;
static ParameterInfo BaseInvokableCall_t1075_BaseInvokableCall_AllowInvoke_m6359_ParameterInfos[] = 
{
	{"delegate", 0, 134219293, &EmptyCustomAttributesCache, &Delegate_t153_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
MethodInfo BaseInvokableCall_AllowInvoke_m6359_MethodInfo = 
{
	"AllowInvoke"/* name */
	, (methodPointerType)&BaseInvokableCall_AllowInvoke_m6359/* method */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, BaseInvokableCall_t1075_BaseInvokableCall_AllowInvoke_m6359_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 148/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo BaseInvokableCall_t1075_BaseInvokableCall_Find_m6539_ParameterInfos[] = 
{
	{"targetObj", 0, 134219294, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219295, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo BaseInvokableCall_Find_m6539_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, BaseInvokableCall_t1075_BaseInvokableCall_Find_m6539_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* BaseInvokableCall_t1075_MethodInfos[] =
{
	&BaseInvokableCall__ctor_m6357_MethodInfo,
	&BaseInvokableCall__ctor_m6358_MethodInfo,
	&BaseInvokableCall_Invoke_m6537_MethodInfo,
	&BaseInvokableCall_ThrowOnInvalidArg_m6538_MethodInfo,
	&BaseInvokableCall_AllowInvoke_m6359_MethodInfo,
	&BaseInvokableCall_Find_m6539_MethodInfo,
	NULL
};
static MethodInfo* BaseInvokableCall_t1075_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	NULL,
	NULL,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
extern Il2CppType BaseInvokableCall_t1075_1_0_0;
struct BaseInvokableCall_t1075;
TypeInfo BaseInvokableCall_t1075_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "BaseInvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, BaseInvokableCall_t1075_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, BaseInvokableCall_t1075_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* cast_class */
	, &BaseInvokableCall_t1075_0_0_0/* byval_arg */
	, &BaseInvokableCall_t1075_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (BaseInvokableCall_t1075)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048704/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 6/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCall.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InvokableCall_t1076_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall
#include "UnityEngine_UnityEngine_Events_InvokableCallMethodDeclarations.h"

// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityAction.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
extern TypeInfo UnityAction_t290_il2cpp_TypeInfo;
extern TypeInfo Void_t99_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction
#include "UnityEngine_UnityEngine_Events_UnityActionMethodDeclarations.h"
extern Il2CppType UnityAction_t290_0_0_0;
extern MethodInfo Type_GetTypeFromHandle_m395_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m456_MethodInfo;
extern MethodInfo Delegate_Combine_m2149_MethodInfo;
extern MethodInfo UnityAction_Invoke_m2116_MethodInfo;


// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern MethodInfo InvokableCall__ctor_m6360_MethodInfo;
 void InvokableCall__ctor_m6360 (InvokableCall_t1076 * __this, Object_t * ___target, MethodInfo_t142 * ___theFunction, MethodInfo* method){
	{
		BaseInvokableCall__ctor_m6358(__this, ___target, ___theFunction, /*hidden argument*/&BaseInvokableCall__ctor_m6358_MethodInfo);
		UnityAction_t290 * L_0 = (__this->___Delegate_0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&UnityAction_t290_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		Delegate_t153 * L_2 = Delegate_CreateDelegate_m456(NULL /*static, unused*/, L_1, ___target, ___theFunction, /*hidden argument*/&Delegate_CreateDelegate_m456_MethodInfo);
		Delegate_t153 * L_3 = Delegate_Combine_m2149(NULL /*static, unused*/, L_0, ((UnityAction_t290 *)IsInst(L_2, InitializedTypeInfo(&UnityAction_t290_il2cpp_TypeInfo))), /*hidden argument*/&Delegate_Combine_m2149_MethodInfo);
		__this->___Delegate_0 = ((UnityAction_t290 *)Castclass(L_3, InitializedTypeInfo(&UnityAction_t290_il2cpp_TypeInfo)));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern MethodInfo InvokableCall_Invoke_m6361_MethodInfo;
 void InvokableCall_Invoke_m6361 (InvokableCall_t1076 * __this, ObjectU5BU5D_t115* ___args, MethodInfo* method){
	{
		UnityAction_t290 * L_0 = (__this->___Delegate_0);
		bool L_1 = BaseInvokableCall_AllowInvoke_m6359(NULL /*static, unused*/, L_0, /*hidden argument*/&BaseInvokableCall_AllowInvoke_m6359_MethodInfo);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		UnityAction_t290 * L_2 = (__this->___Delegate_0);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(&UnityAction_Invoke_m2116_MethodInfo, L_2);
	}

IL_001b:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern MethodInfo InvokableCall_Find_m6362_MethodInfo;
 bool InvokableCall_Find_m6362 (InvokableCall_t1076 * __this, Object_t * ___targetObj, MethodInfo_t142 * ___method, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		UnityAction_t290 * L_0 = (__this->___Delegate_0);
		NullCheck(L_0);
		Object_t * L_1 = Delegate_get_Target_m6536(L_0, /*hidden argument*/&Delegate_get_Target_m6536_MethodInfo);
		if ((((Object_t *)L_1) != ((Object_t *)___targetObj)))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_t290 * L_2 = (__this->___Delegate_0);
		NullCheck(L_2);
		MethodInfo_t142 * L_3 = Delegate_get_Method_m6534(L_2, /*hidden argument*/&Delegate_get_Method_m6534_MethodInfo);
		G_B3_0 = ((((MethodInfo_t142 *)L_3) == ((MethodInfo_t142 *)___method))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCall
extern Il2CppType UnityAction_t290_0_0_1;
FieldInfo InvokableCall_t1076____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_t290_0_0_1/* type */
	, &InvokableCall_t1076_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_t1076, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_t1076_FieldInfos[] =
{
	&InvokableCall_t1076____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_t1076_InvokableCall__ctor_m6360_ParameterInfos[] = 
{
	{"target", 0, 134219296, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219297, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall__ctor_m6360_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall__ctor_m6360/* method */
	, &InvokableCall_t1076_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_t1076_InvokableCall__ctor_m6360_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_t1076_InvokableCall_Invoke_m6361_ParameterInfos[] = 
{
	{"args", 0, 134219298, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
MethodInfo InvokableCall_Invoke_m6361_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_Invoke_m6361/* method */
	, &InvokableCall_t1076_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_t1076_InvokableCall_Invoke_m6361_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_t1076_InvokableCall_Find_m6362_ParameterInfos[] = 
{
	{"targetObj", 0, 134219299, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219300, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_Find_m6362_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_Find_m6362/* method */
	, &InvokableCall_t1076_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_t1076_InvokableCall_Find_m6362_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_t1076_MethodInfos[] =
{
	&InvokableCall__ctor_m6360_MethodInfo,
	&InvokableCall_Invoke_m6361_MethodInfo,
	&InvokableCall_Find_m6362_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_t1076_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_Invoke_m6361_MethodInfo,
	&InvokableCall_Find_m6362_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_t1076_0_0_0;
extern Il2CppType InvokableCall_t1076_1_0_0;
struct InvokableCall_t1076;
TypeInfo InvokableCall_t1076_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_t1076_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_t1076_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_t1076_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_t1076_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_t1076_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_t1076_0_0_0/* byval_arg */
	, &InvokableCall_t1076_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_t1076)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall`1
#include "UnityEngine_UnityEngine_Events_InvokableCall_1.h"
extern Il2CppGenericContainer InvokableCall_1_t1077_Il2CppGenericContainer;
extern TypeInfo InvokableCall_1_t1077_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_1_t1077_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_1_t1077_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
static Il2CppGenericParamFull* InvokableCall_1_t1077_Il2CppGenericParametersArray[1] = 
{
	&InvokableCall_1_t1077_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo InvokableCall_1_t1077_il2cpp_TypeInfo;
Il2CppGenericContainer InvokableCall_1_t1077_Il2CppGenericContainer = { { NULL, NULL }, NULL, &InvokableCall_1_t1077_il2cpp_TypeInfo, 1, 0, InvokableCall_1_t1077_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t1077_InvokableCall_1__ctor_m6540_ParameterInfos[] = 
{
	{"target", 0, 134219301, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219302, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`1::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m6540_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1077_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1077_InvokableCall_1__ctor_m6540_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1174_0_0_0;
extern Il2CppType UnityAction_1_t1174_0_0_0;
static ParameterInfo InvokableCall_1_t1077_InvokableCall_1__ctor_m6541_ParameterInfos[] = 
{
	{"callback", 0, 134219303, &EmptyCustomAttributesCache, &UnityAction_1_t1174_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`1::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m6541_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1077_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1077_InvokableCall_1__ctor_m6541_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t1077_InvokableCall_1_Invoke_m6542_ParameterInfos[] = 
{
	{"args", 0, 134219304, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`1::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m6542_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1077_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1077_InvokableCall_1_Invoke_m6542_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t1077_InvokableCall_1_Find_m6543_ParameterInfos[] = 
{
	{"targetObj", 0, 134219305, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219306, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
// System.Boolean UnityEngine.Events.InvokableCall`1::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m6543_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_1_t1077_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_1_t1077_InvokableCall_1_Find_m6543_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1496/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_1_t1077_MethodInfos[] =
{
	&InvokableCall_1__ctor_m6540_MethodInfo,
	&InvokableCall_1__ctor_m6541_MethodInfo,
	&InvokableCall_1_Invoke_m6542_MethodInfo,
	&InvokableCall_1_Find_m6543_MethodInfo,
	NULL
};
extern Il2CppType UnityAction_1_t1174_0_0_1;
FieldInfo InvokableCall_1_t1077____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t1174_0_0_1/* type */
	, &InvokableCall_1_t1077_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t1077_FieldInfos[] =
{
	&InvokableCall_1_t1077____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t1077_0_0_0;
extern Il2CppType InvokableCall_1_t1077_1_0_0;
struct InvokableCall_1_t1077;
TypeInfo InvokableCall_1_t1077_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t1077_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t1077_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t1077_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &InvokableCall_1_t1077_0_0_0/* byval_arg */
	, &InvokableCall_1_t1077_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &InvokableCall_1_t1077_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall`2
#include "UnityEngine_UnityEngine_Events_InvokableCall_2.h"
extern Il2CppGenericContainer InvokableCall_2_t1078_Il2CppGenericContainer;
extern TypeInfo InvokableCall_2_t1078_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_2_t1078_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_2_t1078_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo InvokableCall_2_t1078_gp_T2_1_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_2_t1078_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_2_t1078_Il2CppGenericContainer, 1}, {NULL, "T2", 0, 0, NULL} };
static Il2CppGenericParamFull* InvokableCall_2_t1078_Il2CppGenericParametersArray[2] = 
{
	&InvokableCall_2_t1078_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_2_t1078_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo InvokableCall_2_t1078_il2cpp_TypeInfo;
Il2CppGenericContainer InvokableCall_2_t1078_Il2CppGenericContainer = { { NULL, NULL }, NULL, &InvokableCall_2_t1078_il2cpp_TypeInfo, 2, 0, InvokableCall_2_t1078_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_2_t1078_InvokableCall_2__ctor_m6544_ParameterInfos[] = 
{
	{"target", 0, 134219307, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219308, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`2::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_2__ctor_m6544_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1078_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1078_InvokableCall_2__ctor_m6544_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1497/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_2_t1078_InvokableCall_2_Invoke_m6545_ParameterInfos[] = 
{
	{"args", 0, 134219309, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`2::Invoke(System.Object[])
MethodInfo InvokableCall_2_Invoke_m6545_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1078_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1078_InvokableCall_2_Invoke_m6545_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1498/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_2_t1078_InvokableCall_2_Find_m6546_ParameterInfos[] = 
{
	{"targetObj", 0, 134219310, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219311, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
// System.Boolean UnityEngine.Events.InvokableCall`2::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_2_Find_m6546_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_2_t1078_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_2_t1078_InvokableCall_2_Find_m6546_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1499/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_2_t1078_MethodInfos[] =
{
	&InvokableCall_2__ctor_m6544_MethodInfo,
	&InvokableCall_2_Invoke_m6545_MethodInfo,
	&InvokableCall_2_Find_m6546_MethodInfo,
	NULL
};
extern Il2CppType UnityAction_2_t1177_0_0_1;
FieldInfo InvokableCall_2_t1078____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_2_t1177_0_0_1/* type */
	, &InvokableCall_2_t1078_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_2_t1078_FieldInfos[] =
{
	&InvokableCall_2_t1078____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_2_t1078_0_0_0;
extern Il2CppType InvokableCall_2_t1078_1_0_0;
struct InvokableCall_2_t1078;
TypeInfo InvokableCall_2_t1078_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_2_t1078_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_2_t1078_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_2_t1078_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &InvokableCall_2_t1078_0_0_0/* byval_arg */
	, &InvokableCall_2_t1078_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &InvokableCall_2_t1078_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall`3
#include "UnityEngine_UnityEngine_Events_InvokableCall_3.h"
extern Il2CppGenericContainer InvokableCall_3_t1079_Il2CppGenericContainer;
extern TypeInfo InvokableCall_3_t1079_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_3_t1079_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_3_t1079_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo InvokableCall_3_t1079_gp_T2_1_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_3_t1079_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_3_t1079_Il2CppGenericContainer, 1}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo InvokableCall_3_t1079_gp_T3_2_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_3_t1079_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_3_t1079_Il2CppGenericContainer, 2}, {NULL, "T3", 0, 0, NULL} };
static Il2CppGenericParamFull* InvokableCall_3_t1079_Il2CppGenericParametersArray[3] = 
{
	&InvokableCall_3_t1079_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_3_t1079_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_3_t1079_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo InvokableCall_3_t1079_il2cpp_TypeInfo;
Il2CppGenericContainer InvokableCall_3_t1079_Il2CppGenericContainer = { { NULL, NULL }, NULL, &InvokableCall_3_t1079_il2cpp_TypeInfo, 3, 0, InvokableCall_3_t1079_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_3_t1079_InvokableCall_3__ctor_m6547_ParameterInfos[] = 
{
	{"target", 0, 134219312, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219313, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`3::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_3__ctor_m6547_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1079_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1079_InvokableCall_3__ctor_m6547_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1500/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_3_t1079_InvokableCall_3_Invoke_m6548_ParameterInfos[] = 
{
	{"args", 0, 134219314, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`3::Invoke(System.Object[])
MethodInfo InvokableCall_3_Invoke_m6548_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1079_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1079_InvokableCall_3_Invoke_m6548_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1501/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_3_t1079_InvokableCall_3_Find_m6549_ParameterInfos[] = 
{
	{"targetObj", 0, 134219315, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219316, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
// System.Boolean UnityEngine.Events.InvokableCall`3::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_3_Find_m6549_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_3_t1079_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_3_t1079_InvokableCall_3_Find_m6549_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1502/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_3_t1079_MethodInfos[] =
{
	&InvokableCall_3__ctor_m6547_MethodInfo,
	&InvokableCall_3_Invoke_m6548_MethodInfo,
	&InvokableCall_3_Find_m6549_MethodInfo,
	NULL
};
extern Il2CppType UnityAction_3_t1181_0_0_1;
FieldInfo InvokableCall_3_t1079____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_3_t1181_0_0_1/* type */
	, &InvokableCall_3_t1079_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_3_t1079_FieldInfos[] =
{
	&InvokableCall_3_t1079____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_3_t1079_0_0_0;
extern Il2CppType InvokableCall_3_t1079_1_0_0;
struct InvokableCall_3_t1079;
TypeInfo InvokableCall_3_t1079_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_3_t1079_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_3_t1079_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_3_t1079_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &InvokableCall_3_t1079_0_0_0/* byval_arg */
	, &InvokableCall_3_t1079_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &InvokableCall_3_t1079_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.InvokableCall`4
#include "UnityEngine_UnityEngine_Events_InvokableCall_4.h"
extern Il2CppGenericContainer InvokableCall_4_t1080_Il2CppGenericContainer;
extern TypeInfo InvokableCall_4_t1080_gp_T1_0_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_4_t1080_gp_T1_0_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_4_t1080_Il2CppGenericContainer, 0}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo InvokableCall_4_t1080_gp_T2_1_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_4_t1080_gp_T2_1_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_4_t1080_Il2CppGenericContainer, 1}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo InvokableCall_4_t1080_gp_T3_2_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_4_t1080_gp_T3_2_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_4_t1080_Il2CppGenericContainer, 2}, {NULL, "T3", 0, 0, NULL} };
extern TypeInfo InvokableCall_4_t1080_gp_T4_3_il2cpp_TypeInfo;
Il2CppGenericParamFull InvokableCall_4_t1080_gp_T4_3_il2cpp_TypeInfo_GenericParamFull = { { &InvokableCall_4_t1080_Il2CppGenericContainer, 3}, {NULL, "T4", 0, 0, NULL} };
static Il2CppGenericParamFull* InvokableCall_4_t1080_Il2CppGenericParametersArray[4] = 
{
	&InvokableCall_4_t1080_gp_T1_0_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1080_gp_T2_1_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1080_gp_T3_2_il2cpp_TypeInfo_GenericParamFull,
	&InvokableCall_4_t1080_gp_T4_3_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo InvokableCall_4_t1080_il2cpp_TypeInfo;
Il2CppGenericContainer InvokableCall_4_t1080_Il2CppGenericContainer = { { NULL, NULL }, NULL, &InvokableCall_4_t1080_il2cpp_TypeInfo, 4, 0, InvokableCall_4_t1080_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_4_t1080_InvokableCall_4__ctor_m6550_ParameterInfos[] = 
{
	{"target", 0, 134219317, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219318, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`4::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_4__ctor_m6550_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1080_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1080_InvokableCall_4__ctor_m6550_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1503/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_4_t1080_InvokableCall_4_Invoke_m6551_ParameterInfos[] = 
{
	{"args", 0, 134219319, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.InvokableCall`4::Invoke(System.Object[])
MethodInfo InvokableCall_4_Invoke_m6551_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1080_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1080_InvokableCall_4_Invoke_m6551_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1504/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_4_t1080_InvokableCall_4_Find_m6552_ParameterInfos[] = 
{
	{"targetObj", 0, 134219320, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219321, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
// System.Boolean UnityEngine.Events.InvokableCall`4::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_4_Find_m6552_MethodInfo = 
{
	"Find"/* name */
	, NULL/* method */
	, &InvokableCall_4_t1080_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, NULL/* invoker_method */
	, InvokableCall_4_t1080_InvokableCall_4_Find_m6552_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1505/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCall_4_t1080_MethodInfos[] =
{
	&InvokableCall_4__ctor_m6550_MethodInfo,
	&InvokableCall_4_Invoke_m6551_MethodInfo,
	&InvokableCall_4_Find_m6552_MethodInfo,
	NULL
};
extern Il2CppType UnityAction_4_t1186_0_0_1;
FieldInfo InvokableCall_4_t1080____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_4_t1186_0_0_1/* type */
	, &InvokableCall_4_t1080_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_4_t1080_FieldInfos[] =
{
	&InvokableCall_4_t1080____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_4_t1080_0_0_0;
extern Il2CppType InvokableCall_4_t1080_1_0_0;
struct InvokableCall_4_t1080;
TypeInfo InvokableCall_4_t1080_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_4_t1080_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_4_t1080_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_4_t1080_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &InvokableCall_4_t1080_0_0_0/* byval_arg */
	, &InvokableCall_4_t1080_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &InvokableCall_4_t1080_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1.h"
extern Il2CppGenericContainer CachedInvokableCall_1_t1081_Il2CppGenericContainer;
extern TypeInfo CachedInvokableCall_1_t1081_gp_T_0_il2cpp_TypeInfo;
Il2CppGenericParamFull CachedInvokableCall_1_t1081_gp_T_0_il2cpp_TypeInfo_GenericParamFull = { { &CachedInvokableCall_1_t1081_Il2CppGenericContainer, 0}, {NULL, "T", 0, 0, NULL} };
static Il2CppGenericParamFull* CachedInvokableCall_1_t1081_Il2CppGenericParametersArray[1] = 
{
	&CachedInvokableCall_1_t1081_gp_T_0_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo CachedInvokableCall_1_t1081_il2cpp_TypeInfo;
Il2CppGenericContainer CachedInvokableCall_1_t1081_Il2CppGenericContainer = { { NULL, NULL }, NULL, &CachedInvokableCall_1_t1081_il2cpp_TypeInfo, 1, 0, CachedInvokableCall_1_t1081_Il2CppGenericParametersArray };
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType CachedInvokableCall_1_t1081_gp_0_0_0_0;
extern Il2CppType CachedInvokableCall_1_t1081_gp_0_0_0_0;
static ParameterInfo CachedInvokableCall_1_t1081_CachedInvokableCall_1__ctor_m6553_ParameterInfos[] = 
{
	{"target", 0, 134219322, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134219323, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134219324, &EmptyCustomAttributesCache, &CachedInvokableCall_1_t1081_gp_0_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.CachedInvokableCall`1::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m6553_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &CachedInvokableCall_1_t1081_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, CachedInvokableCall_1_t1081_CachedInvokableCall_1__ctor_m6553_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1506/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t1081_CachedInvokableCall_1_Invoke_m6554_ParameterInfos[] = 
{
	{"args", 0, 134219325, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.CachedInvokableCall`1::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m6554_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &CachedInvokableCall_1_t1081_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, CachedInvokableCall_1_t1081_CachedInvokableCall_1_Invoke_m6554_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1507/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* CachedInvokableCall_1_t1081_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m6553_MethodInfo,
	&CachedInvokableCall_1_Invoke_m6554_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t1081____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t1081_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t1081_FieldInfos[] =
{
	&CachedInvokableCall_1_t1081____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t1081_0_0_0;
extern Il2CppType CachedInvokableCall_1_t1081_1_0_0;
struct CachedInvokableCall_1_t1081;
TypeInfo CachedInvokableCall_1_t1081_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t1081_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t1081_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t1081_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &CachedInvokableCall_1_t1081_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t1081_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &CachedInvokableCall_1_t1081_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallState.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityEventCallState_t1082_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventCallState
#include "UnityEngine_UnityEngine_Events_UnityEventCallStateMethodDeclarations.h"



// Metadata Definition UnityEngine.Events.UnityEventCallState
extern Il2CppType Int32_t93_0_0_1542;
FieldInfo UnityEventCallState_t1082____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t93_0_0_1542/* type */
	, &UnityEventCallState_t1082_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventCallState_t1082, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UnityEventCallState_t1082_0_0_32854;
FieldInfo UnityEventCallState_t1082____Off_2_FieldInfo = 
{
	"Off"/* name */
	, &UnityEventCallState_t1082_0_0_32854/* type */
	, &UnityEventCallState_t1082_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UnityEventCallState_t1082_0_0_32854;
FieldInfo UnityEventCallState_t1082____EditorAndRuntime_3_FieldInfo = 
{
	"EditorAndRuntime"/* name */
	, &UnityEventCallState_t1082_0_0_32854/* type */
	, &UnityEventCallState_t1082_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UnityEventCallState_t1082_0_0_32854;
FieldInfo UnityEventCallState_t1082____RuntimeOnly_4_FieldInfo = 
{
	"RuntimeOnly"/* name */
	, &UnityEventCallState_t1082_0_0_32854/* type */
	, &UnityEventCallState_t1082_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEventCallState_t1082_FieldInfos[] =
{
	&UnityEventCallState_t1082____value___1_FieldInfo,
	&UnityEventCallState_t1082____Off_2_FieldInfo,
	&UnityEventCallState_t1082____EditorAndRuntime_3_FieldInfo,
	&UnityEventCallState_t1082____RuntimeOnly_4_FieldInfo,
	NULL
};
static const int32_t UnityEventCallState_t1082____Off_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry UnityEventCallState_t1082____Off_2_DefaultValue = 
{
	&UnityEventCallState_t1082____Off_2_FieldInfo/* field */
	, { (char*)&UnityEventCallState_t1082____Off_2_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t UnityEventCallState_t1082____EditorAndRuntime_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry UnityEventCallState_t1082____EditorAndRuntime_3_DefaultValue = 
{
	&UnityEventCallState_t1082____EditorAndRuntime_3_FieldInfo/* field */
	, { (char*)&UnityEventCallState_t1082____EditorAndRuntime_3_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t UnityEventCallState_t1082____RuntimeOnly_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry UnityEventCallState_t1082____RuntimeOnly_4_DefaultValue = 
{
	&UnityEventCallState_t1082____RuntimeOnly_4_FieldInfo/* field */
	, { (char*)&UnityEventCallState_t1082____RuntimeOnly_4_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UnityEventCallState_t1082_FieldDefaultValues[] = 
{
	&UnityEventCallState_t1082____Off_2_DefaultValue,
	&UnityEventCallState_t1082____EditorAndRuntime_3_DefaultValue,
	&UnityEventCallState_t1082____RuntimeOnly_4_DefaultValue,
	NULL
};
static MethodInfo* UnityEventCallState_t1082_MethodInfos[] =
{
	NULL
};
static MethodInfo* UnityEventCallState_t1082_VTable[] =
{
	&Enum_Equals_m191_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Enum_GetHashCode_m193_MethodInfo,
	&Enum_ToString_m194_MethodInfo,
	&Enum_ToString_m195_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m196_MethodInfo,
	&Enum_System_IConvertible_ToByte_m197_MethodInfo,
	&Enum_System_IConvertible_ToChar_m198_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m199_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m200_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m201_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m202_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m203_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m204_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m205_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m206_MethodInfo,
	&Enum_ToString_m207_MethodInfo,
	&Enum_System_IConvertible_ToType_m208_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m209_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m210_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m211_MethodInfo,
	&Enum_CompareTo_m212_MethodInfo,
	&Enum_GetTypeCode_m213_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityEventCallState_t1082_InterfacesOffsets[] = 
{
	{ &IFormattable_t94_il2cpp_TypeInfo, 4},
	{ &IConvertible_t95_il2cpp_TypeInfo, 5},
	{ &IComparable_t96_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEventCallState_t1082_0_0_0;
extern Il2CppType UnityEventCallState_t1082_1_0_0;
TypeInfo UnityEventCallState_t1082_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventCallState"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEventCallState_t1082_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEventCallState_t1082_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t97_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t93_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityEventCallState_t1082_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &UnityEventCallState_t1082_0_0_0/* byval_arg */
	, &UnityEventCallState_t1082_1_0_0/* this_arg */
	, UnityEventCallState_t1082_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UnityEventCallState_t1082_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventCallState_t1082)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCall.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo PersistentCall_t1083_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCall
#include "UnityEngine_UnityEngine_Events_PersistentCallMethodDeclarations.h"

// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBase.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_0.h"
// UnityEngine.Events.CachedInvokableCall`1<System.String>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_1.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_2.h"
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfo.h"
extern TypeInfo UnityEventBase_t1084_il2cpp_TypeInfo;
extern TypeInfo MethodInfo_t142_il2cpp_TypeInfo;
extern TypeInfo CachedInvokableCall_1_t1188_il2cpp_TypeInfo;
extern TypeInfo CachedInvokableCall_1_t1189_il2cpp_TypeInfo;
extern TypeInfo CachedInvokableCall_1_t1190_il2cpp_TypeInfo;
extern TypeInfo CachedInvokableCall_1_t1191_il2cpp_TypeInfo;
extern TypeInfo Object_t117_il2cpp_TypeInfo;
extern TypeInfo TypeU5BU5D_t878_il2cpp_TypeInfo;
extern TypeInfo ConstructorInfo_t1192_il2cpp_TypeInfo;
extern TypeInfo ObjectU5BU5D_t115_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_genMethodDeclarations.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_0MethodDeclarations.h"
// UnityEngine.Events.CachedInvokableCall`1<System.String>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_1MethodDeclarations.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_2MethodDeclarations.h"
// System.Reflection.ConstructorInfo
#include "mscorlib_System_Reflection_ConstructorInfoMethodDeclarations.h"
extern Il2CppType CachedInvokableCall_1_t1081_0_0_0;
extern MethodInfo PersistentCall_get_target_m6364_MethodInfo;
extern MethodInfo Object_op_Inequality_m335_MethodInfo;
extern MethodInfo PersistentCall_get_methodName_m6365_MethodInfo;
extern MethodInfo String_IsNullOrEmpty_m2311_MethodInfo;
extern MethodInfo UnityEventBase_FindMethod_m6380_MethodInfo;
extern MethodInfo UnityEventBase_GetDelegate_m6555_MethodInfo;
extern MethodInfo PersistentCall_GetObjectCall_m6370_MethodInfo;
extern MethodInfo CachedInvokableCall_1__ctor_m6556_MethodInfo;
extern MethodInfo CachedInvokableCall_1__ctor_m6557_MethodInfo;
extern MethodInfo CachedInvokableCall_1__ctor_m6558_MethodInfo;
extern MethodInfo CachedInvokableCall_1__ctor_m6559_MethodInfo;
extern MethodInfo Type_GetType_m6560_MethodInfo;
extern MethodInfo Type_MakeGenericType_m6561_MethodInfo;
extern MethodInfo Type_GetConstructor_m6562_MethodInfo;
extern MethodInfo Object_GetType_m449_MethodInfo;
extern MethodInfo Type_IsAssignableFrom_m6563_MethodInfo;
extern MethodInfo ConstructorInfo_Invoke_m6564_MethodInfo;


// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern MethodInfo PersistentCall__ctor_m6363_MethodInfo;
 void PersistentCall__ctor_m6363 (PersistentCall_t1083 * __this, MethodInfo* method){
	{
		ArgumentCache_t1074 * L_0 = (ArgumentCache_t1074 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentCache_t1074_il2cpp_TypeInfo));
		ArgumentCache__ctor_m6350(L_0, /*hidden argument*/&ArgumentCache__ctor_m6350_MethodInfo);
		__this->___m_Arguments_3 = L_0;
		__this->___m_CallState_4 = 2;
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
 Object_t117 * PersistentCall_get_target_m6364 (PersistentCall_t1083 * __this, MethodInfo* method){
	{
		Object_t117 * L_0 = (__this->___m_Target_0);
		return L_0;
	}
}
// System.String UnityEngine.Events.PersistentCall::get_methodName()
 String_t* PersistentCall_get_methodName_m6365 (PersistentCall_t1083 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___m_MethodName_1);
		return L_0;
	}
}
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern MethodInfo PersistentCall_get_mode_m6366_MethodInfo;
 int32_t PersistentCall_get_mode_m6366 (PersistentCall_t1083 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___m_Mode_2);
		return L_0;
	}
}
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern MethodInfo PersistentCall_get_arguments_m6367_MethodInfo;
 ArgumentCache_t1074 * PersistentCall_get_arguments_m6367 (PersistentCall_t1083 * __this, MethodInfo* method){
	{
		ArgumentCache_t1074 * L_0 = (__this->___m_Arguments_3);
		return L_0;
	}
}
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern MethodInfo PersistentCall_IsValid_m6368_MethodInfo;
 bool PersistentCall_IsValid_m6368 (PersistentCall_t1083 * __this, MethodInfo* method){
	int32_t G_B3_0 = 0;
	{
		Object_t117 * L_0 = PersistentCall_get_target_m6364(__this, /*hidden argument*/&PersistentCall_get_target_m6364_MethodInfo);
		bool L_1 = Object_op_Inequality_m335(NULL /*static, unused*/, L_0, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_2 = PersistentCall_get_methodName_m6365(__this, /*hidden argument*/&PersistentCall_get_methodName_m6365_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_3 = String_IsNullOrEmpty_m2311(NULL /*static, unused*/, L_2, /*hidden argument*/&String_IsNullOrEmpty_m2311_MethodInfo);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern MethodInfo PersistentCall_GetRuntimeCall_m6369_MethodInfo;
 BaseInvokableCall_t1075 * PersistentCall_GetRuntimeCall_m6369 (PersistentCall_t1083 * __this, UnityEventBase_t1084 * ___theEvent, MethodInfo* method){
	MethodInfo_t142 * V_0 = {0};
	int32_t V_1 = {0};
	{
		int32_t L_0 = (__this->___m_CallState_4);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		if (___theEvent)
		{
			goto IL_0013;
		}
	}

IL_0011:
	{
		return (BaseInvokableCall_t1075 *)NULL;
	}

IL_0013:
	{
		NullCheck(___theEvent);
		MethodInfo_t142 * L_1 = UnityEventBase_FindMethod_m6380(___theEvent, __this, /*hidden argument*/&UnityEventBase_FindMethod_m6380_MethodInfo);
		V_0 = L_1;
		if (V_0)
		{
			goto IL_0023;
		}
	}
	{
		return (BaseInvokableCall_t1075 *)NULL;
	}

IL_0023:
	{
		int32_t L_2 = (__this->___m_Mode_2);
		V_1 = L_2;
		if (V_1 == 0)
		{
			goto IL_0051;
		}
		if (V_1 == 1)
		{
			goto IL_00d2;
		}
		if (V_1 == 2)
		{
			goto IL_005f;
		}
		if (V_1 == 3)
		{
			goto IL_008a;
		}
		if (V_1 == 4)
		{
			goto IL_0072;
		}
		if (V_1 == 5)
		{
			goto IL_00a2;
		}
		if (V_1 == 6)
		{
			goto IL_00ba;
		}
	}
	{
		goto IL_00df;
	}

IL_0051:
	{
		Object_t117 * L_3 = PersistentCall_get_target_m6364(__this, /*hidden argument*/&PersistentCall_get_target_m6364_MethodInfo);
		NullCheck(___theEvent);
		BaseInvokableCall_t1075 * L_4 = (BaseInvokableCall_t1075 *)VirtFuncInvoker2< BaseInvokableCall_t1075 *, Object_t *, MethodInfo_t142 * >::Invoke(&UnityEventBase_GetDelegate_m6555_MethodInfo, ___theEvent, L_3, V_0);
		return L_4;
	}

IL_005f:
	{
		Object_t117 * L_5 = PersistentCall_get_target_m6364(__this, /*hidden argument*/&PersistentCall_get_target_m6364_MethodInfo);
		ArgumentCache_t1074 * L_6 = (__this->___m_Arguments_3);
		BaseInvokableCall_t1075 * L_7 = PersistentCall_GetObjectCall_m6370(NULL /*static, unused*/, L_5, V_0, L_6, /*hidden argument*/&PersistentCall_GetObjectCall_m6370_MethodInfo);
		return L_7;
	}

IL_0072:
	{
		Object_t117 * L_8 = PersistentCall_get_target_m6364(__this, /*hidden argument*/&PersistentCall_get_target_m6364_MethodInfo);
		ArgumentCache_t1074 * L_9 = (__this->___m_Arguments_3);
		NullCheck(L_9);
		float L_10 = ArgumentCache_get_floatArgument_m6354(L_9, /*hidden argument*/&ArgumentCache_get_floatArgument_m6354_MethodInfo);
		CachedInvokableCall_1_t1188 * L_11 = (CachedInvokableCall_1_t1188 *)il2cpp_codegen_object_new (InitializedTypeInfo(&CachedInvokableCall_1_t1188_il2cpp_TypeInfo));
		CachedInvokableCall_1__ctor_m6556(L_11, L_8, V_0, L_10, /*hidden argument*/&CachedInvokableCall_1__ctor_m6556_MethodInfo);
		return L_11;
	}

IL_008a:
	{
		Object_t117 * L_12 = PersistentCall_get_target_m6364(__this, /*hidden argument*/&PersistentCall_get_target_m6364_MethodInfo);
		ArgumentCache_t1074 * L_13 = (__this->___m_Arguments_3);
		NullCheck(L_13);
		int32_t L_14 = ArgumentCache_get_intArgument_m6353(L_13, /*hidden argument*/&ArgumentCache_get_intArgument_m6353_MethodInfo);
		CachedInvokableCall_1_t1189 * L_15 = (CachedInvokableCall_1_t1189 *)il2cpp_codegen_object_new (InitializedTypeInfo(&CachedInvokableCall_1_t1189_il2cpp_TypeInfo));
		CachedInvokableCall_1__ctor_m6557(L_15, L_12, V_0, L_14, /*hidden argument*/&CachedInvokableCall_1__ctor_m6557_MethodInfo);
		return L_15;
	}

IL_00a2:
	{
		Object_t117 * L_16 = PersistentCall_get_target_m6364(__this, /*hidden argument*/&PersistentCall_get_target_m6364_MethodInfo);
		ArgumentCache_t1074 * L_17 = (__this->___m_Arguments_3);
		NullCheck(L_17);
		String_t* L_18 = ArgumentCache_get_stringArgument_m6355(L_17, /*hidden argument*/&ArgumentCache_get_stringArgument_m6355_MethodInfo);
		CachedInvokableCall_1_t1190 * L_19 = (CachedInvokableCall_1_t1190 *)il2cpp_codegen_object_new (InitializedTypeInfo(&CachedInvokableCall_1_t1190_il2cpp_TypeInfo));
		CachedInvokableCall_1__ctor_m6558(L_19, L_16, V_0, L_18, /*hidden argument*/&CachedInvokableCall_1__ctor_m6558_MethodInfo);
		return L_19;
	}

IL_00ba:
	{
		Object_t117 * L_20 = PersistentCall_get_target_m6364(__this, /*hidden argument*/&PersistentCall_get_target_m6364_MethodInfo);
		ArgumentCache_t1074 * L_21 = (__this->___m_Arguments_3);
		NullCheck(L_21);
		bool L_22 = ArgumentCache_get_boolArgument_m6356(L_21, /*hidden argument*/&ArgumentCache_get_boolArgument_m6356_MethodInfo);
		CachedInvokableCall_1_t1191 * L_23 = (CachedInvokableCall_1_t1191 *)il2cpp_codegen_object_new (InitializedTypeInfo(&CachedInvokableCall_1_t1191_il2cpp_TypeInfo));
		CachedInvokableCall_1__ctor_m6559(L_23, L_20, V_0, L_22, /*hidden argument*/&CachedInvokableCall_1__ctor_m6559_MethodInfo);
		return L_23;
	}

IL_00d2:
	{
		Object_t117 * L_24 = PersistentCall_get_target_m6364(__this, /*hidden argument*/&PersistentCall_get_target_m6364_MethodInfo);
		InvokableCall_t1076 * L_25 = (InvokableCall_t1076 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvokableCall_t1076_il2cpp_TypeInfo));
		InvokableCall__ctor_m6360(L_25, L_24, V_0, /*hidden argument*/&InvokableCall__ctor_m6360_MethodInfo);
		return L_25;
	}

IL_00df:
	{
		return (BaseInvokableCall_t1075 *)NULL;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
 BaseInvokableCall_t1075 * PersistentCall_GetObjectCall_m6370 (Object_t * __this/* static, unused */, Object_t117 * ___target, MethodInfo_t142 * ___method, ArgumentCache_t1074 * ___arguments, MethodInfo* method){
	Type_t * V_0 = {0};
	Type_t * V_1 = {0};
	Type_t * V_2 = {0};
	ConstructorInfo_t1192 * V_3 = {0};
	Object_t117 * V_4 = {0};
	Type_t * G_B3_0 = {0};
	Type_t * G_B2_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&Object_t117_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		V_0 = L_0;
		NullCheck(___arguments);
		String_t* L_1 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352(___arguments, /*hidden argument*/&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_2 = String_IsNullOrEmpty_m2311(NULL /*static, unused*/, L_1, /*hidden argument*/&String_IsNullOrEmpty_m2311_MethodInfo);
		if (L_2)
		{
			goto IL_0039;
		}
	}
	{
		NullCheck(___arguments);
		String_t* L_3 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352(___arguments, /*hidden argument*/&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_4 = Type_GetType_m6560(NULL /*static, unused*/, L_3, 0, /*hidden argument*/&Type_GetType_m6560_MethodInfo);
		Type_t * L_5 = L_4;
		G_B2_0 = L_5;
		if (L_5)
		{
			G_B3_0 = L_5;
			goto IL_0038;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_6 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&Object_t117_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		G_B3_0 = L_6;
	}

IL_0038:
	{
		V_0 = G_B3_0;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_7 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&CachedInvokableCall_1_t1081_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		V_1 = L_7;
		TypeU5BU5D_t878* L_8 = ((TypeU5BU5D_t878*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t878_il2cpp_TypeInfo), 1));
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, V_0);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_8, 0)) = (Type_t *)V_0;
		NullCheck(V_1);
		Type_t * L_9 = (Type_t *)VirtFuncInvoker1< Type_t *, TypeU5BU5D_t878* >::Invoke(&Type_MakeGenericType_m6561_MethodInfo, V_1, L_8);
		V_2 = L_9;
		TypeU5BU5D_t878* L_10 = ((TypeU5BU5D_t878*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t878_il2cpp_TypeInfo), 3));
		Type_t * L_11 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&Object_t117_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		ArrayElementTypeCheck (L_10, L_11);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_10, 0)) = (Type_t *)L_11;
		TypeU5BU5D_t878* L_12 = L_10;
		Type_t * L_13 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&MethodInfo_t142_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_13);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_12, 1)) = (Type_t *)L_13;
		TypeU5BU5D_t878* L_14 = L_12;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 2);
		ArrayElementTypeCheck (L_14, V_0);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_14, 2)) = (Type_t *)V_0;
		NullCheck(V_2);
		ConstructorInfo_t1192 * L_15 = (ConstructorInfo_t1192 *)VirtFuncInvoker1< ConstructorInfo_t1192 *, TypeU5BU5D_t878* >::Invoke(&Type_GetConstructor_m6562_MethodInfo, V_2, L_14);
		V_3 = L_15;
		NullCheck(___arguments);
		Object_t117 * L_16 = ArgumentCache_get_unityObjectArgument_m6351(___arguments, /*hidden argument*/&ArgumentCache_get_unityObjectArgument_m6351_MethodInfo);
		V_4 = L_16;
		bool L_17 = Object_op_Inequality_m335(NULL /*static, unused*/, V_4, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_17)
		{
			goto IL_00aa;
		}
	}
	{
		NullCheck(V_4);
		Type_t * L_18 = Object_GetType_m449(V_4, /*hidden argument*/&Object_GetType_m449_MethodInfo);
		NullCheck(V_0);
		bool L_19 = (bool)VirtFuncInvoker1< bool, Type_t * >::Invoke(&Type_IsAssignableFrom_m6563_MethodInfo, V_0, L_18);
		if (L_19)
		{
			goto IL_00aa;
		}
	}
	{
		V_4 = (Object_t117 *)NULL;
	}

IL_00aa:
	{
		ObjectU5BU5D_t115* L_20 = ((ObjectU5BU5D_t115*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t115_il2cpp_TypeInfo), 3));
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		ArrayElementTypeCheck (L_20, ___target);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 0)) = (Object_t *)___target;
		ObjectU5BU5D_t115* L_21 = L_20;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		ArrayElementTypeCheck (L_21, ___method);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_21, 1)) = (Object_t *)___method;
		ObjectU5BU5D_t115* L_22 = L_21;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, 2);
		ArrayElementTypeCheck (L_22, V_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_22, 2)) = (Object_t *)V_4;
		NullCheck(V_3);
		Object_t * L_23 = ConstructorInfo_Invoke_m6564(V_3, L_22, /*hidden argument*/&ConstructorInfo_Invoke_m6564_MethodInfo);
		return ((BaseInvokableCall_t1075 *)IsInst(L_23, InitializedTypeInfo(&BaseInvokableCall_t1075_il2cpp_TypeInfo)));
	}
}
// Metadata Definition UnityEngine.Events.PersistentCall
extern Il2CppType Object_t117_0_0_1;
extern CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_Target;
FieldInfo PersistentCall_t1083____m_Target_0_FieldInfo = 
{
	"m_Target"/* name */
	, &Object_t117_0_0_1/* type */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1083, ___m_Target_0)/* data */
	, &PersistentCall_t1083__CustomAttributeCache_m_Target/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_MethodName;
FieldInfo PersistentCall_t1083____m_MethodName_1_FieldInfo = 
{
	"m_MethodName"/* name */
	, &String_t_0_0_1/* type */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1083, ___m_MethodName_1)/* data */
	, &PersistentCall_t1083__CustomAttributeCache_m_MethodName/* custom_attributes_cache */

};
extern Il2CppType PersistentListenerMode_t1073_0_0_1;
extern CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_Mode;
FieldInfo PersistentCall_t1083____m_Mode_2_FieldInfo = 
{
	"m_Mode"/* name */
	, &PersistentListenerMode_t1073_0_0_1/* type */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1083, ___m_Mode_2)/* data */
	, &PersistentCall_t1083__CustomAttributeCache_m_Mode/* custom_attributes_cache */

};
extern Il2CppType ArgumentCache_t1074_0_0_1;
extern CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_Arguments;
FieldInfo PersistentCall_t1083____m_Arguments_3_FieldInfo = 
{
	"m_Arguments"/* name */
	, &ArgumentCache_t1074_0_0_1/* type */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1083, ___m_Arguments_3)/* data */
	, &PersistentCall_t1083__CustomAttributeCache_m_Arguments/* custom_attributes_cache */

};
extern Il2CppType UnityEventCallState_t1082_0_0_1;
extern CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_CallState;
FieldInfo PersistentCall_t1083____m_CallState_4_FieldInfo = 
{
	"m_CallState"/* name */
	, &UnityEventCallState_t1082_0_0_1/* type */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCall_t1083, ___m_CallState_4)/* data */
	, &PersistentCall_t1083__CustomAttributeCache_m_CallState/* custom_attributes_cache */

};
static FieldInfo* PersistentCall_t1083_FieldInfos[] =
{
	&PersistentCall_t1083____m_Target_0_FieldInfo,
	&PersistentCall_t1083____m_MethodName_1_FieldInfo,
	&PersistentCall_t1083____m_Mode_2_FieldInfo,
	&PersistentCall_t1083____m_Arguments_3_FieldInfo,
	&PersistentCall_t1083____m_CallState_4_FieldInfo,
	NULL
};
static PropertyInfo PersistentCall_t1083____target_PropertyInfo = 
{
	&PersistentCall_t1083_il2cpp_TypeInfo/* parent */
	, "target"/* name */
	, &PersistentCall_get_target_m6364_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo PersistentCall_t1083____methodName_PropertyInfo = 
{
	&PersistentCall_t1083_il2cpp_TypeInfo/* parent */
	, "methodName"/* name */
	, &PersistentCall_get_methodName_m6365_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo PersistentCall_t1083____mode_PropertyInfo = 
{
	&PersistentCall_t1083_il2cpp_TypeInfo/* parent */
	, "mode"/* name */
	, &PersistentCall_get_mode_m6366_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo PersistentCall_t1083____arguments_PropertyInfo = 
{
	&PersistentCall_t1083_il2cpp_TypeInfo/* parent */
	, "arguments"/* name */
	, &PersistentCall_get_arguments_m6367_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* PersistentCall_t1083_PropertyInfos[] =
{
	&PersistentCall_t1083____target_PropertyInfo,
	&PersistentCall_t1083____methodName_PropertyInfo,
	&PersistentCall_t1083____mode_PropertyInfo,
	&PersistentCall_t1083____arguments_PropertyInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCall::.ctor()
MethodInfo PersistentCall__ctor_m6363_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PersistentCall__ctor_m6363/* method */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1508/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t117_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
MethodInfo PersistentCall_get_target_m6364_MethodInfo = 
{
	"get_target"/* name */
	, (methodPointerType)&PersistentCall_get_target_m6364/* method */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* declaring_type */
	, &Object_t117_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1509/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.PersistentCall::get_methodName()
MethodInfo PersistentCall_get_methodName_m6365_MethodInfo = 
{
	"get_methodName"/* name */
	, (methodPointerType)&PersistentCall_get_methodName_m6365/* method */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1510/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PersistentListenerMode_t1073_0_0_0;
extern void* RuntimeInvoker_PersistentListenerMode_t1073 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
MethodInfo PersistentCall_get_mode_m6366_MethodInfo = 
{
	"get_mode"/* name */
	, (methodPointerType)&PersistentCall_get_mode_m6366/* method */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* declaring_type */
	, &PersistentListenerMode_t1073_0_0_0/* return_type */
	, RuntimeInvoker_PersistentListenerMode_t1073/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1511/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ArgumentCache_t1074_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
MethodInfo PersistentCall_get_arguments_m6367_MethodInfo = 
{
	"get_arguments"/* name */
	, (methodPointerType)&PersistentCall_get_arguments_m6367/* method */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* declaring_type */
	, &ArgumentCache_t1074_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1512/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
MethodInfo PersistentCall_IsValid_m6368_MethodInfo = 
{
	"IsValid"/* name */
	, (methodPointerType)&PersistentCall_IsValid_m6368/* method */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1513/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityEventBase_t1084_0_0_0;
extern Il2CppType UnityEventBase_t1084_0_0_0;
static ParameterInfo PersistentCall_t1083_PersistentCall_GetRuntimeCall_m6369_ParameterInfos[] = 
{
	{"theEvent", 0, 134219326, &EmptyCustomAttributesCache, &UnityEventBase_t1084_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
MethodInfo PersistentCall_GetRuntimeCall_m6369_MethodInfo = 
{
	"GetRuntimeCall"/* name */
	, (methodPointerType)&PersistentCall_GetRuntimeCall_m6369/* method */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1075_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, PersistentCall_t1083_PersistentCall_GetRuntimeCall_m6369_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1514/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType ArgumentCache_t1074_0_0_0;
static ParameterInfo PersistentCall_t1083_PersistentCall_GetObjectCall_m6370_ParameterInfos[] = 
{
	{"target", 0, 134219327, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"method", 1, 134219328, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"arguments", 2, 134219329, &EmptyCustomAttributesCache, &ArgumentCache_t1074_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
MethodInfo PersistentCall_GetObjectCall_m6370_MethodInfo = 
{
	"GetObjectCall"/* name */
	, (methodPointerType)&PersistentCall_GetObjectCall_m6370/* method */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1075_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, PersistentCall_t1083_PersistentCall_GetObjectCall_m6370_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1515/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PersistentCall_t1083_MethodInfos[] =
{
	&PersistentCall__ctor_m6363_MethodInfo,
	&PersistentCall_get_target_m6364_MethodInfo,
	&PersistentCall_get_methodName_m6365_MethodInfo,
	&PersistentCall_get_mode_m6366_MethodInfo,
	&PersistentCall_get_arguments_m6367_MethodInfo,
	&PersistentCall_IsValid_m6368_MethodInfo,
	&PersistentCall_GetRuntimeCall_m6369_MethodInfo,
	&PersistentCall_GetObjectCall_m6370_MethodInfo,
	NULL
};
static MethodInfo* PersistentCall_t1083_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
void PersistentCall_t1083_CustomAttributesCacheGenerator_m_Target(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t420 * tmp;
		tmp = (FormerlySerializedAsAttribute_t420 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m1900(tmp, il2cpp_codegen_string_new_wrapper("instance"), &FormerlySerializedAsAttribute__ctor_m1900_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void PersistentCall_t1083_CustomAttributesCacheGenerator_m_MethodName(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t420 * tmp;
		tmp = (FormerlySerializedAsAttribute_t420 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m1900(tmp, il2cpp_codegen_string_new_wrapper("methodName"), &FormerlySerializedAsAttribute__ctor_m1900_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void PersistentCall_t1083_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t420 * tmp;
		tmp = (FormerlySerializedAsAttribute_t420 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m1900(tmp, il2cpp_codegen_string_new_wrapper("mode"), &FormerlySerializedAsAttribute__ctor_m1900_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void PersistentCall_t1083_CustomAttributesCacheGenerator_m_Arguments(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t420 * tmp;
		tmp = (FormerlySerializedAsAttribute_t420 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m1900(tmp, il2cpp_codegen_string_new_wrapper("arguments"), &FormerlySerializedAsAttribute__ctor_m1900_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void PersistentCall_t1083_CustomAttributesCacheGenerator_m_CallState(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t420 * tmp;
		tmp = (FormerlySerializedAsAttribute_t420 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m1900(tmp, il2cpp_codegen_string_new_wrapper("m_Enabled"), &FormerlySerializedAsAttribute__ctor_m1900_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t420 * tmp;
		tmp = (FormerlySerializedAsAttribute_t420 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m1900(tmp, il2cpp_codegen_string_new_wrapper("enabled"), &FormerlySerializedAsAttribute__ctor_m1900_MethodInfo);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_Target = {
2,
NULL,
&PersistentCall_t1083_CustomAttributesCacheGenerator_m_Target
};
CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_MethodName = {
2,
NULL,
&PersistentCall_t1083_CustomAttributesCacheGenerator_m_MethodName
};
CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_Mode = {
2,
NULL,
&PersistentCall_t1083_CustomAttributesCacheGenerator_m_Mode
};
CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_Arguments = {
2,
NULL,
&PersistentCall_t1083_CustomAttributesCacheGenerator_m_Arguments
};
CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_CallState = {
3,
NULL,
&PersistentCall_t1083_CustomAttributesCacheGenerator_m_CallState
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType PersistentCall_t1083_0_0_0;
extern Il2CppType PersistentCall_t1083_1_0_0;
struct PersistentCall_t1083;
extern CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_Target;
extern CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_MethodName;
extern CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_Mode;
extern CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_Arguments;
extern CustomAttributesCache PersistentCall_t1083__CustomAttributeCache_m_CallState;
TypeInfo PersistentCall_t1083_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCall"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentCall_t1083_MethodInfos/* methods */
	, PersistentCall_t1083_PropertyInfos/* properties */
	, PersistentCall_t1083_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, PersistentCall_t1083_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &PersistentCall_t1083_il2cpp_TypeInfo/* cast_class */
	, &PersistentCall_t1083_0_0_0/* byval_arg */
	, &PersistentCall_t1083_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCall_t1083)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 8/* method_count */
	, 4/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo PersistentCallGroup_t1086_il2cpp_TypeInfo;
// UnityEngine.Events.PersistentCallGroup
#include "UnityEngine_UnityEngine_Events_PersistentCallGroupMethodDeclarations.h"

// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
#include "mscorlib_System_Collections_Generic_List_1_gen_52.h"
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallList.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_24.h"
extern TypeInfo List_1_t1085_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t1193_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
#include "mscorlib_System_Collections_Generic_List_1_gen_52MethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_24MethodDeclarations.h"
// UnityEngine.Events.InvokableCallList
#include "UnityEngine_UnityEngine_Events_InvokableCallListMethodDeclarations.h"
extern MethodInfo List_1__ctor_m6565_MethodInfo;
extern MethodInfo List_1_GetEnumerator_m6566_MethodInfo;
extern MethodInfo Enumerator_get_Current_m6567_MethodInfo;
extern MethodInfo InvokableCallList_AddPersistentInvokableCall_m6374_MethodInfo;
extern MethodInfo Enumerator_MoveNext_m6568_MethodInfo;
extern MethodInfo IDisposable_Dispose_m459_MethodInfo;


// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern MethodInfo PersistentCallGroup__ctor_m6371_MethodInfo;
 void PersistentCallGroup__ctor_m6371 (PersistentCallGroup_t1086 * __this, MethodInfo* method){
	{
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&List_1_t1085_il2cpp_TypeInfo));
		List_1_t1085 * L_0 = (List_1_t1085 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t1085_il2cpp_TypeInfo));
		List_1__ctor_m6565(L_0, /*hidden argument*/&List_1__ctor_m6565_MethodInfo);
		__this->___m_Calls_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern MethodInfo PersistentCallGroup_Initialize_m6372_MethodInfo;
 void PersistentCallGroup_Initialize_m6372 (PersistentCallGroup_t1086 * __this, InvokableCallList_t1087 * ___invokableList, UnityEventBase_t1084 * ___unityEventBase, MethodInfo* method){
	PersistentCall_t1083 * V_0 = {0};
	Enumerator_t1193  V_1 = {0};
	BaseInvokableCall_t1075 * V_2 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t152 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t152 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		List_1_t1085 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		Enumerator_t1193  L_1 = List_1_GetEnumerator_m6566(L_0, /*hidden argument*/&List_1_GetEnumerator_m6566_MethodInfo);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003e;
		}

IL_0011:
		{
			PersistentCall_t1083 * L_2 = Enumerator_get_Current_m6567((&V_1), /*hidden argument*/&Enumerator_get_Current_m6567_MethodInfo);
			V_0 = L_2;
			NullCheck(V_0);
			bool L_3 = PersistentCall_IsValid_m6368(V_0, /*hidden argument*/&PersistentCall_IsValid_m6368_MethodInfo);
			if (L_3)
			{
				goto IL_0029;
			}
		}

IL_0024:
		{
			goto IL_003e;
		}

IL_0029:
		{
			NullCheck(V_0);
			BaseInvokableCall_t1075 * L_4 = PersistentCall_GetRuntimeCall_m6369(V_0, ___unityEventBase, /*hidden argument*/&PersistentCall_GetRuntimeCall_m6369_MethodInfo);
			V_2 = L_4;
			if (!V_2)
			{
				goto IL_003e;
			}
		}

IL_0037:
		{
			NullCheck(___invokableList);
			InvokableCallList_AddPersistentInvokableCall_m6374(___invokableList, V_2, /*hidden argument*/&InvokableCallList_AddPersistentInvokableCall_m6374_MethodInfo);
		}

IL_003e:
		{
			bool L_5 = Enumerator_MoveNext_m6568((&V_1), /*hidden argument*/&Enumerator_MoveNext_m6568_MethodInfo);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_004a:
		{
			// IL_004a: leave IL_005b
			leaveInstructions[0] = 0x5B; // 1
			THROW_SENTINEL(IL_005b);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_004f;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t152 *)e.ex;
		goto IL_004f;
	}

IL_004f:
	{ // begin finally (depth: 1)
		Enumerator_t1193  L_6 = V_1;
		Object_t * L_7 = Box(InitializedTypeInfo(&Enumerator_t1193_il2cpp_TypeInfo), &L_6);
		NullCheck(L_7);
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m459_MethodInfo, L_7);
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x5B:
				goto IL_005b;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t152 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_005b:
	{
		return;
	}
}
// Metadata Definition UnityEngine.Events.PersistentCallGroup
extern Il2CppType List_1_t1085_0_0_1;
extern CustomAttributesCache PersistentCallGroup_t1086__CustomAttributeCache_m_Calls;
FieldInfo PersistentCallGroup_t1086____m_Calls_0_FieldInfo = 
{
	"m_Calls"/* name */
	, &List_1_t1085_0_0_1/* type */
	, &PersistentCallGroup_t1086_il2cpp_TypeInfo/* parent */
	, offsetof(PersistentCallGroup_t1086, ___m_Calls_0)/* data */
	, &PersistentCallGroup_t1086__CustomAttributeCache_m_Calls/* custom_attributes_cache */

};
static FieldInfo* PersistentCallGroup_t1086_FieldInfos[] =
{
	&PersistentCallGroup_t1086____m_Calls_0_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
MethodInfo PersistentCallGroup__ctor_m6371_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&PersistentCallGroup__ctor_m6371/* method */
	, &PersistentCallGroup_t1086_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1516/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType InvokableCallList_t1087_0_0_0;
extern Il2CppType InvokableCallList_t1087_0_0_0;
extern Il2CppType UnityEventBase_t1084_0_0_0;
static ParameterInfo PersistentCallGroup_t1086_PersistentCallGroup_Initialize_m6372_ParameterInfos[] = 
{
	{"invokableList", 0, 134219330, &EmptyCustomAttributesCache, &InvokableCallList_t1087_0_0_0},
	{"unityEventBase", 1, 134219331, &EmptyCustomAttributesCache, &UnityEventBase_t1084_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
MethodInfo PersistentCallGroup_Initialize_m6372_MethodInfo = 
{
	"Initialize"/* name */
	, (methodPointerType)&PersistentCallGroup_Initialize_m6372/* method */
	, &PersistentCallGroup_t1086_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, PersistentCallGroup_t1086_PersistentCallGroup_Initialize_m6372_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1517/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* PersistentCallGroup_t1086_MethodInfos[] =
{
	&PersistentCallGroup__ctor_m6371_MethodInfo,
	&PersistentCallGroup_Initialize_m6372_MethodInfo,
	NULL
};
static MethodInfo* PersistentCallGroup_t1086_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
void PersistentCallGroup_t1086_CustomAttributesCacheGenerator_m_Calls(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t420 * tmp;
		tmp = (FormerlySerializedAsAttribute_t420 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m1900(tmp, il2cpp_codegen_string_new_wrapper("m_Listeners"), &FormerlySerializedAsAttribute__ctor_m1900_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache PersistentCallGroup_t1086__CustomAttributeCache_m_Calls = {
2,
NULL,
&PersistentCallGroup_t1086_CustomAttributesCacheGenerator_m_Calls
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType PersistentCallGroup_t1086_0_0_0;
extern Il2CppType PersistentCallGroup_t1086_1_0_0;
struct PersistentCallGroup_t1086;
extern CustomAttributesCache PersistentCallGroup_t1086__CustomAttributeCache_m_Calls;
TypeInfo PersistentCallGroup_t1086_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "PersistentCallGroup"/* name */
	, "UnityEngine.Events"/* namespaze */
	, PersistentCallGroup_t1086_MethodInfos/* methods */
	, NULL/* properties */
	, PersistentCallGroup_t1086_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &PersistentCallGroup_t1086_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, PersistentCallGroup_t1086_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &PersistentCallGroup_t1086_il2cpp_TypeInfo/* cast_class */
	, &PersistentCallGroup_t1086_0_0_0/* byval_arg */
	, &PersistentCallGroup_t1086_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (PersistentCallGroup_t1086)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056768/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InvokableCallList_t1087_il2cpp_TypeInfo;

// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Collections_Generic_List_1_gen_53.h"
// System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Predicate_1_gen_3.h"
extern TypeInfo List_1_t1088_il2cpp_TypeInfo;
extern TypeInfo Predicate_1_t1194_il2cpp_TypeInfo;
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Collections_Generic_List_1_gen_53MethodDeclarations.h"
// System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
#include "mscorlib_System_Predicate_1_gen_3MethodDeclarations.h"
extern MethodInfo List_1__ctor_m6569_MethodInfo;
extern MethodInfo List_1_Add_m6570_MethodInfo;
extern MethodInfo List_1_get_Item_m6571_MethodInfo;
extern MethodInfo BaseInvokableCall_Find_m6539_MethodInfo;
extern MethodInfo List_1_get_Count_m6572_MethodInfo;
extern MethodInfo List_1_Contains_m6573_MethodInfo;
extern MethodInfo Predicate_1__ctor_m6574_MethodInfo;
extern MethodInfo List_1_RemoveAll_m6575_MethodInfo;
extern MethodInfo List_1_Clear_m6576_MethodInfo;
extern MethodInfo List_1_AddRange_m6577_MethodInfo;
extern MethodInfo BaseInvokableCall_Invoke_m6537_MethodInfo;


// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern MethodInfo InvokableCallList__ctor_m6373_MethodInfo;
 void InvokableCallList__ctor_m6373 (InvokableCallList_t1087 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&List_1_t1088_il2cpp_TypeInfo));
		List_1_t1088 * L_0 = (List_1_t1088 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t1088_il2cpp_TypeInfo));
		List_1__ctor_m6569(L_0, /*hidden argument*/&List_1__ctor_m6569_MethodInfo);
		__this->___m_PersistentCalls_0 = L_0;
		List_1_t1088 * L_1 = (List_1_t1088 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t1088_il2cpp_TypeInfo));
		List_1__ctor_m6569(L_1, /*hidden argument*/&List_1__ctor_m6569_MethodInfo);
		__this->___m_RuntimeCalls_1 = L_1;
		List_1_t1088 * L_2 = (List_1_t1088 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t1088_il2cpp_TypeInfo));
		List_1__ctor_m6569(L_2, /*hidden argument*/&List_1__ctor_m6569_MethodInfo);
		__this->___m_ExecutingCalls_2 = L_2;
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
 void InvokableCallList_AddPersistentInvokableCall_m6374 (InvokableCallList_t1087 * __this, BaseInvokableCall_t1075 * ___call, MethodInfo* method){
	{
		List_1_t1088 * L_0 = (__this->___m_PersistentCalls_0);
		NullCheck(L_0);
		VirtActionInvoker1< BaseInvokableCall_t1075 * >::Invoke(&List_1_Add_m6570_MethodInfo, L_0, ___call);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern MethodInfo InvokableCallList_AddListener_m6375_MethodInfo;
 void InvokableCallList_AddListener_m6375 (InvokableCallList_t1087 * __this, BaseInvokableCall_t1075 * ___call, MethodInfo* method){
	{
		List_1_t1088 * L_0 = (__this->___m_RuntimeCalls_1);
		NullCheck(L_0);
		VirtActionInvoker1< BaseInvokableCall_t1075 * >::Invoke(&List_1_Add_m6570_MethodInfo, L_0, ___call);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern MethodInfo InvokableCallList_RemoveListener_m6376_MethodInfo;
 void InvokableCallList_RemoveListener_m6376 (InvokableCallList_t1087 * __this, Object_t * ___targetObj, MethodInfo_t142 * ___method, MethodInfo* method){
	List_1_t1088 * V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&List_1_t1088_il2cpp_TypeInfo));
		List_1_t1088 * L_0 = (List_1_t1088 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t1088_il2cpp_TypeInfo));
		List_1__ctor_m6569(L_0, /*hidden argument*/&List_1__ctor_m6569_MethodInfo);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003b;
	}

IL_000d:
	{
		List_1_t1088 * L_1 = (__this->___m_RuntimeCalls_1);
		NullCheck(L_1);
		BaseInvokableCall_t1075 * L_2 = (BaseInvokableCall_t1075 *)VirtFuncInvoker1< BaseInvokableCall_t1075 *, int32_t >::Invoke(&List_1_get_Item_m6571_MethodInfo, L_1, V_1);
		NullCheck(L_2);
		bool L_3 = (bool)VirtFuncInvoker2< bool, Object_t *, MethodInfo_t142 * >::Invoke(&BaseInvokableCall_Find_m6539_MethodInfo, L_2, ___targetObj, ___method);
		if (!L_3)
		{
			goto IL_0037;
		}
	}
	{
		List_1_t1088 * L_4 = (__this->___m_RuntimeCalls_1);
		NullCheck(L_4);
		BaseInvokableCall_t1075 * L_5 = (BaseInvokableCall_t1075 *)VirtFuncInvoker1< BaseInvokableCall_t1075 *, int32_t >::Invoke(&List_1_get_Item_m6571_MethodInfo, L_4, V_1);
		NullCheck(V_0);
		VirtActionInvoker1< BaseInvokableCall_t1075 * >::Invoke(&List_1_Add_m6570_MethodInfo, V_0, L_5);
	}

IL_0037:
	{
		V_1 = ((int32_t)(V_1+1));
	}

IL_003b:
	{
		List_1_t1088 * L_6 = (__this->___m_RuntimeCalls_1);
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&List_1_get_Count_m6572_MethodInfo, L_6);
		if ((((int32_t)V_1) < ((int32_t)L_7)))
		{
			goto IL_000d;
		}
	}
	{
		List_1_t1088 * L_8 = (__this->___m_RuntimeCalls_1);
		List_1_t1088 * L_9 = V_0;
		IntPtr_t121 L_10 = { GetVirtualMethodInfo(L_9, &List_1_Contains_m6573_MethodInfo) };
		Predicate_1_t1194 * L_11 = (Predicate_1_t1194 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Predicate_1_t1194_il2cpp_TypeInfo));
		Predicate_1__ctor_m6574(L_11, L_9, L_10, /*hidden argument*/&Predicate_1__ctor_m6574_MethodInfo);
		NullCheck(L_8);
		List_1_RemoveAll_m6575(L_8, L_11, /*hidden argument*/&List_1_RemoveAll_m6575_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern MethodInfo InvokableCallList_ClearPersistent_m6377_MethodInfo;
 void InvokableCallList_ClearPersistent_m6377 (InvokableCallList_t1087 * __this, MethodInfo* method){
	{
		List_1_t1088 * L_0 = (__this->___m_PersistentCalls_0);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(&List_1_Clear_m6576_MethodInfo, L_0);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
extern MethodInfo InvokableCallList_Invoke_m6378_MethodInfo;
 void InvokableCallList_Invoke_m6378 (InvokableCallList_t1087 * __this, ObjectU5BU5D_t115* ___parameters, MethodInfo* method){
	int32_t V_0 = 0;
	{
		List_1_t1088 * L_0 = (__this->___m_ExecutingCalls_2);
		List_1_t1088 * L_1 = (__this->___m_PersistentCalls_0);
		NullCheck(L_0);
		List_1_AddRange_m6577(L_0, L_1, /*hidden argument*/&List_1_AddRange_m6577_MethodInfo);
		List_1_t1088 * L_2 = (__this->___m_ExecutingCalls_2);
		List_1_t1088 * L_3 = (__this->___m_RuntimeCalls_1);
		NullCheck(L_2);
		List_1_AddRange_m6577(L_2, L_3, /*hidden argument*/&List_1_AddRange_m6577_MethodInfo);
		V_0 = 0;
		goto IL_003f;
	}

IL_0029:
	{
		List_1_t1088 * L_4 = (__this->___m_ExecutingCalls_2);
		NullCheck(L_4);
		BaseInvokableCall_t1075 * L_5 = (BaseInvokableCall_t1075 *)VirtFuncInvoker1< BaseInvokableCall_t1075 *, int32_t >::Invoke(&List_1_get_Item_m6571_MethodInfo, L_4, V_0);
		NullCheck(L_5);
		VirtActionInvoker1< ObjectU5BU5D_t115* >::Invoke(&BaseInvokableCall_Invoke_m6537_MethodInfo, L_5, ___parameters);
		V_0 = ((int32_t)(V_0+1));
	}

IL_003f:
	{
		List_1_t1088 * L_6 = (__this->___m_ExecutingCalls_2);
		NullCheck(L_6);
		int32_t L_7 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&List_1_get_Count_m6572_MethodInfo, L_6);
		if ((((int32_t)V_0) < ((int32_t)L_7)))
		{
			goto IL_0029;
		}
	}
	{
		List_1_t1088 * L_8 = (__this->___m_ExecutingCalls_2);
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(&List_1_Clear_m6576_MethodInfo, L_8);
		return;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCallList
extern Il2CppType List_1_t1088_0_0_33;
FieldInfo InvokableCallList_t1087____m_PersistentCalls_0_FieldInfo = 
{
	"m_PersistentCalls"/* name */
	, &List_1_t1088_0_0_33/* type */
	, &InvokableCallList_t1087_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCallList_t1087, ___m_PersistentCalls_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType List_1_t1088_0_0_33;
FieldInfo InvokableCallList_t1087____m_RuntimeCalls_1_FieldInfo = 
{
	"m_RuntimeCalls"/* name */
	, &List_1_t1088_0_0_33/* type */
	, &InvokableCallList_t1087_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCallList_t1087, ___m_RuntimeCalls_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType List_1_t1088_0_0_33;
FieldInfo InvokableCallList_t1087____m_ExecutingCalls_2_FieldInfo = 
{
	"m_ExecutingCalls"/* name */
	, &List_1_t1088_0_0_33/* type */
	, &InvokableCallList_t1087_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCallList_t1087, ___m_ExecutingCalls_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCallList_t1087_FieldInfos[] =
{
	&InvokableCallList_t1087____m_PersistentCalls_0_FieldInfo,
	&InvokableCallList_t1087____m_RuntimeCalls_1_FieldInfo,
	&InvokableCallList_t1087____m_ExecutingCalls_2_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
MethodInfo InvokableCallList__ctor_m6373_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCallList__ctor_m6373/* method */
	, &InvokableCallList_t1087_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1518/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
static ParameterInfo InvokableCallList_t1087_InvokableCallList_AddPersistentInvokableCall_m6374_ParameterInfos[] = 
{
	{"call", 0, 134219332, &EmptyCustomAttributesCache, &BaseInvokableCall_t1075_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
MethodInfo InvokableCallList_AddPersistentInvokableCall_m6374_MethodInfo = 
{
	"AddPersistentInvokableCall"/* name */
	, (methodPointerType)&InvokableCallList_AddPersistentInvokableCall_m6374/* method */
	, &InvokableCallList_t1087_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCallList_t1087_InvokableCallList_AddPersistentInvokableCall_m6374_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1519/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
static ParameterInfo InvokableCallList_t1087_InvokableCallList_AddListener_m6375_ParameterInfos[] = 
{
	{"call", 0, 134219333, &EmptyCustomAttributesCache, &BaseInvokableCall_t1075_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
MethodInfo InvokableCallList_AddListener_m6375_MethodInfo = 
{
	"AddListener"/* name */
	, (methodPointerType)&InvokableCallList_AddListener_m6375/* method */
	, &InvokableCallList_t1087_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCallList_t1087_InvokableCallList_AddListener_m6375_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1520/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCallList_t1087_InvokableCallList_RemoveListener_m6376_ParameterInfos[] = 
{
	{"targetObj", 0, 134219334, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219335, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCallList_RemoveListener_m6376_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&InvokableCallList_RemoveListener_m6376/* method */
	, &InvokableCallList_t1087_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCallList_t1087_InvokableCallList_RemoveListener_m6376_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1521/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
MethodInfo InvokableCallList_ClearPersistent_m6377_MethodInfo = 
{
	"ClearPersistent"/* name */
	, (methodPointerType)&InvokableCallList_ClearPersistent_m6377/* method */
	, &InvokableCallList_t1087_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1522/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCallList_t1087_InvokableCallList_Invoke_m6378_ParameterInfos[] = 
{
	{"parameters", 0, 134219336, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
MethodInfo InvokableCallList_Invoke_m6378_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCallList_Invoke_m6378/* method */
	, &InvokableCallList_t1087_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCallList_t1087_InvokableCallList_Invoke_m6378_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1523/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* InvokableCallList_t1087_MethodInfos[] =
{
	&InvokableCallList__ctor_m6373_MethodInfo,
	&InvokableCallList_AddPersistentInvokableCall_m6374_MethodInfo,
	&InvokableCallList_AddListener_m6375_MethodInfo,
	&InvokableCallList_RemoveListener_m6376_MethodInfo,
	&InvokableCallList_ClearPersistent_m6377_MethodInfo,
	&InvokableCallList_Invoke_m6378_MethodInfo,
	NULL
};
static MethodInfo* InvokableCallList_t1087_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCallList_t1087_1_0_0;
struct InvokableCallList_t1087;
TypeInfo InvokableCallList_t1087_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCallList"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCallList_t1087_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCallList_t1087_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCallList_t1087_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCallList_t1087_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCallList_t1087_il2cpp_TypeInfo/* cast_class */
	, &InvokableCallList_t1087_0_0_0/* byval_arg */
	, &InvokableCallList_t1087_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCallList_t1087)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 6/* method_count */
	, 0/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.Reflection.BindingFlags
#include "mscorlib_System_Reflection_BindingFlags.h"
// System.Reflection.Binder
#include "mscorlib_System_Reflection_Binder.h"
// System.Reflection.ParameterModifier
#include "mscorlib_System_Reflection_ParameterModifier.h"
extern TypeInfo Single_t105_il2cpp_TypeInfo;
extern TypeInfo BindingFlags_t143_il2cpp_TypeInfo;
extern TypeInfo Binder_t1162_il2cpp_TypeInfo;
extern TypeInfo ParameterModifierU5BU5D_t1163_il2cpp_TypeInfo;
extern TypeInfo ParameterModifier_t1164_il2cpp_TypeInfo;
extern MethodInfo Type_get_AssemblyQualifiedName_m6578_MethodInfo;
extern MethodInfo UnityEventBase_DirtyPersistentCalls_m6382_MethodInfo;
extern MethodInfo UnityEventBase_FindMethod_m6381_MethodInfo;
extern MethodInfo UnityEventBase_FindMethod_Impl_m6579_MethodInfo;
extern MethodInfo UnityEventBase_GetValidMethodInfo_m6387_MethodInfo;
extern MethodInfo UnityEventBase_RebuildPersistentCallsIfNeeded_m6383_MethodInfo;
extern MethodInfo Type_get_FullName_m6580_MethodInfo;
extern MethodInfo Type_GetMethod_m6581_MethodInfo;
extern MethodInfo Type_get_IsPrimitive_m6582_MethodInfo;
extern MethodInfo Type_get_BaseType_m6494_MethodInfo;


// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern MethodInfo UnityEventBase__ctor_m6379_MethodInfo;
 void UnityEventBase__ctor_m6379 (UnityEventBase_t1084 * __this, MethodInfo* method){
	{
		__this->___m_CallsDirty_3 = 1;
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		InvokableCallList_t1087 * L_0 = (InvokableCallList_t1087 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvokableCallList_t1087_il2cpp_TypeInfo));
		InvokableCallList__ctor_m6373(L_0, /*hidden argument*/&InvokableCallList__ctor_m6373_MethodInfo);
		__this->___m_Calls_0 = L_0;
		PersistentCallGroup_t1086 * L_1 = (PersistentCallGroup_t1086 *)il2cpp_codegen_object_new (InitializedTypeInfo(&PersistentCallGroup_t1086_il2cpp_TypeInfo));
		PersistentCallGroup__ctor_m6371(L_1, /*hidden argument*/&PersistentCallGroup__ctor_m6371_MethodInfo);
		__this->___m_PersistentCalls_1 = L_1;
		Type_t * L_2 = Object_GetType_m449(__this, /*hidden argument*/&Object_GetType_m449_MethodInfo);
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Type_get_AssemblyQualifiedName_m6578_MethodInfo, L_2);
		__this->___m_TypeName_2 = L_3;
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1904_MethodInfo;
 void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1904 (UnityEventBase_t1084 * __this, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1905_MethodInfo;
 void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1905 (UnityEventBase_t1084 * __this, MethodInfo* method){
	{
		UnityEventBase_DirtyPersistentCalls_m6382(__this, /*hidden argument*/&UnityEventBase_DirtyPersistentCalls_m6382_MethodInfo);
		Type_t * L_0 = Object_GetType_m449(__this, /*hidden argument*/&Object_GetType_m449_MethodInfo);
		NullCheck(L_0);
		String_t* L_1 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Type_get_AssemblyQualifiedName_m6578_MethodInfo, L_0);
		__this->___m_TypeName_2 = L_1;
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
 MethodInfo_t142 * UnityEventBase_FindMethod_m6380 (UnityEventBase_t1084 * __this, PersistentCall_t1083 * ___call, MethodInfo* method){
	Type_t * V_0 = {0};
	Type_t * G_B3_0 = {0};
	Type_t * G_B2_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&Object_t117_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		V_0 = L_0;
		NullCheck(___call);
		ArgumentCache_t1074 * L_1 = PersistentCall_get_arguments_m6367(___call, /*hidden argument*/&PersistentCall_get_arguments_m6367_MethodInfo);
		NullCheck(L_1);
		String_t* L_2 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352(L_1, /*hidden argument*/&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_3 = String_IsNullOrEmpty_m2311(NULL /*static, unused*/, L_2, /*hidden argument*/&String_IsNullOrEmpty_m2311_MethodInfo);
		if (L_3)
		{
			goto IL_0043;
		}
	}
	{
		NullCheck(___call);
		ArgumentCache_t1074 * L_4 = PersistentCall_get_arguments_m6367(___call, /*hidden argument*/&PersistentCall_get_arguments_m6367_MethodInfo);
		NullCheck(L_4);
		String_t* L_5 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352(L_4, /*hidden argument*/&ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m6352_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_6 = Type_GetType_m6560(NULL /*static, unused*/, L_5, 0, /*hidden argument*/&Type_GetType_m6560_MethodInfo);
		Type_t * L_7 = L_6;
		G_B2_0 = L_7;
		if (L_7)
		{
			G_B3_0 = L_7;
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_8 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&Object_t117_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		G_B3_0 = L_8;
	}

IL_0042:
	{
		V_0 = G_B3_0;
	}

IL_0043:
	{
		NullCheck(___call);
		String_t* L_9 = PersistentCall_get_methodName_m6365(___call, /*hidden argument*/&PersistentCall_get_methodName_m6365_MethodInfo);
		NullCheck(___call);
		Object_t117 * L_10 = PersistentCall_get_target_m6364(___call, /*hidden argument*/&PersistentCall_get_target_m6364_MethodInfo);
		NullCheck(___call);
		int32_t L_11 = PersistentCall_get_mode_m6366(___call, /*hidden argument*/&PersistentCall_get_mode_m6366_MethodInfo);
		MethodInfo_t142 * L_12 = UnityEventBase_FindMethod_m6381(__this, L_9, L_10, L_11, V_0, /*hidden argument*/&UnityEventBase_FindMethod_m6381_MethodInfo);
		return L_12;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
 MethodInfo_t142 * UnityEventBase_FindMethod_m6381 (UnityEventBase_t1084 * __this, String_t* ___name, Object_t * ___listener, int32_t ___mode, Type_t * ___argumentType, MethodInfo* method){
	int32_t V_0 = {0};
	Type_t * G_B10_0 = {0};
	int32_t G_B10_1 = 0;
	TypeU5BU5D_t878* G_B10_2 = {0};
	TypeU5BU5D_t878* G_B10_3 = {0};
	String_t* G_B10_4 = {0};
	Object_t * G_B10_5 = {0};
	Type_t * G_B9_0 = {0};
	int32_t G_B9_1 = 0;
	TypeU5BU5D_t878* G_B9_2 = {0};
	TypeU5BU5D_t878* G_B9_3 = {0};
	String_t* G_B9_4 = {0};
	Object_t * G_B9_5 = {0};
	{
		V_0 = ___mode;
		if (V_0 == 0)
		{
			goto IL_0029;
		}
		if (V_0 == 1)
		{
			goto IL_0032;
		}
		if (V_0 == 2)
		{
			goto IL_00ac;
		}
		if (V_0 == 3)
		{
			goto IL_005b;
		}
		if (V_0 == 4)
		{
			goto IL_0040;
		}
		if (V_0 == 5)
		{
			goto IL_0091;
		}
		if (V_0 == 6)
		{
			goto IL_0076;
		}
	}
	{
		goto IL_00d0;
	}

IL_0029:
	{
		MethodInfo_t142 * L_0 = (MethodInfo_t142 *)VirtFuncInvoker2< MethodInfo_t142 *, String_t*, Object_t * >::Invoke(&UnityEventBase_FindMethod_Impl_m6579_MethodInfo, __this, ___name, ___listener);
		return L_0;
	}

IL_0032:
	{
		MethodInfo_t142 * L_1 = UnityEventBase_GetValidMethodInfo_m6387(NULL /*static, unused*/, ___listener, ___name, ((TypeU5BU5D_t878*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t878_il2cpp_TypeInfo), 0)), /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6387_MethodInfo);
		return L_1;
	}

IL_0040:
	{
		TypeU5BU5D_t878* L_2 = ((TypeU5BU5D_t878*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t878_il2cpp_TypeInfo), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_3 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&Single_t105_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_2, 0)) = (Type_t *)L_3;
		MethodInfo_t142 * L_4 = UnityEventBase_GetValidMethodInfo_m6387(NULL /*static, unused*/, ___listener, ___name, L_2, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6387_MethodInfo);
		return L_4;
	}

IL_005b:
	{
		TypeU5BU5D_t878* L_5 = ((TypeU5BU5D_t878*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t878_il2cpp_TypeInfo), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_6 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&Int32_t93_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 0);
		ArrayElementTypeCheck (L_5, L_6);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_5, 0)) = (Type_t *)L_6;
		MethodInfo_t142 * L_7 = UnityEventBase_GetValidMethodInfo_m6387(NULL /*static, unused*/, ___listener, ___name, L_5, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6387_MethodInfo);
		return L_7;
	}

IL_0076:
	{
		TypeU5BU5D_t878* L_8 = ((TypeU5BU5D_t878*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t878_il2cpp_TypeInfo), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_9 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&Boolean_t106_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, L_9);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_8, 0)) = (Type_t *)L_9;
		MethodInfo_t142 * L_10 = UnityEventBase_GetValidMethodInfo_m6387(NULL /*static, unused*/, ___listener, ___name, L_8, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6387_MethodInfo);
		return L_10;
	}

IL_0091:
	{
		TypeU5BU5D_t878* L_11 = ((TypeU5BU5D_t878*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t878_il2cpp_TypeInfo), 1));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_12 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&String_t_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, L_12);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_11, 0)) = (Type_t *)L_12;
		MethodInfo_t142 * L_13 = UnityEventBase_GetValidMethodInfo_m6387(NULL /*static, unused*/, ___listener, ___name, L_11, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6387_MethodInfo);
		return L_13;
	}

IL_00ac:
	{
		TypeU5BU5D_t878* L_14 = ((TypeU5BU5D_t878*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t878_il2cpp_TypeInfo), 1));
		Type_t * L_15 = ___argumentType;
		G_B9_0 = L_15;
		G_B9_1 = 0;
		G_B9_2 = L_14;
		G_B9_3 = L_14;
		G_B9_4 = ___name;
		G_B9_5 = ___listener;
		if (L_15)
		{
			G_B10_0 = L_15;
			G_B10_1 = 0;
			G_B10_2 = L_14;
			G_B10_3 = L_14;
			G_B10_4 = ___name;
			G_B10_5 = ___listener;
			goto IL_00c9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_16 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&Object_t117_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		G_B10_0 = L_16;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		G_B10_3 = G_B9_3;
		G_B10_4 = G_B9_4;
		G_B10_5 = G_B9_5;
	}

IL_00c9:
	{
		NullCheck(G_B10_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B10_2, G_B10_1);
		ArrayElementTypeCheck (G_B10_2, G_B10_0);
		*((Type_t **)(Type_t **)SZArrayLdElema(G_B10_2, G_B10_1)) = (Type_t *)G_B10_0;
		MethodInfo_t142 * L_17 = UnityEventBase_GetValidMethodInfo_m6387(NULL /*static, unused*/, G_B10_5, G_B10_4, G_B10_3, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6387_MethodInfo);
		return L_17;
	}

IL_00d0:
	{
		return (MethodInfo_t142 *)NULL;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
 void UnityEventBase_DirtyPersistentCalls_m6382 (UnityEventBase_t1084 * __this, MethodInfo* method){
	{
		InvokableCallList_t1087 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		InvokableCallList_ClearPersistent_m6377(L_0, /*hidden argument*/&InvokableCallList_ClearPersistent_m6377_MethodInfo);
		__this->___m_CallsDirty_3 = 1;
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
 void UnityEventBase_RebuildPersistentCallsIfNeeded_m6383 (UnityEventBase_t1084 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___m_CallsDirty_3);
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		PersistentCallGroup_t1086 * L_1 = (__this->___m_PersistentCalls_1);
		InvokableCallList_t1087 * L_2 = (__this->___m_Calls_0);
		NullCheck(L_1);
		PersistentCallGroup_Initialize_m6372(L_1, L_2, __this, /*hidden argument*/&PersistentCallGroup_Initialize_m6372_MethodInfo);
		__this->___m_CallsDirty_3 = 0;
	}

IL_0024:
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern MethodInfo UnityEventBase_AddCall_m6384_MethodInfo;
 void UnityEventBase_AddCall_m6384 (UnityEventBase_t1084 * __this, BaseInvokableCall_t1075 * ___call, MethodInfo* method){
	{
		InvokableCallList_t1087 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		InvokableCallList_AddListener_m6375(L_0, ___call, /*hidden argument*/&InvokableCallList_AddListener_m6375_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern MethodInfo UnityEventBase_RemoveListener_m6385_MethodInfo;
 void UnityEventBase_RemoveListener_m6385 (UnityEventBase_t1084 * __this, Object_t * ___targetObj, MethodInfo_t142 * ___method, MethodInfo* method){
	{
		InvokableCallList_t1087 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		InvokableCallList_RemoveListener_m6376(L_0, ___targetObj, ___method, /*hidden argument*/&InvokableCallList_RemoveListener_m6376_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern MethodInfo UnityEventBase_Invoke_m6386_MethodInfo;
 void UnityEventBase_Invoke_m6386 (UnityEventBase_t1084 * __this, ObjectU5BU5D_t115* ___parameters, MethodInfo* method){
	{
		UnityEventBase_RebuildPersistentCallsIfNeeded_m6383(__this, /*hidden argument*/&UnityEventBase_RebuildPersistentCallsIfNeeded_m6383_MethodInfo);
		InvokableCallList_t1087 * L_0 = (__this->___m_Calls_0);
		NullCheck(L_0);
		InvokableCallList_Invoke_m6378(L_0, ___parameters, /*hidden argument*/&InvokableCallList_Invoke_m6378_MethodInfo);
		return;
	}
}
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern MethodInfo UnityEventBase_ToString_m1903_MethodInfo;
 String_t* UnityEventBase_ToString_m1903 (UnityEventBase_t1084 * __this, MethodInfo* method){
	{
		String_t* L_0 = Object_ToString_m306(__this, /*hidden argument*/&Object_ToString_m306_MethodInfo);
		Type_t * L_1 = Object_GetType_m449(__this, /*hidden argument*/&Object_GetType_m449_MethodInfo);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Type_get_FullName_m6580_MethodInfo, L_1);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_3 = String_Concat_m418(NULL /*static, unused*/, L_0, (String_t*) &_stringLiteral149, L_2, /*hidden argument*/&String_Concat_m418_MethodInfo);
		return L_3;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
 MethodInfo_t142 * UnityEventBase_GetValidMethodInfo_m6387 (Object_t * __this/* static, unused */, Object_t * ___obj, String_t* ___functionName, TypeU5BU5D_t878* ___argumentTypes, MethodInfo* method){
	Type_t * V_0 = {0};
	MethodInfo_t142 * V_1 = {0};
	ParameterInfoU5BU5D_t1168* V_2 = {0};
	bool V_3 = false;
	int32_t V_4 = 0;
	ParameterInfo_t1169 * V_5 = {0};
	ParameterInfoU5BU5D_t1168* V_6 = {0};
	int32_t V_7 = 0;
	Type_t * V_8 = {0};
	Type_t * V_9 = {0};
	{
		NullCheck(___obj);
		Type_t * L_0 = Object_GetType_m449(___obj, /*hidden argument*/&Object_GetType_m449_MethodInfo);
		V_0 = L_0;
		goto IL_008e;
	}

IL_000c:
	{
		NullCheck(V_0);
		MethodInfo_t142 * L_1 = (MethodInfo_t142 *)VirtFuncInvoker5< MethodInfo_t142 *, String_t*, int32_t, Binder_t1162 *, TypeU5BU5D_t878*, ParameterModifierU5BU5D_t1163* >::Invoke(&Type_GetMethod_m6581_MethodInfo, V_0, ___functionName, ((int32_t)52), (Binder_t1162 *)NULL, ___argumentTypes, (ParameterModifierU5BU5D_t1163*)(ParameterModifierU5BU5D_t1163*)NULL);
		V_1 = L_1;
		if (!V_1)
		{
			goto IL_0087;
		}
	}
	{
		NullCheck(V_1);
		ParameterInfoU5BU5D_t1168* L_2 = (ParameterInfoU5BU5D_t1168*)VirtFuncInvoker0< ParameterInfoU5BU5D_t1168* >::Invoke(&MethodBase_GetParameters_m6523_MethodInfo, V_1);
		V_2 = L_2;
		V_3 = 1;
		V_4 = 0;
		V_6 = V_2;
		V_7 = 0;
		goto IL_0074;
	}

IL_0036:
	{
		NullCheck(V_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_6, V_7);
		int32_t L_3 = V_7;
		V_5 = (*(ParameterInfo_t1169 **)(ParameterInfo_t1169 **)SZArrayLdElema(V_6, L_3));
		NullCheck(___argumentTypes);
		IL2CPP_ARRAY_BOUNDS_CHECK(___argumentTypes, V_4);
		int32_t L_4 = V_4;
		V_8 = (*(Type_t **)(Type_t **)SZArrayLdElema(___argumentTypes, L_4));
		NullCheck(V_5);
		Type_t * L_5 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(&ParameterInfo_get_ParameterType_m6524_MethodInfo, V_5);
		V_9 = L_5;
		NullCheck(V_8);
		bool L_6 = (bool)VirtFuncInvoker0< bool >::Invoke(&Type_get_IsPrimitive_m6582_MethodInfo, V_8);
		NullCheck(V_9);
		bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(&Type_get_IsPrimitive_m6582_MethodInfo, V_9);
		V_3 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		if (V_3)
		{
			goto IL_0068;
		}
	}
	{
		goto IL_007f;
	}

IL_0068:
	{
		V_4 = ((int32_t)(V_4+1));
		V_7 = ((int32_t)(V_7+1));
	}

IL_0074:
	{
		NullCheck(V_6);
		if ((((int32_t)V_7) < ((int32_t)(((int32_t)(((Array_t *)V_6)->max_length))))))
		{
			goto IL_0036;
		}
	}

IL_007f:
	{
		if (!V_3)
		{
			goto IL_0087;
		}
	}
	{
		return V_1;
	}

IL_0087:
	{
		NullCheck(V_0);
		Type_t * L_8 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(&Type_get_BaseType_m6494_MethodInfo, V_0);
		V_0 = L_8;
	}

IL_008e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_9 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&Object_t_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		if ((((Type_t *)V_0) == ((Type_t *)L_9)))
		{
			goto IL_00a4;
		}
	}
	{
		if (V_0)
		{
			goto IL_000c;
		}
	}

IL_00a4:
	{
		return (MethodInfo_t142 *)NULL;
	}
}
// Metadata Definition UnityEngine.Events.UnityEventBase
extern Il2CppType InvokableCallList_t1087_0_0_1;
FieldInfo UnityEventBase_t1084____m_Calls_0_FieldInfo = 
{
	"m_Calls"/* name */
	, &InvokableCallList_t1087_0_0_1/* type */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventBase_t1084, ___m_Calls_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType PersistentCallGroup_t1086_0_0_1;
extern CustomAttributesCache UnityEventBase_t1084__CustomAttributeCache_m_PersistentCalls;
FieldInfo UnityEventBase_t1084____m_PersistentCalls_1_FieldInfo = 
{
	"m_PersistentCalls"/* name */
	, &PersistentCallGroup_t1086_0_0_1/* type */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventBase_t1084, ___m_PersistentCalls_1)/* data */
	, &UnityEventBase_t1084__CustomAttributeCache_m_PersistentCalls/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache UnityEventBase_t1084__CustomAttributeCache_m_TypeName;
FieldInfo UnityEventBase_t1084____m_TypeName_2_FieldInfo = 
{
	"m_TypeName"/* name */
	, &String_t_0_0_1/* type */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventBase_t1084, ___m_TypeName_2)/* data */
	, &UnityEventBase_t1084__CustomAttributeCache_m_TypeName/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo UnityEventBase_t1084____m_CallsDirty_3_FieldInfo = 
{
	"m_CallsDirty"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEventBase_t1084, ___m_CallsDirty_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEventBase_t1084_FieldInfos[] =
{
	&UnityEventBase_t1084____m_Calls_0_FieldInfo,
	&UnityEventBase_t1084____m_PersistentCalls_1_FieldInfo,
	&UnityEventBase_t1084____m_TypeName_2_FieldInfo,
	&UnityEventBase_t1084____m_CallsDirty_3_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
MethodInfo UnityEventBase__ctor_m6379_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEventBase__ctor_m6379/* method */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1524/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1904_MethodInfo = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize"/* name */
	, (methodPointerType)&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1904/* method */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1525/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1905_MethodInfo = 
{
	"UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize"/* name */
	, (methodPointerType)&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1905/* method */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1526/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEventBase_t1084_UnityEventBase_FindMethod_Impl_m6579_ParameterInfos[] = 
{
	{"name", 0, 134219337, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134219338, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t142_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEventBase_FindMethod_Impl_m6579_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t142_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1084_UnityEventBase_FindMethod_Impl_m6579_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1476/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1527/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo UnityEventBase_t1084_UnityEventBase_GetDelegate_m6555_ParameterInfos[] = 
{
	{"target", 0, 134219339, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219340, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEventBase_GetDelegate_m6555_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1075_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1084_UnityEventBase_GetDelegate_m6555_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1475/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1528/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType PersistentCall_t1083_0_0_0;
static ParameterInfo UnityEventBase_t1084_UnityEventBase_FindMethod_m6380_ParameterInfos[] = 
{
	{"call", 0, 134219341, &EmptyCustomAttributesCache, &PersistentCall_t1083_0_0_0},
};
extern Il2CppType MethodInfo_t142_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
MethodInfo UnityEventBase_FindMethod_m6380_MethodInfo = 
{
	"FindMethod"/* name */
	, (methodPointerType)&UnityEventBase_FindMethod_m6380/* method */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t142_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1084_UnityEventBase_FindMethod_m6380_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1529/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType PersistentListenerMode_t1073_0_0_0;
extern Il2CppType Type_t_0_0_0;
extern Il2CppType Type_t_0_0_0;
static ParameterInfo UnityEventBase_t1084_UnityEventBase_FindMethod_m6381_ParameterInfos[] = 
{
	{"name", 0, 134219342, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"listener", 1, 134219343, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"mode", 2, 134219344, &EmptyCustomAttributesCache, &PersistentListenerMode_t1073_0_0_0},
	{"argumentType", 3, 134219345, &EmptyCustomAttributesCache, &Type_t_0_0_0},
};
extern Il2CppType MethodInfo_t142_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
MethodInfo UnityEventBase_FindMethod_m6381_MethodInfo = 
{
	"FindMethod"/* name */
	, (methodPointerType)&UnityEventBase_FindMethod_m6381/* method */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t142_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t93_Object_t/* invoker_method */
	, UnityEventBase_t1084_UnityEventBase_FindMethod_m6381_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1530/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
MethodInfo UnityEventBase_DirtyPersistentCalls_m6382_MethodInfo = 
{
	"DirtyPersistentCalls"/* name */
	, (methodPointerType)&UnityEventBase_DirtyPersistentCalls_m6382/* method */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1531/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
MethodInfo UnityEventBase_RebuildPersistentCallsIfNeeded_m6383_MethodInfo = 
{
	"RebuildPersistentCallsIfNeeded"/* name */
	, (methodPointerType)&UnityEventBase_RebuildPersistentCallsIfNeeded_m6383/* method */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1532/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
static ParameterInfo UnityEventBase_t1084_UnityEventBase_AddCall_m6384_ParameterInfos[] = 
{
	{"call", 0, 134219346, &EmptyCustomAttributesCache, &BaseInvokableCall_t1075_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
MethodInfo UnityEventBase_AddCall_m6384_MethodInfo = 
{
	"AddCall"/* name */
	, (methodPointerType)&UnityEventBase_AddCall_m6384/* method */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityEventBase_t1084_UnityEventBase_AddCall_m6384_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1533/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo UnityEventBase_t1084_UnityEventBase_RemoveListener_m6385_ParameterInfos[] = 
{
	{"targetObj", 0, 134219347, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219348, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEventBase_RemoveListener_m6385_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&UnityEventBase_RemoveListener_m6385/* method */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1084_UnityEventBase_RemoveListener_m6385_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1534/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo UnityEventBase_t1084_UnityEventBase_Invoke_m6386_ParameterInfos[] = 
{
	{"parameters", 0, 134219349, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
MethodInfo UnityEventBase_Invoke_m6386_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEventBase_Invoke_m6386/* method */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityEventBase_t1084_UnityEventBase_Invoke_m6386_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 132/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1535/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngine.Events.UnityEventBase::ToString()
MethodInfo UnityEventBase_ToString_m1903_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&UnityEventBase_ToString_m1903/* method */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1536/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType TypeU5BU5D_t878_0_0_0;
extern Il2CppType TypeU5BU5D_t878_0_0_0;
static ParameterInfo UnityEventBase_t1084_UnityEventBase_GetValidMethodInfo_m6387_ParameterInfos[] = 
{
	{"obj", 0, 134219350, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"functionName", 1, 134219351, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"argumentTypes", 2, 134219352, &EmptyCustomAttributesCache, &TypeU5BU5D_t878_0_0_0},
};
extern Il2CppType MethodInfo_t142_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
MethodInfo UnityEventBase_GetValidMethodInfo_m6387_MethodInfo = 
{
	"GetValidMethodInfo"/* name */
	, (methodPointerType)&UnityEventBase_GetValidMethodInfo_m6387/* method */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t142_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEventBase_t1084_UnityEventBase_GetValidMethodInfo_m6387_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1537/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEventBase_t1084_MethodInfos[] =
{
	&UnityEventBase__ctor_m6379_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1904_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1905_MethodInfo,
	&UnityEventBase_FindMethod_Impl_m6579_MethodInfo,
	&UnityEventBase_GetDelegate_m6555_MethodInfo,
	&UnityEventBase_FindMethod_m6380_MethodInfo,
	&UnityEventBase_FindMethod_m6381_MethodInfo,
	&UnityEventBase_DirtyPersistentCalls_m6382_MethodInfo,
	&UnityEventBase_RebuildPersistentCallsIfNeeded_m6383_MethodInfo,
	&UnityEventBase_AddCall_m6384_MethodInfo,
	&UnityEventBase_RemoveListener_m6385_MethodInfo,
	&UnityEventBase_Invoke_m6386_MethodInfo,
	&UnityEventBase_ToString_m1903_MethodInfo,
	&UnityEventBase_GetValidMethodInfo_m6387_MethodInfo,
	NULL
};
static MethodInfo* UnityEventBase_t1084_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&UnityEventBase_ToString_m1903_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1904_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1905_MethodInfo,
	NULL,
	NULL,
};
extern TypeInfo ISerializationCallbackReceiver_t422_il2cpp_TypeInfo;
static TypeInfo* UnityEventBase_t1084_InterfacesTypeInfos[] = 
{
	&ISerializationCallbackReceiver_t422_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair UnityEventBase_t1084_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t422_il2cpp_TypeInfo, 4},
};
void UnityEventBase_t1084_CustomAttributesCacheGenerator_m_PersistentCalls(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t420 * tmp;
		tmp = (FormerlySerializedAsAttribute_t420 *)il2cpp_codegen_object_new (&FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo);
		FormerlySerializedAsAttribute__ctor_m1900(tmp, il2cpp_codegen_string_new_wrapper("m_PersistentListeners"), &FormerlySerializedAsAttribute__ctor_m1900_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void UnityEventBase_t1084_CustomAttributesCacheGenerator_m_TypeName(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache UnityEventBase_t1084__CustomAttributeCache_m_PersistentCalls = {
2,
NULL,
&UnityEventBase_t1084_CustomAttributesCacheGenerator_m_PersistentCalls
};
CustomAttributesCache UnityEventBase_t1084__CustomAttributeCache_m_TypeName = {
1,
NULL,
&UnityEventBase_t1084_CustomAttributesCacheGenerator_m_TypeName
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEventBase_t1084_1_0_0;
struct UnityEventBase_t1084;
extern CustomAttributesCache UnityEventBase_t1084__CustomAttributeCache_m_PersistentCalls;
extern CustomAttributesCache UnityEventBase_t1084__CustomAttributeCache_m_TypeName;
TypeInfo UnityEventBase_t1084_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEventBase"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEventBase_t1084_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEventBase_t1084_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* element_class */
	, UnityEventBase_t1084_InterfacesTypeInfos/* implemented_interfaces */
	, UnityEventBase_t1084_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* cast_class */
	, &UnityEventBase_t1084_0_0_0/* byval_arg */
	, &UnityEventBase_t1084_1_0_0/* this_arg */
	, UnityEventBase_t1084_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEventBase_t1084)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 14/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEvent.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityEvent_t269_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent
#include "UnityEngine_UnityEngine_Events_UnityEventMethodDeclarations.h"



// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern MethodInfo UnityEvent__ctor_m2064_MethodInfo;
 void UnityEvent__ctor_m2064 (UnityEvent_t269 * __this, MethodInfo* method){
	{
		__this->___m_InvokeArray_4 = ((ObjectU5BU5D_t115*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t115_il2cpp_TypeInfo), 0));
		UnityEventBase__ctor_m6379(__this, /*hidden argument*/&UnityEventBase__ctor_m6379_MethodInfo);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern MethodInfo UnityEvent_FindMethod_Impl_m2065_MethodInfo;
 MethodInfo_t142 * UnityEvent_FindMethod_Impl_m2065 (UnityEvent_t269 * __this, String_t* ___name, Object_t * ___targetObj, MethodInfo* method){
	{
		MethodInfo_t142 * L_0 = UnityEventBase_GetValidMethodInfo_m6387(NULL /*static, unused*/, ___targetObj, ___name, ((TypeU5BU5D_t878*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t878_il2cpp_TypeInfo), 0)), /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6387_MethodInfo);
		return L_0;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern MethodInfo UnityEvent_GetDelegate_m2066_MethodInfo;
 BaseInvokableCall_t1075 * UnityEvent_GetDelegate_m2066 (UnityEvent_t269 * __this, Object_t * ___target, MethodInfo_t142 * ___theFunction, MethodInfo* method){
	{
		InvokableCall_t1076 * L_0 = (InvokableCall_t1076 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvokableCall_t1076_il2cpp_TypeInfo));
		InvokableCall__ctor_m6360(L_0, ___target, ___theFunction, /*hidden argument*/&InvokableCall__ctor_m6360_MethodInfo);
		return L_0;
	}
}
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern MethodInfo UnityEvent_Invoke_m2069_MethodInfo;
 void UnityEvent_Invoke_m2069 (UnityEvent_t269 * __this, MethodInfo* method){
	{
		ObjectU5BU5D_t115* L_0 = (__this->___m_InvokeArray_4);
		UnityEventBase_Invoke_m6386(__this, L_0, /*hidden argument*/&UnityEventBase_Invoke_m6386_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo UnityEvent_t269____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &UnityEvent_t269_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEvent_t269, ___m_InvokeArray_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_t269_FieldInfos[] =
{
	&UnityEvent_t269____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::.ctor()
MethodInfo UnityEvent__ctor_m2064_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEvent__ctor_m2064/* method */
	, &UnityEvent_t269_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1538/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_t269_UnityEvent_FindMethod_Impl_m2065_ParameterInfos[] = 
{
	{"name", 0, 134219353, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134219354, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t142_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_FindMethod_Impl_m2065_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, (methodPointerType)&UnityEvent_FindMethod_Impl_m2065/* method */
	, &UnityEvent_t269_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t142_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_t269_UnityEvent_FindMethod_Impl_m2065_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1539/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo UnityEvent_t269_UnityEvent_GetDelegate_m2066_ParameterInfos[] = 
{
	{"target", 0, 134219355, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219356, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_GetDelegate_m2066_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_GetDelegate_m2066/* method */
	, &UnityEvent_t269_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1075_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_t269_UnityEvent_GetDelegate_m2066_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1540/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
MethodInfo UnityEvent_Invoke_m2069_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEvent_Invoke_m2069/* method */
	, &UnityEvent_t269_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1541/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_t269_MethodInfos[] =
{
	&UnityEvent__ctor_m2064_MethodInfo,
	&UnityEvent_FindMethod_Impl_m2065_MethodInfo,
	&UnityEvent_GetDelegate_m2066_MethodInfo,
	&UnityEvent_Invoke_m2069_MethodInfo,
	NULL
};
static MethodInfo* UnityEvent_t269_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&UnityEventBase_ToString_m1903_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1904_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1905_MethodInfo,
	&UnityEvent_FindMethod_Impl_m2065_MethodInfo,
	&UnityEvent_GetDelegate_m2066_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityEvent_t269_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t422_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_t269_0_0_0;
extern Il2CppType UnityEvent_t269_1_0_0;
struct UnityEvent_t269;
TypeInfo UnityEvent_t269_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_t269_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_t269_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_t269_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityEvent_t269_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityEvent_t269_il2cpp_TypeInfo/* cast_class */
	, &UnityEvent_t269_0_0_0/* byval_arg */
	, &UnityEvent_t269_1_0_0/* this_arg */
	, UnityEvent_t269_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_t269)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`1
#include "UnityEngine_UnityEngine_Events_UnityEvent_1.h"
extern Il2CppGenericContainer UnityEvent_1_t1089_Il2CppGenericContainer;
extern TypeInfo UnityEvent_1_t1089_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_1_t1089_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_1_t1089_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityEvent_1_t1089_Il2CppGenericParametersArray[1] = 
{
	&UnityEvent_1_t1089_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityEvent_1_t1089_il2cpp_TypeInfo;
Il2CppGenericContainer UnityEvent_1_t1089_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityEvent_1_t1089_il2cpp_TypeInfo, 1, 0, UnityEvent_1_t1089_Il2CppGenericParametersArray };
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`1::.ctor()
MethodInfo UnityEvent_1__ctor_m6583_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1089_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1542/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1196_0_0_0;
extern Il2CppType UnityAction_1_t1196_0_0_0;
static ParameterInfo UnityEvent_1_t1089_UnityEvent_1_AddListener_m6584_ParameterInfos[] = 
{
	{"call", 0, 134219357, &EmptyCustomAttributesCache, &UnityAction_1_t1196_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`1::AddListener(UnityEngine.Events.UnityAction`1<T0>)
MethodInfo UnityEvent_1_AddListener_m6584_MethodInfo = 
{
	"AddListener"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1089_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1089_UnityEvent_1_AddListener_m6584_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1543/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1196_0_0_0;
static ParameterInfo UnityEvent_1_t1089_UnityEvent_1_RemoveListener_m6585_ParameterInfos[] = 
{
	{"call", 0, 134219358, &EmptyCustomAttributesCache, &UnityAction_1_t1196_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`1::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
MethodInfo UnityEvent_1_RemoveListener_m6585_MethodInfo = 
{
	"RemoveListener"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1089_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1089_UnityEvent_1_RemoveListener_m6585_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1544/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_1_t1089_UnityEvent_1_FindMethod_Impl_m6586_ParameterInfos[] = 
{
	{"name", 0, 134219359, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134219360, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t142_0_0_0;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_1_FindMethod_Impl_m6586_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1089_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t142_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1089_UnityEvent_1_FindMethod_Impl_m6586_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1545/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo UnityEvent_1_t1089_UnityEvent_1_GetDelegate_m6587_ParameterInfos[] = 
{
	{"target", 0, 134219361, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219362, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_1_GetDelegate_m6587_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1089_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1075_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1089_UnityEvent_1_GetDelegate_m6587_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1546/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1196_0_0_0;
static ParameterInfo UnityEvent_1_t1089_UnityEvent_1_GetDelegate_m6588_ParameterInfos[] = 
{
	{"action", 0, 134219363, &EmptyCustomAttributesCache, &UnityAction_1_t1196_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
MethodInfo UnityEvent_1_GetDelegate_m6588_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1089_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1075_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1089_UnityEvent_1_GetDelegate_m6588_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1547/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityEvent_1_t1089_gp_0_0_0_0;
extern Il2CppType UnityEvent_1_t1089_gp_0_0_0_0;
static ParameterInfo UnityEvent_1_t1089_UnityEvent_1_Invoke_m6589_ParameterInfos[] = 
{
	{"arg0", 0, 134219364, &EmptyCustomAttributesCache, &UnityEvent_1_t1089_gp_0_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`1::Invoke(T0)
MethodInfo UnityEvent_1_Invoke_m6589_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityEvent_1_t1089_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_1_t1089_UnityEvent_1_Invoke_m6589_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1548/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_1_t1089_MethodInfos[] =
{
	&UnityEvent_1__ctor_m6583_MethodInfo,
	&UnityEvent_1_AddListener_m6584_MethodInfo,
	&UnityEvent_1_RemoveListener_m6585_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m6586_MethodInfo,
	&UnityEvent_1_GetDelegate_m6587_MethodInfo,
	&UnityEvent_1_GetDelegate_m6588_MethodInfo,
	&UnityEvent_1_Invoke_m6589_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo UnityEvent_1_t1089____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &UnityEvent_1_t1089_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_1_t1089_FieldInfos[] =
{
	&UnityEvent_1_t1089____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_1_t1089_0_0_0;
extern Il2CppType UnityEvent_1_t1089_1_0_0;
struct UnityEvent_1_t1089;
TypeInfo UnityEvent_1_t1089_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_1_t1089_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_1_t1089_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_1_t1089_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityEvent_1_t1089_0_0_0/* byval_arg */
	, &UnityEvent_1_t1089_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityEvent_1_t1089_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`2
#include "UnityEngine_UnityEngine_Events_UnityEvent_2.h"
extern Il2CppGenericContainer UnityEvent_2_t1090_Il2CppGenericContainer;
extern TypeInfo UnityEvent_2_t1090_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_2_t1090_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_2_t1090_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityEvent_2_t1090_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_2_t1090_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_2_t1090_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityEvent_2_t1090_Il2CppGenericParametersArray[2] = 
{
	&UnityEvent_2_t1090_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_2_t1090_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityEvent_2_t1090_il2cpp_TypeInfo;
Il2CppGenericContainer UnityEvent_2_t1090_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityEvent_2_t1090_il2cpp_TypeInfo, 2, 0, UnityEvent_2_t1090_Il2CppGenericParametersArray };
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`2::.ctor()
MethodInfo UnityEvent_2__ctor_m6590_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1090_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1549/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_2_t1090_UnityEvent_2_FindMethod_Impl_m6591_ParameterInfos[] = 
{
	{"name", 0, 134219365, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134219366, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t142_0_0_0;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_2_FindMethod_Impl_m6591_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1090_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t142_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_2_t1090_UnityEvent_2_FindMethod_Impl_m6591_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1550/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo UnityEvent_2_t1090_UnityEvent_2_GetDelegate_m6592_ParameterInfos[] = 
{
	{"target", 0, 134219367, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219368, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_2_GetDelegate_m6592_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_2_t1090_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1075_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_2_t1090_UnityEvent_2_GetDelegate_m6592_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1551/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_2_t1090_MethodInfos[] =
{
	&UnityEvent_2__ctor_m6590_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m6591_MethodInfo,
	&UnityEvent_2_GetDelegate_m6592_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo UnityEvent_2_t1090____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &UnityEvent_2_t1090_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_2_t1090_FieldInfos[] =
{
	&UnityEvent_2_t1090____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_2_t1090_0_0_0;
extern Il2CppType UnityEvent_2_t1090_1_0_0;
struct UnityEvent_2_t1090;
TypeInfo UnityEvent_2_t1090_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_2_t1090_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_2_t1090_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_2_t1090_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityEvent_2_t1090_0_0_0/* byval_arg */
	, &UnityEvent_2_t1090_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityEvent_2_t1090_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`3
#include "UnityEngine_UnityEngine_Events_UnityEvent_3.h"
extern Il2CppGenericContainer UnityEvent_3_t1091_Il2CppGenericContainer;
extern TypeInfo UnityEvent_3_t1091_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_3_t1091_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_3_t1091_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityEvent_3_t1091_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_3_t1091_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_3_t1091_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo UnityEvent_3_t1091_gp_T2_2_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_3_t1091_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_3_t1091_Il2CppGenericContainer, 2}, {NULL, "T2", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityEvent_3_t1091_Il2CppGenericParametersArray[3] = 
{
	&UnityEvent_3_t1091_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_3_t1091_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_3_t1091_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityEvent_3_t1091_il2cpp_TypeInfo;
Il2CppGenericContainer UnityEvent_3_t1091_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityEvent_3_t1091_il2cpp_TypeInfo, 3, 0, UnityEvent_3_t1091_Il2CppGenericParametersArray };
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`3::.ctor()
MethodInfo UnityEvent_3__ctor_m6593_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1091_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1552/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_3_t1091_UnityEvent_3_FindMethod_Impl_m6594_ParameterInfos[] = 
{
	{"name", 0, 134219369, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134219370, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t142_0_0_0;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_3_FindMethod_Impl_m6594_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1091_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t142_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_3_t1091_UnityEvent_3_FindMethod_Impl_m6594_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1553/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo UnityEvent_3_t1091_UnityEvent_3_GetDelegate_m6595_ParameterInfos[] = 
{
	{"target", 0, 134219371, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219372, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_3_GetDelegate_m6595_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_3_t1091_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1075_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_3_t1091_UnityEvent_3_GetDelegate_m6595_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1554/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_3_t1091_MethodInfos[] =
{
	&UnityEvent_3__ctor_m6593_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m6594_MethodInfo,
	&UnityEvent_3_GetDelegate_m6595_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo UnityEvent_3_t1091____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &UnityEvent_3_t1091_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_3_t1091_FieldInfos[] =
{
	&UnityEvent_3_t1091____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_3_t1091_0_0_0;
extern Il2CppType UnityEvent_3_t1091_1_0_0;
struct UnityEvent_3_t1091;
TypeInfo UnityEvent_3_t1091_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_3_t1091_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_3_t1091_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_3_t1091_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityEvent_3_t1091_0_0_0/* byval_arg */
	, &UnityEvent_3_t1091_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityEvent_3_t1091_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`4
#include "UnityEngine_UnityEngine_Events_UnityEvent_4.h"
extern Il2CppGenericContainer UnityEvent_4_t1092_Il2CppGenericContainer;
extern TypeInfo UnityEvent_4_t1092_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_4_t1092_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_4_t1092_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityEvent_4_t1092_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_4_t1092_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_4_t1092_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo UnityEvent_4_t1092_gp_T2_2_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_4_t1092_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_4_t1092_Il2CppGenericContainer, 2}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo UnityEvent_4_t1092_gp_T3_3_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityEvent_4_t1092_gp_T3_3_il2cpp_TypeInfo_GenericParamFull = { { &UnityEvent_4_t1092_Il2CppGenericContainer, 3}, {NULL, "T3", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityEvent_4_t1092_Il2CppGenericParametersArray[4] = 
{
	&UnityEvent_4_t1092_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1092_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1092_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
	&UnityEvent_4_t1092_gp_T3_3_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityEvent_4_t1092_il2cpp_TypeInfo;
Il2CppGenericContainer UnityEvent_4_t1092_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityEvent_4_t1092_il2cpp_TypeInfo, 4, 0, UnityEvent_4_t1092_Il2CppGenericParametersArray };
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityEvent`4::.ctor()
MethodInfo UnityEvent_4__ctor_m6596_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1092_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1555/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_4_t1092_UnityEvent_4_FindMethod_Impl_m6597_ParameterInfos[] = 
{
	{"name", 0, 134219373, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134219374, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t142_0_0_0;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_4_FindMethod_Impl_m6597_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1092_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t142_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_4_t1092_UnityEvent_4_FindMethod_Impl_m6597_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1556/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo UnityEvent_4_t1092_UnityEvent_4_GetDelegate_m6598_ParameterInfos[] = 
{
	{"target", 0, 134219375, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134219376, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_4_GetDelegate_m6598_MethodInfo = 
{
	"GetDelegate"/* name */
	, NULL/* method */
	, &UnityEvent_4_t1092_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1075_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityEvent_4_t1092_UnityEvent_4_GetDelegate_m6598_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1557/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityEvent_4_t1092_MethodInfos[] =
{
	&UnityEvent_4__ctor_m6596_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m6597_MethodInfo,
	&UnityEvent_4_GetDelegate_m6598_MethodInfo,
	NULL
};
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo UnityEvent_4_t1092____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &UnityEvent_4_t1092_il2cpp_TypeInfo/* parent */
	, 0/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_4_t1092_FieldInfos[] =
{
	&UnityEvent_4_t1092____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_4_t1092_0_0_0;
extern Il2CppType UnityEvent_4_t1092_1_0_0;
struct UnityEvent_4_t1092;
TypeInfo UnityEvent_4_t1092_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_4_t1092_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_4_t1092_FieldInfos/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_4_t1092_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityEvent_4_t1092_0_0_0/* byval_arg */
	, &UnityEvent_4_t1092_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityEvent_4_t1092_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialog.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UserAuthorizationDialog_t1093_il2cpp_TypeInfo;
// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialogMethodDeclarations.h"

// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern MethodInfo MonoBehaviour__ctor_m214_MethodInfo;


// System.Void UnityEngine.UserAuthorizationDialog::.ctor()
extern MethodInfo UserAuthorizationDialog__ctor_m6388_MethodInfo;
 void UserAuthorizationDialog__ctor_m6388 (UserAuthorizationDialog_t1093 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// System.Void UnityEngine.UserAuthorizationDialog::Start()
extern MethodInfo UserAuthorizationDialog_Start_m6389_MethodInfo;
 void UserAuthorizationDialog_Start_m6389 (UserAuthorizationDialog_t1093 * __this, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.UserAuthorizationDialog::OnGUI()
extern MethodInfo UserAuthorizationDialog_OnGUI_m6390_MethodInfo;
 void UserAuthorizationDialog_OnGUI_m6390 (UserAuthorizationDialog_t1093 * __this, MethodInfo* method){
	{
		return;
	}
}
// System.Void UnityEngine.UserAuthorizationDialog::DoUserAuthorizationDialog(System.Int32)
extern MethodInfo UserAuthorizationDialog_DoUserAuthorizationDialog_m6391_MethodInfo;
 void UserAuthorizationDialog_DoUserAuthorizationDialog_m6391 (UserAuthorizationDialog_t1093 * __this, int32_t ___windowID, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition UnityEngine.UserAuthorizationDialog
extern Il2CppType Int32_t93_0_0_32849;
FieldInfo UserAuthorizationDialog_t1093____width_2_FieldInfo = 
{
	"width"/* name */
	, &Int32_t93_0_0_32849/* type */
	, &UserAuthorizationDialog_t1093_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_32849;
FieldInfo UserAuthorizationDialog_t1093____height_3_FieldInfo = 
{
	"height"/* name */
	, &Int32_t93_0_0_32849/* type */
	, &UserAuthorizationDialog_t1093_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Rect_t118_0_0_1;
FieldInfo UserAuthorizationDialog_t1093____windowRect_4_FieldInfo = 
{
	"windowRect"/* name */
	, &Rect_t118_0_0_1/* type */
	, &UserAuthorizationDialog_t1093_il2cpp_TypeInfo/* parent */
	, offsetof(UserAuthorizationDialog_t1093, ___windowRect_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Texture_t294_0_0_1;
FieldInfo UserAuthorizationDialog_t1093____warningIcon_5_FieldInfo = 
{
	"warningIcon"/* name */
	, &Texture_t294_0_0_1/* type */
	, &UserAuthorizationDialog_t1093_il2cpp_TypeInfo/* parent */
	, offsetof(UserAuthorizationDialog_t1093, ___warningIcon_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UserAuthorizationDialog_t1093_FieldInfos[] =
{
	&UserAuthorizationDialog_t1093____width_2_FieldInfo,
	&UserAuthorizationDialog_t1093____height_3_FieldInfo,
	&UserAuthorizationDialog_t1093____windowRect_4_FieldInfo,
	&UserAuthorizationDialog_t1093____warningIcon_5_FieldInfo,
	NULL
};
static const int32_t UserAuthorizationDialog_t1093____width_2_DefaultValueData = 385;
static Il2CppFieldDefaultValueEntry UserAuthorizationDialog_t1093____width_2_DefaultValue = 
{
	&UserAuthorizationDialog_t1093____width_2_FieldInfo/* field */
	, { (char*)&UserAuthorizationDialog_t1093____width_2_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t UserAuthorizationDialog_t1093____height_3_DefaultValueData = 155;
static Il2CppFieldDefaultValueEntry UserAuthorizationDialog_t1093____height_3_DefaultValue = 
{
	&UserAuthorizationDialog_t1093____height_3_FieldInfo/* field */
	, { (char*)&UserAuthorizationDialog_t1093____height_3_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* UserAuthorizationDialog_t1093_FieldDefaultValues[] = 
{
	&UserAuthorizationDialog_t1093____width_2_DefaultValue,
	&UserAuthorizationDialog_t1093____height_3_DefaultValue,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::.ctor()
MethodInfo UserAuthorizationDialog__ctor_m6388_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserAuthorizationDialog__ctor_m6388/* method */
	, &UserAuthorizationDialog_t1093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1558/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::Start()
MethodInfo UserAuthorizationDialog_Start_m6389_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&UserAuthorizationDialog_Start_m6389/* method */
	, &UserAuthorizationDialog_t1093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1559/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::OnGUI()
MethodInfo UserAuthorizationDialog_OnGUI_m6390_MethodInfo = 
{
	"OnGUI"/* name */
	, (methodPointerType)&UserAuthorizationDialog_OnGUI_m6390/* method */
	, &UserAuthorizationDialog_t1093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1560/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo UserAuthorizationDialog_t1093_UserAuthorizationDialog_DoUserAuthorizationDialog_m6391_ParameterInfos[] = 
{
	{"windowID", 0, 134219377, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.UserAuthorizationDialog::DoUserAuthorizationDialog(System.Int32)
MethodInfo UserAuthorizationDialog_DoUserAuthorizationDialog_m6391_MethodInfo = 
{
	"DoUserAuthorizationDialog"/* name */
	, (methodPointerType)&UserAuthorizationDialog_DoUserAuthorizationDialog_m6391/* method */
	, &UserAuthorizationDialog_t1093_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, UserAuthorizationDialog_t1093_UserAuthorizationDialog_DoUserAuthorizationDialog_m6391_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1561/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UserAuthorizationDialog_t1093_MethodInfos[] =
{
	&UserAuthorizationDialog__ctor_m6388_MethodInfo,
	&UserAuthorizationDialog_Start_m6389_MethodInfo,
	&UserAuthorizationDialog_OnGUI_m6390_MethodInfo,
	&UserAuthorizationDialog_DoUserAuthorizationDialog_m6391_MethodInfo,
	NULL
};
static MethodInfo* UserAuthorizationDialog_t1093_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern TypeInfo AddComponentMenu_t419_il2cpp_TypeInfo;
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern MethodInfo AddComponentMenu__ctor_m1899_MethodInfo;
void UserAuthorizationDialog_t1093_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t419 * tmp;
		tmp = (AddComponentMenu_t419 *)il2cpp_codegen_object_new (&AddComponentMenu_t419_il2cpp_TypeInfo);
		AddComponentMenu__ctor_m1899(tmp, il2cpp_codegen_string_new_wrapper(""), &AddComponentMenu__ctor_m1899_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache UserAuthorizationDialog_t1093__CustomAttributeCache = {
1,
NULL,
&UserAuthorizationDialog_t1093_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UserAuthorizationDialog_t1093_0_0_0;
extern Il2CppType UserAuthorizationDialog_t1093_1_0_0;
extern TypeInfo MonoBehaviour_t6_il2cpp_TypeInfo;
struct UserAuthorizationDialog_t1093;
extern CustomAttributesCache UserAuthorizationDialog_t1093__CustomAttributeCache;
TypeInfo UserAuthorizationDialog_t1093_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserAuthorizationDialog"/* name */
	, "UnityEngine"/* namespaze */
	, UserAuthorizationDialog_t1093_MethodInfos/* methods */
	, NULL/* properties */
	, UserAuthorizationDialog_t1093_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UserAuthorizationDialog_t1093_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UserAuthorizationDialog_t1093_VTable/* vtable */
	, &UserAuthorizationDialog_t1093__CustomAttributeCache/* custom_attributes_cache */
	, &UserAuthorizationDialog_t1093_il2cpp_TypeInfo/* cast_class */
	, &UserAuthorizationDialog_t1093_0_0_0/* byval_arg */
	, &UserAuthorizationDialog_t1093_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, UserAuthorizationDialog_t1093_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserAuthorizationDialog_t1093)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo DefaultValueAttribute_t1094_il2cpp_TypeInfo;
// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttributeMethodDeclarations.h"

// System.Attribute
#include "mscorlib_System_Attribute.h"
extern MethodInfo DefaultValueAttribute_get_Value_m6393_MethodInfo;


// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern MethodInfo DefaultValueAttribute__ctor_m6392_MethodInfo;
 void DefaultValueAttribute__ctor_m6392 (DefaultValueAttribute_t1094 * __this, String_t* ___value, MethodInfo* method){
	{
		Attribute__ctor_m4248(__this, /*hidden argument*/&Attribute__ctor_m4248_MethodInfo);
		__this->___DefaultValue_0 = ___value;
		return;
	}
}
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
 Object_t * DefaultValueAttribute_get_Value_m6393 (DefaultValueAttribute_t1094 * __this, MethodInfo* method){
	{
		Object_t * L_0 = (__this->___DefaultValue_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern MethodInfo DefaultValueAttribute_Equals_m6394_MethodInfo;
 bool DefaultValueAttribute_Equals_m6394 (DefaultValueAttribute_t1094 * __this, Object_t * ___obj, MethodInfo* method){
	DefaultValueAttribute_t1094 * V_0 = {0};
	{
		V_0 = ((DefaultValueAttribute_t1094 *)IsInst(___obj, InitializedTypeInfo(&DefaultValueAttribute_t1094_il2cpp_TypeInfo)));
		if (V_0)
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}

IL_000f:
	{
		Object_t * L_0 = (__this->___DefaultValue_0);
		if (L_0)
		{
			goto IL_0024;
		}
	}
	{
		NullCheck(V_0);
		Object_t * L_1 = DefaultValueAttribute_get_Value_m6393(V_0, /*hidden argument*/&DefaultValueAttribute_get_Value_m6393_MethodInfo);
		return ((((Object_t *)L_1) == ((Object_t *)NULL))? 1 : 0);
	}

IL_0024:
	{
		Object_t * L_2 = (__this->___DefaultValue_0);
		NullCheck(V_0);
		Object_t * L_3 = DefaultValueAttribute_get_Value_m6393(V_0, /*hidden argument*/&DefaultValueAttribute_get_Value_m6393_MethodInfo);
		NullCheck(L_2);
		bool L_4 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m304_MethodInfo, L_2, L_3);
		return L_4;
	}
}
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern MethodInfo DefaultValueAttribute_GetHashCode_m6395_MethodInfo;
 int32_t DefaultValueAttribute_GetHashCode_m6395 (DefaultValueAttribute_t1094 * __this, MethodInfo* method){
	{
		Object_t * L_0 = (__this->___DefaultValue_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = Attribute_GetHashCode_m4250(__this, /*hidden argument*/&Attribute_GetHashCode_m4250_MethodInfo);
		return L_1;
	}

IL_0012:
	{
		Object_t * L_2 = (__this->___DefaultValue_0);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&Object_GetHashCode_m305_MethodInfo, L_2);
		return L_3;
	}
}
// Metadata Definition UnityEngine.Internal.DefaultValueAttribute
extern Il2CppType Object_t_0_0_1;
FieldInfo DefaultValueAttribute_t1094____DefaultValue_0_FieldInfo = 
{
	"DefaultValue"/* name */
	, &Object_t_0_0_1/* type */
	, &DefaultValueAttribute_t1094_il2cpp_TypeInfo/* parent */
	, offsetof(DefaultValueAttribute_t1094, ___DefaultValue_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* DefaultValueAttribute_t1094_FieldInfos[] =
{
	&DefaultValueAttribute_t1094____DefaultValue_0_FieldInfo,
	NULL
};
static PropertyInfo DefaultValueAttribute_t1094____Value_PropertyInfo = 
{
	&DefaultValueAttribute_t1094_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &DefaultValueAttribute_get_Value_m6393_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* DefaultValueAttribute_t1094_PropertyInfos[] =
{
	&DefaultValueAttribute_t1094____Value_PropertyInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
static ParameterInfo DefaultValueAttribute_t1094_DefaultValueAttribute__ctor_m6392_ParameterInfos[] = 
{
	{"value", 0, 134219378, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
MethodInfo DefaultValueAttribute__ctor_m6392_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&DefaultValueAttribute__ctor_m6392/* method */
	, &DefaultValueAttribute_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, DefaultValueAttribute_t1094_DefaultValueAttribute__ctor_m6392_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1562/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
MethodInfo DefaultValueAttribute_get_Value_m6393_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&DefaultValueAttribute_get_Value_m6393/* method */
	, &DefaultValueAttribute_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1563/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo DefaultValueAttribute_t1094_DefaultValueAttribute_Equals_m6394_ParameterInfos[] = 
{
	{"obj", 0, 134219379, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
MethodInfo DefaultValueAttribute_Equals_m6394_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&DefaultValueAttribute_Equals_m6394/* method */
	, &DefaultValueAttribute_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, DefaultValueAttribute_t1094_DefaultValueAttribute_Equals_m6394_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1564/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
MethodInfo DefaultValueAttribute_GetHashCode_m6395_MethodInfo = 
{
	"GetHashCode"/* name */
	, (methodPointerType)&DefaultValueAttribute_GetHashCode_m6395/* method */
	, &DefaultValueAttribute_t1094_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1565/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* DefaultValueAttribute_t1094_MethodInfos[] =
{
	&DefaultValueAttribute__ctor_m6392_MethodInfo,
	&DefaultValueAttribute_get_Value_m6393_MethodInfo,
	&DefaultValueAttribute_Equals_m6394_MethodInfo,
	&DefaultValueAttribute_GetHashCode_m6395_MethodInfo,
	NULL
};
static MethodInfo* DefaultValueAttribute_t1094_VTable[] =
{
	&DefaultValueAttribute_Equals_m6394_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&DefaultValueAttribute_GetHashCode_m6395_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
static Il2CppInterfaceOffsetPair DefaultValueAttribute_t1094_InterfacesOffsets[] = 
{
	{ &_Attribute_t775_il2cpp_TypeInfo, 4},
};
void DefaultValueAttribute_t1094_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1160 * tmp;
		tmp = (AttributeUsageAttribute_t1160 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1160_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6500(tmp, 18432, &AttributeUsageAttribute__ctor_m6500_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache DefaultValueAttribute_t1094__CustomAttributeCache = {
1,
NULL,
&DefaultValueAttribute_t1094_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType DefaultValueAttribute_t1094_0_0_0;
extern Il2CppType DefaultValueAttribute_t1094_1_0_0;
struct DefaultValueAttribute_t1094;
extern CustomAttributesCache DefaultValueAttribute_t1094__CustomAttributeCache;
TypeInfo DefaultValueAttribute_t1094_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "DefaultValueAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, DefaultValueAttribute_t1094_MethodInfos/* methods */
	, DefaultValueAttribute_t1094_PropertyInfos/* properties */
	, DefaultValueAttribute_t1094_FieldInfos/* fields */
	, NULL/* events */
	, &Attribute_t146_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &DefaultValueAttribute_t1094_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, DefaultValueAttribute_t1094_VTable/* vtable */
	, &DefaultValueAttribute_t1094__CustomAttributeCache/* custom_attributes_cache */
	, &DefaultValueAttribute_t1094_il2cpp_TypeInfo/* cast_class */
	, &DefaultValueAttribute_t1094_0_0_0/* byval_arg */
	, &DefaultValueAttribute_t1094_1_0_0/* this_arg */
	, DefaultValueAttribute_t1094_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (DefaultValueAttribute_t1094)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 1/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ExcludeFromDocsAttribute_t1095_il2cpp_TypeInfo;
// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttributeMethodDeclarations.h"



// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern MethodInfo ExcludeFromDocsAttribute__ctor_m6396_MethodInfo;
 void ExcludeFromDocsAttribute__ctor_m6396 (ExcludeFromDocsAttribute_t1095 * __this, MethodInfo* method){
	{
		Attribute__ctor_m4248(__this, /*hidden argument*/&Attribute__ctor_m4248_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngine.Internal.ExcludeFromDocsAttribute
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
MethodInfo ExcludeFromDocsAttribute__ctor_m6396_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ExcludeFromDocsAttribute__ctor_m6396/* method */
	, &ExcludeFromDocsAttribute_t1095_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1566/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* ExcludeFromDocsAttribute_t1095_MethodInfos[] =
{
	&ExcludeFromDocsAttribute__ctor_m6396_MethodInfo,
	NULL
};
static MethodInfo* ExcludeFromDocsAttribute_t1095_VTable[] =
{
	&Attribute_Equals_m4249_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Attribute_GetHashCode_m4250_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
static Il2CppInterfaceOffsetPair ExcludeFromDocsAttribute_t1095_InterfacesOffsets[] = 
{
	{ &_Attribute_t775_il2cpp_TypeInfo, 4},
};
void ExcludeFromDocsAttribute_t1095_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1160 * tmp;
		tmp = (AttributeUsageAttribute_t1160 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1160_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6500(tmp, 64, &AttributeUsageAttribute__ctor_m6500_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache ExcludeFromDocsAttribute_t1095__CustomAttributeCache = {
1,
NULL,
&ExcludeFromDocsAttribute_t1095_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType ExcludeFromDocsAttribute_t1095_0_0_0;
extern Il2CppType ExcludeFromDocsAttribute_t1095_1_0_0;
struct ExcludeFromDocsAttribute_t1095;
extern CustomAttributesCache ExcludeFromDocsAttribute_t1095__CustomAttributeCache;
TypeInfo ExcludeFromDocsAttribute_t1095_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "ExcludeFromDocsAttribute"/* name */
	, "UnityEngine.Internal"/* namespaze */
	, ExcludeFromDocsAttribute_t1095_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Attribute_t146_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ExcludeFromDocsAttribute_t1095_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ExcludeFromDocsAttribute_t1095_VTable/* vtable */
	, &ExcludeFromDocsAttribute_t1095__CustomAttributeCache/* custom_attributes_cache */
	, &ExcludeFromDocsAttribute_t1095_il2cpp_TypeInfo/* cast_class */
	, &ExcludeFromDocsAttribute_t1095_0_0_0/* byval_arg */
	, &ExcludeFromDocsAttribute_t1095_1_0_0/* this_arg */
	, ExcludeFromDocsAttribute_t1095_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ExcludeFromDocsAttribute_t1095)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
 void FormerlySerializedAsAttribute__ctor_m1900 (FormerlySerializedAsAttribute_t420 * __this, String_t* ___oldName, MethodInfo* method){
	{
		Attribute__ctor_m4248(__this, /*hidden argument*/&Attribute__ctor_m4248_MethodInfo);
		__this->___m_oldName_0 = ___oldName;
		return;
	}
}
// Metadata Definition UnityEngine.Serialization.FormerlySerializedAsAttribute
extern Il2CppType String_t_0_0_1;
FieldInfo FormerlySerializedAsAttribute_t420____m_oldName_0_FieldInfo = 
{
	"m_oldName"/* name */
	, &String_t_0_0_1/* type */
	, &FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo/* parent */
	, offsetof(FormerlySerializedAsAttribute_t420, ___m_oldName_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* FormerlySerializedAsAttribute_t420_FieldInfos[] =
{
	&FormerlySerializedAsAttribute_t420____m_oldName_0_FieldInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
static ParameterInfo FormerlySerializedAsAttribute_t420_FormerlySerializedAsAttribute__ctor_m1900_ParameterInfos[] = 
{
	{"oldName", 0, 134219380, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
MethodInfo FormerlySerializedAsAttribute__ctor_m1900_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&FormerlySerializedAsAttribute__ctor_m1900/* method */
	, &FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, FormerlySerializedAsAttribute_t420_FormerlySerializedAsAttribute__ctor_m1900_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1567/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* FormerlySerializedAsAttribute_t420_MethodInfos[] =
{
	&FormerlySerializedAsAttribute__ctor_m1900_MethodInfo,
	NULL
};
static MethodInfo* FormerlySerializedAsAttribute_t420_VTable[] =
{
	&Attribute_Equals_m4249_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Attribute_GetHashCode_m4250_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
static Il2CppInterfaceOffsetPair FormerlySerializedAsAttribute_t420_InterfacesOffsets[] = 
{
	{ &_Attribute_t775_il2cpp_TypeInfo, 4},
};
void FormerlySerializedAsAttribute_t420_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1160 * tmp;
		tmp = (AttributeUsageAttribute_t1160 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1160_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6500(tmp, 256, &AttributeUsageAttribute__ctor_m6500_MethodInfo);
		AttributeUsageAttribute_set_AllowMultiple_m6502(tmp, true, &AttributeUsageAttribute_set_AllowMultiple_m6502_MethodInfo);
		AttributeUsageAttribute_set_Inherited_m6501(tmp, false, &AttributeUsageAttribute_set_Inherited_m6501_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache FormerlySerializedAsAttribute_t420__CustomAttributeCache = {
1,
NULL,
&FormerlySerializedAsAttribute_t420_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType FormerlySerializedAsAttribute_t420_0_0_0;
extern Il2CppType FormerlySerializedAsAttribute_t420_1_0_0;
struct FormerlySerializedAsAttribute_t420;
extern CustomAttributesCache FormerlySerializedAsAttribute_t420__CustomAttributeCache;
TypeInfo FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "FormerlySerializedAsAttribute"/* name */
	, "UnityEngine.Serialization"/* namespaze */
	, FormerlySerializedAsAttribute_t420_MethodInfos/* methods */
	, NULL/* properties */
	, FormerlySerializedAsAttribute_t420_FieldInfos/* fields */
	, NULL/* events */
	, &Attribute_t146_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, FormerlySerializedAsAttribute_t420_VTable/* vtable */
	, &FormerlySerializedAsAttribute_t420__CustomAttributeCache/* custom_attributes_cache */
	, &FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo/* cast_class */
	, &FormerlySerializedAsAttribute_t420_0_0_0/* byval_arg */
	, &FormerlySerializedAsAttribute_t420_1_0_0/* this_arg */
	, FormerlySerializedAsAttribute_t420_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (FormerlySerializedAsAttribute_t420)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TypeInferenceRules_t1096_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRulesMethodDeclarations.h"



// Metadata Definition UnityEngineInternal.TypeInferenceRules
extern Il2CppType Int32_t93_0_0_1542;
FieldInfo TypeInferenceRules_t1096____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t93_0_0_1542/* type */
	, &TypeInferenceRules_t1096_il2cpp_TypeInfo/* parent */
	, offsetof(TypeInferenceRules_t1096, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TypeInferenceRules_t1096_0_0_32854;
FieldInfo TypeInferenceRules_t1096____TypeReferencedByFirstArgument_2_FieldInfo = 
{
	"TypeReferencedByFirstArgument"/* name */
	, &TypeInferenceRules_t1096_0_0_32854/* type */
	, &TypeInferenceRules_t1096_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TypeInferenceRules_t1096_0_0_32854;
FieldInfo TypeInferenceRules_t1096____TypeReferencedBySecondArgument_3_FieldInfo = 
{
	"TypeReferencedBySecondArgument"/* name */
	, &TypeInferenceRules_t1096_0_0_32854/* type */
	, &TypeInferenceRules_t1096_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TypeInferenceRules_t1096_0_0_32854;
FieldInfo TypeInferenceRules_t1096____ArrayOfTypeReferencedByFirstArgument_4_FieldInfo = 
{
	"ArrayOfTypeReferencedByFirstArgument"/* name */
	, &TypeInferenceRules_t1096_0_0_32854/* type */
	, &TypeInferenceRules_t1096_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType TypeInferenceRules_t1096_0_0_32854;
FieldInfo TypeInferenceRules_t1096____TypeOfFirstArgument_5_FieldInfo = 
{
	"TypeOfFirstArgument"/* name */
	, &TypeInferenceRules_t1096_0_0_32854/* type */
	, &TypeInferenceRules_t1096_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TypeInferenceRules_t1096_FieldInfos[] =
{
	&TypeInferenceRules_t1096____value___1_FieldInfo,
	&TypeInferenceRules_t1096____TypeReferencedByFirstArgument_2_FieldInfo,
	&TypeInferenceRules_t1096____TypeReferencedBySecondArgument_3_FieldInfo,
	&TypeInferenceRules_t1096____ArrayOfTypeReferencedByFirstArgument_4_FieldInfo,
	&TypeInferenceRules_t1096____TypeOfFirstArgument_5_FieldInfo,
	NULL
};
static const int32_t TypeInferenceRules_t1096____TypeReferencedByFirstArgument_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry TypeInferenceRules_t1096____TypeReferencedByFirstArgument_2_DefaultValue = 
{
	&TypeInferenceRules_t1096____TypeReferencedByFirstArgument_2_FieldInfo/* field */
	, { (char*)&TypeInferenceRules_t1096____TypeReferencedByFirstArgument_2_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TypeInferenceRules_t1096____TypeReferencedBySecondArgument_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry TypeInferenceRules_t1096____TypeReferencedBySecondArgument_3_DefaultValue = 
{
	&TypeInferenceRules_t1096____TypeReferencedBySecondArgument_3_FieldInfo/* field */
	, { (char*)&TypeInferenceRules_t1096____TypeReferencedBySecondArgument_3_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TypeInferenceRules_t1096____ArrayOfTypeReferencedByFirstArgument_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry TypeInferenceRules_t1096____ArrayOfTypeReferencedByFirstArgument_4_DefaultValue = 
{
	&TypeInferenceRules_t1096____ArrayOfTypeReferencedByFirstArgument_4_FieldInfo/* field */
	, { (char*)&TypeInferenceRules_t1096____ArrayOfTypeReferencedByFirstArgument_4_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t TypeInferenceRules_t1096____TypeOfFirstArgument_5_DefaultValueData = 3;
static Il2CppFieldDefaultValueEntry TypeInferenceRules_t1096____TypeOfFirstArgument_5_DefaultValue = 
{
	&TypeInferenceRules_t1096____TypeOfFirstArgument_5_FieldInfo/* field */
	, { (char*)&TypeInferenceRules_t1096____TypeOfFirstArgument_5_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* TypeInferenceRules_t1096_FieldDefaultValues[] = 
{
	&TypeInferenceRules_t1096____TypeReferencedByFirstArgument_2_DefaultValue,
	&TypeInferenceRules_t1096____TypeReferencedBySecondArgument_3_DefaultValue,
	&TypeInferenceRules_t1096____ArrayOfTypeReferencedByFirstArgument_4_DefaultValue,
	&TypeInferenceRules_t1096____TypeOfFirstArgument_5_DefaultValue,
	NULL
};
static MethodInfo* TypeInferenceRules_t1096_MethodInfos[] =
{
	NULL
};
static MethodInfo* TypeInferenceRules_t1096_VTable[] =
{
	&Enum_Equals_m191_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Enum_GetHashCode_m193_MethodInfo,
	&Enum_ToString_m194_MethodInfo,
	&Enum_ToString_m195_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m196_MethodInfo,
	&Enum_System_IConvertible_ToByte_m197_MethodInfo,
	&Enum_System_IConvertible_ToChar_m198_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m199_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m200_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m201_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m202_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m203_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m204_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m205_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m206_MethodInfo,
	&Enum_ToString_m207_MethodInfo,
	&Enum_System_IConvertible_ToType_m208_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m209_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m210_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m211_MethodInfo,
	&Enum_CompareTo_m212_MethodInfo,
	&Enum_GetTypeCode_m213_MethodInfo,
};
static Il2CppInterfaceOffsetPair TypeInferenceRules_t1096_InterfacesOffsets[] = 
{
	{ &IFormattable_t94_il2cpp_TypeInfo, 4},
	{ &IConvertible_t95_il2cpp_TypeInfo, 5},
	{ &IComparable_t96_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TypeInferenceRules_t1096_0_0_0;
extern Il2CppType TypeInferenceRules_t1096_1_0_0;
TypeInfo TypeInferenceRules_t1096_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRules"/* name */
	, "UnityEngineInternal"/* namespaze */
	, TypeInferenceRules_t1096_MethodInfos/* methods */
	, NULL/* properties */
	, TypeInferenceRules_t1096_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t97_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t93_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TypeInferenceRules_t1096_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &TypeInferenceRules_t1096_0_0_0/* byval_arg */
	, &TypeInferenceRules_t1096_1_0_0/* this_arg */
	, TypeInferenceRules_t1096_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, TypeInferenceRules_t1096_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRules_t1096)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TypeInferenceRuleAttribute_t1097_il2cpp_TypeInfo;
// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttributeMethodDeclarations.h"

// System.Enum
#include "mscorlib_System_Enum.h"
// System.Enum
#include "mscorlib_System_EnumMethodDeclarations.h"
extern MethodInfo TypeInferenceRuleAttribute__ctor_m6398_MethodInfo;


// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern MethodInfo TypeInferenceRuleAttribute__ctor_m6397_MethodInfo;
 void TypeInferenceRuleAttribute__ctor_m6397 (TypeInferenceRuleAttribute_t1097 * __this, int32_t ___rule, MethodInfo* method){
	{
		int32_t L_0 = ___rule;
		Object_t * L_1 = Box(InitializedTypeInfo(&TypeInferenceRules_t1096_il2cpp_TypeInfo), &L_0);
		NullCheck(L_1);
		String_t* L_2 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(&Enum_ToString_m194_MethodInfo, L_1);
		TypeInferenceRuleAttribute__ctor_m6398(__this, L_2, /*hidden argument*/&TypeInferenceRuleAttribute__ctor_m6398_MethodInfo);
		return;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
 void TypeInferenceRuleAttribute__ctor_m6398 (TypeInferenceRuleAttribute_t1097 * __this, String_t* ___rule, MethodInfo* method){
	{
		Attribute__ctor_m4248(__this, /*hidden argument*/&Attribute__ctor_m4248_MethodInfo);
		__this->____rule_0 = ___rule;
		return;
	}
}
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern MethodInfo TypeInferenceRuleAttribute_ToString_m6399_MethodInfo;
 String_t* TypeInferenceRuleAttribute_ToString_m6399 (TypeInferenceRuleAttribute_t1097 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->____rule_0);
		return L_0;
	}
}
// Metadata Definition UnityEngineInternal.TypeInferenceRuleAttribute
extern Il2CppType String_t_0_0_33;
FieldInfo TypeInferenceRuleAttribute_t1097_____rule_0_FieldInfo = 
{
	"_rule"/* name */
	, &String_t_0_0_33/* type */
	, &TypeInferenceRuleAttribute_t1097_il2cpp_TypeInfo/* parent */
	, offsetof(TypeInferenceRuleAttribute_t1097, ____rule_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TypeInferenceRuleAttribute_t1097_FieldInfos[] =
{
	&TypeInferenceRuleAttribute_t1097_____rule_0_FieldInfo,
	NULL
};
extern Il2CppType TypeInferenceRules_t1096_0_0_0;
static ParameterInfo TypeInferenceRuleAttribute_t1097_TypeInferenceRuleAttribute__ctor_m6397_ParameterInfos[] = 
{
	{"rule", 0, 134219381, &EmptyCustomAttributesCache, &TypeInferenceRules_t1096_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
MethodInfo TypeInferenceRuleAttribute__ctor_m6397_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute__ctor_m6397/* method */
	, &TypeInferenceRuleAttribute_t1097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, TypeInferenceRuleAttribute_t1097_TypeInferenceRuleAttribute__ctor_m6397_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1568/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TypeInferenceRuleAttribute_t1097_TypeInferenceRuleAttribute__ctor_m6398_ParameterInfos[] = 
{
	{"rule", 0, 134219382, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
MethodInfo TypeInferenceRuleAttribute__ctor_m6398_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute__ctor_m6398/* method */
	, &TypeInferenceRuleAttribute_t1097_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, TypeInferenceRuleAttribute_t1097_TypeInferenceRuleAttribute__ctor_m6398_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1569/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
MethodInfo TypeInferenceRuleAttribute_ToString_m6399_MethodInfo = 
{
	"ToString"/* name */
	, (methodPointerType)&TypeInferenceRuleAttribute_ToString_m6399/* method */
	, &TypeInferenceRuleAttribute_t1097_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1570/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TypeInferenceRuleAttribute_t1097_MethodInfos[] =
{
	&TypeInferenceRuleAttribute__ctor_m6397_MethodInfo,
	&TypeInferenceRuleAttribute__ctor_m6398_MethodInfo,
	&TypeInferenceRuleAttribute_ToString_m6399_MethodInfo,
	NULL
};
static MethodInfo* TypeInferenceRuleAttribute_t1097_VTable[] =
{
	&Attribute_Equals_m4249_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Attribute_GetHashCode_m4250_MethodInfo,
	&TypeInferenceRuleAttribute_ToString_m6399_MethodInfo,
};
static Il2CppInterfaceOffsetPair TypeInferenceRuleAttribute_t1097_InterfacesOffsets[] = 
{
	{ &_Attribute_t775_il2cpp_TypeInfo, 4},
};
void TypeInferenceRuleAttribute_t1097_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AttributeUsageAttribute_t1160 * tmp;
		tmp = (AttributeUsageAttribute_t1160 *)il2cpp_codegen_object_new (&AttributeUsageAttribute_t1160_il2cpp_TypeInfo);
		AttributeUsageAttribute__ctor_m6500(tmp, 64, &AttributeUsageAttribute__ctor_m6500_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache TypeInferenceRuleAttribute_t1097__CustomAttributeCache = {
1,
NULL,
&TypeInferenceRuleAttribute_t1097_CustomAttributesCacheGenerator
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType TypeInferenceRuleAttribute_t1097_0_0_0;
extern Il2CppType TypeInferenceRuleAttribute_t1097_1_0_0;
struct TypeInferenceRuleAttribute_t1097;
extern CustomAttributesCache TypeInferenceRuleAttribute_t1097__CustomAttributeCache;
TypeInfo TypeInferenceRuleAttribute_t1097_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "TypeInferenceRuleAttribute"/* name */
	, "UnityEngineInternal"/* namespaze */
	, TypeInferenceRuleAttribute_t1097_MethodInfos/* methods */
	, NULL/* properties */
	, TypeInferenceRuleAttribute_t1097_FieldInfos/* fields */
	, NULL/* events */
	, &Attribute_t146_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TypeInferenceRuleAttribute_t1097_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TypeInferenceRuleAttribute_t1097_VTable/* vtable */
	, &TypeInferenceRuleAttribute_t1097__CustomAttributeCache/* custom_attributes_cache */
	, &TypeInferenceRuleAttribute_t1097_il2cpp_TypeInfo/* cast_class */
	, &TypeInferenceRuleAttribute_t1097_0_0_0/* byval_arg */
	, &TypeInferenceRuleAttribute_t1097_1_0_0/* this_arg */
	, TypeInferenceRuleAttribute_t1097_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TypeInferenceRuleAttribute_t1097)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStack.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo GenericStack_t952_il2cpp_TypeInfo;
// UnityEngineInternal.GenericStack
#include "UnityEngine_UnityEngineInternal_GenericStackMethodDeclarations.h"

// System.Collections.Stack
#include "mscorlib_System_Collections_StackMethodDeclarations.h"
extern MethodInfo Stack__ctor_m6599_MethodInfo;


// System.Void UnityEngineInternal.GenericStack::.ctor()
extern MethodInfo GenericStack__ctor_m6400_MethodInfo;
 void GenericStack__ctor_m6400 (GenericStack_t952 * __this, MethodInfo* method){
	{
		Stack__ctor_m6599(__this, /*hidden argument*/&Stack__ctor_m6599_MethodInfo);
		return;
	}
}
// Metadata Definition UnityEngineInternal.GenericStack
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngineInternal.GenericStack::.ctor()
MethodInfo GenericStack__ctor_m6400_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&GenericStack__ctor_m6400/* method */
	, &GenericStack_t952_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1571/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* GenericStack_t952_MethodInfos[] =
{
	&GenericStack__ctor_m6400_MethodInfo,
	NULL
};
extern MethodInfo Stack_GetEnumerator_m6600_MethodInfo;
extern MethodInfo Stack_get_Count_m6601_MethodInfo;
extern MethodInfo Stack_get_IsSynchronized_m6602_MethodInfo;
extern MethodInfo Stack_get_SyncRoot_m6603_MethodInfo;
extern MethodInfo Stack_CopyTo_m6604_MethodInfo;
extern MethodInfo Stack_Clear_m6427_MethodInfo;
extern MethodInfo Stack_Peek_m6605_MethodInfo;
extern MethodInfo Stack_Pop_m6606_MethodInfo;
extern MethodInfo Stack_Push_m6423_MethodInfo;
static MethodInfo* GenericStack_t952_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Stack_GetEnumerator_m6600_MethodInfo,
	&Stack_get_Count_m6601_MethodInfo,
	&Stack_get_IsSynchronized_m6602_MethodInfo,
	&Stack_get_SyncRoot_m6603_MethodInfo,
	&Stack_CopyTo_m6604_MethodInfo,
	&Stack_get_Count_m6601_MethodInfo,
	&Stack_get_IsSynchronized_m6602_MethodInfo,
	&Stack_get_SyncRoot_m6603_MethodInfo,
	&Stack_Clear_m6427_MethodInfo,
	&Stack_CopyTo_m6604_MethodInfo,
	&Stack_GetEnumerator_m6600_MethodInfo,
	&Stack_Peek_m6605_MethodInfo,
	&Stack_Pop_m6606_MethodInfo,
	&Stack_Push_m6423_MethodInfo,
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
extern TypeInfo ICloneable_t481_il2cpp_TypeInfo;
extern TypeInfo ICollection_t1206_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair GenericStack_t952_InterfacesOffsets[] = 
{
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 4},
	{ &ICloneable_t481_il2cpp_TypeInfo, 5},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType GenericStack_t952_0_0_0;
extern Il2CppType GenericStack_t952_1_0_0;
extern TypeInfo Stack_t1098_il2cpp_TypeInfo;
struct GenericStack_t952;
TypeInfo GenericStack_t952_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "GenericStack"/* name */
	, "UnityEngineInternal"/* namespaze */
	, GenericStack_t952_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &Stack_t1098_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &GenericStack_t952_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, GenericStack_t952_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &GenericStack_t952_il2cpp_TypeInfo/* cast_class */
	, &GenericStack_t952_0_0_0/* byval_arg */
	, &GenericStack_t952_1_0_0/* this_arg */
	, GenericStack_t952_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (GenericStack_t952)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 18/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern MethodInfo UnityAction__ctor_m2276_MethodInfo;
 void UnityAction__ctor_m2276 (UnityAction_t290 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method){
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Events.UnityAction::Invoke()
 void UnityAction_Invoke_m2116 (UnityAction_t290 * __this, MethodInfo* method){
	if(__this->___prev_9 != NULL)
	{
		UnityAction_Invoke_m2116((UnityAction_t290 *)__this->___prev_9, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
	((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
}
void pinvoke_delegate_wrapper_UnityAction_t290(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern MethodInfo UnityAction_BeginInvoke_m6401_MethodInfo;
 Object_t * UnityAction_BeginInvoke_m6401 (UnityAction_t290 * __this, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method){
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern MethodInfo UnityAction_EndInvoke_m6402_MethodInfo;
 void UnityAction_EndInvoke_m6402 (UnityAction_t290 * __this, Object_t * ___result, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Metadata Definition UnityEngine.Events.UnityAction
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_t290_UnityAction__ctor_m2276_ParameterInfos[] = 
{
	{"object", 0, 134219383, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219384, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction__ctor_m2276_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction__ctor_m2276/* method */
	, &UnityAction_t290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_t290_UnityAction__ctor_m2276_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1572/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::Invoke()
MethodInfo UnityAction_Invoke_m2116_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_Invoke_m2116/* method */
	, &UnityAction_t290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1573/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_t290_UnityAction_BeginInvoke_m6401_ParameterInfos[] = 
{
	{"callback", 0, 134219385, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 1, 134219386, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
MethodInfo UnityAction_BeginInvoke_m6401_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_BeginInvoke_m6401/* method */
	, &UnityAction_t290_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_t290_UnityAction_BeginInvoke_m6401_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1574/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_t290_UnityAction_EndInvoke_m6402_ParameterInfos[] = 
{
	{"result", 0, 134219387, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_EndInvoke_m6402_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_EndInvoke_m6402/* method */
	, &UnityAction_t290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_t290_UnityAction_EndInvoke_m6402_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1575/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_t290_MethodInfos[] =
{
	&UnityAction__ctor_m2276_MethodInfo,
	&UnityAction_Invoke_m2116_MethodInfo,
	&UnityAction_BeginInvoke_m6401_MethodInfo,
	&UnityAction_EndInvoke_m6402_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2241_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2242_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2243_MethodInfo;
extern MethodInfo Delegate_Clone_m2244_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2245_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2246_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2247_MethodInfo;
static MethodInfo* UnityAction_t290_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_Invoke_m2116_MethodInfo,
	&UnityAction_BeginInvoke_m6401_MethodInfo,
	&UnityAction_EndInvoke_m6402_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_t290_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_t290_1_0_0;
extern TypeInfo MulticastDelegate_t325_il2cpp_TypeInfo;
struct UnityAction_t290;
TypeInfo UnityAction_t290_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_t290_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_t290_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_t290_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_t290_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_t290_0_0_0/* byval_arg */
	, &UnityAction_t290_1_0_0/* this_arg */
	, UnityAction_t290_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)pinvoke_delegate_wrapper_UnityAction_t290/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_t290)/* instance_size */
	, 0/* element_size */
	, sizeof(methodPointerType)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction`1
#include "UnityEngine_UnityEngine_Events_UnityAction_1.h"
extern Il2CppGenericContainer UnityAction_1_t1099_Il2CppGenericContainer;
extern TypeInfo UnityAction_1_t1099_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_1_t1099_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_1_t1099_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityAction_1_t1099_Il2CppGenericParametersArray[1] = 
{
	&UnityAction_1_t1099_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityAction_1_t1099_il2cpp_TypeInfo;
Il2CppGenericContainer UnityAction_1_t1099_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityAction_1_t1099_il2cpp_TypeInfo, 1, 0, UnityAction_1_t1099_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t1099_UnityAction_1__ctor_m6607_ParameterInfos[] = 
{
	{"object", 0, 134219388, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219389, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityAction`1::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m6607_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_1_t1099_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1099_UnityAction_1__ctor_m6607_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1576/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1099_gp_0_0_0_0;
extern Il2CppType UnityAction_1_t1099_gp_0_0_0_0;
static ParameterInfo UnityAction_1_t1099_UnityAction_1_Invoke_m6608_ParameterInfos[] = 
{
	{"arg0", 0, 134219390, &EmptyCustomAttributesCache, &UnityAction_1_t1099_gp_0_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityAction`1::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m6608_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1099_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1099_UnityAction_1_Invoke_m6608_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1577/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_1_t1099_gp_0_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t1099_UnityAction_1_BeginInvoke_m6609_ParameterInfos[] = 
{
	{"arg0", 0, 134219391, &EmptyCustomAttributesCache, &UnityAction_1_t1099_gp_0_0_0_0},
	{"callback", 1, 134219392, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134219393, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
// System.IAsyncResult UnityEngine.Events.UnityAction`1::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m6609_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1099_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1099_UnityAction_1_BeginInvoke_m6609_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1578/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t1099_UnityAction_1_EndInvoke_m6610_ParameterInfos[] = 
{
	{"result", 0, 134219394, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityAction`1::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m6610_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_1_t1099_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_1_t1099_UnityAction_1_EndInvoke_m6610_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1579/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_1_t1099_MethodInfos[] =
{
	&UnityAction_1__ctor_m6607_MethodInfo,
	&UnityAction_1_Invoke_m6608_MethodInfo,
	&UnityAction_1_BeginInvoke_m6609_MethodInfo,
	&UnityAction_1_EndInvoke_m6610_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t1099_0_0_0;
extern Il2CppType UnityAction_1_t1099_1_0_0;
struct UnityAction_1_t1099;
TypeInfo UnityAction_1_t1099_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t1099_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t1099_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityAction_1_t1099_0_0_0/* byval_arg */
	, &UnityAction_1_t1099_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityAction_1_t1099_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction`2
#include "UnityEngine_UnityEngine_Events_UnityAction_2.h"
extern Il2CppGenericContainer UnityAction_2_t1100_Il2CppGenericContainer;
extern TypeInfo UnityAction_2_t1100_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_2_t1100_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_2_t1100_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityAction_2_t1100_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_2_t1100_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_2_t1100_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityAction_2_t1100_Il2CppGenericParametersArray[2] = 
{
	&UnityAction_2_t1100_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_2_t1100_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityAction_2_t1100_il2cpp_TypeInfo;
Il2CppGenericContainer UnityAction_2_t1100_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityAction_2_t1100_il2cpp_TypeInfo, 2, 0, UnityAction_2_t1100_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_2_t1100_UnityAction_2__ctor_m6611_ParameterInfos[] = 
{
	{"object", 0, 134219395, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219396, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityAction`2::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_2__ctor_m6611_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_2_t1100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1100_UnityAction_2__ctor_m6611_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1580/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_2_t1100_gp_0_0_0_0;
extern Il2CppType UnityAction_2_t1100_gp_0_0_0_0;
extern Il2CppType UnityAction_2_t1100_gp_1_0_0_0;
extern Il2CppType UnityAction_2_t1100_gp_1_0_0_0;
static ParameterInfo UnityAction_2_t1100_UnityAction_2_Invoke_m6612_ParameterInfos[] = 
{
	{"arg0", 0, 134219397, &EmptyCustomAttributesCache, &UnityAction_2_t1100_gp_0_0_0_0},
	{"arg1", 1, 134219398, &EmptyCustomAttributesCache, &UnityAction_2_t1100_gp_1_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityAction`2::Invoke(T0,T1)
MethodInfo UnityAction_2_Invoke_m6612_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1100_UnityAction_2_Invoke_m6612_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1581/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_2_t1100_gp_0_0_0_0;
extern Il2CppType UnityAction_2_t1100_gp_1_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_2_t1100_UnityAction_2_BeginInvoke_m6613_ParameterInfos[] = 
{
	{"arg0", 0, 134219399, &EmptyCustomAttributesCache, &UnityAction_2_t1100_gp_0_0_0_0},
	{"arg1", 1, 134219400, &EmptyCustomAttributesCache, &UnityAction_2_t1100_gp_1_0_0_0},
	{"callback", 2, 134219401, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 3, 134219402, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
// System.IAsyncResult UnityEngine.Events.UnityAction`2::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
MethodInfo UnityAction_2_BeginInvoke_m6613_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1100_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1100_UnityAction_2_BeginInvoke_m6613_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1582/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_2_t1100_UnityAction_2_EndInvoke_m6614_ParameterInfos[] = 
{
	{"result", 0, 134219403, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityAction`2::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_2_EndInvoke_m6614_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_2_t1100_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_2_t1100_UnityAction_2_EndInvoke_m6614_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1583/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_2_t1100_MethodInfos[] =
{
	&UnityAction_2__ctor_m6611_MethodInfo,
	&UnityAction_2_Invoke_m6612_MethodInfo,
	&UnityAction_2_BeginInvoke_m6613_MethodInfo,
	&UnityAction_2_EndInvoke_m6614_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_2_t1100_0_0_0;
extern Il2CppType UnityAction_2_t1100_1_0_0;
struct UnityAction_2_t1100;
TypeInfo UnityAction_2_t1100_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_2_t1100_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_2_t1100_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityAction_2_t1100_0_0_0/* byval_arg */
	, &UnityAction_2_t1100_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityAction_2_t1100_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction`3
#include "UnityEngine_UnityEngine_Events_UnityAction_3.h"
extern Il2CppGenericContainer UnityAction_3_t1101_Il2CppGenericContainer;
extern TypeInfo UnityAction_3_t1101_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_3_t1101_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_3_t1101_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityAction_3_t1101_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_3_t1101_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_3_t1101_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo UnityAction_3_t1101_gp_T2_2_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_3_t1101_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_3_t1101_Il2CppGenericContainer, 2}, {NULL, "T2", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityAction_3_t1101_Il2CppGenericParametersArray[3] = 
{
	&UnityAction_3_t1101_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_3_t1101_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_3_t1101_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityAction_3_t1101_il2cpp_TypeInfo;
Il2CppGenericContainer UnityAction_3_t1101_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityAction_3_t1101_il2cpp_TypeInfo, 3, 0, UnityAction_3_t1101_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_3_t1101_UnityAction_3__ctor_m6615_ParameterInfos[] = 
{
	{"object", 0, 134219404, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219405, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityAction`3::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_3__ctor_m6615_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_3_t1101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1101_UnityAction_3__ctor_m6615_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1584/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_3_t1101_gp_0_0_0_0;
extern Il2CppType UnityAction_3_t1101_gp_0_0_0_0;
extern Il2CppType UnityAction_3_t1101_gp_1_0_0_0;
extern Il2CppType UnityAction_3_t1101_gp_1_0_0_0;
extern Il2CppType UnityAction_3_t1101_gp_2_0_0_0;
extern Il2CppType UnityAction_3_t1101_gp_2_0_0_0;
static ParameterInfo UnityAction_3_t1101_UnityAction_3_Invoke_m6616_ParameterInfos[] = 
{
	{"arg0", 0, 134219406, &EmptyCustomAttributesCache, &UnityAction_3_t1101_gp_0_0_0_0},
	{"arg1", 1, 134219407, &EmptyCustomAttributesCache, &UnityAction_3_t1101_gp_1_0_0_0},
	{"arg2", 2, 134219408, &EmptyCustomAttributesCache, &UnityAction_3_t1101_gp_2_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityAction`3::Invoke(T0,T1,T2)
MethodInfo UnityAction_3_Invoke_m6616_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1101_UnityAction_3_Invoke_m6616_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1585/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_3_t1101_gp_0_0_0_0;
extern Il2CppType UnityAction_3_t1101_gp_1_0_0_0;
extern Il2CppType UnityAction_3_t1101_gp_2_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_3_t1101_UnityAction_3_BeginInvoke_m6617_ParameterInfos[] = 
{
	{"arg0", 0, 134219409, &EmptyCustomAttributesCache, &UnityAction_3_t1101_gp_0_0_0_0},
	{"arg1", 1, 134219410, &EmptyCustomAttributesCache, &UnityAction_3_t1101_gp_1_0_0_0},
	{"arg2", 2, 134219411, &EmptyCustomAttributesCache, &UnityAction_3_t1101_gp_2_0_0_0},
	{"callback", 3, 134219412, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 4, 134219413, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
// System.IAsyncResult UnityEngine.Events.UnityAction`3::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
MethodInfo UnityAction_3_BeginInvoke_m6617_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1101_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1101_UnityAction_3_BeginInvoke_m6617_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 5/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1586/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_3_t1101_UnityAction_3_EndInvoke_m6618_ParameterInfos[] = 
{
	{"result", 0, 134219414, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityAction`3::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_3_EndInvoke_m6618_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_3_t1101_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_3_t1101_UnityAction_3_EndInvoke_m6618_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1587/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_3_t1101_MethodInfos[] =
{
	&UnityAction_3__ctor_m6615_MethodInfo,
	&UnityAction_3_Invoke_m6616_MethodInfo,
	&UnityAction_3_BeginInvoke_m6617_MethodInfo,
	&UnityAction_3_EndInvoke_m6618_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_3_t1101_0_0_0;
extern Il2CppType UnityAction_3_t1101_1_0_0;
struct UnityAction_3_t1101;
TypeInfo UnityAction_3_t1101_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_3_t1101_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_3_t1101_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityAction_3_t1101_0_0_0/* byval_arg */
	, &UnityAction_3_t1101_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityAction_3_t1101_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction`4
#include "UnityEngine_UnityEngine_Events_UnityAction_4.h"
extern Il2CppGenericContainer UnityAction_4_t1102_Il2CppGenericContainer;
extern TypeInfo UnityAction_4_t1102_gp_T0_0_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_4_t1102_gp_T0_0_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_4_t1102_Il2CppGenericContainer, 0}, {NULL, "T0", 0, 0, NULL} };
extern TypeInfo UnityAction_4_t1102_gp_T1_1_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_4_t1102_gp_T1_1_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_4_t1102_Il2CppGenericContainer, 1}, {NULL, "T1", 0, 0, NULL} };
extern TypeInfo UnityAction_4_t1102_gp_T2_2_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_4_t1102_gp_T2_2_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_4_t1102_Il2CppGenericContainer, 2}, {NULL, "T2", 0, 0, NULL} };
extern TypeInfo UnityAction_4_t1102_gp_T3_3_il2cpp_TypeInfo;
Il2CppGenericParamFull UnityAction_4_t1102_gp_T3_3_il2cpp_TypeInfo_GenericParamFull = { { &UnityAction_4_t1102_Il2CppGenericContainer, 3}, {NULL, "T3", 0, 0, NULL} };
static Il2CppGenericParamFull* UnityAction_4_t1102_Il2CppGenericParametersArray[4] = 
{
	&UnityAction_4_t1102_gp_T0_0_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1102_gp_T1_1_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1102_gp_T2_2_il2cpp_TypeInfo_GenericParamFull,
	&UnityAction_4_t1102_gp_T3_3_il2cpp_TypeInfo_GenericParamFull,
};
extern TypeInfo UnityAction_4_t1102_il2cpp_TypeInfo;
Il2CppGenericContainer UnityAction_4_t1102_Il2CppGenericContainer = { { NULL, NULL }, NULL, &UnityAction_4_t1102_il2cpp_TypeInfo, 4, 0, UnityAction_4_t1102_Il2CppGenericParametersArray };
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_4_t1102_UnityAction_4__ctor_m6619_ParameterInfos[] = 
{
	{"object", 0, 134219415, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134219416, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityAction`4::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_4__ctor_m6619_MethodInfo = 
{
	".ctor"/* name */
	, NULL/* method */
	, &UnityAction_4_t1102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1102_UnityAction_4__ctor_m6619_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1588/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_4_t1102_gp_0_0_0_0;
extern Il2CppType UnityAction_4_t1102_gp_0_0_0_0;
extern Il2CppType UnityAction_4_t1102_gp_1_0_0_0;
extern Il2CppType UnityAction_4_t1102_gp_1_0_0_0;
extern Il2CppType UnityAction_4_t1102_gp_2_0_0_0;
extern Il2CppType UnityAction_4_t1102_gp_2_0_0_0;
extern Il2CppType UnityAction_4_t1102_gp_3_0_0_0;
extern Il2CppType UnityAction_4_t1102_gp_3_0_0_0;
static ParameterInfo UnityAction_4_t1102_UnityAction_4_Invoke_m6620_ParameterInfos[] = 
{
	{"arg0", 0, 134219417, &EmptyCustomAttributesCache, &UnityAction_4_t1102_gp_0_0_0_0},
	{"arg1", 1, 134219418, &EmptyCustomAttributesCache, &UnityAction_4_t1102_gp_1_0_0_0},
	{"arg2", 2, 134219419, &EmptyCustomAttributesCache, &UnityAction_4_t1102_gp_2_0_0_0},
	{"arg3", 3, 134219420, &EmptyCustomAttributesCache, &UnityAction_4_t1102_gp_3_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityAction`4::Invoke(T0,T1,T2,T3)
MethodInfo UnityAction_4_Invoke_m6620_MethodInfo = 
{
	"Invoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1102_UnityAction_4_Invoke_m6620_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1589/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType UnityAction_4_t1102_gp_0_0_0_0;
extern Il2CppType UnityAction_4_t1102_gp_1_0_0_0;
extern Il2CppType UnityAction_4_t1102_gp_2_0_0_0;
extern Il2CppType UnityAction_4_t1102_gp_3_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_4_t1102_UnityAction_4_BeginInvoke_m6621_ParameterInfos[] = 
{
	{"arg0", 0, 134219421, &EmptyCustomAttributesCache, &UnityAction_4_t1102_gp_0_0_0_0},
	{"arg1", 1, 134219422, &EmptyCustomAttributesCache, &UnityAction_4_t1102_gp_1_0_0_0},
	{"arg2", 2, 134219423, &EmptyCustomAttributesCache, &UnityAction_4_t1102_gp_2_0_0_0},
	{"arg3", 3, 134219424, &EmptyCustomAttributesCache, &UnityAction_4_t1102_gp_3_0_0_0},
	{"callback", 4, 134219425, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 5, 134219426, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
// System.IAsyncResult UnityEngine.Events.UnityAction`4::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
MethodInfo UnityAction_4_BeginInvoke_m6621_MethodInfo = 
{
	"BeginInvoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1102_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1102_UnityAction_4_BeginInvoke_m6621_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1590/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_4_t1102_UnityAction_4_EndInvoke_m6622_ParameterInfos[] = 
{
	{"result", 0, 134219427, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
// System.Void UnityEngine.Events.UnityAction`4::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_4_EndInvoke_m6622_MethodInfo = 
{
	"EndInvoke"/* name */
	, NULL/* method */
	, &UnityAction_4_t1102_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, NULL/* invoker_method */
	, UnityAction_4_t1102_UnityAction_4_EndInvoke_m6622_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 1591/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UnityAction_4_t1102_MethodInfos[] =
{
	&UnityAction_4__ctor_m6619_MethodInfo,
	&UnityAction_4_Invoke_m6620_MethodInfo,
	&UnityAction_4_BeginInvoke_m6621_MethodInfo,
	&UnityAction_4_EndInvoke_m6622_MethodInfo,
	NULL
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_4_t1102_0_0_0;
extern Il2CppType UnityAction_4_t1102_1_0_0;
struct UnityAction_4_t1102;
TypeInfo UnityAction_4_t1102_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_4_t1102_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_4_t1102_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, NULL/* custom_attributes_cache */
	, NULL/* cast_class */
	, &UnityAction_4_t1102_0_0_0/* byval_arg */
	, &UnityAction_4_t1102_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, &UnityAction_4_t1102_Il2CppGenericContainer/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, NULL/* pinvoke_delegate_wrapper */
	, NULL/* marshal_to_native_func */
	, NULL/* marshal_from_native_func */
	, NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
