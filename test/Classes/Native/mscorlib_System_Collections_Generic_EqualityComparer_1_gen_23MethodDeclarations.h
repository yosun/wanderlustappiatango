﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<UnityEngine.RectTransform>
struct EqualityComparer_1_t3584;
// System.Object
struct Object_t;
// UnityEngine.RectTransform
struct RectTransform_t287;

// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.RectTransform>::.ctor()
// System.Collections.Generic.EqualityComparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_genMethodDeclarations.h"
#define EqualityComparer_1__ctor_m19893(__this, method) (void)EqualityComparer_1__ctor_m14712_gshared((EqualityComparer_1_t2853 *)__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.RectTransform>::.cctor()
#define EqualityComparer_1__cctor_m19894(__this/* static, unused */, method) (void)EqualityComparer_1__cctor_m14713_gshared((Object_t *)__this/* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.RectTransform>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m19895(__this, ___obj, method) (int32_t)EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14714_gshared((EqualityComparer_1_t2853 *)__this, (Object_t *)___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.RectTransform>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m19896(__this, ___x, ___y, method) (bool)EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14715_gshared((EqualityComparer_1_t2853 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.RectTransform>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.RectTransform>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.RectTransform>::get_Default()
#define EqualityComparer_1_get_Default_m19897(__this/* static, unused */, method) (EqualityComparer_1_t3584 *)EqualityComparer_1_get_Default_m14716_gshared((Object_t *)__this/* static, unused */, method)
