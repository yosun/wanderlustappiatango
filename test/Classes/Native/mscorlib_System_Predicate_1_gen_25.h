﻿#pragma once
#include <stdint.h>
// Vuforia.VirtualButton
struct VirtualButton_t595;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.VirtualButton>
struct Predicate_1_t3753  : public MulticastDelegate_t325
{
};
