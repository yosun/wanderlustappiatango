﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TurnOffBehaviour
struct TurnOffBehaviour_t79;

// System.Void Vuforia.TurnOffBehaviour::.ctor()
 void TurnOffBehaviour__ctor_m172 (TurnOffBehaviour_t79 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TurnOffBehaviour::Awake()
 void TurnOffBehaviour_Awake_m173 (TurnOffBehaviour_t79 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
