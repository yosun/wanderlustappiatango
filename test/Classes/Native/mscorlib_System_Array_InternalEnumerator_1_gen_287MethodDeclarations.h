﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.ImageTargetBuilder/FrameQuality>
struct InternalEnumerator_1_t3764;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetBuilder/FrameQuality>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m21064 (InternalEnumerator_1_t3764 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ImageTargetBuilder/FrameQuality>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21065 (InternalEnumerator_1_t3764 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetBuilder/FrameQuality>::Dispose()
 void InternalEnumerator_1_Dispose_m21066 (InternalEnumerator_1_t3764 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ImageTargetBuilder/FrameQuality>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m21067 (InternalEnumerator_1_t3764 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.ImageTargetBuilder/FrameQuality>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m21068 (InternalEnumerator_1_t3764 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
