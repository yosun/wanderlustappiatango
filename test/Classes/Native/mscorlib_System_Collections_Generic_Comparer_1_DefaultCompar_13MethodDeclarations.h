﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Canvas>
struct DefaultComparer_t3361;
// UnityEngine.Canvas
struct Canvas_t289;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Canvas>::.ctor()
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_0MethodDeclarations.h"
#define DefaultComparer__ctor_m18319(__this, method) (void)DefaultComparer__ctor_m14758_gshared((DefaultComparer_t2862 *)__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.Canvas>::Compare(T,T)
#define DefaultComparer_Compare_m18320(__this, ___x, ___y, method) (int32_t)DefaultComparer_Compare_m14759_gshared((DefaultComparer_t2862 *)__this, (Object_t *)___x, (Object_t *)___y, method)
