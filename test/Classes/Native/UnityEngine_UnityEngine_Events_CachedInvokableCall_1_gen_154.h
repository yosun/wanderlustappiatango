﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.StateMachineBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_156.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.StateMachineBehaviour>
struct CachedInvokableCall_1_t4756  : public InvokableCall_1_t4757
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.StateMachineBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
