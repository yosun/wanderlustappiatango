﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARUnityImpl
struct QCARUnityImpl_t664;
// Vuforia.QCARUnity/QCARHint
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_QCARHint.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"

// System.Void Vuforia.QCARUnityImpl::Deinit()
 void QCARUnityImpl_Deinit_m3022 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARUnityImpl::IsRendererDirty()
 bool QCARUnityImpl_IsRendererDirty_m3023 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARUnityImpl::SetHint(Vuforia.QCARUnity/QCARHint,System.Int32)
 bool QCARUnityImpl_SetHint_m3024 (Object_t * __this/* static, unused */, int32_t ___hint, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARUnityImpl::SetHint(System.Int32,System.Int32)
 bool QCARUnityImpl_SetHint_m3025 (Object_t * __this/* static, unused */, int32_t ___hint, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Vuforia.QCARUnityImpl::GetProjectionGL(System.Single,System.Single,UnityEngine.ScreenOrientation)
 Matrix4x4_t176  QCARUnityImpl_GetProjectionGL_m3026 (Object_t * __this/* static, unused */, float ___nearPlane, float ___farPlane, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARUnityImpl::SetApplicationEnvironment()
 void QCARUnityImpl_SetApplicationEnvironment_m3027 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARUnityImpl::OnPause()
 void QCARUnityImpl_OnPause_m3028 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARUnityImpl::OnResume()
 void QCARUnityImpl_OnResume_m3029 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARUnityImpl::SetRendererDirty()
 void QCARUnityImpl_SetRendererDirty_m3030 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARUnityImpl::.cctor()
 void QCARUnityImpl__cctor_m3031 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
