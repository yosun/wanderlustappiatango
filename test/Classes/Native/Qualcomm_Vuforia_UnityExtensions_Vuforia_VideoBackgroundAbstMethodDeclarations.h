﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoBackgroundAbstractBehaviour
struct VideoBackgroundAbstractBehaviour_t84;

// System.Boolean Vuforia.VideoBackgroundAbstractBehaviour::get_VideoBackGroundMirrored()
 bool VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::set_VideoBackGroundMirrored(System.Boolean)
 void VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202 (VideoBackgroundAbstractBehaviour_t84 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ResetBackgroundPlane(System.Boolean)
 void VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4203 (VideoBackgroundAbstractBehaviour_t84 * __this, bool ___disable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::SetStereoDepth(System.Single)
 void VideoBackgroundAbstractBehaviour_SetStereoDepth_m4204 (VideoBackgroundAbstractBehaviour_t84 * __this, float ___depth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ApplyStereoDepthToMatrices()
 void VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4205 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::RenderOnUpdate()
 void VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4206 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::Awake()
 void VideoBackgroundAbstractBehaviour_Awake_m4207 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPreRender()
 void VideoBackgroundAbstractBehaviour_OnPreRender_m4208 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPostRender()
 void VideoBackgroundAbstractBehaviour_OnPostRender_m4209 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnDestroy()
 void VideoBackgroundAbstractBehaviour_OnDestroy_m4210 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::.ctor()
 void VideoBackgroundAbstractBehaviour__ctor_m612 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
