﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.SmartTerrainTrackable>
struct Comparer_1_t3925;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.SmartTerrainTrackable>
struct Comparer_1_t3925  : public Object_t
{
};
struct Comparer_1_t3925_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.SmartTerrainTrackable>::_default
	Comparer_1_t3925 * ____default_0;
};
