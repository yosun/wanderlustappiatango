﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DSASignatureDescription
struct DSASignatureDescription_t2123;

// System.Void System.Security.Cryptography.DSASignatureDescription::.ctor()
 void DSASignatureDescription__ctor_m12120 (DSASignatureDescription_t2123 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
