﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AnimationEvent
struct AnimationEvent_t1014;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t117;
struct Object_t117_marshaled;
// UnityEngine.AnimationState
struct AnimationState_t1013;
// UnityEngine.SendMessageOptions
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
// UnityEngine.AnimatorClipInfo
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"

// System.Void UnityEngine.AnimationEvent::.ctor()
 void AnimationEvent__ctor_m6121 (AnimationEvent_t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.AnimationEvent::get_data()
 String_t* AnimationEvent_get_data_m6122 (AnimationEvent_t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_data(System.String)
 void AnimationEvent_set_data_m6123 (AnimationEvent_t1014 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.AnimationEvent::get_stringParameter()
 String_t* AnimationEvent_get_stringParameter_m6124 (AnimationEvent_t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_stringParameter(System.String)
 void AnimationEvent_set_stringParameter_m6125 (AnimationEvent_t1014 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationEvent::get_floatParameter()
 float AnimationEvent_get_floatParameter_m6126 (AnimationEvent_t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_floatParameter(System.Single)
 void AnimationEvent_set_floatParameter_m6127 (AnimationEvent_t1014 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimationEvent::get_intParameter()
 int32_t AnimationEvent_get_intParameter_m6128 (AnimationEvent_t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_intParameter(System.Int32)
 void AnimationEvent_set_intParameter_m6129 (AnimationEvent_t1014 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AnimationEvent::get_objectReferenceParameter()
 Object_t117 * AnimationEvent_get_objectReferenceParameter_m6130 (AnimationEvent_t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_objectReferenceParameter(UnityEngine.Object)
 void AnimationEvent_set_objectReferenceParameter_m6131 (AnimationEvent_t1014 * __this, Object_t117 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.AnimationEvent::get_functionName()
 String_t* AnimationEvent_get_functionName_m6132 (AnimationEvent_t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_functionName(System.String)
 void AnimationEvent_set_functionName_m6133 (AnimationEvent_t1014 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationEvent::get_time()
 float AnimationEvent_get_time_m6134 (AnimationEvent_t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_time(System.Single)
 void AnimationEvent_set_time_m6135 (AnimationEvent_t1014 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SendMessageOptions UnityEngine.AnimationEvent::get_messageOptions()
 int32_t AnimationEvent_get_messageOptions_m6136 (AnimationEvent_t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationEvent::set_messageOptions(UnityEngine.SendMessageOptions)
 void AnimationEvent_set_messageOptions_m6137 (AnimationEvent_t1014 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByLegacy()
 bool AnimationEvent_get_isFiredByLegacy_m6138 (AnimationEvent_t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByAnimator()
 bool AnimationEvent_get_isFiredByAnimator_m6139 (AnimationEvent_t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationState UnityEngine.AnimationEvent::get_animationState()
 AnimationState_t1013 * AnimationEvent_get_animationState_m6140 (AnimationEvent_t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::get_animatorStateInfo()
 AnimatorStateInfo_t1015  AnimationEvent_get_animatorStateInfo_m6141 (AnimationEvent_t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::get_animatorClipInfo()
 AnimatorClipInfo_t1016  AnimationEvent_get_animatorClipInfo_m6142 (AnimationEvent_t1014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
