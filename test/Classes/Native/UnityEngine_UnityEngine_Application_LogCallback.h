﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// UnityEngine.LogType
#include "UnityEngine_UnityEngine_LogType.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo LogType_t916_il2cpp_TypeInfo;
// UnityEngine.Application/LogCallback
struct LogCallback_t989  : public MulticastDelegate_t325
{
};
