﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>
struct Enumerator_t3626;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t396;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct Stack_1_t3623;

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"
#define Enumerator__ctor_m20087(__this, ___t, method) (void)Enumerator__ctor_m15728_gshared((Enumerator_t3036 *)__this, (Stack_1_t3035 *)___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20088(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m15729_gshared((Enumerator_t3036 *)__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::Dispose()
#define Enumerator_Dispose_m20089(__this, method) (void)Enumerator_Dispose_m15730_gshared((Enumerator_t3036 *)__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::MoveNext()
#define Enumerator_MoveNext_m20090(__this, method) (bool)Enumerator_MoveNext_m15731_gshared((Enumerator_t3036 *)__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Current()
#define Enumerator_get_Current_m20091(__this, method) (List_1_t396 *)Enumerator_get_Current_m15732_gshared((Enumerator_t3036 *)__this, method)
