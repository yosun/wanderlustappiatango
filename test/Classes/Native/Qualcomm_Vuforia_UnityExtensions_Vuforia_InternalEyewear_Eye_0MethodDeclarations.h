﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.InternalEyewear/EyewearCalibrationReading
struct EyewearCalibrationReading_t547;
struct EyewearCalibrationReading_t547_marshaled;

void EyewearCalibrationReading_t547_marshal(const EyewearCalibrationReading_t547& unmarshaled, EyewearCalibrationReading_t547_marshaled& marshaled);
void EyewearCalibrationReading_t547_marshal_back(const EyewearCalibrationReading_t547_marshaled& marshaled, EyewearCalibrationReading_t547& unmarshaled);
void EyewearCalibrationReading_t547_marshal_cleanup(EyewearCalibrationReading_t547_marshaled& marshaled);
