﻿#pragma once
#include <stdint.h>
// System.Collections.ArrayList
struct ArrayList_t1308;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.CollectionBase
struct CollectionBase_t1369  : public Object_t
{
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t1308 * ___list_0;
};
