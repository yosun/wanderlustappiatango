﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<SetRenderQueue>
struct UnityAction_1_t2745;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<SetRenderQueue>
struct InvokableCall_1_t2744  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<SetRenderQueue>::Delegate
	UnityAction_1_t2745 * ___Delegate_0;
};
