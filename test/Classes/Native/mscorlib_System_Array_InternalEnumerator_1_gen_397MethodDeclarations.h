﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.FontStyle>
struct InternalEnumerator_1_t4557;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.FontStyle
#include "UnityEngine_UnityEngine_FontStyle.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.FontStyle>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m27985 (InternalEnumerator_1_t4557 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.FontStyle>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27986 (InternalEnumerator_1_t4557 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.FontStyle>::Dispose()
 void InternalEnumerator_1_Dispose_m27987 (InternalEnumerator_1_t4557 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.FontStyle>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m27988 (InternalEnumerator_1_t4557 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.FontStyle>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m27989 (InternalEnumerator_1_t4557 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
