﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Activation.AppDomainLevelActivator
struct AppDomainLevelActivator_t1983;
// System.String
struct String_t;
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t1980;

// System.Void System.Runtime.Remoting.Activation.AppDomainLevelActivator::.ctor(System.String,System.Runtime.Remoting.Activation.IActivator)
 void AppDomainLevelActivator__ctor_m11443 (AppDomainLevelActivator_t1983 * __this, String_t* ___activationUrl, Object_t * ___next, MethodInfo* method) IL2CPP_METHOD_ATTR;
