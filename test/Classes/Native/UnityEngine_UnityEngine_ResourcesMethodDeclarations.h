﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Resources
struct Resources_t983;
// UnityEngine.Object
struct Object_t117;
struct Object_t117_marshaled;
// System.String
struct String_t;
// System.Type
struct Type_t;
// UnityEngine.AsyncOperation
struct AsyncOperation_t909;
struct AsyncOperation_t909_marshaled;

// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
 Object_t117 * Resources_Load_m5909 (Object_t * __this/* static, unused */, String_t* ___path, Type_t * ___systemTypeInstance, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
 AsyncOperation_t909 * Resources_UnloadUnusedAssets_m5212 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
