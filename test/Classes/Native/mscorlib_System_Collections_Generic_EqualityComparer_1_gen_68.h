﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Event>
struct EqualityComparer_1_t4780;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Event>
struct EqualityComparer_1_t4780  : public Object_t
{
};
struct EqualityComparer_1_t4780_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Event>::_default
	EqualityComparer_1_t4780 * ____default_0;
};
