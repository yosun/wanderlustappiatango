﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct Enumerator_t3316;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t462;
// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct Dictionary_2_t282;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_33MethodDeclarations.h"
#define Enumerator__ctor_m17954(__this, ___host, method) (void)Enumerator__ctor_m17701_gshared((Enumerator_t3287 *)__this, (Dictionary_2_t3275 *)___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17955(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m17702_gshared((Enumerator_t3287 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Dispose()
#define Enumerator_Dispose_m17956(__this, method) (void)Enumerator_Dispose_m17703_gshared((Enumerator_t3287 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::MoveNext()
#define Enumerator_MoveNext_m17957(__this, method) (bool)Enumerator_MoveNext_m17704_gshared((Enumerator_t3287 *)__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Current()
#define Enumerator_get_Current_m17958(__this, method) (List_1_t462 *)Enumerator_get_Current_m17705_gshared((Enumerator_t3287 *)__this, method)
