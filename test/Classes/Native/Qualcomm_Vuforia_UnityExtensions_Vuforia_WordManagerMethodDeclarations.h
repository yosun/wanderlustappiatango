﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordManager
struct WordManager_t688;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult>
struct IEnumerable_1_t689;
// System.Collections.Generic.IEnumerable`1<Vuforia.Word>
struct IEnumerable_1_t690;
// Vuforia.Word
struct Word_t691;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t60;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordAbstractBehaviour>
struct IEnumerable_1_t692;

// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult> Vuforia.WordManager::GetActiveWordResults()
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult> Vuforia.WordManager::GetNewWords()
// System.Collections.Generic.IEnumerable`1<Vuforia.Word> Vuforia.WordManager::GetLostWords()
// System.Boolean Vuforia.WordManager::TryGetWordBehaviour(Vuforia.Word,Vuforia.WordAbstractBehaviour&)
// System.Collections.Generic.IEnumerable`1<Vuforia.WordAbstractBehaviour> Vuforia.WordManager::GetTrackableBehaviours()
// System.Void Vuforia.WordManager::DestroyWordBehaviour(Vuforia.WordAbstractBehaviour,System.Boolean)
// System.Void Vuforia.WordManager::.ctor()
 void WordManager__ctor_m3096 (WordManager_t688 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
