﻿#pragma once
#include <stdint.h>
// System.Action`1<UnityEngine.Font>
struct Action_1_t463;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t1027;
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Font
struct Font_t280  : public Object_t117
{
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t1027 * ___m_FontTextureRebuildCallback_3;
};
struct Font_t280_StaticFields{
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_t463 * ___textureRebuilt_2;
};
