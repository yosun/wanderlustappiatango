﻿#pragma once
#include <stdint.h>
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t1034;
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t1035;
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t1036;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.AttributeHelperEngine
struct AttributeHelperEngine_t1037  : public Object_t
{
};
struct AttributeHelperEngine_t1037_StaticFields{
	// UnityEngine.DisallowMultipleComponent[] UnityEngine.AttributeHelperEngine::_disallowMultipleComponentArray
	DisallowMultipleComponentU5BU5D_t1034* ____disallowMultipleComponentArray_0;
	// UnityEngine.ExecuteInEditMode[] UnityEngine.AttributeHelperEngine::_executeInEditModeArray
	ExecuteInEditModeU5BU5D_t1035* ____executeInEditModeArray_1;
	// UnityEngine.RequireComponent[] UnityEngine.AttributeHelperEngine::_requireComponentArray
	RequireComponentU5BU5D_t1036* ____requireComponentArray_2;
};
