﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Time
struct Time_t1002;

// System.Single UnityEngine.Time::get_deltaTime()
 float Time_get_deltaTime_m235 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_unscaledTime()
 float Time_get_unscaledTime_m2006 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
 float Time_get_unscaledDeltaTime_m2067 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
