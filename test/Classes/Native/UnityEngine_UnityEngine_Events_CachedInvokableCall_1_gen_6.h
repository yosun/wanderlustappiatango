﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<GyroCameraYo>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_2.h"
// UnityEngine.Events.CachedInvokableCall`1<GyroCameraYo>
struct CachedInvokableCall_1_t2722  : public InvokableCall_1_t2723
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<GyroCameraYo>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
