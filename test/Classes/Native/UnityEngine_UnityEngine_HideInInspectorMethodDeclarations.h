﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.HideInInspector
struct HideInInspector_t777;

// System.Void UnityEngine.HideInInspector::.ctor()
 void HideInInspector__ctor_m4300 (HideInInspector_t777 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
