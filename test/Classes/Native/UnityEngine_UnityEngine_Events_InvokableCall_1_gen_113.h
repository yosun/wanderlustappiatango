﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.VirtualButtonAbstractBehaviour>
struct UnityAction_1_t4405;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonAbstractBehaviour>
struct InvokableCall_1_t4404  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.VirtualButtonAbstractBehaviour>::Delegate
	UnityAction_1_t4405 * ___Delegate_0;
};
