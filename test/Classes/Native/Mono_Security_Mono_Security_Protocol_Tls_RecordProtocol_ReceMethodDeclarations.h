﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
struct ReceiveRecordAsyncResult_t1615;
// System.IO.Stream
struct Stream_t1599;
// System.Byte[]
struct ByteU5BU5D_t609;
// System.Object
struct Object_t;
// System.Exception
struct Exception_t152;
// System.Threading.WaitHandle
struct WaitHandle_t1616;
// System.AsyncCallback
struct AsyncCallback_t200;

// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::.ctor(System.AsyncCallback,System.Object,System.Byte[],System.IO.Stream)
 void ReceiveRecordAsyncResult__ctor_m8450 (ReceiveRecordAsyncResult_t1615 * __this, AsyncCallback_t200 * ___userCallback, Object_t * ___userState, ByteU5BU5D_t609* ___initialBuffer, Stream_t1599 * ___record, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_Record()
 Stream_t1599 * ReceiveRecordAsyncResult_get_Record_m8451 (ReceiveRecordAsyncResult_t1615 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_ResultingBuffer()
 ByteU5BU5D_t609* ReceiveRecordAsyncResult_get_ResultingBuffer_m8452 (ReceiveRecordAsyncResult_t1615 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_InitialBuffer()
 ByteU5BU5D_t609* ReceiveRecordAsyncResult_get_InitialBuffer_m8453 (ReceiveRecordAsyncResult_t1615 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncState()
 Object_t * ReceiveRecordAsyncResult_get_AsyncState_m8454 (ReceiveRecordAsyncResult_t1615 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncException()
 Exception_t152 * ReceiveRecordAsyncResult_get_AsyncException_m8455 (ReceiveRecordAsyncResult_t1615 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_CompletedWithError()
 bool ReceiveRecordAsyncResult_get_CompletedWithError_m8456 (ReceiveRecordAsyncResult_t1615 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncWaitHandle()
 WaitHandle_t1616 * ReceiveRecordAsyncResult_get_AsyncWaitHandle_m8457 (ReceiveRecordAsyncResult_t1615 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_IsCompleted()
 bool ReceiveRecordAsyncResult_get_IsCompleted_m8458 (ReceiveRecordAsyncResult_t1615 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception,System.Byte[])
 void ReceiveRecordAsyncResult_SetComplete_m8459 (ReceiveRecordAsyncResult_t1615 * __this, Exception_t152 * ___ex, ByteU5BU5D_t609* ___resultingBuffer, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception)
 void ReceiveRecordAsyncResult_SetComplete_m8460 (ReceiveRecordAsyncResult_t1615 * __this, Exception_t152 * ___ex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Byte[])
 void ReceiveRecordAsyncResult_SetComplete_m8461 (ReceiveRecordAsyncResult_t1615 * __this, ByteU5BU5D_t609* ___resultingBuffer, MethodInfo* method) IL2CPP_METHOD_ATTR;
