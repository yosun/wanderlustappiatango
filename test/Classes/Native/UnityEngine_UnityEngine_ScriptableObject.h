﻿#pragma once
#include <stdint.h>
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.ScriptableObject
struct ScriptableObject_t919  : public Object_t117
{
};
// Native definition for marshalling of: UnityEngine.ScriptableObject
struct ScriptableObject_t919_marshaled
{
};
