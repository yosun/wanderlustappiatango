﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.ContentSizeFitter>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_76.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.ContentSizeFitter>
struct CachedInvokableCall_1_t3563  : public InvokableCall_1_t3564
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.ContentSizeFitter>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
