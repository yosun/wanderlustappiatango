﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.GraphicRaycaster>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_58.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.GraphicRaycaster>
struct CachedInvokableCall_1_t3363  : public InvokableCall_1_t3364
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.GraphicRaycaster>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
