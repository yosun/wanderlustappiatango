﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Misc
struct Misc_t335;
// UnityEngine.Object
struct Object_t117;
struct Object_t117_marshaled;

// System.Void UnityEngine.UI.Misc::Destroy(UnityEngine.Object)
 void Misc_Destroy_m1312 (Object_t * __this/* static, unused */, Object_t117 * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Misc::DestroyImmediate(UnityEngine.Object)
 void Misc_DestroyImmediate_m1313 (Object_t * __this/* static, unused */, Object_t117 * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
