﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>
struct InternalEnumerator_1_t3206;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.RaycastHit2D
#include "UnityEngine_UnityEngine_RaycastHit2D.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m17063 (InternalEnumerator_1_t3206 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17064 (InternalEnumerator_1_t3206 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::Dispose()
 void InternalEnumerator_1_Dispose_m17065 (InternalEnumerator_1_t3206 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m17066 (InternalEnumerator_1_t3206 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::get_Current()
 RaycastHit2D_t447  InternalEnumerator_1_get_Current_m17067 (InternalEnumerator_1_t3206 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
