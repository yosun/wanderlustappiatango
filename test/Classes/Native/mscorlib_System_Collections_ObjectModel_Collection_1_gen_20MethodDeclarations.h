﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>
struct Collection_1_t3695;
// System.Object
struct Object_t;
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t563;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// Vuforia.ICloudRecoEventHandler[]
struct ICloudRecoEventHandlerU5BU5D_t3686;
// System.Collections.Generic.IEnumerator`1<Vuforia.ICloudRecoEventHandler>
struct IEnumerator_1_t3688;
// System.Collections.Generic.IList`1<Vuforia.ICloudRecoEventHandler>
struct IList_1_t3694;

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::.ctor()
// System.Collections.ObjectModel.Collection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_Collection_1_genMethodDeclarations.h"
#define Collection_1__ctor_m20496(__this, method) (void)Collection_1__ctor_m14676_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m20497(__this, method) (bool)Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14677_gshared((Collection_1_t2852 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m20498(__this, ___array, ___index, method) (void)Collection_1_System_Collections_ICollection_CopyTo_m14678_gshared((Collection_1_t2852 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m20499(__this, method) (Object_t *)Collection_1_System_Collections_IEnumerable_GetEnumerator_m14679_gshared((Collection_1_t2852 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m20500(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_Add_m14680_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m20501(__this, ___value, method) (bool)Collection_1_System_Collections_IList_Contains_m14681_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m20502(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_IndexOf_m14682_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m20503(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_Insert_m14683_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m20504(__this, ___value, method) (void)Collection_1_System_Collections_IList_Remove_m14684_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m20505(__this, method) (bool)Collection_1_System_Collections_ICollection_get_IsSynchronized_m14685_gshared((Collection_1_t2852 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m20506(__this, method) (Object_t *)Collection_1_System_Collections_ICollection_get_SyncRoot_m14686_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m20507(__this, method) (bool)Collection_1_System_Collections_IList_get_IsFixedSize_m14687_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m20508(__this, method) (bool)Collection_1_System_Collections_IList_get_IsReadOnly_m14688_gshared((Collection_1_t2852 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m20509(__this, ___index, method) (Object_t *)Collection_1_System_Collections_IList_get_Item_m14689_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m20510(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_set_Item_m14690_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::Add(T)
#define Collection_1_Add_m20511(__this, ___item, method) (void)Collection_1_Add_m14691_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::Clear()
#define Collection_1_Clear_m20512(__this, method) (void)Collection_1_Clear_m14692_gshared((Collection_1_t2852 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::ClearItems()
#define Collection_1_ClearItems_m20513(__this, method) (void)Collection_1_ClearItems_m14693_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::Contains(T)
#define Collection_1_Contains_m20514(__this, ___item, method) (bool)Collection_1_Contains_m14694_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m20515(__this, ___array, ___index, method) (void)Collection_1_CopyTo_m14695_gshared((Collection_1_t2852 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::GetEnumerator()
#define Collection_1_GetEnumerator_m20516(__this, method) (Object_t*)Collection_1_GetEnumerator_m14696_gshared((Collection_1_t2852 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::IndexOf(T)
#define Collection_1_IndexOf_m20517(__this, ___item, method) (int32_t)Collection_1_IndexOf_m14697_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::Insert(System.Int32,T)
#define Collection_1_Insert_m20518(__this, ___index, ___item, method) (void)Collection_1_Insert_m14698_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m20519(__this, ___index, ___item, method) (void)Collection_1_InsertItem_m14699_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::Remove(T)
#define Collection_1_Remove_m20520(__this, ___item, method) (bool)Collection_1_Remove_m14700_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m20521(__this, ___index, method) (void)Collection_1_RemoveAt_m14701_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m20522(__this, ___index, method) (void)Collection_1_RemoveItem_m14702_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::get_Count()
#define Collection_1_get_Count_m20523(__this, method) (int32_t)Collection_1_get_Count_m14703_gshared((Collection_1_t2852 *)__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::get_Item(System.Int32)
#define Collection_1_get_Item_m20524(__this, ___index, method) (Object_t *)Collection_1_get_Item_m14704_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m20525(__this, ___index, ___value, method) (void)Collection_1_set_Item_m14705_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m20526(__this, ___index, ___item, method) (void)Collection_1_SetItem_m14706_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m20527(__this/* static, unused */, ___item, method) (bool)Collection_1_IsValidItem_m14707_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m20528(__this/* static, unused */, ___item, method) (Object_t *)Collection_1_ConvertItem_m14708_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m20529(__this/* static, unused */, ___list, method) (void)Collection_1_CheckWritable_m14709_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m20530(__this/* static, unused */, ___list, method) (bool)Collection_1_IsSynchronized_m14710_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.ICloudRecoEventHandler>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m20531(__this/* static, unused */, ___list, method) (bool)Collection_1_IsFixedSize_m14711_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
