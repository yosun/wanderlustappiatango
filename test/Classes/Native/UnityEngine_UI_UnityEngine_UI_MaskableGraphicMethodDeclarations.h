﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t313;
// UnityEngine.Material
struct Material_t4;

// System.Void UnityEngine.UI.MaskableGraphic::.ctor()
 void MaskableGraphic__ctor_m1299 (MaskableGraphic_t313 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.MaskableGraphic::get_maskable()
 bool MaskableGraphic_get_maskable_m1300 (MaskableGraphic_t313 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::set_maskable(System.Boolean)
 void MaskableGraphic_set_maskable_m1301 (MaskableGraphic_t313 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.UI.MaskableGraphic::get_material()
 Material_t4 * MaskableGraphic_get_material_m1302 (MaskableGraphic_t313 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::set_material(UnityEngine.Material)
 void MaskableGraphic_set_material_m1303 (MaskableGraphic_t313 * __this, Material_t4 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::UpdateInternalState()
 void MaskableGraphic_UpdateInternalState_m1304 (MaskableGraphic_t313 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::OnEnable()
 void MaskableGraphic_OnEnable_m1305 (MaskableGraphic_t313 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::OnDisable()
 void MaskableGraphic_OnDisable_m1306 (MaskableGraphic_t313 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::OnTransformParentChanged()
 void MaskableGraphic_OnTransformParentChanged_m1307 (MaskableGraphic_t313 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::ParentMaskStateChanged()
 void MaskableGraphic_ParentMaskStateChanged_m1308 (MaskableGraphic_t313 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::ClearMaskMaterial()
 void MaskableGraphic_ClearMaskMaterial_m1309 (MaskableGraphic_t313 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.MaskableGraphic::SetMaterialDirty()
 void MaskableGraphic_SetMaterialDirty_m1310 (MaskableGraphic_t313 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.MaskableGraphic::GetStencilForGraphic()
 int32_t MaskableGraphic_GetStencilForGraphic_m1311 (MaskableGraphic_t313 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
