﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.Decoder
struct Decoder_t1862;
// System.Text.DecoderFallback
struct DecoderFallback_t2143;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2142;
// System.Byte[]
struct ByteU5BU5D_t609;
// System.Char[]
struct CharU5BU5D_t108;

// System.Void System.Text.Decoder::.ctor()
 void Decoder__ctor_m12225 (Decoder_t1862 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Decoder::set_Fallback(System.Text.DecoderFallback)
 void Decoder_set_Fallback_m12226 (Decoder_t1862 * __this, DecoderFallback_t2143 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.Decoder::get_FallbackBuffer()
 DecoderFallbackBuffer_t2142 * Decoder_get_FallbackBuffer_m12227 (Decoder_t1862 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.Decoder::GetChars(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32)
