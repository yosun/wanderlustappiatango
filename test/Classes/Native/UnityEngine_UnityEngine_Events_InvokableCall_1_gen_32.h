﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.SurfaceBehaviour>
struct UnityAction_1_t2937;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>
struct InvokableCall_1_t2936  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>::Delegate
	UnityAction_1_t2937 * ___Delegate_0;
};
