﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.ImageTargetAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_103.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ImageTargetAbstractBehaviour>
struct CachedInvokableCall_1_t4282  : public InvokableCall_1_t4283
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.ImageTargetAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
