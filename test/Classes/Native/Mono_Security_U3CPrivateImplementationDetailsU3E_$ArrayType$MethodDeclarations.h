﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$3132
struct $ArrayType$3132_t1647;
struct $ArrayType$3132_t1647_marshaled;

void $ArrayType$3132_t1647_marshal(const $ArrayType$3132_t1647& unmarshaled, $ArrayType$3132_t1647_marshaled& marshaled);
void $ArrayType$3132_t1647_marshal_back(const $ArrayType$3132_t1647_marshaled& marshaled, $ArrayType$3132_t1647& unmarshaled);
void $ArrayType$3132_t1647_marshal_cleanup($ArrayType$3132_t1647_marshaled& marshaled);
