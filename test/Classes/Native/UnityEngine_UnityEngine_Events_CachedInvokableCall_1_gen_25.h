﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.ComponentFactoryStarterBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_21.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ComponentFactoryStarterBehaviour>
struct CachedInvokableCall_1_t2836  : public InvokableCall_1_t2837
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.ComponentFactoryStarterBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
