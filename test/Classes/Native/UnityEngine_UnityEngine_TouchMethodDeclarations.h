﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Touch
struct Touch_t104;
struct Touch_t104_marshaled;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.TouchPhase
#include "UnityEngine_UnityEngine_TouchPhase.h"

// System.Int32 UnityEngine.Touch::get_fingerId()
 int32_t Touch_get_fingerId_m1980 (Touch_t104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
 Vector2_t9  Touch_get_position_m263 (Touch_t104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
 Vector2_t9  Touch_get_deltaPosition_m264 (Touch_t104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
 int32_t Touch_get_phase_m262 (Touch_t104 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void Touch_t104_marshal(const Touch_t104& unmarshaled, Touch_t104_marshaled& marshaled);
void Touch_t104_marshal_back(const Touch_t104_marshaled& marshaled, Touch_t104& unmarshaled);
void Touch_t104_marshal_cleanup(Touch_t104_marshaled& marshaled);
