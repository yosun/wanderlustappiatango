﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CylinderTargetImpl
struct CylinderTargetImpl_t597;
// System.String
struct String_t;
// Vuforia.DataSet
struct DataSet_t568;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Vuforia.CylinderTargetImpl::.ctor(System.String,System.Int32,Vuforia.DataSet)
 void CylinderTargetImpl__ctor_m2827 (CylinderTargetImpl_t597 * __this, String_t* ___name, int32_t ___id, DataSet_t568 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetImpl::SetSize(UnityEngine.Vector3)
 void CylinderTargetImpl_SetSize_m2828 (CylinderTargetImpl_t597 * __this, Vector3_t13  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.CylinderTargetImpl::GetSideLength()
 float CylinderTargetImpl_GetSideLength_m2829 (CylinderTargetImpl_t597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.CylinderTargetImpl::GetTopDiameter()
 float CylinderTargetImpl_GetTopDiameter_m2830 (CylinderTargetImpl_t597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.CylinderTargetImpl::GetBottomDiameter()
 float CylinderTargetImpl_GetBottomDiameter_m2831 (CylinderTargetImpl_t597 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetImpl::SetSideLength(System.Single)
 bool CylinderTargetImpl_SetSideLength_m2832 (CylinderTargetImpl_t597 * __this, float ___sideLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetImpl::SetTopDiameter(System.Single)
 bool CylinderTargetImpl_SetTopDiameter_m2833 (CylinderTargetImpl_t597 * __this, float ___topDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CylinderTargetImpl::SetBottomDiameter(System.Single)
 bool CylinderTargetImpl_SetBottomDiameter_m2834 (CylinderTargetImpl_t597 * __this, float ___bottomDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CylinderTargetImpl::ScaleCylinder(System.Single)
 void CylinderTargetImpl_ScaleCylinder_m2835 (CylinderTargetImpl_t597 * __this, float ___scale, MethodInfo* method) IL2CPP_METHOD_ATTR;
