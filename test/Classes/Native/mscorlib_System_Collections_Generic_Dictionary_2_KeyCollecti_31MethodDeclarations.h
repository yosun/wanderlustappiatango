﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>
struct KeyCollection_t3997;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.WordAbstractBehaviour>
struct Dictionary_2_t696;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3161;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Int32[]
struct Int32U5BU5D_t21;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.WordAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_32.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void KeyCollection__ctor_m23403 (KeyCollection_t3997 * __this, Dictionary_2_t696 * ___dictionary, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
 void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m23404 (KeyCollection_t3997 * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Clear()
 void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m23405 (KeyCollection_t3997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
 bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m23406 (KeyCollection_t3997 * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
 bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m23407 (KeyCollection_t3997 * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
 Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m23408 (KeyCollection_t3997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void KeyCollection_System_Collections_ICollection_CopyTo_m23409 (KeyCollection_t3997 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m23410 (KeyCollection_t3997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
 bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m23411 (KeyCollection_t3997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
 bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m23412 (KeyCollection_t3997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
 Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m23413 (KeyCollection_t3997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>::CopyTo(TKey[],System.Int32)
 void KeyCollection_CopyTo_m23414 (KeyCollection_t3997 * __this, Int32U5BU5D_t21* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>::GetEnumerator()
 Enumerator_t4001  KeyCollection_GetEnumerator_m23415 (KeyCollection_t3997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.WordAbstractBehaviour>::get_Count()
 int32_t KeyCollection_get_Count_m23416 (KeyCollection_t3997 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
