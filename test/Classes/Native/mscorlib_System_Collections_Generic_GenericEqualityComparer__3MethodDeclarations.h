﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>
struct GenericEqualityComparer_1_t2685;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::.ctor()
 void GenericEqualityComparer_1__ctor_m13959 (GenericEqualityComparer_1_t2685 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::GetHashCode(T)
 int32_t GenericEqualityComparer_1_GetHashCode_m31415 (GenericEqualityComparer_1_t2685 * __this, TimeSpan_t113  ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::Equals(T,T)
 bool GenericEqualityComparer_1_Equals_m31416 (GenericEqualityComparer_1_t2685 * __this, TimeSpan_t113  ___x, TimeSpan_t113  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
