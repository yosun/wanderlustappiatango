﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Input2
struct Input2_t14;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void Input2::.ctor()
 void Input2__ctor_m13 (Input2_t14 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Input2::.cctor()
 void Input2__cctor_m14 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Input2::TouchBegin()
 bool Input2_TouchBegin_m15 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Input2::GetFirstPoint()
 Vector2_t9  Input2_GetFirstPoint_m16 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Input2::GetSecondPoint()
 Vector2_t9  Input2_GetSecondPoint_m17 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Input2::GetPointerCount()
 int32_t Input2_GetPointerCount_m18 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Input2::HasPointStarted(System.Int32)
 bool Input2_HasPointStarted_m19 (Object_t * __this/* static, unused */, int32_t ___num, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Input2::HasPointEnded(System.Int32)
 bool Input2_HasPointEnded_m20 (Object_t * __this/* static, unused */, int32_t ___num, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Input2::HasPointMoved(System.Int32)
 bool Input2_HasPointMoved_m21 (Object_t * __this/* static, unused */, int32_t ___num, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Input2::Pinch()
 float Input2_Pinch_m22 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Input2::Twist()
 float Input2_Twist_m23 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Input2::TwistInt()
 int32_t Input2_TwistInt_m24 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Input2::Swipe()
 Vector2_t9  Input2_Swipe_m25 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
