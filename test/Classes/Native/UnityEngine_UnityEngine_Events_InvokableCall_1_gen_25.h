﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.MultiTargetBehaviour>
struct UnityAction_1_t2897;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetBehaviour>
struct InvokableCall_1_t2896  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetBehaviour>::Delegate
	UnityAction_1_t2897 * ___Delegate_0;
};
