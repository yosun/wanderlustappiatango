﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.VirtualButton>
struct Comparer_1_t3761;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.VirtualButton>
struct Comparer_1_t3761  : public Object_t
{
};
struct Comparer_1_t3761_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.VirtualButton>::_default
	Comparer_1_t3761 * ____default_0;
};
