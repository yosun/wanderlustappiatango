﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t55;

// System.Void Vuforia.MaskOutAbstractBehaviour::.ctor()
 void MaskOutAbstractBehaviour__ctor_m486 (MaskOutAbstractBehaviour_t55 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
