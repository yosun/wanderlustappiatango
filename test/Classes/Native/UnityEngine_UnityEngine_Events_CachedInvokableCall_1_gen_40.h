﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_36.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
struct CachedInvokableCall_1_t2957  : public InvokableCall_1_t2958
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
