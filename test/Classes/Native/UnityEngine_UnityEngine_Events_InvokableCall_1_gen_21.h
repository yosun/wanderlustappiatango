﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.ComponentFactoryStarterBehaviour>
struct UnityAction_1_t2838;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.ComponentFactoryStarterBehaviour>
struct InvokableCall_1_t2837  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.ComponentFactoryStarterBehaviour>::Delegate
	UnityAction_1_t2838 * ___Delegate_0;
};
