﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct KeyValuePair_2_t4288;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t56;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m26127 (KeyValuePair_2_t4288 * __this, int32_t ___key, VirtualButtonAbstractBehaviour_t56 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Key()
 int32_t KeyValuePair_2_get_Key_m26128 (KeyValuePair_2_t4288 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m26129 (KeyValuePair_2_t4288 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Value()
 VirtualButtonAbstractBehaviour_t56 * KeyValuePair_2_get_Value_m26130 (KeyValuePair_2_t4288 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m26131 (KeyValuePair_2_t4288 * __this, VirtualButtonAbstractBehaviour_t56 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::ToString()
 String_t* KeyValuePair_2_ToString_m26132 (KeyValuePair_2_t4288 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
