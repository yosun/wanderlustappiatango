﻿#pragma once
#include <stdint.h>
// Vuforia.InternalEyewear
struct InternalEyewear_t579;
// Vuforia.InternalEyewearCalibrationProfileManager
struct InternalEyewearCalibrationProfileManager_t543;
// Vuforia.InternalEyewearUserCalibrator
struct InternalEyewearUserCalibrator_t545;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.InternalEyewear
struct InternalEyewear_t579  : public Object_t
{
	// Vuforia.InternalEyewearCalibrationProfileManager Vuforia.InternalEyewear::mProfileManager
	InternalEyewearCalibrationProfileManager_t543 * ___mProfileManager_1;
	// Vuforia.InternalEyewearUserCalibrator Vuforia.InternalEyewear::mCalibrator
	InternalEyewearUserCalibrator_t545 * ___mCalibrator_2;
};
struct InternalEyewear_t579_StaticFields{
	// Vuforia.InternalEyewear Vuforia.InternalEyewear::mInstance
	InternalEyewear_t579 * ___mInstance_0;
};
