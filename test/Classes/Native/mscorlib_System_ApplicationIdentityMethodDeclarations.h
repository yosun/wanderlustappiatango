﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ApplicationIdentity
struct ApplicationIdentity_t2182;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1066;
// System.String
struct String_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.ApplicationIdentity::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m12528 (ApplicationIdentity_t2182 * __this, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ApplicationIdentity::ToString()
 String_t* ApplicationIdentity_ToString_m12529 (ApplicationIdentity_t2182 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
