﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CanvasListPool
struct CanvasListPool_t405;
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t406;

// System.Void UnityEngine.UI.CanvasListPool::.cctor()
 void CanvasListPool__cctor_m1831 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Canvas> UnityEngine.UI.CanvasListPool::Get()
 List_1_t406 * CanvasListPool_Get_m1832 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CanvasListPool::Release(System.Collections.Generic.List`1<UnityEngine.Canvas>)
 void CanvasListPool_Release_m1833 (Object_t * __this/* static, unused */, List_1_t406 * ___toRelease, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CanvasListPool::<s_CanvasListPool>m__16(System.Collections.Generic.List`1<UnityEngine.Canvas>)
 void CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1834 (Object_t * __this/* static, unused */, List_1_t406 * ___l, MethodInfo* method) IL2CPP_METHOD_ATTR;
