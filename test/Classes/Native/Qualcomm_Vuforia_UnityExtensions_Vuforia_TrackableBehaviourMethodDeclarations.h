﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t44;
// Vuforia.Trackable
struct Trackable_t550;
// System.String
struct String_t;
// Vuforia.ITrackableEventHandler
struct ITrackableEventHandler_t135;
// UnityEngine.Renderer
struct Renderer_t7;
// UnityEngine.Transform
struct Transform_t10;
// UnityEngine.GameObject
struct GameObject_t2;
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// Vuforia.TrackableBehaviour/Status Vuforia.TrackableBehaviour::get_CurrentStatus()
 int32_t TrackableBehaviour_get_CurrentStatus_m2632 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Trackable Vuforia.TrackableBehaviour::get_Trackable()
 Object_t * TrackableBehaviour_get_Trackable_m348 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.TrackableBehaviour::get_TrackableName()
 String_t* TrackableBehaviour_get_TrackableName_m345 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::RegisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
 void TrackableBehaviour_RegisterTrackableEventHandler_m414 (TrackableBehaviour_t44 * __this, Object_t * ___trackableEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::UnregisterTrackableEventHandler(Vuforia.ITrackableEventHandler)
 bool TrackableBehaviour_UnregisterTrackableEventHandler_m2633 (TrackableBehaviour_t44 * __this, Object_t * ___trackableEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::OnTrackerUpdate(Vuforia.TrackableBehaviour/Status)
 void TrackableBehaviour_OnTrackerUpdate_m480 (TrackableBehaviour_t44 * __this, int32_t ___newStatus, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::OnFrameIndexUpdate(System.Int32)
 void TrackableBehaviour_OnFrameIndexUpdate_m426 (TrackableBehaviour_t44 * __this, int32_t ___newFrameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::InternalUnregisterTrackable()
// System.Void Vuforia.TrackableBehaviour::Start()
 void TrackableBehaviour_Start_m2634 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::OnDisable()
 void TrackableBehaviour_OnDisable_m2635 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.CorrectScale()
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m346 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::CorrectScaleImpl()
 bool TrackableBehaviour_CorrectScaleImpl_m496 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetNameForTrackable(System.String)
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m347 (TrackableBehaviour_t44 * __this, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_PreviousScale()
 Vector3_t13  TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m349 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetPreviousScale(UnityEngine.Vector3)
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m350 (TrackableBehaviour_t44 * __this, Vector3_t13  ___previousScale, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_PreserveChildSize()
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m351 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetPreserveChildSize(System.Boolean)
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m352 (TrackableBehaviour_t44 * __this, bool ___preserveChildSize, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_InitializedInEditor()
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m353 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.SetInitializedInEditor(System.Boolean)
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m354 (TrackableBehaviour_t44 * __this, bool ___initializedInEditor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.UnregisterTrackable()
 void TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m355 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Renderer Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.GetRenderer()
 Renderer_t7 * TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m360 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::.ctor()
 void TrackableBehaviour__ctor_m2636 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m2637 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m2638 (TrackableBehaviour_t44 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t10 * TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m2639 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.TrackableBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t2 * TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m2640 (TrackableBehaviour_t44 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
