﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>
struct Collection_1_t4376;
// System.Object
struct Object_t;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t768;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// Vuforia.IUserDefinedTargetEventHandler[]
struct IUserDefinedTargetEventHandlerU5BU5D_t4367;
// System.Collections.Generic.IEnumerator`1<Vuforia.IUserDefinedTargetEventHandler>
struct IEnumerator_1_t4369;
// System.Collections.Generic.IList`1<Vuforia.IUserDefinedTargetEventHandler>
struct IList_1_t4375;

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::.ctor()
// System.Collections.ObjectModel.Collection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_Collection_1_genMethodDeclarations.h"
#define Collection_1__ctor_m26814(__this, method) (void)Collection_1__ctor_m14676_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26815(__this, method) (bool)Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14677_gshared((Collection_1_t2852 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m26816(__this, ___array, ___index, method) (void)Collection_1_System_Collections_ICollection_CopyTo_m14678_gshared((Collection_1_t2852 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m26817(__this, method) (Object_t *)Collection_1_System_Collections_IEnumerable_GetEnumerator_m14679_gshared((Collection_1_t2852 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m26818(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_Add_m14680_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m26819(__this, ___value, method) (bool)Collection_1_System_Collections_IList_Contains_m14681_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m26820(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_IndexOf_m14682_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m26821(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_Insert_m14683_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m26822(__this, ___value, method) (void)Collection_1_System_Collections_IList_Remove_m14684_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m26823(__this, method) (bool)Collection_1_System_Collections_ICollection_get_IsSynchronized_m14685_gshared((Collection_1_t2852 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m26824(__this, method) (Object_t *)Collection_1_System_Collections_ICollection_get_SyncRoot_m14686_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m26825(__this, method) (bool)Collection_1_System_Collections_IList_get_IsFixedSize_m14687_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m26826(__this, method) (bool)Collection_1_System_Collections_IList_get_IsReadOnly_m14688_gshared((Collection_1_t2852 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m26827(__this, ___index, method) (Object_t *)Collection_1_System_Collections_IList_get_Item_m14689_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m26828(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_set_Item_m14690_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::Add(T)
#define Collection_1_Add_m26829(__this, ___item, method) (void)Collection_1_Add_m14691_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::Clear()
#define Collection_1_Clear_m26830(__this, method) (void)Collection_1_Clear_m14692_gshared((Collection_1_t2852 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::ClearItems()
#define Collection_1_ClearItems_m26831(__this, method) (void)Collection_1_ClearItems_m14693_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::Contains(T)
#define Collection_1_Contains_m26832(__this, ___item, method) (bool)Collection_1_Contains_m14694_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m26833(__this, ___array, ___index, method) (void)Collection_1_CopyTo_m14695_gshared((Collection_1_t2852 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::GetEnumerator()
#define Collection_1_GetEnumerator_m26834(__this, method) (Object_t*)Collection_1_GetEnumerator_m14696_gshared((Collection_1_t2852 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::IndexOf(T)
#define Collection_1_IndexOf_m26835(__this, ___item, method) (int32_t)Collection_1_IndexOf_m14697_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::Insert(System.Int32,T)
#define Collection_1_Insert_m26836(__this, ___index, ___item, method) (void)Collection_1_Insert_m14698_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m26837(__this, ___index, ___item, method) (void)Collection_1_InsertItem_m14699_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::Remove(T)
#define Collection_1_Remove_m26838(__this, ___item, method) (bool)Collection_1_Remove_m14700_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m26839(__this, ___index, method) (void)Collection_1_RemoveAt_m14701_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m26840(__this, ___index, method) (void)Collection_1_RemoveItem_m14702_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::get_Count()
#define Collection_1_get_Count_m26841(__this, method) (int32_t)Collection_1_get_Count_m14703_gshared((Collection_1_t2852 *)__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::get_Item(System.Int32)
#define Collection_1_get_Item_m26842(__this, ___index, method) (Object_t *)Collection_1_get_Item_m14704_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m26843(__this, ___index, ___value, method) (void)Collection_1_set_Item_m14705_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m26844(__this, ___index, ___item, method) (void)Collection_1_SetItem_m14706_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m26845(__this/* static, unused */, ___item, method) (bool)Collection_1_IsValidItem_m14707_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m26846(__this/* static, unused */, ___item, method) (Object_t *)Collection_1_ConvertItem_m14708_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m26847(__this/* static, unused */, ___list, method) (void)Collection_1_CheckWritable_m14709_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m26848(__this/* static, unused */, ___list, method) (bool)Collection_1_IsSynchronized_m14710_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.IUserDefinedTargetEventHandler>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m26849(__this/* static, unused */, ___list, method) (bool)Collection_1_IsFixedSize_m14711_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
