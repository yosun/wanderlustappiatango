﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TargetFinderImpl
struct TargetFinderImpl_t739;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerable_1_t733;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t50;
// UnityEngine.GameObject
struct GameObject_t2;
// System.Collections.Generic.IEnumerable`1<Vuforia.ImageTarget>
struct IEnumerable_1_t734;
// Vuforia.TargetFinder/InitState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_InitSt.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// Vuforia.TargetFinder/UpdateState
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Update.h"
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void Vuforia.TargetFinderImpl::.ctor()
 void TargetFinderImpl__ctor_m4015 (TargetFinderImpl_t739 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TargetFinderImpl::Finalize()
 void TargetFinderImpl_Finalize_m4016 (TargetFinderImpl_t739 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TargetFinderImpl::StartInit(System.String,System.String)
 bool TargetFinderImpl_StartInit_m4017 (TargetFinderImpl_t739 * __this, String_t* ___userAuth, String_t* ___secretAuth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TargetFinder/InitState Vuforia.TargetFinderImpl::GetInitState()
 int32_t TargetFinderImpl_GetInitState_m4018 (TargetFinderImpl_t739 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TargetFinderImpl::Deinit()
 bool TargetFinderImpl_Deinit_m4019 (TargetFinderImpl_t739 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TargetFinderImpl::StartRecognition()
 bool TargetFinderImpl_StartRecognition_m4020 (TargetFinderImpl_t739 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TargetFinderImpl::Stop()
 bool TargetFinderImpl_Stop_m4021 (TargetFinderImpl_t739 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TargetFinderImpl::SetUIScanlineColor(UnityEngine.Color)
 void TargetFinderImpl_SetUIScanlineColor_m4022 (TargetFinderImpl_t739 * __this, Color_t19  ___color, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TargetFinderImpl::SetUIPointColor(UnityEngine.Color)
 void TargetFinderImpl_SetUIPointColor_m4023 (TargetFinderImpl_t739 * __this, Color_t19  ___color, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.TargetFinderImpl::IsRequesting()
 bool TargetFinderImpl_IsRequesting_m4024 (TargetFinderImpl_t739 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TargetFinder/UpdateState Vuforia.TargetFinderImpl::Update()
 int32_t TargetFinderImpl_Update_m4025 (TargetFinderImpl_t739 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.TargetFinder/TargetSearchResult> Vuforia.TargetFinderImpl::GetResults()
 Object_t* TargetFinderImpl_GetResults_m4026 (TargetFinderImpl_t739 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.TargetFinderImpl::EnableTracking(Vuforia.TargetFinder/TargetSearchResult,System.String)
 ImageTargetAbstractBehaviour_t50 * TargetFinderImpl_EnableTracking_m4027 (TargetFinderImpl_t739 * __this, TargetSearchResult_t732  ___result, String_t* ___gameObjectName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.TargetFinderImpl::EnableTracking(Vuforia.TargetFinder/TargetSearchResult,UnityEngine.GameObject)
 ImageTargetAbstractBehaviour_t50 * TargetFinderImpl_EnableTracking_m4028 (TargetFinderImpl_t739 * __this, TargetSearchResult_t732  ___result, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TargetFinderImpl::ClearTrackables(System.Boolean)
 void TargetFinderImpl_ClearTrackables_m4029 (TargetFinderImpl_t739 * __this, bool ___destroyGameObjects, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.ImageTarget> Vuforia.TargetFinderImpl::GetImageTargets()
 Object_t* TargetFinderImpl_GetImageTargets_m4030 (TargetFinderImpl_t739 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
