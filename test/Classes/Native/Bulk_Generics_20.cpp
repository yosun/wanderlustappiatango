﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IEnumerator_1_t6220_il2cpp_TypeInfo;


// System.Array
#include "mscorlib_System_Array.h"

// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ILayoutIgnorer>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ILayoutIgnorer>
extern MethodInfo IEnumerator_1_get_Current_m44844_MethodInfo;
static PropertyInfo IEnumerator_1_t6220____Current_PropertyInfo = 
{
	&IEnumerator_1_t6220_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44844_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6220_PropertyInfos[] =
{
	&IEnumerator_1_t6220____Current_PropertyInfo,
	NULL
};
extern Il2CppType ILayoutIgnorer_t512_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44844_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.ILayoutIgnorer>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44844_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6220_il2cpp_TypeInfo/* declaring_type */
	, &ILayoutIgnorer_t512_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44844_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6220_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44844_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t266_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t6220_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6220_0_0_0;
extern Il2CppType IEnumerator_1_t6220_1_0_0;
struct IEnumerator_1_t6220;
extern Il2CppGenericClass IEnumerator_1_t6220_GenericClass;
TypeInfo IEnumerator_1_t6220_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6220_MethodInfos/* methods */
	, IEnumerator_1_t6220_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6220_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6220_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6220_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6220_0_0_0/* byval_arg */
	, &IEnumerator_1_t6220_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6220_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutIgnorer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_258.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3600_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutIgnorer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_258MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo ILayoutIgnorer_t512_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1493_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m19969_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7651_MethodInfo;
extern MethodInfo Array_get_Length_m7656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisILayoutIgnorer_t512_m34791_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m31436_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m31436(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.ILayoutIgnorer>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.ILayoutIgnorer>(System.Int32)
#define Array_InternalArray__get_Item_TisILayoutIgnorer_t512_m34791(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutIgnorer>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutIgnorer>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutIgnorer>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutIgnorer>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutIgnorer>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutIgnorer>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3600____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3600_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3600, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3600____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3600_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3600, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3600_FieldInfos[] =
{
	&InternalEnumerator_1_t3600____array_0_FieldInfo,
	&InternalEnumerator_1_t3600____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19966_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3600____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3600_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19966_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3600____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3600_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m19969_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3600_PropertyInfos[] =
{
	&InternalEnumerator_1_t3600____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3600____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3600_InternalEnumerator_1__ctor_m19965_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m19965_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutIgnorer>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m19965_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3600_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3600_InternalEnumerator_1__ctor_m19965_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m19965_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19966_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutIgnorer>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19966_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3600_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19966_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m19967_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutIgnorer>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m19967_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3600_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m19967_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m19968_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutIgnorer>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m19968_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3600_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m19968_GenericMethod/* genericMethod */

};
extern Il2CppType ILayoutIgnorer_t512_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m19969_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.ILayoutIgnorer>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m19969_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3600_il2cpp_TypeInfo/* declaring_type */
	, &ILayoutIgnorer_t512_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m19969_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3600_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m19965_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19966_MethodInfo,
	&InternalEnumerator_1_Dispose_m19967_MethodInfo,
	&InternalEnumerator_1_MoveNext_m19968_MethodInfo,
	&InternalEnumerator_1_get_Current_m19969_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m19968_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m19967_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3600_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19966_MethodInfo,
	&InternalEnumerator_1_MoveNext_m19968_MethodInfo,
	&InternalEnumerator_1_Dispose_m19967_MethodInfo,
	&InternalEnumerator_1_get_Current_m19969_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3600_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6220_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3600_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6220_il2cpp_TypeInfo, 7},
};
extern TypeInfo ILayoutIgnorer_t512_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3600_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m19969_MethodInfo/* Method Usage */,
	&ILayoutIgnorer_t512_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisILayoutIgnorer_t512_m34791_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3600_0_0_0;
extern Il2CppType InternalEnumerator_1_t3600_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t3600_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t3600_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3600_MethodInfos/* methods */
	, InternalEnumerator_1_t3600_PropertyInfos/* properties */
	, InternalEnumerator_1_t3600_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3600_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3600_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3600_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3600_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3600_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3600_1_0_0/* this_arg */
	, InternalEnumerator_1_t3600_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3600_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3600_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3600)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7900_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutIgnorer>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutIgnorer>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutIgnorer>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutIgnorer>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutIgnorer>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutIgnorer>
extern MethodInfo IList_1_get_Item_m44845_MethodInfo;
extern MethodInfo IList_1_set_Item_m44846_MethodInfo;
static PropertyInfo IList_1_t7900____Item_PropertyInfo = 
{
	&IList_1_t7900_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44845_MethodInfo/* get */
	, &IList_1_set_Item_m44846_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7900_PropertyInfos[] =
{
	&IList_1_t7900____Item_PropertyInfo,
	NULL
};
extern Il2CppType ILayoutIgnorer_t512_0_0_0;
extern Il2CppType ILayoutIgnorer_t512_0_0_0;
static ParameterInfo IList_1_t7900_IList_1_IndexOf_m44847_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ILayoutIgnorer_t512_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44847_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutIgnorer>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44847_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7900_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7900_IList_1_IndexOf_m44847_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44847_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ILayoutIgnorer_t512_0_0_0;
static ParameterInfo IList_1_t7900_IList_1_Insert_m44848_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ILayoutIgnorer_t512_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44848_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutIgnorer>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44848_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7900_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7900_IList_1_Insert_m44848_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44848_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7900_IList_1_RemoveAt_m44849_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44849_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutIgnorer>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44849_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7900_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7900_IList_1_RemoveAt_m44849_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44849_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7900_IList_1_get_Item_m44845_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ILayoutIgnorer_t512_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44845_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutIgnorer>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44845_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7900_il2cpp_TypeInfo/* declaring_type */
	, &ILayoutIgnorer_t512_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7900_IList_1_get_Item_m44845_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44845_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ILayoutIgnorer_t512_0_0_0;
static ParameterInfo IList_1_t7900_IList_1_set_Item_m44846_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ILayoutIgnorer_t512_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44846_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.ILayoutIgnorer>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44846_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7900_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7900_IList_1_set_Item_m44846_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44846_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7900_MethodInfos[] =
{
	&IList_1_IndexOf_m44847_MethodInfo,
	&IList_1_Insert_m44848_MethodInfo,
	&IList_1_RemoveAt_m44849_MethodInfo,
	&IList_1_get_Item_m44845_MethodInfo,
	&IList_1_set_Item_m44846_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7899_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7901_il2cpp_TypeInfo;
static TypeInfo* IList_1_t7900_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7899_il2cpp_TypeInfo,
	&IEnumerable_1_t7901_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7900_0_0_0;
extern Il2CppType IList_1_t7900_1_0_0;
struct IList_1_t7900;
extern Il2CppGenericClass IList_1_t7900_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7900_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7900_MethodInfos/* methods */
	, IList_1_t7900_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7900_il2cpp_TypeInfo/* element_class */
	, IList_1_t7900_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7900_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7900_0_0_0/* byval_arg */
	, &IList_1_t7900_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7900_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutElement>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_78.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3601_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutElement>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_78MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// UnityEngine.UI.LayoutElement
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutElement>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_80.h"
extern TypeInfo ObjectU5BU5D_t115_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo LayoutElement_t390_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t3602_il2cpp_TypeInfo;
extern TypeInfo Void_t99_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutElement>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_80MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m19972_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m19974_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutElement>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutElement>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutElement>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t3601____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t3601_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3601, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3601_FieldInfos[] =
{
	&CachedInvokableCall_1_t3601____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType LayoutElement_t390_0_0_0;
extern Il2CppType LayoutElement_t390_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3601_CachedInvokableCall_1__ctor_m19970_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &LayoutElement_t390_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m19970_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutElement>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m19970_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t3601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3601_CachedInvokableCall_1__ctor_m19970_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m19970_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3601_CachedInvokableCall_1_Invoke_m19971_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m19971_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutElement>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m19971_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t3601_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3601_CachedInvokableCall_1_Invoke_m19971_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m19971_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3601_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m19970_MethodInfo,
	&CachedInvokableCall_1_Invoke_m19971_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m304_MethodInfo;
extern MethodInfo Object_GetHashCode_m305_MethodInfo;
extern MethodInfo Object_ToString_m306_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m19971_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m19975_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3601_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m19971_MethodInfo,
	&InvokableCall_1_Find_m19975_MethodInfo,
};
extern Il2CppType UnityAction_1_t3603_0_0_0;
extern TypeInfo UnityAction_1_t3603_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisLayoutElement_t390_m34801_MethodInfo;
extern TypeInfo LayoutElement_t390_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m19977_MethodInfo;
extern TypeInfo LayoutElement_t390_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3601_RGCTXData[8] = 
{
	&UnityAction_1_t3603_0_0_0/* Type Usage */,
	&UnityAction_1_t3603_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisLayoutElement_t390_m34801_MethodInfo/* Method Usage */,
	&LayoutElement_t390_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m19977_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m19972_MethodInfo/* Method Usage */,
	&LayoutElement_t390_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m19974_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3601_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3601_1_0_0;
struct CachedInvokableCall_1_t3601;
extern Il2CppGenericClass CachedInvokableCall_1_t3601_GenericClass;
TypeInfo CachedInvokableCall_1_t3601_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3601_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3601_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3602_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3601_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3601_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3601_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3601_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3601_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3601_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3601_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3601)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutElement>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_85.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t3603_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t507_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutElement>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_85MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6358_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m395_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m456_MethodInfo;
extern MethodInfo Delegate_Combine_m2149_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6357_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2468_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6359_MethodInfo;
extern MethodInfo Delegate_get_Target_m6536_MethodInfo;
extern MethodInfo Delegate_get_Method_m6534_MethodInfo;
struct BaseInvokableCall_t1075;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.LayoutElement>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.LayoutElement>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisLayoutElement_t390_m34801(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutElement>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutElement>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutElement>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutElement>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutElement>
extern Il2CppType UnityAction_1_t3603_0_0_1;
FieldInfo InvokableCall_1_t3602____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3603_0_0_1/* type */
	, &InvokableCall_1_t3602_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3602, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3602_FieldInfos[] =
{
	&InvokableCall_1_t3602____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3602_InvokableCall_1__ctor_m19972_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m19972_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutElement>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m19972_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t3602_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3602_InvokableCall_1__ctor_m19972_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m19972_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3603_0_0_0;
static ParameterInfo InvokableCall_1_t3602_InvokableCall_1__ctor_m19973_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3603_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m19973_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutElement>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m19973_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t3602_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3602_InvokableCall_1__ctor_m19973_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m19973_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t3602_InvokableCall_1_Invoke_m19974_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m19974_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutElement>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m19974_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t3602_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3602_InvokableCall_1_Invoke_m19974_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m19974_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3602_InvokableCall_1_Find_m19975_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m19975_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutElement>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m19975_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t3602_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3602_InvokableCall_1_Find_m19975_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m19975_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3602_MethodInfos[] =
{
	&InvokableCall_1__ctor_m19972_MethodInfo,
	&InvokableCall_1__ctor_m19973_MethodInfo,
	&InvokableCall_1_Invoke_m19974_MethodInfo,
	&InvokableCall_1_Find_m19975_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3602_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m19974_MethodInfo,
	&InvokableCall_1_Find_m19975_MethodInfo,
};
extern TypeInfo UnityAction_1_t3603_il2cpp_TypeInfo;
extern TypeInfo LayoutElement_t390_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3602_RGCTXData[5] = 
{
	&UnityAction_1_t3603_0_0_0/* Type Usage */,
	&UnityAction_1_t3603_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisLayoutElement_t390_m34801_MethodInfo/* Method Usage */,
	&LayoutElement_t390_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m19977_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3602_0_0_0;
extern Il2CppType InvokableCall_1_t3602_1_0_0;
extern TypeInfo BaseInvokableCall_t1075_il2cpp_TypeInfo;
struct InvokableCall_1_t3602;
extern Il2CppGenericClass InvokableCall_1_t3602_GenericClass;
TypeInfo InvokableCall_1_t3602_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3602_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3602_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3602_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3602_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3602_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3602_0_0_0/* byval_arg */
	, &InvokableCall_1_t3602_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3602_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3602_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3602)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutElement>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutElement>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutElement>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutElement>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutElement>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3603_UnityAction_1__ctor_m19976_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m19976_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutElement>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m19976_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t3603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3603_UnityAction_1__ctor_m19976_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m19976_GenericMethod/* genericMethod */

};
extern Il2CppType LayoutElement_t390_0_0_0;
static ParameterInfo UnityAction_1_t3603_UnityAction_1_Invoke_m19977_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &LayoutElement_t390_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m19977_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutElement>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m19977_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t3603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3603_UnityAction_1_Invoke_m19977_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m19977_GenericMethod/* genericMethod */

};
extern Il2CppType LayoutElement_t390_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3603_UnityAction_1_BeginInvoke_m19978_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &LayoutElement_t390_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m19978_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutElement>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m19978_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t3603_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3603_UnityAction_1_BeginInvoke_m19978_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m19978_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t3603_UnityAction_1_EndInvoke_m19979_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m19979_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutElement>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m19979_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t3603_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3603_UnityAction_1_EndInvoke_m19979_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m19979_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3603_MethodInfos[] =
{
	&UnityAction_1__ctor_m19976_MethodInfo,
	&UnityAction_1_Invoke_m19977_MethodInfo,
	&UnityAction_1_BeginInvoke_m19978_MethodInfo,
	&UnityAction_1_EndInvoke_m19979_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2241_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2242_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2243_MethodInfo;
extern MethodInfo Delegate_Clone_m2244_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2245_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2246_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2247_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m19978_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m19979_MethodInfo;
static MethodInfo* UnityAction_1_t3603_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m19977_MethodInfo,
	&UnityAction_1_BeginInvoke_m19978_MethodInfo,
	&UnityAction_1_EndInvoke_m19979_MethodInfo,
};
extern TypeInfo ICloneable_t481_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t482_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t3603_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3603_1_0_0;
extern TypeInfo MulticastDelegate_t325_il2cpp_TypeInfo;
struct UnityAction_1_t3603;
extern Il2CppGenericClass UnityAction_1_t3603_GenericClass;
TypeInfo UnityAction_1_t3603_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3603_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3603_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3603_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3603_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3603_0_0_0/* byval_arg */
	, &UnityAction_1_t3603_1_0_0/* this_arg */
	, UnityAction_1_t3603_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3603_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3603)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutGroup>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_79.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3604_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutGroup>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_79MethodDeclarations.h"

// UnityEngine.UI.LayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutGroup>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_81.h"
extern TypeInfo LayoutGroup_t387_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t3605_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutGroup>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_81MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m19982_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m19984_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutGroup>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutGroup>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutGroup>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t3604____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t3604_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3604, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3604_FieldInfos[] =
{
	&CachedInvokableCall_1_t3604____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType LayoutGroup_t387_0_0_0;
extern Il2CppType LayoutGroup_t387_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3604_CachedInvokableCall_1__ctor_m19980_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &LayoutGroup_t387_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m19980_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutGroup>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m19980_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t3604_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3604_CachedInvokableCall_1__ctor_m19980_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m19980_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3604_CachedInvokableCall_1_Invoke_m19981_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m19981_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.LayoutGroup>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m19981_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t3604_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3604_CachedInvokableCall_1_Invoke_m19981_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m19981_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3604_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m19980_MethodInfo,
	&CachedInvokableCall_1_Invoke_m19981_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m19981_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m19985_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3604_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m19981_MethodInfo,
	&InvokableCall_1_Find_m19985_MethodInfo,
};
extern Il2CppType UnityAction_1_t3606_0_0_0;
extern TypeInfo UnityAction_1_t3606_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisLayoutGroup_t387_m34802_MethodInfo;
extern TypeInfo LayoutGroup_t387_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m19987_MethodInfo;
extern TypeInfo LayoutGroup_t387_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3604_RGCTXData[8] = 
{
	&UnityAction_1_t3606_0_0_0/* Type Usage */,
	&UnityAction_1_t3606_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisLayoutGroup_t387_m34802_MethodInfo/* Method Usage */,
	&LayoutGroup_t387_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m19987_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m19982_MethodInfo/* Method Usage */,
	&LayoutGroup_t387_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m19984_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3604_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3604_1_0_0;
struct CachedInvokableCall_1_t3604;
extern Il2CppGenericClass CachedInvokableCall_1_t3604_GenericClass;
TypeInfo CachedInvokableCall_1_t3604_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3604_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3604_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3605_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3604_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3604_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3604_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3604_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3604_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3604_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3604_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3604)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutGroup>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_86.h"
extern TypeInfo UnityAction_1_t3606_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutGroup>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_86MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.LayoutGroup>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.LayoutGroup>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisLayoutGroup_t387_m34802(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutGroup>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutGroup>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutGroup>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutGroup>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutGroup>
extern Il2CppType UnityAction_1_t3606_0_0_1;
FieldInfo InvokableCall_1_t3605____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3606_0_0_1/* type */
	, &InvokableCall_1_t3605_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3605, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3605_FieldInfos[] =
{
	&InvokableCall_1_t3605____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3605_InvokableCall_1__ctor_m19982_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m19982_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutGroup>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m19982_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t3605_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3605_InvokableCall_1__ctor_m19982_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m19982_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3606_0_0_0;
static ParameterInfo InvokableCall_1_t3605_InvokableCall_1__ctor_m19983_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3606_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m19983_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutGroup>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m19983_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t3605_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3605_InvokableCall_1__ctor_m19983_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m19983_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t3605_InvokableCall_1_Invoke_m19984_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m19984_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutGroup>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m19984_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t3605_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3605_InvokableCall_1_Invoke_m19984_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m19984_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3605_InvokableCall_1_Find_m19985_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m19985_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.LayoutGroup>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m19985_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t3605_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3605_InvokableCall_1_Find_m19985_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m19985_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3605_MethodInfos[] =
{
	&InvokableCall_1__ctor_m19982_MethodInfo,
	&InvokableCall_1__ctor_m19983_MethodInfo,
	&InvokableCall_1_Invoke_m19984_MethodInfo,
	&InvokableCall_1_Find_m19985_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3605_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m19984_MethodInfo,
	&InvokableCall_1_Find_m19985_MethodInfo,
};
extern TypeInfo UnityAction_1_t3606_il2cpp_TypeInfo;
extern TypeInfo LayoutGroup_t387_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3605_RGCTXData[5] = 
{
	&UnityAction_1_t3606_0_0_0/* Type Usage */,
	&UnityAction_1_t3606_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisLayoutGroup_t387_m34802_MethodInfo/* Method Usage */,
	&LayoutGroup_t387_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m19987_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3605_0_0_0;
extern Il2CppType InvokableCall_1_t3605_1_0_0;
struct InvokableCall_1_t3605;
extern Il2CppGenericClass InvokableCall_1_t3605_GenericClass;
TypeInfo InvokableCall_1_t3605_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3605_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3605_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3605_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3605_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3605_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3605_0_0_0/* byval_arg */
	, &InvokableCall_1_t3605_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3605_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3605_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3605)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutGroup>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutGroup>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutGroup>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutGroup>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutGroup>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3606_UnityAction_1__ctor_m19986_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m19986_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutGroup>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m19986_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t3606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3606_UnityAction_1__ctor_m19986_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m19986_GenericMethod/* genericMethod */

};
extern Il2CppType LayoutGroup_t387_0_0_0;
static ParameterInfo UnityAction_1_t3606_UnityAction_1_Invoke_m19987_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &LayoutGroup_t387_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m19987_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutGroup>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m19987_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t3606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3606_UnityAction_1_Invoke_m19987_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m19987_GenericMethod/* genericMethod */

};
extern Il2CppType LayoutGroup_t387_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3606_UnityAction_1_BeginInvoke_m19988_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &LayoutGroup_t387_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m19988_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutGroup>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m19988_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t3606_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3606_UnityAction_1_BeginInvoke_m19988_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m19988_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t3606_UnityAction_1_EndInvoke_m19989_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m19989_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.LayoutGroup>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m19989_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t3606_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3606_UnityAction_1_EndInvoke_m19989_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m19989_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3606_MethodInfos[] =
{
	&UnityAction_1__ctor_m19986_MethodInfo,
	&UnityAction_1_Invoke_m19987_MethodInfo,
	&UnityAction_1_BeginInvoke_m19988_MethodInfo,
	&UnityAction_1_EndInvoke_m19989_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m19988_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m19989_MethodInfo;
static MethodInfo* UnityAction_1_t3606_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m19987_MethodInfo,
	&UnityAction_1_BeginInvoke_m19988_MethodInfo,
	&UnityAction_1_EndInvoke_m19989_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3606_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3606_1_0_0;
struct UnityAction_1_t3606;
extern Il2CppGenericClass UnityAction_1_t3606_GenericClass;
TypeInfo UnityAction_1_t3606_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3606_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3606_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3606_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3606_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3606_0_0_0/* byval_arg */
	, &UnityAction_1_t3606_1_0_0/* this_arg */
	, UnityAction_1_t3606_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3606_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3606)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEquatable_1_t516_il2cpp_TypeInfo;

// UnityEngine.UI.LayoutRebuilder
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"


// System.Boolean System.IEquatable`1<UnityEngine.UI.LayoutRebuilder>::Equals(T)
// Metadata Definition System.IEquatable`1<UnityEngine.UI.LayoutRebuilder>
extern Il2CppType LayoutRebuilder_t395_0_0_0;
extern Il2CppType LayoutRebuilder_t395_0_0_0;
static ParameterInfo IEquatable_1_t516_IEquatable_1_Equals_m44850_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &LayoutRebuilder_t395_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_LayoutRebuilder_t395 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEquatable_1_Equals_m44850_GenericMethod;
// System.Boolean System.IEquatable`1<UnityEngine.UI.LayoutRebuilder>::Equals(T)
MethodInfo IEquatable_1_Equals_m44850_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEquatable_1_t516_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_LayoutRebuilder_t395/* invoker_method */
	, IEquatable_1_t516_IEquatable_1_Equals_m44850_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEquatable_1_Equals_m44850_GenericMethod/* genericMethod */

};
static MethodInfo* IEquatable_1_t516_MethodInfos[] =
{
	&IEquatable_1_Equals_m44850_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEquatable_1_t516_0_0_0;
extern Il2CppType IEquatable_1_t516_1_0_0;
struct IEquatable_1_t516;
extern Il2CppGenericClass IEquatable_1_t516_GenericClass;
TypeInfo IEquatable_1_t516_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, IEquatable_1_t516_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEquatable_1_t516_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEquatable_1_t516_il2cpp_TypeInfo/* cast_class */
	, &IEquatable_1_t516_0_0_0/* byval_arg */
	, &IEquatable_1_t516_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEquatable_1_t516_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_3.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityAction_1_t393_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_3MethodDeclarations.h"

// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"


// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Component>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Component>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Component>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Component>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.Component>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t393_UnityAction_1__ctor_m2532_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m2532_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Component>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m2532_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t393_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t393_UnityAction_1__ctor_m2532_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m2532_GenericMethod/* genericMethod */

};
extern Il2CppType Component_t100_0_0_0;
extern Il2CppType Component_t100_0_0_0;
static ParameterInfo UnityAction_1_t393_UnityAction_1_Invoke_m2535_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Component_t100_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m2535_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Component>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m2535_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t393_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t393_UnityAction_1_Invoke_m2535_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m2535_GenericMethod/* genericMethod */

};
extern Il2CppType Component_t100_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t393_UnityAction_1_BeginInvoke_m19990_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Component_t100_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m19990_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.Component>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m19990_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t393_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t393_UnityAction_1_BeginInvoke_m19990_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m19990_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t393_UnityAction_1_EndInvoke_m19991_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m19991_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.Component>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m19991_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t393_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t393_UnityAction_1_EndInvoke_m19991_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m19991_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t393_MethodInfos[] =
{
	&UnityAction_1__ctor_m2532_MethodInfo,
	&UnityAction_1_Invoke_m2535_MethodInfo,
	&UnityAction_1_BeginInvoke_m19990_MethodInfo,
	&UnityAction_1_EndInvoke_m19991_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_Invoke_m2535_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m19990_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m19991_MethodInfo;
static MethodInfo* UnityAction_1_t393_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m2535_MethodInfo,
	&UnityAction_1_BeginInvoke_m19990_MethodInfo,
	&UnityAction_1_EndInvoke_m19991_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t393_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t393_0_0_0;
extern Il2CppType UnityAction_1_t393_1_0_0;
struct UnityAction_1_t393;
extern Il2CppGenericClass UnityAction_1_t393_GenericClass;
TypeInfo UnityAction_1_t393_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t393_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t393_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t393_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t393_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t393_0_0_0/* byval_arg */
	, &UnityAction_1_t393_1_0_0/* this_arg */
	, UnityAction_1_t393_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t393_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t393)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
#include "System_Core_System_Func_2_gen_1.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Func_2_t397_il2cpp_TypeInfo;
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
#include "System_Core_System_Func_2_gen_1MethodDeclarations.h"

// System.Single
#include "mscorlib_System_Single.h"


// System.Void System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::.ctor(System.Object,System.IntPtr)
extern MethodInfo Func_2__ctor_m2536_MethodInfo;
 void Func_2__ctor_m2536 (Func_2_t397 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method){
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TResult System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::Invoke(T)
extern MethodInfo Func_2_Invoke_m2537_MethodInfo;
 float Func_2_Invoke_m2537 (Func_2_t397 * __this, Object_t * ___arg1, MethodInfo* method){
	if(__this->___prev_9 != NULL)
	{
		Func_2_Invoke_m2537((Func_2_t397 *)__this->___prev_9,___arg1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___arg1, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Object_t * __this, Object_t * ___arg1, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef float (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern MethodInfo Func_2_BeginInvoke_m19992_MethodInfo;
 Object_t * Func_2_BeginInvoke_m19992 (Func_2_t397 * __this, Object_t * ___arg1, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = ___arg1;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TResult System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::EndInvoke(System.IAsyncResult)
extern MethodInfo Func_2_EndInvoke_m19993_MethodInfo;
 float Func_2_EndInvoke_m19993 (Func_2_t397 * __this, Object_t * ___result, MethodInfo* method){
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Metadata Definition System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Func_2_t397_Func_2__ctor_m2536_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Func_2__ctor_m2536_GenericMethod;
// System.Void System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::.ctor(System.Object,System.IntPtr)
MethodInfo Func_2__ctor_m2536_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Func_2__ctor_m2536/* method */
	, &Func_2_t397_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, Func_2_t397_Func_2__ctor_m2536_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Func_2__ctor_m2536_GenericMethod/* genericMethod */

};
extern Il2CppType ILayoutElement_t399_0_0_0;
extern Il2CppType ILayoutElement_t399_0_0_0;
static ParameterInfo Func_2_t397_Func_2_Invoke_m2537_ParameterInfos[] = 
{
	{"arg1", 0, 134217728, &EmptyCustomAttributesCache, &ILayoutElement_t399_0_0_0},
};
extern Il2CppType Single_t105_0_0_0;
extern void* RuntimeInvoker_Single_t105_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Func_2_Invoke_m2537_GenericMethod;
// TResult System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::Invoke(T)
MethodInfo Func_2_Invoke_m2537_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Func_2_Invoke_m2537/* method */
	, &Func_2_t397_il2cpp_TypeInfo/* declaring_type */
	, &Single_t105_0_0_0/* return_type */
	, RuntimeInvoker_Single_t105_Object_t/* invoker_method */
	, Func_2_t397_Func_2_Invoke_m2537_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Func_2_Invoke_m2537_GenericMethod/* genericMethod */

};
extern Il2CppType ILayoutElement_t399_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Func_2_t397_Func_2_BeginInvoke_m19992_ParameterInfos[] = 
{
	{"arg1", 0, 134217728, &EmptyCustomAttributesCache, &ILayoutElement_t399_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Func_2_BeginInvoke_m19992_GenericMethod;
// System.IAsyncResult System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Func_2_BeginInvoke_m19992_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Func_2_BeginInvoke_m19992/* method */
	, &Func_2_t397_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Func_2_t397_Func_2_BeginInvoke_m19992_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Func_2_BeginInvoke_m19992_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo Func_2_t397_Func_2_EndInvoke_m19993_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Single_t105_0_0_0;
extern void* RuntimeInvoker_Single_t105_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Func_2_EndInvoke_m19993_GenericMethod;
// TResult System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>::EndInvoke(System.IAsyncResult)
MethodInfo Func_2_EndInvoke_m19993_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Func_2_EndInvoke_m19993/* method */
	, &Func_2_t397_il2cpp_TypeInfo/* declaring_type */
	, &Single_t105_0_0_0/* return_type */
	, RuntimeInvoker_Single_t105_Object_t/* invoker_method */
	, Func_2_t397_Func_2_EndInvoke_m19993_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Func_2_EndInvoke_m19993_GenericMethod/* genericMethod */

};
static MethodInfo* Func_2_t397_MethodInfos[] =
{
	&Func_2__ctor_m2536_MethodInfo,
	&Func_2_Invoke_m2537_MethodInfo,
	&Func_2_BeginInvoke_m19992_MethodInfo,
	&Func_2_EndInvoke_m19993_MethodInfo,
	NULL
};
static MethodInfo* Func_2_t397_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&Func_2_Invoke_m2537_MethodInfo,
	&Func_2_BeginInvoke_m19992_MethodInfo,
	&Func_2_EndInvoke_m19993_MethodInfo,
};
static Il2CppInterfaceOffsetPair Func_2_t397_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Func_2_t397_0_0_0;
extern Il2CppType Func_2_t397_1_0_0;
struct Func_2_t397;
extern Il2CppGenericClass Func_2_t397_GenericClass;
TypeInfo Func_2_t397_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Func`2"/* name */
	, "System"/* namespaze */
	, Func_2_t397_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Func_2_t397_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Func_2_t397_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Func_2_t397_il2cpp_TypeInfo/* cast_class */
	, &Func_2_t397_0_0_0/* byval_arg */
	, &Func_2_t397_1_0_0/* this_arg */
	, Func_2_t397_InterfacesOffsets/* interface_offsets */
	, &Func_2_t397_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Func_2_t397)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6224_il2cpp_TypeInfo;

// UnityEngine.UI.VerticalLayoutGroup
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>
extern MethodInfo IEnumerator_1_get_Current_m44851_MethodInfo;
static PropertyInfo IEnumerator_1_t6224____Current_PropertyInfo = 
{
	&IEnumerator_1_t6224_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44851_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6224_PropertyInfos[] =
{
	&IEnumerator_1_t6224____Current_PropertyInfo,
	NULL
};
extern Il2CppType VerticalLayoutGroup_t400_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44851_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44851_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6224_il2cpp_TypeInfo/* declaring_type */
	, &VerticalLayoutGroup_t400_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44851_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6224_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44851_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6224_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6224_0_0_0;
extern Il2CppType IEnumerator_1_t6224_1_0_0;
struct IEnumerator_1_t6224;
extern Il2CppGenericClass IEnumerator_1_t6224_GenericClass;
TypeInfo IEnumerator_1_t6224_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6224_MethodInfos/* methods */
	, IEnumerator_1_t6224_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6224_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6224_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6224_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6224_0_0_0/* byval_arg */
	, &IEnumerator_1_t6224_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6224_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_259.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3607_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_259MethodDeclarations.h"

extern TypeInfo VerticalLayoutGroup_t400_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m19998_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisVerticalLayoutGroup_t400_m34804_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.VerticalLayoutGroup>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.VerticalLayoutGroup>(System.Int32)
#define Array_InternalArray__get_Item_TisVerticalLayoutGroup_t400_m34804(__this, p0, method) (VerticalLayoutGroup_t400 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3607____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3607_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3607, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3607____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3607_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3607, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3607_FieldInfos[] =
{
	&InternalEnumerator_1_t3607____array_0_FieldInfo,
	&InternalEnumerator_1_t3607____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19995_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3607____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3607_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19995_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3607____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3607_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m19998_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3607_PropertyInfos[] =
{
	&InternalEnumerator_1_t3607____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3607____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3607_InternalEnumerator_1__ctor_m19994_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m19994_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m19994_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3607_InternalEnumerator_1__ctor_m19994_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m19994_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19995_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19995_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3607_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19995_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m19996_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m19996_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3607_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m19996_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m19997_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m19997_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3607_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m19997_GenericMethod/* genericMethod */

};
extern Il2CppType VerticalLayoutGroup_t400_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m19998_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.VerticalLayoutGroup>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m19998_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3607_il2cpp_TypeInfo/* declaring_type */
	, &VerticalLayoutGroup_t400_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m19998_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3607_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m19994_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19995_MethodInfo,
	&InternalEnumerator_1_Dispose_m19996_MethodInfo,
	&InternalEnumerator_1_MoveNext_m19997_MethodInfo,
	&InternalEnumerator_1_get_Current_m19998_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m19997_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m19996_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3607_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19995_MethodInfo,
	&InternalEnumerator_1_MoveNext_m19997_MethodInfo,
	&InternalEnumerator_1_Dispose_m19996_MethodInfo,
	&InternalEnumerator_1_get_Current_m19998_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3607_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6224_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3607_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6224_il2cpp_TypeInfo, 7},
};
extern TypeInfo VerticalLayoutGroup_t400_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3607_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m19998_MethodInfo/* Method Usage */,
	&VerticalLayoutGroup_t400_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisVerticalLayoutGroup_t400_m34804_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3607_0_0_0;
extern Il2CppType InternalEnumerator_1_t3607_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3607_GenericClass;
TypeInfo InternalEnumerator_1_t3607_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3607_MethodInfos/* methods */
	, InternalEnumerator_1_t3607_PropertyInfos/* properties */
	, InternalEnumerator_1_t3607_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3607_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3607_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3607_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3607_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3607_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3607_1_0_0/* this_arg */
	, InternalEnumerator_1_t3607_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3607_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3607_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3607)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7902_il2cpp_TypeInfo;

#include "UnityEngine.UI_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>
extern MethodInfo ICollection_1_get_Count_m44852_MethodInfo;
static PropertyInfo ICollection_1_t7902____Count_PropertyInfo = 
{
	&ICollection_1_t7902_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44852_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44853_MethodInfo;
static PropertyInfo ICollection_1_t7902____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7902_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44853_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7902_PropertyInfos[] =
{
	&ICollection_1_t7902____Count_PropertyInfo,
	&ICollection_1_t7902____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44852_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>::get_Count()
MethodInfo ICollection_1_get_Count_m44852_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7902_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44852_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44853_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44853_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7902_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44853_GenericMethod/* genericMethod */

};
extern Il2CppType VerticalLayoutGroup_t400_0_0_0;
extern Il2CppType VerticalLayoutGroup_t400_0_0_0;
static ParameterInfo ICollection_1_t7902_ICollection_1_Add_m44854_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VerticalLayoutGroup_t400_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44854_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>::Add(T)
MethodInfo ICollection_1_Add_m44854_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7902_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7902_ICollection_1_Add_m44854_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44854_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44855_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>::Clear()
MethodInfo ICollection_1_Clear_m44855_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7902_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44855_GenericMethod/* genericMethod */

};
extern Il2CppType VerticalLayoutGroup_t400_0_0_0;
static ParameterInfo ICollection_1_t7902_ICollection_1_Contains_m44856_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VerticalLayoutGroup_t400_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44856_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>::Contains(T)
MethodInfo ICollection_1_Contains_m44856_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7902_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7902_ICollection_1_Contains_m44856_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44856_GenericMethod/* genericMethod */

};
extern Il2CppType VerticalLayoutGroupU5BU5D_t5637_0_0_0;
extern Il2CppType VerticalLayoutGroupU5BU5D_t5637_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7902_ICollection_1_CopyTo_m44857_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &VerticalLayoutGroupU5BU5D_t5637_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44857_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44857_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7902_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7902_ICollection_1_CopyTo_m44857_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44857_GenericMethod/* genericMethod */

};
extern Il2CppType VerticalLayoutGroup_t400_0_0_0;
static ParameterInfo ICollection_1_t7902_ICollection_1_Remove_m44858_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VerticalLayoutGroup_t400_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44858_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.VerticalLayoutGroup>::Remove(T)
MethodInfo ICollection_1_Remove_m44858_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7902_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7902_ICollection_1_Remove_m44858_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44858_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7902_MethodInfos[] =
{
	&ICollection_1_get_Count_m44852_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44853_MethodInfo,
	&ICollection_1_Add_m44854_MethodInfo,
	&ICollection_1_Clear_m44855_MethodInfo,
	&ICollection_1_Contains_m44856_MethodInfo,
	&ICollection_1_CopyTo_m44857_MethodInfo,
	&ICollection_1_Remove_m44858_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7904_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7902_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7904_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7902_0_0_0;
extern Il2CppType ICollection_1_t7902_1_0_0;
struct ICollection_1_t7902;
extern Il2CppGenericClass ICollection_1_t7902_GenericClass;
TypeInfo ICollection_1_t7902_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7902_MethodInfos/* methods */
	, ICollection_1_t7902_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7902_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7902_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7902_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7902_0_0_0/* byval_arg */
	, &ICollection_1_t7902_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7902_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.VerticalLayoutGroup>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.VerticalLayoutGroup>
extern Il2CppType IEnumerator_1_t6224_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44859_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.VerticalLayoutGroup>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44859_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7904_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6224_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44859_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7904_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44859_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7904_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7904_0_0_0;
extern Il2CppType IEnumerable_1_t7904_1_0_0;
struct IEnumerable_1_t7904;
extern Il2CppGenericClass IEnumerable_1_t7904_GenericClass;
TypeInfo IEnumerable_1_t7904_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7904_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7904_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7904_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7904_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7904_0_0_0/* byval_arg */
	, &IEnumerable_1_t7904_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7904_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7903_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.VerticalLayoutGroup>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.VerticalLayoutGroup>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.VerticalLayoutGroup>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.VerticalLayoutGroup>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.VerticalLayoutGroup>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.VerticalLayoutGroup>
extern MethodInfo IList_1_get_Item_m44860_MethodInfo;
extern MethodInfo IList_1_set_Item_m44861_MethodInfo;
static PropertyInfo IList_1_t7903____Item_PropertyInfo = 
{
	&IList_1_t7903_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44860_MethodInfo/* get */
	, &IList_1_set_Item_m44861_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7903_PropertyInfos[] =
{
	&IList_1_t7903____Item_PropertyInfo,
	NULL
};
extern Il2CppType VerticalLayoutGroup_t400_0_0_0;
static ParameterInfo IList_1_t7903_IList_1_IndexOf_m44862_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &VerticalLayoutGroup_t400_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44862_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.VerticalLayoutGroup>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44862_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7903_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7903_IList_1_IndexOf_m44862_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44862_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType VerticalLayoutGroup_t400_0_0_0;
static ParameterInfo IList_1_t7903_IList_1_Insert_m44863_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &VerticalLayoutGroup_t400_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44863_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.VerticalLayoutGroup>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44863_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7903_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7903_IList_1_Insert_m44863_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44863_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7903_IList_1_RemoveAt_m44864_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44864_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.VerticalLayoutGroup>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44864_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7903_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7903_IList_1_RemoveAt_m44864_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44864_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7903_IList_1_get_Item_m44860_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType VerticalLayoutGroup_t400_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44860_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.VerticalLayoutGroup>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44860_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7903_il2cpp_TypeInfo/* declaring_type */
	, &VerticalLayoutGroup_t400_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7903_IList_1_get_Item_m44860_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44860_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType VerticalLayoutGroup_t400_0_0_0;
static ParameterInfo IList_1_t7903_IList_1_set_Item_m44861_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &VerticalLayoutGroup_t400_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44861_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.VerticalLayoutGroup>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44861_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7903_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7903_IList_1_set_Item_m44861_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44861_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7903_MethodInfos[] =
{
	&IList_1_IndexOf_m44862_MethodInfo,
	&IList_1_Insert_m44863_MethodInfo,
	&IList_1_RemoveAt_m44864_MethodInfo,
	&IList_1_get_Item_m44860_MethodInfo,
	&IList_1_set_Item_m44861_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7903_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7902_il2cpp_TypeInfo,
	&IEnumerable_1_t7904_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7903_0_0_0;
extern Il2CppType IList_1_t7903_1_0_0;
struct IList_1_t7903;
extern Il2CppGenericClass IList_1_t7903_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7903_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7903_MethodInfos/* methods */
	, IList_1_t7903_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7903_il2cpp_TypeInfo/* element_class */
	, IList_1_t7903_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7903_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7903_0_0_0/* byval_arg */
	, &IList_1_t7903_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7903_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_80.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3608_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_80MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_82.h"
extern TypeInfo InvokableCall_1_t3609_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_82MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m20001_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m20003_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t3608____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t3608_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3608, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3608_FieldInfos[] =
{
	&CachedInvokableCall_1_t3608____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType VerticalLayoutGroup_t400_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3608_CachedInvokableCall_1__ctor_m19999_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &VerticalLayoutGroup_t400_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m19999_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m19999_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t3608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3608_CachedInvokableCall_1__ctor_m19999_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m19999_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3608_CachedInvokableCall_1_Invoke_m20000_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m20000_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m20000_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t3608_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3608_CachedInvokableCall_1_Invoke_m20000_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m20000_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3608_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m19999_MethodInfo,
	&CachedInvokableCall_1_Invoke_m20000_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m20000_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m20004_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3608_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m20000_MethodInfo,
	&InvokableCall_1_Find_m20004_MethodInfo,
};
extern Il2CppType UnityAction_1_t3610_0_0_0;
extern TypeInfo UnityAction_1_t3610_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisVerticalLayoutGroup_t400_m34814_MethodInfo;
extern TypeInfo VerticalLayoutGroup_t400_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m20006_MethodInfo;
extern TypeInfo VerticalLayoutGroup_t400_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3608_RGCTXData[8] = 
{
	&UnityAction_1_t3610_0_0_0/* Type Usage */,
	&UnityAction_1_t3610_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisVerticalLayoutGroup_t400_m34814_MethodInfo/* Method Usage */,
	&VerticalLayoutGroup_t400_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m20006_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m20001_MethodInfo/* Method Usage */,
	&VerticalLayoutGroup_t400_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m20003_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3608_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3608_1_0_0;
struct CachedInvokableCall_1_t3608;
extern Il2CppGenericClass CachedInvokableCall_1_t3608_GenericClass;
TypeInfo CachedInvokableCall_1_t3608_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3608_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3608_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3609_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3608_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3608_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3608_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3608_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3608_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3608_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3608_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3608)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.UI.VerticalLayoutGroup>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_87.h"
extern TypeInfo UnityAction_1_t3610_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.VerticalLayoutGroup>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_87MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.VerticalLayoutGroup>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.VerticalLayoutGroup>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisVerticalLayoutGroup_t400_m34814(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>
extern Il2CppType UnityAction_1_t3610_0_0_1;
FieldInfo InvokableCall_1_t3609____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3610_0_0_1/* type */
	, &InvokableCall_1_t3609_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3609, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3609_FieldInfos[] =
{
	&InvokableCall_1_t3609____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3609_InvokableCall_1__ctor_m20001_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m20001_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m20001_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t3609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3609_InvokableCall_1__ctor_m20001_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m20001_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3610_0_0_0;
static ParameterInfo InvokableCall_1_t3609_InvokableCall_1__ctor_m20002_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3610_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m20002_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m20002_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t3609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3609_InvokableCall_1__ctor_m20002_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m20002_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t3609_InvokableCall_1_Invoke_m20003_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m20003_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m20003_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t3609_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3609_InvokableCall_1_Invoke_m20003_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m20003_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3609_InvokableCall_1_Find_m20004_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m20004_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.VerticalLayoutGroup>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m20004_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t3609_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3609_InvokableCall_1_Find_m20004_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m20004_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3609_MethodInfos[] =
{
	&InvokableCall_1__ctor_m20001_MethodInfo,
	&InvokableCall_1__ctor_m20002_MethodInfo,
	&InvokableCall_1_Invoke_m20003_MethodInfo,
	&InvokableCall_1_Find_m20004_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3609_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m20003_MethodInfo,
	&InvokableCall_1_Find_m20004_MethodInfo,
};
extern TypeInfo UnityAction_1_t3610_il2cpp_TypeInfo;
extern TypeInfo VerticalLayoutGroup_t400_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3609_RGCTXData[5] = 
{
	&UnityAction_1_t3610_0_0_0/* Type Usage */,
	&UnityAction_1_t3610_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisVerticalLayoutGroup_t400_m34814_MethodInfo/* Method Usage */,
	&VerticalLayoutGroup_t400_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m20006_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3609_0_0_0;
extern Il2CppType InvokableCall_1_t3609_1_0_0;
struct InvokableCall_1_t3609;
extern Il2CppGenericClass InvokableCall_1_t3609_GenericClass;
TypeInfo InvokableCall_1_t3609_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3609_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3609_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3609_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3609_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3609_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3609_0_0_0/* byval_arg */
	, &InvokableCall_1_t3609_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3609_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3609_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3609)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.VerticalLayoutGroup>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.VerticalLayoutGroup>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.VerticalLayoutGroup>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.VerticalLayoutGroup>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.VerticalLayoutGroup>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3610_UnityAction_1__ctor_m20005_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m20005_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.VerticalLayoutGroup>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m20005_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t3610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3610_UnityAction_1__ctor_m20005_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m20005_GenericMethod/* genericMethod */

};
extern Il2CppType VerticalLayoutGroup_t400_0_0_0;
static ParameterInfo UnityAction_1_t3610_UnityAction_1_Invoke_m20006_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &VerticalLayoutGroup_t400_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m20006_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.VerticalLayoutGroup>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m20006_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t3610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3610_UnityAction_1_Invoke_m20006_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m20006_GenericMethod/* genericMethod */

};
extern Il2CppType VerticalLayoutGroup_t400_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3610_UnityAction_1_BeginInvoke_m20007_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &VerticalLayoutGroup_t400_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m20007_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.VerticalLayoutGroup>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m20007_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t3610_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3610_UnityAction_1_BeginInvoke_m20007_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m20007_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t3610_UnityAction_1_EndInvoke_m20008_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m20008_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.VerticalLayoutGroup>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m20008_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t3610_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3610_UnityAction_1_EndInvoke_m20008_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m20008_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3610_MethodInfos[] =
{
	&UnityAction_1__ctor_m20005_MethodInfo,
	&UnityAction_1_Invoke_m20006_MethodInfo,
	&UnityAction_1_BeginInvoke_m20007_MethodInfo,
	&UnityAction_1_EndInvoke_m20008_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m20007_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m20008_MethodInfo;
static MethodInfo* UnityAction_1_t3610_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m20006_MethodInfo,
	&UnityAction_1_BeginInvoke_m20007_MethodInfo,
	&UnityAction_1_EndInvoke_m20008_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3610_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3610_1_0_0;
struct UnityAction_1_t3610;
extern Il2CppGenericClass UnityAction_1_t3610_GenericClass;
TypeInfo UnityAction_1_t3610_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3610_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3610_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3610_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3610_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3610_0_0_0/* byval_arg */
	, &UnityAction_1_t3610_1_0_0/* this_arg */
	, UnityAction_1_t3610_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3610_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3610)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6226_il2cpp_TypeInfo;

// UnityEngine.UI.Mask
#include "UnityEngine_UI_UnityEngine_UI_Mask.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Mask>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Mask>
extern MethodInfo IEnumerator_1_get_Current_m44865_MethodInfo;
static PropertyInfo IEnumerator_1_t6226____Current_PropertyInfo = 
{
	&IEnumerator_1_t6226_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44865_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6226_PropertyInfos[] =
{
	&IEnumerator_1_t6226____Current_PropertyInfo,
	NULL
};
extern Il2CppType Mask_t401_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44865_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Mask>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44865_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6226_il2cpp_TypeInfo/* declaring_type */
	, &Mask_t401_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44865_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6226_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44865_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6226_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6226_0_0_0;
extern Il2CppType IEnumerator_1_t6226_1_0_0;
struct IEnumerator_1_t6226;
extern Il2CppGenericClass IEnumerator_1_t6226_GenericClass;
TypeInfo IEnumerator_1_t6226_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6226_MethodInfos/* methods */
	, IEnumerator_1_t6226_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6226_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6226_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6226_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6226_0_0_0/* byval_arg */
	, &IEnumerator_1_t6226_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6226_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.Mask>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_260.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3611_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.Mask>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_260MethodDeclarations.h"

extern TypeInfo Mask_t401_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m20013_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMask_t401_m34816_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Mask>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Mask>(System.Int32)
#define Array_InternalArray__get_Item_TisMask_t401_m34816(__this, p0, method) (Mask_t401 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Mask>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Mask>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Mask>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Mask>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Mask>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Mask>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3611____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3611_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3611, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3611____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3611_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3611, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3611_FieldInfos[] =
{
	&InternalEnumerator_1_t3611____array_0_FieldInfo,
	&InternalEnumerator_1_t3611____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20010_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3611____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3611_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20010_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3611____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3611_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m20013_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3611_PropertyInfos[] =
{
	&InternalEnumerator_1_t3611____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3611____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3611_InternalEnumerator_1__ctor_m20009_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m20009_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Mask>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m20009_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3611_InternalEnumerator_1__ctor_m20009_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m20009_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20010_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Mask>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20010_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3611_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20010_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m20011_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Mask>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m20011_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3611_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m20011_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m20012_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Mask>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m20012_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3611_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m20012_GenericMethod/* genericMethod */

};
extern Il2CppType Mask_t401_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m20013_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Mask>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m20013_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3611_il2cpp_TypeInfo/* declaring_type */
	, &Mask_t401_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m20013_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3611_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m20009_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20010_MethodInfo,
	&InternalEnumerator_1_Dispose_m20011_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20012_MethodInfo,
	&InternalEnumerator_1_get_Current_m20013_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m20012_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m20011_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3611_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20010_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20012_MethodInfo,
	&InternalEnumerator_1_Dispose_m20011_MethodInfo,
	&InternalEnumerator_1_get_Current_m20013_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3611_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6226_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3611_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6226_il2cpp_TypeInfo, 7},
};
extern TypeInfo Mask_t401_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3611_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m20013_MethodInfo/* Method Usage */,
	&Mask_t401_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMask_t401_m34816_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3611_0_0_0;
extern Il2CppType InternalEnumerator_1_t3611_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3611_GenericClass;
TypeInfo InternalEnumerator_1_t3611_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3611_MethodInfos/* methods */
	, InternalEnumerator_1_t3611_PropertyInfos/* properties */
	, InternalEnumerator_1_t3611_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3611_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3611_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3611_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3611_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3611_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3611_1_0_0/* this_arg */
	, InternalEnumerator_1_t3611_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3611_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3611_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3611)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7905_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>
extern MethodInfo ICollection_1_get_Count_m44866_MethodInfo;
static PropertyInfo ICollection_1_t7905____Count_PropertyInfo = 
{
	&ICollection_1_t7905_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44866_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44867_MethodInfo;
static PropertyInfo ICollection_1_t7905____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7905_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44867_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7905_PropertyInfos[] =
{
	&ICollection_1_t7905____Count_PropertyInfo,
	&ICollection_1_t7905____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44866_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>::get_Count()
MethodInfo ICollection_1_get_Count_m44866_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7905_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44866_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44867_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44867_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7905_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44867_GenericMethod/* genericMethod */

};
extern Il2CppType Mask_t401_0_0_0;
extern Il2CppType Mask_t401_0_0_0;
static ParameterInfo ICollection_1_t7905_ICollection_1_Add_m44868_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Mask_t401_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44868_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>::Add(T)
MethodInfo ICollection_1_Add_m44868_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7905_ICollection_1_Add_m44868_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44868_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44869_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>::Clear()
MethodInfo ICollection_1_Clear_m44869_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44869_GenericMethod/* genericMethod */

};
extern Il2CppType Mask_t401_0_0_0;
static ParameterInfo ICollection_1_t7905_ICollection_1_Contains_m44870_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Mask_t401_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44870_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>::Contains(T)
MethodInfo ICollection_1_Contains_m44870_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7905_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7905_ICollection_1_Contains_m44870_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44870_GenericMethod/* genericMethod */

};
extern Il2CppType MaskU5BU5D_t5638_0_0_0;
extern Il2CppType MaskU5BU5D_t5638_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7905_ICollection_1_CopyTo_m44871_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MaskU5BU5D_t5638_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44871_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44871_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7905_ICollection_1_CopyTo_m44871_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44871_GenericMethod/* genericMethod */

};
extern Il2CppType Mask_t401_0_0_0;
static ParameterInfo ICollection_1_t7905_ICollection_1_Remove_m44872_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Mask_t401_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44872_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Mask>::Remove(T)
MethodInfo ICollection_1_Remove_m44872_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7905_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7905_ICollection_1_Remove_m44872_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44872_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7905_MethodInfos[] =
{
	&ICollection_1_get_Count_m44866_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44867_MethodInfo,
	&ICollection_1_Add_m44868_MethodInfo,
	&ICollection_1_Clear_m44869_MethodInfo,
	&ICollection_1_Contains_m44870_MethodInfo,
	&ICollection_1_CopyTo_m44871_MethodInfo,
	&ICollection_1_Remove_m44872_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7907_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7905_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7907_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7905_0_0_0;
extern Il2CppType ICollection_1_t7905_1_0_0;
struct ICollection_1_t7905;
extern Il2CppGenericClass ICollection_1_t7905_GenericClass;
TypeInfo ICollection_1_t7905_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7905_MethodInfos/* methods */
	, ICollection_1_t7905_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7905_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7905_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7905_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7905_0_0_0/* byval_arg */
	, &ICollection_1_t7905_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7905_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Mask>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Mask>
extern Il2CppType IEnumerator_1_t6226_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44873_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Mask>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44873_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7907_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6226_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44873_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7907_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44873_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7907_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7907_0_0_0;
extern Il2CppType IEnumerable_1_t7907_1_0_0;
struct IEnumerable_1_t7907;
extern Il2CppGenericClass IEnumerable_1_t7907_GenericClass;
TypeInfo IEnumerable_1_t7907_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7907_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7907_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7907_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7907_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7907_0_0_0/* byval_arg */
	, &IEnumerable_1_t7907_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7907_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7906_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Mask>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Mask>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Mask>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.Mask>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Mask>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Mask>
extern MethodInfo IList_1_get_Item_m44874_MethodInfo;
extern MethodInfo IList_1_set_Item_m44875_MethodInfo;
static PropertyInfo IList_1_t7906____Item_PropertyInfo = 
{
	&IList_1_t7906_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44874_MethodInfo/* get */
	, &IList_1_set_Item_m44875_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7906_PropertyInfos[] =
{
	&IList_1_t7906____Item_PropertyInfo,
	NULL
};
extern Il2CppType Mask_t401_0_0_0;
static ParameterInfo IList_1_t7906_IList_1_IndexOf_m44876_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Mask_t401_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44876_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Mask>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44876_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7906_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7906_IList_1_IndexOf_m44876_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44876_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Mask_t401_0_0_0;
static ParameterInfo IList_1_t7906_IList_1_Insert_m44877_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Mask_t401_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44877_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Mask>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44877_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7906_IList_1_Insert_m44877_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44877_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7906_IList_1_RemoveAt_m44878_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44878_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Mask>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44878_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7906_IList_1_RemoveAt_m44878_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44878_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7906_IList_1_get_Item_m44874_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Mask_t401_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44874_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.Mask>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44874_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7906_il2cpp_TypeInfo/* declaring_type */
	, &Mask_t401_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7906_IList_1_get_Item_m44874_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44874_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Mask_t401_0_0_0;
static ParameterInfo IList_1_t7906_IList_1_set_Item_m44875_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Mask_t401_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44875_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Mask>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44875_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7906_IList_1_set_Item_m44875_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44875_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7906_MethodInfos[] =
{
	&IList_1_IndexOf_m44876_MethodInfo,
	&IList_1_Insert_m44877_MethodInfo,
	&IList_1_RemoveAt_m44878_MethodInfo,
	&IList_1_get_Item_m44874_MethodInfo,
	&IList_1_set_Item_m44875_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7906_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7905_il2cpp_TypeInfo,
	&IEnumerable_1_t7907_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7906_0_0_0;
extern Il2CppType IList_1_t7906_1_0_0;
struct IList_1_t7906;
extern Il2CppGenericClass IList_1_t7906_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7906_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7906_MethodInfos/* methods */
	, IList_1_t7906_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7906_il2cpp_TypeInfo/* element_class */
	, IList_1_t7906_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7906_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7906_0_0_0/* byval_arg */
	, &IList_1_t7906_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7906_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7908_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>
extern MethodInfo ICollection_1_get_Count_m44879_MethodInfo;
static PropertyInfo ICollection_1_t7908____Count_PropertyInfo = 
{
	&ICollection_1_t7908_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44879_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44880_MethodInfo;
static PropertyInfo ICollection_1_t7908____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7908_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44880_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7908_PropertyInfos[] =
{
	&ICollection_1_t7908____Count_PropertyInfo,
	&ICollection_1_t7908____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44879_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>::get_Count()
MethodInfo ICollection_1_get_Count_m44879_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7908_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44879_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44880_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44880_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7908_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44880_GenericMethod/* genericMethod */

};
extern Il2CppType IGraphicEnabledDisabled_t465_0_0_0;
extern Il2CppType IGraphicEnabledDisabled_t465_0_0_0;
static ParameterInfo ICollection_1_t7908_ICollection_1_Add_m44881_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IGraphicEnabledDisabled_t465_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44881_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>::Add(T)
MethodInfo ICollection_1_Add_m44881_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7908_ICollection_1_Add_m44881_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44881_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44882_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>::Clear()
MethodInfo ICollection_1_Clear_m44882_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44882_GenericMethod/* genericMethod */

};
extern Il2CppType IGraphicEnabledDisabled_t465_0_0_0;
static ParameterInfo ICollection_1_t7908_ICollection_1_Contains_m44883_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IGraphicEnabledDisabled_t465_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44883_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>::Contains(T)
MethodInfo ICollection_1_Contains_m44883_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7908_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7908_ICollection_1_Contains_m44883_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44883_GenericMethod/* genericMethod */

};
extern Il2CppType IGraphicEnabledDisabledU5BU5D_t5639_0_0_0;
extern Il2CppType IGraphicEnabledDisabledU5BU5D_t5639_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7908_ICollection_1_CopyTo_m44884_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IGraphicEnabledDisabledU5BU5D_t5639_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44884_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44884_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7908_ICollection_1_CopyTo_m44884_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44884_GenericMethod/* genericMethod */

};
extern Il2CppType IGraphicEnabledDisabled_t465_0_0_0;
static ParameterInfo ICollection_1_t7908_ICollection_1_Remove_m44885_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IGraphicEnabledDisabled_t465_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44885_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IGraphicEnabledDisabled>::Remove(T)
MethodInfo ICollection_1_Remove_m44885_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7908_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7908_ICollection_1_Remove_m44885_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44885_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7908_MethodInfos[] =
{
	&ICollection_1_get_Count_m44879_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44880_MethodInfo,
	&ICollection_1_Add_m44881_MethodInfo,
	&ICollection_1_Clear_m44882_MethodInfo,
	&ICollection_1_Contains_m44883_MethodInfo,
	&ICollection_1_CopyTo_m44884_MethodInfo,
	&ICollection_1_Remove_m44885_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7910_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7908_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7910_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7908_0_0_0;
extern Il2CppType ICollection_1_t7908_1_0_0;
struct ICollection_1_t7908;
extern Il2CppGenericClass ICollection_1_t7908_GenericClass;
TypeInfo ICollection_1_t7908_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7908_MethodInfos/* methods */
	, ICollection_1_t7908_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7908_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7908_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7908_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7908_0_0_0/* byval_arg */
	, &ICollection_1_t7908_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7908_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IGraphicEnabledDisabled>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IGraphicEnabledDisabled>
extern Il2CppType IEnumerator_1_t6228_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44886_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IGraphicEnabledDisabled>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44886_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7910_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6228_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44886_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7910_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44886_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7910_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7910_0_0_0;
extern Il2CppType IEnumerable_1_t7910_1_0_0;
struct IEnumerable_1_t7910;
extern Il2CppGenericClass IEnumerable_1_t7910_GenericClass;
TypeInfo IEnumerable_1_t7910_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7910_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7910_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7910_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7910_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7910_0_0_0/* byval_arg */
	, &IEnumerable_1_t7910_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7910_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6228_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>
extern MethodInfo IEnumerator_1_get_Current_m44887_MethodInfo;
static PropertyInfo IEnumerator_1_t6228____Current_PropertyInfo = 
{
	&IEnumerator_1_t6228_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44887_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6228_PropertyInfos[] =
{
	&IEnumerator_1_t6228____Current_PropertyInfo,
	NULL
};
extern Il2CppType IGraphicEnabledDisabled_t465_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44887_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44887_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6228_il2cpp_TypeInfo/* declaring_type */
	, &IGraphicEnabledDisabled_t465_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44887_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6228_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44887_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6228_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6228_0_0_0;
extern Il2CppType IEnumerator_1_t6228_1_0_0;
struct IEnumerator_1_t6228;
extern Il2CppGenericClass IEnumerator_1_t6228_GenericClass;
TypeInfo IEnumerator_1_t6228_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6228_MethodInfos/* methods */
	, IEnumerator_1_t6228_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6228_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6228_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6228_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6228_0_0_0/* byval_arg */
	, &IEnumerator_1_t6228_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6228_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_261.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3612_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_261MethodDeclarations.h"

extern TypeInfo IGraphicEnabledDisabled_t465_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m20018_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIGraphicEnabledDisabled_t465_m34827_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.IGraphicEnabledDisabled>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.IGraphicEnabledDisabled>(System.Int32)
#define Array_InternalArray__get_Item_TisIGraphicEnabledDisabled_t465_m34827(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3612____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3612_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3612, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3612____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3612_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3612, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3612_FieldInfos[] =
{
	&InternalEnumerator_1_t3612____array_0_FieldInfo,
	&InternalEnumerator_1_t3612____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20015_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3612____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3612_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20015_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3612____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3612_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m20018_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3612_PropertyInfos[] =
{
	&InternalEnumerator_1_t3612____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3612____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3612_InternalEnumerator_1__ctor_m20014_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m20014_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m20014_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3612_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3612_InternalEnumerator_1__ctor_m20014_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m20014_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20015_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20015_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3612_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20015_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m20016_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m20016_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3612_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m20016_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m20017_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m20017_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3612_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m20017_GenericMethod/* genericMethod */

};
extern Il2CppType IGraphicEnabledDisabled_t465_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m20018_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.IGraphicEnabledDisabled>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m20018_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3612_il2cpp_TypeInfo/* declaring_type */
	, &IGraphicEnabledDisabled_t465_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m20018_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3612_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m20014_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20015_MethodInfo,
	&InternalEnumerator_1_Dispose_m20016_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20017_MethodInfo,
	&InternalEnumerator_1_get_Current_m20018_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m20017_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m20016_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3612_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20015_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20017_MethodInfo,
	&InternalEnumerator_1_Dispose_m20016_MethodInfo,
	&InternalEnumerator_1_get_Current_m20018_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3612_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6228_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3612_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6228_il2cpp_TypeInfo, 7},
};
extern TypeInfo IGraphicEnabledDisabled_t465_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3612_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m20018_MethodInfo/* Method Usage */,
	&IGraphicEnabledDisabled_t465_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIGraphicEnabledDisabled_t465_m34827_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3612_0_0_0;
extern Il2CppType InternalEnumerator_1_t3612_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3612_GenericClass;
TypeInfo InternalEnumerator_1_t3612_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3612_MethodInfos/* methods */
	, InternalEnumerator_1_t3612_PropertyInfos/* properties */
	, InternalEnumerator_1_t3612_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3612_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3612_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3612_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3612_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3612_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3612_1_0_0/* this_arg */
	, InternalEnumerator_1_t3612_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3612_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3612_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3612)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7909_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.IGraphicEnabledDisabled>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IGraphicEnabledDisabled>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IGraphicEnabledDisabled>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.IGraphicEnabledDisabled>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IGraphicEnabledDisabled>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.IGraphicEnabledDisabled>
extern MethodInfo IList_1_get_Item_m44888_MethodInfo;
extern MethodInfo IList_1_set_Item_m44889_MethodInfo;
static PropertyInfo IList_1_t7909____Item_PropertyInfo = 
{
	&IList_1_t7909_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44888_MethodInfo/* get */
	, &IList_1_set_Item_m44889_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7909_PropertyInfos[] =
{
	&IList_1_t7909____Item_PropertyInfo,
	NULL
};
extern Il2CppType IGraphicEnabledDisabled_t465_0_0_0;
static ParameterInfo IList_1_t7909_IList_1_IndexOf_m44890_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IGraphicEnabledDisabled_t465_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44890_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.IGraphicEnabledDisabled>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44890_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7909_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7909_IList_1_IndexOf_m44890_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44890_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IGraphicEnabledDisabled_t465_0_0_0;
static ParameterInfo IList_1_t7909_IList_1_Insert_m44891_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IGraphicEnabledDisabled_t465_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44891_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IGraphicEnabledDisabled>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44891_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7909_IList_1_Insert_m44891_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44891_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7909_IList_1_RemoveAt_m44892_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44892_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IGraphicEnabledDisabled>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44892_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7909_IList_1_RemoveAt_m44892_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44892_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7909_IList_1_get_Item_m44888_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IGraphicEnabledDisabled_t465_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44888_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.IGraphicEnabledDisabled>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44888_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7909_il2cpp_TypeInfo/* declaring_type */
	, &IGraphicEnabledDisabled_t465_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7909_IList_1_get_Item_m44888_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44888_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IGraphicEnabledDisabled_t465_0_0_0;
static ParameterInfo IList_1_t7909_IList_1_set_Item_m44889_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IGraphicEnabledDisabled_t465_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44889_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IGraphicEnabledDisabled>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44889_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7909_IList_1_set_Item_m44889_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44889_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7909_MethodInfos[] =
{
	&IList_1_IndexOf_m44890_MethodInfo,
	&IList_1_Insert_m44891_MethodInfo,
	&IList_1_RemoveAt_m44892_MethodInfo,
	&IList_1_get_Item_m44888_MethodInfo,
	&IList_1_set_Item_m44889_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7909_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7908_il2cpp_TypeInfo,
	&IEnumerable_1_t7910_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7909_0_0_0;
extern Il2CppType IList_1_t7909_1_0_0;
struct IList_1_t7909;
extern Il2CppGenericClass IList_1_t7909_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7909_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7909_MethodInfos/* methods */
	, IList_1_t7909_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7909_il2cpp_TypeInfo/* element_class */
	, IList_1_t7909_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7909_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7909_0_0_0/* byval_arg */
	, &IList_1_t7909_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7909_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7911_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>
extern MethodInfo ICollection_1_get_Count_m44893_MethodInfo;
static PropertyInfo ICollection_1_t7911____Count_PropertyInfo = 
{
	&ICollection_1_t7911_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44893_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44894_MethodInfo;
static PropertyInfo ICollection_1_t7911____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7911_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44894_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7911_PropertyInfos[] =
{
	&ICollection_1_t7911____Count_PropertyInfo,
	&ICollection_1_t7911____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44893_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>::get_Count()
MethodInfo ICollection_1_get_Count_m44893_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7911_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44893_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44894_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44894_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7911_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44894_GenericMethod/* genericMethod */

};
extern Il2CppType IMask_t478_0_0_0;
extern Il2CppType IMask_t478_0_0_0;
static ParameterInfo ICollection_1_t7911_ICollection_1_Add_m44895_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMask_t478_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44895_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>::Add(T)
MethodInfo ICollection_1_Add_m44895_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7911_ICollection_1_Add_m44895_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44895_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44896_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>::Clear()
MethodInfo ICollection_1_Clear_m44896_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44896_GenericMethod/* genericMethod */

};
extern Il2CppType IMask_t478_0_0_0;
static ParameterInfo ICollection_1_t7911_ICollection_1_Contains_m44897_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMask_t478_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44897_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>::Contains(T)
MethodInfo ICollection_1_Contains_m44897_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7911_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7911_ICollection_1_Contains_m44897_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44897_GenericMethod/* genericMethod */

};
extern Il2CppType IMaskU5BU5D_t5640_0_0_0;
extern Il2CppType IMaskU5BU5D_t5640_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7911_ICollection_1_CopyTo_m44898_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IMaskU5BU5D_t5640_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44898_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44898_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7911_ICollection_1_CopyTo_m44898_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44898_GenericMethod/* genericMethod */

};
extern Il2CppType IMask_t478_0_0_0;
static ParameterInfo ICollection_1_t7911_ICollection_1_Remove_m44899_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMask_t478_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44899_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IMask>::Remove(T)
MethodInfo ICollection_1_Remove_m44899_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7911_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7911_ICollection_1_Remove_m44899_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44899_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7911_MethodInfos[] =
{
	&ICollection_1_get_Count_m44893_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44894_MethodInfo,
	&ICollection_1_Add_m44895_MethodInfo,
	&ICollection_1_Clear_m44896_MethodInfo,
	&ICollection_1_Contains_m44897_MethodInfo,
	&ICollection_1_CopyTo_m44898_MethodInfo,
	&ICollection_1_Remove_m44899_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7913_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7911_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7913_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7911_0_0_0;
extern Il2CppType ICollection_1_t7911_1_0_0;
struct ICollection_1_t7911;
extern Il2CppGenericClass ICollection_1_t7911_GenericClass;
TypeInfo ICollection_1_t7911_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7911_MethodInfos/* methods */
	, ICollection_1_t7911_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7911_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7911_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7911_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7911_0_0_0/* byval_arg */
	, &ICollection_1_t7911_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7911_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IMask>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IMask>
extern Il2CppType IEnumerator_1_t6230_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44900_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IMask>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44900_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7913_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6230_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44900_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7913_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44900_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7913_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7913_0_0_0;
extern Il2CppType IEnumerable_1_t7913_1_0_0;
struct IEnumerable_1_t7913;
extern Il2CppGenericClass IEnumerable_1_t7913_GenericClass;
TypeInfo IEnumerable_1_t7913_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7913_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7913_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7913_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7913_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7913_0_0_0/* byval_arg */
	, &IEnumerable_1_t7913_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7913_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6230_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IMask>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IMask>
extern MethodInfo IEnumerator_1_get_Current_m44901_MethodInfo;
static PropertyInfo IEnumerator_1_t6230____Current_PropertyInfo = 
{
	&IEnumerator_1_t6230_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44901_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6230_PropertyInfos[] =
{
	&IEnumerator_1_t6230____Current_PropertyInfo,
	NULL
};
extern Il2CppType IMask_t478_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44901_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IMask>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44901_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6230_il2cpp_TypeInfo/* declaring_type */
	, &IMask_t478_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44901_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6230_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44901_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6230_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6230_0_0_0;
extern Il2CppType IEnumerator_1_t6230_1_0_0;
struct IEnumerator_1_t6230;
extern Il2CppGenericClass IEnumerator_1_t6230_GenericClass;
TypeInfo IEnumerator_1_t6230_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6230_MethodInfos/* methods */
	, IEnumerator_1_t6230_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6230_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6230_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6230_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6230_0_0_0/* byval_arg */
	, &IEnumerator_1_t6230_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6230_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.IMask>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_262.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3613_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.IMask>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_262MethodDeclarations.h"

extern TypeInfo IMask_t478_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m20023_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIMask_t478_m34838_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.IMask>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.IMask>(System.Int32)
#define Array_InternalArray__get_Item_TisIMask_t478_m34838(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IMask>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.IMask>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IMask>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.IMask>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UI.IMask>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.IMask>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3613____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3613_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3613, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3613____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3613_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3613, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3613_FieldInfos[] =
{
	&InternalEnumerator_1_t3613____array_0_FieldInfo,
	&InternalEnumerator_1_t3613____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20020_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3613____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3613_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20020_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3613____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3613_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m20023_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3613_PropertyInfos[] =
{
	&InternalEnumerator_1_t3613____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3613____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3613_InternalEnumerator_1__ctor_m20019_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m20019_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IMask>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m20019_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3613_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3613_InternalEnumerator_1__ctor_m20019_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m20019_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20020_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.IMask>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20020_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3613_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20020_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m20021_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IMask>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m20021_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3613_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m20021_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m20022_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.IMask>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m20022_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3613_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m20022_GenericMethod/* genericMethod */

};
extern Il2CppType IMask_t478_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m20023_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.IMask>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m20023_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3613_il2cpp_TypeInfo/* declaring_type */
	, &IMask_t478_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m20023_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3613_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m20019_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20020_MethodInfo,
	&InternalEnumerator_1_Dispose_m20021_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20022_MethodInfo,
	&InternalEnumerator_1_get_Current_m20023_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m20022_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m20021_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3613_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20020_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20022_MethodInfo,
	&InternalEnumerator_1_Dispose_m20021_MethodInfo,
	&InternalEnumerator_1_get_Current_m20023_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3613_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6230_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3613_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6230_il2cpp_TypeInfo, 7},
};
extern TypeInfo IMask_t478_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3613_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m20023_MethodInfo/* Method Usage */,
	&IMask_t478_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIMask_t478_m34838_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3613_0_0_0;
extern Il2CppType InternalEnumerator_1_t3613_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3613_GenericClass;
TypeInfo InternalEnumerator_1_t3613_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3613_MethodInfos/* methods */
	, InternalEnumerator_1_t3613_PropertyInfos/* properties */
	, InternalEnumerator_1_t3613_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3613_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3613_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3613_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3613_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3613_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3613_1_0_0/* this_arg */
	, InternalEnumerator_1_t3613_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3613_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3613_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3613)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7912_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.IMask>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IMask>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IMask>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.IMask>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IMask>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.IMask>
extern MethodInfo IList_1_get_Item_m44902_MethodInfo;
extern MethodInfo IList_1_set_Item_m44903_MethodInfo;
static PropertyInfo IList_1_t7912____Item_PropertyInfo = 
{
	&IList_1_t7912_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44902_MethodInfo/* get */
	, &IList_1_set_Item_m44903_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7912_PropertyInfos[] =
{
	&IList_1_t7912____Item_PropertyInfo,
	NULL
};
extern Il2CppType IMask_t478_0_0_0;
static ParameterInfo IList_1_t7912_IList_1_IndexOf_m44904_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMask_t478_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44904_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.IMask>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44904_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7912_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7912_IList_1_IndexOf_m44904_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44904_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IMask_t478_0_0_0;
static ParameterInfo IList_1_t7912_IList_1_Insert_m44905_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IMask_t478_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44905_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IMask>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44905_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7912_IList_1_Insert_m44905_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44905_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7912_IList_1_RemoveAt_m44906_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44906_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IMask>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44906_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7912_IList_1_RemoveAt_m44906_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44906_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7912_IList_1_get_Item_m44902_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IMask_t478_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44902_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.IMask>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44902_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7912_il2cpp_TypeInfo/* declaring_type */
	, &IMask_t478_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7912_IList_1_get_Item_m44902_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44902_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IMask_t478_0_0_0;
static ParameterInfo IList_1_t7912_IList_1_set_Item_m44903_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IMask_t478_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44903_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IMask>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44903_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7912_IList_1_set_Item_m44903_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44903_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7912_MethodInfos[] =
{
	&IList_1_IndexOf_m44904_MethodInfo,
	&IList_1_Insert_m44905_MethodInfo,
	&IList_1_RemoveAt_m44906_MethodInfo,
	&IList_1_get_Item_m44902_MethodInfo,
	&IList_1_set_Item_m44903_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7912_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7911_il2cpp_TypeInfo,
	&IEnumerable_1_t7913_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7912_0_0_0;
extern Il2CppType IList_1_t7912_1_0_0;
struct IList_1_t7912;
extern Il2CppGenericClass IList_1_t7912_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7912_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7912_MethodInfos/* methods */
	, IList_1_t7912_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7912_il2cpp_TypeInfo/* element_class */
	, IList_1_t7912_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7912_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7912_0_0_0/* byval_arg */
	, &IList_1_t7912_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7912_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7914_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>
extern MethodInfo ICollection_1_get_Count_m44907_MethodInfo;
static PropertyInfo ICollection_1_t7914____Count_PropertyInfo = 
{
	&ICollection_1_t7914_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44907_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44908_MethodInfo;
static PropertyInfo ICollection_1_t7914____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7914_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44908_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7914_PropertyInfos[] =
{
	&ICollection_1_t7914____Count_PropertyInfo,
	&ICollection_1_t7914____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44907_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>::get_Count()
MethodInfo ICollection_1_get_Count_m44907_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7914_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44907_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44908_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44908_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7914_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44908_GenericMethod/* genericMethod */

};
extern Il2CppType IMaterialModifier_t464_0_0_0;
extern Il2CppType IMaterialModifier_t464_0_0_0;
static ParameterInfo ICollection_1_t7914_ICollection_1_Add_m44909_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMaterialModifier_t464_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44909_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>::Add(T)
MethodInfo ICollection_1_Add_m44909_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7914_ICollection_1_Add_m44909_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44909_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44910_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>::Clear()
MethodInfo ICollection_1_Clear_m44910_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44910_GenericMethod/* genericMethod */

};
extern Il2CppType IMaterialModifier_t464_0_0_0;
static ParameterInfo ICollection_1_t7914_ICollection_1_Contains_m44911_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMaterialModifier_t464_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44911_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>::Contains(T)
MethodInfo ICollection_1_Contains_m44911_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7914_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7914_ICollection_1_Contains_m44911_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44911_GenericMethod/* genericMethod */

};
extern Il2CppType IMaterialModifierU5BU5D_t5641_0_0_0;
extern Il2CppType IMaterialModifierU5BU5D_t5641_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7914_ICollection_1_CopyTo_m44912_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IMaterialModifierU5BU5D_t5641_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44912_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44912_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7914_ICollection_1_CopyTo_m44912_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44912_GenericMethod/* genericMethod */

};
extern Il2CppType IMaterialModifier_t464_0_0_0;
static ParameterInfo ICollection_1_t7914_ICollection_1_Remove_m44913_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMaterialModifier_t464_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44913_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IMaterialModifier>::Remove(T)
MethodInfo ICollection_1_Remove_m44913_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7914_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7914_ICollection_1_Remove_m44913_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44913_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7914_MethodInfos[] =
{
	&ICollection_1_get_Count_m44907_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44908_MethodInfo,
	&ICollection_1_Add_m44909_MethodInfo,
	&ICollection_1_Clear_m44910_MethodInfo,
	&ICollection_1_Contains_m44911_MethodInfo,
	&ICollection_1_CopyTo_m44912_MethodInfo,
	&ICollection_1_Remove_m44913_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7916_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7914_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7916_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7914_0_0_0;
extern Il2CppType ICollection_1_t7914_1_0_0;
struct ICollection_1_t7914;
extern Il2CppGenericClass ICollection_1_t7914_GenericClass;
TypeInfo ICollection_1_t7914_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7914_MethodInfos/* methods */
	, ICollection_1_t7914_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7914_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7914_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7914_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7914_0_0_0/* byval_arg */
	, &ICollection_1_t7914_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7914_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IMaterialModifier>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IMaterialModifier>
extern Il2CppType IEnumerator_1_t6232_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44914_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IMaterialModifier>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44914_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7916_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6232_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44914_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7916_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44914_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7916_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7916_0_0_0;
extern Il2CppType IEnumerable_1_t7916_1_0_0;
struct IEnumerable_1_t7916;
extern Il2CppGenericClass IEnumerable_1_t7916_GenericClass;
TypeInfo IEnumerable_1_t7916_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7916_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7916_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7916_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7916_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7916_0_0_0/* byval_arg */
	, &IEnumerable_1_t7916_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7916_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6232_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IMaterialModifier>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IMaterialModifier>
extern MethodInfo IEnumerator_1_get_Current_m44915_MethodInfo;
static PropertyInfo IEnumerator_1_t6232____Current_PropertyInfo = 
{
	&IEnumerator_1_t6232_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44915_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6232_PropertyInfos[] =
{
	&IEnumerator_1_t6232____Current_PropertyInfo,
	NULL
};
extern Il2CppType IMaterialModifier_t464_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44915_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IMaterialModifier>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44915_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6232_il2cpp_TypeInfo/* declaring_type */
	, &IMaterialModifier_t464_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44915_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6232_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44915_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6232_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6232_0_0_0;
extern Il2CppType IEnumerator_1_t6232_1_0_0;
struct IEnumerator_1_t6232;
extern Il2CppGenericClass IEnumerator_1_t6232_GenericClass;
TypeInfo IEnumerator_1_t6232_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6232_MethodInfos/* methods */
	, IEnumerator_1_t6232_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6232_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6232_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6232_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6232_0_0_0/* byval_arg */
	, &IEnumerator_1_t6232_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6232_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.IMaterialModifier>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_263.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3614_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.IMaterialModifier>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_263MethodDeclarations.h"

extern TypeInfo IMaterialModifier_t464_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m20028_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIMaterialModifier_t464_m34849_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.IMaterialModifier>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.IMaterialModifier>(System.Int32)
#define Array_InternalArray__get_Item_TisIMaterialModifier_t464_m34849(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IMaterialModifier>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.IMaterialModifier>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IMaterialModifier>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.IMaterialModifier>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UI.IMaterialModifier>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.IMaterialModifier>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3614____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3614_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3614, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3614____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3614_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3614, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3614_FieldInfos[] =
{
	&InternalEnumerator_1_t3614____array_0_FieldInfo,
	&InternalEnumerator_1_t3614____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20025_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3614____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3614_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20025_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3614____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3614_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m20028_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3614_PropertyInfos[] =
{
	&InternalEnumerator_1_t3614____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3614____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3614_InternalEnumerator_1__ctor_m20024_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m20024_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IMaterialModifier>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m20024_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3614_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3614_InternalEnumerator_1__ctor_m20024_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m20024_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20025_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.IMaterialModifier>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20025_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3614_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20025_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m20026_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IMaterialModifier>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m20026_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3614_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m20026_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m20027_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.IMaterialModifier>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m20027_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3614_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m20027_GenericMethod/* genericMethod */

};
extern Il2CppType IMaterialModifier_t464_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m20028_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.IMaterialModifier>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m20028_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3614_il2cpp_TypeInfo/* declaring_type */
	, &IMaterialModifier_t464_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m20028_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3614_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m20024_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20025_MethodInfo,
	&InternalEnumerator_1_Dispose_m20026_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20027_MethodInfo,
	&InternalEnumerator_1_get_Current_m20028_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m20027_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m20026_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3614_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20025_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20027_MethodInfo,
	&InternalEnumerator_1_Dispose_m20026_MethodInfo,
	&InternalEnumerator_1_get_Current_m20028_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3614_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6232_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3614_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6232_il2cpp_TypeInfo, 7},
};
extern TypeInfo IMaterialModifier_t464_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3614_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m20028_MethodInfo/* Method Usage */,
	&IMaterialModifier_t464_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIMaterialModifier_t464_m34849_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3614_0_0_0;
extern Il2CppType InternalEnumerator_1_t3614_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3614_GenericClass;
TypeInfo InternalEnumerator_1_t3614_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3614_MethodInfos/* methods */
	, InternalEnumerator_1_t3614_PropertyInfos/* properties */
	, InternalEnumerator_1_t3614_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3614_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3614_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3614_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3614_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3614_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3614_1_0_0/* this_arg */
	, InternalEnumerator_1_t3614_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3614_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3614_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3614)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7915_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.IMaterialModifier>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IMaterialModifier>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IMaterialModifier>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.IMaterialModifier>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IMaterialModifier>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.IMaterialModifier>
extern MethodInfo IList_1_get_Item_m44916_MethodInfo;
extern MethodInfo IList_1_set_Item_m44917_MethodInfo;
static PropertyInfo IList_1_t7915____Item_PropertyInfo = 
{
	&IList_1_t7915_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44916_MethodInfo/* get */
	, &IList_1_set_Item_m44917_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7915_PropertyInfos[] =
{
	&IList_1_t7915____Item_PropertyInfo,
	NULL
};
extern Il2CppType IMaterialModifier_t464_0_0_0;
static ParameterInfo IList_1_t7915_IList_1_IndexOf_m44918_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IMaterialModifier_t464_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44918_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.IMaterialModifier>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44918_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7915_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7915_IList_1_IndexOf_m44918_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44918_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IMaterialModifier_t464_0_0_0;
static ParameterInfo IList_1_t7915_IList_1_Insert_m44919_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IMaterialModifier_t464_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44919_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IMaterialModifier>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44919_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7915_IList_1_Insert_m44919_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44919_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7915_IList_1_RemoveAt_m44920_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44920_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IMaterialModifier>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44920_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7915_IList_1_RemoveAt_m44920_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44920_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7915_IList_1_get_Item_m44916_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IMaterialModifier_t464_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44916_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.IMaterialModifier>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44916_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7915_il2cpp_TypeInfo/* declaring_type */
	, &IMaterialModifier_t464_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7915_IList_1_get_Item_m44916_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44916_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IMaterialModifier_t464_0_0_0;
static ParameterInfo IList_1_t7915_IList_1_set_Item_m44917_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IMaterialModifier_t464_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44917_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IMaterialModifier>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44917_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7915_IList_1_set_Item_m44917_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44917_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7915_MethodInfos[] =
{
	&IList_1_IndexOf_m44918_MethodInfo,
	&IList_1_Insert_m44919_MethodInfo,
	&IList_1_RemoveAt_m44920_MethodInfo,
	&IList_1_get_Item_m44916_MethodInfo,
	&IList_1_set_Item_m44917_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7915_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7914_il2cpp_TypeInfo,
	&IEnumerable_1_t7916_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7915_0_0_0;
extern Il2CppType IList_1_t7915_1_0_0;
struct IList_1_t7915;
extern Il2CppGenericClass IList_1_t7915_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7915_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7915_MethodInfos/* methods */
	, IList_1_t7915_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7915_il2cpp_TypeInfo/* element_class */
	, IList_1_t7915_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7915_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7915_0_0_0/* byval_arg */
	, &IList_1_t7915_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7915_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Mask>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_81.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3615_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Mask>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_81MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Mask>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_83.h"
extern TypeInfo InvokableCall_1_t3616_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Mask>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_83MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m20031_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m20033_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Mask>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Mask>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Mask>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t3615____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t3615_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3615, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3615_FieldInfos[] =
{
	&CachedInvokableCall_1_t3615____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType Mask_t401_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3615_CachedInvokableCall_1__ctor_m20029_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Mask_t401_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m20029_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Mask>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m20029_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t3615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3615_CachedInvokableCall_1__ctor_m20029_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m20029_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3615_CachedInvokableCall_1_Invoke_m20030_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m20030_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Mask>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m20030_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t3615_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3615_CachedInvokableCall_1_Invoke_m20030_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m20030_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3615_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m20029_MethodInfo,
	&CachedInvokableCall_1_Invoke_m20030_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m20030_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m20034_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3615_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m20030_MethodInfo,
	&InvokableCall_1_Find_m20034_MethodInfo,
};
extern Il2CppType UnityAction_1_t3617_0_0_0;
extern TypeInfo UnityAction_1_t3617_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisMask_t401_m34859_MethodInfo;
extern TypeInfo Mask_t401_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m20036_MethodInfo;
extern TypeInfo Mask_t401_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3615_RGCTXData[8] = 
{
	&UnityAction_1_t3617_0_0_0/* Type Usage */,
	&UnityAction_1_t3617_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMask_t401_m34859_MethodInfo/* Method Usage */,
	&Mask_t401_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m20036_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m20031_MethodInfo/* Method Usage */,
	&Mask_t401_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m20033_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3615_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3615_1_0_0;
struct CachedInvokableCall_1_t3615;
extern Il2CppGenericClass CachedInvokableCall_1_t3615_GenericClass;
TypeInfo CachedInvokableCall_1_t3615_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3615_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3615_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3616_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3615_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3615_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3615_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3615_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3615_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3615_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3615_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3615)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_88.h"
extern TypeInfo UnityAction_1_t3617_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_88MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.Mask>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.Mask>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisMask_t401_m34859(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Mask>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Mask>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Mask>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Mask>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Mask>
extern Il2CppType UnityAction_1_t3617_0_0_1;
FieldInfo InvokableCall_1_t3616____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3617_0_0_1/* type */
	, &InvokableCall_1_t3616_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3616, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3616_FieldInfos[] =
{
	&InvokableCall_1_t3616____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3616_InvokableCall_1__ctor_m20031_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m20031_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Mask>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m20031_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t3616_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3616_InvokableCall_1__ctor_m20031_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m20031_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3617_0_0_0;
static ParameterInfo InvokableCall_1_t3616_InvokableCall_1__ctor_m20032_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3617_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m20032_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Mask>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m20032_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t3616_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3616_InvokableCall_1__ctor_m20032_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m20032_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t3616_InvokableCall_1_Invoke_m20033_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m20033_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Mask>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m20033_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t3616_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3616_InvokableCall_1_Invoke_m20033_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m20033_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3616_InvokableCall_1_Find_m20034_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m20034_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Mask>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m20034_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t3616_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3616_InvokableCall_1_Find_m20034_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m20034_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3616_MethodInfos[] =
{
	&InvokableCall_1__ctor_m20031_MethodInfo,
	&InvokableCall_1__ctor_m20032_MethodInfo,
	&InvokableCall_1_Invoke_m20033_MethodInfo,
	&InvokableCall_1_Find_m20034_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3616_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m20033_MethodInfo,
	&InvokableCall_1_Find_m20034_MethodInfo,
};
extern TypeInfo UnityAction_1_t3617_il2cpp_TypeInfo;
extern TypeInfo Mask_t401_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3616_RGCTXData[5] = 
{
	&UnityAction_1_t3617_0_0_0/* Type Usage */,
	&UnityAction_1_t3617_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMask_t401_m34859_MethodInfo/* Method Usage */,
	&Mask_t401_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m20036_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3616_0_0_0;
extern Il2CppType InvokableCall_1_t3616_1_0_0;
struct InvokableCall_1_t3616;
extern Il2CppGenericClass InvokableCall_1_t3616_GenericClass;
TypeInfo InvokableCall_1_t3616_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3616_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3616_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3616_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3616_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3616_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3616_0_0_0/* byval_arg */
	, &InvokableCall_1_t3616_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3616_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3616_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3616)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3617_UnityAction_1__ctor_m20035_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m20035_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m20035_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t3617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3617_UnityAction_1__ctor_m20035_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m20035_GenericMethod/* genericMethod */

};
extern Il2CppType Mask_t401_0_0_0;
static ParameterInfo UnityAction_1_t3617_UnityAction_1_Invoke_m20036_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Mask_t401_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m20036_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m20036_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t3617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3617_UnityAction_1_Invoke_m20036_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m20036_GenericMethod/* genericMethod */

};
extern Il2CppType Mask_t401_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3617_UnityAction_1_BeginInvoke_m20037_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Mask_t401_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m20037_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m20037_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t3617_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3617_UnityAction_1_BeginInvoke_m20037_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m20037_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t3617_UnityAction_1_EndInvoke_m20038_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m20038_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Mask>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m20038_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t3617_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3617_UnityAction_1_EndInvoke_m20038_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m20038_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3617_MethodInfos[] =
{
	&UnityAction_1__ctor_m20035_MethodInfo,
	&UnityAction_1_Invoke_m20036_MethodInfo,
	&UnityAction_1_BeginInvoke_m20037_MethodInfo,
	&UnityAction_1_EndInvoke_m20038_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m20037_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m20038_MethodInfo;
static MethodInfo* UnityAction_1_t3617_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m20036_MethodInfo,
	&UnityAction_1_BeginInvoke_m20037_MethodInfo,
	&UnityAction_1_EndInvoke_m20038_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3617_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3617_1_0_0;
struct UnityAction_1_t3617;
extern Il2CppGenericClass UnityAction_1_t3617_GenericClass;
TypeInfo UnityAction_1_t3617_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3617_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3617_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3617_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3617_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3617_0_0_0/* byval_arg */
	, &UnityAction_1_t3617_1_0_0/* this_arg */
	, UnityAction_1_t3617_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3617_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3617)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_1.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ObjectPool_1_t403_il2cpp_TypeInfo;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_1MethodDeclarations.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "System_System_Collections_Generic_Stack_1_gen_3.h"
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_4.h"
// System.Collections.Generic.List`1<UnityEngine.Canvas>
#include "mscorlib_System_Collections_Generic_List_1_gen_11.h"
extern TypeInfo Stack_1_t3618_il2cpp_TypeInfo;
extern TypeInfo Int32_t93_il2cpp_TypeInfo;
extern TypeInfo List_1_t406_il2cpp_TypeInfo;
extern TypeInfo UnityAction_1_t404_il2cpp_TypeInfo;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "System_System_Collections_Generic_Stack_1_gen_3MethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.Activator
#include "mscorlib_System_ActivatorMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_4MethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
extern MethodInfo ObjectPool_1_get_countAll_m20039_MethodInfo;
extern MethodInfo ObjectPool_1_get_countInactive_m20042_MethodInfo;
extern MethodInfo Stack_1_get_Count_m20052_MethodInfo;
extern MethodInfo Stack_1__ctor_m20043_MethodInfo;
extern MethodInfo Object__ctor_m271_MethodInfo;
extern MethodInfo Activator_CreateInstance_TisList_1_t406_m34877_MethodInfo;
extern MethodInfo ObjectPool_1_set_countAll_m20040_MethodInfo;
extern MethodInfo Stack_1_Pop_m20050_MethodInfo;
extern MethodInfo UnityAction_1_Invoke_m20064_MethodInfo;
extern MethodInfo Stack_1_Peek_m20049_MethodInfo;
extern MethodInfo Object_ReferenceEquals_m6448_MethodInfo;
extern MethodInfo Debug_LogError_m403_MethodInfo;
extern MethodInfo Stack_1_Push_m20051_MethodInfo;
struct Activator_t2179;
// System.Activator
#include "mscorlib_System_Activator.h"
struct Activator_t2179;
// Declaration !!0 System.Activator::CreateInstance<System.Object>()
// !!0 System.Activator::CreateInstance<System.Object>()
 Object_t * Activator_CreateInstance_TisObject_t_m33042_gshared (Object_t * __this/* static, unused */, MethodInfo* method);
#define Activator_CreateInstance_TisObject_t_m33042(__this/* static, unused */, method) (Object_t *)Activator_CreateInstance_TisObject_t_m33042_gshared((Object_t *)__this/* static, unused */, method)
// Declaration !!0 System.Activator::CreateInstance<System.Collections.Generic.List`1<UnityEngine.Canvas>>()
// !!0 System.Activator::CreateInstance<System.Collections.Generic.List`1<UnityEngine.Canvas>>()
#define Activator_CreateInstance_TisList_1_t406_m34877(__this/* static, unused */, method) (List_1_t406 *)Activator_CreateInstance_TisObject_t_m33042_gshared((Object_t *)__this/* static, unused */, method)


// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countAll()
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::set_countAll(System.Int32)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countActive()
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countInactive()
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Get()
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Release(T)
// Metadata Definition UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern Il2CppType Stack_1_t3618_0_0_33;
FieldInfo ObjectPool_1_t403____m_Stack_0_FieldInfo = 
{
	"m_Stack"/* name */
	, &Stack_1_t3618_0_0_33/* type */
	, &ObjectPool_1_t403_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectPool_1_t403, ___m_Stack_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UnityAction_1_t404_0_0_33;
FieldInfo ObjectPool_1_t403____m_ActionOnGet_1_FieldInfo = 
{
	"m_ActionOnGet"/* name */
	, &UnityAction_1_t404_0_0_33/* type */
	, &ObjectPool_1_t403_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectPool_1_t403, ___m_ActionOnGet_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UnityAction_1_t404_0_0_33;
FieldInfo ObjectPool_1_t403____m_ActionOnRelease_2_FieldInfo = 
{
	"m_ActionOnRelease"/* name */
	, &UnityAction_1_t404_0_0_33/* type */
	, &ObjectPool_1_t403_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectPool_1_t403, ___m_ActionOnRelease_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
extern CustomAttributesCache ObjectPool_1_t410__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
FieldInfo ObjectPool_1_t403____U3CcountAllU3Ek__BackingField_3_FieldInfo = 
{
	"<countAll>k__BackingField"/* name */
	, &Int32_t93_0_0_1/* type */
	, &ObjectPool_1_t403_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectPool_1_t403, ___U3CcountAllU3Ek__BackingField_3)/* data */
	, &ObjectPool_1_t410__CustomAttributeCache_U3CcountAllU3Ek__BackingField/* custom_attributes_cache */

};
static FieldInfo* ObjectPool_1_t403_FieldInfos[] =
{
	&ObjectPool_1_t403____m_Stack_0_FieldInfo,
	&ObjectPool_1_t403____m_ActionOnGet_1_FieldInfo,
	&ObjectPool_1_t403____m_ActionOnRelease_2_FieldInfo,
	&ObjectPool_1_t403____U3CcountAllU3Ek__BackingField_3_FieldInfo,
	NULL
};
static PropertyInfo ObjectPool_1_t403____countAll_PropertyInfo = 
{
	&ObjectPool_1_t403_il2cpp_TypeInfo/* parent */
	, "countAll"/* name */
	, &ObjectPool_1_get_countAll_m20039_MethodInfo/* get */
	, &ObjectPool_1_set_countAll_m20040_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ObjectPool_1_get_countActive_m20041_MethodInfo;
static PropertyInfo ObjectPool_1_t403____countActive_PropertyInfo = 
{
	&ObjectPool_1_t403_il2cpp_TypeInfo/* parent */
	, "countActive"/* name */
	, &ObjectPool_1_get_countActive_m20041_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo ObjectPool_1_t403____countInactive_PropertyInfo = 
{
	&ObjectPool_1_t403_il2cpp_TypeInfo/* parent */
	, "countInactive"/* name */
	, &ObjectPool_1_get_countInactive_m20042_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ObjectPool_1_t403_PropertyInfos[] =
{
	&ObjectPool_1_t403____countAll_PropertyInfo,
	&ObjectPool_1_t403____countActive_PropertyInfo,
	&ObjectPool_1_t403____countInactive_PropertyInfo,
	NULL
};
extern Il2CppType UnityAction_1_t404_0_0_0;
extern Il2CppType UnityAction_1_t404_0_0_0;
extern Il2CppType UnityAction_1_t404_0_0_0;
static ParameterInfo ObjectPool_1_t403_ObjectPool_1__ctor_m2560_ParameterInfos[] = 
{
	{"actionOnGet", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t404_0_0_0},
	{"actionOnRelease", 1, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t404_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ObjectPool_1__ctor_m2560_GenericMethod;
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
MethodInfo ObjectPool_1__ctor_m2560_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectPool_1__ctor_m15704_gshared/* method */
	, &ObjectPool_1_t403_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, ObjectPool_1_t403_ObjectPool_1__ctor_m2560_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ObjectPool_1__ctor_m2560_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache ObjectPool_1_t410__CustomAttributeCache_ObjectPool_1_get_countAll_m2570;
extern Il2CppGenericMethod ObjectPool_1_get_countAll_m20039_GenericMethod;
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countAll()
MethodInfo ObjectPool_1_get_countAll_m20039_MethodInfo = 
{
	"get_countAll"/* name */
	, (methodPointerType)&ObjectPool_1_get_countAll_m15706_gshared/* method */
	, &ObjectPool_1_t403_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &ObjectPool_1_t410__CustomAttributeCache_ObjectPool_1_get_countAll_m2570/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ObjectPool_1_get_countAll_m20039_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ObjectPool_1_t403_ObjectPool_1_set_countAll_m20040_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache ObjectPool_1_t410__CustomAttributeCache_ObjectPool_1_set_countAll_m2571;
extern Il2CppGenericMethod ObjectPool_1_set_countAll_m20040_GenericMethod;
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::set_countAll(System.Int32)
MethodInfo ObjectPool_1_set_countAll_m20040_MethodInfo = 
{
	"set_countAll"/* name */
	, (methodPointerType)&ObjectPool_1_set_countAll_m15708_gshared/* method */
	, &ObjectPool_1_t403_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ObjectPool_1_t403_ObjectPool_1_set_countAll_m20040_ParameterInfos/* parameters */
	, &ObjectPool_1_t410__CustomAttributeCache_ObjectPool_1_set_countAll_m2571/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ObjectPool_1_set_countAll_m20040_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ObjectPool_1_get_countActive_m20041_GenericMethod;
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countActive()
MethodInfo ObjectPool_1_get_countActive_m20041_MethodInfo = 
{
	"get_countActive"/* name */
	, (methodPointerType)&ObjectPool_1_get_countActive_m15710_gshared/* method */
	, &ObjectPool_1_t403_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ObjectPool_1_get_countActive_m20041_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ObjectPool_1_get_countInactive_m20042_GenericMethod;
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_countInactive()
MethodInfo ObjectPool_1_get_countInactive_m20042_MethodInfo = 
{
	"get_countInactive"/* name */
	, (methodPointerType)&ObjectPool_1_get_countInactive_m15712_gshared/* method */
	, &ObjectPool_1_t403_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ObjectPool_1_get_countInactive_m20042_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t406_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ObjectPool_1_Get_m2561_GenericMethod;
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Get()
MethodInfo ObjectPool_1_Get_m2561_MethodInfo = 
{
	"Get"/* name */
	, (methodPointerType)&ObjectPool_1_Get_m15714_gshared/* method */
	, &ObjectPool_1_t403_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t406_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ObjectPool_1_Get_m2561_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t406_0_0_0;
extern Il2CppType List_1_t406_0_0_0;
static ParameterInfo ObjectPool_1_t403_ObjectPool_1_Release_m2562_ParameterInfos[] = 
{
	{"element", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t406_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ObjectPool_1_Release_m2562_GenericMethod;
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Release(T)
MethodInfo ObjectPool_1_Release_m2562_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&ObjectPool_1_Release_m15716_gshared/* method */
	, &ObjectPool_1_t403_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ObjectPool_1_t403_ObjectPool_1_Release_m2562_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ObjectPool_1_Release_m2562_GenericMethod/* genericMethod */

};
static MethodInfo* ObjectPool_1_t403_MethodInfos[] =
{
	&ObjectPool_1__ctor_m2560_MethodInfo,
	&ObjectPool_1_get_countAll_m20039_MethodInfo,
	&ObjectPool_1_set_countAll_m20040_MethodInfo,
	&ObjectPool_1_get_countActive_m20041_MethodInfo,
	&ObjectPool_1_get_countInactive_m20042_MethodInfo,
	&ObjectPool_1_Get_m2561_MethodInfo,
	&ObjectPool_1_Release_m2562_MethodInfo,
	NULL
};
static MethodInfo* ObjectPool_1_t403_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
extern TypeInfo Stack_1_t3618_il2cpp_TypeInfo;
extern TypeInfo List_1_t406_il2cpp_TypeInfo;
static Il2CppRGCTXData ObjectPool_1_t403_RGCTXData[12] = 
{
	&Stack_1_t3618_il2cpp_TypeInfo/* Class Usage */,
	&Stack_1__ctor_m20043_MethodInfo/* Method Usage */,
	&ObjectPool_1_get_countAll_m20039_MethodInfo/* Method Usage */,
	&ObjectPool_1_get_countInactive_m20042_MethodInfo/* Method Usage */,
	&Stack_1_get_Count_m20052_MethodInfo/* Method Usage */,
	&List_1_t406_il2cpp_TypeInfo/* Class Usage */,
	&Activator_CreateInstance_TisList_1_t406_m34877_MethodInfo/* Method Usage */,
	&ObjectPool_1_set_countAll_m20040_MethodInfo/* Method Usage */,
	&Stack_1_Pop_m20050_MethodInfo/* Method Usage */,
	&UnityAction_1_Invoke_m20064_MethodInfo/* Method Usage */,
	&Stack_1_Peek_m20049_MethodInfo/* Method Usage */,
	&Stack_1_Push_m20051_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ObjectPool_1_t403_0_0_0;
extern Il2CppType ObjectPool_1_t403_1_0_0;
struct ObjectPool_1_t403;
extern Il2CppGenericClass ObjectPool_1_t403_GenericClass;
extern CustomAttributesCache ObjectPool_1_t410__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
extern CustomAttributesCache ObjectPool_1_t410__CustomAttributeCache_ObjectPool_1_get_countAll_m2570;
extern CustomAttributesCache ObjectPool_1_t410__CustomAttributeCache_ObjectPool_1_set_countAll_m2571;
TypeInfo ObjectPool_1_t403_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectPool`1"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ObjectPool_1_t403_MethodInfos/* methods */
	, ObjectPool_1_t403_PropertyInfos/* properties */
	, ObjectPool_1_t403_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ObjectPool_1_t403_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ObjectPool_1_t403_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ObjectPool_1_t403_il2cpp_TypeInfo/* cast_class */
	, &ObjectPool_1_t403_0_0_0/* byval_arg */
	, &ObjectPool_1_t403_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ObjectPool_1_t403_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, ObjectPool_1_t403_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectPool_1_t403)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_2.h"
extern TypeInfo ArrayTypeMismatchException_t2190_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t3621_il2cpp_TypeInfo;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_2MethodDeclarations.h"
extern MethodInfo Array_CopyTo_m7879_MethodInfo;
extern MethodInfo Array_Reverse_m8849_MethodInfo;
extern MethodInfo ArgumentException__ctor_m12530_MethodInfo;
extern MethodInfo Stack_1_GetEnumerator_m20053_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7659_MethodInfo;
extern MethodInfo Array_Resize_TisList_1_t406_m34876_MethodInfo;
extern MethodInfo Enumerator__ctor_m20059_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
 void Array_Resize_TisObject_t_m32177_gshared (Object_t * __this/* static, unused */, ObjectU5BU5D_t115** p0, int32_t p1, MethodInfo* method);
#define Array_Resize_TisObject_t_m32177(__this/* static, unused */, p0, p1, method) (void)Array_Resize_TisObject_t_m32177_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t115**)p0, (int32_t)p1, method)
// Declaration System.Void System.Array::Resize<System.Collections.Generic.List`1<UnityEngine.Canvas>>(!!0[]&,System.Int32)
// System.Void System.Array::Resize<System.Collections.Generic.List`1<UnityEngine.Canvas>>(!!0[]&,System.Int32)
#define Array_Resize_TisList_1_t406_m34876(__this/* static, unused */, p0, p1, method) (void)Array_Resize_TisObject_t_m32177_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t115**)p0, (int32_t)p1, method)


// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor()
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.get_IsSynchronized()
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.get_SyncRoot()
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.IEnumerable.GetEnumerator()
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Peek()
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Pop()
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Push(T)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Count()
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::GetEnumerator()
 Enumerator_t3621  Stack_1_GetEnumerator_m20053 (Stack_1_t3618 * __this, MethodInfo* method){
	{
		Enumerator_t3621  L_0 = {0};
		Enumerator__ctor_m20059(&L_0, __this, /*hidden argument*/&Enumerator__ctor_m20059_MethodInfo);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern Il2CppType Int32_t93_0_0_32849;
FieldInfo Stack_1_t3618____INITIAL_SIZE_0_FieldInfo = 
{
	"INITIAL_SIZE"/* name */
	, &Int32_t93_0_0_32849/* type */
	, &Stack_1_t3618_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType List_1U5BU5D_t3619_0_0_1;
FieldInfo Stack_1_t3618_____array_1_FieldInfo = 
{
	"_array"/* name */
	, &List_1U5BU5D_t3619_0_0_1/* type */
	, &Stack_1_t3618_il2cpp_TypeInfo/* parent */
	, offsetof(Stack_1_t3618, ____array_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo Stack_1_t3618_____size_2_FieldInfo = 
{
	"_size"/* name */
	, &Int32_t93_0_0_1/* type */
	, &Stack_1_t3618_il2cpp_TypeInfo/* parent */
	, offsetof(Stack_1_t3618, ____size_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo Stack_1_t3618_____version_3_FieldInfo = 
{
	"_version"/* name */
	, &Int32_t93_0_0_1/* type */
	, &Stack_1_t3618_il2cpp_TypeInfo/* parent */
	, offsetof(Stack_1_t3618, ____version_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Stack_1_t3618_FieldInfos[] =
{
	&Stack_1_t3618____INITIAL_SIZE_0_FieldInfo,
	&Stack_1_t3618_____array_1_FieldInfo,
	&Stack_1_t3618_____size_2_FieldInfo,
	&Stack_1_t3618_____version_3_FieldInfo,
	NULL
};
static const int32_t Stack_1_t3618____INITIAL_SIZE_0_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry Stack_1_t3618____INITIAL_SIZE_0_DefaultValue = 
{
	&Stack_1_t3618____INITIAL_SIZE_0_FieldInfo/* field */
	, { (char*)&Stack_1_t3618____INITIAL_SIZE_0_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* Stack_1_t3618_FieldDefaultValues[] = 
{
	&Stack_1_t3618____INITIAL_SIZE_0_DefaultValue,
	NULL
};
extern MethodInfo Stack_1_System_Collections_ICollection_get_IsSynchronized_m20044_MethodInfo;
static PropertyInfo Stack_1_t3618____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&Stack_1_t3618_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.IsSynchronized"/* name */
	, &Stack_1_System_Collections_ICollection_get_IsSynchronized_m20044_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Stack_1_System_Collections_ICollection_get_SyncRoot_m20045_MethodInfo;
static PropertyInfo Stack_1_t3618____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&Stack_1_t3618_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.SyncRoot"/* name */
	, &Stack_1_System_Collections_ICollection_get_SyncRoot_m20045_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo Stack_1_t3618____Count_PropertyInfo = 
{
	&Stack_1_t3618_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &Stack_1_get_Count_m20052_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Stack_1_t3618_PropertyInfos[] =
{
	&Stack_1_t3618____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&Stack_1_t3618____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&Stack_1_t3618____Count_PropertyInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1__ctor_m20043_GenericMethod;
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor()
MethodInfo Stack_1__ctor_m20043_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Stack_1__ctor_m15717_gshared/* method */
	, &Stack_1_t3618_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1__ctor_m20043_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_System_Collections_ICollection_get_IsSynchronized_m20044_GenericMethod;
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.get_IsSynchronized()
MethodInfo Stack_1_System_Collections_ICollection_get_IsSynchronized_m20044_MethodInfo = 
{
	"System.Collections.ICollection.get_IsSynchronized"/* name */
	, (methodPointerType)&Stack_1_System_Collections_ICollection_get_IsSynchronized_m15718_gshared/* method */
	, &Stack_1_t3618_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_System_Collections_ICollection_get_IsSynchronized_m20044_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_System_Collections_ICollection_get_SyncRoot_m20045_GenericMethod;
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.get_SyncRoot()
MethodInfo Stack_1_System_Collections_ICollection_get_SyncRoot_m20045_MethodInfo = 
{
	"System.Collections.ICollection.get_SyncRoot"/* name */
	, (methodPointerType)&Stack_1_System_Collections_ICollection_get_SyncRoot_m15719_gshared/* method */
	, &Stack_1_t3618_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_System_Collections_ICollection_get_SyncRoot_m20045_GenericMethod/* genericMethod */

};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo Stack_1_t3618_Stack_1_System_Collections_ICollection_CopyTo_m20046_ParameterInfos[] = 
{
	{"dest", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
	{"idx", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_System_Collections_ICollection_CopyTo_m20046_GenericMethod;
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
MethodInfo Stack_1_System_Collections_ICollection_CopyTo_m20046_MethodInfo = 
{
	"System.Collections.ICollection.CopyTo"/* name */
	, (methodPointerType)&Stack_1_System_Collections_ICollection_CopyTo_m15720_gshared/* method */
	, &Stack_1_t3618_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, Stack_1_t3618_Stack_1_System_Collections_ICollection_CopyTo_m20046_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_System_Collections_ICollection_CopyTo_m20046_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_1_t3620_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20047_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
MethodInfo Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20047_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator"/* name */
	, (methodPointerType)&Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15721_gshared/* method */
	, &Stack_1_t3618_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t3620_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20047_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_t266_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_System_Collections_IEnumerable_GetEnumerator_m20048_GenericMethod;
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.IEnumerable.GetEnumerator()
MethodInfo Stack_1_System_Collections_IEnumerable_GetEnumerator_m20048_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, (methodPointerType)&Stack_1_System_Collections_IEnumerable_GetEnumerator_m15722_gshared/* method */
	, &Stack_1_t3618_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_System_Collections_IEnumerable_GetEnumerator_m20048_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t406_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_Peek_m20049_GenericMethod;
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Peek()
MethodInfo Stack_1_Peek_m20049_MethodInfo = 
{
	"Peek"/* name */
	, (methodPointerType)&Stack_1_Peek_m15723_gshared/* method */
	, &Stack_1_t3618_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t406_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_Peek_m20049_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t406_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_Pop_m20050_GenericMethod;
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Pop()
MethodInfo Stack_1_Pop_m20050_MethodInfo = 
{
	"Pop"/* name */
	, (methodPointerType)&Stack_1_Pop_m15724_gshared/* method */
	, &Stack_1_t3618_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t406_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_Pop_m20050_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t406_0_0_0;
static ParameterInfo Stack_1_t3618_Stack_1_Push_m20051_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t406_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_Push_m20051_GenericMethod;
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Push(T)
MethodInfo Stack_1_Push_m20051_MethodInfo = 
{
	"Push"/* name */
	, (methodPointerType)&Stack_1_Push_m15725_gshared/* method */
	, &Stack_1_t3618_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, Stack_1_t3618_Stack_1_Push_m20051_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_Push_m20051_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_get_Count_m20052_GenericMethod;
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Count()
MethodInfo Stack_1_get_Count_m20052_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&Stack_1_get_Count_m15726_gshared/* method */
	, &Stack_1_t3618_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_get_Count_m20052_GenericMethod/* genericMethod */

};
extern Il2CppType Enumerator_t3621_0_0_0;
extern void* RuntimeInvoker_Enumerator_t3621 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_GetEnumerator_m20053_GenericMethod;
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::GetEnumerator()
MethodInfo Stack_1_GetEnumerator_m20053_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&Stack_1_GetEnumerator_m20053/* method */
	, &Stack_1_t3618_il2cpp_TypeInfo/* declaring_type */
	, &Enumerator_t3621_0_0_0/* return_type */
	, RuntimeInvoker_Enumerator_t3621/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_GetEnumerator_m20053_GenericMethod/* genericMethod */

};
static MethodInfo* Stack_1_t3618_MethodInfos[] =
{
	&Stack_1__ctor_m20043_MethodInfo,
	&Stack_1_System_Collections_ICollection_get_IsSynchronized_m20044_MethodInfo,
	&Stack_1_System_Collections_ICollection_get_SyncRoot_m20045_MethodInfo,
	&Stack_1_System_Collections_ICollection_CopyTo_m20046_MethodInfo,
	&Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20047_MethodInfo,
	&Stack_1_System_Collections_IEnumerable_GetEnumerator_m20048_MethodInfo,
	&Stack_1_Peek_m20049_MethodInfo,
	&Stack_1_Pop_m20050_MethodInfo,
	&Stack_1_Push_m20051_MethodInfo,
	&Stack_1_get_Count_m20052_MethodInfo,
	&Stack_1_GetEnumerator_m20053_MethodInfo,
	NULL
};
extern MethodInfo Stack_1_System_Collections_ICollection_CopyTo_m20046_MethodInfo;
extern MethodInfo Stack_1_System_Collections_IEnumerable_GetEnumerator_m20048_MethodInfo;
extern MethodInfo Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20047_MethodInfo;
static MethodInfo* Stack_1_t3618_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Stack_1_get_Count_m20052_MethodInfo,
	&Stack_1_System_Collections_ICollection_get_IsSynchronized_m20044_MethodInfo,
	&Stack_1_System_Collections_ICollection_get_SyncRoot_m20045_MethodInfo,
	&Stack_1_System_Collections_ICollection_CopyTo_m20046_MethodInfo,
	&Stack_1_System_Collections_IEnumerable_GetEnumerator_m20048_MethodInfo,
	&Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20047_MethodInfo,
};
extern TypeInfo ICollection_t1206_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7919_il2cpp_TypeInfo;
static TypeInfo* Stack_1_t3618_InterfacesTypeInfos[] = 
{
	&ICollection_t1206_il2cpp_TypeInfo,
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7919_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Stack_1_t3618_InterfacesOffsets[] = 
{
	{ &ICollection_t1206_il2cpp_TypeInfo, 4},
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 8},
	{ &IEnumerable_1_t7919_il2cpp_TypeInfo, 9},
};
extern TypeInfo Enumerator_t3621_il2cpp_TypeInfo;
static Il2CppRGCTXData Stack_1_t3618_RGCTXData[4] = 
{
	&Stack_1_GetEnumerator_m20053_MethodInfo/* Method Usage */,
	&Enumerator_t3621_il2cpp_TypeInfo/* Class Usage */,
	&Array_Resize_TisList_1_t406_m34876_MethodInfo/* Method Usage */,
	&Enumerator__ctor_m20059_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Stack_1_t3618_0_0_0;
extern Il2CppType Stack_1_t3618_1_0_0;
struct Stack_1_t3618;
extern Il2CppGenericClass Stack_1_t3618_GenericClass;
extern CustomAttributesCache Stack_1_t1294__CustomAttributeCache;
TypeInfo Stack_1_t3618_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Stack`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, Stack_1_t3618_MethodInfos/* methods */
	, Stack_1_t3618_PropertyInfos/* properties */
	, Stack_1_t3618_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Stack_1_t3618_il2cpp_TypeInfo/* element_class */
	, Stack_1_t3618_InterfacesTypeInfos/* implemented_interfaces */
	, Stack_1_t3618_VTable/* vtable */
	, &Stack_1_t1294__CustomAttributeCache/* custom_attributes_cache */
	, &Stack_1_t3618_il2cpp_TypeInfo/* cast_class */
	, &Stack_1_t3618_0_0_0/* byval_arg */
	, &Stack_1_t3618_1_0_0/* this_arg */
	, Stack_1_t3618_InterfacesOffsets/* interface_offsets */
	, &Stack_1_t3618_GenericClass/* generic_class */
	, NULL/* generic_container */
	, Stack_1_t3618_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, Stack_1_t3618_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Stack_1_t3618)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 11/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern Il2CppType IEnumerator_1_t3620_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44921_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44921_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7919_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t3620_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44921_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7919_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44921_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7919_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7919_0_0_0;
extern Il2CppType IEnumerable_1_t7919_1_0_0;
struct IEnumerable_1_t7919;
extern Il2CppGenericClass IEnumerable_1_t7919_GenericClass;
TypeInfo IEnumerable_1_t7919_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7919_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7919_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7919_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7919_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7919_0_0_0/* byval_arg */
	, &IEnumerable_1_t7919_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7919_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t3620_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern MethodInfo IEnumerator_1_get_Current_m44922_MethodInfo;
static PropertyInfo IEnumerator_1_t3620____Current_PropertyInfo = 
{
	&IEnumerator_1_t3620_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44922_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t3620_PropertyInfos[] =
{
	&IEnumerator_1_t3620____Current_PropertyInfo,
	NULL
};
extern Il2CppType List_1_t406_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44922_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44922_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t3620_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t406_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44922_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t3620_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44922_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t3620_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t3620_0_0_0;
extern Il2CppType IEnumerator_1_t3620_1_0_0;
struct IEnumerator_1_t3620;
extern Il2CppGenericClass IEnumerator_1_t3620_GenericClass;
TypeInfo IEnumerator_1_t3620_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t3620_MethodInfos/* methods */
	, IEnumerator_1_t3620_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t3620_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t3620_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t3620_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t3620_0_0_0/* byval_arg */
	, &IEnumerator_1_t3620_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t3620_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_264.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3622_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_264MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m20058_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisList_1_t406_m34865_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.List`1<UnityEngine.Canvas>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.List`1<UnityEngine.Canvas>>(System.Int32)
#define Array_InternalArray__get_Item_TisList_1_t406_m34865(__this, p0, method) (List_1_t406 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3622____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3622_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3622, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3622____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3622_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3622, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3622_FieldInfos[] =
{
	&InternalEnumerator_1_t3622____array_0_FieldInfo,
	&InternalEnumerator_1_t3622____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20055_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3622____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3622_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20055_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3622____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3622_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m20058_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3622_PropertyInfos[] =
{
	&InternalEnumerator_1_t3622____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3622____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3622_InternalEnumerator_1__ctor_m20054_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m20054_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m20054_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3622_InternalEnumerator_1__ctor_m20054_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m20054_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20055_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20055_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3622_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20055_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m20056_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m20056_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3622_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m20056_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m20057_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m20057_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3622_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m20057_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t406_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m20058_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m20058_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3622_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t406_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m20058_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3622_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m20054_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20055_MethodInfo,
	&InternalEnumerator_1_Dispose_m20056_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20057_MethodInfo,
	&InternalEnumerator_1_get_Current_m20058_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m20057_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m20056_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3622_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20055_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20057_MethodInfo,
	&InternalEnumerator_1_Dispose_m20056_MethodInfo,
	&InternalEnumerator_1_get_Current_m20058_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3622_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t3620_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3622_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t3620_il2cpp_TypeInfo, 7},
};
extern TypeInfo List_1_t406_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3622_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m20058_MethodInfo/* Method Usage */,
	&List_1_t406_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisList_1_t406_m34865_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3622_0_0_0;
extern Il2CppType InternalEnumerator_1_t3622_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3622_GenericClass;
TypeInfo InternalEnumerator_1_t3622_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3622_MethodInfos/* methods */
	, InternalEnumerator_1_t3622_PropertyInfos/* properties */
	, InternalEnumerator_1_t3622_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3622_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3622_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3622_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3622_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3622_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3622_1_0_0/* this_arg */
	, InternalEnumerator_1_t3622_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3622_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3622_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3622)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7917_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern MethodInfo ICollection_1_get_Count_m44923_MethodInfo;
static PropertyInfo ICollection_1_t7917____Count_PropertyInfo = 
{
	&ICollection_1_t7917_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44923_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44924_MethodInfo;
static PropertyInfo ICollection_1_t7917____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7917_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44924_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7917_PropertyInfos[] =
{
	&ICollection_1_t7917____Count_PropertyInfo,
	&ICollection_1_t7917____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44923_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Count()
MethodInfo ICollection_1_get_Count_m44923_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7917_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44923_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44924_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44924_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7917_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44924_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t406_0_0_0;
static ParameterInfo ICollection_1_t7917_ICollection_1_Add_m44925_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t406_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44925_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Add(T)
MethodInfo ICollection_1_Add_m44925_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7917_ICollection_1_Add_m44925_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44925_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44926_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Clear()
MethodInfo ICollection_1_Clear_m44926_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44926_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t406_0_0_0;
static ParameterInfo ICollection_1_t7917_ICollection_1_Contains_m44927_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t406_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44927_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Contains(T)
MethodInfo ICollection_1_Contains_m44927_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7917_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7917_ICollection_1_Contains_m44927_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44927_GenericMethod/* genericMethod */

};
extern Il2CppType List_1U5BU5D_t3619_0_0_0;
extern Il2CppType List_1U5BU5D_t3619_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7917_ICollection_1_CopyTo_m44928_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &List_1U5BU5D_t3619_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44928_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44928_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7917_ICollection_1_CopyTo_m44928_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44928_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t406_0_0_0;
static ParameterInfo ICollection_1_t7917_ICollection_1_Remove_m44929_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t406_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44929_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Remove(T)
MethodInfo ICollection_1_Remove_m44929_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7917_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7917_ICollection_1_Remove_m44929_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44929_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7917_MethodInfos[] =
{
	&ICollection_1_get_Count_m44923_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44924_MethodInfo,
	&ICollection_1_Add_m44925_MethodInfo,
	&ICollection_1_Clear_m44926_MethodInfo,
	&ICollection_1_Contains_m44927_MethodInfo,
	&ICollection_1_CopyTo_m44928_MethodInfo,
	&ICollection_1_Remove_m44929_MethodInfo,
	NULL
};
static TypeInfo* ICollection_1_t7917_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7919_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7917_0_0_0;
extern Il2CppType ICollection_1_t7917_1_0_0;
struct ICollection_1_t7917;
extern Il2CppGenericClass ICollection_1_t7917_GenericClass;
TypeInfo ICollection_1_t7917_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7917_MethodInfos/* methods */
	, ICollection_1_t7917_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7917_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7917_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7917_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7917_0_0_0/* byval_arg */
	, &ICollection_1_t7917_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7917_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7918_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern MethodInfo IList_1_get_Item_m44930_MethodInfo;
extern MethodInfo IList_1_set_Item_m44931_MethodInfo;
static PropertyInfo IList_1_t7918____Item_PropertyInfo = 
{
	&IList_1_t7918_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44930_MethodInfo/* get */
	, &IList_1_set_Item_m44931_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7918_PropertyInfos[] =
{
	&IList_1_t7918____Item_PropertyInfo,
	NULL
};
extern Il2CppType List_1_t406_0_0_0;
static ParameterInfo IList_1_t7918_IList_1_IndexOf_m44932_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t406_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44932_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44932_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7918_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7918_IList_1_IndexOf_m44932_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44932_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType List_1_t406_0_0_0;
static ParameterInfo IList_1_t7918_IList_1_Insert_m44933_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &List_1_t406_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44933_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44933_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7918_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7918_IList_1_Insert_m44933_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44933_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7918_IList_1_RemoveAt_m44934_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44934_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44934_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7918_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7918_IList_1_RemoveAt_m44934_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44934_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7918_IList_1_get_Item_m44930_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType List_1_t406_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44930_GenericMethod;
// T System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44930_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7918_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t406_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7918_IList_1_get_Item_m44930_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44930_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType List_1_t406_0_0_0;
static ParameterInfo IList_1_t7918_IList_1_set_Item_m44931_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &List_1_t406_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44931_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44931_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7918_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7918_IList_1_set_Item_m44931_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44931_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7918_MethodInfos[] =
{
	&IList_1_IndexOf_m44932_MethodInfo,
	&IList_1_Insert_m44933_MethodInfo,
	&IList_1_RemoveAt_m44934_MethodInfo,
	&IList_1_get_Item_m44930_MethodInfo,
	&IList_1_set_Item_m44931_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7918_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7917_il2cpp_TypeInfo,
	&IEnumerable_1_t7919_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7918_0_0_0;
extern Il2CppType IList_1_t7918_1_0_0;
struct IList_1_t7918;
extern Il2CppGenericClass IList_1_t7918_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7918_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7918_MethodInfos/* methods */
	, IList_1_t7918_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7918_il2cpp_TypeInfo/* element_class */
	, IList_1_t7918_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7918_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7918_0_0_0/* byval_arg */
	, &IList_1_t7918_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7918_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo Enumerator_get_Current_m20063_MethodInfo;


// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Dispose()
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>::MoveNext()
// T System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Current()
// Metadata Definition System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern Il2CppType Stack_1_t3618_0_0_1;
FieldInfo Enumerator_t3621____parent_0_FieldInfo = 
{
	"parent"/* name */
	, &Stack_1_t3618_0_0_1/* type */
	, &Enumerator_t3621_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t3621, ___parent_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo Enumerator_t3621____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &Enumerator_t3621_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t3621, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo Enumerator_t3621_____version_2_FieldInfo = 
{
	"_version"/* name */
	, &Int32_t93_0_0_1/* type */
	, &Enumerator_t3621_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t3621, ____version_2) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Enumerator_t3621_FieldInfos[] =
{
	&Enumerator_t3621____parent_0_FieldInfo,
	&Enumerator_t3621____idx_1_FieldInfo,
	&Enumerator_t3621_____version_2_FieldInfo,
	NULL
};
extern MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m20060_MethodInfo;
static PropertyInfo Enumerator_t3621____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&Enumerator_t3621_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &Enumerator_System_Collections_IEnumerator_get_Current_m20060_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo Enumerator_t3621____Current_PropertyInfo = 
{
	&Enumerator_t3621_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &Enumerator_get_Current_m20063_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Enumerator_t3621_PropertyInfos[] =
{
	&Enumerator_t3621____System_Collections_IEnumerator_Current_PropertyInfo,
	&Enumerator_t3621____Current_PropertyInfo,
	NULL
};
extern Il2CppType Stack_1_t3618_0_0_0;
static ParameterInfo Enumerator_t3621_Enumerator__ctor_m20059_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &Stack_1_t3618_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator__ctor_m20059_GenericMethod;
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor(System.Collections.Generic.Stack`1<T>)
MethodInfo Enumerator__ctor_m20059_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Enumerator__ctor_m15728_gshared/* method */
	, &Enumerator_t3621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, Enumerator_t3621_Enumerator__ctor_m20059_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator__ctor_m20059_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_System_Collections_IEnumerator_get_Current_m20060_GenericMethod;
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.IEnumerator.get_Current()
MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m20060_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15729_gshared/* method */
	, &Enumerator_t3621_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_System_Collections_IEnumerator_get_Current_m20060_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_Dispose_m20061_GenericMethod;
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Dispose()
MethodInfo Enumerator_Dispose_m20061_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&Enumerator_Dispose_m15730_gshared/* method */
	, &Enumerator_t3621_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_Dispose_m20061_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_MoveNext_m20062_GenericMethod;
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>::MoveNext()
MethodInfo Enumerator_MoveNext_m20062_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&Enumerator_MoveNext_m15731_gshared/* method */
	, &Enumerator_t3621_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_MoveNext_m20062_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t406_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_get_Current_m20063_GenericMethod;
// T System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Current()
MethodInfo Enumerator_get_Current_m20063_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&Enumerator_get_Current_m15732_gshared/* method */
	, &Enumerator_t3621_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t406_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_get_Current_m20063_GenericMethod/* genericMethod */

};
static MethodInfo* Enumerator_t3621_MethodInfos[] =
{
	&Enumerator__ctor_m20059_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m20060_MethodInfo,
	&Enumerator_Dispose_m20061_MethodInfo,
	&Enumerator_MoveNext_m20062_MethodInfo,
	&Enumerator_get_Current_m20063_MethodInfo,
	NULL
};
extern MethodInfo Enumerator_MoveNext_m20062_MethodInfo;
extern MethodInfo Enumerator_Dispose_m20061_MethodInfo;
static MethodInfo* Enumerator_t3621_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m20060_MethodInfo,
	&Enumerator_MoveNext_m20062_MethodInfo,
	&Enumerator_Dispose_m20061_MethodInfo,
	&Enumerator_get_Current_m20063_MethodInfo,
};
static TypeInfo* Enumerator_t3621_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t3620_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Enumerator_t3621_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t3620_il2cpp_TypeInfo, 7},
};
extern TypeInfo List_1_t406_il2cpp_TypeInfo;
static Il2CppRGCTXData Enumerator_t3621_RGCTXData[2] = 
{
	&Enumerator_get_Current_m20063_MethodInfo/* Method Usage */,
	&List_1_t406_il2cpp_TypeInfo/* Class Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Enumerator_t3621_0_0_0;
extern Il2CppType Enumerator_t3621_1_0_0;
extern Il2CppGenericClass Enumerator_t3621_GenericClass;
extern TypeInfo Stack_1_t1294_il2cpp_TypeInfo;
TypeInfo Enumerator_t3621_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, Enumerator_t3621_MethodInfos/* methods */
	, Enumerator_t3621_PropertyInfos/* properties */
	, Enumerator_t3621_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Stack_1_t1294_il2cpp_TypeInfo/* nested_in */
	, &Enumerator_t3621_il2cpp_TypeInfo/* element_class */
	, Enumerator_t3621_InterfacesTypeInfos/* implemented_interfaces */
	, Enumerator_t3621_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Enumerator_t3621_il2cpp_TypeInfo/* cast_class */
	, &Enumerator_t3621_0_0_0/* byval_arg */
	, &Enumerator_t3621_1_0_0/* this_arg */
	, Enumerator_t3621_InterfacesOffsets/* interface_offsets */
	, &Enumerator_t3621_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, Enumerator_t3621_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t3621)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t404_UnityAction_1__ctor_m2559_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m2559_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m2559_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t404_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t404_UnityAction_1__ctor_m2559_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m2559_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t406_0_0_0;
static ParameterInfo UnityAction_1_t404_UnityAction_1_Invoke_m20064_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t406_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m20064_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m20064_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t404_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t404_UnityAction_1_Invoke_m20064_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m20064_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t406_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t404_UnityAction_1_BeginInvoke_m20065_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t406_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m20065_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m20065_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t404_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t404_UnityAction_1_BeginInvoke_m20065_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m20065_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t404_UnityAction_1_EndInvoke_m20066_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m20066_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m20066_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t404_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t404_UnityAction_1_EndInvoke_m20066_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m20066_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t404_MethodInfos[] =
{
	&UnityAction_1__ctor_m2559_MethodInfo,
	&UnityAction_1_Invoke_m20064_MethodInfo,
	&UnityAction_1_BeginInvoke_m20065_MethodInfo,
	&UnityAction_1_EndInvoke_m20066_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m20065_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m20066_MethodInfo;
static MethodInfo* UnityAction_1_t404_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m20064_MethodInfo,
	&UnityAction_1_BeginInvoke_m20065_MethodInfo,
	&UnityAction_1_EndInvoke_m20066_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t404_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t404_1_0_0;
struct UnityAction_1_t404;
extern Il2CppGenericClass UnityAction_1_t404_GenericClass;
TypeInfo UnityAction_1_t404_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t404_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t404_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t404_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t404_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t404_0_0_0/* byval_arg */
	, &UnityAction_1_t404_1_0_0/* this_arg */
	, UnityAction_1_t404_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t404_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t404)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_2.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ObjectPool_1_t407_il2cpp_TypeInfo;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_2MethodDeclarations.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "System_System_Collections_Generic_Stack_1_gen_4.h"
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_5.h"
// System.Collections.Generic.List`1<UnityEngine.Component>
#include "mscorlib_System_Collections_Generic_List_1_gen_9.h"
extern TypeInfo Stack_1_t3623_il2cpp_TypeInfo;
extern TypeInfo List_1_t396_il2cpp_TypeInfo;
extern TypeInfo UnityAction_1_t408_il2cpp_TypeInfo;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "System_System_Collections_Generic_Stack_1_gen_4MethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_5MethodDeclarations.h"
extern MethodInfo ObjectPool_1_get_countAll_m20067_MethodInfo;
extern MethodInfo ObjectPool_1_get_countInactive_m20070_MethodInfo;
extern MethodInfo Stack_1_get_Count_m20080_MethodInfo;
extern MethodInfo Stack_1__ctor_m20071_MethodInfo;
extern MethodInfo Activator_CreateInstance_TisList_1_t396_m34891_MethodInfo;
extern MethodInfo ObjectPool_1_set_countAll_m20068_MethodInfo;
extern MethodInfo Stack_1_Pop_m20078_MethodInfo;
extern MethodInfo UnityAction_1_Invoke_m20092_MethodInfo;
extern MethodInfo Stack_1_Peek_m20077_MethodInfo;
extern MethodInfo Stack_1_Push_m20079_MethodInfo;
struct Activator_t2179;
// Declaration !!0 System.Activator::CreateInstance<System.Collections.Generic.List`1<UnityEngine.Component>>()
// !!0 System.Activator::CreateInstance<System.Collections.Generic.List`1<UnityEngine.Component>>()
#define Activator_CreateInstance_TisList_1_t396_m34891(__this/* static, unused */, method) (List_1_t396 *)Activator_CreateInstance_TisObject_t_m33042_gshared((Object_t *)__this/* static, unused */, method)


// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countAll()
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::set_countAll(System.Int32)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countActive()
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countInactive()
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Get()
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Release(T)
// Metadata Definition UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern Il2CppType Stack_1_t3623_0_0_33;
FieldInfo ObjectPool_1_t407____m_Stack_0_FieldInfo = 
{
	"m_Stack"/* name */
	, &Stack_1_t3623_0_0_33/* type */
	, &ObjectPool_1_t407_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectPool_1_t407, ___m_Stack_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UnityAction_1_t408_0_0_33;
FieldInfo ObjectPool_1_t407____m_ActionOnGet_1_FieldInfo = 
{
	"m_ActionOnGet"/* name */
	, &UnityAction_1_t408_0_0_33/* type */
	, &ObjectPool_1_t407_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectPool_1_t407, ___m_ActionOnGet_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UnityAction_1_t408_0_0_33;
FieldInfo ObjectPool_1_t407____m_ActionOnRelease_2_FieldInfo = 
{
	"m_ActionOnRelease"/* name */
	, &UnityAction_1_t408_0_0_33/* type */
	, &ObjectPool_1_t407_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectPool_1_t407, ___m_ActionOnRelease_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
extern CustomAttributesCache ObjectPool_1_t410__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
FieldInfo ObjectPool_1_t407____U3CcountAllU3Ek__BackingField_3_FieldInfo = 
{
	"<countAll>k__BackingField"/* name */
	, &Int32_t93_0_0_1/* type */
	, &ObjectPool_1_t407_il2cpp_TypeInfo/* parent */
	, offsetof(ObjectPool_1_t407, ___U3CcountAllU3Ek__BackingField_3)/* data */
	, &ObjectPool_1_t410__CustomAttributeCache_U3CcountAllU3Ek__BackingField/* custom_attributes_cache */

};
static FieldInfo* ObjectPool_1_t407_FieldInfos[] =
{
	&ObjectPool_1_t407____m_Stack_0_FieldInfo,
	&ObjectPool_1_t407____m_ActionOnGet_1_FieldInfo,
	&ObjectPool_1_t407____m_ActionOnRelease_2_FieldInfo,
	&ObjectPool_1_t407____U3CcountAllU3Ek__BackingField_3_FieldInfo,
	NULL
};
static PropertyInfo ObjectPool_1_t407____countAll_PropertyInfo = 
{
	&ObjectPool_1_t407_il2cpp_TypeInfo/* parent */
	, "countAll"/* name */
	, &ObjectPool_1_get_countAll_m20067_MethodInfo/* get */
	, &ObjectPool_1_set_countAll_m20068_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ObjectPool_1_get_countActive_m20069_MethodInfo;
static PropertyInfo ObjectPool_1_t407____countActive_PropertyInfo = 
{
	&ObjectPool_1_t407_il2cpp_TypeInfo/* parent */
	, "countActive"/* name */
	, &ObjectPool_1_get_countActive_m20069_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo ObjectPool_1_t407____countInactive_PropertyInfo = 
{
	&ObjectPool_1_t407_il2cpp_TypeInfo/* parent */
	, "countInactive"/* name */
	, &ObjectPool_1_get_countInactive_m20070_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ObjectPool_1_t407_PropertyInfos[] =
{
	&ObjectPool_1_t407____countAll_PropertyInfo,
	&ObjectPool_1_t407____countActive_PropertyInfo,
	&ObjectPool_1_t407____countInactive_PropertyInfo,
	NULL
};
extern Il2CppType UnityAction_1_t408_0_0_0;
extern Il2CppType UnityAction_1_t408_0_0_0;
extern Il2CppType UnityAction_1_t408_0_0_0;
static ParameterInfo ObjectPool_1_t407_ObjectPool_1__ctor_m2565_ParameterInfos[] = 
{
	{"actionOnGet", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t408_0_0_0},
	{"actionOnRelease", 1, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t408_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ObjectPool_1__ctor_m2565_GenericMethod;
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
MethodInfo ObjectPool_1__ctor_m2565_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&ObjectPool_1__ctor_m15704_gshared/* method */
	, &ObjectPool_1_t407_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, ObjectPool_1_t407_ObjectPool_1__ctor_m2565_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ObjectPool_1__ctor_m2565_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache ObjectPool_1_t410__CustomAttributeCache_ObjectPool_1_get_countAll_m2570;
extern Il2CppGenericMethod ObjectPool_1_get_countAll_m20067_GenericMethod;
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countAll()
MethodInfo ObjectPool_1_get_countAll_m20067_MethodInfo = 
{
	"get_countAll"/* name */
	, (methodPointerType)&ObjectPool_1_get_countAll_m15706_gshared/* method */
	, &ObjectPool_1_t407_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &ObjectPool_1_t410__CustomAttributeCache_ObjectPool_1_get_countAll_m2570/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ObjectPool_1_get_countAll_m20067_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ObjectPool_1_t407_ObjectPool_1_set_countAll_m20068_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache ObjectPool_1_t410__CustomAttributeCache_ObjectPool_1_set_countAll_m2571;
extern Il2CppGenericMethod ObjectPool_1_set_countAll_m20068_GenericMethod;
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::set_countAll(System.Int32)
MethodInfo ObjectPool_1_set_countAll_m20068_MethodInfo = 
{
	"set_countAll"/* name */
	, (methodPointerType)&ObjectPool_1_set_countAll_m15708_gshared/* method */
	, &ObjectPool_1_t407_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ObjectPool_1_t407_ObjectPool_1_set_countAll_m20068_ParameterInfos/* parameters */
	, &ObjectPool_1_t410__CustomAttributeCache_ObjectPool_1_set_countAll_m2571/* custom_attributes_cache */
	, 2177/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ObjectPool_1_set_countAll_m20068_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ObjectPool_1_get_countActive_m20069_GenericMethod;
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countActive()
MethodInfo ObjectPool_1_get_countActive_m20069_MethodInfo = 
{
	"get_countActive"/* name */
	, (methodPointerType)&ObjectPool_1_get_countActive_m15710_gshared/* method */
	, &ObjectPool_1_t407_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ObjectPool_1_get_countActive_m20069_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ObjectPool_1_get_countInactive_m20070_GenericMethod;
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countInactive()
MethodInfo ObjectPool_1_get_countInactive_m20070_MethodInfo = 
{
	"get_countInactive"/* name */
	, (methodPointerType)&ObjectPool_1_get_countInactive_m15712_gshared/* method */
	, &ObjectPool_1_t407_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ObjectPool_1_get_countInactive_m20070_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t396_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ObjectPool_1_Get_m2566_GenericMethod;
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Get()
MethodInfo ObjectPool_1_Get_m2566_MethodInfo = 
{
	"Get"/* name */
	, (methodPointerType)&ObjectPool_1_Get_m15714_gshared/* method */
	, &ObjectPool_1_t407_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ObjectPool_1_Get_m2566_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t396_0_0_0;
extern Il2CppType List_1_t396_0_0_0;
static ParameterInfo ObjectPool_1_t407_ObjectPool_1_Release_m2567_ParameterInfos[] = 
{
	{"element", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t396_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ObjectPool_1_Release_m2567_GenericMethod;
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Release(T)
MethodInfo ObjectPool_1_Release_m2567_MethodInfo = 
{
	"Release"/* name */
	, (methodPointerType)&ObjectPool_1_Release_m15716_gshared/* method */
	, &ObjectPool_1_t407_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ObjectPool_1_t407_ObjectPool_1_Release_m2567_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ObjectPool_1_Release_m2567_GenericMethod/* genericMethod */

};
static MethodInfo* ObjectPool_1_t407_MethodInfos[] =
{
	&ObjectPool_1__ctor_m2565_MethodInfo,
	&ObjectPool_1_get_countAll_m20067_MethodInfo,
	&ObjectPool_1_set_countAll_m20068_MethodInfo,
	&ObjectPool_1_get_countActive_m20069_MethodInfo,
	&ObjectPool_1_get_countInactive_m20070_MethodInfo,
	&ObjectPool_1_Get_m2566_MethodInfo,
	&ObjectPool_1_Release_m2567_MethodInfo,
	NULL
};
static MethodInfo* ObjectPool_1_t407_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
extern TypeInfo Stack_1_t3623_il2cpp_TypeInfo;
extern TypeInfo List_1_t396_il2cpp_TypeInfo;
static Il2CppRGCTXData ObjectPool_1_t407_RGCTXData[12] = 
{
	&Stack_1_t3623_il2cpp_TypeInfo/* Class Usage */,
	&Stack_1__ctor_m20071_MethodInfo/* Method Usage */,
	&ObjectPool_1_get_countAll_m20067_MethodInfo/* Method Usage */,
	&ObjectPool_1_get_countInactive_m20070_MethodInfo/* Method Usage */,
	&Stack_1_get_Count_m20080_MethodInfo/* Method Usage */,
	&List_1_t396_il2cpp_TypeInfo/* Class Usage */,
	&Activator_CreateInstance_TisList_1_t396_m34891_MethodInfo/* Method Usage */,
	&ObjectPool_1_set_countAll_m20068_MethodInfo/* Method Usage */,
	&Stack_1_Pop_m20078_MethodInfo/* Method Usage */,
	&UnityAction_1_Invoke_m20092_MethodInfo/* Method Usage */,
	&Stack_1_Peek_m20077_MethodInfo/* Method Usage */,
	&Stack_1_Push_m20079_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_UI_dll_Image;
extern Il2CppType ObjectPool_1_t407_0_0_0;
extern Il2CppType ObjectPool_1_t407_1_0_0;
struct ObjectPool_1_t407;
extern Il2CppGenericClass ObjectPool_1_t407_GenericClass;
extern CustomAttributesCache ObjectPool_1_t410__CustomAttributeCache_U3CcountAllU3Ek__BackingField;
extern CustomAttributesCache ObjectPool_1_t410__CustomAttributeCache_ObjectPool_1_get_countAll_m2570;
extern CustomAttributesCache ObjectPool_1_t410__CustomAttributeCache_ObjectPool_1_set_countAll_m2571;
TypeInfo ObjectPool_1_t407_il2cpp_TypeInfo = 
{
	&g_UnityEngine_UI_dll_Image/* image */
	, NULL/* gc_desc */
	, "ObjectPool`1"/* name */
	, "UnityEngine.UI"/* namespaze */
	, ObjectPool_1_t407_MethodInfos/* methods */
	, ObjectPool_1_t407_PropertyInfos/* properties */
	, ObjectPool_1_t407_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ObjectPool_1_t407_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, ObjectPool_1_t407_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ObjectPool_1_t407_il2cpp_TypeInfo/* cast_class */
	, &ObjectPool_1_t407_0_0_0/* byval_arg */
	, &ObjectPool_1_t407_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ObjectPool_1_t407_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, ObjectPool_1_t407_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (ObjectPool_1_t407)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_3.h"
extern TypeInfo Enumerator_t3626_il2cpp_TypeInfo;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_3MethodDeclarations.h"
extern MethodInfo Stack_1_GetEnumerator_m20081_MethodInfo;
extern MethodInfo Array_Resize_TisList_1_t396_m34890_MethodInfo;
extern MethodInfo Enumerator__ctor_m20087_MethodInfo;
struct Array_t;
// Declaration System.Void System.Array::Resize<System.Collections.Generic.List`1<UnityEngine.Component>>(!!0[]&,System.Int32)
// System.Void System.Array::Resize<System.Collections.Generic.List`1<UnityEngine.Component>>(!!0[]&,System.Int32)
#define Array_Resize_TisList_1_t396_m34890(__this/* static, unused */, p0, p1, method) (void)Array_Resize_TisObject_t_m32177_gshared((Object_t *)__this/* static, unused */, (ObjectU5BU5D_t115**)p0, (int32_t)p1, method)


// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor()
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.get_IsSynchronized()
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.get_SyncRoot()
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.IEnumerable.GetEnumerator()
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Peek()
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Pop()
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Push(T)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Count()
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::GetEnumerator()
 Enumerator_t3626  Stack_1_GetEnumerator_m20081 (Stack_1_t3623 * __this, MethodInfo* method){
	{
		Enumerator_t3626  L_0 = {0};
		Enumerator__ctor_m20087(&L_0, __this, /*hidden argument*/&Enumerator__ctor_m20087_MethodInfo);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern Il2CppType Int32_t93_0_0_32849;
FieldInfo Stack_1_t3623____INITIAL_SIZE_0_FieldInfo = 
{
	"INITIAL_SIZE"/* name */
	, &Int32_t93_0_0_32849/* type */
	, &Stack_1_t3623_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType List_1U5BU5D_t3624_0_0_1;
FieldInfo Stack_1_t3623_____array_1_FieldInfo = 
{
	"_array"/* name */
	, &List_1U5BU5D_t3624_0_0_1/* type */
	, &Stack_1_t3623_il2cpp_TypeInfo/* parent */
	, offsetof(Stack_1_t3623, ____array_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo Stack_1_t3623_____size_2_FieldInfo = 
{
	"_size"/* name */
	, &Int32_t93_0_0_1/* type */
	, &Stack_1_t3623_il2cpp_TypeInfo/* parent */
	, offsetof(Stack_1_t3623, ____size_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo Stack_1_t3623_____version_3_FieldInfo = 
{
	"_version"/* name */
	, &Int32_t93_0_0_1/* type */
	, &Stack_1_t3623_il2cpp_TypeInfo/* parent */
	, offsetof(Stack_1_t3623, ____version_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Stack_1_t3623_FieldInfos[] =
{
	&Stack_1_t3623____INITIAL_SIZE_0_FieldInfo,
	&Stack_1_t3623_____array_1_FieldInfo,
	&Stack_1_t3623_____size_2_FieldInfo,
	&Stack_1_t3623_____version_3_FieldInfo,
	NULL
};
static const int32_t Stack_1_t3623____INITIAL_SIZE_0_DefaultValueData = 16;
static Il2CppFieldDefaultValueEntry Stack_1_t3623____INITIAL_SIZE_0_DefaultValue = 
{
	&Stack_1_t3623____INITIAL_SIZE_0_FieldInfo/* field */
	, { (char*)&Stack_1_t3623____INITIAL_SIZE_0_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* Stack_1_t3623_FieldDefaultValues[] = 
{
	&Stack_1_t3623____INITIAL_SIZE_0_DefaultValue,
	NULL
};
extern MethodInfo Stack_1_System_Collections_ICollection_get_IsSynchronized_m20072_MethodInfo;
static PropertyInfo Stack_1_t3623____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&Stack_1_t3623_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.IsSynchronized"/* name */
	, &Stack_1_System_Collections_ICollection_get_IsSynchronized_m20072_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo Stack_1_System_Collections_ICollection_get_SyncRoot_m20073_MethodInfo;
static PropertyInfo Stack_1_t3623____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&Stack_1_t3623_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.SyncRoot"/* name */
	, &Stack_1_System_Collections_ICollection_get_SyncRoot_m20073_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo Stack_1_t3623____Count_PropertyInfo = 
{
	&Stack_1_t3623_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &Stack_1_get_Count_m20080_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Stack_1_t3623_PropertyInfos[] =
{
	&Stack_1_t3623____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&Stack_1_t3623____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&Stack_1_t3623____Count_PropertyInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1__ctor_m20071_GenericMethod;
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor()
MethodInfo Stack_1__ctor_m20071_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Stack_1__ctor_m15717_gshared/* method */
	, &Stack_1_t3623_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1__ctor_m20071_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_System_Collections_ICollection_get_IsSynchronized_m20072_GenericMethod;
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.get_IsSynchronized()
MethodInfo Stack_1_System_Collections_ICollection_get_IsSynchronized_m20072_MethodInfo = 
{
	"System.Collections.ICollection.get_IsSynchronized"/* name */
	, (methodPointerType)&Stack_1_System_Collections_ICollection_get_IsSynchronized_m15718_gshared/* method */
	, &Stack_1_t3623_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_System_Collections_ICollection_get_IsSynchronized_m20072_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_System_Collections_ICollection_get_SyncRoot_m20073_GenericMethod;
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.get_SyncRoot()
MethodInfo Stack_1_System_Collections_ICollection_get_SyncRoot_m20073_MethodInfo = 
{
	"System.Collections.ICollection.get_SyncRoot"/* name */
	, (methodPointerType)&Stack_1_System_Collections_ICollection_get_SyncRoot_m15719_gshared/* method */
	, &Stack_1_t3623_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_System_Collections_ICollection_get_SyncRoot_m20073_GenericMethod/* genericMethod */

};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo Stack_1_t3623_Stack_1_System_Collections_ICollection_CopyTo_m20074_ParameterInfos[] = 
{
	{"dest", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
	{"idx", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_System_Collections_ICollection_CopyTo_m20074_GenericMethod;
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
MethodInfo Stack_1_System_Collections_ICollection_CopyTo_m20074_MethodInfo = 
{
	"System.Collections.ICollection.CopyTo"/* name */
	, (methodPointerType)&Stack_1_System_Collections_ICollection_CopyTo_m15720_gshared/* method */
	, &Stack_1_t3623_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, Stack_1_t3623_Stack_1_System_Collections_ICollection_CopyTo_m20074_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_System_Collections_ICollection_CopyTo_m20074_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_1_t3625_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20075_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
MethodInfo Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20075_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator"/* name */
	, (methodPointerType)&Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15721_gshared/* method */
	, &Stack_1_t3623_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t3625_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20075_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_t266_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_System_Collections_IEnumerable_GetEnumerator_m20076_GenericMethod;
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.IEnumerable.GetEnumerator()
MethodInfo Stack_1_System_Collections_IEnumerable_GetEnumerator_m20076_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, (methodPointerType)&Stack_1_System_Collections_IEnumerable_GetEnumerator_m15722_gshared/* method */
	, &Stack_1_t3623_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_System_Collections_IEnumerable_GetEnumerator_m20076_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t396_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_Peek_m20077_GenericMethod;
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Peek()
MethodInfo Stack_1_Peek_m20077_MethodInfo = 
{
	"Peek"/* name */
	, (methodPointerType)&Stack_1_Peek_m15723_gshared/* method */
	, &Stack_1_t3623_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_Peek_m20077_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t396_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_Pop_m20078_GenericMethod;
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Pop()
MethodInfo Stack_1_Pop_m20078_MethodInfo = 
{
	"Pop"/* name */
	, (methodPointerType)&Stack_1_Pop_m15724_gshared/* method */
	, &Stack_1_t3623_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_Pop_m20078_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t396_0_0_0;
static ParameterInfo Stack_1_t3623_Stack_1_Push_m20079_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t396_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_Push_m20079_GenericMethod;
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Push(T)
MethodInfo Stack_1_Push_m20079_MethodInfo = 
{
	"Push"/* name */
	, (methodPointerType)&Stack_1_Push_m15725_gshared/* method */
	, &Stack_1_t3623_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, Stack_1_t3623_Stack_1_Push_m20079_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_Push_m20079_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_get_Count_m20080_GenericMethod;
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Count()
MethodInfo Stack_1_get_Count_m20080_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&Stack_1_get_Count_m15726_gshared/* method */
	, &Stack_1_t3623_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_get_Count_m20080_GenericMethod/* genericMethod */

};
extern Il2CppType Enumerator_t3626_0_0_0;
extern void* RuntimeInvoker_Enumerator_t3626 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Stack_1_GetEnumerator_m20081_GenericMethod;
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::GetEnumerator()
MethodInfo Stack_1_GetEnumerator_m20081_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&Stack_1_GetEnumerator_m20081/* method */
	, &Stack_1_t3623_il2cpp_TypeInfo/* declaring_type */
	, &Enumerator_t3626_0_0_0/* return_type */
	, RuntimeInvoker_Enumerator_t3626/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Stack_1_GetEnumerator_m20081_GenericMethod/* genericMethod */

};
static MethodInfo* Stack_1_t3623_MethodInfos[] =
{
	&Stack_1__ctor_m20071_MethodInfo,
	&Stack_1_System_Collections_ICollection_get_IsSynchronized_m20072_MethodInfo,
	&Stack_1_System_Collections_ICollection_get_SyncRoot_m20073_MethodInfo,
	&Stack_1_System_Collections_ICollection_CopyTo_m20074_MethodInfo,
	&Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20075_MethodInfo,
	&Stack_1_System_Collections_IEnumerable_GetEnumerator_m20076_MethodInfo,
	&Stack_1_Peek_m20077_MethodInfo,
	&Stack_1_Pop_m20078_MethodInfo,
	&Stack_1_Push_m20079_MethodInfo,
	&Stack_1_get_Count_m20080_MethodInfo,
	&Stack_1_GetEnumerator_m20081_MethodInfo,
	NULL
};
extern MethodInfo Stack_1_System_Collections_ICollection_CopyTo_m20074_MethodInfo;
extern MethodInfo Stack_1_System_Collections_IEnumerable_GetEnumerator_m20076_MethodInfo;
extern MethodInfo Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20075_MethodInfo;
static MethodInfo* Stack_1_t3623_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&Stack_1_get_Count_m20080_MethodInfo,
	&Stack_1_System_Collections_ICollection_get_IsSynchronized_m20072_MethodInfo,
	&Stack_1_System_Collections_ICollection_get_SyncRoot_m20073_MethodInfo,
	&Stack_1_System_Collections_ICollection_CopyTo_m20074_MethodInfo,
	&Stack_1_System_Collections_IEnumerable_GetEnumerator_m20076_MethodInfo,
	&Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m20075_MethodInfo,
};
extern TypeInfo IEnumerable_1_t7922_il2cpp_TypeInfo;
static TypeInfo* Stack_1_t3623_InterfacesTypeInfos[] = 
{
	&ICollection_t1206_il2cpp_TypeInfo,
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7922_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Stack_1_t3623_InterfacesOffsets[] = 
{
	{ &ICollection_t1206_il2cpp_TypeInfo, 4},
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 8},
	{ &IEnumerable_1_t7922_il2cpp_TypeInfo, 9},
};
extern TypeInfo Enumerator_t3626_il2cpp_TypeInfo;
static Il2CppRGCTXData Stack_1_t3623_RGCTXData[4] = 
{
	&Stack_1_GetEnumerator_m20081_MethodInfo/* Method Usage */,
	&Enumerator_t3626_il2cpp_TypeInfo/* Class Usage */,
	&Array_Resize_TisList_1_t396_m34890_MethodInfo/* Method Usage */,
	&Enumerator__ctor_m20087_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Stack_1_t3623_0_0_0;
extern Il2CppType Stack_1_t3623_1_0_0;
struct Stack_1_t3623;
extern Il2CppGenericClass Stack_1_t3623_GenericClass;
extern CustomAttributesCache Stack_1_t1294__CustomAttributeCache;
TypeInfo Stack_1_t3623_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Stack`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, Stack_1_t3623_MethodInfos/* methods */
	, Stack_1_t3623_PropertyInfos/* properties */
	, Stack_1_t3623_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Stack_1_t3623_il2cpp_TypeInfo/* element_class */
	, Stack_1_t3623_InterfacesTypeInfos/* implemented_interfaces */
	, Stack_1_t3623_VTable/* vtable */
	, &Stack_1_t1294__CustomAttributeCache/* custom_attributes_cache */
	, &Stack_1_t3623_il2cpp_TypeInfo/* cast_class */
	, &Stack_1_t3623_0_0_0/* byval_arg */
	, &Stack_1_t3623_1_0_0/* this_arg */
	, Stack_1_t3623_InterfacesOffsets/* interface_offsets */
	, &Stack_1_t3623_GenericClass/* generic_class */
	, NULL/* generic_container */
	, Stack_1_t3623_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, Stack_1_t3623_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Stack_1_t3623)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 11/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 10/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.List`1<UnityEngine.Component>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern Il2CppType IEnumerator_1_t3625_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44935_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.List`1<UnityEngine.Component>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44935_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7922_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t3625_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44935_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7922_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44935_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7922_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7922_0_0_0;
extern Il2CppType IEnumerable_1_t7922_1_0_0;
struct IEnumerable_1_t7922;
extern Il2CppGenericClass IEnumerable_1_t7922_GenericClass;
TypeInfo IEnumerable_1_t7922_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7922_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7922_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7922_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7922_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7922_0_0_0/* byval_arg */
	, &IEnumerable_1_t7922_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7922_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t3625_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern MethodInfo IEnumerator_1_get_Current_m44936_MethodInfo;
static PropertyInfo IEnumerator_1_t3625____Current_PropertyInfo = 
{
	&IEnumerator_1_t3625_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44936_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t3625_PropertyInfos[] =
{
	&IEnumerator_1_t3625____Current_PropertyInfo,
	NULL
};
extern Il2CppType List_1_t396_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44936_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44936_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t3625_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44936_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t3625_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44936_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t3625_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t3625_0_0_0;
extern Il2CppType IEnumerator_1_t3625_1_0_0;
struct IEnumerator_1_t3625;
extern Il2CppGenericClass IEnumerator_1_t3625_GenericClass;
TypeInfo IEnumerator_1_t3625_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t3625_MethodInfos/* methods */
	, IEnumerator_1_t3625_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t3625_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t3625_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t3625_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t3625_0_0_0/* byval_arg */
	, &IEnumerator_1_t3625_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t3625_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_265.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3627_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_265MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m20086_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisList_1_t396_m34879_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.List`1<UnityEngine.Component>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.List`1<UnityEngine.Component>>(System.Int32)
#define Array_InternalArray__get_Item_TisList_1_t396_m34879(__this, p0, method) (List_1_t396 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3627____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3627_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3627, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3627____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3627_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3627, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3627_FieldInfos[] =
{
	&InternalEnumerator_1_t3627____array_0_FieldInfo,
	&InternalEnumerator_1_t3627____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20083_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3627____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3627_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20083_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3627____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3627_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m20086_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3627_PropertyInfos[] =
{
	&InternalEnumerator_1_t3627____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3627____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3627_InternalEnumerator_1__ctor_m20082_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m20082_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m20082_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3627_InternalEnumerator_1__ctor_m20082_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m20082_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20083_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20083_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3627_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20083_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m20084_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m20084_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m20084_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m20085_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m20085_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3627_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m20085_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t396_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m20086_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m20086_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3627_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m20086_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3627_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m20082_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20083_MethodInfo,
	&InternalEnumerator_1_Dispose_m20084_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20085_MethodInfo,
	&InternalEnumerator_1_get_Current_m20086_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m20085_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m20084_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3627_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20083_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20085_MethodInfo,
	&InternalEnumerator_1_Dispose_m20084_MethodInfo,
	&InternalEnumerator_1_get_Current_m20086_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3627_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t3625_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3627_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t3625_il2cpp_TypeInfo, 7},
};
extern TypeInfo List_1_t396_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3627_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m20086_MethodInfo/* Method Usage */,
	&List_1_t396_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisList_1_t396_m34879_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3627_0_0_0;
extern Il2CppType InternalEnumerator_1_t3627_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3627_GenericClass;
TypeInfo InternalEnumerator_1_t3627_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3627_MethodInfos/* methods */
	, InternalEnumerator_1_t3627_PropertyInfos/* properties */
	, InternalEnumerator_1_t3627_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3627_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3627_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3627_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3627_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3627_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3627_1_0_0/* this_arg */
	, InternalEnumerator_1_t3627_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3627_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3627_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3627)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7920_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern MethodInfo ICollection_1_get_Count_m44937_MethodInfo;
static PropertyInfo ICollection_1_t7920____Count_PropertyInfo = 
{
	&ICollection_1_t7920_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44937_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44938_MethodInfo;
static PropertyInfo ICollection_1_t7920____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7920_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44938_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7920_PropertyInfos[] =
{
	&ICollection_1_t7920____Count_PropertyInfo,
	&ICollection_1_t7920____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44937_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Count()
MethodInfo ICollection_1_get_Count_m44937_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7920_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44937_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44938_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44938_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7920_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44938_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t396_0_0_0;
static ParameterInfo ICollection_1_t7920_ICollection_1_Add_m44939_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t396_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44939_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Add(T)
MethodInfo ICollection_1_Add_m44939_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7920_ICollection_1_Add_m44939_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44939_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44940_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Clear()
MethodInfo ICollection_1_Clear_m44940_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44940_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t396_0_0_0;
static ParameterInfo ICollection_1_t7920_ICollection_1_Contains_m44941_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t396_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44941_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Contains(T)
MethodInfo ICollection_1_Contains_m44941_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7920_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7920_ICollection_1_Contains_m44941_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44941_GenericMethod/* genericMethod */

};
extern Il2CppType List_1U5BU5D_t3624_0_0_0;
extern Il2CppType List_1U5BU5D_t3624_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7920_ICollection_1_CopyTo_m44942_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &List_1U5BU5D_t3624_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44942_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44942_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7920_ICollection_1_CopyTo_m44942_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44942_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t396_0_0_0;
static ParameterInfo ICollection_1_t7920_ICollection_1_Remove_m44943_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t396_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44943_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Remove(T)
MethodInfo ICollection_1_Remove_m44943_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7920_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7920_ICollection_1_Remove_m44943_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44943_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7920_MethodInfos[] =
{
	&ICollection_1_get_Count_m44937_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44938_MethodInfo,
	&ICollection_1_Add_m44939_MethodInfo,
	&ICollection_1_Clear_m44940_MethodInfo,
	&ICollection_1_Contains_m44941_MethodInfo,
	&ICollection_1_CopyTo_m44942_MethodInfo,
	&ICollection_1_Remove_m44943_MethodInfo,
	NULL
};
static TypeInfo* ICollection_1_t7920_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7922_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7920_0_0_0;
extern Il2CppType ICollection_1_t7920_1_0_0;
struct ICollection_1_t7920;
extern Il2CppGenericClass ICollection_1_t7920_GenericClass;
TypeInfo ICollection_1_t7920_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7920_MethodInfos/* methods */
	, ICollection_1_t7920_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7920_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7920_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7920_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7920_0_0_0/* byval_arg */
	, &ICollection_1_t7920_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7920_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7921_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Component>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Component>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Component>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern MethodInfo IList_1_get_Item_m44944_MethodInfo;
extern MethodInfo IList_1_set_Item_m44945_MethodInfo;
static PropertyInfo IList_1_t7921____Item_PropertyInfo = 
{
	&IList_1_t7921_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44944_MethodInfo/* get */
	, &IList_1_set_Item_m44945_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7921_PropertyInfos[] =
{
	&IList_1_t7921____Item_PropertyInfo,
	NULL
};
extern Il2CppType List_1_t396_0_0_0;
static ParameterInfo IList_1_t7921_IList_1_IndexOf_m44946_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t396_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44946_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Component>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44946_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7921_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7921_IList_1_IndexOf_m44946_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44946_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType List_1_t396_0_0_0;
static ParameterInfo IList_1_t7921_IList_1_Insert_m44947_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &List_1_t396_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44947_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44947_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7921_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7921_IList_1_Insert_m44947_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44947_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7921_IList_1_RemoveAt_m44948_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44948_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Component>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44948_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7921_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7921_IList_1_RemoveAt_m44948_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44948_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7921_IList_1_get_Item_m44944_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType List_1_t396_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44944_GenericMethod;
// T System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44944_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7921_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7921_IList_1_get_Item_m44944_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44944_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType List_1_t396_0_0_0;
static ParameterInfo IList_1_t7921_IList_1_set_Item_m44945_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &List_1_t396_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44945_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Collections.Generic.List`1<UnityEngine.Component>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44945_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7921_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7921_IList_1_set_Item_m44945_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44945_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7921_MethodInfos[] =
{
	&IList_1_IndexOf_m44946_MethodInfo,
	&IList_1_Insert_m44947_MethodInfo,
	&IList_1_RemoveAt_m44948_MethodInfo,
	&IList_1_get_Item_m44944_MethodInfo,
	&IList_1_set_Item_m44945_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7921_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7920_il2cpp_TypeInfo,
	&IEnumerable_1_t7922_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7921_0_0_0;
extern Il2CppType IList_1_t7921_1_0_0;
struct IList_1_t7921;
extern Il2CppGenericClass IList_1_t7921_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7921_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7921_MethodInfos/* methods */
	, IList_1_t7921_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7921_il2cpp_TypeInfo/* element_class */
	, IList_1_t7921_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7921_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7921_0_0_0/* byval_arg */
	, &IList_1_t7921_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7921_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo Enumerator_get_Current_m20091_MethodInfo;


// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::Dispose()
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::MoveNext()
// T System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Current()
// Metadata Definition System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>
extern Il2CppType Stack_1_t3623_0_0_1;
FieldInfo Enumerator_t3626____parent_0_FieldInfo = 
{
	"parent"/* name */
	, &Stack_1_t3623_0_0_1/* type */
	, &Enumerator_t3626_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t3626, ___parent_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo Enumerator_t3626____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &Enumerator_t3626_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t3626, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo Enumerator_t3626_____version_2_FieldInfo = 
{
	"_version"/* name */
	, &Int32_t93_0_0_1/* type */
	, &Enumerator_t3626_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t3626, ____version_2) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Enumerator_t3626_FieldInfos[] =
{
	&Enumerator_t3626____parent_0_FieldInfo,
	&Enumerator_t3626____idx_1_FieldInfo,
	&Enumerator_t3626_____version_2_FieldInfo,
	NULL
};
extern MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m20088_MethodInfo;
static PropertyInfo Enumerator_t3626____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&Enumerator_t3626_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &Enumerator_System_Collections_IEnumerator_get_Current_m20088_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo Enumerator_t3626____Current_PropertyInfo = 
{
	&Enumerator_t3626_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &Enumerator_get_Current_m20091_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Enumerator_t3626_PropertyInfos[] =
{
	&Enumerator_t3626____System_Collections_IEnumerator_Current_PropertyInfo,
	&Enumerator_t3626____Current_PropertyInfo,
	NULL
};
extern Il2CppType Stack_1_t3623_0_0_0;
static ParameterInfo Enumerator_t3626_Enumerator__ctor_m20087_ParameterInfos[] = 
{
	{"t", 0, 134217728, &EmptyCustomAttributesCache, &Stack_1_t3623_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator__ctor_m20087_GenericMethod;
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(System.Collections.Generic.Stack`1<T>)
MethodInfo Enumerator__ctor_m20087_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Enumerator__ctor_m15728_gshared/* method */
	, &Enumerator_t3626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, Enumerator_t3626_Enumerator__ctor_m20087_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator__ctor_m20087_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_System_Collections_IEnumerator_get_Current_m20088_GenericMethod;
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.IEnumerator.get_Current()
MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m20088_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m15729_gshared/* method */
	, &Enumerator_t3626_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_System_Collections_IEnumerator_get_Current_m20088_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_Dispose_m20089_GenericMethod;
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::Dispose()
MethodInfo Enumerator_Dispose_m20089_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&Enumerator_Dispose_m15730_gshared/* method */
	, &Enumerator_t3626_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_Dispose_m20089_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_MoveNext_m20090_GenericMethod;
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::MoveNext()
MethodInfo Enumerator_MoveNext_m20090_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&Enumerator_MoveNext_m15731_gshared/* method */
	, &Enumerator_t3626_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_MoveNext_m20090_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t396_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_get_Current_m20091_GenericMethod;
// T System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Current()
MethodInfo Enumerator_get_Current_m20091_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&Enumerator_get_Current_m15732_gshared/* method */
	, &Enumerator_t3626_il2cpp_TypeInfo/* declaring_type */
	, &List_1_t396_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_get_Current_m20091_GenericMethod/* genericMethod */

};
static MethodInfo* Enumerator_t3626_MethodInfos[] =
{
	&Enumerator__ctor_m20087_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m20088_MethodInfo,
	&Enumerator_Dispose_m20089_MethodInfo,
	&Enumerator_MoveNext_m20090_MethodInfo,
	&Enumerator_get_Current_m20091_MethodInfo,
	NULL
};
extern MethodInfo Enumerator_MoveNext_m20090_MethodInfo;
extern MethodInfo Enumerator_Dispose_m20089_MethodInfo;
static MethodInfo* Enumerator_t3626_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m20088_MethodInfo,
	&Enumerator_MoveNext_m20090_MethodInfo,
	&Enumerator_Dispose_m20089_MethodInfo,
	&Enumerator_get_Current_m20091_MethodInfo,
};
static TypeInfo* Enumerator_t3626_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t3625_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Enumerator_t3626_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t3625_il2cpp_TypeInfo, 7},
};
extern TypeInfo List_1_t396_il2cpp_TypeInfo;
static Il2CppRGCTXData Enumerator_t3626_RGCTXData[2] = 
{
	&Enumerator_get_Current_m20091_MethodInfo/* Method Usage */,
	&List_1_t396_il2cpp_TypeInfo/* Class Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Enumerator_t3626_0_0_0;
extern Il2CppType Enumerator_t3626_1_0_0;
extern Il2CppGenericClass Enumerator_t3626_GenericClass;
TypeInfo Enumerator_t3626_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, Enumerator_t3626_MethodInfos/* methods */
	, Enumerator_t3626_PropertyInfos/* properties */
	, Enumerator_t3626_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Stack_1_t1294_il2cpp_TypeInfo/* nested_in */
	, &Enumerator_t3626_il2cpp_TypeInfo/* element_class */
	, Enumerator_t3626_InterfacesTypeInfos/* implemented_interfaces */
	, Enumerator_t3626_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Enumerator_t3626_il2cpp_TypeInfo/* cast_class */
	, &Enumerator_t3626_0_0_0/* byval_arg */
	, &Enumerator_t3626_1_0_0/* this_arg */
	, Enumerator_t3626_InterfacesOffsets/* interface_offsets */
	, &Enumerator_t3626_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, Enumerator_t3626_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t3626)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t408_UnityAction_1__ctor_m2564_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m2564_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m2564_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t408_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t408_UnityAction_1__ctor_m2564_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m2564_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t396_0_0_0;
static ParameterInfo UnityAction_1_t408_UnityAction_1_Invoke_m20092_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t396_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m20092_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m20092_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t408_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t408_UnityAction_1_Invoke_m20092_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m20092_GenericMethod/* genericMethod */

};
extern Il2CppType List_1_t396_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t408_UnityAction_1_BeginInvoke_m20093_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &List_1_t396_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m20093_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m20093_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t408_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t408_UnityAction_1_BeginInvoke_m20093_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m20093_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t408_UnityAction_1_EndInvoke_m20094_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m20094_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Component>>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m20094_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t408_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t408_UnityAction_1_EndInvoke_m20094_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m20094_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t408_MethodInfos[] =
{
	&UnityAction_1__ctor_m2564_MethodInfo,
	&UnityAction_1_Invoke_m20092_MethodInfo,
	&UnityAction_1_BeginInvoke_m20093_MethodInfo,
	&UnityAction_1_EndInvoke_m20094_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m20093_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m20094_MethodInfo;
static MethodInfo* UnityAction_1_t408_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m20092_MethodInfo,
	&UnityAction_1_BeginInvoke_m20093_MethodInfo,
	&UnityAction_1_EndInvoke_m20094_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t408_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t408_1_0_0;
struct UnityAction_1_t408;
extern Il2CppGenericClass UnityAction_1_t408_GenericClass;
TypeInfo UnityAction_1_t408_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t408_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t408_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t408_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t408_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t408_0_0_0/* byval_arg */
	, &UnityAction_1_t408_1_0_0/* this_arg */
	, UnityAction_1_t408_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t408_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t408)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6238_il2cpp_TypeInfo;

// UnityEngine.UI.BaseVertexEffect
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.BaseVertexEffect>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.BaseVertexEffect>
extern MethodInfo IEnumerator_1_get_Current_m44949_MethodInfo;
static PropertyInfo IEnumerator_1_t6238____Current_PropertyInfo = 
{
	&IEnumerator_1_t6238_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44949_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6238_PropertyInfos[] =
{
	&IEnumerator_1_t6238____Current_PropertyInfo,
	NULL
};
extern Il2CppType BaseVertexEffect_t411_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44949_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.BaseVertexEffect>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44949_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6238_il2cpp_TypeInfo/* declaring_type */
	, &BaseVertexEffect_t411_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44949_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6238_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44949_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6238_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6238_0_0_0;
extern Il2CppType IEnumerator_1_t6238_1_0_0;
struct IEnumerator_1_t6238;
extern Il2CppGenericClass IEnumerator_1_t6238_GenericClass;
TypeInfo IEnumerator_1_t6238_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6238_MethodInfos/* methods */
	, IEnumerator_1_t6238_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6238_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6238_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6238_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6238_0_0_0/* byval_arg */
	, &IEnumerator_1_t6238_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6238_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.BaseVertexEffect>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_266.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3628_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.BaseVertexEffect>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_266MethodDeclarations.h"

extern TypeInfo BaseVertexEffect_t411_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m20099_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisBaseVertexEffect_t411_m34893_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.BaseVertexEffect>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.BaseVertexEffect>(System.Int32)
#define Array_InternalArray__get_Item_TisBaseVertexEffect_t411_m34893(__this, p0, method) (BaseVertexEffect_t411 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.BaseVertexEffect>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.BaseVertexEffect>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.BaseVertexEffect>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.BaseVertexEffect>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UI.BaseVertexEffect>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.BaseVertexEffect>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3628____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3628_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3628, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3628____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3628_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3628, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3628_FieldInfos[] =
{
	&InternalEnumerator_1_t3628____array_0_FieldInfo,
	&InternalEnumerator_1_t3628____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20096_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3628____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3628_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20096_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3628____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3628_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m20099_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3628_PropertyInfos[] =
{
	&InternalEnumerator_1_t3628____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3628____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3628_InternalEnumerator_1__ctor_m20095_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m20095_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.BaseVertexEffect>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m20095_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3628_InternalEnumerator_1__ctor_m20095_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m20095_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20096_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.BaseVertexEffect>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20096_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3628_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20096_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m20097_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.BaseVertexEffect>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m20097_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3628_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m20097_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m20098_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.BaseVertexEffect>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m20098_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3628_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m20098_GenericMethod/* genericMethod */

};
extern Il2CppType BaseVertexEffect_t411_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m20099_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.BaseVertexEffect>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m20099_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3628_il2cpp_TypeInfo/* declaring_type */
	, &BaseVertexEffect_t411_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m20099_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3628_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m20095_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20096_MethodInfo,
	&InternalEnumerator_1_Dispose_m20097_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20098_MethodInfo,
	&InternalEnumerator_1_get_Current_m20099_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m20098_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m20097_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3628_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20096_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20098_MethodInfo,
	&InternalEnumerator_1_Dispose_m20097_MethodInfo,
	&InternalEnumerator_1_get_Current_m20099_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3628_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6238_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3628_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6238_il2cpp_TypeInfo, 7},
};
extern TypeInfo BaseVertexEffect_t411_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3628_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m20099_MethodInfo/* Method Usage */,
	&BaseVertexEffect_t411_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisBaseVertexEffect_t411_m34893_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3628_0_0_0;
extern Il2CppType InternalEnumerator_1_t3628_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3628_GenericClass;
TypeInfo InternalEnumerator_1_t3628_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3628_MethodInfos/* methods */
	, InternalEnumerator_1_t3628_PropertyInfos/* properties */
	, InternalEnumerator_1_t3628_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3628_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3628_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3628_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3628_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3628_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3628_1_0_0/* this_arg */
	, InternalEnumerator_1_t3628_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3628_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3628_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3628)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7923_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>
extern MethodInfo ICollection_1_get_Count_m44950_MethodInfo;
static PropertyInfo ICollection_1_t7923____Count_PropertyInfo = 
{
	&ICollection_1_t7923_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44950_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44951_MethodInfo;
static PropertyInfo ICollection_1_t7923____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7923_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44951_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7923_PropertyInfos[] =
{
	&ICollection_1_t7923____Count_PropertyInfo,
	&ICollection_1_t7923____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44950_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>::get_Count()
MethodInfo ICollection_1_get_Count_m44950_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7923_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44950_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44951_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44951_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7923_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44951_GenericMethod/* genericMethod */

};
extern Il2CppType BaseVertexEffect_t411_0_0_0;
extern Il2CppType BaseVertexEffect_t411_0_0_0;
static ParameterInfo ICollection_1_t7923_ICollection_1_Add_m44952_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseVertexEffect_t411_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44952_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>::Add(T)
MethodInfo ICollection_1_Add_m44952_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7923_ICollection_1_Add_m44952_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44952_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44953_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>::Clear()
MethodInfo ICollection_1_Clear_m44953_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44953_GenericMethod/* genericMethod */

};
extern Il2CppType BaseVertexEffect_t411_0_0_0;
static ParameterInfo ICollection_1_t7923_ICollection_1_Contains_m44954_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseVertexEffect_t411_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44954_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>::Contains(T)
MethodInfo ICollection_1_Contains_m44954_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7923_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7923_ICollection_1_Contains_m44954_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44954_GenericMethod/* genericMethod */

};
extern Il2CppType BaseVertexEffectU5BU5D_t5642_0_0_0;
extern Il2CppType BaseVertexEffectU5BU5D_t5642_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7923_ICollection_1_CopyTo_m44955_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BaseVertexEffectU5BU5D_t5642_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44955_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44955_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7923_ICollection_1_CopyTo_m44955_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44955_GenericMethod/* genericMethod */

};
extern Il2CppType BaseVertexEffect_t411_0_0_0;
static ParameterInfo ICollection_1_t7923_ICollection_1_Remove_m44956_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseVertexEffect_t411_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44956_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.BaseVertexEffect>::Remove(T)
MethodInfo ICollection_1_Remove_m44956_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7923_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7923_ICollection_1_Remove_m44956_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44956_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7923_MethodInfos[] =
{
	&ICollection_1_get_Count_m44950_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44951_MethodInfo,
	&ICollection_1_Add_m44952_MethodInfo,
	&ICollection_1_Clear_m44953_MethodInfo,
	&ICollection_1_Contains_m44954_MethodInfo,
	&ICollection_1_CopyTo_m44955_MethodInfo,
	&ICollection_1_Remove_m44956_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7925_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7923_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7925_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7923_0_0_0;
extern Il2CppType ICollection_1_t7923_1_0_0;
struct ICollection_1_t7923;
extern Il2CppGenericClass ICollection_1_t7923_GenericClass;
TypeInfo ICollection_1_t7923_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7923_MethodInfos/* methods */
	, ICollection_1_t7923_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7923_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7923_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7923_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7923_0_0_0/* byval_arg */
	, &ICollection_1_t7923_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7923_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.BaseVertexEffect>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.BaseVertexEffect>
extern Il2CppType IEnumerator_1_t6238_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44957_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.BaseVertexEffect>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44957_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7925_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6238_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44957_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7925_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44957_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7925_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7925_0_0_0;
extern Il2CppType IEnumerable_1_t7925_1_0_0;
struct IEnumerable_1_t7925;
extern Il2CppGenericClass IEnumerable_1_t7925_GenericClass;
TypeInfo IEnumerable_1_t7925_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7925_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7925_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7925_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7925_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7925_0_0_0/* byval_arg */
	, &IEnumerable_1_t7925_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7925_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7924_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.BaseVertexEffect>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.BaseVertexEffect>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.BaseVertexEffect>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.BaseVertexEffect>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.BaseVertexEffect>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.BaseVertexEffect>
extern MethodInfo IList_1_get_Item_m44958_MethodInfo;
extern MethodInfo IList_1_set_Item_m44959_MethodInfo;
static PropertyInfo IList_1_t7924____Item_PropertyInfo = 
{
	&IList_1_t7924_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44958_MethodInfo/* get */
	, &IList_1_set_Item_m44959_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7924_PropertyInfos[] =
{
	&IList_1_t7924____Item_PropertyInfo,
	NULL
};
extern Il2CppType BaseVertexEffect_t411_0_0_0;
static ParameterInfo IList_1_t7924_IList_1_IndexOf_m44960_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &BaseVertexEffect_t411_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44960_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.BaseVertexEffect>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44960_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7924_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7924_IList_1_IndexOf_m44960_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44960_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType BaseVertexEffect_t411_0_0_0;
static ParameterInfo IList_1_t7924_IList_1_Insert_m44961_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &BaseVertexEffect_t411_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44961_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.BaseVertexEffect>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44961_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7924_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7924_IList_1_Insert_m44961_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44961_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7924_IList_1_RemoveAt_m44962_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44962_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.BaseVertexEffect>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44962_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7924_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7924_IList_1_RemoveAt_m44962_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44962_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7924_IList_1_get_Item_m44958_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType BaseVertexEffect_t411_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44958_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.BaseVertexEffect>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44958_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7924_il2cpp_TypeInfo/* declaring_type */
	, &BaseVertexEffect_t411_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7924_IList_1_get_Item_m44958_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44958_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType BaseVertexEffect_t411_0_0_0;
static ParameterInfo IList_1_t7924_IList_1_set_Item_m44959_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &BaseVertexEffect_t411_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44959_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.BaseVertexEffect>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44959_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7924_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7924_IList_1_set_Item_m44959_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44959_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7924_MethodInfos[] =
{
	&IList_1_IndexOf_m44960_MethodInfo,
	&IList_1_Insert_m44961_MethodInfo,
	&IList_1_RemoveAt_m44962_MethodInfo,
	&IList_1_get_Item_m44958_MethodInfo,
	&IList_1_set_Item_m44959_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7924_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7923_il2cpp_TypeInfo,
	&IEnumerable_1_t7925_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7924_0_0_0;
extern Il2CppType IList_1_t7924_1_0_0;
struct IList_1_t7924;
extern Il2CppGenericClass IList_1_t7924_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7924_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7924_MethodInfos/* methods */
	, IList_1_t7924_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7924_il2cpp_TypeInfo/* element_class */
	, IList_1_t7924_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7924_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7924_0_0_0/* byval_arg */
	, &IList_1_t7924_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7924_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7926_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>
extern MethodInfo ICollection_1_get_Count_m44963_MethodInfo;
static PropertyInfo ICollection_1_t7926____Count_PropertyInfo = 
{
	&ICollection_1_t7926_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44963_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44964_MethodInfo;
static PropertyInfo ICollection_1_t7926____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7926_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44964_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7926_PropertyInfos[] =
{
	&ICollection_1_t7926____Count_PropertyInfo,
	&ICollection_1_t7926____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44963_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>::get_Count()
MethodInfo ICollection_1_get_Count_m44963_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7926_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44963_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44964_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44964_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44964_GenericMethod/* genericMethod */

};
extern Il2CppType IVertexModifier_t466_0_0_0;
extern Il2CppType IVertexModifier_t466_0_0_0;
static ParameterInfo ICollection_1_t7926_ICollection_1_Add_m44965_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IVertexModifier_t466_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44965_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>::Add(T)
MethodInfo ICollection_1_Add_m44965_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7926_ICollection_1_Add_m44965_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44965_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44966_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>::Clear()
MethodInfo ICollection_1_Clear_m44966_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44966_GenericMethod/* genericMethod */

};
extern Il2CppType IVertexModifier_t466_0_0_0;
static ParameterInfo ICollection_1_t7926_ICollection_1_Contains_m44967_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IVertexModifier_t466_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44967_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>::Contains(T)
MethodInfo ICollection_1_Contains_m44967_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7926_ICollection_1_Contains_m44967_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44967_GenericMethod/* genericMethod */

};
extern Il2CppType IVertexModifierU5BU5D_t5643_0_0_0;
extern Il2CppType IVertexModifierU5BU5D_t5643_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7926_ICollection_1_CopyTo_m44968_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IVertexModifierU5BU5D_t5643_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44968_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44968_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7926_ICollection_1_CopyTo_m44968_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44968_GenericMethod/* genericMethod */

};
extern Il2CppType IVertexModifier_t466_0_0_0;
static ParameterInfo ICollection_1_t7926_ICollection_1_Remove_m44969_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IVertexModifier_t466_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44969_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.IVertexModifier>::Remove(T)
MethodInfo ICollection_1_Remove_m44969_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7926_ICollection_1_Remove_m44969_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44969_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7926_MethodInfos[] =
{
	&ICollection_1_get_Count_m44963_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44964_MethodInfo,
	&ICollection_1_Add_m44965_MethodInfo,
	&ICollection_1_Clear_m44966_MethodInfo,
	&ICollection_1_Contains_m44967_MethodInfo,
	&ICollection_1_CopyTo_m44968_MethodInfo,
	&ICollection_1_Remove_m44969_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7928_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7926_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7928_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7926_0_0_0;
extern Il2CppType ICollection_1_t7926_1_0_0;
struct ICollection_1_t7926;
extern Il2CppGenericClass ICollection_1_t7926_GenericClass;
TypeInfo ICollection_1_t7926_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7926_MethodInfos/* methods */
	, ICollection_1_t7926_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7926_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7926_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7926_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7926_0_0_0/* byval_arg */
	, &ICollection_1_t7926_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7926_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IVertexModifier>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IVertexModifier>
extern Il2CppType IEnumerator_1_t6240_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44970_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.IVertexModifier>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44970_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7928_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6240_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44970_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7928_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44970_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7928_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7928_0_0_0;
extern Il2CppType IEnumerable_1_t7928_1_0_0;
struct IEnumerable_1_t7928;
extern Il2CppGenericClass IEnumerable_1_t7928_GenericClass;
TypeInfo IEnumerable_1_t7928_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7928_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7928_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7928_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7928_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7928_0_0_0/* byval_arg */
	, &IEnumerable_1_t7928_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7928_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6240_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IVertexModifier>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IVertexModifier>
extern MethodInfo IEnumerator_1_get_Current_m44971_MethodInfo;
static PropertyInfo IEnumerator_1_t6240____Current_PropertyInfo = 
{
	&IEnumerator_1_t6240_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44971_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6240_PropertyInfos[] =
{
	&IEnumerator_1_t6240____Current_PropertyInfo,
	NULL
};
extern Il2CppType IVertexModifier_t466_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44971_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.IVertexModifier>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44971_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6240_il2cpp_TypeInfo/* declaring_type */
	, &IVertexModifier_t466_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44971_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6240_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44971_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6240_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6240_0_0_0;
extern Il2CppType IEnumerator_1_t6240_1_0_0;
struct IEnumerator_1_t6240;
extern Il2CppGenericClass IEnumerator_1_t6240_GenericClass;
TypeInfo IEnumerator_1_t6240_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6240_MethodInfos/* methods */
	, IEnumerator_1_t6240_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6240_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6240_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6240_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6240_0_0_0/* byval_arg */
	, &IEnumerator_1_t6240_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6240_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.IVertexModifier>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_267.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3629_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.IVertexModifier>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_267MethodDeclarations.h"

extern TypeInfo IVertexModifier_t466_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m20104_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIVertexModifier_t466_m34904_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.IVertexModifier>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.IVertexModifier>(System.Int32)
#define Array_InternalArray__get_Item_TisIVertexModifier_t466_m34904(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IVertexModifier>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.IVertexModifier>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IVertexModifier>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.IVertexModifier>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UI.IVertexModifier>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.IVertexModifier>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3629____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3629_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3629, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3629____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3629_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3629, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3629_FieldInfos[] =
{
	&InternalEnumerator_1_t3629____array_0_FieldInfo,
	&InternalEnumerator_1_t3629____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20101_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3629____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3629_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20101_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3629____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3629_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m20104_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3629_PropertyInfos[] =
{
	&InternalEnumerator_1_t3629____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3629____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3629_InternalEnumerator_1__ctor_m20100_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m20100_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IVertexModifier>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m20100_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3629_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3629_InternalEnumerator_1__ctor_m20100_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m20100_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20101_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.IVertexModifier>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20101_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3629_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20101_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m20102_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.IVertexModifier>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m20102_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3629_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m20102_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m20103_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.IVertexModifier>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m20103_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3629_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m20103_GenericMethod/* genericMethod */

};
extern Il2CppType IVertexModifier_t466_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m20104_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.IVertexModifier>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m20104_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3629_il2cpp_TypeInfo/* declaring_type */
	, &IVertexModifier_t466_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m20104_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3629_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m20100_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20101_MethodInfo,
	&InternalEnumerator_1_Dispose_m20102_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20103_MethodInfo,
	&InternalEnumerator_1_get_Current_m20104_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m20103_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m20102_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3629_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20101_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20103_MethodInfo,
	&InternalEnumerator_1_Dispose_m20102_MethodInfo,
	&InternalEnumerator_1_get_Current_m20104_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3629_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6240_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3629_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6240_il2cpp_TypeInfo, 7},
};
extern TypeInfo IVertexModifier_t466_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3629_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m20104_MethodInfo/* Method Usage */,
	&IVertexModifier_t466_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIVertexModifier_t466_m34904_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3629_0_0_0;
extern Il2CppType InternalEnumerator_1_t3629_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3629_GenericClass;
TypeInfo InternalEnumerator_1_t3629_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3629_MethodInfos/* methods */
	, InternalEnumerator_1_t3629_PropertyInfos/* properties */
	, InternalEnumerator_1_t3629_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3629_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3629_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3629_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3629_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3629_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3629_1_0_0/* this_arg */
	, InternalEnumerator_1_t3629_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3629_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3629_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3629)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7927_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.IVertexModifier>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IVertexModifier>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IVertexModifier>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.IVertexModifier>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IVertexModifier>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.IVertexModifier>
extern MethodInfo IList_1_get_Item_m44972_MethodInfo;
extern MethodInfo IList_1_set_Item_m44973_MethodInfo;
static PropertyInfo IList_1_t7927____Item_PropertyInfo = 
{
	&IList_1_t7927_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44972_MethodInfo/* get */
	, &IList_1_set_Item_m44973_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7927_PropertyInfos[] =
{
	&IList_1_t7927____Item_PropertyInfo,
	NULL
};
extern Il2CppType IVertexModifier_t466_0_0_0;
static ParameterInfo IList_1_t7927_IList_1_IndexOf_m44974_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IVertexModifier_t466_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44974_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.IVertexModifier>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44974_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7927_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7927_IList_1_IndexOf_m44974_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44974_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IVertexModifier_t466_0_0_0;
static ParameterInfo IList_1_t7927_IList_1_Insert_m44975_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IVertexModifier_t466_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44975_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IVertexModifier>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44975_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7927_IList_1_Insert_m44975_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44975_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7927_IList_1_RemoveAt_m44976_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44976_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IVertexModifier>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44976_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7927_IList_1_RemoveAt_m44976_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44976_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7927_IList_1_get_Item_m44972_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IVertexModifier_t466_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44972_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.IVertexModifier>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44972_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7927_il2cpp_TypeInfo/* declaring_type */
	, &IVertexModifier_t466_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7927_IList_1_get_Item_m44972_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44972_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IVertexModifier_t466_0_0_0;
static ParameterInfo IList_1_t7927_IList_1_set_Item_m44973_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IVertexModifier_t466_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44973_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.IVertexModifier>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44973_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7927_IList_1_set_Item_m44973_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44973_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7927_MethodInfos[] =
{
	&IList_1_IndexOf_m44974_MethodInfo,
	&IList_1_Insert_m44975_MethodInfo,
	&IList_1_RemoveAt_m44976_MethodInfo,
	&IList_1_get_Item_m44972_MethodInfo,
	&IList_1_set_Item_m44973_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7927_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7926_il2cpp_TypeInfo,
	&IEnumerable_1_t7928_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7927_0_0_0;
extern Il2CppType IList_1_t7927_1_0_0;
struct IList_1_t7927;
extern Il2CppGenericClass IList_1_t7927_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7927_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7927_MethodInfos/* methods */
	, IList_1_t7927_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7927_il2cpp_TypeInfo/* element_class */
	, IList_1_t7927_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7927_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7927_0_0_0/* byval_arg */
	, &IList_1_t7927_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7927_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.BaseVertexEffect>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_82.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3630_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.BaseVertexEffect>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_82MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.BaseVertexEffect>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_84.h"
extern TypeInfo InvokableCall_1_t3631_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.BaseVertexEffect>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_84MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m20107_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m20109_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.BaseVertexEffect>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.BaseVertexEffect>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.BaseVertexEffect>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t3630____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t3630_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3630, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3630_FieldInfos[] =
{
	&CachedInvokableCall_1_t3630____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType BaseVertexEffect_t411_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3630_CachedInvokableCall_1__ctor_m20105_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &BaseVertexEffect_t411_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m20105_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.BaseVertexEffect>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m20105_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t3630_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3630_CachedInvokableCall_1__ctor_m20105_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m20105_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3630_CachedInvokableCall_1_Invoke_m20106_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m20106_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.BaseVertexEffect>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m20106_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t3630_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3630_CachedInvokableCall_1_Invoke_m20106_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m20106_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3630_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m20105_MethodInfo,
	&CachedInvokableCall_1_Invoke_m20106_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m20106_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m20110_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3630_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m20106_MethodInfo,
	&InvokableCall_1_Find_m20110_MethodInfo,
};
extern Il2CppType UnityAction_1_t3632_0_0_0;
extern TypeInfo UnityAction_1_t3632_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisBaseVertexEffect_t411_m34914_MethodInfo;
extern TypeInfo BaseVertexEffect_t411_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m20112_MethodInfo;
extern TypeInfo BaseVertexEffect_t411_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3630_RGCTXData[8] = 
{
	&UnityAction_1_t3632_0_0_0/* Type Usage */,
	&UnityAction_1_t3632_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBaseVertexEffect_t411_m34914_MethodInfo/* Method Usage */,
	&BaseVertexEffect_t411_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m20112_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m20107_MethodInfo/* Method Usage */,
	&BaseVertexEffect_t411_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m20109_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3630_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3630_1_0_0;
struct CachedInvokableCall_1_t3630;
extern Il2CppGenericClass CachedInvokableCall_1_t3630_GenericClass;
TypeInfo CachedInvokableCall_1_t3630_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3630_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3630_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3631_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3630_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3630_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3630_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3630_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3630_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3630_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3630_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3630)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.UI.BaseVertexEffect>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_89.h"
extern TypeInfo UnityAction_1_t3632_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.BaseVertexEffect>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_89MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.BaseVertexEffect>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.BaseVertexEffect>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisBaseVertexEffect_t411_m34914(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.BaseVertexEffect>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.BaseVertexEffect>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.BaseVertexEffect>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.BaseVertexEffect>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.BaseVertexEffect>
extern Il2CppType UnityAction_1_t3632_0_0_1;
FieldInfo InvokableCall_1_t3631____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3632_0_0_1/* type */
	, &InvokableCall_1_t3631_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3631, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3631_FieldInfos[] =
{
	&InvokableCall_1_t3631____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3631_InvokableCall_1__ctor_m20107_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m20107_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.BaseVertexEffect>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m20107_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t3631_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3631_InvokableCall_1__ctor_m20107_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m20107_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3632_0_0_0;
static ParameterInfo InvokableCall_1_t3631_InvokableCall_1__ctor_m20108_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3632_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m20108_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.BaseVertexEffect>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m20108_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t3631_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3631_InvokableCall_1__ctor_m20108_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m20108_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t3631_InvokableCall_1_Invoke_m20109_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m20109_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.BaseVertexEffect>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m20109_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t3631_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3631_InvokableCall_1_Invoke_m20109_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m20109_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3631_InvokableCall_1_Find_m20110_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m20110_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.BaseVertexEffect>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m20110_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t3631_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3631_InvokableCall_1_Find_m20110_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m20110_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3631_MethodInfos[] =
{
	&InvokableCall_1__ctor_m20107_MethodInfo,
	&InvokableCall_1__ctor_m20108_MethodInfo,
	&InvokableCall_1_Invoke_m20109_MethodInfo,
	&InvokableCall_1_Find_m20110_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3631_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m20109_MethodInfo,
	&InvokableCall_1_Find_m20110_MethodInfo,
};
extern TypeInfo UnityAction_1_t3632_il2cpp_TypeInfo;
extern TypeInfo BaseVertexEffect_t411_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3631_RGCTXData[5] = 
{
	&UnityAction_1_t3632_0_0_0/* Type Usage */,
	&UnityAction_1_t3632_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisBaseVertexEffect_t411_m34914_MethodInfo/* Method Usage */,
	&BaseVertexEffect_t411_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m20112_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3631_0_0_0;
extern Il2CppType InvokableCall_1_t3631_1_0_0;
struct InvokableCall_1_t3631;
extern Il2CppGenericClass InvokableCall_1_t3631_GenericClass;
TypeInfo InvokableCall_1_t3631_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3631_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3631_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3631_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3631_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3631_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3631_0_0_0/* byval_arg */
	, &InvokableCall_1_t3631_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3631_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3631_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3631)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.BaseVertexEffect>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.BaseVertexEffect>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.BaseVertexEffect>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.BaseVertexEffect>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.BaseVertexEffect>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3632_UnityAction_1__ctor_m20111_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m20111_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.BaseVertexEffect>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m20111_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t3632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3632_UnityAction_1__ctor_m20111_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m20111_GenericMethod/* genericMethod */

};
extern Il2CppType BaseVertexEffect_t411_0_0_0;
static ParameterInfo UnityAction_1_t3632_UnityAction_1_Invoke_m20112_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &BaseVertexEffect_t411_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m20112_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.BaseVertexEffect>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m20112_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t3632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3632_UnityAction_1_Invoke_m20112_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m20112_GenericMethod/* genericMethod */

};
extern Il2CppType BaseVertexEffect_t411_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3632_UnityAction_1_BeginInvoke_m20113_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &BaseVertexEffect_t411_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m20113_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.BaseVertexEffect>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m20113_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t3632_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3632_UnityAction_1_BeginInvoke_m20113_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m20113_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t3632_UnityAction_1_EndInvoke_m20114_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m20114_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.BaseVertexEffect>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m20114_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t3632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3632_UnityAction_1_EndInvoke_m20114_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m20114_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3632_MethodInfos[] =
{
	&UnityAction_1__ctor_m20111_MethodInfo,
	&UnityAction_1_Invoke_m20112_MethodInfo,
	&UnityAction_1_BeginInvoke_m20113_MethodInfo,
	&UnityAction_1_EndInvoke_m20114_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m20113_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m20114_MethodInfo;
static MethodInfo* UnityAction_1_t3632_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m20112_MethodInfo,
	&UnityAction_1_BeginInvoke_m20113_MethodInfo,
	&UnityAction_1_EndInvoke_m20114_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3632_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3632_1_0_0;
struct UnityAction_1_t3632;
extern Il2CppGenericClass UnityAction_1_t3632_GenericClass;
TypeInfo UnityAction_1_t3632_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3632_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3632_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3632_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3632_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3632_0_0_0/* byval_arg */
	, &UnityAction_1_t3632_1_0_0/* this_arg */
	, UnityAction_1_t3632_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3632_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3632)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6242_il2cpp_TypeInfo;

// UnityEngine.UI.Outline
#include "UnityEngine_UI_UnityEngine_UI_Outline.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Outline>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Outline>
extern MethodInfo IEnumerator_1_get_Current_m44977_MethodInfo;
static PropertyInfo IEnumerator_1_t6242____Current_PropertyInfo = 
{
	&IEnumerator_1_t6242_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44977_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6242_PropertyInfos[] =
{
	&IEnumerator_1_t6242____Current_PropertyInfo,
	NULL
};
extern Il2CppType Outline_t412_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44977_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Outline>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44977_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6242_il2cpp_TypeInfo/* declaring_type */
	, &Outline_t412_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44977_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6242_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44977_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6242_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6242_0_0_0;
extern Il2CppType IEnumerator_1_t6242_1_0_0;
struct IEnumerator_1_t6242;
extern Il2CppGenericClass IEnumerator_1_t6242_GenericClass;
TypeInfo IEnumerator_1_t6242_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6242_MethodInfos/* methods */
	, IEnumerator_1_t6242_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6242_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6242_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6242_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6242_0_0_0/* byval_arg */
	, &IEnumerator_1_t6242_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6242_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.Outline>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_268.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3633_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.Outline>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_268MethodDeclarations.h"

extern TypeInfo Outline_t412_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m20119_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisOutline_t412_m34916_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Outline>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Outline>(System.Int32)
#define Array_InternalArray__get_Item_TisOutline_t412_m34916(__this, p0, method) (Outline_t412 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Outline>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Outline>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Outline>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Outline>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Outline>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Outline>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3633____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3633_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3633, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3633____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3633_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3633, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3633_FieldInfos[] =
{
	&InternalEnumerator_1_t3633____array_0_FieldInfo,
	&InternalEnumerator_1_t3633____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20116_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3633____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3633_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20116_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3633____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3633_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m20119_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3633_PropertyInfos[] =
{
	&InternalEnumerator_1_t3633____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3633____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3633_InternalEnumerator_1__ctor_m20115_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m20115_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Outline>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m20115_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3633_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3633_InternalEnumerator_1__ctor_m20115_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m20115_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20116_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Outline>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20116_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3633_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20116_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m20117_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Outline>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m20117_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3633_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m20117_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m20118_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Outline>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m20118_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3633_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m20118_GenericMethod/* genericMethod */

};
extern Il2CppType Outline_t412_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m20119_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Outline>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m20119_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3633_il2cpp_TypeInfo/* declaring_type */
	, &Outline_t412_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m20119_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3633_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m20115_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20116_MethodInfo,
	&InternalEnumerator_1_Dispose_m20117_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20118_MethodInfo,
	&InternalEnumerator_1_get_Current_m20119_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m20118_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m20117_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3633_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20116_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20118_MethodInfo,
	&InternalEnumerator_1_Dispose_m20117_MethodInfo,
	&InternalEnumerator_1_get_Current_m20119_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3633_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6242_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3633_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6242_il2cpp_TypeInfo, 7},
};
extern TypeInfo Outline_t412_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3633_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m20119_MethodInfo/* Method Usage */,
	&Outline_t412_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisOutline_t412_m34916_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3633_0_0_0;
extern Il2CppType InternalEnumerator_1_t3633_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3633_GenericClass;
TypeInfo InternalEnumerator_1_t3633_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3633_MethodInfos/* methods */
	, InternalEnumerator_1_t3633_PropertyInfos/* properties */
	, InternalEnumerator_1_t3633_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3633_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3633_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3633_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3633_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3633_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3633_1_0_0/* this_arg */
	, InternalEnumerator_1_t3633_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3633_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3633_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3633)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7929_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>
extern MethodInfo ICollection_1_get_Count_m44978_MethodInfo;
static PropertyInfo ICollection_1_t7929____Count_PropertyInfo = 
{
	&ICollection_1_t7929_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44978_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44979_MethodInfo;
static PropertyInfo ICollection_1_t7929____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7929_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44979_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7929_PropertyInfos[] =
{
	&ICollection_1_t7929____Count_PropertyInfo,
	&ICollection_1_t7929____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44978_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>::get_Count()
MethodInfo ICollection_1_get_Count_m44978_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7929_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44978_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44979_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44979_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7929_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44979_GenericMethod/* genericMethod */

};
extern Il2CppType Outline_t412_0_0_0;
extern Il2CppType Outline_t412_0_0_0;
static ParameterInfo ICollection_1_t7929_ICollection_1_Add_m44980_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Outline_t412_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44980_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>::Add(T)
MethodInfo ICollection_1_Add_m44980_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7929_ICollection_1_Add_m44980_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44980_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44981_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>::Clear()
MethodInfo ICollection_1_Clear_m44981_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44981_GenericMethod/* genericMethod */

};
extern Il2CppType Outline_t412_0_0_0;
static ParameterInfo ICollection_1_t7929_ICollection_1_Contains_m44982_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Outline_t412_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44982_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>::Contains(T)
MethodInfo ICollection_1_Contains_m44982_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7929_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7929_ICollection_1_Contains_m44982_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44982_GenericMethod/* genericMethod */

};
extern Il2CppType OutlineU5BU5D_t5644_0_0_0;
extern Il2CppType OutlineU5BU5D_t5644_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7929_ICollection_1_CopyTo_m44983_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &OutlineU5BU5D_t5644_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44983_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44983_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7929_ICollection_1_CopyTo_m44983_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44983_GenericMethod/* genericMethod */

};
extern Il2CppType Outline_t412_0_0_0;
static ParameterInfo ICollection_1_t7929_ICollection_1_Remove_m44984_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Outline_t412_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44984_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Outline>::Remove(T)
MethodInfo ICollection_1_Remove_m44984_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7929_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7929_ICollection_1_Remove_m44984_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44984_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7929_MethodInfos[] =
{
	&ICollection_1_get_Count_m44978_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44979_MethodInfo,
	&ICollection_1_Add_m44980_MethodInfo,
	&ICollection_1_Clear_m44981_MethodInfo,
	&ICollection_1_Contains_m44982_MethodInfo,
	&ICollection_1_CopyTo_m44983_MethodInfo,
	&ICollection_1_Remove_m44984_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7931_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7929_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7931_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7929_0_0_0;
extern Il2CppType ICollection_1_t7929_1_0_0;
struct ICollection_1_t7929;
extern Il2CppGenericClass ICollection_1_t7929_GenericClass;
TypeInfo ICollection_1_t7929_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7929_MethodInfos/* methods */
	, ICollection_1_t7929_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7929_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7929_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7929_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7929_0_0_0/* byval_arg */
	, &ICollection_1_t7929_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7929_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Outline>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Outline>
extern Il2CppType IEnumerator_1_t6242_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44985_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Outline>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44985_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7931_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6242_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44985_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7931_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44985_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7931_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7931_0_0_0;
extern Il2CppType IEnumerable_1_t7931_1_0_0;
struct IEnumerable_1_t7931;
extern Il2CppGenericClass IEnumerable_1_t7931_GenericClass;
TypeInfo IEnumerable_1_t7931_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7931_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7931_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7931_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7931_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7931_0_0_0/* byval_arg */
	, &IEnumerable_1_t7931_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7931_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7930_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Outline>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Outline>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Outline>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.Outline>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Outline>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Outline>
extern MethodInfo IList_1_get_Item_m44986_MethodInfo;
extern MethodInfo IList_1_set_Item_m44987_MethodInfo;
static PropertyInfo IList_1_t7930____Item_PropertyInfo = 
{
	&IList_1_t7930_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44986_MethodInfo/* get */
	, &IList_1_set_Item_m44987_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7930_PropertyInfos[] =
{
	&IList_1_t7930____Item_PropertyInfo,
	NULL
};
extern Il2CppType Outline_t412_0_0_0;
static ParameterInfo IList_1_t7930_IList_1_IndexOf_m44988_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Outline_t412_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44988_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Outline>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44988_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7930_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7930_IList_1_IndexOf_m44988_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44988_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Outline_t412_0_0_0;
static ParameterInfo IList_1_t7930_IList_1_Insert_m44989_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Outline_t412_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44989_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Outline>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44989_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7930_IList_1_Insert_m44989_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44989_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7930_IList_1_RemoveAt_m44990_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44990_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Outline>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44990_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7930_IList_1_RemoveAt_m44990_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44990_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7930_IList_1_get_Item_m44986_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Outline_t412_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44986_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.Outline>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44986_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7930_il2cpp_TypeInfo/* declaring_type */
	, &Outline_t412_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7930_IList_1_get_Item_m44986_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44986_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Outline_t412_0_0_0;
static ParameterInfo IList_1_t7930_IList_1_set_Item_m44987_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Outline_t412_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44987_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Outline>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44987_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7930_IList_1_set_Item_m44987_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44987_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7930_MethodInfos[] =
{
	&IList_1_IndexOf_m44988_MethodInfo,
	&IList_1_Insert_m44989_MethodInfo,
	&IList_1_RemoveAt_m44990_MethodInfo,
	&IList_1_get_Item_m44986_MethodInfo,
	&IList_1_set_Item_m44987_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7930_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7929_il2cpp_TypeInfo,
	&IEnumerable_1_t7931_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7930_0_0_0;
extern Il2CppType IList_1_t7930_1_0_0;
struct IList_1_t7930;
extern Il2CppGenericClass IList_1_t7930_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7930_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7930_MethodInfos/* methods */
	, IList_1_t7930_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7930_il2cpp_TypeInfo/* element_class */
	, IList_1_t7930_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7930_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7930_0_0_0/* byval_arg */
	, &IList_1_t7930_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7930_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7932_il2cpp_TypeInfo;

// UnityEngine.UI.Shadow
#include "UnityEngine_UI_UnityEngine_UI_Shadow.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>
extern MethodInfo ICollection_1_get_Count_m44991_MethodInfo;
static PropertyInfo ICollection_1_t7932____Count_PropertyInfo = 
{
	&ICollection_1_t7932_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44991_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44992_MethodInfo;
static PropertyInfo ICollection_1_t7932____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7932_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44992_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7932_PropertyInfos[] =
{
	&ICollection_1_t7932____Count_PropertyInfo,
	&ICollection_1_t7932____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44991_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>::get_Count()
MethodInfo ICollection_1_get_Count_m44991_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7932_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44991_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44992_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44992_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7932_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44992_GenericMethod/* genericMethod */

};
extern Il2CppType Shadow_t413_0_0_0;
extern Il2CppType Shadow_t413_0_0_0;
static ParameterInfo ICollection_1_t7932_ICollection_1_Add_m44993_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Shadow_t413_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44993_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>::Add(T)
MethodInfo ICollection_1_Add_m44993_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7932_ICollection_1_Add_m44993_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44993_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44994_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>::Clear()
MethodInfo ICollection_1_Clear_m44994_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44994_GenericMethod/* genericMethod */

};
extern Il2CppType Shadow_t413_0_0_0;
static ParameterInfo ICollection_1_t7932_ICollection_1_Contains_m44995_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Shadow_t413_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44995_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>::Contains(T)
MethodInfo ICollection_1_Contains_m44995_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7932_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7932_ICollection_1_Contains_m44995_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44995_GenericMethod/* genericMethod */

};
extern Il2CppType ShadowU5BU5D_t5645_0_0_0;
extern Il2CppType ShadowU5BU5D_t5645_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7932_ICollection_1_CopyTo_m44996_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ShadowU5BU5D_t5645_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44996_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44996_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7932_ICollection_1_CopyTo_m44996_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44996_GenericMethod/* genericMethod */

};
extern Il2CppType Shadow_t413_0_0_0;
static ParameterInfo ICollection_1_t7932_ICollection_1_Remove_m44997_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Shadow_t413_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44997_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Shadow>::Remove(T)
MethodInfo ICollection_1_Remove_m44997_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7932_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7932_ICollection_1_Remove_m44997_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44997_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7932_MethodInfos[] =
{
	&ICollection_1_get_Count_m44991_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44992_MethodInfo,
	&ICollection_1_Add_m44993_MethodInfo,
	&ICollection_1_Clear_m44994_MethodInfo,
	&ICollection_1_Contains_m44995_MethodInfo,
	&ICollection_1_CopyTo_m44996_MethodInfo,
	&ICollection_1_Remove_m44997_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7934_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7932_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7934_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7932_0_0_0;
extern Il2CppType ICollection_1_t7932_1_0_0;
struct ICollection_1_t7932;
extern Il2CppGenericClass ICollection_1_t7932_GenericClass;
TypeInfo ICollection_1_t7932_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7932_MethodInfos/* methods */
	, ICollection_1_t7932_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7932_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7932_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7932_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7932_0_0_0/* byval_arg */
	, &ICollection_1_t7932_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7932_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Shadow>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Shadow>
extern Il2CppType IEnumerator_1_t6244_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44998_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Shadow>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44998_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7934_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6244_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44998_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7934_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44998_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7934_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7934_0_0_0;
extern Il2CppType IEnumerable_1_t7934_1_0_0;
struct IEnumerable_1_t7934;
extern Il2CppGenericClass IEnumerable_1_t7934_GenericClass;
TypeInfo IEnumerable_1_t7934_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7934_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7934_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7934_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7934_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7934_0_0_0/* byval_arg */
	, &IEnumerable_1_t7934_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7934_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6244_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Shadow>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Shadow>
extern MethodInfo IEnumerator_1_get_Current_m44999_MethodInfo;
static PropertyInfo IEnumerator_1_t6244____Current_PropertyInfo = 
{
	&IEnumerator_1_t6244_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44999_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6244_PropertyInfos[] =
{
	&IEnumerator_1_t6244____Current_PropertyInfo,
	NULL
};
extern Il2CppType Shadow_t413_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44999_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Shadow>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44999_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6244_il2cpp_TypeInfo/* declaring_type */
	, &Shadow_t413_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44999_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6244_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44999_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6244_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6244_0_0_0;
extern Il2CppType IEnumerator_1_t6244_1_0_0;
struct IEnumerator_1_t6244;
extern Il2CppGenericClass IEnumerator_1_t6244_GenericClass;
TypeInfo IEnumerator_1_t6244_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6244_MethodInfos/* methods */
	, IEnumerator_1_t6244_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6244_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6244_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6244_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6244_0_0_0/* byval_arg */
	, &IEnumerator_1_t6244_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6244_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.Shadow>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_269.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3634_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.Shadow>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_269MethodDeclarations.h"

extern TypeInfo Shadow_t413_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m20124_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisShadow_t413_m34927_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Shadow>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Shadow>(System.Int32)
#define Array_InternalArray__get_Item_TisShadow_t413_m34927(__this, p0, method) (Shadow_t413 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Shadow>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Shadow>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Shadow>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Shadow>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Shadow>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Shadow>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3634____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3634_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3634, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3634____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3634_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3634, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3634_FieldInfos[] =
{
	&InternalEnumerator_1_t3634____array_0_FieldInfo,
	&InternalEnumerator_1_t3634____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20121_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3634____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3634_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20121_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3634____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3634_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m20124_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3634_PropertyInfos[] =
{
	&InternalEnumerator_1_t3634____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3634____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3634_InternalEnumerator_1__ctor_m20120_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m20120_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Shadow>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m20120_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3634_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3634_InternalEnumerator_1__ctor_m20120_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m20120_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20121_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Shadow>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20121_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3634_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20121_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m20122_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Shadow>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m20122_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3634_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m20122_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m20123_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Shadow>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m20123_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3634_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m20123_GenericMethod/* genericMethod */

};
extern Il2CppType Shadow_t413_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m20124_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Shadow>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m20124_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3634_il2cpp_TypeInfo/* declaring_type */
	, &Shadow_t413_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m20124_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3634_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m20120_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20121_MethodInfo,
	&InternalEnumerator_1_Dispose_m20122_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20123_MethodInfo,
	&InternalEnumerator_1_get_Current_m20124_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m20123_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m20122_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3634_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20121_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20123_MethodInfo,
	&InternalEnumerator_1_Dispose_m20122_MethodInfo,
	&InternalEnumerator_1_get_Current_m20124_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3634_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6244_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3634_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6244_il2cpp_TypeInfo, 7},
};
extern TypeInfo Shadow_t413_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3634_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m20124_MethodInfo/* Method Usage */,
	&Shadow_t413_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisShadow_t413_m34927_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3634_0_0_0;
extern Il2CppType InternalEnumerator_1_t3634_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3634_GenericClass;
TypeInfo InternalEnumerator_1_t3634_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3634_MethodInfos/* methods */
	, InternalEnumerator_1_t3634_PropertyInfos/* properties */
	, InternalEnumerator_1_t3634_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3634_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3634_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3634_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3634_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3634_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3634_1_0_0/* this_arg */
	, InternalEnumerator_1_t3634_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3634_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3634_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3634)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7933_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Shadow>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Shadow>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Shadow>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.Shadow>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Shadow>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Shadow>
extern MethodInfo IList_1_get_Item_m45000_MethodInfo;
extern MethodInfo IList_1_set_Item_m45001_MethodInfo;
static PropertyInfo IList_1_t7933____Item_PropertyInfo = 
{
	&IList_1_t7933_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m45000_MethodInfo/* get */
	, &IList_1_set_Item_m45001_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7933_PropertyInfos[] =
{
	&IList_1_t7933____Item_PropertyInfo,
	NULL
};
extern Il2CppType Shadow_t413_0_0_0;
static ParameterInfo IList_1_t7933_IList_1_IndexOf_m45002_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Shadow_t413_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m45002_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Shadow>::IndexOf(T)
MethodInfo IList_1_IndexOf_m45002_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7933_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7933_IList_1_IndexOf_m45002_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m45002_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Shadow_t413_0_0_0;
static ParameterInfo IList_1_t7933_IList_1_Insert_m45003_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Shadow_t413_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m45003_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Shadow>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m45003_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7933_IList_1_Insert_m45003_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m45003_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7933_IList_1_RemoveAt_m45004_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m45004_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Shadow>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m45004_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7933_IList_1_RemoveAt_m45004_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m45004_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7933_IList_1_get_Item_m45000_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Shadow_t413_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m45000_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.Shadow>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m45000_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7933_il2cpp_TypeInfo/* declaring_type */
	, &Shadow_t413_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7933_IList_1_get_Item_m45000_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m45000_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Shadow_t413_0_0_0;
static ParameterInfo IList_1_t7933_IList_1_set_Item_m45001_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Shadow_t413_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m45001_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Shadow>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m45001_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7933_IList_1_set_Item_m45001_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m45001_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7933_MethodInfos[] =
{
	&IList_1_IndexOf_m45002_MethodInfo,
	&IList_1_Insert_m45003_MethodInfo,
	&IList_1_RemoveAt_m45004_MethodInfo,
	&IList_1_get_Item_m45000_MethodInfo,
	&IList_1_set_Item_m45001_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7933_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7932_il2cpp_TypeInfo,
	&IEnumerable_1_t7934_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7933_0_0_0;
extern Il2CppType IList_1_t7933_1_0_0;
struct IList_1_t7933;
extern Il2CppGenericClass IList_1_t7933_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7933_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7933_MethodInfos/* methods */
	, IList_1_t7933_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7933_il2cpp_TypeInfo/* element_class */
	, IList_1_t7933_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7933_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7933_0_0_0/* byval_arg */
	, &IList_1_t7933_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7933_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Outline>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_83.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3635_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Outline>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_83MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Outline>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_85.h"
extern TypeInfo InvokableCall_1_t3636_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Outline>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_85MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m20127_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m20129_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Outline>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Outline>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Outline>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t3635____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t3635_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3635, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3635_FieldInfos[] =
{
	&CachedInvokableCall_1_t3635____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType Outline_t412_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3635_CachedInvokableCall_1__ctor_m20125_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Outline_t412_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m20125_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Outline>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m20125_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t3635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3635_CachedInvokableCall_1__ctor_m20125_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m20125_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3635_CachedInvokableCall_1_Invoke_m20126_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m20126_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Outline>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m20126_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t3635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3635_CachedInvokableCall_1_Invoke_m20126_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m20126_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3635_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m20125_MethodInfo,
	&CachedInvokableCall_1_Invoke_m20126_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m20126_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m20130_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3635_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m20126_MethodInfo,
	&InvokableCall_1_Find_m20130_MethodInfo,
};
extern Il2CppType UnityAction_1_t3637_0_0_0;
extern TypeInfo UnityAction_1_t3637_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisOutline_t412_m34937_MethodInfo;
extern TypeInfo Outline_t412_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m20132_MethodInfo;
extern TypeInfo Outline_t412_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3635_RGCTXData[8] = 
{
	&UnityAction_1_t3637_0_0_0/* Type Usage */,
	&UnityAction_1_t3637_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisOutline_t412_m34937_MethodInfo/* Method Usage */,
	&Outline_t412_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m20132_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m20127_MethodInfo/* Method Usage */,
	&Outline_t412_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m20129_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3635_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3635_1_0_0;
struct CachedInvokableCall_1_t3635;
extern Il2CppGenericClass CachedInvokableCall_1_t3635_GenericClass;
TypeInfo CachedInvokableCall_1_t3635_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3635_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3635_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3636_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3635_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3635_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3635_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3635_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3635_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3635_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3635_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3635)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.UI.Outline>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_90.h"
extern TypeInfo UnityAction_1_t3637_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.Outline>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_90MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.Outline>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.Outline>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisOutline_t412_m34937(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Outline>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Outline>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Outline>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Outline>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Outline>
extern Il2CppType UnityAction_1_t3637_0_0_1;
FieldInfo InvokableCall_1_t3636____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3637_0_0_1/* type */
	, &InvokableCall_1_t3636_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3636, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3636_FieldInfos[] =
{
	&InvokableCall_1_t3636____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3636_InvokableCall_1__ctor_m20127_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m20127_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Outline>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m20127_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t3636_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3636_InvokableCall_1__ctor_m20127_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m20127_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3637_0_0_0;
static ParameterInfo InvokableCall_1_t3636_InvokableCall_1__ctor_m20128_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3637_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m20128_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Outline>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m20128_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t3636_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3636_InvokableCall_1__ctor_m20128_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m20128_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t3636_InvokableCall_1_Invoke_m20129_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m20129_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Outline>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m20129_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t3636_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3636_InvokableCall_1_Invoke_m20129_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m20129_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3636_InvokableCall_1_Find_m20130_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m20130_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Outline>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m20130_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t3636_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3636_InvokableCall_1_Find_m20130_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m20130_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3636_MethodInfos[] =
{
	&InvokableCall_1__ctor_m20127_MethodInfo,
	&InvokableCall_1__ctor_m20128_MethodInfo,
	&InvokableCall_1_Invoke_m20129_MethodInfo,
	&InvokableCall_1_Find_m20130_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3636_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m20129_MethodInfo,
	&InvokableCall_1_Find_m20130_MethodInfo,
};
extern TypeInfo UnityAction_1_t3637_il2cpp_TypeInfo;
extern TypeInfo Outline_t412_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3636_RGCTXData[5] = 
{
	&UnityAction_1_t3637_0_0_0/* Type Usage */,
	&UnityAction_1_t3637_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisOutline_t412_m34937_MethodInfo/* Method Usage */,
	&Outline_t412_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m20132_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3636_0_0_0;
extern Il2CppType InvokableCall_1_t3636_1_0_0;
struct InvokableCall_1_t3636;
extern Il2CppGenericClass InvokableCall_1_t3636_GenericClass;
TypeInfo InvokableCall_1_t3636_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3636_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3636_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3636_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3636_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3636_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3636_0_0_0/* byval_arg */
	, &InvokableCall_1_t3636_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3636_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3636_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3636)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Outline>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Outline>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.Outline>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Outline>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.Outline>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3637_UnityAction_1__ctor_m20131_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m20131_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Outline>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m20131_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t3637_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3637_UnityAction_1__ctor_m20131_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m20131_GenericMethod/* genericMethod */

};
extern Il2CppType Outline_t412_0_0_0;
static ParameterInfo UnityAction_1_t3637_UnityAction_1_Invoke_m20132_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Outline_t412_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m20132_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Outline>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m20132_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t3637_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3637_UnityAction_1_Invoke_m20132_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m20132_GenericMethod/* genericMethod */

};
extern Il2CppType Outline_t412_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3637_UnityAction_1_BeginInvoke_m20133_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Outline_t412_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m20133_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.Outline>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m20133_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t3637_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3637_UnityAction_1_BeginInvoke_m20133_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m20133_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t3637_UnityAction_1_EndInvoke_m20134_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m20134_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Outline>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m20134_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t3637_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3637_UnityAction_1_EndInvoke_m20134_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m20134_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3637_MethodInfos[] =
{
	&UnityAction_1__ctor_m20131_MethodInfo,
	&UnityAction_1_Invoke_m20132_MethodInfo,
	&UnityAction_1_BeginInvoke_m20133_MethodInfo,
	&UnityAction_1_EndInvoke_m20134_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m20133_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m20134_MethodInfo;
static MethodInfo* UnityAction_1_t3637_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m20132_MethodInfo,
	&UnityAction_1_BeginInvoke_m20133_MethodInfo,
	&UnityAction_1_EndInvoke_m20134_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3637_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3637_1_0_0;
struct UnityAction_1_t3637;
extern Il2CppGenericClass UnityAction_1_t3637_GenericClass;
TypeInfo UnityAction_1_t3637_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3637_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3637_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3637_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3637_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3637_0_0_0/* byval_arg */
	, &UnityAction_1_t3637_1_0_0/* this_arg */
	, UnityAction_1_t3637_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3637_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3637)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6246_il2cpp_TypeInfo;

// UnityEngine.UI.PositionAsUV1
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV1.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.PositionAsUV1>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.PositionAsUV1>
extern MethodInfo IEnumerator_1_get_Current_m45005_MethodInfo;
static PropertyInfo IEnumerator_1_t6246____Current_PropertyInfo = 
{
	&IEnumerator_1_t6246_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m45005_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6246_PropertyInfos[] =
{
	&IEnumerator_1_t6246____Current_PropertyInfo,
	NULL
};
extern Il2CppType PositionAsUV1_t414_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m45005_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.PositionAsUV1>::get_Current()
MethodInfo IEnumerator_1_get_Current_m45005_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6246_il2cpp_TypeInfo/* declaring_type */
	, &PositionAsUV1_t414_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m45005_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6246_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m45005_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6246_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6246_0_0_0;
extern Il2CppType IEnumerator_1_t6246_1_0_0;
struct IEnumerator_1_t6246;
extern Il2CppGenericClass IEnumerator_1_t6246_GenericClass;
TypeInfo IEnumerator_1_t6246_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6246_MethodInfos/* methods */
	, IEnumerator_1_t6246_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6246_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6246_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6246_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6246_0_0_0/* byval_arg */
	, &IEnumerator_1_t6246_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6246_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.PositionAsUV1>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_270.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3638_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.PositionAsUV1>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_270MethodDeclarations.h"

extern TypeInfo PositionAsUV1_t414_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m20139_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisPositionAsUV1_t414_m34939_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.PositionAsUV1>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.PositionAsUV1>(System.Int32)
#define Array_InternalArray__get_Item_TisPositionAsUV1_t414_m34939(__this, p0, method) (PositionAsUV1_t414 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.PositionAsUV1>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.PositionAsUV1>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.PositionAsUV1>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.PositionAsUV1>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UI.PositionAsUV1>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.PositionAsUV1>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3638____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3638_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3638, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3638____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3638_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3638, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3638_FieldInfos[] =
{
	&InternalEnumerator_1_t3638____array_0_FieldInfo,
	&InternalEnumerator_1_t3638____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20136_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3638____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3638_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20136_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3638____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3638_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m20139_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3638_PropertyInfos[] =
{
	&InternalEnumerator_1_t3638____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3638____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3638_InternalEnumerator_1__ctor_m20135_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m20135_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.PositionAsUV1>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m20135_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3638_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3638_InternalEnumerator_1__ctor_m20135_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m20135_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20136_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.PositionAsUV1>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20136_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3638_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20136_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m20137_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.PositionAsUV1>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m20137_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3638_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m20137_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m20138_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.PositionAsUV1>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m20138_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3638_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m20138_GenericMethod/* genericMethod */

};
extern Il2CppType PositionAsUV1_t414_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m20139_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.PositionAsUV1>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m20139_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3638_il2cpp_TypeInfo/* declaring_type */
	, &PositionAsUV1_t414_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m20139_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3638_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m20135_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20136_MethodInfo,
	&InternalEnumerator_1_Dispose_m20137_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20138_MethodInfo,
	&InternalEnumerator_1_get_Current_m20139_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m20138_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m20137_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3638_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20136_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20138_MethodInfo,
	&InternalEnumerator_1_Dispose_m20137_MethodInfo,
	&InternalEnumerator_1_get_Current_m20139_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3638_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6246_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3638_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6246_il2cpp_TypeInfo, 7},
};
extern TypeInfo PositionAsUV1_t414_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3638_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m20139_MethodInfo/* Method Usage */,
	&PositionAsUV1_t414_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisPositionAsUV1_t414_m34939_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3638_0_0_0;
extern Il2CppType InternalEnumerator_1_t3638_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3638_GenericClass;
TypeInfo InternalEnumerator_1_t3638_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3638_MethodInfos/* methods */
	, InternalEnumerator_1_t3638_PropertyInfos/* properties */
	, InternalEnumerator_1_t3638_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3638_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3638_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3638_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3638_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3638_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3638_1_0_0/* this_arg */
	, InternalEnumerator_1_t3638_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3638_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3638_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3638)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7935_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>
extern MethodInfo ICollection_1_get_Count_m45006_MethodInfo;
static PropertyInfo ICollection_1_t7935____Count_PropertyInfo = 
{
	&ICollection_1_t7935_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m45006_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m45007_MethodInfo;
static PropertyInfo ICollection_1_t7935____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7935_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m45007_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7935_PropertyInfos[] =
{
	&ICollection_1_t7935____Count_PropertyInfo,
	&ICollection_1_t7935____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m45006_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>::get_Count()
MethodInfo ICollection_1_get_Count_m45006_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7935_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m45006_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m45007_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m45007_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7935_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m45007_GenericMethod/* genericMethod */

};
extern Il2CppType PositionAsUV1_t414_0_0_0;
extern Il2CppType PositionAsUV1_t414_0_0_0;
static ParameterInfo ICollection_1_t7935_ICollection_1_Add_m45008_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PositionAsUV1_t414_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m45008_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>::Add(T)
MethodInfo ICollection_1_Add_m45008_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7935_ICollection_1_Add_m45008_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m45008_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m45009_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>::Clear()
MethodInfo ICollection_1_Clear_m45009_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m45009_GenericMethod/* genericMethod */

};
extern Il2CppType PositionAsUV1_t414_0_0_0;
static ParameterInfo ICollection_1_t7935_ICollection_1_Contains_m45010_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PositionAsUV1_t414_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m45010_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>::Contains(T)
MethodInfo ICollection_1_Contains_m45010_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7935_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7935_ICollection_1_Contains_m45010_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m45010_GenericMethod/* genericMethod */

};
extern Il2CppType PositionAsUV1U5BU5D_t5646_0_0_0;
extern Il2CppType PositionAsUV1U5BU5D_t5646_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7935_ICollection_1_CopyTo_m45011_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &PositionAsUV1U5BU5D_t5646_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m45011_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m45011_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7935_ICollection_1_CopyTo_m45011_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m45011_GenericMethod/* genericMethod */

};
extern Il2CppType PositionAsUV1_t414_0_0_0;
static ParameterInfo ICollection_1_t7935_ICollection_1_Remove_m45012_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PositionAsUV1_t414_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m45012_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.PositionAsUV1>::Remove(T)
MethodInfo ICollection_1_Remove_m45012_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7935_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7935_ICollection_1_Remove_m45012_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m45012_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7935_MethodInfos[] =
{
	&ICollection_1_get_Count_m45006_MethodInfo,
	&ICollection_1_get_IsReadOnly_m45007_MethodInfo,
	&ICollection_1_Add_m45008_MethodInfo,
	&ICollection_1_Clear_m45009_MethodInfo,
	&ICollection_1_Contains_m45010_MethodInfo,
	&ICollection_1_CopyTo_m45011_MethodInfo,
	&ICollection_1_Remove_m45012_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7937_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7935_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7937_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7935_0_0_0;
extern Il2CppType ICollection_1_t7935_1_0_0;
struct ICollection_1_t7935;
extern Il2CppGenericClass ICollection_1_t7935_GenericClass;
TypeInfo ICollection_1_t7935_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7935_MethodInfos/* methods */
	, ICollection_1_t7935_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7935_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7935_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7935_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7935_0_0_0/* byval_arg */
	, &ICollection_1_t7935_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7935_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.PositionAsUV1>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.PositionAsUV1>
extern Il2CppType IEnumerator_1_t6246_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m45013_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.PositionAsUV1>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m45013_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7937_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6246_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m45013_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7937_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m45013_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7937_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7937_0_0_0;
extern Il2CppType IEnumerable_1_t7937_1_0_0;
struct IEnumerable_1_t7937;
extern Il2CppGenericClass IEnumerable_1_t7937_GenericClass;
TypeInfo IEnumerable_1_t7937_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7937_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7937_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7937_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7937_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7937_0_0_0/* byval_arg */
	, &IEnumerable_1_t7937_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7937_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7936_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.PositionAsUV1>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.PositionAsUV1>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.PositionAsUV1>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.PositionAsUV1>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.PositionAsUV1>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.PositionAsUV1>
extern MethodInfo IList_1_get_Item_m45014_MethodInfo;
extern MethodInfo IList_1_set_Item_m45015_MethodInfo;
static PropertyInfo IList_1_t7936____Item_PropertyInfo = 
{
	&IList_1_t7936_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m45014_MethodInfo/* get */
	, &IList_1_set_Item_m45015_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7936_PropertyInfos[] =
{
	&IList_1_t7936____Item_PropertyInfo,
	NULL
};
extern Il2CppType PositionAsUV1_t414_0_0_0;
static ParameterInfo IList_1_t7936_IList_1_IndexOf_m45016_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &PositionAsUV1_t414_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m45016_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.PositionAsUV1>::IndexOf(T)
MethodInfo IList_1_IndexOf_m45016_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7936_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7936_IList_1_IndexOf_m45016_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m45016_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType PositionAsUV1_t414_0_0_0;
static ParameterInfo IList_1_t7936_IList_1_Insert_m45017_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &PositionAsUV1_t414_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m45017_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.PositionAsUV1>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m45017_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7936_IList_1_Insert_m45017_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m45017_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7936_IList_1_RemoveAt_m45018_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m45018_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.PositionAsUV1>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m45018_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7936_IList_1_RemoveAt_m45018_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m45018_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7936_IList_1_get_Item_m45014_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType PositionAsUV1_t414_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m45014_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.PositionAsUV1>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m45014_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7936_il2cpp_TypeInfo/* declaring_type */
	, &PositionAsUV1_t414_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7936_IList_1_get_Item_m45014_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m45014_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType PositionAsUV1_t414_0_0_0;
static ParameterInfo IList_1_t7936_IList_1_set_Item_m45015_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &PositionAsUV1_t414_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m45015_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.PositionAsUV1>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m45015_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7936_IList_1_set_Item_m45015_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m45015_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7936_MethodInfos[] =
{
	&IList_1_IndexOf_m45016_MethodInfo,
	&IList_1_Insert_m45017_MethodInfo,
	&IList_1_RemoveAt_m45018_MethodInfo,
	&IList_1_get_Item_m45014_MethodInfo,
	&IList_1_set_Item_m45015_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7936_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7935_il2cpp_TypeInfo,
	&IEnumerable_1_t7937_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7936_0_0_0;
extern Il2CppType IList_1_t7936_1_0_0;
struct IList_1_t7936;
extern Il2CppGenericClass IList_1_t7936_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7936_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7936_MethodInfos/* methods */
	, IList_1_t7936_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7936_il2cpp_TypeInfo/* element_class */
	, IList_1_t7936_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7936_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7936_0_0_0/* byval_arg */
	, &IList_1_t7936_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7936_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.PositionAsUV1>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_84.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3639_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.PositionAsUV1>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_84MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.PositionAsUV1>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_86.h"
extern TypeInfo InvokableCall_1_t3640_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.PositionAsUV1>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_86MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m20142_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m20144_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.PositionAsUV1>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.PositionAsUV1>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.PositionAsUV1>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t3639____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t3639_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3639, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3639_FieldInfos[] =
{
	&CachedInvokableCall_1_t3639____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType PositionAsUV1_t414_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3639_CachedInvokableCall_1__ctor_m20140_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &PositionAsUV1_t414_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m20140_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.PositionAsUV1>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m20140_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t3639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3639_CachedInvokableCall_1__ctor_m20140_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m20140_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3639_CachedInvokableCall_1_Invoke_m20141_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m20141_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.PositionAsUV1>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m20141_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t3639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3639_CachedInvokableCall_1_Invoke_m20141_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m20141_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3639_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m20140_MethodInfo,
	&CachedInvokableCall_1_Invoke_m20141_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m20141_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m20145_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3639_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m20141_MethodInfo,
	&InvokableCall_1_Find_m20145_MethodInfo,
};
extern Il2CppType UnityAction_1_t3641_0_0_0;
extern TypeInfo UnityAction_1_t3641_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisPositionAsUV1_t414_m34949_MethodInfo;
extern TypeInfo PositionAsUV1_t414_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m20147_MethodInfo;
extern TypeInfo PositionAsUV1_t414_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3639_RGCTXData[8] = 
{
	&UnityAction_1_t3641_0_0_0/* Type Usage */,
	&UnityAction_1_t3641_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisPositionAsUV1_t414_m34949_MethodInfo/* Method Usage */,
	&PositionAsUV1_t414_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m20147_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m20142_MethodInfo/* Method Usage */,
	&PositionAsUV1_t414_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m20144_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3639_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3639_1_0_0;
struct CachedInvokableCall_1_t3639;
extern Il2CppGenericClass CachedInvokableCall_1_t3639_GenericClass;
TypeInfo CachedInvokableCall_1_t3639_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3639_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3639_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3640_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3639_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3639_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3639_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3639_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3639_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3639_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3639_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3639)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.UI.PositionAsUV1>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_91.h"
extern TypeInfo UnityAction_1_t3641_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.PositionAsUV1>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_91MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.PositionAsUV1>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.PositionAsUV1>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisPositionAsUV1_t414_m34949(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.PositionAsUV1>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.PositionAsUV1>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.PositionAsUV1>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.PositionAsUV1>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.PositionAsUV1>
extern Il2CppType UnityAction_1_t3641_0_0_1;
FieldInfo InvokableCall_1_t3640____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3641_0_0_1/* type */
	, &InvokableCall_1_t3640_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3640, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3640_FieldInfos[] =
{
	&InvokableCall_1_t3640____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3640_InvokableCall_1__ctor_m20142_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m20142_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.PositionAsUV1>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m20142_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t3640_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3640_InvokableCall_1__ctor_m20142_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m20142_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3641_0_0_0;
static ParameterInfo InvokableCall_1_t3640_InvokableCall_1__ctor_m20143_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3641_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m20143_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.PositionAsUV1>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m20143_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t3640_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3640_InvokableCall_1__ctor_m20143_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m20143_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t3640_InvokableCall_1_Invoke_m20144_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m20144_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.PositionAsUV1>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m20144_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t3640_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3640_InvokableCall_1_Invoke_m20144_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m20144_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3640_InvokableCall_1_Find_m20145_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m20145_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.PositionAsUV1>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m20145_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t3640_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3640_InvokableCall_1_Find_m20145_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m20145_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3640_MethodInfos[] =
{
	&InvokableCall_1__ctor_m20142_MethodInfo,
	&InvokableCall_1__ctor_m20143_MethodInfo,
	&InvokableCall_1_Invoke_m20144_MethodInfo,
	&InvokableCall_1_Find_m20145_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3640_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m20144_MethodInfo,
	&InvokableCall_1_Find_m20145_MethodInfo,
};
extern TypeInfo UnityAction_1_t3641_il2cpp_TypeInfo;
extern TypeInfo PositionAsUV1_t414_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3640_RGCTXData[5] = 
{
	&UnityAction_1_t3641_0_0_0/* Type Usage */,
	&UnityAction_1_t3641_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisPositionAsUV1_t414_m34949_MethodInfo/* Method Usage */,
	&PositionAsUV1_t414_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m20147_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3640_0_0_0;
extern Il2CppType InvokableCall_1_t3640_1_0_0;
struct InvokableCall_1_t3640;
extern Il2CppGenericClass InvokableCall_1_t3640_GenericClass;
TypeInfo InvokableCall_1_t3640_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3640_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3640_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3640_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3640_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3640_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3640_0_0_0/* byval_arg */
	, &InvokableCall_1_t3640_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3640_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3640_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3640)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.PositionAsUV1>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.PositionAsUV1>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.PositionAsUV1>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.PositionAsUV1>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.PositionAsUV1>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3641_UnityAction_1__ctor_m20146_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m20146_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.PositionAsUV1>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m20146_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t3641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3641_UnityAction_1__ctor_m20146_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m20146_GenericMethod/* genericMethod */

};
extern Il2CppType PositionAsUV1_t414_0_0_0;
static ParameterInfo UnityAction_1_t3641_UnityAction_1_Invoke_m20147_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &PositionAsUV1_t414_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m20147_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.PositionAsUV1>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m20147_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t3641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3641_UnityAction_1_Invoke_m20147_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m20147_GenericMethod/* genericMethod */

};
extern Il2CppType PositionAsUV1_t414_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3641_UnityAction_1_BeginInvoke_m20148_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &PositionAsUV1_t414_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m20148_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.PositionAsUV1>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m20148_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t3641_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3641_UnityAction_1_BeginInvoke_m20148_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m20148_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t3641_UnityAction_1_EndInvoke_m20149_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m20149_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.PositionAsUV1>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m20149_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t3641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3641_UnityAction_1_EndInvoke_m20149_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m20149_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3641_MethodInfos[] =
{
	&UnityAction_1__ctor_m20146_MethodInfo,
	&UnityAction_1_Invoke_m20147_MethodInfo,
	&UnityAction_1_BeginInvoke_m20148_MethodInfo,
	&UnityAction_1_EndInvoke_m20149_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m20148_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m20149_MethodInfo;
static MethodInfo* UnityAction_1_t3641_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m20147_MethodInfo,
	&UnityAction_1_BeginInvoke_m20148_MethodInfo,
	&UnityAction_1_EndInvoke_m20149_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3641_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3641_1_0_0;
struct UnityAction_1_t3641;
extern Il2CppGenericClass UnityAction_1_t3641_GenericClass;
TypeInfo UnityAction_1_t3641_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3641_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3641_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3641_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3641_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3641_0_0_0/* byval_arg */
	, &UnityAction_1_t3641_1_0_0/* this_arg */
	, UnityAction_1_t3641_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3641_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3641)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Shadow>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_85.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3642_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Shadow>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_85MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Shadow>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_87.h"
extern TypeInfo InvokableCall_1_t3643_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Shadow>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_87MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m20152_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m20154_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Shadow>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Shadow>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Shadow>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t3642____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t3642_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3642, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3642_FieldInfos[] =
{
	&CachedInvokableCall_1_t3642____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType Shadow_t413_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3642_CachedInvokableCall_1__ctor_m20150_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Shadow_t413_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m20150_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Shadow>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m20150_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t3642_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3642_CachedInvokableCall_1__ctor_m20150_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m20150_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3642_CachedInvokableCall_1_Invoke_m20151_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m20151_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Shadow>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m20151_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t3642_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3642_CachedInvokableCall_1_Invoke_m20151_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m20151_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3642_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m20150_MethodInfo,
	&CachedInvokableCall_1_Invoke_m20151_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m20151_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m20155_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3642_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m20151_MethodInfo,
	&InvokableCall_1_Find_m20155_MethodInfo,
};
extern Il2CppType UnityAction_1_t3644_0_0_0;
extern TypeInfo UnityAction_1_t3644_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisShadow_t413_m34950_MethodInfo;
extern TypeInfo Shadow_t413_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m20157_MethodInfo;
extern TypeInfo Shadow_t413_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3642_RGCTXData[8] = 
{
	&UnityAction_1_t3644_0_0_0/* Type Usage */,
	&UnityAction_1_t3644_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisShadow_t413_m34950_MethodInfo/* Method Usage */,
	&Shadow_t413_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m20157_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m20152_MethodInfo/* Method Usage */,
	&Shadow_t413_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m20154_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3642_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3642_1_0_0;
struct CachedInvokableCall_1_t3642;
extern Il2CppGenericClass CachedInvokableCall_1_t3642_GenericClass;
TypeInfo CachedInvokableCall_1_t3642_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3642_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3642_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3643_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3642_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3642_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3642_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3642_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3642_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3642_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3642_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3642)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.UI.Shadow>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_92.h"
extern TypeInfo UnityAction_1_t3644_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.Shadow>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_92MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.Shadow>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.Shadow>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisShadow_t413_m34950(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Shadow>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Shadow>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Shadow>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Shadow>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Shadow>
extern Il2CppType UnityAction_1_t3644_0_0_1;
FieldInfo InvokableCall_1_t3643____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3644_0_0_1/* type */
	, &InvokableCall_1_t3643_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3643, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3643_FieldInfos[] =
{
	&InvokableCall_1_t3643____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3643_InvokableCall_1__ctor_m20152_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m20152_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Shadow>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m20152_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t3643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3643_InvokableCall_1__ctor_m20152_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m20152_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3644_0_0_0;
static ParameterInfo InvokableCall_1_t3643_InvokableCall_1__ctor_m20153_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3644_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m20153_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Shadow>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m20153_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t3643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3643_InvokableCall_1__ctor_m20153_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m20153_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t3643_InvokableCall_1_Invoke_m20154_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m20154_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Shadow>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m20154_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t3643_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3643_InvokableCall_1_Invoke_m20154_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m20154_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3643_InvokableCall_1_Find_m20155_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m20155_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Shadow>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m20155_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t3643_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3643_InvokableCall_1_Find_m20155_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m20155_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3643_MethodInfos[] =
{
	&InvokableCall_1__ctor_m20152_MethodInfo,
	&InvokableCall_1__ctor_m20153_MethodInfo,
	&InvokableCall_1_Invoke_m20154_MethodInfo,
	&InvokableCall_1_Find_m20155_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3643_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m20154_MethodInfo,
	&InvokableCall_1_Find_m20155_MethodInfo,
};
extern TypeInfo UnityAction_1_t3644_il2cpp_TypeInfo;
extern TypeInfo Shadow_t413_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3643_RGCTXData[5] = 
{
	&UnityAction_1_t3644_0_0_0/* Type Usage */,
	&UnityAction_1_t3644_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisShadow_t413_m34950_MethodInfo/* Method Usage */,
	&Shadow_t413_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m20157_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3643_0_0_0;
extern Il2CppType InvokableCall_1_t3643_1_0_0;
struct InvokableCall_1_t3643;
extern Il2CppGenericClass InvokableCall_1_t3643_GenericClass;
TypeInfo InvokableCall_1_t3643_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3643_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3643_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3643_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3643_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3643_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3643_0_0_0/* byval_arg */
	, &InvokableCall_1_t3643_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3643_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3643_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3643)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Shadow>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Shadow>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.Shadow>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Shadow>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.Shadow>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3644_UnityAction_1__ctor_m20156_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m20156_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Shadow>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m20156_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t3644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3644_UnityAction_1__ctor_m20156_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m20156_GenericMethod/* genericMethod */

};
extern Il2CppType Shadow_t413_0_0_0;
static ParameterInfo UnityAction_1_t3644_UnityAction_1_Invoke_m20157_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Shadow_t413_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m20157_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Shadow>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m20157_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t3644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3644_UnityAction_1_Invoke_m20157_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m20157_GenericMethod/* genericMethod */

};
extern Il2CppType Shadow_t413_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3644_UnityAction_1_BeginInvoke_m20158_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Shadow_t413_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m20158_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.Shadow>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m20158_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t3644_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3644_UnityAction_1_BeginInvoke_m20158_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m20158_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t3644_UnityAction_1_EndInvoke_m20159_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m20159_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.Shadow>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m20159_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t3644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3644_UnityAction_1_EndInvoke_m20159_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m20159_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3644_MethodInfos[] =
{
	&UnityAction_1__ctor_m20156_MethodInfo,
	&UnityAction_1_Invoke_m20157_MethodInfo,
	&UnityAction_1_BeginInvoke_m20158_MethodInfo,
	&UnityAction_1_EndInvoke_m20159_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m20158_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m20159_MethodInfo;
static MethodInfo* UnityAction_1_t3644_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m20157_MethodInfo,
	&UnityAction_1_BeginInvoke_m20158_MethodInfo,
	&UnityAction_1_EndInvoke_m20159_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3644_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3644_1_0_0;
struct UnityAction_1_t3644;
extern Il2CppGenericClass UnityAction_1_t3644_GenericClass;
TypeInfo UnityAction_1_t3644_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3644_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3644_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3644_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3644_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3644_0_0_0/* byval_arg */
	, &UnityAction_1_t3644_1_0_0/* this_arg */
	, UnityAction_1_t3644_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3644_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3644)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6248_il2cpp_TypeInfo;

// Vuforia.FactorySetter
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_FactorySetter.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.FactorySetter>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.FactorySetter>
extern MethodInfo IEnumerator_1_get_Current_m45019_MethodInfo;
static PropertyInfo IEnumerator_1_t6248____Current_PropertyInfo = 
{
	&IEnumerator_1_t6248_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m45019_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6248_PropertyInfos[] =
{
	&IEnumerator_1_t6248____Current_PropertyInfo,
	NULL
};
extern Il2CppType FactorySetter_t147_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m45019_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.FactorySetter>::get_Current()
MethodInfo IEnumerator_1_get_Current_m45019_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6248_il2cpp_TypeInfo/* declaring_type */
	, &FactorySetter_t147_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m45019_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6248_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m45019_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6248_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6248_0_0_0;
extern Il2CppType IEnumerator_1_t6248_1_0_0;
struct IEnumerator_1_t6248;
extern Il2CppGenericClass IEnumerator_1_t6248_GenericClass;
TypeInfo IEnumerator_1_t6248_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6248_MethodInfos/* methods */
	, IEnumerator_1_t6248_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6248_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6248_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6248_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6248_0_0_0/* byval_arg */
	, &IEnumerator_1_t6248_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6248_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.FactorySetter>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_271.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3645_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.FactorySetter>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_271MethodDeclarations.h"

extern TypeInfo FactorySetter_t147_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m20164_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFactorySetter_t147_m34952_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.FactorySetter>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.FactorySetter>(System.Int32)
#define Array_InternalArray__get_Item_TisFactorySetter_t147_m34952(__this, p0, method) (FactorySetter_t147 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.FactorySetter>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.FactorySetter>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.FactorySetter>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.FactorySetter>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.FactorySetter>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.FactorySetter>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3645____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3645_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3645, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3645____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3645_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3645, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3645_FieldInfos[] =
{
	&InternalEnumerator_1_t3645____array_0_FieldInfo,
	&InternalEnumerator_1_t3645____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20161_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3645____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3645_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20161_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3645____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3645_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m20164_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3645_PropertyInfos[] =
{
	&InternalEnumerator_1_t3645____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3645____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3645_InternalEnumerator_1__ctor_m20160_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m20160_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.FactorySetter>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m20160_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3645_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3645_InternalEnumerator_1__ctor_m20160_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m20160_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20161_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.FactorySetter>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20161_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3645_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20161_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m20162_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.FactorySetter>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m20162_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3645_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m20162_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m20163_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.FactorySetter>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m20163_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3645_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m20163_GenericMethod/* genericMethod */

};
extern Il2CppType FactorySetter_t147_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m20164_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.FactorySetter>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m20164_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3645_il2cpp_TypeInfo/* declaring_type */
	, &FactorySetter_t147_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m20164_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3645_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m20160_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20161_MethodInfo,
	&InternalEnumerator_1_Dispose_m20162_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20163_MethodInfo,
	&InternalEnumerator_1_get_Current_m20164_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m20163_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m20162_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3645_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20161_MethodInfo,
	&InternalEnumerator_1_MoveNext_m20163_MethodInfo,
	&InternalEnumerator_1_Dispose_m20162_MethodInfo,
	&InternalEnumerator_1_get_Current_m20164_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3645_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6248_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3645_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6248_il2cpp_TypeInfo, 7},
};
extern TypeInfo FactorySetter_t147_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3645_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m20164_MethodInfo/* Method Usage */,
	&FactorySetter_t147_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisFactorySetter_t147_m34952_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3645_0_0_0;
extern Il2CppType InternalEnumerator_1_t3645_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3645_GenericClass;
TypeInfo InternalEnumerator_1_t3645_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3645_MethodInfos/* methods */
	, InternalEnumerator_1_t3645_PropertyInfos/* properties */
	, InternalEnumerator_1_t3645_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3645_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3645_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3645_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3645_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3645_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3645_1_0_0/* this_arg */
	, InternalEnumerator_1_t3645_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3645_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3645_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3645)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
