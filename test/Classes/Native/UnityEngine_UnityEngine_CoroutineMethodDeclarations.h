﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Coroutine
struct Coroutine_t332;
struct Coroutine_t332_marshaled;

// System.Void UnityEngine.Coroutine::.ctor()
 void Coroutine__ctor_m5436 (Coroutine_t332 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
 void Coroutine_ReleaseCoroutine_m5437 (Coroutine_t332 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::Finalize()
 void Coroutine_Finalize_m5438 (Coroutine_t332 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void Coroutine_t332_marshal(const Coroutine_t332& unmarshaled, Coroutine_t332_marshaled& marshaled);
void Coroutine_t332_marshal_back(const Coroutine_t332_marshaled& marshaled, Coroutine_t332& unmarshaled);
void Coroutine_t332_marshal_cleanup(Coroutine_t332_marshaled& marshaled);
