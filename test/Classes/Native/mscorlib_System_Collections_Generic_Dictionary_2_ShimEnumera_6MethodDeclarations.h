﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct ShimEnumerator_t3411;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>
struct Dictionary_2_t301;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ShimEnumera_3MethodDeclarations.h"
#define ShimEnumerator__ctor_m18748(__this, ___host, method) (void)ShimEnumerator__ctor_m17714_gshared((ShimEnumerator_t3289 *)__this, (Dictionary_2_t3275 *)___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::MoveNext()
#define ShimEnumerator_MoveNext_m18749(__this, method) (bool)ShimEnumerator_MoveNext_m17715_gshared((ShimEnumerator_t3289 *)__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Entry()
#define ShimEnumerator_get_Entry_m18750(__this, method) (DictionaryEntry_t1302 )ShimEnumerator_get_Entry_m17716_gshared((ShimEnumerator_t3289 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Key()
#define ShimEnumerator_get_Key_m18751(__this, method) (Object_t *)ShimEnumerator_get_Key_m17717_gshared((ShimEnumerator_t3289 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Value()
#define ShimEnumerator_get_Value_m18752(__this, method) (Object_t *)ShimEnumerator_get_Value_m17718_gshared((ShimEnumerator_t3289 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Current()
#define ShimEnumerator_get_Current_m18753(__this, method) (Object_t *)ShimEnumerator_get_Current_m17719_gshared((ShimEnumerator_t3289 *)__this, method)
