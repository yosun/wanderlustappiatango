﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WebCamTexture
struct WebCamTexture_t683;
// System.String
struct String_t;
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t886;

// System.Void UnityEngine.WebCamTexture::.ctor()
 void WebCamTexture__ctor_m4947 (WebCamTexture_t683 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)
 void WebCamTexture_Internal_CreateWebCamTexture_m6118 (Object_t * __this/* static, unused */, WebCamTexture_t683 * ___self, String_t* ___scriptingDevice, int32_t ___requestedWidth, int32_t ___requestedHeight, int32_t ___maxFramerate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::Play()
 void WebCamTexture_Play_m4952 (WebCamTexture_t683 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)
 void WebCamTexture_INTERNAL_CALL_Play_m6119 (Object_t * __this/* static, unused */, WebCamTexture_t683 * ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::Stop()
 void WebCamTexture_Stop_m4953 (WebCamTexture_t683 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Stop(UnityEngine.WebCamTexture)
 void WebCamTexture_INTERNAL_CALL_Stop_m6120 (Object_t * __this/* static, unused */, WebCamTexture_t683 * ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WebCamTexture::get_isPlaying()
 bool WebCamTexture_get_isPlaying_m4946 (WebCamTexture_t683 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::set_deviceName(System.String)
 void WebCamTexture_set_deviceName_m4948 (WebCamTexture_t683 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::set_requestedFPS(System.Single)
 void WebCamTexture_set_requestedFPS_m4949 (WebCamTexture_t683 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::set_requestedWidth(System.Int32)
 void WebCamTexture_set_requestedWidth_m4950 (WebCamTexture_t683 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::set_requestedHeight(System.Int32)
 void WebCamTexture_set_requestedHeight_m4951 (WebCamTexture_t683 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WebCamDevice[] UnityEngine.WebCamTexture::get_devices()
 WebCamDeviceU5BU5D_t886* WebCamTexture_get_devices_m5308 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WebCamTexture::get_didUpdateThisFrame()
 bool WebCamTexture_get_didUpdateThisFrame_m4945 (WebCamTexture_t683 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
