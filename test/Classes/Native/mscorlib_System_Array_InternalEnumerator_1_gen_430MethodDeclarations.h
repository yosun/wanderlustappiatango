﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>
struct InternalEnumerator_1_t4685;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.HorizontalWrapMode
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m28639 (InternalEnumerator_1_t4685 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m28640 (InternalEnumerator_1_t4685 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::Dispose()
 void InternalEnumerator_1_Dispose_m28641 (InternalEnumerator_1_t4685 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m28642 (InternalEnumerator_1_t4685 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.HorizontalWrapMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m28643 (InternalEnumerator_1_t4685 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
