﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// GyroCameraYo
struct GyroCameraYo_t11;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void GyroCameraYo::.ctor()
 void GyroCameraYo__ctor_m8 (GyroCameraYo_t11 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroCameraYo::Start()
 void GyroCameraYo_Start_m9 (GyroCameraYo_t11 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroCameraYo::Update()
 void GyroCameraYo_Update_m10 (GyroCameraYo_t11 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion GyroCameraYo::ConvertRotation(UnityEngine.Quaternion)
 Quaternion_t12  GyroCameraYo_ConvertRotation_m11 (Object_t * __this/* static, unused */, Quaternion_t12  ___q, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GyroCameraYo::GyroCam()
 void GyroCameraYo_GyroCam_m12 (GyroCameraYo_t11 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
