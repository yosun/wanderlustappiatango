﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator
struct DictionaryNodeEnumerator_t1301;
// System.Object
struct Object_t;
// System.Collections.Specialized.ListDictionary/DictionaryNode
struct DictionaryNode_t1300;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_t1296;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::.ctor(System.Collections.Specialized.ListDictionary)
 void DictionaryNodeEnumerator__ctor_m6737 (DictionaryNodeEnumerator_t1301 * __this, ListDictionary_t1296 * ___dict, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::FailFast()
 void DictionaryNodeEnumerator_FailFast_m6738 (DictionaryNodeEnumerator_t1301 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::MoveNext()
 bool DictionaryNodeEnumerator_MoveNext_m6739 (DictionaryNodeEnumerator_t1301 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::Reset()
 void DictionaryNodeEnumerator_Reset_m6740 (DictionaryNodeEnumerator_t1301 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_Current()
 Object_t * DictionaryNodeEnumerator_get_Current_m6741 (DictionaryNodeEnumerator_t1301 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Specialized.ListDictionary/DictionaryNode System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_DictionaryNode()
 DictionaryNode_t1300 * DictionaryNodeEnumerator_get_DictionaryNode_m6742 (DictionaryNodeEnumerator_t1301 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_Entry()
 DictionaryEntry_t1302  DictionaryNodeEnumerator_get_Entry_m6743 (DictionaryNodeEnumerator_t1301 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_Key()
 Object_t * DictionaryNodeEnumerator_get_Key_m6744 (DictionaryNodeEnumerator_t1301 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Specialized.ListDictionary/DictionaryNodeEnumerator::get_Value()
 Object_t * DictionaryNodeEnumerator_get_Value_m6745 (DictionaryNodeEnumerator_t1301 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
