﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
extern TypeInfo InternalsVisibleToAttribute_t904_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
extern MethodInfo InternalsVisibleToAttribute__ctor_m5418_MethodInfo;
extern TypeInfo RuntimeCompatibilityAttribute_t179_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
extern MethodInfo RuntimeCompatibilityAttribute__ctor_m676_MethodInfo;
extern MethodInfo RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m677_MethodInfo;
extern TypeInfo ExtensionAttribute_t779_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern MethodInfo ExtensionAttribute__ctor_m4364_MethodInfo;
void g_UnityEngine_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InternalsVisibleToAttribute_t904 * tmp;
		tmp = (InternalsVisibleToAttribute_t904 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t904_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5418(tmp, il2cpp_codegen_string_new_wrapper("Unity.Automation"), &InternalsVisibleToAttribute__ctor_m5418_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t904 * tmp;
		tmp = (InternalsVisibleToAttribute_t904 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t904_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5418(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Advertisements"), &InternalsVisibleToAttribute__ctor_m5418_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t904 * tmp;
		tmp = (InternalsVisibleToAttribute_t904 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t904_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5418(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Cloud"), &InternalsVisibleToAttribute__ctor_m5418_MethodInfo);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t904 * tmp;
		tmp = (InternalsVisibleToAttribute_t904 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t904_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5418(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Networking"), &InternalsVisibleToAttribute__ctor_m5418_MethodInfo);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t904 * tmp;
		tmp = (InternalsVisibleToAttribute_t904 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t904_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5418(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.TerrainPhysics"), &InternalsVisibleToAttribute__ctor_m5418_MethodInfo);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t904 * tmp;
		tmp = (InternalsVisibleToAttribute_t904 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t904_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5418(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Terrain"), &InternalsVisibleToAttribute__ctor_m5418_MethodInfo);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t904 * tmp;
		tmp = (InternalsVisibleToAttribute_t904 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t904_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5418(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests"), &InternalsVisibleToAttribute__ctor_m5418_MethodInfo);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t904 * tmp;
		tmp = (InternalsVisibleToAttribute_t904 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t904_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5418(tmp, il2cpp_codegen_string_new_wrapper("UnityEngine.Physics"), &InternalsVisibleToAttribute__ctor_m5418_MethodInfo);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t179 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t179 *)il2cpp_codegen_object_new (&RuntimeCompatibilityAttribute_t179_il2cpp_TypeInfo);
		RuntimeCompatibilityAttribute__ctor_m676(tmp, &RuntimeCompatibilityAttribute__ctor_m676_MethodInfo);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m677(tmp, true, &RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m677_MethodInfo);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t779 * tmp;
		tmp = (ExtensionAttribute_t779 *)il2cpp_codegen_object_new (&ExtensionAttribute_t779_il2cpp_TypeInfo);
		ExtensionAttribute__ctor_m4364(tmp, &ExtensionAttribute__ctor_m4364_MethodInfo);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t904 * tmp;
		tmp = (InternalsVisibleToAttribute_t904 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t904_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5418(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework.Tests"), &InternalsVisibleToAttribute__ctor_m5418_MethodInfo);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t904 * tmp;
		tmp = (InternalsVisibleToAttribute_t904 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t904_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5418(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests.Framework"), &InternalsVisibleToAttribute__ctor_m5418_MethodInfo);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t904 * tmp;
		tmp = (InternalsVisibleToAttribute_t904 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t904_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5418(tmp, il2cpp_codegen_string_new_wrapper("Unity.RuntimeTests"), &InternalsVisibleToAttribute__ctor_m5418_MethodInfo);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t904 * tmp;
		tmp = (InternalsVisibleToAttribute_t904 *)il2cpp_codegen_object_new (&InternalsVisibleToAttribute_t904_il2cpp_TypeInfo);
		InternalsVisibleToAttribute__ctor_m5418(tmp, il2cpp_codegen_string_new_wrapper("Unity.IntegrationTests.Framework"), &InternalsVisibleToAttribute__ctor_m5418_MethodInfo);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache g_UnityEngine_Assembly__CustomAttributeCache = {
14,
NULL,
&g_UnityEngine_Assembly_CustomAttributesCacheGenerator
};
