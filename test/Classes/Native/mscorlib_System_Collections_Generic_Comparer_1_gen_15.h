﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.CanvasGroup>
struct Comparer_1_t3493;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.CanvasGroup>
struct Comparer_1_t3493  : public Object_t
{
};
struct Comparer_1_t3493_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.CanvasGroup>::_default
	Comparer_1_t3493 * ____default_0;
};
