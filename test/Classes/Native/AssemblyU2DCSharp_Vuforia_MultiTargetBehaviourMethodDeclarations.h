﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MultiTargetBehaviour
struct MultiTargetBehaviour_t67;

// System.Void Vuforia.MultiTargetBehaviour::.ctor()
 void MultiTargetBehaviour__ctor_m162 (MultiTargetBehaviour_t67 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
