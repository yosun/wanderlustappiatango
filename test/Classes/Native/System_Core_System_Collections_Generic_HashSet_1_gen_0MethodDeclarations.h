﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t4389;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1066;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t455;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3225;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0.h"

// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor()
 void HashSet_1__ctor_m26882_gshared (HashSet_1_t4389 * __this, MethodInfo* method);
#define HashSet_1__ctor_m26882(__this, method) (void)HashSet_1__ctor_m26882_gshared((HashSet_1_t4389 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void HashSet_1__ctor_m26884_gshared (HashSet_1_t4389 * __this, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___context, MethodInfo* method);
#define HashSet_1__ctor_m26884(__this, ___info, ___context, method) (void)HashSet_1__ctor_m26884_gshared((HashSet_1_t4389 *)__this, (SerializationInfo_t1066 *)___info, (StreamingContext_t1067 )___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
 Object_t* HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26886_gshared (HashSet_1_t4389 * __this, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26886(__this, method) (Object_t*)HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26886_gshared((HashSet_1_t4389 *)__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
 bool HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26888_gshared (HashSet_1_t4389 * __this, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26888(__this, method) (bool)HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26888_gshared((HashSet_1_t4389 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
 void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m26890_gshared (HashSet_1_t4389 * __this, ObjectU5BU5D_t115* ___array, int32_t ___index, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m26890(__this, ___array, ___index, method) (void)HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m26890_gshared((HashSet_1_t4389 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
 void HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26892_gshared (HashSet_1_t4389 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26892(__this, ___item, method) (void)HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26892_gshared((HashSet_1_t4389 *)__this, (Object_t *)___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * HashSet_1_System_Collections_IEnumerable_GetEnumerator_m26894_gshared (HashSet_1_t4389 * __this, MethodInfo* method);
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m26894(__this, method) (Object_t *)HashSet_1_System_Collections_IEnumerable_GetEnumerator_m26894_gshared((HashSet_1_t4389 *)__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::get_Count()
 int32_t HashSet_1_get_Count_m26896_gshared (HashSet_1_t4389 * __this, MethodInfo* method);
#define HashSet_1_get_Count_m26896(__this, method) (int32_t)HashSet_1_get_Count_m26896_gshared((HashSet_1_t4389 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
 void HashSet_1_Init_m26898_gshared (HashSet_1_t4389 * __this, int32_t ___capacity, Object_t* ___comparer, MethodInfo* method);
#define HashSet_1_Init_m26898(__this, ___capacity, ___comparer, method) (void)HashSet_1_Init_m26898_gshared((HashSet_1_t4389 *)__this, (int32_t)___capacity, (Object_t*)___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::InitArrays(System.Int32)
 void HashSet_1_InitArrays_m26900_gshared (HashSet_1_t4389 * __this, int32_t ___size, MethodInfo* method);
#define HashSet_1_InitArrays_m26900(__this, ___size, method) (void)HashSet_1_InitArrays_m26900_gshared((HashSet_1_t4389 *)__this, (int32_t)___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::SlotsContainsAt(System.Int32,System.Int32,T)
 bool HashSet_1_SlotsContainsAt_m26902_gshared (HashSet_1_t4389 * __this, int32_t ___index, int32_t ___hash, Object_t * ___item, MethodInfo* method);
#define HashSet_1_SlotsContainsAt_m26902(__this, ___index, ___hash, ___item, method) (bool)HashSet_1_SlotsContainsAt_m26902_gshared((HashSet_1_t4389 *)__this, (int32_t)___index, (int32_t)___hash, (Object_t *)___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32)
 void HashSet_1_CopyTo_m26904_gshared (HashSet_1_t4389 * __this, ObjectU5BU5D_t115* ___array, int32_t ___index, MethodInfo* method);
#define HashSet_1_CopyTo_m26904(__this, ___array, ___index, method) (void)HashSet_1_CopyTo_m26904_gshared((HashSet_1_t4389 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::CopyTo(T[],System.Int32,System.Int32)
 void HashSet_1_CopyTo_m26906_gshared (HashSet_1_t4389 * __this, ObjectU5BU5D_t115* ___array, int32_t ___index, int32_t ___count, MethodInfo* method);
#define HashSet_1_CopyTo_m26906(__this, ___array, ___index, ___count, method) (void)HashSet_1_CopyTo_m26906_gshared((HashSet_1_t4389 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, (int32_t)___count, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Resize()
 void HashSet_1_Resize_m26908_gshared (HashSet_1_t4389 * __this, MethodInfo* method);
#define HashSet_1_Resize_m26908(__this, method) (void)HashSet_1_Resize_m26908_gshared((HashSet_1_t4389 *)__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetLinkHashCode(System.Int32)
 int32_t HashSet_1_GetLinkHashCode_m26910_gshared (HashSet_1_t4389 * __this, int32_t ___index, MethodInfo* method);
#define HashSet_1_GetLinkHashCode_m26910(__this, ___index, method) (int32_t)HashSet_1_GetLinkHashCode_m26910_gshared((HashSet_1_t4389 *)__this, (int32_t)___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<System.Object>::GetItemHashCode(T)
 int32_t HashSet_1_GetItemHashCode_m26912_gshared (HashSet_1_t4389 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_GetItemHashCode_m26912(__this, ___item, method) (int32_t)HashSet_1_GetItemHashCode_m26912_gshared((HashSet_1_t4389 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Add(T)
 bool HashSet_1_Add_m26913_gshared (HashSet_1_t4389 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_Add_m26913(__this, ___item, method) (bool)HashSet_1_Add_m26913_gshared((HashSet_1_t4389 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::Clear()
 void HashSet_1_Clear_m26914_gshared (HashSet_1_t4389 * __this, MethodInfo* method);
#define HashSet_1_Clear_m26914(__this, method) (void)HashSet_1_Clear_m26914_gshared((HashSet_1_t4389 *)__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Contains(T)
 bool HashSet_1_Contains_m26915_gshared (HashSet_1_t4389 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_Contains_m26915(__this, ___item, method) (bool)HashSet_1_Contains_m26915_gshared((HashSet_1_t4389 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<System.Object>::Remove(T)
 bool HashSet_1_Remove_m26917_gshared (HashSet_1_t4389 * __this, Object_t * ___item, MethodInfo* method);
#define HashSet_1_Remove_m26917(__this, ___item, method) (bool)HashSet_1_Remove_m26917_gshared((HashSet_1_t4389 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void HashSet_1_GetObjectData_m26919_gshared (HashSet_1_t4389 * __this, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___context, MethodInfo* method);
#define HashSet_1_GetObjectData_m26919(__this, ___info, ___context, method) (void)HashSet_1_GetObjectData_m26919_gshared((HashSet_1_t4389 *)__this, (SerializationInfo_t1066 *)___info, (StreamingContext_t1067 )___context, method)
// System.Void System.Collections.Generic.HashSet`1<System.Object>::OnDeserialization(System.Object)
 void HashSet_1_OnDeserialization_m26921_gshared (HashSet_1_t4389 * __this, Object_t * ___sender, MethodInfo* method);
#define HashSet_1_OnDeserialization_m26921(__this, ___sender, method) (void)HashSet_1_OnDeserialization_m26921_gshared((HashSet_1_t4389 *)__this, (Object_t *)___sender, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<System.Object>::GetEnumerator()
 Enumerator_t4392  HashSet_1_GetEnumerator_m26922 (HashSet_1_t4389 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
