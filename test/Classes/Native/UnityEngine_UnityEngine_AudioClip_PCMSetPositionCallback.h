﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.Int32
#include "mscorlib_System_Int32.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo Int32_t93_il2cpp_TypeInfo;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t1010  : public MulticastDelegate_t325
{
};
