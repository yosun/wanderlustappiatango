﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>
struct DefaultComparer_t5154;
// System.TimeSpan
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::.ctor()
 void DefaultComparer__ctor_m31422 (DefaultComparer_t5154 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::GetHashCode(T)
 int32_t DefaultComparer_GetHashCode_m31423 (DefaultComparer_t5154 * __this, TimeSpan_t113  ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.TimeSpan>::Equals(T,T)
 bool DefaultComparer_Equals_m31424 (DefaultComparer_t5154 * __this, TimeSpan_t113  ___x, TimeSpan_t113  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
