﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>
struct UnityAction_1_t2817;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>
struct InvokableCall_1_t2816  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::Delegate
	UnityAction_1_t2817 * ___Delegate_0;
};
