﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.AudioClip>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_147.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.AudioClip>
struct CachedInvokableCall_1_t4663  : public InvokableCall_1_t4664
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.AudioClip>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
