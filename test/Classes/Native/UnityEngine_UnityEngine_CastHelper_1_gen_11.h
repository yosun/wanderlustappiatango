﻿#pragma once
#include <stdint.h>
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t288;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.CanvasRenderer>
struct CastHelper_1_t3328 
{
	// T UnityEngine.CastHelper`1<UnityEngine.CanvasRenderer>::t
	CanvasRenderer_t288 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.CanvasRenderer>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
