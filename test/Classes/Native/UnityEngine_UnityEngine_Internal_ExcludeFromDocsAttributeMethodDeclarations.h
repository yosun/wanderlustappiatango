﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Internal.ExcludeFromDocsAttribute
struct ExcludeFromDocsAttribute_t1095;

// System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
 void ExcludeFromDocsAttribute__ctor_m6396 (ExcludeFromDocsAttribute_t1095 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
