﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.LayoutElement
struct LayoutElement_t390;

// System.Void UnityEngine.UI.LayoutElement::.ctor()
 void LayoutElement__ctor_m1721 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.LayoutElement::get_ignoreLayout()
 bool LayoutElement_get_ignoreLayout_m1722 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_ignoreLayout(System.Boolean)
 void LayoutElement_set_ignoreLayout_m1723 (LayoutElement_t390 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputHorizontal()
 void LayoutElement_CalculateLayoutInputHorizontal_m1724 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::CalculateLayoutInputVertical()
 void LayoutElement_CalculateLayoutInputVertical_m1725 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_minWidth()
 float LayoutElement_get_minWidth_m1726 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_minWidth(System.Single)
 void LayoutElement_set_minWidth_m1727 (LayoutElement_t390 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_minHeight()
 float LayoutElement_get_minHeight_m1728 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_minHeight(System.Single)
 void LayoutElement_set_minHeight_m1729 (LayoutElement_t390 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_preferredWidth()
 float LayoutElement_get_preferredWidth_m1730 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_preferredWidth(System.Single)
 void LayoutElement_set_preferredWidth_m1731 (LayoutElement_t390 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_preferredHeight()
 float LayoutElement_get_preferredHeight_m1732 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_preferredHeight(System.Single)
 void LayoutElement_set_preferredHeight_m1733 (LayoutElement_t390 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_flexibleWidth()
 float LayoutElement_get_flexibleWidth_m1734 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_flexibleWidth(System.Single)
 void LayoutElement_set_flexibleWidth_m1735 (LayoutElement_t390 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.LayoutElement::get_flexibleHeight()
 float LayoutElement_get_flexibleHeight_m1736 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::set_flexibleHeight(System.Single)
 void LayoutElement_set_flexibleHeight_m1737 (LayoutElement_t390 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.LayoutElement::get_layoutPriority()
 int32_t LayoutElement_get_layoutPriority_m1738 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnEnable()
 void LayoutElement_OnEnable_m1739 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnTransformParentChanged()
 void LayoutElement_OnTransformParentChanged_m1740 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnDisable()
 void LayoutElement_OnDisable_m1741 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnDidApplyAnimationProperties()
 void LayoutElement_OnDidApplyAnimationProperties_m1742 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::OnBeforeTransformParentChanged()
 void LayoutElement_OnBeforeTransformParentChanged_m1743 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.LayoutElement::SetDirty()
 void LayoutElement_SetDirty_m1744 (LayoutElement_t390 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
