﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t495;

// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
 void WaitForEndOfFrame__ctor_m2361 (WaitForEndOfFrame_t495 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
