﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ColorBlock
struct ColorBlock_t279;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.UI.ColorBlock
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"

// UnityEngine.Color UnityEngine.UI.ColorBlock::get_normalColor()
 Color_t19  ColorBlock_get_normalColor_m1003 (ColorBlock_t279 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_normalColor(UnityEngine.Color)
 void ColorBlock_set_normalColor_m1004 (ColorBlock_t279 * __this, Color_t19  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_highlightedColor()
 Color_t19  ColorBlock_get_highlightedColor_m1005 (ColorBlock_t279 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_highlightedColor(UnityEngine.Color)
 void ColorBlock_set_highlightedColor_m1006 (ColorBlock_t279 * __this, Color_t19  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_pressedColor()
 Color_t19  ColorBlock_get_pressedColor_m1007 (ColorBlock_t279 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_pressedColor(UnityEngine.Color)
 void ColorBlock_set_pressedColor_m1008 (ColorBlock_t279 * __this, Color_t19  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.ColorBlock::get_disabledColor()
 Color_t19  ColorBlock_get_disabledColor_m1009 (ColorBlock_t279 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_disabledColor(UnityEngine.Color)
 void ColorBlock_set_disabledColor_m1010 (ColorBlock_t279 * __this, Color_t19  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.ColorBlock::get_colorMultiplier()
 float ColorBlock_get_colorMultiplier_m1011 (ColorBlock_t279 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_colorMultiplier(System.Single)
 void ColorBlock_set_colorMultiplier_m1012 (ColorBlock_t279 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.ColorBlock::get_fadeDuration()
 float ColorBlock_get_fadeDuration_m1013 (ColorBlock_t279 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_fadeDuration(System.Single)
 void ColorBlock_set_fadeDuration_m1014 (ColorBlock_t279 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::get_defaultColorBlock()
 ColorBlock_t279  ColorBlock_get_defaultColorBlock_m1015 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
