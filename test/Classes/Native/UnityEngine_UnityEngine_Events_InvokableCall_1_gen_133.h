﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.SpriteRenderer>
struct UnityAction_1_t4598;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>
struct InvokableCall_1_t4597  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.SpriteRenderer>::Delegate
	UnityAction_1_t4598 * ___Delegate_0;
};
