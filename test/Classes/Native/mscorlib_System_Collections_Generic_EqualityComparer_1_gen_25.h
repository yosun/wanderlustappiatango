﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.ReconstructionAbstractBehaviour>
struct EqualityComparer_1_t3675;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.ReconstructionAbstractBehaviour>
struct EqualityComparer_1_t3675  : public Object_t
{
};
struct EqualityComparer_1_t3675_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.ReconstructionAbstractBehaviour>::_default
	EqualityComparer_1_t3675 * ____default_0;
};
