﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.GuidAttribute
struct GuidAttribute_t536;
// System.String
struct String_t;

// System.Void System.Runtime.InteropServices.GuidAttribute::.ctor(System.String)
 void GuidAttribute__ctor_m2587 (GuidAttribute_t536 * __this, String_t* ___guid, MethodInfo* method) IL2CPP_METHOD_ATTR;
