﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>
struct InternalEnumerator_1_t5052;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Runtime.ConstrainedExecution.Cer
#include "mscorlib_System_Runtime_ConstrainedExecution_Cer.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30840 (InternalEnumerator_1_t5052 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30841 (InternalEnumerator_1_t5052 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::Dispose()
 void InternalEnumerator_1_Dispose_m30842 (InternalEnumerator_1_t5052 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30843 (InternalEnumerator_1_t5052 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Runtime.ConstrainedExecution.Cer>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30844 (InternalEnumerator_1_t5052 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
