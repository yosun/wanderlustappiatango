﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Canvas>
struct List_1_t406;
// UnityEngine.Canvas
struct Canvas_t289;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>
struct Enumerator_t3354 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::l
	List_1_t406 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Canvas>::current
	Canvas_t289 * ___current_3;
};
