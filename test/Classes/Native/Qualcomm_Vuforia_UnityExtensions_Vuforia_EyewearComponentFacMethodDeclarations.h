﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.EyewearComponentFactory/NullEyewearComponentFactory
struct NullEyewearComponentFactory_t540;
// System.Type
struct Type_t;

// System.Type Vuforia.EyewearComponentFactory/NullEyewearComponentFactory::GetOVRInitControllerType()
 Type_t * NullEyewearComponentFactory_GetOVRInitControllerType_m2590 (NullEyewearComponentFactory_t540 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.EyewearComponentFactory/NullEyewearComponentFactory::.ctor()
 void NullEyewearComponentFactory__ctor_m2591 (NullEyewearComponentFactory_t540 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
